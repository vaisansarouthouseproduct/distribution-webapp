package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.OrderUsedProduct;

public class OrderProductDetailListForWebApp {

	private long srno;
	private String orderId;
	private String categoryName;
	private String brandName;
	private String productName;
	private String salesPersonName;
	private long employeeId;
	private String employeeGenId;
	private double rateWithTax;

	private double discount;
	private double discountAmt;
	private double netAmount;

	private long quantity;
	private long returnQuantity;
	private double totalAmount;
	private double totalAmountWithTax;
	private Date orderDate;
	private String type;
	private String reason;

	public OrderProductDetailListForWebApp(long srno, String orderId, String categoryName, String brandName,
			String productName, String salesPersonName, long employeeId, String employeeGenId, double rateWithTax,
			long quantity, double totalAmount, double totalAmountWithTax, Date orderDate, String type) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.categoryName = categoryName;
		this.brandName = brandName;
		this.productName = productName;
		this.salesPersonName = salesPersonName;
		this.employeeId = employeeId;
		this.employeeGenId = employeeGenId;
		this.rateWithTax = rateWithTax;
		this.quantity = quantity;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.orderDate = orderDate;
		this.type = type;
	}

	public OrderProductDetailListForWebApp(long srno, String orderId, String categoryName, String brandName,
			String productName, String salesPersonName, long employeeId, String employeeGenId, double rateWithTax,
			long quantity, long returnQuantity, double discount, double discountAmt, double netAmount,
			double totalAmountWithTax, Date orderDate, String type, String reason) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.categoryName = categoryName;
		this.brandName = brandName;
		this.productName = productName;
		this.salesPersonName = salesPersonName;
		this.employeeId = employeeId;
		this.employeeGenId = employeeGenId;
		this.rateWithTax = rateWithTax;
		this.quantity = quantity;
		this.returnQuantity = returnQuantity;
		this.discount = discount;
		this.discountAmt = discountAmt;
		this.netAmount = netAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.orderDate = orderDate;
		this.type = type;
		this.reason = reason;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSalesPersonName() {
		return salesPersonName;
	}

	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeGenId() {
		return employeeGenId;
	}

	public void setEmployeeGenId(String employeeGenId) {
		this.employeeGenId = employeeGenId;
	}

	public double getRateWithTax() {
		return rateWithTax;
	}

	public void setRateWithTax(double rateWithTax) {
		this.rateWithTax = rateWithTax;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "OrderProductDetailListForWebApp [srno=" + srno + ", orderId=" + orderId + ", categoryName="
				+ categoryName + ", brandName=" + brandName + ", productName=" + productName + ", salesPersonName="
				+ salesPersonName + ", employeeId=" + employeeId + ", employeeGenId=" + employeeGenId + ", rateWithTax="
				+ rateWithTax + ", discount=" + discount + ", discountAmt=" + discountAmt + ", netAmount=" + netAmount
				+ ", quantity=" + quantity + ", returnQuantity=" + returnQuantity + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", orderDate=" + orderDate + ", type=" + type
				+ ", reason=" + reason + "]";
	}

}
