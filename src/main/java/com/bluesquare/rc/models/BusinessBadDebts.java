package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.responseEntities.AreaModel;


public class BusinessBadDebts {

	private long srno;
	private String businessNameId;
	private String customerName;
	private String mobileNumber;
	private double totalAmountOfLoss;
	private AreaModel area;
	private int totalNumberOfOrder;
	private boolean status;
	private String counterOrderId;
	private Date badDebtsDefineDate;

	public BusinessBadDebts(long srno, String businessNameId, String customerName, String mobileNumber,
			double totalAmountOfLoss, AreaModel area, int totalNumberOfOrder, boolean status, String counterOrderId,
			Date badDebtsDefineDate) {
		super();
		this.srno = srno;
		this.businessNameId = businessNameId;
		this.customerName = customerName;
		this.mobileNumber = mobileNumber;
		this.totalAmountOfLoss = totalAmountOfLoss;
		this.area = area;
		this.totalNumberOfOrder = totalNumberOfOrder;
		this.status = status;
		this.counterOrderId = counterOrderId;
		this.badDebtsDefineDate = badDebtsDefineDate;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getTotalAmountOfLoss() {
		return totalAmountOfLoss;
	}

	public void setTotalAmountOfLoss(double totalAmountOfLoss) {
		this.totalAmountOfLoss = totalAmountOfLoss;
	}

	public AreaModel getArea() {
		return area;
	}

	public void setArea(AreaModel area) {
		this.area = area;
	}

	public int getTotalNumberOfOrder() {
		return totalNumberOfOrder;
	}

	public void setTotalNumberOfOrder(int totalNumberOfOrder) {
		this.totalNumberOfOrder = totalNumberOfOrder;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public Date getBadDebtsDefineDate() {
		return badDebtsDefineDate;
	}

	public void setBadDebtsDefineDate(Date badDebtsDefineDate) {
		this.badDebtsDefineDate = badDebtsDefineDate;
	}

	@Override
	public String toString() {
		return "BusinessBadDebts [srno=" + srno + ", businessNameId=" + businessNameId + ", customerName="
				+ customerName + ", mobileNumber=" + mobileNumber + ", totalAmountOfLoss=" + totalAmountOfLoss
				+ ", area=" + area + ", totalNumberOfOrder=" + totalNumberOfOrder + ", status=" + status
				+ ", counterOrderId=" + counterOrderId + ", badDebtsDefineDate=" + badDebtsDefineDate + "]";
	}

}
