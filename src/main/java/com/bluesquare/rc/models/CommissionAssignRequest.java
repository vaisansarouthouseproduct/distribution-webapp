package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.CommissionAssignResModel;

public class CommissionAssignRequest extends BaseDomain {

	public CommissionAssign commissionAssign;
	public CommissionAssignResModel commissionAssign1;
	public List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList;
	public List<CommissionAssignProducts> commissionAssignProductsList;

	public CommissionAssign getCommissionAssign() {
		return commissionAssign;
	}

	public void setCommissionAssign(CommissionAssign commissionAssign) {
		this.commissionAssign = commissionAssign;
	}

	public CommissionAssignResModel getCommissionAssign1() {
		return commissionAssign1;
	}

	public void setCommissionAssign1(CommissionAssignResModel commissionAssign1) {
		this.commissionAssign1 = commissionAssign1;
	}

	public List<CommissionAssignTargetSlabs> getCommissionAssignTargetSlabsList() {
		return commissionAssignTargetSlabsList;
	}

	public void setCommissionAssignTargetSlabsList(List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList) {
		this.commissionAssignTargetSlabsList = commissionAssignTargetSlabsList;
	}

	public List<CommissionAssignProducts> getCommissionAssignProductsList() {
		return commissionAssignProductsList;
	}

	public void setCommissionAssignProductsList(List<CommissionAssignProducts> commissionAssignProductsList) {
		this.commissionAssignProductsList = commissionAssignProductsList;
	}

	@Override
	public String toString() {
		return "CommissionAssignRequest [commissionAssign=" + commissionAssign + ", commissionAssign1="
				+ commissionAssign1 + ", commissionAssignTargetSlabsList=" + commissionAssignTargetSlabsList
				+ ", commissionAssignProductsList=" + commissionAssignProductsList + "]";
	}

}
