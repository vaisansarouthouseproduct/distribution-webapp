/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.EmployeeDetails;

/**
 * @author aNKIT
 *
 */

public class ComplainReplyModelForWeb 
{
	private long complainReplyId;
	private EmployeeDetails employeeDetailsReply;
	private String employeeReplyAreaList; 
	private EmployeeDetails employeeDetailsComplain;
	private String employeeComplainAreaList;
	private String message;
	private String reply;
	private Date msgDateTime;
	private Date replyDateTime;
	
	public ComplainReplyModelForWeb(long complainReplyId, EmployeeDetails employeeDetailsReply,
			String employeeReplyAreaList, EmployeeDetails employeeDetailsComplain, String employeeComplainAreaList,
			String message, String reply, Date msgDateTime, Date replyDateTime) {
		super();
		this.complainReplyId = complainReplyId;
		this.employeeDetailsReply = employeeDetailsReply;
		this.employeeReplyAreaList = employeeReplyAreaList;
		this.employeeDetailsComplain = employeeDetailsComplain;
		this.employeeComplainAreaList = employeeComplainAreaList;
		this.message = message;
		this.reply = reply;
		this.msgDateTime = msgDateTime;
		this.replyDateTime = replyDateTime;
	}
	public long getComplainReplyId() {
		return complainReplyId;
	}
	public void setComplainReplyId(long complainReplyId) {
		this.complainReplyId = complainReplyId;
	}
	public EmployeeDetails getEmployeeDetailsReply() {
		return employeeDetailsReply;
	}
	public void setEmployeeDetailsReply(EmployeeDetails employeeDetailsReply) {
		this.employeeDetailsReply = employeeDetailsReply;
	}
	public String getEmployeeReplyAreaList() {
		return employeeReplyAreaList;
	}
	public void setEmployeeReplyAreaList(String employeeReplyAreaList) {
		this.employeeReplyAreaList = employeeReplyAreaList;
	}
	public EmployeeDetails getEmployeeDetailsComplain() {
		return employeeDetailsComplain;
	}
	public void setEmployeeDetailsComplain(EmployeeDetails employeeDetailsComplain) {
		this.employeeDetailsComplain = employeeDetailsComplain;
	}
	public String getEmployeeComplainAreaList() {
		return employeeComplainAreaList;
	}
	public void setEmployeeComplainAreaList(String employeeComplainAreaList) {
		this.employeeComplainAreaList = employeeComplainAreaList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public Date getMsgDateTime() {
		return msgDateTime;
	}
	public void setMsgDateTime(Date msgDateTime) {
		this.msgDateTime = msgDateTime;
	}
	public Date getReplyDateTime() {
		return replyDateTime;
	}
	public void setReplyDateTime(Date replyDateTime) {
		this.replyDateTime = replyDateTime;
	}
	@Override
	public String toString() {
		return "ComplainReplyModelForWeb [complainReplyId=" + complainReplyId + ", employeeDetailsReply="
				+ employeeDetailsReply + ", employeeReplyAreaList=" + employeeReplyAreaList
				+ ", employeeDetailsComplain=" + employeeDetailsComplain + ", employeeComplainAreaList="
				+ employeeComplainAreaList + ", message=" + message + ", reply=" + reply + ", msgDateTime="
				+ msgDateTime + ", replyDateTime=" + replyDateTime + "]";
	}
	
	
}
