package com.bluesquare.rc.models;

import java.util.List;

public class EmployeeHolidayModel {

	private long employeeDetailPkId;
	private String employeeDetailId;
	private String name;
	private String departmentname;
	private String mobileNumber;
	private boolean status;
	private long noOfHolidays;
	private List<EmployeeHolidayList> employeeHolidayList;
	public EmployeeHolidayModel(long employeeDetailPkId, String employeeDetailId, String name, String departmentname,
			String mobileNumber, boolean status, long noOfHolidays, List<EmployeeHolidayList> employeeHolidayList) {
		super();
		this.employeeDetailPkId = employeeDetailPkId;
		this.employeeDetailId = employeeDetailId;
		this.name = name;
		this.departmentname = departmentname;
		this.mobileNumber = mobileNumber;
		this.status = status;
		this.noOfHolidays = noOfHolidays;
		this.employeeHolidayList = employeeHolidayList;
	}
	public long getEmployeeDetailPkId() {
		return employeeDetailPkId;
	}
	public void setEmployeeDetailPkId(long employeeDetailPkId) {
		this.employeeDetailPkId = employeeDetailPkId;
	}
	public String getEmployeeDetailId() {
		return employeeDetailId;
	}
	public void setEmployeeDetailId(String employeeDetailId) {
		this.employeeDetailId = employeeDetailId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public long getNoOfHolidays() {
		return noOfHolidays;
	}
	public void setNoOfHolidays(long noOfHolidays) {
		this.noOfHolidays = noOfHolidays;
	}
	public List<EmployeeHolidayList> getEmployeeHolidayList() {
		return employeeHolidayList;
	}
	public void setEmployeeHolidayList(List<EmployeeHolidayList> employeeHolidayList) {
		this.employeeHolidayList = employeeHolidayList;
	}
	@Override
	public String toString() {
		return "EmployeeHolidayModel [employeeDetailPkId=" + employeeDetailPkId + ", employeeDetailId="
				+ employeeDetailId + ", name=" + name + ", departmentname=" + departmentname + ", mobileNumber="
				+ mobileNumber + ", status=" + status + ", noOfHolidays=" + noOfHolidays + ", employeeHolidayList="
				+ employeeHolidayList + "]";
	}



}


