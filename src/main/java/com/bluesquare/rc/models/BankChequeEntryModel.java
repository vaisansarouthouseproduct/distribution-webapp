package com.bluesquare.rc.models;

import java.util.Date;

public class BankChequeEntryModel {

	Date chequeAddedDate;
	String chequeNo;
	String payeeName;
	double chequeAmt;
	Date chequeDate;
	String remark;

	public BankChequeEntryModel(Date chequeAddedDate, String chequeNo, String payeeName, double chequeAmt, Date chequeDate,
			String remark) {
		super();
		this.chequeAddedDate = chequeAddedDate;
		this.chequeNo = chequeNo;
		this.payeeName = payeeName;
		this.chequeAmt = chequeAmt;
		this.chequeDate = chequeDate;
		this.remark = remark;
	}

	public Date getChequeAddedDate() {
		return chequeAddedDate;
	}

	public void setChequeAddedDate(Date chequeAddedDate) {
		this.chequeAddedDate = chequeAddedDate;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public double getChequeAmt() {
		return chequeAmt;
	}

	public void setChequeAmt(double chequeAmt) {
		this.chequeAmt = chequeAmt;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "BankChequeEntry [chequeAddedDate=" + chequeAddedDate + ", chequeNo=" + chequeNo + ", payeeName="
				+ payeeName + ", chequeAmt=" + chequeAmt + ", chequeDate=" + chequeDate + ", remark=" + remark + "]";
	}

}
