package com.bluesquare.rc.models;

import java.util.Arrays;

public class EmployeeIdListRequest {
	public long employeeIds[];

	public long[] getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(long[] employeeIds) {
		this.employeeIds = employeeIds;
	}

	@Override
	public String toString() {
		return "EmployeeIdListRequest [employeeIds=" + Arrays.toString(employeeIds) + "]";
	}

}
