package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnCounterOrder;
import com.bluesquare.rc.entities.ReturnCounterOrderProducts;


public class ReturnCounterRequest {

	public ReturnCounterOrder returnCounterOrder;
	public List<ReturnCounterOrderProducts> returnCounterOrderProductsList;

	public double refAmount;
	public String payType;
	public String bankName;
	public String chequeNumber;
	public String chequeDate;
	public String transactionRef;
	public String comment;
	public long paymentMethodId;

	public ReturnCounterOrder getReturnCounterOrder() {
		return returnCounterOrder;
	}

	public void setReturnCounterOrder(ReturnCounterOrder returnCounterOrder) {
		this.returnCounterOrder = returnCounterOrder;
	}

	public List<ReturnCounterOrderProducts> getReturnCounterOrderProductsList() {
		return returnCounterOrderProductsList;
	}

	public void setReturnCounterOrderProductsList(List<ReturnCounterOrderProducts> returnCounterOrderProductsList) {
		this.returnCounterOrderProductsList = returnCounterOrderProductsList;
	}

	public double getRefAmount() {
		return refAmount;
	}

	public void setRefAmount(double refAmount) {
		this.refAmount = refAmount;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	@Override
	public String toString() {
		return "ReturnCounterRequest [returnCounterOrder=" + returnCounterOrder + ", returnCounterOrderProductsList="
				+ returnCounterOrderProductsList + ", refAmount=" + refAmount + ", payType=" + payType + ", bankName="
				+ bankName + ", chequeNumber=" + chequeNumber + ", chequeDate=" + chequeDate + ", transactionRef="
				+ transactionRef + ", comment=" + comment + ", paymentMethodId=" + paymentMethodId + "]";
	}

}
