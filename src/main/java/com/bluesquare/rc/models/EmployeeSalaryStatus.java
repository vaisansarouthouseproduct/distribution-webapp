package com.bluesquare.rc.models;

import java.util.Date;
import java.util.List;

public class EmployeeSalaryStatus {

	private long srno;
	private long employeeDetailsPkId;
	private String employeeDetailsId;
	private String name;
	private String departmentname;
	private String mobileNumber;
	private String address;
	private String areaList;
	private int noOfHolidays;
	private double totalAmountCurrentMonth;
	private double amountPaidCurrentMonth;
	private double amountPendingCurrentMonth;
	private double basicMonthlySalary;
	private double getIncentive;
	private double deduction;
	private double totalAmount;
	private Date date;
	private boolean status;
	private List<EmployeeSalaryModel> employeeSalaryList;
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getEmployeeDetailsPkId() {
		return employeeDetailsPkId;
	}
	public void setEmployeeDetailsPkId(long employeeDetailsPkId) {
		this.employeeDetailsPkId = employeeDetailsPkId;
	}
	public String getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(String employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAreaList() {
		return areaList;
	}
	public void setAreaList(String areaList) {
		this.areaList = areaList;
	}
	public int getNoOfHolidays() {
		return noOfHolidays;
	}
	public void setNoOfHolidays(int noOfHolidays) {
		this.noOfHolidays = noOfHolidays;
	}
	public double getTotalAmountCurrentMonth() {
		return totalAmountCurrentMonth;
	}
	public void setTotalAmountCurrentMonth(double totalAmountCurrentMonth) {
		this.totalAmountCurrentMonth = totalAmountCurrentMonth;
	}
	public double getAmountPaidCurrentMonth() {
		return amountPaidCurrentMonth;
	}
	public void setAmountPaidCurrentMonth(double amountPaidCurrentMonth) {
		this.amountPaidCurrentMonth = amountPaidCurrentMonth;
	}
	public double getAmountPendingCurrentMonth() {
		return amountPendingCurrentMonth;
	}
	public void setAmountPendingCurrentMonth(double amountPendingCurrentMonth) {
		this.amountPendingCurrentMonth = amountPendingCurrentMonth;
	}
	public double getBasicMonthlySalary() {
		return basicMonthlySalary;
	}
	public void setBasicMonthlySalary(double basicMonthlySalary) {
		this.basicMonthlySalary = basicMonthlySalary;
	}
	public double getGetIncentive() {
		return getIncentive;
	}
	public void setGetIncentive(double getIncentive) {
		this.getIncentive = getIncentive;
	}
	public double getDeduction() {
		return deduction;
	}
	public void setDeduction(double deduction) {
		this.deduction = deduction;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public List<EmployeeSalaryModel> getEmployeeSalaryList() {
		return employeeSalaryList;
	}
	public void setEmployeeSalaryList(List<EmployeeSalaryModel> employeeSalaryList) {
		this.employeeSalaryList = employeeSalaryList;
	}
	public EmployeeSalaryStatus(long srno, long employeeDetailsPkId, String employeeDetailsId, String name,
			String departmentname, String mobileNumber, String address, String areaList, int noOfHolidays,
			double totalAmountCurrentMonth, double amountPaidCurrentMonth, double amountPendingCurrentMonth,
			double basicMonthlySalary, double getIncentive, double deduction, double totalAmount, Date date,
			boolean status, List<EmployeeSalaryModel> employeeSalaryList) {
		super();
		this.srno = srno;
		this.employeeDetailsPkId = employeeDetailsPkId;
		this.employeeDetailsId = employeeDetailsId;
		this.name = name;
		this.departmentname = departmentname;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.areaList = areaList;
		this.noOfHolidays = noOfHolidays;
		this.totalAmountCurrentMonth = totalAmountCurrentMonth;
		this.amountPaidCurrentMonth = amountPaidCurrentMonth;
		this.amountPendingCurrentMonth = amountPendingCurrentMonth;
		this.basicMonthlySalary = basicMonthlySalary;
		this.getIncentive = getIncentive;
		this.deduction = deduction;
		this.totalAmount = totalAmount;
		this.date = date;
		this.status = status;
		this.employeeSalaryList = employeeSalaryList;
	}
	@Override
	public String toString() {
		return "EmployeeSalaryStatus [srno=" + srno + ", employeeDetailsPkId=" + employeeDetailsPkId
				+ ", employeeDetailsId=" + employeeDetailsId + ", name=" + name + ", departmentname=" + departmentname
				+ ", mobileNumber=" + mobileNumber + ", address=" + address + ", areaList=" + areaList
				+ ", noOfHolidays=" + noOfHolidays + ", totalAmountCurrentMonth=" + totalAmountCurrentMonth
				+ ", amountPaidCurrentMonth=" + amountPaidCurrentMonth + ", amountPendingCurrentMonth="
				+ amountPendingCurrentMonth + ", basicMonthlySalary=" + basicMonthlySalary + ", getIncentive="
				+ getIncentive + ", deduction=" + deduction + ", totalAmount=" + totalAmount + ", date=" + date
				+ ", status=" + status + ", employeeSalaryList=" + employeeSalaryList + "]";
	}

	
	
}
