package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.CounterOrder;
public class CounterOrderDetailsModel {

	private CounterOrder counterOrder;
	private String gateKeeperName;

	public CounterOrderDetailsModel(CounterOrder counterOrder, String gateKeeperName) {
		super();
		this.counterOrder = counterOrder;
		this.gateKeeperName = gateKeeperName;
	}

	public CounterOrder getCounterOrder() {
		return counterOrder;
	}

	public void setCounterOrder(CounterOrder counterOrder) {
		this.counterOrder = counterOrder;
	}

	public String getGateKeeperName() {
		return gateKeeperName;
	}

	public void setGateKeeperName(String gateKeeperName) {
		this.gateKeeperName = gateKeeperName;
	}

	@Override
	public String toString() {
		return "CounterOrderDetailsModel [counterOrder=" + counterOrder + ", gateKeeperName=" + gateKeeperName + "]";
	}

}
