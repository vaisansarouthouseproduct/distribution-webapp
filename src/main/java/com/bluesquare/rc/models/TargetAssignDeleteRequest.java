package com.bluesquare.rc.models;

public class TargetAssignDeleteRequest {
	public String deleteRegular;
	public long targetAssignId;

	public String getDeleteRegular() {
		return deleteRegular;
	}

	public void setDeleteRegular(String deleteRegular) {
		this.deleteRegular = deleteRegular;
	}

	public long getTargetAssignId() {
		return targetAssignId;
	}

	public void setTargetAssignId(long targetAssignId) {
		this.targetAssignId = targetAssignId;
	}

	@Override
	public String toString() {
		return "TargetAssignDeleteRequest [deleteRegular=" + deleteRegular + ", targetAssignId=" + targetAssignId + "]";
	}

}
