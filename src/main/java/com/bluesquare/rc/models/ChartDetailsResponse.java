/**
 * 
 */
package com.bluesquare.rc.models;

/**
 * @author aNKIT
 *
 */
public class ChartDetailsResponse {

	private double recieved;
	private double expected;
	
	
	private String productName1;
	private float productPercentage1;
	private String productName2;
	private float productPercentage2;
	private String productName3;
	private float productPercentage3;
	private String productName4;
	private float productPercentage4;
	private String productName5;
	private float productPercentage5;
	
	private String salesManName1;
	private float salesManPercentage1;
	private String salesManName2;
	private float salesManPercentage2;
	private String salesManName3;
	private float salesManPercentage3;
	private String salesManName4;
	private float salesManPercentage4;
	private String salesManName5;
	private float salesManPercentage5;
	
	private String returnProductName1;
	private float returnProductPercentage1;
	private String returnProductName2;
	private float returnProductPercentage2;
	private String returnProductName3;
	private float returnProductPercentage3;
	private String returnProductName4;
	private float returnProductPercentage4;
	private String returnProductName5;
	private float returnProductPercentage5;
	
	private double totalSales;
	private double totalAmountInvestInMarket;
	private double totalValueOfCurrentInventory;
	private long productUnderThresholdCount;
	
	/**
	 * 
	 * Nov 14, 20173:54:54 PM
	 */
	public ChartDetailsResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setProductNameChartDetailsResponse(String productName1, float productPercentage1, String productName2,
			float productPercentage2, String productName3, float productPercentage3, String productName4,
			float productPercentage4, String productName5, float productPercentage5) {
		this.productName1 = productName1;
		this.productPercentage1 = productPercentage1;
		this.productName2 = productName2;
		this.productPercentage2 = productPercentage2;
		this.productName3 = productName3;
		this.productPercentage3 = productPercentage3;
		this.productName4 = productName4;
		this.productPercentage4 = productPercentage4;
		this.productName5 = productName5;
		this.productPercentage5 = productPercentage5;
	}

	public void setSalesManChartDetailsResponse(String salesManName1, float salesManPercentage1, String salesManName2,
			float salesManPercentage2, String salesManName3, float salesManPercentage3, String salesManName4,
			float salesManPercentage4, String salesManName5, float salesManPercentage5) {
		this.salesManName1 = salesManName1;
		this.salesManPercentage1 = salesManPercentage1;
		this.salesManName2 = salesManName2;
		this.salesManPercentage2 = salesManPercentage2;
		this.salesManName3 = salesManName3;
		this.salesManPercentage3 = salesManPercentage3;
		this.salesManName4 = salesManName4;
		this.salesManPercentage4 = salesManPercentage4;
		this.salesManName5 = salesManName5;
		this.salesManPercentage5 = salesManPercentage5;
	}
	
	public void setReturnProductChartDetailsResponse(String returnProductName1, float returnProductPercentage1, String returnProductName2,
			float returnProductPercentage2, String returnProductName3, float returnProductPercentage3,
			String returnProductName4, float returnProductPercentage4, String returnProductName5,
			float returnProductPercentage5) {
		this.returnProductName1 = returnProductName1;
		this.returnProductPercentage1 = returnProductPercentage1;
		this.returnProductName2 = returnProductName2;
		this.returnProductPercentage2 = returnProductPercentage2;
		this.returnProductName3 = returnProductName3;
		this.returnProductPercentage3 = returnProductPercentage3;
		this.returnProductName4 = returnProductName4;
		this.returnProductPercentage4 = returnProductPercentage4;
		this.returnProductName5 = returnProductName5;
		this.returnProductPercentage5 = returnProductPercentage5;
	}

	public ChartDetailsResponse(double totalSales, double totalAmountInvestInMarket,
			double totalValueOfCurrentInventory, long productUnderThresholdCount) {
		super();
		this.totalSales = totalSales;
		this.totalAmountInvestInMarket = totalAmountInvestInMarket;
		this.totalValueOfCurrentInventory = totalValueOfCurrentInventory;
		this.productUnderThresholdCount = productUnderThresholdCount;
	}

	public ChartDetailsResponse(double recieved, double expected) {
		super();
		this.recieved = recieved;
		this.expected = expected;
	}

	public double getRecieved() {
		return recieved;
	}
	public void setRecieved(double recieved) {
		this.recieved = recieved;
	}
	public double getExpected() {
		return expected;
	}
	public void setExpected(double expected) {
		this.expected = expected;
	}
	public String getProductName1() {
		return productName1;
	}
	public void setProductName1(String productName1) {
		this.productName1 = productName1;
	}
	public float getProductPercentage1() {
		return productPercentage1;
	}
	public void setProductPercentage1(float productPercentage1) {
		this.productPercentage1 = productPercentage1;
	}
	public String getProductName2() {
		return productName2;
	}
	public void setProductName2(String productName2) {
		this.productName2 = productName2;
	}
	public float getProductPercentage2() {
		return productPercentage2;
	}
	public void setProductPercentage2(float productPercentage2) {
		this.productPercentage2 = productPercentage2;
	}
	public String getProductName3() {
		return productName3;
	}
	public void setProductName3(String productName3) {
		this.productName3 = productName3;
	}
	public float getProductPercentage3() {
		return productPercentage3;
	}
	public void setProductPercentage3(float productPercentage3) {
		this.productPercentage3 = productPercentage3;
	}
	public String getProductName4() {
		return productName4;
	}
	public void setProductName4(String productName4) {
		this.productName4 = productName4;
	}
	public float getProductPercentage4() {
		return productPercentage4;
	}
	public void setProductPercentage4(float productPercentage4) {
		this.productPercentage4 = productPercentage4;
	}
	public String getProductName5() {
		return productName5;
	}
	public void setProductName5(String productName5) {
		this.productName5 = productName5;
	}
	public float getProductPercentage5() {
		return productPercentage5;
	}
	public void setProductPercentage5(float productPercentage5) {
		this.productPercentage5 = productPercentage5;
	}
	public String getSalesManName1() {
		return salesManName1;
	}
	public void setSalesManName1(String salesManName1) {
		this.salesManName1 = salesManName1;
	}
	public float getSalesManPercentage1() {
		return salesManPercentage1;
	}
	public void setSalesManPercentage1(float salesManPercentage1) {
		this.salesManPercentage1 = salesManPercentage1;
	}
	public String getSalesManName2() {
		return salesManName2;
	}
	public void setSalesManName2(String salesManName2) {
		this.salesManName2 = salesManName2;
	}
	public float getSalesManPercentage2() {
		return salesManPercentage2;
	}
	public void setSalesManPercentage2(float salesManPercentage2) {
		this.salesManPercentage2 = salesManPercentage2;
	}
	public String getSalesManName3() {
		return salesManName3;
	}
	public void setSalesManName3(String salesManName3) {
		this.salesManName3 = salesManName3;
	}
	public float getSalesManPercentage3() {
		return salesManPercentage3;
	}
	public void setSalesManPercentage3(float salesManPercentage3) {
		this.salesManPercentage3 = salesManPercentage3;
	}
	public String getSalesManName4() {
		return salesManName4;
	}
	public void setSalesManName4(String salesManName4) {
		this.salesManName4 = salesManName4;
	}
	public float getSalesManPercentage4() {
		return salesManPercentage4;
	}
	public void setSalesManPercentage4(float salesManPercentage4) {
		this.salesManPercentage4 = salesManPercentage4;
	}
	public String getSalesManName5() {
		return salesManName5;
	}
	public void setSalesManName5(String salesManName5) {
		this.salesManName5 = salesManName5;
	}
	public float getSalesManPercentage5() {
		return salesManPercentage5;
	}
	public void setSalesManPercentage5(float salesManPercentage5) {
		this.salesManPercentage5 = salesManPercentage5;
	}
	public String getReturnProductName1() {
		return returnProductName1;
	}
	public void setReturnProductName1(String returnProductName1) {
		this.returnProductName1 = returnProductName1;
	}
	public float getReturnProductPercentage1() {
		return returnProductPercentage1;
	}
	public void setReturnProductPercentage1(float returnProductPercentage1) {
		this.returnProductPercentage1 = returnProductPercentage1;
	}
	public String getReturnProductName2() {
		return returnProductName2;
	}
	public void setReturnProductName2(String returnProductName2) {
		this.returnProductName2 = returnProductName2;
	}
	public float getReturnProductPercentage2() {
		return returnProductPercentage2;
	}
	public void setReturnProductPercentage2(float returnProductPercentage2) {
		this.returnProductPercentage2 = returnProductPercentage2;
	}
	public String getReturnProductName3() {
		return returnProductName3;
	}
	public void setReturnProductName3(String returnProductName3) {
		this.returnProductName3 = returnProductName3;
	}
	public float getReturnProductPercentage3() {
		return returnProductPercentage3;
	}
	public void setReturnProductPercentage3(float returnProductPercentage3) {
		this.returnProductPercentage3 = returnProductPercentage3;
	}
	public String getReturnProductName4() {
		return returnProductName4;
	}
	public void setReturnProductName4(String returnProductName4) {
		this.returnProductName4 = returnProductName4;
	}
	public float getReturnProductPercentage4() {
		return returnProductPercentage4;
	}
	public void setReturnProductPercentage4(float returnProductPercentage4) {
		this.returnProductPercentage4 = returnProductPercentage4;
	}
	public String getReturnProductName5() {
		return returnProductName5;
	}
	public void setReturnProductName5(String returnProductName5) {
		this.returnProductName5 = returnProductName5;
	}
	public float getReturnProductPercentage5() {
		return returnProductPercentage5;
	}
	public void setReturnProductPercentage5(float returnProductPercentage5) {
		this.returnProductPercentage5 = returnProductPercentage5;
	}
	public double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(double totalSales) {
		this.totalSales = totalSales;
	}
	public double getTotalAmountInvestInMarket() {
		return totalAmountInvestInMarket;
	}
	public void setTotalAmountInvestInMarket(double totalAmountInvestInMarket) {
		this.totalAmountInvestInMarket = totalAmountInvestInMarket;
	}
	public double getTotalValueOfCurrentInventory() {
		return totalValueOfCurrentInventory;
	}
	public void setTotalValueOfCurrentInventory(double totalValueOfCurrentInventory) {
		this.totalValueOfCurrentInventory = totalValueOfCurrentInventory;
	}
	public long getProductUnderThresholdCount() {
		return productUnderThresholdCount;
	}
	public void setProductUnderThresholdCount(long productUnderThresholdCount) {
		this.productUnderThresholdCount = productUnderThresholdCount;
	}
	@Override
	public String toString() {
		return "ChartDetailsResponse [recieved=" + recieved + ", expected=" + expected + ", productName1="
				+ productName1 + ", productPercentage1=" + productPercentage1 + ", productName2=" + productName2
				+ ", productPercentage2=" + productPercentage2 + ", productName3=" + productName3
				+ ", productPercentage3=" + productPercentage3 + ", productName4=" + productName4
				+ ", productPercentage4=" + productPercentage4 + ", productName5=" + productName5
				+ ", productPercentage5=" + productPercentage5 + ", salesManName1=" + salesManName1
				+ ", salesManPercentage1=" + salesManPercentage1 + ", salesManName2=" + salesManName2
				+ ", salesManPercentage2=" + salesManPercentage2 + ", salesManName3=" + salesManName3
				+ ", salesManPercentage3=" + salesManPercentage3 + ", salesManName4=" + salesManName4
				+ ", salesManPercentage4=" + salesManPercentage4 + ", salesManName5=" + salesManName5
				+ ", salesManPercentage5=" + salesManPercentage5 + ", returnProductName1=" + returnProductName1
				+ ", returnProductPercentage1=" + returnProductPercentage1 + ", returnProductName2="
				+ returnProductName2 + ", returnProductPercentage2=" + returnProductPercentage2
				+ ", returnProductName3=" + returnProductName3 + ", returnProductPercentage3="
				+ returnProductPercentage3 + ", returnProductName4=" + returnProductName4
				+ ", returnProductPercentage4=" + returnProductPercentage4 + ", returnProductName5="
				+ returnProductName5 + ", returnProductPercentage5=" + returnProductPercentage5 + ", totalSales="
				+ totalSales + ", totalAmountInvestInMarket=" + totalAmountInvestInMarket
				+ ", totalValueOfCurrentInventory=" + totalValueOfCurrentInventory + ", productUnderThresholdCount="
				+ productUnderThresholdCount + "]";
	}
	
}
