/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.EmployeeDetails;

/**
 * @author aNKIT
 *
 */
public class EmployeeLocation {

	private EmployeeDetails employeeDetails;
	private List<EmployeeRouteList> employeeRouteList;
	public EmployeeLocation(EmployeeDetails employeeDetails, List<EmployeeRouteList> employeeRouteList) {
		super();
		this.employeeDetails = employeeDetails;
		this.employeeRouteList = employeeRouteList;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public List<EmployeeRouteList> getEmployeeRouteList() {
		return employeeRouteList;
	}
	public void setEmployeeRouteList(List<EmployeeRouteList> employeeRouteList) {
		this.employeeRouteList = employeeRouteList;
	}
	@Override
	public String toString() {
		return "EmployeeLocation [employeeDetails=" + employeeDetails + ", employeeRouteList=" + employeeRouteList
				+ "]";
	}
	
}
