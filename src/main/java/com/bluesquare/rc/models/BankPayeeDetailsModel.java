package com.bluesquare.rc.models;

import java.util.Date;

import javax.persistence.Column;

import org.springframework.format.annotation.DateTimeFormat;

public class BankPayeeDetailsModel {

	private long payeeId;

	private String payeeName;

	private String payeeAddress;

	private Date payeeAddedDatetime;

	private Date payeeUpdatedDatetime;

	public BankPayeeDetailsModel(long payeeId, String payeeName, String payeeAddress, Date payeeAddedDatetime,
			Date payeeUpdatedDatetime) {
		super();
		this.payeeId = payeeId;
		this.payeeName = payeeName;
		this.payeeAddress = payeeAddress;
		this.payeeAddedDatetime = payeeAddedDatetime;
		this.payeeUpdatedDatetime = payeeUpdatedDatetime;
	}

	public long getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(long payeeId) {
		this.payeeId = payeeId;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getPayeeAddress() {
		return payeeAddress;
	}

	public void setPayeeAddress(String payeeAddress) {
		this.payeeAddress = payeeAddress;
	}

	public Date getPayeeAddedDatetime() {
		return payeeAddedDatetime;
	}

	public void setPayeeAddedDatetime(Date payeeAddedDatetime) {
		this.payeeAddedDatetime = payeeAddedDatetime;
	}

	public Date getPayeeUpdatedDatetime() {
		return payeeUpdatedDatetime;
	}

	public void setPayeeUpdatedDatetime(Date payeeUpdatedDatetime) {
		this.payeeUpdatedDatetime = payeeUpdatedDatetime;
	}

	@Override
	public String toString() {
		return "BankPayeeDetailsModel [payeeId=" + payeeId + ", payeeName=" + payeeName + ", payeeAddress="
				+ payeeAddress + ", payeeAddedDatetime=" + payeeAddedDatetime + ", payeeUpdatedDatetime="
				+ payeeUpdatedDatetime + "]";
	}

}
