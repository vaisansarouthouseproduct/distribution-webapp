package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.OfflineAPIRequest;

public class CancelOrderRequest  extends OfflineAPIRequest{
	
	private long employeeId;
	private String orderId;
	private String reason;
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "CancelOrderRequest [employeeId=" + employeeId + ", orderId=" + orderId + ", reason=" + reason + "]";
	}
	
	
	

}
