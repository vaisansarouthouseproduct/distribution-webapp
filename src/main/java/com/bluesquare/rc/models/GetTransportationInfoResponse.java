package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.rest.models.BaseDomain;

public class GetTransportationInfoResponse extends BaseDomain {
	
	TransportationModel transportation;

	public TransportationModel getTransportation() {
		return transportation;
	}

	public void setTransportation(TransportationModel transportation) {
		this.transportation = transportation;
	}

	@Override
	public String toString() {
		return "GetTransportationInfoResponse [transportation=" + transportation + "]";
	}
	
	
	
}
