package com.bluesquare.rc.models;

public class FetchAccountInfoRequest {
	public String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "FetchAccountInfoRequest [id=" + id + "]";
	}
}
