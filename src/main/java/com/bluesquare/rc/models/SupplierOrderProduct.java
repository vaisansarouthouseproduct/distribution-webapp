/**
 * 
 */
package com.bluesquare.rc.models;

/**
 * @author aNKIT
 *
 */
public class SupplierOrderProduct {

	private String productName;
	private long quatity;
	public SupplierOrderProduct(String productName, long quatity) {
		super();
		this.productName = productName;
		this.quatity = quatity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getQuatity() {
		return quatity;
	}
	public void setQuatity(long quatity) {
		this.quatity = quatity;
	}
	@Override
	public String toString() {
		return "SupplierOrderProduct [productName=" + productName + ", quatity=" + quatity + "]";
	}
	
	
}
