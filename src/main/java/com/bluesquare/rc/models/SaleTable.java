/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.OrderProductDetails;

/**
 * @author aNKIT
 *
 */
public class SaleTable {
	private long srNo;
	private String date;
	private OrderProductDetails orderProductDetails;
	private CounterOrderProductDetails counterOrderProductDetails;
	private double taxableAmount;
	private double cgstAmt;
	private double sgstAmt;
	private double igstAmt;
	private double totalAmount;
	public SaleTable(long srNo, String date, OrderProductDetails orderProductDetails, double taxableAmount,
			double cgstAmt, double sgstAmt, double igstAmt, double totalAmount) {
		super();
		this.srNo = srNo;
		this.date = date;
		this.orderProductDetails = orderProductDetails;
		this.taxableAmount = taxableAmount;
		this.cgstAmt = cgstAmt;
		this.sgstAmt = sgstAmt;
		this.igstAmt = igstAmt;
		this.totalAmount = totalAmount;
	}
	public SaleTable(long srNo, String date, CounterOrderProductDetails counterOrderProductDetails, double taxableAmount,
			double cgstAmt, double sgstAmt, double igstAmt, double totalAmount) {
		super();
		this.srNo = srNo;
		this.date = date;
		this.counterOrderProductDetails = counterOrderProductDetails;
		this.taxableAmount = taxableAmount;
		this.cgstAmt = cgstAmt;
		this.sgstAmt = sgstAmt;
		this.igstAmt = igstAmt;
		this.totalAmount = totalAmount;
	}
	public long getSrNo() {
		return srNo;
	}
	public void setSrNo(long srNo) {
		this.srNo = srNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public OrderProductDetails getOrderProductDetails() {
		return orderProductDetails;
	}
	public void setOrderProductDetails(OrderProductDetails orderProductDetails) {
		this.orderProductDetails = orderProductDetails;
	}
	public double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public double getCgstAmt() {
		return cgstAmt;
	}
	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}
	public double getSgstAmt() {
		return sgstAmt;
	}
	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}
	public double getIgstAmt() {
		return igstAmt;
	}
	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public CounterOrderProductDetails getCounterOrderProductDetails() {
		return counterOrderProductDetails;
	}
	public void setCounterOrderProductDetails(CounterOrderProductDetails counterOrderProductDetails) {
		this.counterOrderProductDetails = counterOrderProductDetails;
	}
	@Override
	public String toString() {
		return "SaleTable [srNo=" + srNo + ", date=" + date + ", orderProductDetails=" + orderProductDetails
				+ ", counterOrderProductDetails=" + counterOrderProductDetails + ", taxableAmount=" + taxableAmount
				+ ", cgstAmt=" + cgstAmt + ", sgstAmt=" + sgstAmt + ", igstAmt=" + igstAmt + ", totalAmount="
				+ totalAmount + "]";
	}
	
}
