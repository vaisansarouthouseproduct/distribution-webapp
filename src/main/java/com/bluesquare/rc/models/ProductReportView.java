/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.responseEntities.ProductModel;

public class ProductReportView {
	private long srno;
	private ProductModel product;
	private long issuedQuantity;
	private long damageQuantity;
	private long freeQuantity;
	private double amountWithTax;
	private double rateWithTax;
	public ProductReportView(long srno, ProductModel product, long issuedQuantity, long damageQuantity, long freeQuantity,
			double amountWithTax, double rateWithTax) {
		super();
		this.srno = srno;
		this.product = product;
		this.issuedQuantity = issuedQuantity;
		this.damageQuantity = damageQuantity;
		this.freeQuantity = freeQuantity;
		this.amountWithTax = amountWithTax;
		this.rateWithTax = rateWithTax;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public ProductModel getProduct() {
		return product;
	}
	public void setProduct(ProductModel product) {
		this.product = product;
	}
	public long getIssuedQuantity() {
		return issuedQuantity;
	}
	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}
	public long getDamageQuantity() {
		return damageQuantity;
	}
	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}
	public long getFreeQuantity() {
		return freeQuantity;
	}
	public void setFreeQuantity(long freeQuantity) {
		this.freeQuantity = freeQuantity;
	}
	public double getAmountWithTax() {
		return amountWithTax;
	}
	public void setAmountWithTax(double amountWithTax) {
		this.amountWithTax = amountWithTax;
	}
	public double getRateWithTax() {
		return rateWithTax;
	}
	public void setRateWithTax(double rateWithTax) {
		this.rateWithTax = rateWithTax;
	}
	@Override
	public String toString() {
		return "ProductReportView [srno=" + srno + ", product=" + product + ", issuedQuantity=" + issuedQuantity
				+ ", damageQuantity=" + damageQuantity + ", freeQuantity=" + freeQuantity + ", amountWithTax="
				+ amountWithTax + ", rateWithTax=" + rateWithTax + "]";
	}
}
