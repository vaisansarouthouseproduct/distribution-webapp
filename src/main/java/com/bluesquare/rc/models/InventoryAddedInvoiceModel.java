package com.bluesquare.rc.models;

import java.util.List;
import java.util.Map;

public class InventoryAddedInvoiceModel {

	String companyName;
	String companyAddress;
	String coTelNo;
	String gstInNo;
	String emailId;

	String billNo;
	String billDate;

	String supplierName;
	String supplierAddress;
	String StateCode;
	String mobNoAndTelNo;
	String supplierGstInNo;

	String inventoryTsnNo;
	String deliveryDate;

	List<Map<String, Object>> productList;// srNo, productName, hsnCode,
											// taxSlab, MRP, Qty, totalAmt,
											// discPer, discAmt, newTotalAmt

	String totalQuantity;
	String totalAmountBeforeDiscount;
	String discountPer;
	String discountAmt;
	String discountType;
	String totalAmountAfterDiscount;
	String roundOff;
	String totalAmountAfterRoundOff;

	String totalAmountAfterRoundOffInWord;

	List<Map<String, Object>> taxList;// hsnCode, taxableValue, cgstRate,
										// cgstAmt, sgstRate, sgstAmt, igstRate,
										// igstAmt
	String totalTaxableAmt;
	String totalCgstAmt;
	String totalSgstAmt;
	String totalIgstAmt;
	String discTaxableAmt;
	String discCgstAmt;
	String discSgstAmt;
	String discIgstAmt;
	String netTaxableAmt;
	String netCgstAmt;
	String netSgstAmt;
	String netIgstAmt;

	String netTaxAmtInWord;

	public InventoryAddedInvoiceModel(String companyName, String companyAddress, String coTelNo, String gstInNo,
			String emailId, String billNo, String billDate, String supplierName, String supplierAddress,
			String stateCode, String mobNoAndTelNo, String supplierGstInNo, String inventoryTsnNo, String deliveryDate,
			List<Map<String, Object>> productList, String totalQuantity, String totalAmountBeforeDiscount,
			String discountPer, String discountAmt, String discountType, String totalAmountAfterDiscount,
			String roundOff, String totalAmountAfterRoundOff, String totalAmountAfterRoundOffInWord,
			List<Map<String, Object>> taxList, String totalTaxableAmt, String totalCgstAmt, String totalSgstAmt,
			String totalIgstAmt, String discTaxableAmt, String discCgstAmt, String discSgstAmt, String discIgstAmt,
			String netTaxableAmt, String netCgstAmt, String netSgstAmt, String netIgstAmt, String netTaxAmtInWord) {
		super();
		this.companyName = companyName;
		this.companyAddress = companyAddress;
		this.coTelNo = coTelNo;
		this.gstInNo = gstInNo;
		this.emailId = emailId;
		this.billNo = billNo;
		this.billDate = billDate;
		this.supplierName = supplierName;
		this.supplierAddress = supplierAddress;
		StateCode = stateCode;
		this.mobNoAndTelNo = mobNoAndTelNo;
		this.supplierGstInNo = supplierGstInNo;
		this.inventoryTsnNo = inventoryTsnNo;
		this.deliveryDate = deliveryDate;
		this.productList = productList;
		this.totalQuantity = totalQuantity;
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
		this.discountPer = discountPer;
		this.discountAmt = discountAmt;
		this.discountType = discountType;
		this.totalAmountAfterDiscount = totalAmountAfterDiscount;
		this.roundOff = roundOff;
		this.totalAmountAfterRoundOff = totalAmountAfterRoundOff;
		this.totalAmountAfterRoundOffInWord = totalAmountAfterRoundOffInWord;
		this.taxList = taxList;
		this.totalTaxableAmt = totalTaxableAmt;
		this.totalCgstAmt = totalCgstAmt;
		this.totalSgstAmt = totalSgstAmt;
		this.totalIgstAmt = totalIgstAmt;
		this.discTaxableAmt = discTaxableAmt;
		this.discCgstAmt = discCgstAmt;
		this.discSgstAmt = discSgstAmt;
		this.discIgstAmt = discIgstAmt;
		this.netTaxableAmt = netTaxableAmt;
		this.netCgstAmt = netCgstAmt;
		this.netSgstAmt = netSgstAmt;
		this.netIgstAmt = netIgstAmt;
		this.netTaxAmtInWord = netTaxAmtInWord;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCoTelNo() {
		return coTelNo;
	}

	public void setCoTelNo(String coTelNo) {
		this.coTelNo = coTelNo;
	}

	public String getGstInNo() {
		return gstInNo;
	}

	public void setGstInNo(String gstInNo) {
		this.gstInNo = gstInNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierAddress() {
		return supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public String getStateCode() {
		return StateCode;
	}

	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}

	public String getMobNoAndTelNo() {
		return mobNoAndTelNo;
	}

	public void setMobNoAndTelNo(String mobNoAndTelNo) {
		this.mobNoAndTelNo = mobNoAndTelNo;
	}

	public String getSupplierGstInNo() {
		return supplierGstInNo;
	}

	public void setSupplierGstInNo(String supplierGstInNo) {
		this.supplierGstInNo = supplierGstInNo;
	}

	public String getInventoryTsnNo() {
		return inventoryTsnNo;
	}

	public void setInventoryTsnNo(String inventoryTsnNo) {
		this.inventoryTsnNo = inventoryTsnNo;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<Map<String, Object>> getProductList() {
		return productList;
	}

	public void setProductList(List<Map<String, Object>> productList) {
		this.productList = productList;
	}

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getTotalAmountBeforeDiscount() {
		return totalAmountBeforeDiscount;
	}

	public void setTotalAmountBeforeDiscount(String totalAmountBeforeDiscount) {
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
	}

	public String getDiscountPer() {
		return discountPer;
	}

	public void setDiscountPer(String discountPer) {
		this.discountPer = discountPer;
	}

	public String getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(String discountAmt) {
		this.discountAmt = discountAmt;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getTotalAmountAfterDiscount() {
		return totalAmountAfterDiscount;
	}

	public void setTotalAmountAfterDiscount(String totalAmountAfterDiscount) {
		this.totalAmountAfterDiscount = totalAmountAfterDiscount;
	}

	public String getRoundOff() {
		return roundOff;
	}

	public void setRoundOff(String roundOff) {
		this.roundOff = roundOff;
	}

	public String getTotalAmountAfterRoundOff() {
		return totalAmountAfterRoundOff;
	}

	public void setTotalAmountAfterRoundOff(String totalAmountAfterRoundOff) {
		this.totalAmountAfterRoundOff = totalAmountAfterRoundOff;
	}

	public String getTotalAmountAfterRoundOffInWord() {
		return totalAmountAfterRoundOffInWord;
	}

	public void setTotalAmountAfterRoundOffInWord(String totalAmountAfterRoundOffInWord) {
		this.totalAmountAfterRoundOffInWord = totalAmountAfterRoundOffInWord;
	}

	public List<Map<String, Object>> getTaxList() {
		return taxList;
	}

	public void setTaxList(List<Map<String, Object>> taxList) {
		this.taxList = taxList;
	}

	public String getTotalTaxableAmt() {
		return totalTaxableAmt;
	}

	public void setTotalTaxableAmt(String totalTaxableAmt) {
		this.totalTaxableAmt = totalTaxableAmt;
	}

	public String getTotalCgstAmt() {
		return totalCgstAmt;
	}

	public void setTotalCgstAmt(String totalCgstAmt) {
		this.totalCgstAmt = totalCgstAmt;
	}

	public String getTotalSgstAmt() {
		return totalSgstAmt;
	}

	public void setTotalSgstAmt(String totalSgstAmt) {
		this.totalSgstAmt = totalSgstAmt;
	}

	public String getTotalIgstAmt() {
		return totalIgstAmt;
	}

	public void setTotalIgstAmt(String totalIgstAmt) {
		this.totalIgstAmt = totalIgstAmt;
	}

	public String getDiscTaxableAmt() {
		return discTaxableAmt;
	}

	public void setDiscTaxableAmt(String discTaxableAmt) {
		this.discTaxableAmt = discTaxableAmt;
	}

	public String getDiscCgstAmt() {
		return discCgstAmt;
	}

	public void setDiscCgstAmt(String discCgstAmt) {
		this.discCgstAmt = discCgstAmt;
	}

	public String getDiscSgstAmt() {
		return discSgstAmt;
	}

	public void setDiscSgstAmt(String discSgstAmt) {
		this.discSgstAmt = discSgstAmt;
	}

	public String getDiscIgstAmt() {
		return discIgstAmt;
	}

	public void setDiscIgstAmt(String discIgstAmt) {
		this.discIgstAmt = discIgstAmt;
	}

	public String getNetTaxableAmt() {
		return netTaxableAmt;
	}

	public void setNetTaxableAmt(String netTaxableAmt) {
		this.netTaxableAmt = netTaxableAmt;
	}

	public String getNetCgstAmt() {
		return netCgstAmt;
	}

	public void setNetCgstAmt(String netCgstAmt) {
		this.netCgstAmt = netCgstAmt;
	}

	public String getNetSgstAmt() {
		return netSgstAmt;
	}

	public void setNetSgstAmt(String netSgstAmt) {
		this.netSgstAmt = netSgstAmt;
	}

	public String getNetIgstAmt() {
		return netIgstAmt;
	}

	public void setNetIgstAmt(String netIgstAmt) {
		this.netIgstAmt = netIgstAmt;
	}

	public String getNetTaxAmtInWord() {
		return netTaxAmtInWord;
	}

	public void setNetTaxAmtInWord(String netTaxAmtInWord) {
		this.netTaxAmtInWord = netTaxAmtInWord;
	}

	@Override
	public String toString() {
		return "InventoryAddedInvoiceModel [companyName=" + companyName + ", companyAddress=" + companyAddress
				+ ", coTelNo=" + coTelNo + ", gstInNo=" + gstInNo + ", emailId=" + emailId + ", billNo=" + billNo
				+ ", billDate=" + billDate + ", supplierName=" + supplierName + ", supplierAddress=" + supplierAddress
				+ ", StateCode=" + StateCode + ", mobNoAndTelNo=" + mobNoAndTelNo + ", supplierGstInNo="
				+ supplierGstInNo + ", inventoryTsnNo=" + inventoryTsnNo + ", deliveryDate=" + deliveryDate
				+ ", productList=" + productList + ", totalQuantity=" + totalQuantity + ", totalAmountBeforeDiscount="
				+ totalAmountBeforeDiscount + ", discountPer=" + discountPer + ", discountAmt=" + discountAmt
				+ ", discountType=" + discountType + ", totalAmountAfterDiscount=" + totalAmountAfterDiscount
				+ ", roundOff=" + roundOff + ", totalAmountAfterRoundOff=" + totalAmountAfterRoundOff
				+ ", totalAmountAfterRoundOffInWord=" + totalAmountAfterRoundOffInWord + ", taxList=" + taxList
				+ ", totalTaxableAmt=" + totalTaxableAmt + ", totalCgstAmt=" + totalCgstAmt + ", totalSgstAmt="
				+ totalSgstAmt + ", totalIgstAmt=" + totalIgstAmt + ", discTaxableAmt=" + discTaxableAmt
				+ ", discCgstAmt=" + discCgstAmt + ", discSgstAmt=" + discSgstAmt + ", discIgstAmt=" + discIgstAmt
				+ ", netTaxableAmt=" + netTaxableAmt + ", netCgstAmt=" + netCgstAmt + ", netSgstAmt=" + netSgstAmt
				+ ", netIgstAmt=" + netIgstAmt + ", netTaxAmtInWord=" + netTaxAmtInWord + "]";
	}

}
