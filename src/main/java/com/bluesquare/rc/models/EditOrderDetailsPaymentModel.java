package com.bluesquare.rc.models;

public class EditOrderDetailsPaymentModel {

	private String customerShopName;
	private String orderId;
	private long paymentId;
	private String mobileNumber;
	private String address;
	private double totalAmount;
	private double totalAmountPaid;
	private double balanceAmount;
	private String fullPartialStatus;
	private String cashChequeStatus;
	private String bankName;
	private double amountPaid;
	private double amountBalance;
	private String chequeNumber;
	private String chequeDate;
	private String dueDate;
	private long paymentMethodId;
	private String transactionRefNo;
	private String comment;
	private String payType;

	public EditOrderDetailsPaymentModel(String customerShopName, String orderId, long paymentId, String mobileNumber,
			String address, double totalAmount, double totalAmountPaid, double balanceAmount, String fullPartialStatus,
			String cashChequeStatus, String bankName, double amountPaid, double amountBalance, String chequeNumber,
			String chequeDate, String dueDate, long paymentMethodId, String transactionRefNo, String comment,
			String payType) {
		super();
		this.customerShopName = customerShopName;
		this.orderId = orderId;
		this.paymentId = paymentId;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.totalAmount = totalAmount;
		this.totalAmountPaid = totalAmountPaid;
		this.balanceAmount = balanceAmount;
		this.fullPartialStatus = fullPartialStatus;
		this.cashChequeStatus = cashChequeStatus;
		this.bankName = bankName;
		this.amountPaid = amountPaid;
		this.amountBalance = amountBalance;
		this.chequeNumber = chequeNumber;
		this.chequeDate = chequeDate;
		this.dueDate = dueDate;
		this.paymentMethodId = paymentMethodId;
		this.transactionRefNo = transactionRefNo;
		this.comment = comment;
		this.payType = payType;
	}

	public String getCustomerShopName() {
		return customerShopName;
	}

	public void setCustomerShopName(String customerShopName) {
		this.customerShopName = customerShopName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getFullPartialStatus() {
		return fullPartialStatus;
	}

	public void setFullPartialStatus(String fullPartialStatus) {
		this.fullPartialStatus = fullPartialStatus;
	}

	public String getCashChequeStatus() {
		return cashChequeStatus;
	}

	public void setCashChequeStatus(String cashChequeStatus) {
		this.cashChequeStatus = cashChequeStatus;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Override
	public String toString() {
		return "EditOrderDetailsPaymentModel [customerShopName=" + customerShopName + ", orderId=" + orderId
				+ ", paymentId=" + paymentId + ", mobileNumber=" + mobileNumber + ", address=" + address
				+ ", totalAmount=" + totalAmount + ", totalAmountPaid=" + totalAmountPaid + ", balanceAmount="
				+ balanceAmount + ", fullPartialStatus=" + fullPartialStatus + ", cashChequeStatus=" + cashChequeStatus
				+ ", bankName=" + bankName + ", amountPaid=" + amountPaid + ", amountBalance=" + amountBalance
				+ ", chequeNumber=" + chequeNumber + ", chequeDate=" + chequeDate + ", dueDate=" + dueDate
				+ ", paymentMethodId=" + paymentMethodId + ", transactionRefNo=" + transactionRefNo + ", comment="
				+ comment + ", payType=" + payType + "]";
	}

}
