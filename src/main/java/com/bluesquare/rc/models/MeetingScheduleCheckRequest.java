package com.bluesquare.rc.models;

public class MeetingScheduleCheckRequest {
	long meetingId;
	long employeeId;
	String date;
	String fromTime;
	String toTime;
	public long getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(long meetingId) {
		this.meetingId = meetingId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	@Override
	public String toString() {
		return "MeetingScheduleCheckRequest [meetingId=" + meetingId + ", employeeId=" + employeeId + ", date=" + date
				+ ", fromTime=" + fromTime + ", toTime=" + toTime + "]";
	}
	
	
}
