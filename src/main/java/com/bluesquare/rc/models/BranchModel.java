package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.Company;

public class BranchModel {
	private long branchId;

	private String name;

	private Company company;

	private double balance;
	
	public BranchModel() {
		// TODO Auto-generated constructor stub
	}

	public BranchModel(long branchId, String name, Company company, double balance) {
		super();
		this.branchId = branchId;
		this.name = name;
		this.company = company;
		this.balance = balance;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Branch [branchId=" + branchId + ", name=" + name + ", company=" + company + ", balance=" + balance
				+ "]";
	}

}
