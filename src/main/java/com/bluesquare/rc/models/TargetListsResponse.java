package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.rest.models.BaseDomain;

public class TargetListsResponse extends BaseDomain {

	public List<TargetAssignDetails> targetLists;

	public List<TargetAssignDetails> getTargetLists() {
		return targetLists;
	}

	public void setTargetLists(List<TargetAssignDetails> targetLists) {
		this.targetLists = targetLists;
	}

	@Override
	public String toString() {
		return "TargetListsResponse [targetLists=" + targetLists + "]";
	}

}
