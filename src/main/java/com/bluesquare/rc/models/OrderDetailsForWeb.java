/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderStatus;

/**
 * @author aNKIT
 *
 */
public class OrderDetailsForWeb {

	private String orderId;

	private double totalAmount;

	private double totalAmountWithTax;
	private long totalQuantity;

	private long employeeDetailsId;
	private String employeeName;

	private String shopName;
	private String areaName;

	private long paymentPeriodDays;

	private OrderStatus orderStatus;

	private boolean payStatus;

	private double issuedTotalAmount;

	private double issuedTotalAmountWithTax;

	private long issuedTotalQuantity;

	private Date orderDetailsPaymentTakeDatetime;

	private Date orderDetailsAddedDatetime;

	private Date cancelDate;

	private Date issueDate;

	private Date packedDate;
	private Date deliveryDate;

	private Date confirmDate;

	private double confirmTotalAmount;

	private double confirmTotalAmountWithTax;

	private long confirmTotalQuantity;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getPaymentPeriodDays() {
		return paymentPeriodDays;
	}

	public void setPaymentPeriodDays(long paymentPeriodDays) {
		this.paymentPeriodDays = paymentPeriodDays;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public double getIssuedTotalAmount() {
		return issuedTotalAmount;
	}

	public void setIssuedTotalAmount(double issuedTotalAmount) {
		this.issuedTotalAmount = issuedTotalAmount;
	}

	public double getIssuedTotalAmountWithTax() {
		return issuedTotalAmountWithTax;
	}

	public void setIssuedTotalAmountWithTax(double issuedTotalAmountWithTax) {
		this.issuedTotalAmountWithTax = issuedTotalAmountWithTax;
	}

	public long getIssuedTotalQuantity() {
		return issuedTotalQuantity;
	}

	public void setIssuedTotalQuantity(long issuedTotalQuantity) {
		this.issuedTotalQuantity = issuedTotalQuantity;
	}

	public Date getOrderDetailsPaymentTakeDatetime() {
		return orderDetailsPaymentTakeDatetime;
	}

	public void setOrderDetailsPaymentTakeDatetime(Date orderDetailsPaymentTakeDatetime) {
		this.orderDetailsPaymentTakeDatetime = orderDetailsPaymentTakeDatetime;
	}

	public Date getOrderDetailsAddedDatetime() {
		return orderDetailsAddedDatetime;
	}

	public void setOrderDetailsAddedDatetime(Date orderDetailsAddedDatetime) {
		this.orderDetailsAddedDatetime = orderDetailsAddedDatetime;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getPackedDate() {
		return packedDate;
	}

	public void setPackedDate(Date packedDate) {
		this.packedDate = packedDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public double getConfirmTotalAmount() {
		return confirmTotalAmount;
	}

	public void setConfirmTotalAmount(double confirmTotalAmount) {
		this.confirmTotalAmount = confirmTotalAmount;
	}

	public double getConfirmTotalAmountWithTax() {
		return confirmTotalAmountWithTax;
	}

	public void setConfirmTotalAmountWithTax(double confirmTotalAmountWithTax) {
		this.confirmTotalAmountWithTax = confirmTotalAmountWithTax;
	}

	public long getConfirmTotalQuantity() {
		return confirmTotalQuantity;
	}

	public void setConfirmTotalQuantity(long confirmTotalQuantity) {
		this.confirmTotalQuantity = confirmTotalQuantity;
	}

	@Override
	public String toString() {
		return "OrderDetailsForWeb [orderId=" + orderId + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", employeeDetailsId=" + employeeDetailsId
				+ ", employeeName=" + employeeName + ", shopName=" + shopName + ", areaName=" + areaName
				+ ", paymentPeriodDays=" + paymentPeriodDays + ", orderStatus=" + orderStatus + ", payStatus="
				+ payStatus + ", issuedTotalAmount=" + issuedTotalAmount + ", issuedTotalAmountWithTax="
				+ issuedTotalAmountWithTax + ", issuedTotalQuantity=" + issuedTotalQuantity
				+ ", orderDetailsPaymentTakeDatetime=" + orderDetailsPaymentTakeDatetime
				+ ", orderDetailsAddedDatetime=" + orderDetailsAddedDatetime + ", cancelDate=" + cancelDate
				+ ", issueDate=" + issueDate + ", packedDate=" + packedDate + ", deliveryDate=" + deliveryDate
				+ ", confirmDate=" + confirmDate + ", confirmTotalAmount=" + confirmTotalAmount
				+ ", confirmTotalAmountWithTax=" + confirmTotalAmountWithTax + ", confirmTotalQuantity="
				+ confirmTotalQuantity + "]";
	}



}
