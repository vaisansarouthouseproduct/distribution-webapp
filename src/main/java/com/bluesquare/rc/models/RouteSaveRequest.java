package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.RoutePoints;

public class RouteSaveRequest {

	public Route route;

	public List<RoutePoints> routePointsList;

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public List<RoutePoints> getRoutePointsList() {
		return routePointsList;
	}

	public void setRoutePointsList(List<RoutePoints> routePointsList) {
		this.routePointsList = routePointsList;
	}

	@Override
	public String toString() {
		return "RouteSaveRequest [route=" + route + ", routePointsList=" + routePointsList + ", getRoute()="
				+ getRoute() + ", getRoutePointsList()=" + getRoutePointsList() + "]";
	}
	
	
}
