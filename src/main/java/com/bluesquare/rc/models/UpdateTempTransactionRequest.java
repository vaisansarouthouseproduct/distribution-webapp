package com.bluesquare.rc.models;

public class UpdateTempTransactionRequest {

	public String id;
	
	public String updatedStatus;
	
	public String reason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUpdatedStatus() {
		return updatedStatus;
	}

	public void setUpdatedStatus(String updatedStatus) {
		this.updatedStatus = updatedStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "UpdateTempTransactionRequest [id=" + id + ", updatedStatus=" + updatedStatus + ", reason=" + reason
				+ "]";
	}

	
}
