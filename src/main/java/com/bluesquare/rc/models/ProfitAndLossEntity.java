package com.bluesquare.rc.models;
public class ProfitAndLossEntity {
	
	public String name;
	
	public double amount;

	

	public ProfitAndLossEntity(String name, double amount) {
		super();
		this.name = name;
		this.amount = amount;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	@Override
	public String toString() {
		return "ProfitAndLossEntity [name=" + name + ", amount=" + amount + "]";
	}
	

	
	

}
