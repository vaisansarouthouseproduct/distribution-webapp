/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

/**
 * @author aNKIT
 *
 */
public class GstBillReport {

	private List<SaleTable> saleTableList;
	private List<PurchaseTable> purchaseTableList;
	
	private List<SaleAndPurchaseTableMerge> saleMergeTableList;
	private List<SaleAndPurchaseTableMerge> purchaseMergeTableList;
	
	private double saleTotalTaxableAmt;
	private double saleCgstAmt;
	private double saleSgstAmt;
	private double saleIgstAmt;
	private double saleFinalAmt;
	
	private double purchaseTotalTaxableAmt;
	private double purchaseCgstAmt;
	private double purchaseSgstAmt;
	private double purchaseIgstAmt;
	private double purchaseFinalAmt;
	public GstBillReport(List<SaleTable> saleTableList, List<PurchaseTable> purchaseTableList,
			List<SaleAndPurchaseTableMerge> saleMergeTableList, List<SaleAndPurchaseTableMerge> purchaseMergeTableList,
			double saleTotalTaxableAmt, double saleCgstAmt, double saleSgstAmt, double saleIgstAmt, double saleFinalAmt,
			double purchaseTotalTaxableAmt, double purchaseCgstAmt, double purchaseSgstAmt, double purchaseIgstAmt,
			double purchaseFinalAmt) {
		super();
		this.saleTableList = saleTableList;
		this.purchaseTableList = purchaseTableList;
		this.saleMergeTableList = saleMergeTableList;
		this.purchaseMergeTableList = purchaseMergeTableList;
		this.saleTotalTaxableAmt = saleTotalTaxableAmt;
		this.saleCgstAmt = saleCgstAmt;
		this.saleSgstAmt = saleSgstAmt;
		this.saleIgstAmt = saleIgstAmt;
		this.saleFinalAmt = saleFinalAmt;
		this.purchaseTotalTaxableAmt = purchaseTotalTaxableAmt;
		this.purchaseCgstAmt = purchaseCgstAmt;
		this.purchaseSgstAmt = purchaseSgstAmt;
		this.purchaseIgstAmt = purchaseIgstAmt;
		this.purchaseFinalAmt = purchaseFinalAmt;
	}
	public List<SaleTable> getSaleTableList() {
		return saleTableList;
	}
	public void setSaleTableList(List<SaleTable> saleTableList) {
		this.saleTableList = saleTableList;
	}
	public List<PurchaseTable> getPurchaseTableList() {
		return purchaseTableList;
	}
	public void setPurchaseTableList(List<PurchaseTable> purchaseTableList) {
		this.purchaseTableList = purchaseTableList;
	}
	public List<SaleAndPurchaseTableMerge> getSaleMergeTableList() {
		return saleMergeTableList;
	}
	public void setSaleMergeTableList(List<SaleAndPurchaseTableMerge> saleMergeTableList) {
		this.saleMergeTableList = saleMergeTableList;
	}
	public List<SaleAndPurchaseTableMerge> getPurchaseMergeTableList() {
		return purchaseMergeTableList;
	}
	public void setPurchaseMergeTableList(List<SaleAndPurchaseTableMerge> purchaseMergeTableList) {
		this.purchaseMergeTableList = purchaseMergeTableList;
	}
	public double getSaleTotalTaxableAmt() {
		return saleTotalTaxableAmt;
	}
	public void setSaleTotalTaxableAmt(double saleTotalTaxableAmt) {
		this.saleTotalTaxableAmt = saleTotalTaxableAmt;
	}
	public double getSaleCgstAmt() {
		return saleCgstAmt;
	}
	public void setSaleCgstAmt(double saleCgstAmt) {
		this.saleCgstAmt = saleCgstAmt;
	}
	public double getSaleSgstAmt() {
		return saleSgstAmt;
	}
	public void setSaleSgstAmt(double saleSgstAmt) {
		this.saleSgstAmt = saleSgstAmt;
	}
	public double getSaleIgstAmt() {
		return saleIgstAmt;
	}
	public void setSaleIgstAmt(double saleIgstAmt) {
		this.saleIgstAmt = saleIgstAmt;
	}
	public double getSaleFinalAmt() {
		return saleFinalAmt;
	}
	public void setSaleFinalAmt(double saleFinalAmt) {
		this.saleFinalAmt = saleFinalAmt;
	}
	public double getPurchaseTotalTaxableAmt() {
		return purchaseTotalTaxableAmt;
	}
	public void setPurchaseTotalTaxableAmt(double purchaseTotalTaxableAmt) {
		this.purchaseTotalTaxableAmt = purchaseTotalTaxableAmt;
	}
	public double getPurchaseCgstAmt() {
		return purchaseCgstAmt;
	}
	public void setPurchaseCgstAmt(double purchaseCgstAmt) {
		this.purchaseCgstAmt = purchaseCgstAmt;
	}
	public double getPurchaseSgstAmt() {
		return purchaseSgstAmt;
	}
	public void setPurchaseSgstAmt(double purchaseSgstAmt) {
		this.purchaseSgstAmt = purchaseSgstAmt;
	}
	public double getPurchaseIgstAmt() {
		return purchaseIgstAmt;
	}
	public void setPurchaseIgstAmt(double purchaseIgstAmt) {
		this.purchaseIgstAmt = purchaseIgstAmt;
	}
	public double getPurchaseFinalAmt() {
		return purchaseFinalAmt;
	}
	public void setPurchaseFinalAmt(double purchaseFinalAmt) {
		this.purchaseFinalAmt = purchaseFinalAmt;
	}
	@Override
	public String toString() {
		return "GstBillReport [saleTableList=" + saleTableList + ", purchaseTableList=" + purchaseTableList
				+ ", saleMergeTableList=" + saleMergeTableList + ", purchaseMergeTableList=" + purchaseMergeTableList
				+ ", saleTotalTaxableAmt=" + saleTotalTaxableAmt + ", saleCgstAmt=" + saleCgstAmt + ", saleSgstAmt="
				+ saleSgstAmt + ", saleIgstAmt=" + saleIgstAmt + ", saleFinalAmt=" + saleFinalAmt
				+ ", purchaseTotalTaxableAmt=" + purchaseTotalTaxableAmt + ", purchaseCgstAmt=" + purchaseCgstAmt
				+ ", purchaseSgstAmt=" + purchaseSgstAmt + ", purchaseIgstAmt=" + purchaseIgstAmt
				+ ", purchaseFinalAmt=" + purchaseFinalAmt + "]";
	}
	
	
}
