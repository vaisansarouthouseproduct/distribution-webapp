package com.bluesquare.rc.models;

public class GetTransportationInfoRequest {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "GetTransportationInfoRequest [id=" + id + "]";
	}
	
	
}
