package com.bluesquare.rc.models;

public class UpdateTransportForAccountingRequest {
	private String invoiceNumber;
	
	private String transportName;
	
	private String gstNo;
	
	private String mobNo;
	
	private String vehicleNo;
	
	private String docketNo;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getTransportName() {
		return transportName;
	}

	public void setTransportName(String transportName) {
		this.transportName = transportName;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	@Override
	public String toString() {
		return "UpdateTransportForAccountingRequest [invoiceNumber=" + invoiceNumber + ", transportName="
				+ transportName + ", gstNo=" + gstNo + ", mobNo=" + mobNo + ", vehicleNo=" + vehicleNo + ", docketNo="
				+ docketNo + "]";
	}
	
	
	
}
