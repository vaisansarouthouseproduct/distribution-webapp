package com.bluesquare.rc.models;

import java.util.Date;

public class ExpenseListModel {

	private String expenseId;	
	private String description;
	private String reference;
	private double amount;
	private String type;
	private Date addDate;
	private Date updateDate;
	private String userName;
	public ExpenseListModel(String expenseId, String description, String reference, double amount, String type,
			Date addDate, Date updateDate, String userName) {
		super();
		this.expenseId = expenseId;
		this.description = description;
		this.reference = reference;
		this.amount = amount;
		this.type = type;
		this.addDate = addDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}
	public String getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "ExpenseListModel [expenseId=" + expenseId + ", description=" + description + ", reference=" + reference
				+ ", amount=" + amount + ", type=" + type + ", addDate=" + addDate + ", updateDate=" + updateDate
				+ ", userName=" + userName + "]";
	}
	
}
