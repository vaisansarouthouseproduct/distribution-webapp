package com.bluesquare.rc.models;

public class ProformaOrderReport {

	private int srno;
	private long id;
	private String proformaOrderId;
	private String shopName;
	private double totalAmount;
	private double totalAmountWithTax;
	private long totalQuantity;
	private String orderTakenDate;
	private String orderTakenTime;
	private String orderComment;

	public int getSrno() {
		return srno;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProformaOrderId() {
		return proformaOrderId;
	}

	public void setProformaOrderId(String proformaOrderId) {
		this.proformaOrderId = proformaOrderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getOrderTakenDate() {
		return orderTakenDate;
	}

	public void setOrderTakenDate(String orderTakenDate) {
		this.orderTakenDate = orderTakenDate;
	}

	public String getOrderTakenTime() {
		return orderTakenTime;
	}

	public void setOrderTakenTime(String orderTakenTime) {
		this.orderTakenTime = orderTakenTime;
	}

	public String getOrderComment() {
		return orderComment;
	}

	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}

	public ProformaOrderReport(int srno, long id, String proformaOrderId, String shopName, double totalAmount,
			double totalAmountWithTax, long totalQuantity, String orderTakenDate, String orderTakenTime,
			String orderComment) {
		super();
		this.srno = srno;
		this.id = id;
		this.proformaOrderId = proformaOrderId;
		this.shopName = shopName;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalQuantity = totalQuantity;
		this.orderTakenDate = orderTakenDate;
		this.orderTakenTime = orderTakenTime;
		this.orderComment = orderComment;
	}

	@Override
	public String toString() {
		return "ProformaOrderReport [srno=" + srno + ", id=" + id + ", proformaOrderId=" + proformaOrderId
				+ ", shopName=" + shopName + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", orderTakenDate=" + orderTakenDate
				+ ", orderTakenTime=" + orderTakenTime + ", orderComment=" + orderComment + "]";
	}

}
