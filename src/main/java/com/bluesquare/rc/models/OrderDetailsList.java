package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.rest.models.BaseDomain;

public class OrderDetailsList extends BaseDomain {

	private List<OrderDetailsForWeb> orderDetailsList;
	private List<AreaModel> areaList;
	private String pageName;
	private String salesManName;

	public List<OrderDetailsForWeb> getOrderDetailsList() {
		return orderDetailsList;
	}

	public void setOrderDetailsList(List<OrderDetailsForWeb> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}

	public List<AreaModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getSalesManName() {
		return salesManName;
	}

	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}

	@Override
	public String toString() {
		return "OrderDetailsList [orderDetailsList=" + orderDetailsList + ", areaList=" + areaList + ", pageName="
				+ pageName + ", salesManName=" + salesManName + "]";
	}

}
