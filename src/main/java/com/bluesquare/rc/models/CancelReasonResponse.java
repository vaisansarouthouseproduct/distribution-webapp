package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.CancelReason;
import com.bluesquare.rc.rest.models.BaseDomain;

public class CancelReasonResponse extends BaseDomain {
	
	private List<CancelReason> cancelReasons;

	public List<CancelReason> getCancelReasons() {
		return cancelReasons;
	}

	public void setCancelReasons(List<CancelReason> cancelReasons) {
		this.cancelReasons = cancelReasons;
	}

	@Override
	public String toString() {
		return "CancelReasonResponse [cancelReasons=" + cancelReasons + "]";
	}
	

	
}
