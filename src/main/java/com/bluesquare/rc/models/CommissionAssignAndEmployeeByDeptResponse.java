package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.rest.models.BaseDomain;

public class CommissionAssignAndEmployeeByDeptResponse extends BaseDomain {
	public List<CommissionAssign> commissionAssignList;
	public List<EmployeeDetails> employeeDetailsList;

	public CommissionAssignAndEmployeeByDeptResponse(List<CommissionAssign> commissionAssignList,
			List<EmployeeDetails> employeeDetailsList) {
		super();
		this.commissionAssignList = commissionAssignList;
		this.employeeDetailsList = employeeDetailsList;
	}

	public List<CommissionAssign> getCommissionAssignList() {
		return commissionAssignList;
	}

	public void setCommissionAssignList(List<CommissionAssign> commissionAssignList) {
		this.commissionAssignList = commissionAssignList;
	}

	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}

	@Override
	public String toString() {
		return "CommissionAssignAndEmployeeByDeptResponse [commissionAssignList=" + commissionAssignList
				+ ", employeeDetailsList=" + employeeDetailsList + "]";
	}

}
