package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.OrderDetails;

public class PaymentListReport {
	public long paymentId;

	public String businessNameId;

	public String orderId;

	public double totalAmountWithTax;

	public String employeeName;

	public double totalAmount;

	public double dueAmount;

	public Date dueDate;

	public Date lastDueDate;

	public double paidAmount;

	public Date paidDate;

	public String payType;

	public String chequeNumber;

	public String bankName;

	public Date chequeDate;

	public boolean status;

	public boolean chequeClearStatus;

	public String paymentMethodName;
	public String transactionRefNo;
	public String comment;

	public PaymentListReport(long paymentId, String businessNameId, String orderId, double totalAmountWithTax,
			String employeeName, double totalAmount, double dueAmount, Date dueDate, Date lastDueDate,
			double paidAmount, Date paidDate, String payType, String chequeNumber, String bankName, Date chequeDate,
			boolean status, boolean chequeClearStatus, String paymentMethodName, String transactionRefNo,
			String comment) {
		super();
		this.paymentId = paymentId;
		this.businessNameId = businessNameId;
		this.orderId = orderId;
		this.totalAmountWithTax = totalAmountWithTax;
		this.employeeName = employeeName;
		this.totalAmount = totalAmount;
		this.dueAmount = dueAmount;
		this.dueDate = dueDate;
		this.lastDueDate = lastDueDate;
		this.paidAmount = paidAmount;
		this.paidDate = paidDate;
		this.payType = payType;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.chequeDate = chequeDate;
		this.status = status;
		this.chequeClearStatus = chequeClearStatus;
		this.paymentMethodName = paymentMethodName;
		this.transactionRefNo = transactionRefNo;
		this.comment = comment;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isChequeClearStatus() {
		return chequeClearStatus;
	}

	public void setChequeClearStatus(boolean chequeClearStatus) {
		this.chequeClearStatus = chequeClearStatus;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "PaymentListReport [paymentId=" + paymentId + ", businessNameId=" + businessNameId + ", orderId="
				+ orderId + ", totalAmountWithTax=" + totalAmountWithTax + ", employeeName=" + employeeName
				+ ", totalAmount=" + totalAmount + ", dueAmount=" + dueAmount + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", paidAmount=" + paidAmount + ", paidDate=" + paidDate
				+ ", payType=" + payType + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName + ", chequeDate="
				+ chequeDate + ", status=" + status + ", chequeClearStatus=" + chequeClearStatus
				+ ", paymentMethodName=" + paymentMethodName + ", transactionRefNo=" + transactionRefNo + ", comment="
				+ comment + "]";
	}

}
