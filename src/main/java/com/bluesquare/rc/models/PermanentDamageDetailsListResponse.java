package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.rest.models.BaseDomain;


public class PermanentDamageDetailsListResponse extends BaseDomain {

	List<PermanentDamageDetailsList> permanentDamageDetailsList;


	public List<PermanentDamageDetailsList> getPermanentDamageDetailsList() {
		return permanentDamageDetailsList;
	}

	public void setPermanentDamageDetailsList(List<PermanentDamageDetailsList> permanentDamageDetailsList) {
		this.permanentDamageDetailsList = permanentDamageDetailsList;
	}

	@Override
	public String toString() {
		return "PermanentDamageDetailsListResponse [permanentDamageDetailsList=" + permanentDamageDetailsList + "]";
	}

}
