package com.bluesquare.rc.models;

import java.util.List;

public class EmployeeCommissionModel {

	long srno;
	String name;
	String departmentName;
	List<EmployeeCommissionDetailsModel> employeeCommissionDetailsModelList;
	double totalCommissionValue;

	public EmployeeCommissionModel(long srno, String name, String departmentName,
			List<EmployeeCommissionDetailsModel> employeeCommissionDetailsModelList, double totalCommissionValue) {
		super();
		this.srno = srno;
		this.name = name;
		this.departmentName = departmentName;
		this.employeeCommissionDetailsModelList = employeeCommissionDetailsModelList;
		this.totalCommissionValue = totalCommissionValue;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<EmployeeCommissionDetailsModel> getEmployeeCommissionDetailsModelList() {
		return employeeCommissionDetailsModelList;
	}

	public void setEmployeeCommissionDetailsModelList(
			List<EmployeeCommissionDetailsModel> employeeCommissionDetailsModelList) {
		this.employeeCommissionDetailsModelList = employeeCommissionDetailsModelList;
	}

	public double getTotalCommissionValue() {
		return totalCommissionValue;
	}

	public void setTotalCommissionValue(double totalCommissionValue) {
		this.totalCommissionValue = totalCommissionValue;
	}

	@Override
	public String toString() {
		return "EmployeeCommissionModel [srno=" + srno + ", name=" + name + ", departmentName=" + departmentName
				+ ", employeeCommissionDetailsModelList=" + employeeCommissionDetailsModelList
				+ ", totalCommissionValue=" + totalCommissionValue + "]";
	}

}
