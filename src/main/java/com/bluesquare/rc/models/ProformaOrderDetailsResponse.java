package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.ProformaOrder;
import com.bluesquare.rc.entities.ProformaOrderProductDetails;
import com.bluesquare.rc.rest.models.BaseDomain;



public class ProformaOrderDetailsResponse extends BaseDomain {

	public ProformaOrder proformaOrder;
	public List<ProformaOrderProductDetails> productDetailsList;

	public ProformaOrder getProformaOrder() {
		return proformaOrder;
	}

	public void setProformaOrder(ProformaOrder proformaOrder) {
		this.proformaOrder = proformaOrder;
	}

	public List<ProformaOrderProductDetails> getProductDetailsList() {
		return productDetailsList;
	}

	public void setProductDetailsList(List<ProformaOrderProductDetails> productDetailsList) {
		this.productDetailsList = productDetailsList;
	}


	@Override
	public String toString() {
		return "ProformaOrderDetailsResponse [proformaOrder=" + proformaOrder + ", productDetailsList="
				+ productDetailsList + "]";
	}

}
