package com.bluesquare.rc.models;

import java.util.Date;

public class PaymentDoInfo {

	private long pkId;
	private String id;
	private String name;
	private String inventoryId;
	private double amountPaid;
	private double amountUnPaid;
	private double totalAmount;
	private String type;
	private String url;

	// for edit
	private long paymentId;
	private double paidAmount;
	private String bankName;
	private String checkNumber;
	private String payType;
	private String checkDate;
	private String comment;
	private long paymentMethodId;
	private String transactionRefNo;

	public PaymentDoInfo() {
		// TODO Auto-generated constructor stub
	}

	public PaymentDoInfo(long pkId, String id, String name, String inventoryId, double amountPaid, double amountUnPaid,
			double totalAmount, String type, String url) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.inventoryId = inventoryId;
		this.amountPaid = amountPaid;
		this.amountUnPaid = amountUnPaid;
		this.totalAmount = totalAmount;
		this.type = type;
		this.url = url;
	}

	public long getPkId() {
		return pkId;
	}

	public void setPkId(long pkId) {
		this.pkId = pkId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public double getAmountUnPaid() {
		return amountUnPaid;
	}

	public void setAmountUnPaid(double amountUnPaid) {
		this.amountUnPaid = amountUnPaid;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String chekNumber) {
		this.checkNumber = chekNumber;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	@Override
	public String toString() {
		return "PaymentDoInfo [pkId=" + pkId + ", id=" + id + ", name=" + name + ", inventoryId=" + inventoryId
				+ ", amountPaid=" + amountPaid + ", amountUnPaid=" + amountUnPaid + ", totalAmount=" + totalAmount
				+ ", type=" + type + ", url=" + url + ", paymentId=" + paymentId + ", paidAmount=" + paidAmount
				+ ", bankName=" + bankName + ", checkNumber=" + checkNumber + ", payType=" + payType + ", checkDate="
				+ checkDate + ", comment=" + comment + ", paymentMethodId=" + paymentMethodId + ", transactionRefNo="
				+ transactionRefNo + "]";
	}

}
