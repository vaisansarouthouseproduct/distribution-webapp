/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.responseEntities.EmployeeIncentivesModel;

/**
 * @author aNKIT
 *
 */
public class EmployeeIncentiveModel {

	private String employeeName;
	private boolean status;
	private List<EmployeeIncentivesModel> employeeIncentiveList;

	public EmployeeIncentiveModel(String employeeName, boolean status,
			List<EmployeeIncentivesModel> employeeIncentiveList) {
		super();
		this.employeeName = employeeName;
		this.status = status;
		this.employeeIncentiveList = employeeIncentiveList;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<EmployeeIncentivesModel> getEmployeeIncentiveList() {
		return employeeIncentiveList;
	}

	public void setEmployeeIncentiveList(List<EmployeeIncentivesModel> employeeIncentiveList) {
		this.employeeIncentiveList = employeeIncentiveList;
	}

	@Override
	public String toString() {
		return "EmployeeIncentiveModel [employeeName=" + employeeName + ", status=" + status
				+ ", employeeIncentiveList=" + employeeIncentiveList + "]";
	}

}
