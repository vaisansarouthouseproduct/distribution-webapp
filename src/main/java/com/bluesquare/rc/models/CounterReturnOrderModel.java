package com.bluesquare.rc.models;

import java.util.Date;

public class CounterReturnOrderModel {

	private long srno;
	private long returnCounterOrderId;
	private String counterOrderId;
	private String gateKeeperName;
	private String customerName;
	private String returnCounterOrderGenId;
	private long totalQuantity;
	private double totalAmount;
	private double totalAmountWithTax;
	private Date returnDate;

	public CounterReturnOrderModel(long srno, long returnCounterOrderId, String counterOrderId, String gateKeeperName,
			String customerName, String returnCounterOrderGenId, long totalQuantity, double totalAmount,
			double totalAmountWithTax, Date returnDate) {
		super();
		this.srno = srno;
		this.returnCounterOrderId = returnCounterOrderId;
		this.counterOrderId = counterOrderId;
		this.gateKeeperName = gateKeeperName;
		this.customerName = customerName;
		this.returnCounterOrderGenId = returnCounterOrderGenId;
		this.totalQuantity = totalQuantity;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.returnDate = returnDate;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getReturnCounterOrderId() {
		return returnCounterOrderId;
	}

	public void setReturnCounterOrderId(long returnCounterOrderId) {
		this.returnCounterOrderId = returnCounterOrderId;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public String getGateKeeperName() {
		return gateKeeperName;
	}

	public void setGateKeeperName(String gateKeeperName) {
		this.gateKeeperName = gateKeeperName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getReturnCounterOrderGenId() {
		return returnCounterOrderGenId;
	}

	public void setReturnCounterOrderGenId(String returnCounterOrderGenId) {
		this.returnCounterOrderGenId = returnCounterOrderGenId;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	@Override
	public String toString() {
		return "CounterReturnOrderModel [srno=" + srno + ", returnCounterOrderId=" + returnCounterOrderId
				+ ", counterOrderId=" + counterOrderId + ", gateKeeperName=" + gateKeeperName + ", customerName="
				+ customerName + ", returnCounterOrderGenId=" + returnCounterOrderGenId + ", totalQuantity="
				+ totalQuantity + ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax
				+ ", returnDate=" + returnDate + "]";
	}

}
