/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.InventoryDetails;

/**
 * @author aNKIT
 *
 */
public class PurchaseTable {
	private long srNo;
	private String date;
	private InventoryDetails inventoryDetails;
	private double taxableAmount;
	private double cgstAmt;
	private double sgstAmt;
	private double igstAmt;
	private double totalAmount;
	public PurchaseTable(long srNo, String date, InventoryDetails inventoryDetails, double taxableAmount,
			double cgstAmt, double sgstAmt, double igstAmt, double totalAmount) {
		super();
		this.srNo = srNo;
		this.date = date;
		this.inventoryDetails = inventoryDetails;
		this.taxableAmount = taxableAmount;
		this.cgstAmt = cgstAmt;
		this.sgstAmt = sgstAmt;
		this.igstAmt = igstAmt;
		this.totalAmount = totalAmount;
	}
	public long getSrNo() {
		return srNo;
	}
	public void setSrNo(long srNo) {
		this.srNo = srNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public InventoryDetails getInventoryDetails() {
		return inventoryDetails;
	}
	public void setInventoryDetails(InventoryDetails inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}
	public double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public double getCgstAmt() {
		return cgstAmt;
	}
	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}
	public double getSgstAmt() {
		return sgstAmt;
	}
	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}
	public double getIgstAmt() {
		return igstAmt;
	}
	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "PurchaseTable [srNo=" + srNo + ", date=" + date + ", inventoryDetails=" + inventoryDetails
				+ ", taxableAmount=" + taxableAmount + ", cgstAmt=" + cgstAmt + ", sgstAmt=" + sgstAmt + ", igstAmt="
				+ igstAmt + ", totalAmount=" + totalAmount + "]";
	}


}
