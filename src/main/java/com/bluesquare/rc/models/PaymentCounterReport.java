package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.CounterOrder;

public class PaymentCounterReport {

	public long paymentCounterId;

	public double currentAmountPaid;

	public double currentAmountRefund;

	public Date paidDate;

	public Date dueDate;

	public Date lastDueDate;

	public String payType;

	public String chequeNumber;

	public String bankName;

	public Date chequeDate;

	public boolean isBusinessHave;

	public String businessNameId;

	public String customerName;

	public String counterOrderId;

	public double totalAmountWithTax;

	public String employeeName;

	public boolean status;

	public boolean chequeClearStatus;

	public String transactionRefNo;
	public String comment;
	public String paymentMethodName;

	public PaymentCounterReport(long paymentCounterId, double currentAmountPaid, double currentAmountRefund,
			Date paidDate, Date dueDate, Date lastDueDate, String payType, String chequeNumber, String bankName,
			Date chequeDate, boolean isBusinessHave, String businessNameId, String customerName, String counterOrderId,
			double totalAmountWithTax, String employeeName, boolean status, boolean chequeClearStatus,
			String transactionRefNo, String comment, String paymentMethodName) {
		super();
		this.paymentCounterId = paymentCounterId;
		this.currentAmountPaid = currentAmountPaid;
		this.currentAmountRefund = currentAmountRefund;
		this.paidDate = paidDate;
		this.dueDate = dueDate;
		this.lastDueDate = lastDueDate;
		this.payType = payType;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.chequeDate = chequeDate;
		this.isBusinessHave = isBusinessHave;
		this.businessNameId = businessNameId;
		this.customerName = customerName;
		this.counterOrderId = counterOrderId;
		this.totalAmountWithTax = totalAmountWithTax;
		this.employeeName = employeeName;
		this.status = status;
		this.chequeClearStatus = chequeClearStatus;
		this.transactionRefNo = transactionRefNo;
		this.comment = comment;
		this.paymentMethodName = paymentMethodName;
	}

	public long getPaymentCounterId() {
		return paymentCounterId;
	}

	public void setPaymentCounterId(long paymentCounterId) {
		this.paymentCounterId = paymentCounterId;
	}

	public double getCurrentAmountPaid() {
		return currentAmountPaid;
	}

	public void setCurrentAmountPaid(double currentAmountPaid) {
		this.currentAmountPaid = currentAmountPaid;
	}

	public double getCurrentAmountRefund() {
		return currentAmountRefund;
	}

	public void setCurrentAmountRefund(double currentAmountRefund) {
		this.currentAmountRefund = currentAmountRefund;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public boolean isBusinessHave() {
		return isBusinessHave;
	}

	public void setBusinessHave(boolean isBusinessHave) {
		this.isBusinessHave = isBusinessHave;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isChequeClearStatus() {
		return chequeClearStatus;
	}

	public void setChequeClearStatus(boolean chequeClearStatus) {
		this.chequeClearStatus = chequeClearStatus;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	@Override
	public String toString() {
		return "PaymentCounterReport [paymentCounterId=" + paymentCounterId + ", currentAmountPaid=" + currentAmountPaid
				+ ", currentAmountRefund=" + currentAmountRefund + ", paidDate=" + paidDate + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", payType=" + payType + ", chequeNumber=" + chequeNumber
				+ ", bankName=" + bankName + ", chequeDate=" + chequeDate + ", isBusinessHave=" + isBusinessHave
				+ ", businessNameId=" + businessNameId + ", customerName=" + customerName + ", counterOrderId="
				+ counterOrderId + ", totalAmountWithTax=" + totalAmountWithTax + ", employeeName=" + employeeName
				+ ", status=" + status + ", chequeClearStatus=" + chequeClearStatus + ", transactionRefNo="
				+ transactionRefNo + ", comment=" + comment + ", paymentMethodName=" + paymentMethodName + "]";
	}

}
