package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.responseEntities.AreaModel;

public class EmployeeAreaDetails {

	private String name;
	private String departmentName;
	private String mobileNumber;
	private String employeeGenId;
	private List<AreaModel> areaList;

	public EmployeeAreaDetails(String name, String departmentName, String mobileNumber, String employeeGenId,
			List<AreaModel> areaList) {
		super();
		this.name = name;
		this.departmentName = departmentName;
		this.mobileNumber = mobileNumber;
		this.employeeGenId = employeeGenId;
		this.areaList = areaList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmployeeGenId() {
		return employeeGenId;
	}

	public void setEmployeeGenId(String employeeGenId) {
		this.employeeGenId = employeeGenId;
	}

	public List<AreaModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "EmployeeAreaDetails [name=" + name + ", departmentName=" + departmentName + ", mobileNumber="
				+ mobileNumber + ", employeeGenId=" + employeeGenId + ", areaList=" + areaList + "]";
	}

}
