package com.bluesquare.rc.models;

import javax.persistence.Column;

import com.bluesquare.rc.responseEntities.BankAccountModel;

public class BankChequeBookModel {

	BankAccountModel bankAccount;
	private String srtChqNo;
	private String endChqNo;
	private long numberOfLeaves;
	private long numberOfUnusedLeaves;

	public BankChequeBookModel(BankAccountModel bankAccount, String srtChqNo, String endChqNo, long numberOfLeaves,
			long numberOfUnusedLeaves) {
		super();
		this.bankAccount = bankAccount;
		this.srtChqNo = srtChqNo;
		this.endChqNo = endChqNo;
		this.numberOfLeaves = numberOfLeaves;
		this.numberOfUnusedLeaves = numberOfUnusedLeaves;
	}

	public BankAccountModel getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccountModel bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getSrtChqNo() {
		return srtChqNo;
	}

	public void setSrtChqNo(String srtChqNo) {
		this.srtChqNo = srtChqNo;
	}

	public String getEndChqNo() {
		return endChqNo;
	}

	public void setEndChqNo(String endChqNo) {
		this.endChqNo = endChqNo;
	}

	public long getNumberOfLeaves() {
		return numberOfLeaves;
	}

	public void setNumberOfLeaves(long numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}

	public long getNumberOfUnusedLeaves() {
		return numberOfUnusedLeaves;
	}

	public void setNumberOfUnusedLeaves(long numberOfUnusedLeaves) {
		this.numberOfUnusedLeaves = numberOfUnusedLeaves;
	}

	@Override
	public String toString() {
		return "BankChequeBookModel [bankAccount=" + bankAccount + ", srtChqNo=" + srtChqNo + ", endChqNo=" + endChqNo
				+ ", numberOfLeaves=" + numberOfLeaves + ", numberOfUnusedLeaves=" + numberOfUnusedLeaves + "]";
	}

}
