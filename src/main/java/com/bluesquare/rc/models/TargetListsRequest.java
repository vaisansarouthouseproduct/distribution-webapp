package com.bluesquare.rc.models;

public class TargetListsRequest {
	public String targetPeriod;
	public String currentFuture;
	public long departmentId;

	public String getTargetPeriod() {
		return targetPeriod;
	}

	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}

	public String getCurrentFuture() {
		return currentFuture;
	}

	public void setCurrentFuture(String currentFuture) {
		this.currentFuture = currentFuture;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "TargetListsRequest [targetPeriod=" + targetPeriod + ", currentFuture=" + currentFuture
				+ ", departmentId=" + departmentId + "]";
	}

}
