package com.bluesquare.rc.models;

import java.util.Date;

public class LedgerPaymentView {
	private Date dateTime;
	private String description;
	private String reference;
	private String payMode;
	private double debit;
	private double credit;
	private double balance;
	public LedgerPaymentView(Date dateTime, String description, String reference, String payMode, double debit,
			double credit, double balance) {
		super();
		this.dateTime = dateTime;
		this.description = description;
		this.reference = reference;
		this.payMode = payMode;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public double getDebit() {
		return debit;
	}
	public void setDebit(double debit) {
		this.debit = debit;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "LedgerPaymentView [dateTime=" + dateTime + ", description=" + description + ", reference=" + reference
				+ ", payMode=" + payMode + ", debit=" + debit + ", credit=" + credit + ", balance=" + balance + "]";
	}
}
