package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.OfflineAPIRequest;

public class ChangePaymentDateRequest extends OfflineAPIRequest{

	 private String orderId;
	 private long date;
	 
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ChangePaymentDateRequest [orderId=" + orderId + ", date=" + date + "]";
	}
	 
	 
	 

}
