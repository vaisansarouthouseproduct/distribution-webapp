package com.bluesquare.rc.models;

import com.bluesquare.rc.responseEntities.TransportationModel;

public class InvoiceDetails {

	private String invoiceNumber;
	private String orderId;
	private TransportationModel transportation;
	private String vehicalNumber;
	private String docketNumber;
	private double transportationCharges;
	
	
	
	
	
	public InvoiceDetails(String invoiceNumber, String orderId, TransportationModel transportation,
			String vehicalNumber, String docketNumber, double transportationCharges) {
		super();
		this.invoiceNumber = invoiceNumber;
		this.orderId = orderId;
		this.transportation = transportation;
		this.vehicalNumber = vehicalNumber;
		this.docketNumber = docketNumber;
		this.transportationCharges = transportationCharges;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public TransportationModel getTransportation() {
		return transportation;
	}
	public void setTransportation(TransportationModel transportation) {
		this.transportation = transportation;
	}
	public String getVehicalNumber() {
		return vehicalNumber;
	}
	public void setVehicalNumber(String vehicalNumber) {
		this.vehicalNumber = vehicalNumber;
	}
	public String getDocketNumber() {
		return docketNumber;
	}
	public void setDocketNumber(String docketNumber) {
		this.docketNumber = docketNumber;
	}
	public double getTransportationCharges() {
		return transportationCharges;
	}
	public void setTransportationCharges(double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}
	@Override
	public String toString() {
		return "InvoiceDetails [invoiceNumber=" + invoiceNumber + ", orderId=" + orderId + ", transportation="
				+ transportation + ", vehicalNumber=" + vehicalNumber + ", docketNumber=" + docketNumber
				+ ", transportationCharges=" + transportationCharges + "]";
	}
	
	
	
	
	
	
}
