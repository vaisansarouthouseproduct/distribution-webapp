package com.bluesquare.rc.models;

public class FetchBankTransactionsRequest{

	private String transactionType;
	
	private String ordering;
	
	private String fromDate;
	
	private String toDate;
	
	private int transactionCount;

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getOrdering() {
		return ordering;
	}

	public void setOrdering(String ordering) {
		this.ordering = ordering;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public int getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}

	@Override
	public String toString() {
		return "FetchBankTransactionsRequest [transactionType=" + transactionType + ", ordering=" + ordering
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + ", transactionCount=" + transactionCount + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
