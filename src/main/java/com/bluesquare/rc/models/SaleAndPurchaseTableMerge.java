/**
 * 
 */
package com.bluesquare.rc.models;

/**
 * @author aNKIT
 *
 */
public class SaleAndPurchaseTableMerge {
		
	String date;
	String invoiceNo;
	String name;
	String gstNo;
	double taxableAmount;
	double cgstAmt;
	double sgstAmt;
	double igstAmt;
	double totalAmount;
	
	public SaleAndPurchaseTableMerge(String date, String invoiceNo, String name, String gstNo, double taxableAmount,
			double cgstAmt, double sgstAmt, double igstAmt, double totalAmount) {
		super();
		this.date = date;
		this.invoiceNo = invoiceNo;
		this.name = name;
		this.gstNo = gstNo;
		this.taxableAmount = taxableAmount;
		this.cgstAmt = cgstAmt;
		this.sgstAmt = sgstAmt;
		this.igstAmt = igstAmt;
		this.totalAmount = totalAmount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGstNo() {
		return gstNo;
	}
	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}
	public double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public double getCgstAmt() {
		return cgstAmt;
	}
	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}
	public double getSgstAmt() {
		return sgstAmt;
	}
	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}
	public double getIgstAmt() {
		return igstAmt;
	}
	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "SaleAndPurchaseTableMerge [date=" + date + ", invoiceNo=" + invoiceNo + ", name=" + name + ", gstNo=" + gstNo
				+ ", taxableAmount=" + taxableAmount + ", cgstAmt=" + cgstAmt + ", sgstAmt=" + sgstAmt + ", igstAmt="
				+ igstAmt + ", totalAmount=" + totalAmount + "]";
	}
	
	
}
