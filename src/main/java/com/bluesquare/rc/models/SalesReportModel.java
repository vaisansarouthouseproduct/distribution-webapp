package com.bluesquare.rc.models;
	

/**
 * @author aNKIT
 *
 */
public class SalesReportModel {

	private long srno;
	private String orderId;
	private String businessNameId;
	private String shopName;
	private String areaName;
	private String smName;
	private long employeeDetailsId;
	private float orderAmount;
	private float orderAmountWithTax;
	private String dateOfPayment;
	private String orderStatusSM;
	private String orderStatusSMDate;
	private String orderStatusSMTime;
	private String orderStatusGK;
	private String orderStatusGKDate;
	private String orderStatusGKTime;
	private String orderStatusDB;
	private String orderStatusDBDate;
	private String orderStatusDBTime;
	public SalesReportModel(long srno, String orderId, String businessNameId, String shopName, String areaName,
			String smName, long employeeDetailsId, float orderAmount, float orderAmountWithTax, String dateOfPayment,
			String orderStatusSM, String orderStatusSMDate, String orderStatusSMTime, String orderStatusGK,
			String orderStatusGKDate, String orderStatusGKTime, String orderStatusDB, String orderStatusDBDate,
			String orderStatusDBTime) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.businessNameId = businessNameId;
		this.shopName = shopName;
		this.areaName = areaName;
		this.smName = smName;
		this.employeeDetailsId = employeeDetailsId;
		this.orderAmount = orderAmount;
		this.orderAmountWithTax = orderAmountWithTax;
		this.dateOfPayment = dateOfPayment;
		this.orderStatusSM = orderStatusSM;
		this.orderStatusSMDate = orderStatusSMDate;
		this.orderStatusSMTime = orderStatusSMTime;
		this.orderStatusGK = orderStatusGK;
		this.orderStatusGKDate = orderStatusGKDate;
		this.orderStatusGKTime = orderStatusGKTime;
		this.orderStatusDB = orderStatusDB;
		this.orderStatusDBDate = orderStatusDBDate;
		this.orderStatusDBTime = orderStatusDBTime;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getSmName() {
		return smName;
	}
	public void setSmName(String smName) {
		this.smName = smName;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public float getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(float orderAmount) {
		this.orderAmount = orderAmount;
	}
	public float getOrderAmountWithTax() {
		return orderAmountWithTax;
	}
	public void setOrderAmountWithTax(float orderAmountWithTax) {
		this.orderAmountWithTax = orderAmountWithTax;
	}
	public String getDateOfPayment() {
		return dateOfPayment;
	}
	public void setDateOfPayment(String dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}
	public String getOrderStatusSM() {
		return orderStatusSM;
	}
	public void setOrderStatusSM(String orderStatusSM) {
		this.orderStatusSM = orderStatusSM;
	}
	public String getOrderStatusSMDate() {
		return orderStatusSMDate;
	}
	public void setOrderStatusSMDate(String orderStatusSMDate) {
		this.orderStatusSMDate = orderStatusSMDate;
	}
	public String getOrderStatusSMTime() {
		return orderStatusSMTime;
	}
	public void setOrderStatusSMTime(String orderStatusSMTime) {
		this.orderStatusSMTime = orderStatusSMTime;
	}
	public String getOrderStatusGK() {
		return orderStatusGK;
	}
	public void setOrderStatusGK(String orderStatusGK) {
		this.orderStatusGK = orderStatusGK;
	}
	public String getOrderStatusGKDate() {
		return orderStatusGKDate;
	}
	public void setOrderStatusGKDate(String orderStatusGKDate) {
		this.orderStatusGKDate = orderStatusGKDate;
	}
	public String getOrderStatusGKTime() {
		return orderStatusGKTime;
	}
	public void setOrderStatusGKTime(String orderStatusGKTime) {
		this.orderStatusGKTime = orderStatusGKTime;
	}
	public String getOrderStatusDB() {
		return orderStatusDB;
	}
	public void setOrderStatusDB(String orderStatusDB) {
		this.orderStatusDB = orderStatusDB;
	}
	public String getOrderStatusDBDate() {
		return orderStatusDBDate;
	}
	public void setOrderStatusDBDate(String orderStatusDBDate) {
		this.orderStatusDBDate = orderStatusDBDate;
	}
	public String getOrderStatusDBTime() {
		return orderStatusDBTime;
	}
	public void setOrderStatusDBTime(String orderStatusDBTime) {
		this.orderStatusDBTime = orderStatusDBTime;
	}
	@Override
	public String toString() {
		return "SalesReportModel [srno=" + srno + ", orderId=" + orderId + ", businessNameId=" + businessNameId
				+ ", shopName=" + shopName + ", areaName=" + areaName + ", smName=" + smName + ", employeeDetailsId="
				+ employeeDetailsId + ", orderAmount=" + orderAmount + ", orderAmountWithTax=" + orderAmountWithTax
				+ ", dateOfPayment=" + dateOfPayment + ", orderStatusSM=" + orderStatusSM + ", orderStatusSMDate="
				+ orderStatusSMDate + ", orderStatusSMTime=" + orderStatusSMTime + ", orderStatusGK=" + orderStatusGK
				+ ", orderStatusGKDate=" + orderStatusGKDate + ", orderStatusGKTime=" + orderStatusGKTime
				+ ", orderStatusDB=" + orderStatusDB + ", orderStatusDBDate=" + orderStatusDBDate
				+ ", orderStatusDBTime=" + orderStatusDBTime + "]";
	}
	
	
	
	
}
