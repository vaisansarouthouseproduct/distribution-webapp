package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;

public class TargetLists {
	public TargetAssign targetAssign;
	public List<TargetAssignTargets> targetAssignTargetList;

	
	public TargetLists(TargetAssign targetAssign, List<TargetAssignTargets> targetAssignTargetList) {
		super();
		this.targetAssign = targetAssign;
		this.targetAssignTargetList = targetAssignTargetList;
	}

	public TargetAssign getTargetAssign() {
		return targetAssign;
	}

	public void setTargetAssign(TargetAssign targetAssign) {
		this.targetAssign = targetAssign;
	}

	public List<TargetAssignTargets> getTargetAssignTargetList() {
		return targetAssignTargetList;
	}

	public void setTargetAssignTargetList(List<TargetAssignTargets> targetAssignTargetList) {
		this.targetAssignTargetList = targetAssignTargetList;
	}

	@Override
	public String toString() {
		return "TargetLists [targetAssign=" + targetAssign + ", targetAssignTargetList=" + targetAssignTargetList + "]";
	}

}
