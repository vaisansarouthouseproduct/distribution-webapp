package com.bluesquare.rc.models;

public class ReturnOrderFromDeliveryBoyReport {

	private long srno;
	private String returnFromDeliveryBoyMainId;
	private String shopName;
	private long totalIssuedQuantity;
	private long totalReceivedQuantity;
	private boolean status;
	private String smName;
	private String areaName;
	private long totalDeliveryQuantity;
	private long totalDamageQuantity;

	public ReturnOrderFromDeliveryBoyReport(long srno, String returnFromDeliveryBoyMainId, String shopName,
			long totalIssuedQuantity, long totalReceivedQuantity, boolean status, String smName, String areaName,
			long totalDeliveryQuantity, long totalDamageQuantity) {
		super();
		this.srno = srno;
		this.returnFromDeliveryBoyMainId = returnFromDeliveryBoyMainId;
		this.shopName = shopName;
		this.totalIssuedQuantity = totalIssuedQuantity;
		this.totalReceivedQuantity = totalReceivedQuantity;
		this.status = status;
		this.smName = smName;
		this.areaName = areaName;
		this.totalDeliveryQuantity = totalDeliveryQuantity;
		this.totalDamageQuantity = totalDamageQuantity;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getReturnFromDeliveryBoyMainId() {
		return returnFromDeliveryBoyMainId;
	}

	public void setReturnFromDeliveryBoyMainId(String returnFromDeliveryBoyMainId) {
		this.returnFromDeliveryBoyMainId = returnFromDeliveryBoyMainId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public long getTotalIssuedQuantity() {
		return totalIssuedQuantity;
	}

	public void setTotalIssuedQuantity(long totalIssuedQuantity) {
		this.totalIssuedQuantity = totalIssuedQuantity;
	}

	public long getTotalReceivedQuantity() {
		return totalReceivedQuantity;
	}

	public void setTotalReceivedQuantity(long totalReceivedQuantity) {
		this.totalReceivedQuantity = totalReceivedQuantity;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getSmName() {
		return smName;
	}

	public void setSmName(String smName) {
		this.smName = smName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getTotalDeliveryQuantity() {
		return totalDeliveryQuantity;
	}

	public void setTotalDeliveryQuantity(long totalDeliveryQuantity) {
		this.totalDeliveryQuantity = totalDeliveryQuantity;
	}

	public long getTotalDamageQuantity() {
		return totalDamageQuantity;
	}

	public void setTotalDamageQuantity(long totalDamageQuantity) {
		this.totalDamageQuantity = totalDamageQuantity;
	}

	@Override
	public String toString() {
		return "ReturnOrderFromDeliveryBoyReport [srno=" + srno + ", returnFromDeliveryBoyMainId="
				+ returnFromDeliveryBoyMainId + ", shopName=" + shopName + ", totalIssuedQuantity="
				+ totalIssuedQuantity + ", totalReceivedQuantity=" + totalReceivedQuantity + ", status=" + status
				+ ", smName=" + smName + ", areaName=" + areaName + ", totalDeliveryQuantity=" + totalDeliveryQuantity
				+ ", totalDamageQuantity=" + totalDamageQuantity + "]";
	}

}
