package com.bluesquare.rc.models;

public class CalculateProperTaxModel {

	private float mrp;
	private float unitprice;
	private float igst;
	private float sgst;
	private float cgst;
	public CalculateProperTaxModel(float mrp, float unitprice, float igst, float sgst, float cgst) {
		super();
		this.mrp = mrp;
		this.unitprice = unitprice;
		this.igst = igst;
		this.sgst = sgst;
		this.cgst = cgst;
	}
	public float getMrp() {
		return mrp;
	}
	public void setMrp(float mrp) {
		this.mrp = mrp;
	}
	public float getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(float unitprice) {
		this.unitprice = unitprice;
	}
	public float getIgst() {
		return igst;
	}
	public void setIgst(float igst) {
		this.igst = igst;
	}
	public float getSgst() {
		return sgst;
	}
	public void setSgst(float sgst) {
		this.sgst = sgst;
	}
	public float getCgst() {
		return cgst;
	}
	public void setCgst(float cgst) {
		this.cgst = cgst;
	}
	@Override
	public String toString() {
		return "CalculateProperTaxModel [mrp=" + mrp + ", unitprice=" + unitprice + ", igst=" + igst + ", sgst=" + sgst
				+ ", cgst=" + cgst + "]";
	}
	
	
	
}
