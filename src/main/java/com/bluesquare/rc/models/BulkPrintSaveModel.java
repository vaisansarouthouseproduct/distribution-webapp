package com.bluesquare.rc.models;

public class BulkPrintSaveModel {
	
	public String chequeNo;
	public String payeeName;
	public double chequeAmt;
	public String acPayee;
	public String chequeDate;
	public String remark;
	
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public String getPayeeName() {
		return payeeName;
	}
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	public double getChequeAmt() {
		return chequeAmt;
	}
	public void setChequeAmt(double chequeAmt) {
		this.chequeAmt = chequeAmt;
	}
	public String getAcPayee() {
		return acPayee;
	}
	public void setAcPayee(String acPayee) {
		this.acPayee = acPayee;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "BulkPrintSaveModel [chequeNo=" + chequeNo + ", payeeName=" + payeeName + ", chequeAmt=" + chequeAmt
				+ ", acPayee=" + acPayee + ", chequeDate=" + chequeDate + ", remark=" + remark + "]";
	}
	
	
	
	

}
