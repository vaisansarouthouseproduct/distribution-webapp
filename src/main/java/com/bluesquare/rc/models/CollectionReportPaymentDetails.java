package com.bluesquare.rc.models;

import java.util.Date;

public class CollectionReportPaymentDetails {

	private long srno;
	private String businessId;	
	private String shopName;
	private String orderId;
	private String mobileNumber;
	private String areaName;
	private String regionName;
	private String cityName;
	private double totalAmount;
	private double amountPaid;
	private double balanceAmount;
	private Date nextDueDate;
	private Date paidDate;
	private String payMode;
	private String payStatus;
	public CollectionReportPaymentDetails(long srno, String businessId, String shopName, String orderId,
			String mobileNumber, String areaName, String regionName, String cityName, double totalAmount,
			double amountPaid, double balanceAmount, Date nextDueDate, Date paidDate, String payMode,
			String payStatus) {
		super();
		this.srno = srno;
		this.businessId = businessId;
		this.shopName = shopName;
		this.orderId = orderId;
		this.mobileNumber = mobileNumber;
		this.areaName = areaName;
		this.regionName = regionName;
		this.cityName = cityName;
		this.totalAmount = totalAmount;
		this.amountPaid = amountPaid;
		this.balanceAmount = balanceAmount;
		this.nextDueDate = nextDueDate;
		this.paidDate = paidDate;
		this.payMode = payMode;
		this.payStatus = payStatus;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Date getNextDueDate() {
		return nextDueDate;
	}
	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	@Override
	public String toString() {
		return "CollectionReportPaymentDetails [srno=" + srno + ", businessId=" + businessId + ", shopName=" + shopName
				+ ", orderId=" + orderId + ", mobileNumber=" + mobileNumber + ", areaName=" + areaName + ", regionName="
				+ regionName + ", cityName=" + cityName + ", totalAmount=" + totalAmount + ", amountPaid=" + amountPaid
				+ ", balanceAmount=" + balanceAmount + ", nextDueDate=" + nextDueDate + ", paidDate=" + paidDate
				+ ", payMode=" + payMode + ", payStatus=" + payStatus + "]";
	}


	

	
	
}
