package com.bluesquare.rc.models;

import com.bluesquare.rc.responseEntities.BankAccountModel;
import com.bluesquare.rc.rest.models.BaseDomain;

public class FetchAccountInfoResponse extends BaseDomain{
	public BankAccountModel bankAccount;

	public BankAccountModel getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccountModel bankAccount) {
		this.bankAccount = bankAccount;
	}

	@Override
	public String toString() {
		return "FetchAccountInfoResponse [bankAccount=" + bankAccount + "]";
	}
	
	
	
}
