/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderDetailsReport {

	private String orderId;
	private String shopName;
	private String areaName;
	private long reIssueOrderId;
	private String employeeName;
	private String employeeDepartmentName;
	private long reIssuedQuantity;
	private long returnQuantity;
	private Date reIssueDate;
	private Date returnOrderDateTime;
	private Date reIssueDeliveryDate;
	private String reIssueStatus;
	private double replacementTotalAmountWithTax;
	private String businessNameId;
	private String returnOrderProductId;
	private String mobileNumber;
	
	public ReIssueOrderDetailsReport(String orderId, String shopName, String areaName, long reIssueOrderId,
			String employeeName, String employeeDepartmentName, long reIssuedQuantity, long returnQuantity,
			Date reIssueDate, Date returnOrderDateTime, Date reIssueDeliveryDate, String reIssueStatus,
			double replacementTotalAmountWithTax, String businessNameId, String returnOrderProductId,
			String mobileNumber) {
		super();
		this.orderId = orderId;
		this.shopName = shopName;
		this.areaName = areaName;
		this.reIssueOrderId = reIssueOrderId;
		this.employeeName = employeeName;
		this.employeeDepartmentName = employeeDepartmentName;
		this.reIssuedQuantity = reIssuedQuantity;
		this.returnQuantity = returnQuantity;
		this.reIssueDate = reIssueDate;
		this.returnOrderDateTime = returnOrderDateTime;
		this.reIssueDeliveryDate = reIssueDeliveryDate;
		this.reIssueStatus = reIssueStatus;
		this.replacementTotalAmountWithTax = replacementTotalAmountWithTax;
		this.businessNameId = businessNameId;
		this.returnOrderProductId = returnOrderProductId;
		this.mobileNumber = mobileNumber;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getReIssueOrderId() {
		return reIssueOrderId;
	}

	public void setReIssueOrderId(long reIssueOrderId) {
		this.reIssueOrderId = reIssueOrderId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeDepartmentName() {
		return employeeDepartmentName;
	}

	public void setEmployeeDepartmentName(String employeeDepartmentName) {
		this.employeeDepartmentName = employeeDepartmentName;
	}

	public long getReIssuedQuantity() {
		return reIssuedQuantity;
	}

	public void setReIssuedQuantity(long reIssuedQuantity) {
		this.reIssuedQuantity = reIssuedQuantity;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public Date getReIssueDate() {
		return reIssueDate;
	}

	public void setReIssueDate(Date reIssueDate) {
		this.reIssueDate = reIssueDate;
	}

	public Date getReturnOrderDateTime() {
		return returnOrderDateTime;
	}

	public void setReturnOrderDateTime(Date returnOrderDateTime) {
		this.returnOrderDateTime = returnOrderDateTime;
	}

	public Date getReIssueDeliveryDate() {
		return reIssueDeliveryDate;
	}

	public void setReIssueDeliveryDate(Date reIssueDeliveryDate) {
		this.reIssueDeliveryDate = reIssueDeliveryDate;
	}

	public String getReIssueStatus() {
		return reIssueStatus;
	}

	public void setReIssueStatus(String reIssueStatus) {
		this.reIssueStatus = reIssueStatus;
	}

	public double getReplacementTotalAmountWithTax() {
		return replacementTotalAmountWithTax;
	}

	public void setReplacementTotalAmountWithTax(double replacementTotalAmountWithTax) {
		this.replacementTotalAmountWithTax = replacementTotalAmountWithTax;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsReport [orderId=" + orderId + ", shopName=" + shopName + ", areaName=" + areaName
				+ ", reIssueOrderId=" + reIssueOrderId + ", employeeName=" + employeeName + ", employeeDepartmentName="
				+ employeeDepartmentName + ", reIssuedQuantity=" + reIssuedQuantity + ", returnQuantity="
				+ returnQuantity + ", reIssueDate=" + reIssueDate + ", returnOrderDateTime=" + returnOrderDateTime
				+ ", reIssueDeliveryDate=" + reIssueDeliveryDate + ", reIssueStatus=" + reIssueStatus
				+ ", replacementTotalAmountWithTax=" + replacementTotalAmountWithTax + ", businessNameId="
				+ businessNameId + ", returnOrderProductId=" + returnOrderProductId + ", mobileNumber=" + mobileNumber
				+ "]";
	}

}
