package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.rest.models.BaseDomain;

public class EmployeeCommissionResponse extends BaseDomain{
	
	public List<EmployeeCommissionModel> employeeCommissionModelsList;


	public List<EmployeeCommissionModel> getEmployeeCommissionModelsList() {
		return employeeCommissionModelsList;
	}

	public void setEmployeeCommissionModelsList(List<EmployeeCommissionModel> employeeCommissionModelsList) {
		this.employeeCommissionModelsList = employeeCommissionModelsList;
	}

	@Override
	public String toString() {
		return "EmployeeCommissionResponse [employeeCommissionModelsList=" + employeeCommissionModelsList + "]";
	}
	
	

}
