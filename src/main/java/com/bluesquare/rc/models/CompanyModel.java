package com.bluesquare.rc.models;

public class CompanyModel {

	long companyId;
	String companyName;

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "CompanyModel [companyId=" + companyId + ", companyName=" + companyName + "]";
	}

}
