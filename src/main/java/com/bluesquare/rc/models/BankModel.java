package com.bluesquare.rc.models;

import java.util.Date;

public class BankModel {

	String bankName;
	String shortName;
	Date bankAddedDatetime;
	Date bankUpdatedDatetime;
	long bankId;

	public BankModel(String bankName, String shortName, Date bankAddedDatetime, Date bankUpdatedDatetime, long bankId) {
		super();
		this.bankName = bankName;
		this.shortName = shortName;
		this.bankAddedDatetime = bankAddedDatetime;
		this.bankUpdatedDatetime = bankUpdatedDatetime;
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Date getBankAddedDatetime() {
		return bankAddedDatetime;
	}

	public void setBankAddedDatetime(Date bankAddedDatetime) {
		this.bankAddedDatetime = bankAddedDatetime;
	}

	public Date getBankUpdatedDatetime() {
		return bankUpdatedDatetime;
	}

	public void setBankUpdatedDatetime(Date bankUpdatedDatetime) {
		this.bankUpdatedDatetime = bankUpdatedDatetime;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	@Override
	public String toString() {
		return "BankModel [bankName=" + bankName + ", shortName=" + shortName + ", bankAddedDatetime="
				+ bankAddedDatetime + ", bankUpdatedDatetime=" + bankUpdatedDatetime + ", bankId=" + bankId + "]";
	}

}
