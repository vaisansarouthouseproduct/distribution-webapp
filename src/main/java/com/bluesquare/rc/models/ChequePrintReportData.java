package com.bluesquare.rc.models;

public class ChequePrintReportData {
	
	private String chqBookSeriesNo;
	private long chequeBookId;
	private  long totalChequeCount;
	private long printedChequeCount;
	private long unusedChequeCount;
	private long cancelChequeCount;
	
	public String getChqBookSeriesNo() {
		return chqBookSeriesNo;
	}
	public void setChqBookSeriesNo(String chqBookSeriesNo) {
		this.chqBookSeriesNo = chqBookSeriesNo;
	}
	public long getChequeBookId() {
		return chequeBookId;
	}
	public void setChequeBookId(long chequeBookId) {
		this.chequeBookId = chequeBookId;
	}
	public long getTotalChequeCount() {
		return totalChequeCount;
	}
	public void setTotalChequeCount(long totalChequeCount) {
		this.totalChequeCount = totalChequeCount;
	}
	public long getPrintedChequeCount() {
		return printedChequeCount;
	}
	public void setPrintedChequeCount(long printedChequeCount) {
		this.printedChequeCount = printedChequeCount;
	}
	public long getUnusedChequeCount() {
		return unusedChequeCount;
	}
	public void setUnusedChequeCount(long unusedChequeCount) {
		this.unusedChequeCount = unusedChequeCount;
	}
	public long getCancelChequeCount() {
		return cancelChequeCount;
	}
	public void setCancelChequeCount(long cancelChequeCount) {
		this.cancelChequeCount = cancelChequeCount;
	}
	
	
	@Override
	public String toString() {
		return "ChequePrintReportData [chqBookSeriesNo=" + chqBookSeriesNo + ", chequeBookId=" + chequeBookId
				+ ", totalChequeCount=" + totalChequeCount + ", printedChequeCount=" + printedChequeCount
				+ ", unusedChequeCount=" + unusedChequeCount + ", cancelChequeCount=" + cancelChequeCount + "]";
	}
	
	
	
	
	

}
