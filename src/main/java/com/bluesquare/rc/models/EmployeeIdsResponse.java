package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.rest.models.BaseDomain;

public class EmployeeIdsResponse extends BaseDomain {
	public List<Long> employeeIds;

	public List<Long> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(List<Long> employeeIds) {
		this.employeeIds = employeeIds;
	}

	@Override
	public String toString() {
		return "EmployeeIdsResponse [employeeIds=" + employeeIds + "]";
	}

}
