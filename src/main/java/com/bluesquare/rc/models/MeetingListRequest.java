package com.bluesquare.rc.models;

public class MeetingListRequest {
	long employeeDetailsId;
	long departmentId;
	String pickDate;

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getPickDate() {
		return pickDate;
	}

	public void setPickDate(String pickDate) {
		this.pickDate = pickDate;
	}

	@Override
	public String toString() {
		return "MeetingListRequest [employeeDetailsId=" + employeeDetailsId + ", departmentId=" + departmentId
				+ ", pickDate=" + pickDate + "]";
	}

}
