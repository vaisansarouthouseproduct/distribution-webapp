package com.bluesquare.rc.models;

import java.util.Date;

import javax.persistence.Column;

import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InventoryReportView {

	private long srno;
	private String transactionId;
	private SupplierModel supplier;
	private double totalQuantity;
	private double totalAmount;
	private double totalAmountTax;
	private double amountPaid;
	private double amountUnPaid;
	private Date addedDate;
	private Date paymentDate;
	private String byUserName;
	private String payStatus;
	private Date billDate;
	private String billNumber;
	private double discountAmount;
	private double discountPercentage;
	private String discountType;
	private double totalAmountBeforeDiscount;
	private double totalAmountTaxBeforeDiscount;

	@JsonProperty
	private boolean isDiscountGiven;
	private String discountOnMRP;

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public SupplierModel getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierModel supplier) {
		this.supplier = supplier;
	}

	public double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountTax() {
		return totalAmountTax;
	}

	public void setTotalAmountTax(double totalAmountTax) {
		this.totalAmountTax = totalAmountTax;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public double getAmountUnPaid() {
		return amountUnPaid;
	}

	public void setAmountUnPaid(double amountUnPaid) {
		this.amountUnPaid = amountUnPaid;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getByUserName() {
		return byUserName;
	}

	public void setByUserName(String byUserName) {
		this.byUserName = byUserName;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getTotalAmountBeforeDiscount() {
		return totalAmountBeforeDiscount;
	}

	public void setTotalAmountBeforeDiscount(double totalAmountBeforeDiscount) {
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
	}

	public double getTotalAmountTaxBeforeDiscount() {
		return totalAmountTaxBeforeDiscount;
	}

	public void setTotalAmountTaxBeforeDiscount(double totalAmountTaxBeforeDiscount) {
		this.totalAmountTaxBeforeDiscount = totalAmountTaxBeforeDiscount;
	}

	public boolean isDiscountGiven() {
		return isDiscountGiven;
	}

	public void setDiscountGiven(boolean isDiscountGiven) {
		this.isDiscountGiven = isDiscountGiven;
	}
	
	

	public String getDiscountOnMRP() {
		return discountOnMRP;
	}

	public void setDiscountOnMRP(String discountOnMRP) {
		this.discountOnMRP = discountOnMRP;
	}

	public InventoryReportView(long srno, String transactionId, SupplierModel supplier, double totalQuantity,
			double totalAmount, double totalAmountTax, double amountPaid, double amountUnPaid, Date addedDate,
			Date paymentDate, String byUserName, String payStatus, Date billDate, String billNumber,
			double discountAmount, double discountPercentage, String discountType, double totalAmountBeforeDiscount,
			double totalAmountTaxBeforeDiscount, boolean isDiscountGiven,String discountOnMRP) {
		super();
		this.srno = srno;
		this.transactionId = transactionId;
		this.supplier = supplier;
		this.totalQuantity = totalQuantity;
		this.totalAmount = totalAmount;
		this.totalAmountTax = totalAmountTax;
		this.amountPaid = amountPaid;
		this.amountUnPaid = amountUnPaid;
		this.addedDate = addedDate;
		this.paymentDate = paymentDate;
		this.byUserName = byUserName;
		this.payStatus = payStatus;
		this.billDate = billDate;
		this.billNumber = billNumber;
		this.discountAmount = discountAmount;
		this.discountPercentage = discountPercentage;
		this.discountType = discountType;
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
		this.totalAmountTaxBeforeDiscount = totalAmountTaxBeforeDiscount;
		this.isDiscountGiven = isDiscountGiven;
		this.discountOnMRP = discountOnMRP;
	}

	@Override
	public String toString() {
		return "InventoryReportView [srno=" + srno + ", transactionId=" + transactionId + ", supplier=" + supplier
				+ ", totalQuantity=" + totalQuantity + ", totalAmount=" + totalAmount + ", totalAmountTax="
				+ totalAmountTax + ", amountPaid=" + amountPaid + ", amountUnPaid=" + amountUnPaid + ", addedDate="
				+ addedDate + ", paymentDate=" + paymentDate + ", byUserName=" + byUserName + ", payStatus=" + payStatus
				+ ", billDate=" + billDate + ", billNumber=" + billNumber + ", discountAmount=" + discountAmount
				+ ", discountPercentage=" + discountPercentage + ", discountType=" + discountType
				+ ", totalAmountBeforeDiscount=" + totalAmountBeforeDiscount + ", totalAmountTaxBeforeDiscount="
				+ totalAmountTaxBeforeDiscount + ", isDiscountGiven=" + isDiscountGiven + ", discountOnMRP="
				+ discountOnMRP + "]";
	}

	
}
