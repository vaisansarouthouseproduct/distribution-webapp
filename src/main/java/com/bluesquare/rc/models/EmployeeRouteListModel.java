package com.bluesquare.rc.models;

import java.util.List;

public class EmployeeRouteListModel {
	private List<EmployeeRouteList> employeeRouteList;

	public EmployeeRouteListModel(List<EmployeeRouteList> employeeRouteList) {
		super();
		this.employeeRouteList = employeeRouteList;
	}

	public List<EmployeeRouteList> getEmployeeRouteList() {
		return employeeRouteList;
	}

	public void setEmployeeRouteList(List<EmployeeRouteList> employeeRouteList) {
		this.employeeRouteList = employeeRouteList;
	}

	@Override
	public String toString() {
		return "EmployeeRouteListModel [employeeRouteList=" + employeeRouteList + "]";
	}
	
	
}