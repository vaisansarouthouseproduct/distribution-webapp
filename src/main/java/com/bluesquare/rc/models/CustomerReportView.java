/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.responseEntities.AreaModel;

/**
 * @author aNKIT
 *
 */
public class CustomerReportView {

	private double totalAmountAllBusiness;
	private List<CustomerReport> customerReportlist;
	private List<AreaModel> areaList;
	
	public double getTotalAmountAllBusiness() {
		return totalAmountAllBusiness;
	}
	public void setTotalAmountAllBusiness(double totalAmountAllBusiness) {
		this.totalAmountAllBusiness = totalAmountAllBusiness;
	}
	public List<CustomerReport> getCustomerReportlist() {
		return customerReportlist;
	}
	public void setCustomerReportlist(List<CustomerReport> customerReportlist) {
		this.customerReportlist = customerReportlist;
	}
	public List<AreaModel> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}
	@Override
	public String toString() {
		return "CustomerReportView [totalAmountAllBusiness=" + totalAmountAllBusiness + ", customerReportlist="
				+ customerReportlist + ", areaList=" + areaList + "]";
	}
	public CustomerReportView(double totalAmountAllBusiness, List<CustomerReport> customerReportlist,
			List<AreaModel> areaList) {
		super();
		this.totalAmountAllBusiness = totalAmountAllBusiness;
		this.customerReportlist = customerReportlist;
		this.areaList = areaList;
	}
	
 }
