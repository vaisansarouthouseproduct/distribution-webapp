package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.rest.models.BaseDomain;

public class BranchListResponse extends BaseDomain {

	public List<BranchModel> branchList;

	public List<BranchModel> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<BranchModel> branchList) {
		this.branchList = branchList;
	}

	@Override
	public String toString() {
		return "BranchListResponse [branchList=" + branchList + "]";
	}

}
