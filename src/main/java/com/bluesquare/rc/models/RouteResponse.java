package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.RouteModel;
import com.bluesquare.rc.responseEntities.RoutePointsModel;
import com.bluesquare.rc.rest.models.BaseDomain;

public class RouteResponse extends BaseDomain {
	public List<RouteModel> routeList;
	public List<RoutePointsModel> routePointsList;
	public RouteModel route;
	public RoutePointsModel routePoints;

	public List<RouteModel> getRouteList() {
		return routeList;
	}

	public void setRouteList(List<RouteModel> routeList) {
		this.routeList = routeList;
	}

	public List<RoutePointsModel> getRoutePointsList() {
		return routePointsList;
	}

	public void setRoutePointsList(List<RoutePointsModel> routePointsList) {
		this.routePointsList = routePointsList;
	}

	public RouteModel getRoute() {
		return route;
	}

	public void setRoute(RouteModel route) {
		this.route = route;
	}

	public RoutePointsModel getRoutePoints() {
		return routePoints;
	}

	public void setRoutePoints(RoutePointsModel routePoints) {
		this.routePoints = routePoints;
	}

	@Override
	public String toString() {
		return "RouteListResponse [routeList=" + routeList + ", routePointsList=" + routePointsList + ", route=" + route
				+ ", routePoints=" + routePoints + "]";
	}

}
