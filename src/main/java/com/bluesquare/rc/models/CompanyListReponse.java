package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.rest.models.BaseDomain;

public class CompanyListReponse extends BaseDomain{
	
	private List<Company> companyList;
	
	private Company company;

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}
	
	

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "CompanyListReponse [companyList=" + companyList + ", company=" + company + "]";
	}

	
	
	

}
