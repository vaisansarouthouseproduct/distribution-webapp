package com.bluesquare.rc.models;

import com.bluesquare.rc.responseEntities.BusinessNameModel;

public class BusinessNameList {

	private BusinessNameModel businessName;
	private double balanceAmount;
	public BusinessNameList(BusinessNameModel businessName, double balanceAmount) {
		super();
		this.businessName = businessName;
		this.balanceAmount = balanceAmount;
	}
	public BusinessNameModel getBusinessName() {
		return businessName;
	}
	public void setBusinessName(BusinessNameModel businessName) {
		this.businessName = businessName;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@Override
	public String toString() {
		return "BusinessNameList [businessName=" + businessName + ", balanceAmount=" + balanceAmount + "]";
	}
	
	
}
