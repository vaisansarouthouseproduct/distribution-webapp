package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;

public class EmployeeListResponse extends BaseDomain{

	public List<EmployeeNameAndId> employeeDetailsList;

	public List<EmployeeNameAndId> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	public void setEmployeeDetailsList(List<EmployeeNameAndId> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}

	@Override
	public String toString() {
		return "EmployeeListResponse [employeeDetailsList=" + employeeDetailsList + "]";
	}

}
