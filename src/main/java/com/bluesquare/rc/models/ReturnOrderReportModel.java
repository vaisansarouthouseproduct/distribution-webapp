package com.bluesquare.rc.models;

import java.util.Date;

public class ReturnOrderReportModel {

	private String returnOrderProductId;
	private String orderId;
	private String shopName;
	private long employeeDetailsId;
	private String employeeName;
	private String areaName;
	private String regionName;
	private String cityName;
	private long returnTotalQuantity;
	private double returnTotalAmount;
	private double returnTotalAmountWithTax;
	private Date returnOrderProductDateTime;
	private String reIssueStatus;
	private String businessAddress;
	private Date orderDate;

	public ReturnOrderReportModel(String returnOrderProductId, String orderId, String shopName, long employeeDetailsId,
			String employeeName, String areaName, String regionName, String cityName, long returnTotalQuantity,
			double returnTotalAmount, double returnTotalAmountWithTax, Date returnOrderProductDateTime,
			String reIssueStatus, String businessAddress, Date orderDate) {
		super();
		this.returnOrderProductId = returnOrderProductId;
		this.orderId = orderId;
		this.shopName = shopName;
		this.employeeDetailsId = employeeDetailsId;
		this.employeeName = employeeName;
		this.areaName = areaName;
		this.regionName = regionName;
		this.cityName = cityName;
		this.returnTotalQuantity = returnTotalQuantity;
		this.returnTotalAmount = returnTotalAmount;
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
		this.returnOrderProductDateTime = returnOrderProductDateTime;
		this.reIssueStatus = reIssueStatus;
		this.businessAddress = businessAddress;
		this.orderDate = orderDate;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public long getReturnTotalQuantity() {
		return returnTotalQuantity;
	}

	public void setReturnTotalQuantity(long returnTotalQuantity) {
		this.returnTotalQuantity = returnTotalQuantity;
	}

	public double getReturnTotalAmount() {
		return returnTotalAmount;
	}

	public void setReturnTotalAmount(double returnTotalAmount) {
		this.returnTotalAmount = returnTotalAmount;
	}

	public double getReturnTotalAmountWithTax() {
		return returnTotalAmountWithTax;
	}

	public void setReturnTotalAmountWithTax(double returnTotalAmountWithTax) {
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
	}

	public Date getReturnOrderProductDateTime() {
		return returnOrderProductDateTime;
	}

	public void setReturnOrderProductDateTime(Date returnOrderProductDateTime) {
		this.returnOrderProductDateTime = returnOrderProductDateTime;
	}

	public String getReIssueStatus() {
		return reIssueStatus;
	}

	public void setReIssueStatus(String reIssueStatus) {
		this.reIssueStatus = reIssueStatus;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@Override
	public String toString() {
		return "ReturnOrderReportModel [returnOrderProductId=" + returnOrderProductId + ", orderId=" + orderId
				+ ", shopName=" + shopName + ", employeeDetailsId=" + employeeDetailsId + ", employeeName="
				+ employeeName + ", areaName=" + areaName + ", regionName=" + regionName + ", cityName=" + cityName
				+ ", returnTotalQuantity=" + returnTotalQuantity + ", returnTotalAmount=" + returnTotalAmount
				+ ", returnTotalAmountWithTax=" + returnTotalAmountWithTax + ", returnOrderProductDateTime="
				+ returnOrderProductDateTime + ", reIssueStatus=" + reIssueStatus + ", businessAddress="
				+ businessAddress + ", orderDate=" + orderDate + "]";
	}

}
