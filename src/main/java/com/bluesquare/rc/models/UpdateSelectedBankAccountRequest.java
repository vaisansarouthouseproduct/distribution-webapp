package com.bluesquare.rc.models;

public class UpdateSelectedBankAccountRequest {
	public long bankAccountId;

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	@Override
	public String toString() {
		return "UpdateSelectedBankAccountRequest [bankAccountId=" + bankAccountId + "]";
	}

	
	
}
