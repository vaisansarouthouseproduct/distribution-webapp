package com.bluesquare.rc.models;

import java.util.List;

public class InventoryDetailsResponseModel {
	InventoryReportView inventoryReportView;
	List<InventoryDetailsModel> inventoryDetailsModelList;
	double totalBeforeAllDiscount = 0, totalProductsAllDiscount = 0;

	public InventoryDetailsResponseModel(InventoryReportView inventoryReportView,
			List<InventoryDetailsModel> inventoryDetailsModelList, double totalBeforeAllDiscount,
			double totalProductsAllDiscount) {
		super();
		this.inventoryReportView = inventoryReportView;
		this.inventoryDetailsModelList = inventoryDetailsModelList;
		this.totalBeforeAllDiscount = totalBeforeAllDiscount;
		this.totalProductsAllDiscount = totalProductsAllDiscount;
	}

	public InventoryReportView getInventoryReportView() {
		return inventoryReportView;
	}

	public void setInventoryReportView(InventoryReportView inventoryReportView) {
		this.inventoryReportView = inventoryReportView;
	}

	public List<InventoryDetailsModel> getInventoryDetailsModelList() {
		return inventoryDetailsModelList;
	}

	public void setInventoryDetailsModelList(List<InventoryDetailsModel> inventoryDetailsModelList) {
		this.inventoryDetailsModelList = inventoryDetailsModelList;
	}

	public double getTotalBeforeAllDiscount() {
		return totalBeforeAllDiscount;
	}

	public void setTotalBeforeAllDiscount(double totalBeforeAllDiscount) {
		this.totalBeforeAllDiscount = totalBeforeAllDiscount;
	}

	public double getTotalProductsAllDiscount() {
		return totalProductsAllDiscount;
	}

	public void setTotalProductsAllDiscount(double totalProductsAllDiscount) {
		this.totalProductsAllDiscount = totalProductsAllDiscount;
	}

	@Override
	public String toString() {
		return "InventoryDetailsResponseModel [inventoryReportView=" + inventoryReportView
				+ ", inventoryDetailsModelList=" + inventoryDetailsModelList + ", totalBeforeAllDiscount="
				+ totalBeforeAllDiscount + ", totalProductsAllDiscount=" + totalProductsAllDiscount + "]";
	}

}
