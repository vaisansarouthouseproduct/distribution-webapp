package com.bluesquare.rc.models;

public class MeetingScheduleModel {

	public long srno;
	public long employeeDetailsId;
	public String employeeName;
	public String departmentName;
	public boolean meetingScheduled;

	public MeetingScheduleModel(long srno, long employeeDetailsId, String employeeName, String departmentName,
			boolean isMeetingScheduled) {
		super();
		this.srno = srno;
		this.employeeDetailsId = employeeDetailsId;
		this.employeeName = employeeName;
		this.departmentName = departmentName;
		this.meetingScheduled = isMeetingScheduled;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public boolean getMeetingScheduled() {
		return meetingScheduled;
	}

	public void setMeetingScheduled(boolean meetingScheduled) {
		this.meetingScheduled = meetingScheduled;
	}

	@Override
	public String toString() {
		return "MeetingScheduleModel [srno=" + srno + ", employeeDetailsId=" + employeeDetailsId + ", employeeName="
				+ employeeName + ", departmentName=" + departmentName + ", meetingScheduled=" + meetingScheduled + "]";
	}

	

}
