/**
 * 
 */
package com.bluesquare.rc.models;

/**
 * @author aNKIT
 *
 */
public class CategoryWiseAmountForBill {

	private String hsnCode;
	private String taxableValue;
	private String cgstPercentage;
	private String cgstRate;
	private String igstPercentage;
	private String igstRate;
	private String sgstPercentage;
	private String sgstRate;
	public CategoryWiseAmountForBill(String hsnCode, String taxableValue, String cgstPercentage, String cgstRate,
			String igstPercentage, String igstRate, String sgstPercentage, String sgstRate) {
		super();
		this.hsnCode = hsnCode;
		this.taxableValue = taxableValue;
		this.cgstPercentage = cgstPercentage;
		this.cgstRate = cgstRate;
		this.igstPercentage = igstPercentage;
		this.igstRate = igstRate;
		this.sgstPercentage = sgstPercentage;
		this.sgstRate = sgstRate;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getTaxableValue() {
		return taxableValue;
	}
	public void setTaxableValue(String taxableValue) {
		this.taxableValue = taxableValue;
	}
	public String getCgstPercentage() {
		return cgstPercentage;
	}
	public void setCgstPercentage(String cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}
	public String getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(String cgstRate) {
		this.cgstRate = cgstRate;
	}
	public String getIgstPercentage() {
		return igstPercentage;
	}
	public void setIgstPercentage(String igstPercentage) {
		this.igstPercentage = igstPercentage;
	}
	public String getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(String igstRate) {
		this.igstRate = igstRate;
	}
	public String getSgstPercentage() {
		return sgstPercentage;
	}
	public void setSgstPercentage(String sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}
	public String getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(String sgstRate) {
		this.sgstRate = sgstRate;
	}
	@Override
	public String toString() {
		return "CategoryWiseAmountForBill [hsnCode=" + hsnCode + ", taxableValue=" + taxableValue + ", cgstPercentage="
				+ cgstPercentage + ", cgstRate=" + cgstRate + ", igstPercentage=" + igstPercentage + ", igstRate="
				+ igstRate + ", sgstPercentage=" + sgstPercentage + ", sgstRate=" + sgstRate + "]";
	}
	
}
