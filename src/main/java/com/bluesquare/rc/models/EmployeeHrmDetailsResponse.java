package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.rest.models.BaseDomain;

public class EmployeeHrmDetailsResponse extends BaseDomain{
	
	private EmployeeDetails employeeDetails;
	private long totalLeaves;
	private double payableAmt;
	
	
	public EmployeeHrmDetailsResponse(EmployeeDetails employeeDetails, long totalLeaves, double payableAmt) {
		super();
		this.employeeDetails = employeeDetails;
		this.totalLeaves = totalLeaves;
		this.payableAmt = payableAmt;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public long getTotalLeaves() {
		return totalLeaves;
	}
	public void setTotalLeaves(long totalLeaves) {
		this.totalLeaves = totalLeaves;
	}
	public double getPayableAmt() {
		return payableAmt;
	}
	public void setPayableAmt(double payableAmt) {
		this.payableAmt = payableAmt;
	}
	@Override
	public String toString() {
		return "EmployeeHrmDetailsResponse [employeeDetails=" + employeeDetails + ", totalLeaves=" + totalLeaves
				+ ", payableAmt=" + payableAmt + "]";
	}
	
	

}
