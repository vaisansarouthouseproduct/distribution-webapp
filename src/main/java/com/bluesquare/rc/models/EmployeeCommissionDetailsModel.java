package com.bluesquare.rc.models;

public class EmployeeCommissionDetailsModel {
	String commissionName;
	String saleValueType;
	double saleValue;
	double commissionValue;
	long commissionAssignId;

	public EmployeeCommissionDetailsModel(String commissionName, String saleValueType, double saleValue,
			double commissionValue, long commissionAssignId) {
		super();
		this.commissionName = commissionName;
		this.saleValueType = saleValueType;
		this.saleValue = saleValue;
		this.commissionValue = commissionValue;
		this.commissionAssignId = commissionAssignId;
	}

	public String getCommissionName() {
		return commissionName;
	}

	public void setCommissionName(String commissionName) {
		this.commissionName = commissionName;
	}

	public String getSaleValueType() {
		return saleValueType;
	}

	public void setSaleValueType(String saleValueType) {
		this.saleValueType = saleValueType;
	}

	public double getSaleValue() {
		return saleValue;
	}

	public void setSaleValue(double saleValue) {
		this.saleValue = saleValue;
	}

	public double getCommissionValue() {
		return commissionValue;
	}

	public void setCommissionValue(double commissionValue) {
		this.commissionValue = commissionValue;
	}

	public long getCommissionAssignId() {
		return commissionAssignId;
	}

	public void setCommissionAssignId(long commissionAssignId) {
		this.commissionAssignId = commissionAssignId;
	}

	@Override
	public String toString() {
		return "EmployeeCommissionDetailsModel [commissionName=" + commissionName + ", saleValueType=" + saleValueType
				+ ", saleValue=" + saleValue + ", commissionValue=" + commissionValue + ", commissionAssignId="
				+ commissionAssignId + "]";
	}

}
