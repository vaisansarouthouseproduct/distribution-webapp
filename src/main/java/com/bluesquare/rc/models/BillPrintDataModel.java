/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.BankDetailsForInvoice;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Employee;

/**
 * @author aNKIT
 *
 */
public class BillPrintDataModel {

	private String orderNumber;
	private String invoiceNumber;
	private String creditNoteNumber;
	private String returnDate;
	private String orderDate;
	private String transporterName;
	private String vehicalNumber;
	private String transporterGstNumber;
	private String docketNo;
	private List<String> addressLineList;
	private String comment;

	private BusinessName businessName;
	private Employee employeeGKCounterOrder;
	private String customerName;
	private String customerMobileNumber;
	private String customerGstNumber;

	private String deliveryDate;

	private List<ProductListForBill> productListForBill;

	private String cGSTAmount;
	private String iGSTAmount;
	private String sGSTAmount;

	private String roundOffAmount;

	private String totalAmountWithoutTax;

	private String totalQuantity;
	private String totalAmountWithTax;
	private String totalAmountOfDiscount;
	private String totalPercentageOfDiscount;
	private String totalAmountWithTaxWithDiscNonRoundOff;
	private String totalAmountWithTaxWithDisc;
	private String totalAmountWithTaxRoundOff;
	private String totalAmountWithTaxInWord;

	private List<CategoryWiseAmountForBill> categoryWiseAmountForBills;

	private String totalAmount;
	private String taxAmountInWord;

	private String totalCGSTAmount;
	private String totalIGSTAmount;
	private String totalSGSTAmount;

	String totalAmountDisc;
	String totalCGSTAmountDisc;
	String totalIGSTAmountDisc;
	String totalSGSTAmountDisc;
	String netTotalAmount;
	String netTotalCGSTAmount;
	String netTotalIGSTAmount;
	String netTotalSGSTAmount;

	double trasportationCharge;

	public BillPrintDataModel() {
		// TODO Auto-generated constructor stub
	}

	// Order by BusinessName
	public BillPrintDataModel(String orderNumber, String invoiceNumber, String creditNoteNumber, String returnDate,
			String orderDate, List<String> addressLineList, BusinessName businessName, Employee employeeGKCounterOrder,
			String deliveryDate, List<ProductListForBill> productListForBill, String cGSTAmount, String iGSTAmount,
			String sGSTAmount, String roundOffAmount, String totalAmountWithoutTax, String totalQuantity,
			String totalAmountWithTax, String totalAmountOfDiscount, String totalPercentageOfDiscount,
			String totalAmountWithTaxWithDiscNonRoundOff, String totalAmountWithTaxWithDisc,
			String totalAmountWithTaxRoundOff, String totalAmountWithTaxInWord,
			List<CategoryWiseAmountForBill> categoryWiseAmountForBills, String totalAmount, String taxAmountInWord,
			String totalCGSTAmount, String totalIGSTAmount, String totalSGSTAmount, String totalAmountDisc,
			String totalCGSTAmountDisc, String totalIGSTAmountDisc, String totalSGSTAmountDisc, String netTotalAmount,
			String netTotalCGSTAmount, String netTotalIGSTAmount, String netTotalSGSTAmount, String transporterName,
			String vehicalNumber, String transporterGstNumber, String docketNo, String comment,
			double trasportationCharge) {
		super();
		this.orderNumber = orderNumber;
		this.invoiceNumber = invoiceNumber;
		this.creditNoteNumber = creditNoteNumber;
		this.returnDate = returnDate;
		this.orderDate = orderDate;
		this.addressLineList = addressLineList;
		this.businessName = businessName;
		this.employeeGKCounterOrder = employeeGKCounterOrder;
		this.deliveryDate = deliveryDate;
		this.productListForBill = productListForBill;
		this.cGSTAmount = cGSTAmount;
		this.iGSTAmount = iGSTAmount;
		this.sGSTAmount = sGSTAmount;
		this.roundOffAmount = roundOffAmount;
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalQuantity = totalQuantity;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalAmountOfDiscount = totalAmountOfDiscount;
		this.totalPercentageOfDiscount = totalPercentageOfDiscount;
		this.totalAmountWithTaxWithDiscNonRoundOff = totalAmountWithTaxWithDiscNonRoundOff;
		this.totalAmountWithTaxWithDisc = totalAmountWithTaxWithDisc;
		this.totalAmountWithTaxRoundOff = totalAmountWithTaxRoundOff;
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
		this.totalAmount = totalAmount;
		this.taxAmountInWord = taxAmountInWord;
		this.totalCGSTAmount = totalCGSTAmount;
		this.totalIGSTAmount = totalIGSTAmount;
		this.totalSGSTAmount = totalSGSTAmount;
		this.totalAmountDisc = totalAmountDisc;
		this.totalCGSTAmountDisc = totalCGSTAmountDisc;
		this.totalIGSTAmountDisc = totalIGSTAmountDisc;
		this.totalSGSTAmountDisc = totalSGSTAmountDisc;
		this.netTotalAmount = netTotalAmount;
		this.netTotalCGSTAmount = netTotalCGSTAmount;
		this.netTotalIGSTAmount = netTotalIGSTAmount;
		this.netTotalSGSTAmount = netTotalSGSTAmount;
		this.transporterName = transporterName;
		this.vehicalNumber = vehicalNumber;
		this.transporterGstNumber = transporterGstNumber;
		this.docketNo = docketNo;
		this.comment = comment;
		this.trasportationCharge = trasportationCharge;
	}

	// order by counter customer
	public BillPrintDataModel(String orderNumber, String invoiceNumber, String creditNoteNumber, String returnDate,
			String orderDate, List<String> addressLineList, String customerName, String customerMobileNumber,
			String customerGstNumber, Employee employeeGKCounterOrder, String deliveryDate,
			List<ProductListForBill> productListForBill, String cGSTAmount, String iGSTAmount, String sGSTAmount,
			String roundOffAmount, String totalAmountWithoutTax, String totalQuantity, String totalAmountWithTax,
			String totalAmountOfDiscount, String totalPercentageOfDiscount,
			String totalAmountWithTaxWithDiscNonRoundOff, String totalAmountWithTaxWithDisc,
			String totalAmountWithTaxRoundOff, String totalAmountWithTaxInWord,
			List<CategoryWiseAmountForBill> categoryWiseAmountForBills, String totalAmount, String taxAmountInWord,
			String totalCGSTAmount, String totalIGSTAmount, String totalSGSTAmount, String totalAmountDisc,
			String totalCGSTAmountDisc, String totalIGSTAmountDisc, String totalSGSTAmountDisc, String netTotalAmount,
			String netTotalCGSTAmount, String netTotalIGSTAmount, String netTotalSGSTAmount, String transporterName,
			String vehicalNumber, String transporterGstNumber, String docketNo, String comment,
			double trasportationCharge) {
		super();
		this.orderNumber = orderNumber;
		this.invoiceNumber = invoiceNumber;
		this.creditNoteNumber = creditNoteNumber;
		this.returnDate = returnDate;
		this.orderDate = orderDate;
		this.addressLineList = addressLineList;
		this.customerName = customerName;
		this.customerMobileNumber = customerMobileNumber;
		this.customerGstNumber = customerGstNumber;
		this.employeeGKCounterOrder = employeeGKCounterOrder;
		this.deliveryDate = deliveryDate;
		this.productListForBill = productListForBill;
		this.cGSTAmount = cGSTAmount;
		this.iGSTAmount = iGSTAmount;
		this.sGSTAmount = sGSTAmount;
		this.roundOffAmount = roundOffAmount;
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalQuantity = totalQuantity;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalAmountOfDiscount = totalAmountOfDiscount;
		this.totalPercentageOfDiscount = totalPercentageOfDiscount;
		this.totalAmountWithTaxWithDiscNonRoundOff = totalAmountWithTaxWithDiscNonRoundOff;
		this.totalAmountWithTaxWithDisc = totalAmountWithTaxWithDisc;
		this.totalAmountWithTaxRoundOff = totalAmountWithTaxRoundOff;
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
		this.totalAmount = totalAmount;
		this.taxAmountInWord = taxAmountInWord;
		this.totalCGSTAmount = totalCGSTAmount;
		this.totalIGSTAmount = totalIGSTAmount;
		this.totalSGSTAmount = totalSGSTAmount;
		this.totalAmountDisc = totalAmountDisc;
		this.totalCGSTAmountDisc = totalCGSTAmountDisc;
		this.totalIGSTAmountDisc = totalIGSTAmountDisc;
		this.totalSGSTAmountDisc = totalSGSTAmountDisc;
		this.netTotalAmount = netTotalAmount;
		this.netTotalCGSTAmount = netTotalCGSTAmount;
		this.netTotalIGSTAmount = netTotalIGSTAmount;
		this.netTotalSGSTAmount = netTotalSGSTAmount;
		this.transporterName = transporterName;
		this.vehicalNumber = vehicalNumber;
		this.transporterGstNumber = transporterGstNumber;
		this.docketNo = docketNo;
		this.comment = comment;
		this.trasportationCharge = trasportationCharge;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}

	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public String getVehicalNumber() {
		return vehicalNumber;
	}

	public void setVehicalNumber(String vehicalNumber) {
		this.vehicalNumber = vehicalNumber;
	}

	public String getTransporterGstNumber() {
		return transporterGstNumber;
	}

	public void setTransporterGstNumber(String transporterGstNumber) {
		this.transporterGstNumber = transporterGstNumber;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public List<String> getAddressLineList() {
		return addressLineList;
	}

	public void setAddressLineList(List<String> addressLineList) {
		this.addressLineList = addressLineList;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public Employee getEmployeeGKCounterOrder() {
		return employeeGKCounterOrder;
	}

	public void setEmployeeGKCounterOrder(Employee employeeGKCounterOrder) {
		this.employeeGKCounterOrder = employeeGKCounterOrder;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerGstNumber() {
		return customerGstNumber;
	}

	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<ProductListForBill> getProductListForBill() {
		return productListForBill;
	}

	public void setProductListForBill(List<ProductListForBill> productListForBill) {
		this.productListForBill = productListForBill;
	}

	public String getcGSTAmount() {
		return cGSTAmount;
	}

	public void setcGSTAmount(String cGSTAmount) {
		this.cGSTAmount = cGSTAmount;
	}

	public String getiGSTAmount() {
		return iGSTAmount;
	}

	public void setiGSTAmount(String iGSTAmount) {
		this.iGSTAmount = iGSTAmount;
	}

	public String getsGSTAmount() {
		return sGSTAmount;
	}

	public void setsGSTAmount(String sGSTAmount) {
		this.sGSTAmount = sGSTAmount;
	}

	public String getRoundOffAmount() {
		return roundOffAmount;
	}

	public void setRoundOffAmount(String roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}

	public String getTotalAmountWithoutTax() {
		return totalAmountWithoutTax;
	}

	public void setTotalAmountWithoutTax(String totalAmountWithoutTax) {
		this.totalAmountWithoutTax = totalAmountWithoutTax;
	}

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(String totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public String getTotalAmountOfDiscount() {
		return totalAmountOfDiscount;
	}

	public void setTotalAmountOfDiscount(String totalAmountOfDiscount) {
		this.totalAmountOfDiscount = totalAmountOfDiscount;
	}

	public String getTotalPercentageOfDiscount() {
		return totalPercentageOfDiscount;
	}

	public void setTotalPercentageOfDiscount(String totalPercentageOfDiscount) {
		this.totalPercentageOfDiscount = totalPercentageOfDiscount;
	}

	public String getTotalAmountWithTaxWithDiscNonRoundOff() {
		return totalAmountWithTaxWithDiscNonRoundOff;
	}

	public void setTotalAmountWithTaxWithDiscNonRoundOff(String totalAmountWithTaxWithDiscNonRoundOff) {
		this.totalAmountWithTaxWithDiscNonRoundOff = totalAmountWithTaxWithDiscNonRoundOff;
	}

	public String getTotalAmountWithTaxWithDisc() {
		return totalAmountWithTaxWithDisc;
	}

	public void setTotalAmountWithTaxWithDisc(String totalAmountWithTaxWithDisc) {
		this.totalAmountWithTaxWithDisc = totalAmountWithTaxWithDisc;
	}

	public String getTotalAmountWithTaxRoundOff() {
		return totalAmountWithTaxRoundOff;
	}

	public void setTotalAmountWithTaxRoundOff(String totalAmountWithTaxRoundOff) {
		this.totalAmountWithTaxRoundOff = totalAmountWithTaxRoundOff;
	}

	public String getTotalAmountWithTaxInWord() {
		return totalAmountWithTaxInWord;
	}

	public void setTotalAmountWithTaxInWord(String totalAmountWithTaxInWord) {
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
	}

	public List<CategoryWiseAmountForBill> getCategoryWiseAmountForBills() {
		return categoryWiseAmountForBills;
	}

	public void setCategoryWiseAmountForBills(List<CategoryWiseAmountForBill> categoryWiseAmountForBills) {
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTaxAmountInWord() {
		return taxAmountInWord;
	}

	public void setTaxAmountInWord(String taxAmountInWord) {
		this.taxAmountInWord = taxAmountInWord;
	}

	public String getTotalCGSTAmount() {
		return totalCGSTAmount;
	}

	public void setTotalCGSTAmount(String totalCGSTAmount) {
		this.totalCGSTAmount = totalCGSTAmount;
	}

	public String getTotalIGSTAmount() {
		return totalIGSTAmount;
	}

	public void setTotalIGSTAmount(String totalIGSTAmount) {
		this.totalIGSTAmount = totalIGSTAmount;
	}

	public String getTotalSGSTAmount() {
		return totalSGSTAmount;
	}

	public void setTotalSGSTAmount(String totalSGSTAmount) {
		this.totalSGSTAmount = totalSGSTAmount;
	}

	public String getTotalAmountDisc() {
		return totalAmountDisc;
	}

	public void setTotalAmountDisc(String totalAmountDisc) {
		this.totalAmountDisc = totalAmountDisc;
	}

	public String getTotalCGSTAmountDisc() {
		return totalCGSTAmountDisc;
	}

	public void setTotalCGSTAmountDisc(String totalCGSTAmountDisc) {
		this.totalCGSTAmountDisc = totalCGSTAmountDisc;
	}

	public String getTotalIGSTAmountDisc() {
		return totalIGSTAmountDisc;
	}

	public void setTotalIGSTAmountDisc(String totalIGSTAmountDisc) {
		this.totalIGSTAmountDisc = totalIGSTAmountDisc;
	}

	public String getTotalSGSTAmountDisc() {
		return totalSGSTAmountDisc;
	}

	public void setTotalSGSTAmountDisc(String totalSGSTAmountDisc) {
		this.totalSGSTAmountDisc = totalSGSTAmountDisc;
	}

	public String getNetTotalAmount() {
		return netTotalAmount;
	}

	public void setNetTotalAmount(String netTotalAmount) {
		this.netTotalAmount = netTotalAmount;
	}

	public String getNetTotalCGSTAmount() {
		return netTotalCGSTAmount;
	}

	public void setNetTotalCGSTAmount(String netTotalCGSTAmount) {
		this.netTotalCGSTAmount = netTotalCGSTAmount;
	}

	public String getNetTotalIGSTAmount() {
		return netTotalIGSTAmount;
	}

	public void setNetTotalIGSTAmount(String netTotalIGSTAmount) {
		this.netTotalIGSTAmount = netTotalIGSTAmount;
	}

	public String getNetTotalSGSTAmount() {
		return netTotalSGSTAmount;
	}

	public void setNetTotalSGSTAmount(String netTotalSGSTAmount) {
		this.netTotalSGSTAmount = netTotalSGSTAmount;
	}

	public double getTrasportationCharge() {
		return trasportationCharge;
	}

	public void setTrasportationCharge(double trasportationCharge) {
		this.trasportationCharge = trasportationCharge;
	}

	@Override
	public String toString() {
		return "BillPrintDataModel [orderNumber=" + orderNumber + ", invoiceNumber=" + invoiceNumber
				+ ", creditNoteNumber=" + creditNoteNumber + ", returnDate=" + returnDate + ", orderDate=" + orderDate
				+ ", transporterName=" + transporterName + ", vehicalNumber=" + vehicalNumber
				+ ", transporterGstNumber=" + transporterGstNumber + ", docketNo=" + docketNo + ", addressLineList="
				+ addressLineList + ", comment=" + comment + ", businessName=" + businessName
				+ ", employeeGKCounterOrder=" + employeeGKCounterOrder + ", customerName=" + customerName
				+ ", customerMobileNumber=" + customerMobileNumber + ", customerGstNumber=" + customerGstNumber
				+ ", deliveryDate=" + deliveryDate + ", productListForBill=" + productListForBill + ", cGSTAmount="
				+ cGSTAmount + ", iGSTAmount=" + iGSTAmount + ", sGSTAmount=" + sGSTAmount + ", roundOffAmount="
				+ roundOffAmount + ", totalAmountWithoutTax=" + totalAmountWithoutTax + ", totalQuantity="
				+ totalQuantity + ", totalAmountWithTax=" + totalAmountWithTax + ", totalAmountOfDiscount="
				+ totalAmountOfDiscount + ", totalPercentageOfDiscount=" + totalPercentageOfDiscount
				+ ", totalAmountWithTaxWithDiscNonRoundOff=" + totalAmountWithTaxWithDiscNonRoundOff
				+ ", totalAmountWithTaxWithDisc=" + totalAmountWithTaxWithDisc + ", totalAmountWithTaxRoundOff="
				+ totalAmountWithTaxRoundOff + ", totalAmountWithTaxInWord=" + totalAmountWithTaxInWord
				+ ", categoryWiseAmountForBills=" + categoryWiseAmountForBills + ", totalAmount=" + totalAmount
				+ ", taxAmountInWord=" + taxAmountInWord + ", totalCGSTAmount=" + totalCGSTAmount + ", totalIGSTAmount="
				+ totalIGSTAmount + ", totalSGSTAmount=" + totalSGSTAmount + ", totalAmountDisc=" + totalAmountDisc
				+ ", totalCGSTAmountDisc=" + totalCGSTAmountDisc + ", totalIGSTAmountDisc=" + totalIGSTAmountDisc
				+ ", totalSGSTAmountDisc=" + totalSGSTAmountDisc + ", netTotalAmount=" + netTotalAmount
				+ ", netTotalCGSTAmount=" + netTotalCGSTAmount + ", netTotalIGSTAmount=" + netTotalIGSTAmount
				+ ", netTotalSGSTAmount=" + netTotalSGSTAmount + ", trasportationCharge=" + trasportationCharge + "]";
	}

}
