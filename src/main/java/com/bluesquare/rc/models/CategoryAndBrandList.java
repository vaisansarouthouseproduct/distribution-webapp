package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.SupplierModel;

public class CategoryAndBrandList {

	public List<CategoriesModel> categoryList;
	public List<BrandModel> brandList;
	public List<SupplierModel> supplierList;
	public CategoryAndBrandList(List<CategoriesModel> categoryList, List<BrandModel> brandList, List<SupplierModel> supplierList) {
		super();
		this.categoryList = categoryList;
		this.brandList = brandList;
		this.supplierList = supplierList;
	}
	public List<CategoriesModel> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<CategoriesModel> categoryList) {
		this.categoryList = categoryList;
	}
	public List<BrandModel> getBrandList() {
		return brandList;
	}
	public void setBrandList(List<BrandModel> brandList) {
		this.brandList = brandList;
	}
	public List<SupplierModel> getSupplierList() {
		return supplierList;
	}
	public void setSupplierList(List<SupplierModel> supplierList) {
		this.supplierList = supplierList;
	}
	@Override
	public String toString() {
		return "CategoryAndBrandList [categoryList=" + categoryList + ", brandList=" + brandList + ", supplierList="
				+ supplierList + "]";
	}
	


	
	
}
