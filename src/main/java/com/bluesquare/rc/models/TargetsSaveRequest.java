package com.bluesquare.rc.models;

import java.util.Arrays;
import java.util.List;

public class TargetsSaveRequest {

	public List<String[]> targetWithValue;
	public String targetPeriod;
	public String employeeIds[];
	public long targetAssignedId;
	public String updateInRegular;

	public List<String[]> getTargetWithValue() {
		return targetWithValue;
	}

	public void setTargetWithValue(List<String[]> targetWithValue) {
		this.targetWithValue = targetWithValue;
	}

	public String getTargetPeriod() {
		return targetPeriod;
	}

	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}

	public String[] getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(String[] employeeIds) {
		this.employeeIds = employeeIds;
	}

	public long getTargetAssignedId() {
		return targetAssignedId;
	}

	public void setTargetAssignedId(long targetAssignedId) {
		this.targetAssignedId = targetAssignedId;
	}

	public String getUpdateInRegular() {
		return updateInRegular;
	}

	public void setUpdateInRegular(String updateInRegular) {
		this.updateInRegular = updateInRegular;
	}

	@Override
	public String toString() {
		return "TargetsSaveRequest [targetWithValue=" + targetWithValue + ", targetPeriod=" + targetPeriod
				+ ", employeeIds=" + Arrays.toString(employeeIds) + ", targetAssignedId=" + targetAssignedId
				+ ", updateInRegular=" + updateInRegular + "]";
	}
}
