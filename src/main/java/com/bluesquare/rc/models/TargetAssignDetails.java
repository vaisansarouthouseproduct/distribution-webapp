package com.bluesquare.rc.models;

import java.util.List;

public class TargetAssignDetails {

	long srno;
	long targetAssignId;
	String employeeName;
	String departmentName;
	List<EmployeeCommissionDetails> employeeCommissionDetails;

	public TargetAssignDetails(long srno, long targetAssignId, String employeeName, String departmentName,
			List<EmployeeCommissionDetails> employeeCommissionDetails) {
		super();
		this.srno = srno;
		this.targetAssignId = targetAssignId;
		this.employeeName = employeeName;
		this.departmentName = departmentName;
		this.employeeCommissionDetails = employeeCommissionDetails;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getTargetAssignId() {
		return targetAssignId;
	}

	public void setTargetAssignId(long targetAssignId) {
		this.targetAssignId = targetAssignId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<EmployeeCommissionDetails> getEmployeeCommissionDetails() {
		return employeeCommissionDetails;
	}

	public void setEmployeeCommissionDetails(List<EmployeeCommissionDetails> employeeCommissionDetails) {
		this.employeeCommissionDetails = employeeCommissionDetails;
	}

	@Override
	public String toString() {
		return "TargetAssignDetails [srno=" + srno + ", targetAssignId=" + targetAssignId + ", employeeName="
				+ employeeName + ", departmentName=" + departmentName + ", employeeCommissionDetails="
				+ employeeCommissionDetails + "]";
	}

}
