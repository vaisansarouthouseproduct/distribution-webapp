package com.bluesquare.rc.models;

public class TaxCalculationManageInventory {

	private double igstPercentage;
	private double igstRate;
	private double cgstPercentage;
	private double cgstRate;
	private double sgstPercentage;
	private double sgstRate;
	private double tax;
	private double totalTaxAmount;
	public TaxCalculationManageInventory(double igstPercentage, double igstRate, double cgstPercentage, double cgstRate,
			double sgstPercentage, double sgstRate, double tax, double totalTaxAmount) {
		super();
		this.igstPercentage = igstPercentage;
		this.igstRate = igstRate;
		this.cgstPercentage = cgstPercentage;
		this.cgstRate = cgstRate;
		this.sgstPercentage = sgstPercentage;
		this.sgstRate = sgstRate;
		this.tax = tax;
		this.totalTaxAmount = totalTaxAmount;
	}
	public double getIgstPercentage() {
		return igstPercentage;
	}
	public void setIgstPercentage(double igstPercentage) {
		this.igstPercentage = igstPercentage;
	}
	public double getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(double igstRate) {
		this.igstRate = igstRate;
	}
	public double getCgstPercentage() {
		return cgstPercentage;
	}
	public void setCgstPercentage(double cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}
	public double getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(double cgstRate) {
		this.cgstRate = cgstRate;
	}
	public double getSgstPercentage() {
		return sgstPercentage;
	}
	public void setSgstPercentage(double sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}
	public double getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(double sgstRate) {
		this.sgstRate = sgstRate;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getTotalTaxAmount() {
		return totalTaxAmount;
	}
	public void setTotalTaxAmount(double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}
	@Override
	public String toString() {
		return "TaxCalculationManageInventory [igstPercentage=" + igstPercentage + ", igstRate=" + igstRate
				+ ", cgstPercentage=" + cgstPercentage + ", cgstRate=" + cgstRate + ", sgstPercentage=" + sgstPercentage
				+ ", sgstRate=" + sgstRate + ", tax=" + tax + ", totalTaxAmount=" + totalTaxAmount + "]";
	}
	
	
}
