package com.bluesquare.rc.models;

public class EmployeeCommissionDetails {

	long commissionAssignId;
	String commissionAssignName;
	double targetMinimumValue;

	public EmployeeCommissionDetails(long commissionAssignId, String commissionAssignName, double targetMinimumValue) {
		super();
		this.commissionAssignId = commissionAssignId;
		this.commissionAssignName = commissionAssignName;
		this.targetMinimumValue = targetMinimumValue;
	}

	public long getCommissionAssignId() {
		return commissionAssignId;
	}

	public void setCommissionAssignId(long commissionAssignId) {
		this.commissionAssignId = commissionAssignId;
	}

	public String getCommissionAssignName() {
		return commissionAssignName;
	}

	public void setCommissionAssignName(String commissionAssignName) {
		this.commissionAssignName = commissionAssignName;
	}

	public double getTargetMinimumValue() {
		return targetMinimumValue;
	}

	public void setTargetMinimumValue(double targetMinimumValue) {
		this.targetMinimumValue = targetMinimumValue;
	}

	@Override
	public String toString() {
		return "EmployeeCommissionDetails [commissionAssignId=" + commissionAssignId + ", commissionAssignName="
				+ commissionAssignName + ", targetMinimumValue=" + targetMinimumValue + "]";
	}

}
