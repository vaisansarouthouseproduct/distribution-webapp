package com.bluesquare.rc.models;

import java.util.Date;

public class PermanentDamageReport {

	long srno;
	long permanentDamageMonthWiseId;
	String productName;
	long totalQuantityDamage;
	Date damageDate;

	public PermanentDamageReport(long srno, long permanentDamageMonthWiseId, String productName,
			long totalQuantityDamage, Date damageDate) {
		super();
		this.srno = srno;
		this.permanentDamageMonthWiseId = permanentDamageMonthWiseId;
		this.productName = productName;
		this.totalQuantityDamage = totalQuantityDamage;
		this.damageDate = damageDate;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getPermanentDamageMonthWiseId() {
		return permanentDamageMonthWiseId;
	}

	public void setPermanentDamageMonthWiseId(long permanentDamageMonthWiseId) {
		this.permanentDamageMonthWiseId = permanentDamageMonthWiseId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getTotalQuantityDamage() {
		return totalQuantityDamage;
	}

	public void setTotalQuantityDamage(long totalQuantityDamage) {
		this.totalQuantityDamage = totalQuantityDamage;
	}

	public Date getDamageDate() {
		return damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	@Override
	public String toString() {
		return "PermanentDamageReport [srno=" + srno + ", permanentDamageMonthWiseId=" + permanentDamageMonthWiseId
				+ ", productName=" + productName + ", totalQuantityDamage=" + totalQuantityDamage + ", damageDate="
				+ damageDate + "]";
	}

}
