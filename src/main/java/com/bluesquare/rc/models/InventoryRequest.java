package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Inventory;

public class InventoryRequest {

	Inventory inventory;
	List<InventoryDetailsModel> inventoryDetailsList;

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public List<InventoryDetailsModel> getInventoryDetailsList() {
		return inventoryDetailsList;
	}

	public void setInventoryDetailsList(List<InventoryDetailsModel> inventoryDetailsList) {
		this.inventoryDetailsList = inventoryDetailsList;
	}

	@Override
	public String toString() {
		return "InventoryRequest [inventory=" + inventory + ", inventoryDetailsList=" + inventoryDetailsList + "]";
	}

}
