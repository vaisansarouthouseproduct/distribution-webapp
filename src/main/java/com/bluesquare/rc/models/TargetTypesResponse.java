package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.TargetType;
import com.bluesquare.rc.rest.models.BaseDomain;

public class TargetTypesResponse extends BaseDomain{

	public List<TargetType> targetTypeList;

	public List<TargetType> getTargetTypeList() {
		return targetTypeList;
	}

	public void setTargetTypeList(List<TargetType> targetTypeList) {
		this.targetTypeList = targetTypeList;
	}

	@Override
	public String toString() {
		return "TargetTypesResponse [targetTypeList=" + targetTypeList + "]";
	}
	
	
}
