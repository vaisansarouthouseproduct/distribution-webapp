package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.responseEntities.EmployeeDetailsModel;

public class EmployeeLastLocation {

	private EmployeeDetailsModel employeeDetails;
	private String lat;
	private String lng;
	private String address;
	private List<EmployeeRouteListModel> employeeRouteListModelList;
	public EmployeeLastLocation(EmployeeDetailsModel employeeDetails, String lat, String lng, String address,
			List<EmployeeRouteListModel> employeeRouteListModelList) {
		super();
		this.employeeDetails = employeeDetails;
		this.lat = lat;
		this.lng = lng;
		this.address = address;
		this.employeeRouteListModelList = employeeRouteListModelList;
	}
	public EmployeeDetailsModel getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetailsModel employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<EmployeeRouteListModel> getEmployeeRouteListModelList() {
		return employeeRouteListModelList;
	}
	public void setEmployeeRouteListModelList(List<EmployeeRouteListModel> employeeRouteListModelList) {
		this.employeeRouteListModelList = employeeRouteListModelList;
	}
	@Override
	public String toString() {
		return "EmployeeLastLocation [employeeDetails=" + employeeDetails + ", lat=" + lat + ", lng=" + lng
				+ ", address=" + address + ", employeeRouteListModelList=" + employeeRouteListModelList + "]";
	}
	
	


}