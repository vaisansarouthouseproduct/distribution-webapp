package com.bluesquare.rc.models;

public class CheckBankAccountNumberRequest {
	public String accountNumber;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override
	public String toString() {
		return "CheckBankAccountNumberRequest [accountNumber=" + accountNumber + "]";
	}
	
}
