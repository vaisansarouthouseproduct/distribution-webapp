package com.bluesquare.rc.models;

import java.util.Date;

public class EmployeeHolidayList {

	private long srno;
	private  long employeeHolidayId;
	private long noOfHolidays;
	private String typeOfLeave;
	private double amountDeduct;
	private Date date;
	private Date dateFrom;
	private Date dateTo;	
	private String reason;
	public EmployeeHolidayList(long srno, long employeeHolidayId, long noOfHolidays, String typeOfLeave,
			double amountDeduct, Date date, Date dateFrom, Date dateTo, String reason) {
		super();
		this.srno = srno;
		this.employeeHolidayId = employeeHolidayId;
		this.noOfHolidays = noOfHolidays;
		this.typeOfLeave = typeOfLeave;
		this.amountDeduct = amountDeduct;
		this.date = date;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.reason = reason;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getEmployeeHolidayId() {
		return employeeHolidayId;
	}
	public void setEmployeeHolidayId(long employeeHolidayId) {
		this.employeeHolidayId = employeeHolidayId;
	}
	public long getNoOfHolidays() {
		return noOfHolidays;
	}
	public void setNoOfHolidays(long noOfHolidays) {
		this.noOfHolidays = noOfHolidays;
	}
	public String getTypeOfLeave() {
		return typeOfLeave;
	}
	public void setTypeOfLeave(String typeOfLeave) {
		this.typeOfLeave = typeOfLeave;
	}
	public double getAmountDeduct() {
		return amountDeduct;
	}
	public void setAmountDeduct(double amountDeduct) {
		this.amountDeduct = amountDeduct;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "EmployeeHolidayList [srno=" + srno + ", employeeHolidayId=" + employeeHolidayId + ", noOfHolidays="
				+ noOfHolidays + ", typeOfLeave=" + typeOfLeave + ", amountDeduct=" + amountDeduct + ", date=" + date
				+ ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", reason=" + reason + "]";
	}
	
	
}
