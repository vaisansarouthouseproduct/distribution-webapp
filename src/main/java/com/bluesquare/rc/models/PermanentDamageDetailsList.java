package com.bluesquare.rc.models;

import java.util.Date;

public class PermanentDamageDetailsList {
	long srno;
	long damageQuantity;
	String reason;
	String damageFrom;
	Date damageDate;

	public PermanentDamageDetailsList(long srno, long damageQuantity, String reason, String damageFrom,
			Date damageDate) {
		super();
		this.srno = srno;
		this.damageQuantity = damageQuantity;
		this.reason = reason;
		this.damageFrom = damageFrom;
		this.damageDate = damageDate;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDamageFrom() {
		return damageFrom;
	}

	public void setDamageFrom(String damageFrom) {
		this.damageFrom = damageFrom;
	}

	public Date getDamageDate() {
		return damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	@Override
	public String toString() {
		return "PermanentDamageDetailsList [srno=" + srno + ", damageQuantity=" + damageQuantity + ", reason=" + reason
				+ ", damageFrom=" + damageFrom + ", damageDate=" + damageDate + "]";
	}

}
