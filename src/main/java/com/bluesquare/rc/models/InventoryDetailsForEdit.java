/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.OrderUsedProduct;

/**
 * @author aNKIT
 *
 */
public class InventoryDetailsForEdit {
	private double totalAmountWithoutTax;
	private double totalAmountWithTax;
	private OrderUsedProduct orderUsedProduct;
	private long quantity;

	private double unitPrice;
	private double mrp;
	private double totalAmountBeforeDiscount;
	private double discountAmount;
	private double discountPercentage;
	private String discountType;
	private String discountOnMRP;

	public InventoryDetailsForEdit(double totalAmountWithoutTax, double totalAmountWithTax,
			OrderUsedProduct orderUsedProduct, long quantity, double unitPrice, double mrp,
			double totalAmountBeforeDiscount, double discountAmount, double discountPercentage, String discountType,String discountOnMRP) {
		super();
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalAmountWithTax = totalAmountWithTax;
		this.orderUsedProduct = orderUsedProduct;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.mrp = mrp;
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
		this.discountAmount = discountAmount;
		this.discountPercentage = discountPercentage;
		this.discountType = discountType;
		this.discountOnMRP=discountOnMRP;
	}

	public double getTotalAmountWithoutTax() {
		return totalAmountWithoutTax;
	}

	public void setTotalAmountWithoutTax(double totalAmountWithoutTax) {
		this.totalAmountWithoutTax = totalAmountWithoutTax;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public OrderUsedProduct getOrderUsedProduct() {
		return orderUsedProduct;
	}

	public void setOrderUsedProduct(OrderUsedProduct orderUsedProduct) {
		this.orderUsedProduct = orderUsedProduct;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getTotalAmountBeforeDiscount() {
		return totalAmountBeforeDiscount;
	}

	public void setTotalAmountBeforeDiscount(double totalAmountBeforeDiscount) {
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountOnMRP() {
		return discountOnMRP;
	}

	public void setDiscountOnMRP(String discountOnMRP) {
		this.discountOnMRP = discountOnMRP;
	}

	@Override
	public String toString() {
		return "InventoryDetailsForEdit [totalAmountWithoutTax=" + totalAmountWithoutTax + ", totalAmountWithTax="
				+ totalAmountWithTax + ", orderUsedProduct=" + orderUsedProduct + ", quantity=" + quantity
				+ ", unitPrice=" + unitPrice + ", mrp=" + mrp + ", totalAmountBeforeDiscount="
				+ totalAmountBeforeDiscount + ", discountAmount=" + discountAmount + ", discountPercentage="
				+ discountPercentage + ", discountType=" + discountType + ", discountOnMRP=" + discountOnMRP + "]";
	}

	

	// js needed for process
	// [productId,quantity,unitPrice,mrp,taxableAmountBeforeDiscount,totalAmountBeforeDiscount,
	// discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]

}
