package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.BankAccountTempTransactionModel;
import com.bluesquare.rc.responseEntities.BankAccountTransactionModel;
import com.bluesquare.rc.rest.models.BaseDomain;


public class FetchBankTransactionsResponse extends BaseDomain{
	private List<BankAccountTransactionModel> bankAccountTransactions;  
	
	private List<BankAccountTempTransactionModel> bankAccountTempTransactions;
	
	private double opening;
	
	private double closing;

	public List<BankAccountTransactionModel> getBankAccountTransactions() {
		return bankAccountTransactions;
	}

	public void setBankAccountTransactions(List<BankAccountTransactionModel> bankAccountTransactions) {
		this.bankAccountTransactions = bankAccountTransactions;
	}

	public List<BankAccountTempTransactionModel> getBankAccountTempTransactions() {
		return bankAccountTempTransactions;
	}

	public void setBankAccountTempTransactions(List<BankAccountTempTransactionModel> bankAccountTempTransactions) {
		this.bankAccountTempTransactions = bankAccountTempTransactions;
	}

	public double getOpening() {
		return opening;
	}

	public void setOpening(double opening) {
		this.opening = opening;
	}

	public double getClosing() {
		return closing;
	}

	public void setClosing(double closing) {
		this.closing = closing;
	}

	@Override
	public String toString() {
		return "FetchBankTransactionsResponse [bankAccountTransactions=" + bankAccountTransactions
				+ ", bankAccountTempTransactions=" + bankAccountTempTransactions + ", opening=" + opening + ", closing="
				+ closing + "]";
	}
	
	
}
