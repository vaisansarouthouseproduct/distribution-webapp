package com.bluesquare.rc.models;

import java.util.Date;

public class EmployeeSalaryModel {

	private int srno;
	private long employeeSalaryId;
	private double amount;
	private Date date;
	private String modeOfPayment;
	private String bankDetail;
	private String checkDate;
	private String comment;
	public EmployeeSalaryModel(int srno, long employeeSalaryId, double amount, Date date, String modeOfPayment,
			String bankDetail, String checkDate, String comment) {
		super();
		this.srno = srno;
		this.employeeSalaryId = employeeSalaryId;
		this.amount = amount;
		this.date = date;
		this.modeOfPayment = modeOfPayment;
		this.bankDetail = bankDetail;
		this.checkDate = checkDate;
		this.comment = comment;
	}
	public int getSrno() {
		return srno;
	}
	public void setSrno(int srno) {
		this.srno = srno;
	}
	public long getEmployeeSalaryId() {
		return employeeSalaryId;
	}
	public void setEmployeeSalaryId(long employeeSalaryId) {
		this.employeeSalaryId = employeeSalaryId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public String getBankDetail() {
		return bankDetail;
	}
	public void setBankDetail(String bankDetail) {
		this.bankDetail = bankDetail;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "EmployeeSalaryModel [srno=" + srno + ", employeeSalaryId=" + employeeSalaryId + ", amount=" + amount
				+ ", date=" + date + ", modeOfPayment=" + modeOfPayment + ", bankDetail=" + bankDetail + ", checkDate="
				+ checkDate + ", comment=" + comment + "]";
	}	
}
