package com.bluesquare.rc.models;

import java.util.List;

public class BulkPrintSaveModelRequest {
	public List<BulkPrintSaveModel> bulkPrintSaveModelList;
	public long cbId;
	public String isEMICheque;
	

	public List<BulkPrintSaveModel> getBulkPrintSaveModelList() {
		return bulkPrintSaveModelList;
	}

	public void setBulkPrintSaveModelList(List<BulkPrintSaveModel> bulkPrintSaveModelList) {
		this.bulkPrintSaveModelList = bulkPrintSaveModelList;
	}

	public long getCbId() {
		return cbId;
	}

	public void setCbId(long cbId) {
		this.cbId = cbId;
	}

	
	
	public String getIsEMICheque() {
		return isEMICheque;
	}

	public void setIsEMICheque(String isEMICheque) {
		this.isEMICheque = isEMICheque;
	}

	@Override
	public String toString() {
		return "BulkPrintSaveModelRequest [bulkPrintSaveModelList=" + bulkPrintSaveModelList + ", cbId=" + cbId
				+ ", isEMICheque=" + isEMICheque + "]";
	}

	
	
	

}
