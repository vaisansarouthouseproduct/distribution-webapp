package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.rest.models.TargetTypeModel;

public class TargetsAndEmployeeByDeptResponse {
	List<TargetTypeModel> targetTypeList;
	List<EmployeeDetails> employeeDetailsList;
	public TargetsAndEmployeeByDeptResponse(List<TargetTypeModel> targetTypeList,
			List<EmployeeDetails> employeeDetailsList) {
		super();
		this.targetTypeList = targetTypeList;
		this.employeeDetailsList = employeeDetailsList;
	}
	public List<TargetTypeModel> getTargetTypeList() {
		return targetTypeList;
	}
	public void setTargetTypeList(List<TargetTypeModel> targetTypeList) {
		this.targetTypeList = targetTypeList;
	}
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}
	@Override
	public String toString() {
		return "TargetsAndEmployeeByDeptResponse [targetTypeList=" + targetTypeList + ", employeeDetailsList="
				+ employeeDetailsList + "]";
	}
	
	
}
