package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.CancelReason;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.models.CancelOrderRequest;
import com.bluesquare.rc.models.CancelReasonResponse;
import com.bluesquare.rc.models.ChangePaymentDateRequest;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.responseEntities.OrderProductDetailsModel;
import com.bluesquare.rc.responseEntities.ReturnFromDeliveryBoyModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.BookOrderMainRequest;
import com.bluesquare.rc.rest.models.BookOrderResponse;
import com.bluesquare.rc.rest.models.CustomerReportMainResponseModel;
import com.bluesquare.rc.rest.models.CustomerReportRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.DBReportRequest;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.FetchOrderDetailsModel;
import com.bluesquare.rc.rest.models.GkSnapProductListMainResponse;
import com.bluesquare.rc.rest.models.GkSnapProductResponse;
import com.bluesquare.rc.rest.models.InventoryReportRequest;
import com.bluesquare.rc.rest.models.OrderDetailByBusinessNameIdEmployeeIdRequest;
import com.bluesquare.rc.rest.models.OrderDetailResponse;
import com.bluesquare.rc.rest.models.OrderDetailsForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.OrderDetailsListAndProductListResponse;
import com.bluesquare.rc.rest.models.OrderDetailsListForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentList;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentListByBusinessName;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductDetailsResponse;
import com.bluesquare.rc.rest.models.OrderProductIssueListForIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderProductIssueReportRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.OrderRequest;
import com.bluesquare.rc.rest.models.PaymentListRequest;
import com.bluesquare.rc.rest.models.ReIssueDelivered;
import com.bluesquare.rc.rest.models.ReIssueOrderDetailsListModel;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.ReturnOrderFromDeliveryBoyReportModel;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * API End Points
 * 1.bookOrder
 * 2.updateBookOrder
 * 3.OrderDetailsTodayList/{employeeId}/{areaId}
 * 4.OrderDetailsPendingList/{employeeId}/{areaId}
 * 5.packedOrderDetailsTodayList/{employeeId}/{areaId}
 * 6.packedOrderDetailsPendingList/{employeeId}/{areaId}
 * 7.issuedOrderDetailsTodayList/{employeeId}/{areaId}
 * 8.issuedOrderDetailsPendingList/{employeeId}/{areaId}
 * 9.fetchOrderDetailsByOrderId/{orderId}
 * 10.fetchOrderDetailsByOrderIdForReIssue/{orderId}/{gateKeeperId}
 * 11.fetchOrderListForPaymentpackedOrderToDeliverBoy
 * 12.changePaymentDueDateSet
 * 13.fetchOrderDetailsForTotalCollectionByEmployeeIdAndDateRange
 * 14.packedOrderToDeliverBoy
 * 15.updateTransportationDetails
 * 16.confirmIssuedOrderFromDB/{orderId}
 * 17.orderDeliveredAndReturn
 * 18.fetchOrderDetailsTodaysListByAreaId/{areaId}
 * 19.fetchOrderDetailsPendingListByAreaId/{areaId}
 * 20.fetchOrderProductIssueDetailsListReportByEmpIdAndDateRangeAndAreaId
 * 21.cancelOrderDetails
 * 22.reIssue
 * 23.fetchIssueProductDetailsForIssueReportByOrderId/{orderId}
 * 24.fetchOrderDetailForDBReport
 * 25.fetchOrderDetailsForSalesReportByEmpIdAndDateRange
 * 26.fetchReplacementIssuedOrdersByEmployeeId/{employeeId}/{status}
 * 27.doneReIssueAndOrderStatusDelivered
 * 28.fetchOrderDetailsForReIssueDeliveredByReIssueOrderId/{reIssueOrderId}
 * 29.fetchOrderDetailByBusinessNameIdEmployeeId
 * 30.fetchCancelOrderByEmployeeId
 * 31.fetchReturnOrderFromDeliveryBoyReportForApp/{range}
 * 32.fetchReturnFromDeliveryBoyForApp/{returnFromDeliveryBoyMainId}
 * 33.updateReturnFromDeliveryBoy
 * 34.fetchSnapShotForGK
 * 35.fetchingLast3MonthDeliveredOrderListForOfflineMode/{employeeId}
 * 36.fetchCancelReason
 * </pre>
 */
@RestController
public class OrderControllerForApp {

	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	OrderDetails  orderDetails;
	
	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	BusinessNameService businessNameService;

	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TransportationDAO transportationDAO;
	/**
	 * <pre>
	 * check offline / online request
	 * save non-free product with orderDetails
	 * update free products
	 * update payment period days
	 * </pre>
	 * @param token
	 * @param bookOrderMainRequest
	 * @return
	 */
	@Transactional 	@PostMapping("/bookOrder")
	public ResponseEntity<BookOrderResponse> bookOrder(@RequestHeader("Authorization") String token,@RequestBody BookOrderMainRequest bookOrderMainRequest){
		
		HttpStatus httpStatus;
		BookOrderResponse bookOrderResponse=new BookOrderResponse();
		
		boolean isPerform=true;
		
		// if Offline Api is hit
		if(bookOrderMainRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(bookOrderMainRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(bookOrderMainRequest.getDbId());
			apiRequestImeiAndDbId.setTaskPerform(bookOrderMainRequest.getTaskPerform());
			apiRequestImeiAndDbId.setIsOfflineRequest(bookOrderMainRequest.getIsOfflineRequest());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);

			// managing offline apiRequest by SQLite db id and Imei number
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				// if that API is already processed
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		if(isPerform){
			//book order non free
			String orderDetailsId=orderDetailsService.bookOrder(bookOrderMainRequest.getOrderRequest());
			//orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderDetailsId);
			
			//book free product
			if(bookOrderMainRequest.getFreeProductHave().equals("Yes"))
			{
				bookOrderMainRequest.getBookOrderFreeProductRequest().setOrderId(orderDetailsId);
				orderDetailsService.freeProductAddInOrderProductDetails(bookOrderMainRequest.getBookOrderFreeProductRequest());
			}
			//update payment period days
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderDetailsId);
			orderDetails.setPaymentPeriodDays(bookOrderMainRequest.getOrderPaymentPeriodDaysRequest().getOrderPaymentPeriodDays());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			
			//response
			bookOrderResponse.setBusinessName(orderDetails.getBusinessName().getShopName());
			bookOrderResponse.setOrderId(orderDetailsId);
			bookOrderResponse.setTotalAmount(orderDetails.getTotalAmount());
			bookOrderResponse.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
			
			if(bookOrderMainRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
				orderDetailsService.updateTheStatusOfOfflineAPIRequest(bookOrderMainRequest.getDbId(), bookOrderMainRequest.getImeiNumber(), Constants.OFFLINE_STATUS_COMPLETE);
			}
			bookOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
		}else{
			bookOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
			
			return new ResponseEntity<BookOrderResponse>(bookOrderResponse, httpStatus);
					
	}
	
	/*@PostMapping("/bookOrderFreeProducts")
	public ResponseEntity<BaseDomain> bookOrder(@RequestBody BookOrderFreeProductRequest bookOrderFreeProductRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		try 
		{
			orderDetailsService.freeProductAddInOrderProductDetails(bookOrderFreeProductRequest);
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		} catch (Exception e) {
			
			httpStatus=HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}		
	}
	*/
	/**
	 * <pre>
	 * check extra issued quantity with current quantity
	 * set image and content type
	 * update booked/packed/issued
	 * </pre>
	 * @param token
	 * @param orderRequest
	 * @return
	 */
	@Transactional 	@PostMapping("/updateBookOrder")
	public ResponseEntity<BaseDomain> updateBookOrder(@RequestHeader("Authorization") String token,@RequestBody OrderRequest orderRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderId(orderRequest.getOrderDetails().getOrderId());
		
		List<OrderProductDetails> orderProductDetailsList=orderDetailsService.fetchOrderProductDetailByOrderId(orderRequest.getOrderDetails().getOrderId());
		
		if(!orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED)){
			/* check order product quantity with current inventory*/
			Map<Long,Long> productListWithQty=new HashMap<>();
			for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()){
				OrderProductDetails oldOrderProductDetails=null;
				
				for(OrderProductDetails orderProductDetailsOld :  orderProductDetailsList){
					if(orderProductDetails.getProduct().getProductId()==orderProductDetailsOld.getProduct().getProduct().getProductId() && orderProductDetails.getType().equals(orderProductDetailsOld.getType())){
						oldOrderProductDetails=orderProductDetailsOld;
					}
				}			
				
				if(productListWithQty.containsKey(orderProductDetails.getProduct().getProductId())){
					long qty;//=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
					
					if(oldOrderProductDetails==null){
						qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
					}else{
						qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity()-oldOrderProductDetails.getIssuedQuantity();
					}
					if(qty>0){
						productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
					}
				}else{
					long qty;
					if(oldOrderProductDetails==null){
						qty=orderProductDetails.getIssuedQuantity();
					}else{
						qty=orderProductDetails.getIssuedQuantity()-oldOrderProductDetails.getIssuedQuantity();
					}
					if(qty>0){
						productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
					}
				}
			}
			
			for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
				Product product=productService.fetchProductForWebApp(entry.getKey());
				System.out.println("product : "+product);
				System.out.println("productId : "+entry.getKey());
				if(product.getCurrentQuantity()<entry.getValue()){
					
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);				
				}
			}
			/*End*/
		}
		
		
		//set image and content type
		List<Product> productList=productService.fetchProductListForWebApp();
		List<OrderProductDetails> orderProductDetailsListTemp=new ArrayList<>();
		for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()){
			Product product=orderProductDetails.getProduct().getProduct();
			for(Product product2: productList){ 
				if(product2.getProductId()==product.getProductId()){
					/*product.setProductImage(product2.getProductImage());
					product.setProductContentType(product2.getProductContentType());*/
					
					product.setThreshold(product2.getThreshold());
					product.setFreeQuantity(product2.getFreeQuantity());
					product.setProductAddedDatetime(product2.getProductAddedDatetime());
					product.setProductQuantityUpdatedDatetime(product2.getProductQuantityUpdatedDatetime());
					product.setCompany(product2.getCompany());
					product.setBranch(product2.getBranch());
					
					orderProductDetails.getProduct().setProduct(product);
					orderProductDetailsListTemp.add(orderProductDetails);
				}
			}
		}
		orderRequest.setOrderProductDetailList(orderProductDetailsListTemp);
		
		String orderDetailsId=orderDetailsService.updateBookOrder(orderRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
			
	}
	
	/*@PostMapping("/orderPaymentPeriodDaysUpdate")
	public ResponseEntity<BaseDomain> orderPaymentPeriodDaysUpdate(@RequestBody OrderPaymentPeriodDaysRequest orderPaymentPeriodDaysRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		try 
		{
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderPaymentPeriodDaysRequest.getOrderId());
			orderDetails.setPaymentPeriodDays(orderPaymentPeriodDaysRequest.getOrderPaymentPeriodDays());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		} catch (Exception e) {
			
			httpStatus=HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		
	}*/
	/**
	 * <pre>
	 * fetch todays booked order details list for gatekeeper
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList} 
	 */
	@Transactional 	@GetMapping("/OrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> OrderDetailsTodaysList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
		
		
			orderDetailsList = orderDetailsService.fetchOrderListByAreaId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	

	
	/**
	 * <pre>
	 * fetch before todays booked order details list for gatekeeper
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList}  
	 */
	@Transactional 	@GetMapping("/OrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> OrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

	
		
		
			orderDetailsList = orderDetailsService.fetchPendingOrderListByAreaId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Pending Order");
			} else {

				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}

			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		
	}
	/**
	 * <pre>
	 * fetch todays packed order details list for delivery boy
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList} 
	 */
	@Transactional 	@GetMapping("/packedOrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> PackedOrderDetailsTodayList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		

			orderDetailsList = orderDetailsService.fetchTodaysPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	/**
	 * <pre>
	 * fetch before todays packed order details list for delivery boy
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList}  
	 */
	@Transactional 	@GetMapping("/packedOrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> packedOrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		

			orderDetailsList = orderDetailsService.fetchPendingPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	/**
	 * <pre>
	 * fetch todays issued order details list for delivery boy
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList} 
	 */
	@Transactional 	@GetMapping("/issuedOrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> issuedOrderDetailsTodayList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
			orderDetailsList = orderDetailsService.fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	/**
	 * <pre>
	 * fetch before todays issued order details list for delivery boy
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param areaId
	 * @return {@link OrderDetailsList} 
	 */
	@Transactional 	@GetMapping("/issuedOrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> issuedOrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
			orderDetailsList = orderDetailsService.fetchPendingIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	/**
	 * <pre>
	 * fetch order details by order id
	 * </pre>
	 * @param token
	 * @param orderId
	 * @return {@link OrderDetailResponse}
	 */
	@Transactional 	@GetMapping("/fetchOrderDetailsByOrderId/{orderId}")
    public ResponseEntity<OrderDetailResponse> fetchOrderDetailsByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
        
        OrderDetailResponse orderDetailResponse=new OrderDetailResponse();
        HttpStatus httpStatus;
        
            List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
            if(orderProductDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                orderDetailResponse.setErrorMsg("Order Details Not Found");
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
           
            OrderDetails orderDetailList=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
            if(orderDetailList==null){
            	
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                orderDetailResponse.setErrorMsg("Order Details Not Found");
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
            //order taker sales man details
            EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());
            
            orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
            orderDetailResponse.setOrderDetailList(orderDetailList);
            orderDetailResponse.setOrderProductDetailList(orderProductDetailList);
            orderDetailResponse.setSalesPersonName(employeeDetails.getName());
            httpStatus=HttpStatus.OK;
            return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
       
    }
	/**
	 * <pre>
	 * fetch order details by order id
	 * get delivery/sales person list by gatekeeper id (which is in same areas) 
	 * return order by order id which is pending
	 * </pre>
	 * @param token
	 * @param orderId
	 * @param gateKeeperId
	 * @return {@link OrderDetailResponse}
	 */
	@Transactional 	@GetMapping("/fetchOrderDetailsByOrderIdForReIssue/{orderId}/{gateKeeperId}")
    public ResponseEntity<OrderProductDetailsResponse> fetchOrderDetailsByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId,@ModelAttribute("gateKeeperId") long gateKeeperId){
        
        
		OrderProductDetailsResponse orderProductDetailsResponse=new OrderProductDetailsResponse();
        HttpStatus httpStatus;
        
        
        
       
            List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
            if(orderProductDetailList==null){
                orderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderProductDetailsResponse>(orderProductDetailsResponse,httpStatus);
            }
            List<OrderProductDetailsModel> orderProductDetailsModelList=new ArrayList<>();
            for(OrderProductDetails orderProductDetails : orderProductDetailList){
            	orderProductDetailsModelList.add(new OrderProductDetailsModel(
            			orderProductDetails.getOrderProductDetailsid(), 
            			orderProductDetails.getPurchaseQuantity(), 
            			orderProductDetails.getDetails(), 
            			orderProductDetails.getIssuedQuantity(), 
            			orderProductDetails.getSellingRate(), 
            			orderProductDetails.getPurchaseAmount(), 
            			orderProductDetails.getIssueAmount(), 
            			orderProductDetails.getConfirmQuantity(), 
            			orderProductDetails.getConfirmAmount(), 
            			orderProductDetails.getProduct(), 
            			null,
            			orderProductDetails.getType()));
            }
            //orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
            
            OrderDetails orderDetailList=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
            if(orderDetailList==null){
                orderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderProductDetailsResponse>(orderProductDetailsResponse,httpStatus);
            }
            //get delivery/sales person list by gatekeeper id (which is in same areas) 
            EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());
            List<EmployeeNameAndId> employeeNameAndIdSMAndDBList=employeeDetailsService.fetchSMandDBByGateKeeperId(gateKeeperId);
            
            //return order by order id
            ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderProductByReIssueStatus(orderId);
            //returnOrderProduct.setEmployee(null); // null set for data load on network
            
            orderProductDetailsResponse.setReturnOrderProduct(returnOrderProduct);
            orderProductDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
           // orderProductDetailsResponse.setOrderDetailList(orderDetailList);
            
			orderProductDetailsResponse.setOrderId(orderDetailList.getOrderId());
			orderProductDetailsResponse.setOrderDetailsAddedDatetime(orderDetailList.getOrderDetailsAddedDatetime());
			orderProductDetailsResponse.setShopName(orderDetailList.getBusinessName().getShopName());
			orderProductDetailsResponse.setMobileNumber(orderDetailList.getBusinessName().getContact().getMobileNumber());
			orderProductDetailsResponse.setAreaName(orderDetailList.getBusinessName().getArea().getName());
			orderProductDetailsResponse.setOrderStatus(orderDetailList.getOrderStatus().getStatus());
			orderProductDetailsResponse.setBusinessNameId(orderDetailList.getBusinessName().getBusinessNameId());
			
            orderProductDetailsResponse.setOrderProductDetailList(orderProductDetailsModelList);
            orderProductDetailsResponse.setSalesPersonName(employeeDetails.getName());
            orderProductDetailsResponse.setEmployeeNameAndIdSMAndDBList(employeeNameAndIdSMAndDBList);
            httpStatus=HttpStatus.OK;
            return new ResponseEntity<OrderProductDetailsResponse>(orderProductDetailsResponse,httpStatus);
            
    }
    
    /**
     * <pre>
     * fetch order details by paystatus,range,employeeId
     * find orderId,BusinessName,amountDue,totalAmountWithTax,dueDate in list according order
     * </pre>
     * @param token
     * @param paymentListRequest
     * @return {@link OrderDetailsListForPayment}
     */
	@Transactional 	@PostMapping("/fetchOrderListForPayment")
    public ResponseEntity<OrderDetailsListForPayment> fetchOrderListForPayment(@RequestHeader("Authorization") String token,@RequestBody PaymentListRequest paymentListRequest){
		
		OrderDetailsListForPayment orderDetailsListForPayment=new OrderDetailsListForPayment();
		HttpStatus httpStatus;
		
		
		
			List<OrderDetailsForPayment> orderDetailsForPaymentList=orderDetailsService.fetchOrderListForPayment(paymentListRequest);
			
			if(orderDetailsForPaymentList==null)
			{
				orderDetailsListForPayment.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				orderDetailsListForPayment.setOrderDetailsList(orderDetailsForPaymentList);
				orderDetailsListForPayment.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
		
		
		return new ResponseEntity<OrderDetailsListForPayment>(orderDetailsListForPayment,httpStatus);
	}

	/**
	 * <pre>
	 * check offline/online request
	 * change payment due date against order id
	 * </pre>
	 * @param token
	 * @param changePaymentDateRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	
	@PostMapping("/changePaymentDueDateSet")
    public ResponseEntity<BaseDomain> changePaymentDueDateSet(@RequestHeader("Authorization") String token,@RequestBody ChangePaymentDateRequest changePaymentDateRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		
		boolean isPerform=true;
		
		if(changePaymentDateRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(changePaymentDateRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(changePaymentDateRequest.getDbId());
			apiRequestImeiAndDbId.setIsOfflineRequest(changePaymentDateRequest.getIsOfflineRequest());
			apiRequestImeiAndDbId.setTaskPerform(changePaymentDateRequest.getTaskPerform());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		
		if(isPerform){
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(changePaymentDateRequest.getOrderId());
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(changePaymentDateRequest.getDate());
			orderDetails.setOrderDetailsPaymentTakeDatetime(cal.getTime());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			
			if(changePaymentDateRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
				changePaymentDateRequest.setStatus(Constants.OFFLINE_STATUS_COMPLETE);
				orderDetailsService.updateTheStatusOfOfflineAPIRequest(changePaymentDateRequest.getDbId(),changePaymentDateRequest.getImeiNumber(),Constants.OFFLINE_STATUS_COMPLETE);
			}
			
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
			
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		
	}
	/**
	 * <pre>
	 * find order collection details list
	 * businessNameId,shopName,areaName,noOfOrders,totalAmount;
	 * </pre>
	 * @param token
	 * @param customerReportRequest
	 * @return {@link CustomerReportMainResponseModel}
	 */
	@Transactional 	@PostMapping("/fetchOrderDetailsForTotalCollectionByEmployeeIdAndDateRange")
	public ResponseEntity<CustomerReportMainResponseModel> fetchOrderDetailByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
		
		CustomerReportMainResponseModel customerReportMainResponseModel=new CustomerReportMainResponseModel();
		HttpStatus httpStatus;
		
		
		
		List<CustomerReportResponse> totelCollectionlist=orderDetailsService.fetchTotalCollectionByDateRangeAndEmployeeId(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
			
		if(totelCollectionlist.isEmpty())
		{
			customerReportMainResponseModel.setStatus(Constants.FAILURE_RESPONSE);
			customerReportMainResponseModel.setErrorMsg("Records Not Found");
			httpStatus=HttpStatus.OK;
		}
		else
		{
			customerReportMainResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
			customerReportMainResponseModel.setErrorMsg(Constants.NONE);
			customerReportMainResponseModel.setCustomerReportResponse(totelCollectionlist);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<CustomerReportMainResponseModel>(customerReportMainResponseModel,httpStatus);
	}
	/**
	 * <pre>
	 * check issued quantity with current quantity 
	 * issued quantity cut from current quantity
	 * order status change booked to packed
	 * </pre>
	 * @param token
	 * @param orderIssueRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/packedOrderToDeliverBoy")
	public ResponseEntity<BaseDomain> issueOrderToDeliverBoy(@RequestHeader("Authorization") String token,@RequestBody OrderIssueRequest orderIssueRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
				
		/* check order product quantity with current inventory*/
		Map<Long,Long> productListWithQty=new HashMap<>();
		for(OrderProductDetails orderProductDetails : orderIssueRequest.getOrderProductDetailsList()){
			if(productListWithQty.containsKey(orderProductDetails.getProduct().getProductId())){
				long qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
				productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
			}else{
				productListWithQty.put(orderProductDetails.getProduct().getProductId(),orderProductDetails.getIssuedQuantity());
			}
		}
		
		for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
			Product product=productService.fetchProductForWebApp(entry.getKey());
			if(product.getCurrentQuantity()<entry.getValue()){
				
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
				httpStatus=HttpStatus.OK;
				return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);				
			}
		}
		/*End*/
		
		String result=orderDetailsService.packedOrderToDeliverBoy(orderIssueRequest);
		if(result.equals("Success"))
		{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		else
		{
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	/**
	 * update transportation details of order
	 * @param token
	 * @param orderIssueRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/updateTransportationDetails")
	public ResponseEntity<BaseDomain> issueOrderToDeliverBoy(@RequestBody OrderDetails orderDetails){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		if(orderDetails.getOrderId().contains("CORD")){
			CounterOrder counterOrder=counterOrderService.fetchCounterOrder(orderDetails.getOrderId());
			Transportation transportation=transportationDAO.fetchById(orderDetails.getTransportation().getId()+"");
			counterOrder.setTransportation(transportation);
			counterOrder.setVehicleNo(orderDetails.getVehicleNo());
			counterOrder.setDocketNo(orderDetails.getDocketNo());
			counterOrder.setTransportationCharges(orderDetails.getTransportationCharges());
			counterOrderService.updateCounterOrder(counterOrder);
		}else{
			
			OrderDetails orderDetailsOld=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderDetails.getOrderId());
			Transportation transportation=transportationDAO.fetchById(orderDetails.getTransportation().getId()+"");
			orderDetailsOld.setTransportation(transportation);
			orderDetailsOld.setVehicleNo(orderDetails.getVehicleNo());
			orderDetailsOld.setDocketNo(orderDetails.getDocketNo());
			orderDetailsOld.setTransportationCharges(orderDetails.getTransportationCharges());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetailsOld);
		}
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;	
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	/**
	 * <pre>
	 * change order status Packed to Issued
	 * </pre>
	 * @param token
	 * @param orderId
	 * @return BaseDomain with message
	 */
	@Transactional 	@GetMapping("/confirmIssuedOrderFromDB/{orderId}")
	public ResponseEntity<BaseDomain> confirmIssuedOrderFromDB(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
				
		orderDetailsService.confirmPackedOrderFromDB(orderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	/**
	 * <pre>
	 * check offline/online
	 * order delivered and some product come return 
	 * order status change Issued to Delivered or Delivered Pending
	 * If Return come then define Delivered Pending otherwise Delivered
	 * </pre>
	 * @param token
	 * @param returnOrderRequest
	 * @param request
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/orderDeliveredAndReturn")
	public ResponseEntity<BaseDomain> orderDeliveredAndReturn(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderRequest returnOrderRequest,HttpServletRequest request){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		boolean isPerform=true;
		
		if(returnOrderRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(returnOrderRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(returnOrderRequest.getDbId());
			apiRequestImeiAndDbId.setIsOfflineRequest(returnOrderRequest.getIsOfflineRequest());
			apiRequestImeiAndDbId.setTaskPerform(returnOrderRequest.getTaskPerform());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		if(isPerform){

			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/");
			orderDetailsService.orderDeliveredAndReturn(returnOrderRequest,appPath);
			
			if(returnOrderRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
				orderDetailsService.updateTheStatusOfOfflineAPIRequest(returnOrderRequest.getDbId(), returnOrderRequest.getImeiNumber(), Constants.OFFLINE_STATUS_COMPLETE);
			}
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	/**
	 * <pre>
	 * fetch booked current date order details and product list by areaId
	 * </pre>
	 * @param token
	 * @param areaId
	 * @return {@link OrderDetailsList}
	 */
	/*@Transactional 	@GetMapping("/fetchOrderDetailsTodaysListByAreaId/{areaId}")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsTodaysListByAreaId(@RequestHeader("Authorization") String token,@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		
		


			List<OrderDetails> orderDetailsListtodays = orderDetailsService.fetchOrderDetailsTodaysListByAreaId(areaId);
			if (orderDetailsListtodays == null) {
				httpStatus = HttpStatus.NO_CONTENT;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			} else {
				orderDetailsList.setOrderDetailsList(orderDetailsListtodays);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
		

		return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}*/
	/**
	 * fetch booked before todays date order details and product list by areaId
	 * @param token
	 * @param areaId
	 * @return {@link OrderDetailsList}
	 */
	/*@Transactional 	@GetMapping("/fetchOrderDetailsPendingListByAreaId/{areaId}")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsPendingListByAreaId(@RequestHeader("Authorization") String token,
			@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		
		
		
			List<OrderDetails> orderDetailsListPending = orderDetailsService.fetchOrderDetailsPendingListByAreaId(areaId);
			if (orderDetailsListPending == null) {
				httpStatus = HttpStatus.NO_CONTENT;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			} else {
				orderDetailsList.setOrderDetailsList(orderDetailsListPending);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
		

		return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}*/
	/**
	 * <pre>
	 * fetch issued orders details by employeeId,fromDate,toDate,range ,areaId
	 * if areaId not zero then fetch order by business areaId otherwise all 
	 * </pre>
	 * @param token
	 * @param orderProductIssueReportRequest
	 * @return OrderProductIssueReportResponse
	 */
	@Transactional 	@PostMapping("/fetchOrderProductIssueDetailsListReportByEmpIdAndDateRangeAndAreaId")
	public ResponseEntity<OrderProductIssueReportResponse> fetchOrderProductIssueDetailsListByEmpIdAndDateRangeAndAreaId(@RequestHeader("Authorization") String token,
			@RequestBody OrderProductIssueReportRequest orderProductIssueReportRequest) {

		HttpStatus httpStatus;
		OrderProductIssueReportResponse orderProductIssueReportResponse = new OrderProductIssueReportResponse();

	
		
		
			OrderProductIssueReportResponse orderProductIssueDetailsList = orderDetailsService.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(orderProductIssueReportRequest.getEmployeeId(),orderProductIssueReportRequest.getAreaId() , orderProductIssueReportRequest.getFromDate(), orderProductIssueReportRequest.getToDate(), orderProductIssueReportRequest.getRange());
										
			if(orderProductIssueDetailsList.getFetchOrderDetailsModelList()==null)
			{
				orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				httpStatus=HttpStatus.OK;
				orderProductIssueReportResponse.setErrorMsg("No Order Found");
			}
			else
			{
				orderProductIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				orderProductIssueReportResponse.setFetchOrderDetailsModelList(orderProductIssueDetailsList.getFetchOrderDetailsModelList());
				httpStatus=HttpStatus.OK;
			}
				
		
		return new ResponseEntity<OrderProductIssueReportResponse>(orderProductIssueReportResponse, httpStatus);

	}
	/**
	 * check offline / online data
	 * cancel order by order id from employee
	 * @param token
	 * @param cancelOrderRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/cancelOrderDetails")
	public ResponseEntity<BaseDomain> cancelOrder (@RequestHeader("Authorization") String token,@RequestBody CancelOrderRequest cancelOrderRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		boolean isPerform=true;
		
		if(cancelOrderRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(cancelOrderRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(cancelOrderRequest.getDbId());
			apiRequestImeiAndDbId.setIsOfflineRequest(cancelOrderRequest.getIsOfflineRequest());
			apiRequestImeiAndDbId.setTaskPerform(cancelOrderRequest.getTaskPerform());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		
		if(isPerform){
			String responseText=orderDetailsService.cancelOrder(cancelOrderRequest);
			if(responseText.equals("Success"))
			{
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				if(cancelOrderRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
					cancelOrderRequest.setStatus(Constants.OFFLINE_STATUS_COMPLETE);
					orderDetailsService.updateTheStatusOfOfflineAPIRequest(cancelOrderRequest.getDbId(),cancelOrderRequest.getImeiNumber(),Constants.OFFLINE_STATUS_COMPLETE);
				}
				
			}
			else
			{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(responseText);
				httpStatus=HttpStatus.OK;
			}
		
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			baseDomain.setErrorMsg("Already added to Db");
			httpStatus=HttpStatus.OK;
		}
			
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}

	/**
	 * <pre>
	 * check reissued quantity with current quantity
	 * reissue define
	 * reduce product current quantity basis of which give reissue quantity
	 * reissue same quantity also define in damage and damage recovery
	 * send notification to assigned delivery boy / salesman 
	 * </pre>
	 * @param token
	 * @param orderReIssueRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/reIssue")
	public ResponseEntity<BaseDomain> reIssueOrderDetails(@RequestHeader("Authorization") String token,@RequestBody OrderReIssueRequest orderReIssueRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
		/* check order product quantity with current inventory*/
		Map<Long,Long> productListWithQty=new HashMap<>();
		for(ReIssueOrderProductDetails reIssueOrderProductDetails : orderReIssueRequest.getReIssueOrderProductDetailsList()){
			if(productListWithQty.containsKey(reIssueOrderProductDetails.getProduct().getProduct().getProductId())){
				long qty=productListWithQty.get(reIssueOrderProductDetails.getProduct().getProduct().getProductId())+reIssueOrderProductDetails.getReIssueQuantity();
				productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),qty);
			}else{
				productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),reIssueOrderProductDetails.getReIssueQuantity());
			}
		}
		
		for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
			Product product=productService.fetchProductForWebApp(entry.getKey());
			if(product.getCurrentQuantity()<entry.getValue()){
				
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
				httpStatus=HttpStatus.OK;
				
				return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);			
			}
		}
		/*End*/
		
		orderDetailsService.reIssueOrderDetails(orderReIssueRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
		/**
		 * fetching IssueProductDetailsList For GateKeeper IssueReport By OrderId
		 * @param token
		 * @param orderId
		 * @return {@link OrderProductIssueListForIssueReportResponse}
		 */
		@Transactional 	@GetMapping("/fetchIssueProductDetailsForIssueReportByOrderId/{orderId}")
		public ResponseEntity<OrderProductIssueListForIssueReportResponse> fetchIssueProductDetailsForIssueReportByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
			
			OrderProductIssueListForIssueReportResponse orderProductIssueListForIssueReportResponse=new OrderProductIssueListForIssueReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchIssueProductDetailsForIssueReportByOrderId");
						
				// Fetching orderProductIssueDetails By orderId
				OrderProductIssueDetails orderProductIssueDetails=orderDetailsService.fetchOrderProductIssueListForIssueReportByOrderId(orderId);
				if(orderProductIssueDetails==null)
				{
					orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);	
					orderProductIssueListForIssueReportResponse.setErrorMsg("No Orderdetails");
					httpStatus=HttpStatus.OK;
				}
				orderProductIssueListForIssueReportResponse.setOrderProductIssueDetails(orderProductIssueDetails);
				
				// in below 2 lines we r fetching SalesManNAame and DeliveryBoy Name by getting empId from orderProductIssueDetails 
				EmployeeDetails employeeDetailsSM =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getOrderDetails().getEmployeeSM().getEmployeeId());
				EmployeeDetails employeeDetailsDB =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getEmployeeDB().getEmployeeId());
				
				//here we are fetching OrderProductDetalis By OrderId and making the ProductImage Null
				List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
				//orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
				 
				if(orderProductDetailList==null)
				{
					orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderProductIssueListForIssueReportResponse.setErrorMsg("Order Details not Found");
				    httpStatus=HttpStatus.OK;
				}
				else
				{
					List<OrderProductDetailsModel> orderProductDetailsModelList=new ArrayList<>();
		            for(OrderProductDetails orderProductDetails : orderProductDetailList){
		            	orderProductDetailsModelList.add(new OrderProductDetailsModel(
		            			orderProductDetails.getOrderProductDetailsid(), 
		            			orderProductDetails.getPurchaseQuantity(), 
		            			orderProductDetails.getDetails(), 
		            			orderProductDetails.getIssuedQuantity(), 
		            			orderProductDetails.getSellingRate(), 
		            			orderProductDetails.getPurchaseAmount(), 
		            			orderProductDetails.getIssueAmount(), 
		            			orderProductDetails.getConfirmQuantity(), 
		            			orderProductDetails.getConfirmAmount(), 
		            			orderProductDetails.getProduct(), 
		            			null,
		            			orderProductDetails.getType()));
		            }
					orderProductIssueListForIssueReportResponse.setOrderProductDetailsList(orderProductDetailsModelList);
					orderProductIssueListForIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					orderProductIssueListForIssueReportResponse.setSalesman(employeeDetailsSM.getName());
					orderProductIssueListForIssueReportResponse.setDeliveryBoy(employeeDetailsDB.getName());
					httpStatus=HttpStatus.OK;
				}
				        
	        return new ResponseEntity<OrderProductIssueListForIssueReportResponse>(orderProductIssueListForIssueReportResponse,httpStatus);
			
		}
		
		/**
		 * here we are fetching OrderDetails For DB Report By DateRange and EmpId And orderStatus 	
		 * @param token
		 * @param dBReportRequest
		 * @return {@link OrderProductIssueReportResponse}
		 */
		@Transactional 	@PostMapping("/fetchOrderDetailForDBReport")
		public ResponseEntity<OrderProductIssueReportResponse>  fetchOrderDetailForDBReport(@RequestHeader("Authorization") String token,@RequestBody DBReportRequest dBReportRequest){
			
			OrderProductIssueReportResponse orderProductIssueReportResponse=new OrderProductIssueReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchOrderDetailForDBReport");
						
				List<OrderProductIssueDetails> orderProductIssueDetailsList=orderDetailsService.fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus
																						   (dBReportRequest.getEmployeeId(),
																							dBReportRequest.getFromDate(),
																							dBReportRequest.getToDate(),
																							dBReportRequest.getRange(),
																							dBReportRequest.getOrderStatus());


				long deliveredCount=0;
				long deliveredPendingCount=0;
				if(orderProductIssueDetailsList==null)
				{
					orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderProductIssueReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
					orderProductIssueReportResponse.setDeliveredCount(deliveredCount);
					orderProductIssueReportResponse.setDeliveredPendingCount(deliveredPendingCount);
				}
				else
				{
					
					orderProductIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;

					List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
					for(OrderProductIssueDetails orderProductIssueDetails : orderProductIssueDetailsList){
						
						if(orderProductIssueDetails.getOrderDetails().getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED))
						{
							deliveredCount++;
						}
						else
						{
							deliveredPendingCount++;
						}						
						
						FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
						fetchOrderDetailsModel.setOrderId(orderProductIssueDetails.getOrderDetails().getOrderId());
						fetchOrderDetailsModel.setAreaName(orderProductIssueDetails.getOrderDetails().getBusinessName().getArea().getName());
						fetchOrderDetailsModel.setOrderDate(orderProductIssueDetails.getOrderDetails().getOrderDetailsAddedDatetime());
						fetchOrderDetailsModel.setPurchasedQuantity(orderProductIssueDetails.getOrderDetails().getTotalQuantity());
						fetchOrderDetailsModel.setShopName(orderProductIssueDetails.getOrderDetails().getBusinessName().getShopName());
						fetchOrderDetailsModel.setOrderStatus(orderProductIssueDetails.getOrderDetails().getOrderStatus().getStatus());
						fetchOrderDetailsModel.setAreaId(orderProductIssueDetails.getOrderDetails().getBusinessName().getArea().getAreaId());
						fetchOrderDetailsModel.setBusinessNameId(orderProductIssueDetails.getOrderDetails().getBusinessName().getBusinessNameId());
						fetchOrderDetailsModel.setOwnerName(orderProductIssueDetails.getOrderDetails().getBusinessName().getOwnerName());
						fetchOrderDetailsModel.setIssuedQuantity(orderProductIssueDetails.getOrderDetails().getIssuedTotalQuantity());
						fetchOrderDetailsModel.setPackedDate(orderProductIssueDetails.getOrderDetails().getPackedDate());
						fetchOrderDetailsModel.setIssuedDate(orderProductIssueDetails.getOrderDetails().getIssueDate());
						fetchOrderDetailsModel.setTotalAmount(orderProductIssueDetails.getOrderDetails().getTotalAmount());
						fetchOrderDetailsModel.setTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getTotalAmountWithTax());
						fetchOrderDetailsModel.setIssuedTotalAmount(orderProductIssueDetails.getOrderDetails().getIssuedTotalAmount());
						fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getIssuedTotalAmountWithTax());
						fetchOrderDetailsModel.setConfirmTotalAmount(orderProductIssueDetails.getOrderDetails().getConfirmTotalAmount());
						fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getConfirmTotalAmountWithTax());
						fetchOrderDetailsModel.setCancelEmployeeDepartment((orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel()!=null)?orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel().getDepartment().getName():null);
						fetchOrderDetailsModel.setCancelDate((orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel()!=null)?orderProductIssueDetails.getOrderDetails().getCancelDate():null);
						fetchOrderDetailsModel.setConfirmDate(orderProductIssueDetails.getOrderDetails().getConfirmDate());
						fetchOrderDetailsModel.setMobileNo(orderProductIssueDetails.getOrderDetails().getBusinessName().getContact().getMobileNumber());
						fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
					}
					orderProductIssueReportResponse.setFetchOrderDetailsModelList(fetchOrderDetailsModelList);
					
					orderProductIssueReportResponse.setDeliveredCount(deliveredCount);
					orderProductIssueReportResponse.setDeliveredPendingCount(deliveredPendingCount);
				}
			
			return new ResponseEntity<OrderProductIssueReportResponse>(orderProductIssueReportResponse,httpStatus);
		}
		
		
		// sachin 26/10/2017
		
		/**
		 *  here we r fetching OrderDetail for SalesReport BY empId date Range
		 * @param token
		 * @param customerReportRequest
		 * @return {@link OrderDetailsList}
		 */
	@Transactional 	@PostMapping("/fetchOrderDetailsForSalesReportByEmpIdAndDateRange")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsForSalesReportByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		HttpStatus httpStatus;
		
		
		
		
			List<OrderDetails> orderDetailsList1=orderDetailsService.fetchOrderDetailsByDateRangeAndEmpId(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
						
			if(orderDetailsList1==null)
			{
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				orderDetailsList.setErrorMsg("No Record found");
			}
			else
			{
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
				for(OrderDetails orderDetails: orderDetailsList1){
					FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
					fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
					fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
					fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
					fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
					fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
					fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
					fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
					fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
					fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
				}
				orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
				httpStatus=HttpStatus.OK;
			}
		
		return new ResponseEntity<OrderDetailsList>(orderDetailsList,httpStatus);
		
		
	}

	/**
	 * <pre>
	 * fetch replacement issued orders by employee id and status(Complete,Pending)
	 * </pre>
	 * @param token
	 * @param employeeId
	 * @param status
	 * @return {@link ReIssueOrderDetailsListModel}
	 */
	@Transactional 	@GetMapping("/fetchReplacementIssuedOrdersByEmployeeId/{employeeId}/{status}")
	public ResponseEntity<ReIssueOrderDetailsListModel> fetchOrderDetailsForSalesReportByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("status") String status){
		ReIssueOrderDetailsListModel reIssueOrderDetailsListModel=new ReIssueOrderDetailsListModel();
		HttpStatus httpStatus;
		
		
		
	
			List<ReIssueOrderDetails> reIssueOrderDetailsLists=orderDetailsService.fetchReplacementIssuedOrdersByEmployeeId(employeeId,status);
			if(reIssueOrderDetailsLists==null)
			{
				reIssueOrderDetailsListModel.setStatus(Constants.FAILURE_RESPONSE);
				reIssueOrderDetailsListModel.setErrorMsg("No Order Found");
				httpStatus=HttpStatus.OK;
			}
			else
			{
				List<ReIssueOrderDetailsReport> reIssueOrderList=new ArrayList<>();
				for(ReIssueOrderDetails reIssueOrderDetails : reIssueOrderDetailsLists){
					EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());
					reIssueOrderList.add(new ReIssueOrderDetailsReport(
							reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId(), 
							reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getShopName(), 
							reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getArea().getName(), 
							reIssueOrderDetails.getReIssueOrderId(), 
							employeeDetails.getName(), 
							employeeDetails.getEmployee().getDepartment().getName(), 
							reIssueOrderDetails.getTotalQuantity(), 
							reIssueOrderDetails.getReturnOrderProduct().getTotalQuantity(), 
							reIssueOrderDetails.getReIssueDate(), 
							reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductDatetime(), 
							reIssueOrderDetails.getReIssueDeliveredDate(), 
							reIssueOrderDetails.getStatus(),
							reIssueOrderDetails.getTotalAmountWithTax(),
							reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getBusinessNameId(),
							reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId(),
							reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getContact().getMobileNumber()));
				}
				reIssueOrderDetailsListModel.setReIssueOrderDetailsList(reIssueOrderList);
				reIssueOrderDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
		
		return new ResponseEntity<ReIssueOrderDetailsListModel>(reIssueOrderDetailsListModel,httpStatus);
	}
					
	/**
	 * <pre> 
	 * check offline/online request 
	 * order confirm quantity,amount same as issued quantity ,amount
	 * reissue status Completed
	 * </pre>				
	 * @param token
	 * @param reIssueDelivered
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/doneReIssueAndOrderStatusDelivered")
	public ResponseEntity<BaseDomain> doneReIssueAndOrderStatusDelivered(@RequestHeader("Authorization") String token,@RequestBody ReIssueDelivered reIssueDelivered){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		boolean isPerform=true;
		
		if(reIssueDelivered.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(reIssueDelivered.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(reIssueDelivered.getDbId());
			apiRequestImeiAndDbId.setIsOfflineRequest(reIssueDelivered.getIsOfflineRequest());
			apiRequestImeiAndDbId.setTaskPerform(reIssueDelivered.getTaskPerform());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		
		if(isPerform){
			String response=orderDetailsService.doneReIssueAndOrderStatusDelivered(reIssueDelivered);
			if(response.equals("Success"))
			{
				httpStatus=HttpStatus.OK;
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				if(reIssueDelivered.getIsOfflineRequest().equalsIgnoreCase("Yes")){
					orderDetailsService.updateTheStatusOfOfflineAPIRequest(reIssueDelivered.getDbId(), reIssueDelivered.getImeiNumber(), Constants.OFFLINE_STATUS_COMPLETE);
				}
			}
			else
			{
				httpStatus=HttpStatus.FORBIDDEN;
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			}
		}else{
			httpStatus=HttpStatus.OK;
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
			
			
			
		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	/**
	 * <pre>
	 * fetch ReIssue Order Main Details
	 * fetch ReIssue Order Product Wise Details
	 * </pre>
	 * @param token
	 * @param reIssueOrderId
	 * @return {@link ReIssueOrderProductDetailsListModel}
	 */
	@Transactional 	@GetMapping("/fetchOrderDetailsForReIssueDeliveredByReIssueOrderId/{reIssueOrderId}")
	public ResponseEntity<ReIssueOrderProductDetailsListModel> doneReIssueAndOrderStatusDelivered(@RequestHeader("Authorization") String token,@ModelAttribute("reIssueOrderId") long reIssueOrderId){
		ReIssueOrderProductDetailsListModel reIssueOrderProductDetailsListModel=new ReIssueOrderProductDetailsListModel();
		HttpStatus httpStatus;
		
		
		
		
			reIssueOrderProductDetailsListModel=orderDetailsService.fetchOrderDetailsForReIssueDelivedByOrderId(reIssueOrderId);
			if(reIssueOrderProductDetailsListModel.getReIssueOrderLists()==null)
			{
				reIssueOrderProductDetailsListModel.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				reIssueOrderProductDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
		
		return new ResponseEntity<ReIssueOrderProductDetailsListModel>(reIssueOrderProductDetailsListModel,httpStatus);
	}
	
		/**
		 * <pre>
		 * here we are fetching OrderDetail for TotalCollection Report ByBusinessNameIdEmployeeId
		 * </pre>
		 * @param token
		 * @param orderDetailByBusinessNameIdEmployeeIdRequest
		 * @return {@link OrderDetailsPaymentListByBusinessName}
		 */
		@Transactional 	@PostMapping("/fetchOrderDetailByBusinessNameIdEmployeeId")
		public ResponseEntity<OrderDetailsPaymentListByBusinessName> fetchOrderDetailForTotalCollectionReportByBusinessNameIdEmployeeId(@RequestHeader("Authorization") String token,@RequestBody OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest){
			OrderDetailsPaymentListByBusinessName orderDetailsPaymentListByBusinessName =new OrderDetailsPaymentListByBusinessName();
			HttpStatus httpStatus;
			
		
			
			System.out.println("fetchOrderDetailForTotalCollectionReportByBusinessNameIdEmployeeId");
			
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailByBusinessNameIdEmployeeIdRequest.getEmployeeId());
			List<OrderDetailsPaymentList> orderDetailsPaymentList=orderDetailsService.fetchOrderDetailForTotalCollectionReportByBusinessNameId(orderDetailByBusinessNameIdEmployeeIdRequest);
			if(orderDetailsPaymentList==null){
				orderDetailsPaymentListByBusinessName.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsPaymentListByBusinessName.setErrorMsg(Constants.NO_CONTENT);
				httpStatus=HttpStatus.NO_CONTENT;
				}
			else{
				BusinessName businessName=businessNameService.fetchBusinessForWebApp(orderDetailByBusinessNameIdEmployeeIdRequest.getBusinessNameId());
				orderDetailsPaymentListByBusinessName.setStatus(Constants.SUCCESS_RESPONSE);
				orderDetailsPaymentListByBusinessName.setErrorMsg(Constants.NONE);
				orderDetailsPaymentListByBusinessName.setOrderDetailsPaymentList(orderDetailsPaymentList);
				orderDetailsPaymentListByBusinessName.setShopName(businessName.getShopName());
				orderDetailsPaymentListByBusinessName.setAreaName(businessName.getArea().getName());
				orderDetailsPaymentListByBusinessName.setMobileNumber(businessName.getContact().getMobileNumber());
				orderDetailsPaymentListByBusinessName.setSalesManName(employeeDetails.getName());
				httpStatus=HttpStatus.OK;
				}
			
			return new ResponseEntity<OrderDetailsPaymentListByBusinessName>(orderDetailsPaymentListByBusinessName,httpStatus);
		}
		/**
		 * <pre>
		 * fetch order details list by range,startDate,endDate which order status is Cancelled
		 * and also its required in area which given employeeId belongs 
		 * </pre>
		 * @param token
		 * @param inventoryReportRequest
		 * @return {@link OrderDetailsList}
		 */
		@Transactional 	@PostMapping("fetchCancelOrderByEmployeeId")
		ResponseEntity<OrderDetailsList> fetchCancelReport(@RequestHeader("Authorization") String token,@RequestBody InventoryReportRequest inventoryReportRequest){
			
			OrderDetailsList orderDetailsList=new OrderDetailsList();
			HttpStatus httpStatus;
			
		
			
			List<OrderDetails> list=orderDetailsService.fetchCancelOrderReportByEmployeeId(inventoryReportRequest.getEmployeeId(),
						inventoryReportRequest.getFromDate(), inventoryReportRequest.getToDate(), inventoryReportRequest.getRange());
				
				if(list==null){
					orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
					orderDetailsList.setErrorMsg("Records Not Found");
					httpStatus=HttpStatus.OK;
				}else{
					List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
					for(OrderDetails orderDetails: list){
						FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
						fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
						fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
						fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
						fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
						fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
						fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
						fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
						fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
						fetchOrderDetailsModel.setCancelEmployeeDepartment(orderDetails.getEmployeeIdCancel().getDepartment().getName());
						fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
					}
					orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
					orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			
			
			return new ResponseEntity<OrderDetailsList>(orderDetailsList,httpStatus);
		}
		
		/**
		 * <pre>
		 * fetch return from return from delivery boy list by range, startDate, endDate
		 * </pre>
		 * @param token
		 * @param range
		 * @return {@link ReturnOrderFromDeliveryBoyReportModel}
		 */
		@Transactional 	@GetMapping("/fetchReturnOrderFromDeliveryBoyReportForApp/{range}")
		ResponseEntity<ReturnOrderFromDeliveryBoyReportModel> orderFromDeliveryBoyReport(@RequestHeader("Authorization") String token,@ModelAttribute("range") String range){
			
			ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel=new ReturnOrderFromDeliveryBoyReportModel();
			HttpStatus httpStatus;
			
				String startDate="";
				String endDate="";
				List<ReturnOrderFromDeliveryBoyReport>  returnOrderFromDeliveryBoyReportList=orderDetailsService.fetchReturnOrderFromDeliveryBoyReport(range, startDate, endDate);
				
				if(returnOrderFromDeliveryBoyReportList.isEmpty()){
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.FAILURE_RESPONSE);
					returnOrderFromDeliveryBoyReportModel.setErrorMsg(Constants.NOT_FOUND);
					httpStatus=HttpStatus.OK;
				}else{
					returnOrderFromDeliveryBoyReportModel.setReturnOrderFromDeliveryBoyReportList(returnOrderFromDeliveryBoyReportList);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			
			
			return new ResponseEntity<ReturnOrderFromDeliveryBoyReportModel>(returnOrderFromDeliveryBoyReportModel,httpStatus);
		}
		/**
		 * <pre>
		 * fetch return from delivery boy order details by returnFromDeliveryBoyMainId
		 * </pre>
		 * @param token
		 * @param returnFromDeliveryBoyMainId
		 * @return {@link ReturnOrderFromDeliveryBoyReportModel}
		 */
		@Transactional 	@GetMapping("/fetchReturnFromDeliveryBoyForApp/{returnFromDeliveryBoyMainId}")
		ResponseEntity<ReturnOrderFromDeliveryBoyReportModel> fetchReturnFromDeliveryBoyForApp(@RequestHeader("Authorization") String token,@ModelAttribute("returnFromDeliveryBoyMainId") String returnFromDeliveryBoyMainId){
			
			ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel=new ReturnOrderFromDeliveryBoyReportModel();
			HttpStatus httpStatus;
				ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=orderDetailsService.fetchReturnFromDeliveryBoyMain(returnFromDeliveryBoyMainId);
				List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList=orderDetailsService.fetchReturnFromDeliveryBoyList(returnFromDeliveryBoyMainId);
				
				if(returnFromDeliveryBoyList==null){
					returnOrderFromDeliveryBoyReportModel.setErrorMsg(Constants.NOT_FOUND);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus=HttpStatus.OK;
				}else{
					List<ReturnFromDeliveryBoyModel> returnFromDeliveryBoyModelList=new ArrayList<>();
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy : returnFromDeliveryBoyList){
						returnFromDeliveryBoyModelList.add(new ReturnFromDeliveryBoyModel(
								returnFromDeliveryBoy.getReturnFromDeliveryBoyId(), 
								returnFromDeliveryBoy.getProduct().getProductName(),
								returnFromDeliveryBoy.getProduct().getProduct().getProductId(),
								returnFromDeliveryBoy.getReturnQuantity(), 
								returnFromDeliveryBoy.getIssuedQuantity(), 
								returnFromDeliveryBoy.getDeliveryQuantity(), 
								returnFromDeliveryBoy.getDamageQuantity(), 
								returnFromDeliveryBoy.getNonDamageQuantity(),
								returnFromDeliveryBoy.getType()));
					}
					EmployeeDetails employeeDetails=null;
					if(returnFromDeliveryBoyList.get(0).getReturnFromDeliveryBoyMain().getOrderDetails() !=null){
						employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyList.get(0).getReturnFromDeliveryBoyMain().getOrderDetails().getEmployeeSM().getEmployeeId());
						returnOrderFromDeliveryBoyReportModel.setAreaName(returnFromDeliveryBoyMain.getOrderDetails().getBusinessName().getArea().getName());
						returnOrderFromDeliveryBoyReportModel.setMobileNumber(returnFromDeliveryBoyMain.getOrderDetails().getBusinessName().getContact().getMobileNumber());
						returnOrderFromDeliveryBoyReportModel.setOrderDate(returnFromDeliveryBoyMain.getOrderDetails().getOrderDetailsAddedDatetime());
						returnOrderFromDeliveryBoyReportModel.setOrderId(returnFromDeliveryBoyMain.getOrderDetails().getOrderId());
						returnOrderFromDeliveryBoyReportModel.setShopName(returnFromDeliveryBoyMain.getOrderDetails().getBusinessName().getShopName());
					}else{
						employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyList.get(0).getReturnFromDeliveryBoyMain().getCounterOrder().getEmployeeGk().getEmployeeId());
						returnOrderFromDeliveryBoyReportModel.setAreaName(returnFromDeliveryBoyMain.getCounterOrder().getBusinessName().getArea().getName());
						returnOrderFromDeliveryBoyReportModel.setMobileNumber(returnFromDeliveryBoyMain.getCounterOrder().getBusinessName().getContact().getMobileNumber());
						returnOrderFromDeliveryBoyReportModel.setOrderDate(returnFromDeliveryBoyMain.getCounterOrder().getDateOfOrderTaken());
						returnOrderFromDeliveryBoyReportModel.setOrderId(returnFromDeliveryBoyMain.getCounterOrder().getCounterOrderId());
						returnOrderFromDeliveryBoyReportModel.setShopName(returnFromDeliveryBoyMain.getCounterOrder().getBusinessName().getShopName());
					}
					
					returnOrderFromDeliveryBoyReportModel.setReturnOrderFromDeliveryBoyMainId(returnFromDeliveryBoyMainId);
					returnOrderFromDeliveryBoyReportModel.setTotalDamageQuantity(returnFromDeliveryBoyMain.getTotalDamageQuantity());
					returnOrderFromDeliveryBoyReportModel.setTotalDeliveredQuantity(returnFromDeliveryBoyMain.getTotalDeliveryQuantity());
					returnOrderFromDeliveryBoyReportModel.setTotalIssueQuantity(returnFromDeliveryBoyMain.getTotalIssuedQuantity());					
					returnOrderFromDeliveryBoyReportModel.setEmployeeName(employeeDetails.getName());
					returnOrderFromDeliveryBoyReportModel.setReturnFromDeliveryBoyList(returnFromDeliveryBoyModelList);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			return new ResponseEntity<ReturnOrderFromDeliveryBoyReportModel>(returnOrderFromDeliveryBoyReportModel,httpStatus);
		}
		/**
		 * <pre>
		 * update damage and non damage quantity in ReturnFromDeliveryBoy List and total in ReturnFromDeliveryBoyMain 
		 * If Any Quantity found in damage then its define in damage define and damageRecovery
		 * and non damage quantity added to current quantity product wise
		 * </pre>
		 * @param token
		 * @param returnOrderFromDeliveryBoyReportModel
		 * @return BaseDomain with message
		 */
		@Transactional 	@PostMapping("/updateReturnFromDeliveryBoy")
		ResponseEntity<BaseDomain> updateReturnFromDeliveryBoy(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel){
			
			BaseDomain baseDomain=new BaseDomain();
			HttpStatus httpStatus;
			
			orderDetailsService.updateReturnFromDeliveryBoyForApp(returnOrderFromDeliveryBoyReportModel.getReturnFromDeliveryBoyList(),returnOrderFromDeliveryBoyReportModel.getReturnOrderFromDeliveryBoyMainId());
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
			return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
		}
		/**
		 * <pre> 
		 * fetch order details list which comes in logged user area and company 
		 * and current date with booked orders only
		 * calculate total ordered , available and required quantity product wise
		 * </pre>
		 * @param token
		 * @return GkSnapProductListMainResponse
		 */
		@Transactional 	@GetMapping("/fetchSnapShotForGK")
		public ResponseEntity<GkSnapProductListMainResponse> fetchSnapShotForGK(@RequestHeader("Authorization") String token){
			
			GkSnapProductListMainResponse gkSnapProductListMainResponse=new GkSnapProductListMainResponse();
			HttpStatus httpStatus;
			System.out.println("Inside fetchSnapShotForGK");
		
				List<GkSnapProductResponse> gkSnapProductResponses=orderDetailsService.fetchSnapProductDetailsForGk();
				if(gkSnapProductResponses==null){
					gkSnapProductListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus=HttpStatus.OK;
					gkSnapProductListMainResponse.setErrorMsg("No Record Found");
					
				}else{
					gkSnapProductListMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
					gkSnapProductListMainResponse.setGkSnapProductResponses(gkSnapProductResponses);
				}
			return new ResponseEntity<GkSnapProductListMainResponse>(gkSnapProductListMainResponse,httpStatus);
		}
		
		/*@PostMapping("/sendEmail")
	    public ResponseEntity<?> uploadFileMulti(HttpServletRequest request,HttpServletResponse response,
	    		@RequestParam("businessNameId") String businessNameId,
	    		@RequestParam("fileName") String fileName,
	            @RequestParam("files") MultipartFile[] uploadfiles) {

	        try {

	        	BusinessName businessName=businessNameService.fetchBusinessForWebApp(businessNameId);
	        	if(businessName.getContact().getEmailId()==null)
	        	{
	        		return new ResponseEntity("Email-Id not found",HttpStatus.BAD_REQUEST);
	        	}
	        // Get file name
	        String uploadedFileName = uploadfiles[0].getOriginalFilename();

	        if (org.springframework.util.StringUtils.isEmpty(uploadedFileName)) {
	            return new ResponseEntity("please select a file!", HttpStatus.OK);
	        }

	        for (MultipartFile file : uploadfiles) {

	            if (file.isEmpty()) {
	                continue; //next pls
	            }

	            byte[] bytes = file.getBytes();
	            
	            String filePath="/resources/pdfFiles/invoice.pdf";
				
   				ServletContext context = request.getServletContext();
   				String appPath = context.getRealPath("/");
   				System.out.println("appPath = " + appPath);

   				// construct the complete absolute path of the file
   				String fullPath = appPath + filePath;
	            
	            Path path = Paths.get(fullPath);
	            Files.write(path, bytes);
	         
		        File fileSend=new File(fullPath);
		        FileOutputStream fop = new FileOutputStream(fileSend);
		        
		        byte[] contentInBytes = file.getBytes();

				fop.write(contentInBytes);
				fop.flush();
				fop.close();
				
				EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
				emailSender.sendEmail("Order Invoice", " ", "sachinupawar24@gmail.com", true, fileSend,fileName);
	        }
	        

	        return new ResponseEntity("Successfully Mail Send - "+ fileName, HttpStatus.OK);
	            //saveUploadedFiles(Arrays.asList(uploadfiles));

	        } catch (Exception e) {
	            return new ResponseEntity("Failed To Send Mail",HttpStatus.BAD_REQUEST);
	        }

	        

	    }*/
		
		
		/**
		 * <pre>
		 * here we are fetching the last 3 month order list by employee Area List to take the return from them
		 * </pre>
		 * @param token
		 * @param employeeId
		 * @return {@link OrderDetailsListAndProductListResponse}
		 */
		@Transactional
		@GetMapping("/fetchingLast3MonthDeliveredOrderListForOfflineMode/{employeeId}")
		public ResponseEntity<OrderDetailsListAndProductListResponse> fetchingLast3MonthDeliveredOrderListForOfflineMode(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId) {
			HttpStatus httpStatus;
			OrderDetailsListAndProductListResponse orderDetailResponse = new OrderDetailsListAndProductListResponse();	
		
			
			List<OrderDetails> orderDetailsList= orderDetailsService.fetchLast3MonthOrderListByAreaIdForOfflineMode(employeeId);
			if (orderDetailsList == null) {
				httpStatus = HttpStatus.OK;
				orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailResponse.setErrorMsg("No Order Today");
				System.out.println("Sachin Offline mode start data not found");
			} else {
				orderDetailResponse.setOrderDetailList(orderDetailsList);
				orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				System.out.println("Sachin Offline mode start data mil gya found");
			}
			return new ResponseEntity<OrderDetailsListAndProductListResponse>(orderDetailResponse, httpStatus);

		}

		/**
		 * <pre>
		 * fetching cancel reason list for cancelling the order
		 * </pre>
		 * @return {@link CancelReasonResponse}
		 */
		@Transactional @GetMapping("/fetchCancelReason")
		public ResponseEntity<CancelReasonResponse> fetchCancelReason(){
			CancelReasonResponse cancelReasonResponse=new CancelReasonResponse();
			HttpStatus httpStatus;
			System.out.println("Inside fetchCancelReason ");
			
			try {
				List<CancelReason> cancelReasonsList=orderDetailsService.fetchCancelReason();
				if(cancelReasonsList==null){
					cancelReasonResponse.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus=HttpStatus.NO_CONTENT;
				}else{
					cancelReasonResponse.setStatus(Constants.SUCCESS_RESPONSE);
					cancelReasonResponse.setCancelReasons(cancelReasonsList);
					httpStatus=HttpStatus.OK;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cancelReasonResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.FORBIDDEN;
			}
			return new ResponseEntity<CancelReasonResponse>(cancelReasonResponse,httpStatus);
		}
}
