package com.bluesquare.rc.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.rest.models.TargetCommissionResponse;
import com.bluesquare.rc.rest.models.TargetMainResponse;
import com.bluesquare.rc.rest.models.TargetRequestModel;
import com.bluesquare.rc.service.TargetAssignService;
import com.bluesquare.rc.utils.Constants;

/**
 * @author Sachin Pawar 19-06-2018 Code
 * <pre>
 * API End Points
 * 1.target_list_by_periods
 * 2.target_list_by_emp/{employeeId}
 * 3.target_commission_list_by_commission_assign_id/{commissionId}
 * </pre>
 */
@Controller
public class TargetsController {

	@Autowired
	TargetAssignService targetAssignService;
	
	@Transactional 	@PostMapping("/target_list_by_periods")
	public ResponseEntity<TargetMainResponse> fetchSupplier(@RequestBody TargetRequestModel targetRequestModel){
		System.out.println("in fetchSupplier");
		
		HttpStatus httpStatus;
		TargetMainResponse targetMainResponse=new TargetMainResponse();
		
		//targetMainResponse=targetAssignService.fetchTargetAssignByTargetPeriods(targetRequestModel);
		
		if(targetMainResponse.getTargetResponseList()==null){
			targetMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			targetMainResponse.setErrorMsg("Targets Assign Not Found");
			httpStatus=HttpStatus.OK;
		}else{
			targetMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}	
		
		return new ResponseEntity<TargetMainResponse>(targetMainResponse, httpStatus);
	}
	
	/*@Transactional
	@GetMapping("target_list_by_emp/{employeeId}")
	public ResponseEntity<TargetCommissionResponse> targetListByEmpId(@ModelAttribute("employeeId") long employeeId){
		TargetCommissionResponse targetCommissionResponse=new TargetCommissionResponse();
		HttpStatus httpStatus;
		System.out.println("Inside TARGET List by Emp Id");
		
		List<TargetAssignTargets> targetAssignRegularTargets=targetAssignService.fetchCommissionAssignListByEmpId(employeeId);
		if (targetAssignRegularTargets== null) {
			
			targetCommissionResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			targetCommissionResponse.setErrorMsg("target Not assigned");
			
		}else{
			targetCommissionResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			targetCommissionResponse.setTargetAssignTargetsList(targetAssignRegularTargets);
		}
		
		return new ResponseEntity<TargetCommissionResponse>(targetCommissionResponse,httpStatus);
	}*/
	
	@Transactional
	@GetMapping("target_commission_list_by_commission_assign_id/{commissionId}")
	public ResponseEntity<TargetCommissionResponse> targetCommissionSlabListByEmpId(@ModelAttribute("commissionId") long commissionId){
		TargetCommissionResponse targetCommissionResponse=new TargetCommissionResponse();
		HttpStatus httpStatus;
		
		System.out.println("Inside TARGET List by Emp Id");
		
		List<CommissionAssignTargetSlabs> assignTargetSlabList=targetAssignService.fetchCommissionAssignTargetSlabsList(commissionId);
		if(assignTargetSlabList==null){
			targetCommissionResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			targetCommissionResponse.setErrorMsg("Commission Not assigned");
			
		}else{
			targetCommissionResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			targetCommissionResponse.setCommissionAssignTargetSlabsList(assignTargetSlabList);
		}
		
		return new ResponseEntity<TargetCommissionResponse>(targetCommissionResponse,httpStatus);
	}
	
}
