package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.rest.models.TransportationResponse;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.fetchTransportationList
 * </pre>
 */
@RestController
public class TransportationControllerApp {
	
	@Autowired
	TransportationDAO transportationDAO;
		
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	CompanyDAO companyDAO;
	
	@Transactional
	@PostMapping("/fetchTransportationList")
	public ResponseEntity<TransportationResponse> fetchTransportationList(HttpServletRequest request,
			HttpSession session) {
		TransportationResponse transportationResponse=new TransportationResponse();
		List<Transportation> transportations = transportationDAO.fetchAll();
		if(transportations.isEmpty()){
			transportationResponse.setStatus(Constants.FAILURE_RESPONSE);
			transportationResponse.setErrorMsg("Transport List Not found");
		}else{
			List<TransportationModel> transportationModelList=new ArrayList<>();
			for(Transportation transportation : transportations){
				transportationModelList.add(new TransportationModel(
						transportation.getId(), 
						transportation.getTransportName(), 
						transportation.getMobNo(), 
						transportation.getGstNo(), 
						transportation.getVehicleNo()));
			}
			transportationResponse.setStatus(Constants.SUCCESS_RESPONSE);
			transportationResponse.setTransportations(transportationModelList);
		}		
		return new ResponseEntity<TransportationResponse>(transportationResponse, HttpStatus.OK);
	}
	
	
	
}
