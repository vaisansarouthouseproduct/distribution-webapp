package com.bluesquare.rc.rcontroller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeLocation;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.models.EmployeeHrmDetailsResponse;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.rest.models.AreaListResponse;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.DeliveryBoyResponse;
import com.bluesquare.rc.rest.models.EmployeeHolidayDetailsResponse;
import com.bluesquare.rc.rest.models.EmployeeImageModal;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.EmployeeNameAndIdResponse;
import com.bluesquare.rc.rest.models.EmployeePaymentDetailsResponse;
import com.bluesquare.rc.rest.models.EmployeedetailsEntityResponse;
import com.bluesquare.rc.rest.models.LocationRequest;
import com.bluesquare.rc.rest.models.RangeRequest;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.LocationInfo;
/**
 * <pre>
 *  @author Sachin Pawar 22-05-2018 Code Documentation
 *  API End Points
 *  1.fetchAreaListByEmployeeId/{employeeId}
 *  2.fetchDeliveryBoyListByBusinessNameAreaId/{businessNameAreaId}
 *  3.fetchEmployeeDetailsByEmployeeId/{employeeId}
 *  4.saveLocation
 *  5.fetchEmployeeHrmDetails
 *  6.fetchEmployeeSalaryDetails/{startDate}/{endDate}
 *  7.fetchEmployeeHolidayDetails
 *  8.fetchEmployeeImage
 *  9.employeeImageSave
 *  </pre>
 */
@RestController
public class EmployeeDetailsController {

	 @Autowired
	    EmployeeDetailsService employeeDetailsService;
	 
	 @Autowired
	 AreaService areaService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;

	    /*@GetMapping("/fetchDeliveryBoyDetailbyAreaId/{areaId}")	    
	    public ResponseEntity<DeliveryBoyResponse> fetchDeliveryBoyDetailbyAreaId(@ModelAttribute("areaId") long areaId){
	        
	        System.out.println("fetchEmployeeDetailbyAreaId");
	        HttpStatus httpStatus;
	        DeliveryBoyResponse deliveryBoyResponse=new DeliveryBoyResponse();
	        
	        List<EmployeeDetails> deliveryBoyList=employeeDetailsService.fetchDBEmployeeDetailByAreaId(areaId);
	        
	        
	        
	        if(deliveryBoyList==null)
	        {
	            deliveryBoyResponse.setStatus(Constants.FAILURE_RESPONSE);
	            httpStatus=HttpStatus.NO_CONTENT;
	        }
	        else
	        {
	            deliveryBoyResponse.setDeliveryBoyList(deliveryBoyList);  
	            deliveryBoyResponse.setStatus(Constants.SUCCESS_RESPONSE);
	            httpStatus=HttpStatus.OK;
	        }
	        
	        return new ResponseEntity<DeliveryBoyResponse>(deliveryBoyResponse, httpStatus);
	  }*/
	    /**
	     * <pre>
	     * fetch area list by employee id
	     * which areas assigned by company at time of add/update to employee 
	     * </pre>
	     * @param token
	     * @param employeeId
	     * @return AreaListResponse
	     */
	    @Transactional 	@GetMapping("/fetchAreaListByEmployeeId/{employeeId}")	    
	    public ResponseEntity<AreaListResponse> fetchAreaListByEmployeeId(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId){
	    	
	    	System.out.println("fetchAreaListByEmployeeId");
	        HttpStatus httpStatus;
	        AreaListResponse areaListResponse=new AreaListResponse();
	      
	        
	       
				List<Area> areaList=employeeDetailsService.fetchAreaByEmployeeId(employeeId);
				
				if(areaList==null)
				{
				    areaListResponse.setStatus(Constants.FAILURE_RESPONSE);
				    httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{
					List<AreaModel> areaModelList=new ArrayList<>();
					for(Area area : areaList){
						areaModelList.add(new AreaModel(area.getAreaId(),area.getName(),area.getPincode()));
					}
					areaListResponse.setAreaList(areaModelList);
				    areaListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				    httpStatus=HttpStatus.OK;
				}
			
	        
	        return new ResponseEntity<AreaListResponse>(areaListResponse, httpStatus);
	    	
	    }
	    
	   /* @GetMapping("/fetchEmployeeDetailbyAreaId/{areaId}")	    
	    public @ResponseBody List<EmployeeDetails> fetchEmployeeDetailbyAreaId(@ModelAttribute("areaId") long areaId){
	        
	        System.out.println("fetchEmployeeDetailbyAreaId");
	        
	        
	        List<EmployeeDetails> salesPersonBoyList=employeeDetailsService.fetchSMEmployeeDetailByAreaId(areaId);
	        
	        return salesPersonBoyList; 
	  }*/
	    
	    /**
	     * <pre>
	     * fetch delivery boy list by business areaId
	     * get delivery boy/employee list which come under business area  
	     * </pre>
	     * @param token
	     * @param businessNameAreaId
	     * @return DeliveryBoyResponse
	     */
	    @Transactional 	@GetMapping("/fetchDeliveryBoyListByBusinessNameAreaId/{businessNameAreaId}")
	    public ResponseEntity<EmployeeNameAndIdResponse> fetchDeliveryBoyListByBusinessNameAreaId(@RequestHeader("Authorization") String token,@ModelAttribute("businessNameAreaId") long businessNameAreaId){
	    	
	    	System.out.println("fetchDeliveryBoyListByBusinessNameAreaId");
	    	EmployeeNameAndIdResponse  employeeNameAndIdResponse=new EmployeeNameAndIdResponse();
	    	HttpStatus httpStatus;
	    	
	    	
	    	
	    	
	    	List<EmployeeNameAndId>  employeeNameAndIdList=employeeDetailsService.fetchDeliveryBoyListByBusinessNameAreaId(businessNameAreaId);
				
			/*	if(deliveryBoyList==null){
					deliveryBoyResponse.setStatus(Constants.FAILURE_RESPONSE);
					    httpStatus=HttpStatus.NO_CONTENT;
				}*/
				employeeNameAndIdResponse.setStatus(Constants.SUCCESS_RESPONSE);
				employeeNameAndIdResponse.setEmployeeNameAndIdList(employeeNameAndIdList);
				 httpStatus=HttpStatus.OK;
			
	    	
	    	return new ResponseEntity<EmployeeNameAndIdResponse>(employeeNameAndIdResponse,httpStatus);
	    }
	    /**
	     * <pre>
	     * fetch employee details by employee id
	     * </pre>
	     * @param token
	     * @param employeeId
	     * @return EmployeedetailsEntityResponse
	     */
	    @Transactional 	@GetMapping("/fetchEmployeeDetailsByEmployeeId/{employeeId}")
    public ResponseEntity<EmployeedetailsEntityResponse> fetchEmployeeDetailsByEmployeeId(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId")long employeeId){
	    	
	    	EmployeedetailsEntityResponse employeedetailsEntityResponse =new EmployeedetailsEntityResponse();
	    	HttpStatus httpStatus;
	    	
	    	
	    	
	    	
				EmployeeDetails employeeDetailList=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				String areaList=areaService.fetchAreaListByEmployeeId(employeeId);
				if(employeeDetailList==null)
				{
					employeedetailsEntityResponse.setStatus(Constants.FAILURE_RESPONSE);
					employeedetailsEntityResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{
					employeedetailsEntityResponse.setStatus(Constants.SUCCESS_RESPONSE);
					employeedetailsEntityResponse.setEmployeeDetails(employeeDetailList);
					employeedetailsEntityResponse.setEmployeeAreaListString(areaList);
					httpStatus=HttpStatus.OK;
				}
			
	    	return new ResponseEntity<EmployeedetailsEntityResponse>(employeedetailsEntityResponse,httpStatus);
	    	
	    }
	    /**
	     * <pre>
	     * save employee location(latitude,longitude,date,address,info,isNewRoute)
	     * </pre>
	     * @param token
	     * @param locationRequest
	     * @return BaseDomain
	     * @throws Exception 
	     */
	    @Transactional 	@PostMapping("/saveLocation")
	    public ResponseEntity<BaseDomain> saveLocation(@RequestHeader("Authorization") String token,@RequestBody LocationRequest locationRequest) throws Exception{
	    
	    	BaseDomain baseDomain =new BaseDomain();
	    	HttpStatus httpStatus;
	    		    	
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(locationRequest.getEmployeeId());
				
				EmployeeLocation employeeLocation=new EmployeeLocation();
				employeeLocation.setLatitude(locationRequest.getLatitude());
				employeeLocation.setLongitude(locationRequest.getLongitude());
				employeeLocation.setEmployeeDetails(employeeDetails);
				employeeLocation.setDatetime(new Date(locationRequest.getDateTime()));
				employeeLocation.setLocationInfo(locationRequest.getInfo());
				//employeeLocation.setAddress(locationRequest.getAddress());
				//if(locationRequest.getAddress().isEmpty() || locationRequest.getAddress().equals("")){
					String address=LocationInfo.fetchAddressbyLatLng(locationRequest.getLatitude(),locationRequest.getLongitude()); //getAddressByGpsCoordinates(locationRequest.getLongitude(),locationRequest.getLatitude());
					employeeLocation.setAddress(address);
				/*}else{
					employeeLocation.setAddress(locationRequest.getAddress());
				}*/
				
				
				employeeLocation.setNewRoute(locationRequest.isNewRoute());
				employeeDetailsService.saveEmployeeLocation(employeeLocation);
				
				httpStatus=HttpStatus.OK;
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			 	
	    	
	    	return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	    }
	    /**
	     * <pre>
	     * fetch employee details with no of holidays and balance pay in current month for App HRM
	     * @return EmployeeHrmDetailsResponse
	     * </pre>
	     */
	    @Transactional 	@PostMapping("/fetchEmployeeHrmDetails")
	    public ResponseEntity<EmployeeHrmDetailsResponse> fetchEmployeeHrmDetails(){
	    
	    	EmployeeHrmDetailsResponse employeeHrmDetailsResponse=employeeDetailsService.fetchEmployeeLeaveAndBalanceAmountForAPP(tokenHandlerDAO.getAppLoggedEmployeeId());
	    	HttpStatus httpStatus;
			httpStatus=HttpStatus.OK;
			employeeHrmDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			
			return new ResponseEntity<EmployeeHrmDetailsResponse>(employeeHrmDetailsResponse,httpStatus);
	    }
	    /**
	     * <pre>
		 * get employee deductions,incentives,total amount,paid amount ,unpaid amount between given start and end date
		 * </pre>
	     * @param startDate
	     * @param endDate
	     * @return EmployeePaymentDetailsResponse
	     */
	    @Transactional 	@GetMapping("/fetchEmployeeSalaryDetails/{startDate}/{endDate}")
	    public ResponseEntity<EmployeePaymentDetailsResponse> fetchEmployeeSalaryDetails(@ModelAttribute("startDate") String startDate,@ModelAttribute("endDate") String endDate){
	    
	    	EmployeePaymentDetailsResponse employeePaymentDetailsResponse=employeeDetailsService.fetchEmployeeSalaryDetails(startDate, endDate);
	    	HttpStatus httpStatus;
			httpStatus=HttpStatus.OK;
			employeePaymentDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			
			return new ResponseEntity<EmployeePaymentDetailsResponse>(employeePaymentDetailsResponse,httpStatus);
	    }
	    /**
	     * <pre>
		 * fetch employee holiday details no of paid/unpaid/total holidays and list of holidays
		 * </pre>
	     * @param startDate
	     * @param endDate
	     * @return EmployeeHolidayDetailsResponse
	     */
	    @Transactional 	@PostMapping("/fetchEmployeeHolidayDetails")
	    public ResponseEntity<EmployeeHolidayDetailsResponse> fetchEmployeeHolidayDetails(@RequestBody RangeRequest shopVisitRequest){
	    	
	    	String range=shopVisitRequest.getRange();
	    	String startDate=shopVisitRequest.getFromDate();
	    	String endDate=shopVisitRequest.getToDate();
	    
	    	EmployeeHolidayDetailsResponse employeeHolidayDetailsResponse=employeeDetailsService.fetchEmployeeHolidayDetailsForApp(range, startDate, endDate);
	    	HttpStatus httpStatus;
			httpStatus=HttpStatus.OK;
			if(employeeHolidayDetailsResponse.getEmployeeHolidayList()==null){
				employeeHolidayDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
				employeeHolidayDetailsResponse.setErrorMsg("Holiday List Not Found");
			}else{
				employeeHolidayDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			}
			
			return new ResponseEntity<EmployeeHolidayDetailsResponse>(employeeHolidayDetailsResponse,httpStatus);
	    }
	    
	    
	    /**
		 *
		 * @param lng
		 * @param lat
		 * @return
	     * @throws IOException 
	     * @throws MalformedURLException 
		 */
		@SuppressWarnings("finally")
		public static String getAddressByGpsCoordinates(String lng, String lat) throws MalformedURLException, IOException {

			URL url = null;
			try {
				/*url = new URL(
						"http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true");*/
				
				url = new URL(
						"https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key='"+Constants.MAP_API_KEY+"'");
				
				
			
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			HttpURLConnection urlConnection = null;
			try {
				urlConnection = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String formattedAddress = "";
			
			InputStream in=null;
			
			try {
				in = (InputStream) urlConnection.getInputStream();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String result, line = reader.readLine();
				result = line;
				while ((line = reader.readLine()) != null) {
					result += line;
				}

				JSONParser parser = new JSONParser();
				JSONObject rsp = (JSONObject) parser.parse(result);

				if (rsp.containsKey("results")) {
					JSONArray matches = (JSONArray) rsp.get("results");
					JSONObject data = (JSONObject) matches.get(0); // TODO: check if
																	// idx=0 exists
					formattedAddress = (String) data.get("formatted_address");
				}

				return "";
			} finally {
				urlConnection.disconnect();
				return formattedAddress;
			}
		}
		
		public static void main(String[] args) throws Exception {
			System.out.println(getAddressByGpsCoordinates("19.2314854","72.8626712"));;
		}
		/**
		 * fetch employee image by employeeId
		 * @return employeeImage
		 */
		@Transactional 	@PostMapping("/fetchEmployeeImage")
	    public ResponseEntity<EmployeeImageModal> fetchEmployeeImage() {
				EmployeeImageModal employeeImageModal=new EmployeeImageModal();
				EmployeeProfilePicture employeeProfilePicture=employeeDetailsService.fetchEmployeeImage();	
				employeeImageModal.setEmployeeImage(employeeProfilePicture.getUserImgBase64());
				employeeImageModal.setStatus(Constants.SUCCESS_RESPONSE);
	    	return new ResponseEntity<EmployeeImageModal>(employeeImageModal,HttpStatus.OK);
	    }
		/**
		 * update employee image
		 * @param employeeImageModal
		 * @return BaseDomain with status success
		 */
		@Transactional 	@PostMapping("/employeeImageSave")
	    public ResponseEntity<BaseDomain> employeeImageSave(@RequestBody EmployeeImageModal employeeImageModal) {
				
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(tokenHandlerDAO.getAppLoggedEmployeeId());	
				EmployeeProfilePicture employeeProfilePicture=employeeDetailsService.fetchEmployeeImage();	
				employeeProfilePicture.setUserImgBase64(employeeImageModal.getEmployeeImage());
				employeeProfilePicture.setEmployee(employeeDetails.getEmployee());
				employeeDetailsService.updateEmployeeProfilePicture(employeeProfilePicture);
				
				BaseDomain baseDomain=new BaseDomain();
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				
	    	return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	    }
}
