package com.bluesquare.rc.rcontroller;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * Interceptor Methods
 * 1.isIgnoreUrl
 * 2.preHandle
 * 3.postHandle
 * 4.afterCompletion
 * </pre>
 */
@Component
public class RequestProcessingInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	JsonWebToken jsonWebToken;

	@Autowired
	HttpSession session;

	//mention ignore url
	static String[] ignoreUrls = { "downloadProductImage", "login", "loginEmployee", "authentication", "resources","fetchAllCompanyList","fetch_branch_list_by_companyId","adminLogin","counterOrderInvoiceForWhatsApp.pdf"};

	public static boolean isIgnoreUrl(String serviceUrl) {

		for (String url : ignoreUrls) {
			if (url.equals(serviceUrl.trim())) {
				return true;
			}
		}

		// do split for ignore file request i.e. jquery.js,script.js,invoice.pdf
		if ((serviceUrl.split("\\.")).length > 1) {
			return true;
		}

		return false;
	}

	//before going any end point
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// set few parameters to handle ajax request from different host
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
		response.addHeader("Access-Control-Max-Age", "1000");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type");
		response.addHeader("Cache-Control", "private");

		//get requested url
		String reqUri = request.getRequestURI();
		System.out.println(" incoming request uri : " + reqUri);
		
//		String requestBody= IOUtils.toString(request.getInputStream());	
//		System.out.println(" incoming request body : " + requestBody);
		
		String urlParts[] = reqUri.split("/");
		String serviceName = "";
		try {
			//get end controllers end point
			serviceName = urlParts[2];
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//assign blanck("") for http://localhost:8080/BlueSquare/
			serviceName = "";
		}

		// App side Authorization
		String token = request.getHeader("Authorization");

		//if token not get then its panel url 
		if (token == null) {
			//check its ignore url or not
			//if its ignore url then not check authenticate
			if (!isIgnoreUrl(serviceName)) {

				String loginType = (String) session.getAttribute("loginType");
				if (loginType == null) {
					response.sendRedirect(request.getContextPath() + "/login");
					return false;
				} else {
					if (loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)) {
						EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
						if (employeeDetails == null) {
							response.sendRedirect(request.getContextPath() + "/login");
							return false;
						}
					}else if (loginType.equals(Constants.COMPANY_ADMIN)) {
						Company company=(Company)session.getAttribute("companyDetails");
						if (company == null) {
							response.sendRedirect(request.getContextPath() + "/login");
							return false;
						}
					}
				}
			}
		} else {
			// token verify
			if (!isIgnoreUrl(serviceName)) {
				if (jsonWebToken.verifyToken(token)) {
					throw new Exception("Token Invalid "+token);
				}
				session.setAttribute("authToken", token);
			}

		}
		return super.preHandle(request, response, handler);
	}
	/**
	 * after going any End Points
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		String reqUri = request.getRequestURI();
		System.out.println(" processing request uri : " + reqUri);

		super.postHandle(request, response, handler, modelAndView);
	}
	/**
	 * after end all process of any End Points
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		String reqUri = request.getRequestURI();
		System.out.println(" completed request uri : " + reqUri);
	}
}
