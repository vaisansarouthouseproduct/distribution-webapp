package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BusinessNameModel;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;
import com.bluesquare.rc.responseEntities.CityModel;
import com.bluesquare.rc.responseEntities.RegionModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.BusinessNameListRequest;
import com.bluesquare.rc.rest.models.BusinessNameListResponse;
import com.bluesquare.rc.rest.models.BusinessNameResponse;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.saveBusinessNameForApp
 * 2.updateBusinessNameForApp
 * 3.fetchBusinessNameListForApp
 * 4.fetchBusinessNameBybusinessNameIdForApp/{businessNameId}
 * 5.businessNameListByAreaIdAndBusinessTypeId
 * 6.fetchBusinessNameByAreaId/{areaId}
 * </pre>
 */
@RestController
public class BusinessNameController {

	@Autowired
	BusinessName businessName;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	/**
	 * <pre>
	 * check duplication (telephoneNumber,gstNo,mobileNumber,emailId)
	 * set generated id,
	 * set tax type(inter ,intra),
	 * send sms on register mobile number about registration
	 * </pre>
	 * @param businessName
	 * @param token
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/saveBusinessNameForApp")
	public ResponseEntity<BaseDomain> saveBusinessName(@RequestBody BusinessName businessName,HttpServletRequest request,
													   @RequestHeader("Authorization") String token)
	{
		
		System.out.println("inside saveBusinessName");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
		/*String response=businessNameService.checkBusinessDuplication(businessName.getContact().getMobileNumber(), "mobileNumber", "0");
		if(response.equals("Failed")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Mobile Number Already Exist");
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		if(businessName.getContact().getTelephoneNumber()!=null){
			if(!businessName.getContact().getTelephoneNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getTelephoneNumber(), "telephoneNumber", "0");
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Telephone Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getGstinNumber()!=null){
			if(!businessName.getGstinNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getGstinNumber(), "gstNo", "0");	
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("GST Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getContact().getEmailId()!=null){
			if(!businessName.getContact().getEmailId().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getEmailId(), "emailId", "0");
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Email Id Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}*/
		String[] branchIds=request.getParameterValues("branchIdList");
		for(String branchId : branchIds){
			Branch branch=branchService.fetchBranchByBranchId(Long.parseLong(branchId));
			businessName.setBranch(branch);
			BusinessName businessName2=new BusinessName(
					businessName.getShopName(), 
					businessName.getOwnerName(), 
					businessName.getComment(), 
					businessName.getOther(), 
					businessName.getCreditLimit(), 
					businessName.getGstinNumber(),
					businessName.getBusinessType(),
					businessName.getAddress(), 
					businessName.getTaxType(),
					businessName.getContact(), 
					businessName.getArea(), 
					businessName.getEmployeeSM(),
					businessName.getBusinessAddedDatetime(), 
					businessName.getBusinessUpdatedDatetime(),
					businessName.getStatus(), 
					businessName.getBadDebtsStatus(),
					businessName.getAmountLoss(),
					businessName.getBadDebtsDatetime(),
					businessName.getDisableDatetime(), 
					businessName.getCompany(),
					branch,
					businessName.getAmountPending());
			businessNameService.saveBusinessName(businessName2);
		}
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	/**
	 * <pre>
	 * check duplication (telephoneNumber,gstNo,mobileNumber,emailId)
	 * set tax type(inter ,intra),
	 * send sms on register mobile number about update info
	 * </pre>
	 * @param businessName
	 * @param token
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/updateBusinessNameForApp")
	public ResponseEntity<BaseDomain> updateBusinessNameForApp(@RequestBody BusinessName businessName,
			   												   @RequestHeader("Authorization") String token)
	{
		System.out.println("inside updateBusinessNameForApp");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;		
		
		/*String response=businessNameService.checkBusinessDuplication(businessName.getContact().getMobileNumber(), "mobileNumber", businessName.getBusinessNameId());
		if(response.equals("Failed")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Mobile Number Already Exist");
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		if(businessName.getContact().getTelephoneNumber()!=null){
			if(!businessName.getContact().getTelephoneNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getTelephoneNumber(), "telephoneNumber", businessName.getBusinessNameId());
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Telephone Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getGstinNumber()!=null){
			if(!businessName.getGstinNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getGstinNumber(), "gstNo", businessName.getBusinessNameId());	
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("GST Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getContact().getEmailId()!=null){
			if(!businessName.getContact().getEmailId().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getEmailId(), "emailId", businessName.getBusinessNameId());
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Email Id Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}*/
	
			BusinessName businessNm=businessNameService.fetchBusinessForWebApp(businessName.getBusinessNameId());
			businessName.setBusinessNamePkId(businessNm.getBusinessNamePkId());
			businessName.setBusinessAddedDatetime(businessNm.getBusinessAddedDatetime());
			businessName.setDisableDatetime(businessNm.getDisableDatetime());
			businessName.setBusinessUpdatedDatetime(new Date());
			businessName.setStatus(businessNm.getStatus());
			businessName.setCompany(businessNm.getCompany());
			businessName.setBranch(businessNm.getBranch());
			businessName.setBadDebtsStatus(businessNm.getBadDebtsStatus());
			businessNameService.updateForWebApp(businessName);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
		
	}
	
	@Transactional 	@PostMapping("/fetchBusinessNameListForApp")
	public ResponseEntity<BusinessNameListResponse> fetchBusinessNameList(@RequestHeader("Authorization") String token)
	{
		System.out.println("inside fetchBusinessNameListForApp");
		BusinessNameListResponse businessNameListResponse=new BusinessNameListResponse();
		HttpStatus httpStatus;	
	
		
			List<BusinessName> businessNamesList=businessNameService.getBusinessNameListForApp();
			if(businessNamesList==null)
			{
				businessNameListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				List<BusinessNameModel> businessNameModelList=new ArrayList<>();
				for(BusinessName businessName :businessNamesList){
					businessNameModelList.add(new BusinessNameModel(
							businessName.getBusinessNamePkId(), 
							businessName.getBusinessNameId(), 
							businessName.getShopName(), 
							businessName.getOwnerName(), 
							businessName.getCreditLimit(), 
							businessName.getGstinNumber(), 
							new BusinessTypeModel(businessName.getBusinessType().getBusinessTypeId(), businessName.getBusinessType().getName()), 
							businessName.getAddress(), 
							businessName.getTaxType(), 
							businessName.getContact(), 
							new AreaModel(
									businessName.getArea().getAreaId(), 
									businessName.getArea().getName(), 
									businessName.getArea().getPincode(),
									new RegionModel(
											businessName.getArea().getRegion().getRegionId(), 
											businessName.getArea().getRegion().getName(), 
											new CityModel(
													businessName.getArea().getRegion().getCity().getCityId(), 
													businessName.getArea().getRegion().getCity().getName()))), 
							businessName.getEmployeeSM()==null?null:
							new EmployeeNameAndId(businessName.getEmployeeSM().getEmployeeId(), employeeDetailsDAO.getEmployeeDetailsByemployeeId(businessName.getEmployeeSM().getEmployeeId()).getName()),
							businessName.getStatus(),
							businessName.getBadDebtsStatus()));
				}
				businessNameListResponse.setBusinessNamesList(businessNameModelList);
				businessNameListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			
			return new ResponseEntity<BusinessNameListResponse>(businessNameListResponse, httpStatus);
		
				
	}
	
	@Transactional 	@GetMapping("/fetchBusinessNameBybusinessNameIdForApp/{businessNameId}")
	public ResponseEntity<BusinessNameResponse> fetchBusinessNameNameBybusinessNameId(@ModelAttribute("businessNameId") String businessNameId,
																					  @RequestHeader("Authorization") String token)
	{
		System.out.println("inside fetchBusinessNameBybusinessNameIdForApp");
		BusinessNameResponse businessNameResponse=new BusinessNameResponse();
		HttpStatus httpStatus;	
		
		
		
		
			BusinessName businessName=businessNameService.fetchBusinessForWebApp(businessNameId);
			if(businessName==null)
			{
				businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
				businessNameResponse.setErrorMsg(Constants.NOT_FOUND);
				httpStatus=HttpStatus.OK;
			}
			else
			{
				if(businessName.getStatus())
				{
					businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
					businessNameResponse.setErrorMsg("This Business Is Disable by Admin");
					httpStatus=HttpStatus.OK;
				}
				else
				{
					businessNameResponse.setBusinessName(new BusinessNameModel(
							businessName.getBusinessNamePkId(), 
							businessName.getBusinessNameId(), 
							businessName.getShopName(), 
							businessName.getOwnerName(), 
							businessName.getCreditLimit(), 
							businessName.getGstinNumber(), 
							new BusinessTypeModel(businessName.getBusinessType().getBusinessTypeId(), businessName.getBusinessType().getName()), 
							businessName.getAddress(), 
							businessName.getTaxType(), 
							businessName.getContact(), 
							new AreaModel(
									businessName.getArea().getAreaId(), 
									businessName.getArea().getName(), 
									businessName.getArea().getPincode(),
									new RegionModel(
											businessName.getArea().getRegion().getRegionId(), 
											businessName.getArea().getRegion().getName(), 
											new CityModel(
													businessName.getArea().getRegion().getCity().getCityId(), 
													businessName.getArea().getRegion().getCity().getName()))), 
							businessName.getEmployeeSM()==null?null:
							new EmployeeNameAndId(businessName.getEmployeeSM().getEmployeeId(), employeeDetailsDAO.getEmployeeDetailsByemployeeId(businessName.getEmployeeSM().getEmployeeId()).getName()),
							businessName.getStatus(),
							businessName.getBadDebtsStatus()));
					businessNameResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
			}
			return new ResponseEntity<BusinessNameResponse>(businessNameResponse, httpStatus);
		
				
	}
	/**
	 * <pre>
	 * not used
	 * filter business list with areaId,businessType id
	 * </pre>
	 * @param businessnameListRequest
	 * @param token
	 * @return BusinessNameListResponse
	 */
	@Transactional 	@PostMapping("/businessNameListByAreaIdAndBusinessTypeId")
    public ResponseEntity<BusinessNameListResponse> BusinessNameByAreaIdAndBusinessTypeId(@RequestBody BusinessNameListRequest businessnameListRequest ,@RequestHeader("Authorization") String token){
        
        System.out.println("business Name By businessTypeId and AreaId ");
        BusinessNameListResponse businessNameListResponse=new BusinessNameListResponse();		
		HttpStatus httpStatus;
		
		
		
       
			  
			List<BusinessName> businessNameList=businessNameService.businessNameByAreaIdAndBusinessTypeIdForWebApp(businessnameListRequest.getBusinessTypeId(),businessnameListRequest.getAreaId());
			
			if (businessNameList==null) {
			    businessNameListResponse.setStatus(Constants.FAILURE_RESPONSE);
			    businessNameListResponse.setErrorMsg(Constants.NOT_FOUND);
			    httpStatus = HttpStatus.OK;
			}
			else
			{
				Iterator<BusinessName> itr=businessNameList.iterator();
				while(itr.hasNext())
				{
					if(itr.next().getStatus())
					{
						itr.remove();
					}
				}
				List<BusinessNameModel> businessNameModelList=new ArrayList<>();
				for(BusinessName businessName :businessNameList){
					businessNameModelList.add(new BusinessNameModel(
							businessName.getBusinessNamePkId(), 
							businessName.getBusinessNameId(), 
							businessName.getShopName(), 
							businessName.getOwnerName(), 
							businessName.getCreditLimit(), 
							businessName.getGstinNumber(), 
							new BusinessTypeModel(businessName.getBusinessType().getBusinessTypeId(), businessName.getBusinessType().getName()), 
							businessName.getAddress(), 
							businessName.getTaxType(), 
							businessName.getContact(), 
							new AreaModel(
									businessName.getArea().getAreaId(), 
									businessName.getArea().getName(), 
									businessName.getArea().getPincode(),
									new RegionModel(
											businessName.getArea().getRegion().getRegionId(), 
											businessName.getArea().getRegion().getName(), 
											new CityModel(
													businessName.getArea().getRegion().getCity().getCityId(), 
													businessName.getArea().getRegion().getCity().getName()))), 
							businessName.getEmployeeSM()==null?null:
							new EmployeeNameAndId(businessName.getEmployeeSM().getEmployeeId(), employeeDetailsDAO.getEmployeeDetailsByemployeeId(businessName.getEmployeeSM().getEmployeeId()).getName()),
							businessName.getStatus(),
							businessName.getBadDebtsStatus()));
				}
				businessNameListResponse.setBusinessNamesList(businessNameModelList);
			    businessNameListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			    httpStatus = HttpStatus.OK;
			}
			
			
			return new ResponseEntity<BusinessNameListResponse>(businessNameListResponse, httpStatus);
		
    }
	/**
	 * <pre>
	 * filter business list with areaId
	 * </pre>
	 * @param businessnameListRequest
	 * @param token
	 * @return BusinessNameListResponse
	 */
	@Transactional 	@GetMapping("/fetchBusinessNameByAreaId/{areaId}")
    public ResponseEntity<BusinessNameListResponse> fetchBusinessNameByAreaId(@ModelAttribute("areaId") long areaId,@RequestHeader("Authorization") String token){
		System.out.println("inside fetchBusinessNameBybusinessNameIdForApp");
		BusinessNameListResponse businessNameResponse=new BusinessNameListResponse();
		HttpStatus httpStatus;

		
		
		
				
			List<BusinessName> businessNamesList=businessNameService.fetchBusinessNameByAreaId(areaId);
			
			if(businessNamesList==null){
				
				businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
				businessNameResponse.setErrorMsg(Constants.NOT_FOUND);
				httpStatus=HttpStatus.OK;
				  return new ResponseEntity<BusinessNameListResponse>(businessNameResponse,httpStatus);
				
			}else{
				
				List<BusinessNameModel> businessNameModelList=new ArrayList<>();
				for(BusinessName businessName :businessNamesList){
					businessNameModelList.add(new BusinessNameModel(
							businessName.getBusinessNamePkId(), 
							businessName.getBusinessNameId(), 
							businessName.getShopName(), 
							businessName.getOwnerName(), 
							businessName.getCreditLimit(), 
							businessName.getGstinNumber(), 
							new BusinessTypeModel(businessName.getBusinessType().getBusinessTypeId(), businessName.getBusinessType().getName()), 
							businessName.getAddress(), 
							businessName.getTaxType(), 
							businessName.getContact(), 
							new AreaModel(
									businessName.getArea().getAreaId(), 
									businessName.getArea().getName(), 
									businessName.getArea().getPincode(),
									new RegionModel(
											businessName.getArea().getRegion().getRegionId(), 
											businessName.getArea().getRegion().getName(), 
											new CityModel(
													businessName.getArea().getRegion().getCity().getCityId(), 
													businessName.getArea().getRegion().getCity().getName()))), 
							businessName.getEmployeeSM()==null?null:
							new EmployeeNameAndId(businessName.getEmployeeSM().getEmployeeId(), employeeDetailsDAO.getEmployeeDetailsByemployeeId(businessName.getEmployeeSM().getEmployeeId()).getName()),
							businessName.getStatus(),
							businessName.getBadDebtsStatus()));
				}
				businessNameResponse.setBusinessNamesList(businessNameModelList);
				//businessNameResponse.setBusinessName(businessName);
				businessNameResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				return new ResponseEntity<BusinessNameListResponse>(businessNameResponse,httpStatus);
			}
		
}

}
