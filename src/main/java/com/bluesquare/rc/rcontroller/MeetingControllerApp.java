package com.bluesquare.rc.rcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.models.MeetingScheduleModel;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BusinessNameModel;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;
import com.bluesquare.rc.responseEntities.MeetingModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.MeetingListResponse;
import com.bluesquare.rc.rest.models.MeetingRequest;
import com.bluesquare.rc.rest.models.MeetingScheduleListResponse;
import com.bluesquare.rc.rest.models.RangeRequest;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.MeetingService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar Code Documentation 19-06-2018
 * API End Points
 * 1.save_meeting_app
 * 2.update_meeting_app
 * 3.fetch_meeting_app/{meetingId}
 * 4.delete_meeting_app/{meetingId}
 * 5.cancel_meeting_app
 * 6.fetch_scheduled_meeting_app/{departmentId}/{pickDate}
 * 7.fetchMeetingsByEmployeeIdAndPickDateForApp/{employeeDetailsId}/{pickDate}
 * 8.fetchMeetingsByEmployeeIdAndPickDateForApp/{employeeId}/{pickDate}
 * 9.meetingCompleted
 * 10.cancelledMeetingsReport
 * 11.completeMeetingsReport
 * 12.pendingMeetingsReport
 * 13.rescheduleTheMeeting
 * </pre>
 */
@RestController
public class MeetingControllerApp {

	@Autowired
	MeetingService meetingService;
	
	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	/**
	 * <pre>
	 * validation of already meeting exist on that time or not
	 * save meeting by existing business or new business
	 * </pre>
	 * @param meeting
	 * @param session
	 * @param request
	 * @return BaseDomain with message
	 * @throws ParseException
	 */
	@Transactional 	@PostMapping("/save_meeting_app")
	 public ResponseEntity<BaseDomain> saveMeeting(@RequestBody Meeting meeting,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("In Save meeting");
				
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
		
		long employeeId=meeting.getEmployeeDetails().getEmployee().getEmployeeId();
		String date=sdfDate.format(meeting.getMeetingFromDateTime());
		String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
		String toTime=sdfTime.format(meeting.getMeetingToDateTime());
		
		String msg=meetingService.checkMeetingSchedule(employeeId, date, fromTime, toTime);
		BaseDomain baseDomain=new BaseDomain();
		if(!msg.equals("")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg(msg);
			return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
		}
		
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getEmployeeDetails().getEmployee().getEmployeeId());
		
		meeting.setEmployeeDetails(employeeDetails);
		
		if(meeting.getBusinessName()!=null){
			meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(meeting.getBusinessName().getBusinessNameId()));
		}

		meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		meeting.setMeetingStatus(Constants.MEETING_PENDING);
		meeting.setAddedDateTime(new Date());
		meeting.setUpdatedDateTime(null);
		meeting.setMeetingStatus(Constants.MEETING_PENDING);
		
		meetingService.saveMeeting(meeting);
		
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	/**
	 * <pre>
	 * validation for check meeting already exist on that time or not
	 * update meeting by existing business or new business
	 * </pre>
	 * @param meeting
	 * @param session
	 * @param request
	 * @return BaseDomain with message
	 * @throws ParseException
	 */
	@Transactional 	@PostMapping("/update_meeting_app")
	 public ResponseEntity<BaseDomain> updateMeeting(@RequestBody Meeting meeting,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("In Update meeting");
		
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
		
		long employeeId=meeting.getEmployeeDetails().getEmployee().getEmployeeId();
		String date=sdfDate.format(meeting.getMeetingFromDateTime());
		String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
		String toTime=sdfTime.format(meeting.getMeetingToDateTime());
		
		String msg=meetingService.checkMeetingScheduleForUpdate(employeeId, date, fromTime, toTime,meeting.getMeetingId());
		BaseDomain baseDomain=new BaseDomain();
		if(!msg.equals("")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg(msg);
			return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
		}
		
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getEmployeeDetails().getEmployee().getEmployeeId());
		
		meeting.setEmployeeDetails(employeeDetails);
		
		if(meeting.getBusinessName()!=null){
			meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(meeting.getBusinessName().getBusinessNameId()));
		}
		

		meeting.setMeetingStatus(Constants.MEETING_PENDING);
		meeting.setUpdatedDateTime(new Date());
		
		meetingService.updateMeeting(meeting);
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	/**
	 * <pre>
	 * fetch meeting details by meetingId
	 * </pre>
	 * @param meetingId
	 * @param session
	 * @param request
	 * @return MeetingResponse
	 * @throws ParseException
	 */
	@Transactional 	@GetMapping("/fetch_meeting_app/{meetingId}")
	 public ResponseEntity<MeetingListResponse> fetchMeeting(@ModelAttribute("meetingId") long meetingId,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("In fetch meeting");
				
		Meeting meeting=meetingService.fetchMeetingByMeetingId(meetingId);

		Calendar calendarDb=Calendar.getInstance();
		calendarDb.set(Calendar.HOUR_OF_DAY, 0);
		calendarDb.set(Calendar.MINUTE, 0);
		calendarDb.set(Calendar.SECOND, 0);
		
		Calendar calendarCurrent=Calendar.getInstance();
		calendarCurrent.set(Calendar.HOUR_OF_DAY, 0);
		calendarCurrent.set(Calendar.MINUTE, 0);
		calendarCurrent.set(Calendar.SECOND, 0);
				
		MeetingListResponse meetingListResponse=new MeetingListResponse();
		//MeetingResponse meetingResponse=new MeetingResponse();
		if (calendarDb.before(calendarCurrent) || !meeting.getMeetingStatus().equals(Constants.MEETING_PENDING)) {
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meeting Expired Or Cancelled");
		}else{
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			meetingListResponse.setMeeting(new MeetingModel(
												meeting.getMeetingId(), 
												null, 
												meeting.getBusinessName()==null?null:
												new BusinessNameModel(
														meeting.getBusinessName().getBusinessNamePkId(), 
														meeting.getBusinessName().getBusinessNameId(), 
														meeting.getBusinessName().getShopName(), 
														meeting.getBusinessName().getOwnerName(), 
														meeting.getBusinessName().getCreditLimit(), 
														meeting.getBusinessName().getGstinNumber(), 
														new BusinessTypeModel(
																meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
																meeting.getBusinessName().getBusinessType().getName()), 
														meeting.getBusinessName().getAddress(), 
														meeting.getBusinessName().getTaxType(), 
														meeting.getBusinessName().getContact(), 
														new AreaModel(
																meeting.getBusinessName().getArea().getAreaId(), 
																meeting.getBusinessName().getArea().getName(), 
																meeting.getBusinessName().getArea().getPincode()), 
														meeting.getBusinessName().getEmployeeSM()==null?null:
														new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
														meeting.getBusinessName().getStatus(),
														meeting.getBusinessName().getBadDebtsStatus()), 
												meeting.getOwnerName(), 
												meeting.getShopName(), 
												meeting.getMobileNumber(), 
												meeting.getAddress(), 
												meeting.getMeetingVenue(), 
												meeting.getCancelReason(), 
												meeting.getMeetingStatus(), 
												meeting.getMeetingReview(), 
												meeting.getMeetingFromDateTime(), 
												meeting.getMeetingToDateTime(), 
												meeting.getAddedDateTime(), 
												meeting.getUpdatedDateTime(), 
												meeting.getCancelDateTime()));
		}
		
		return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	}
	/**
	 * <pre>
	 * delete meeting permanent
	 * </pre>
	 * @param meetingId
	 * @param session
	 * @param request
	 * @return BaseDomain with message and status
	 * @throws ParseException
	 */
	@Transactional 	@PostMapping("/delete_meeting_app/{meetingId}")
	 public ResponseEntity<BaseDomain> deleteMeeting(@ModelAttribute("meetingId") long meetingId,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("In delete meeting");
				
		meetingService.deleteMeeting(meetingId);
		
		BaseDomain baseDomain=new BaseDomain();
		baseDomain.setStatus(Constants.SAVE_SUCCESS);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	/**
	 * <pre>
	 * cancel meeting and change status of meeting to true
	 * record not delet from db
	 * </pre>
	 * @param meetingId
	 * @param session
	 * @param request
	 * @return BaseDomain with message and status
	 * @throws ParseException
	 */
	@Transactional 	@PostMapping("/cancel_meeting_app")
	 public ResponseEntity<BaseDomain> cancelMeeting(@RequestBody MeetingRequest meetingRequest,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("In cancel meeting");
				
		long meetingId=meetingRequest.getMeetingId();
		String cancelReason=meetingRequest.getCancelReason();
		meetingService.updateCancelStatus(meetingId,cancelReason);
		
		BaseDomain baseDomain=new BaseDomain();
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	/**
	 * <pre>
	 * find meeting scheduled or not given date and department for employees
	 * </pre>
	 * @param departmentId
	 * @param pickDate
	 * @param session
	 * @param request
	 * @return MeetingScheduleListResponse
	 * @throws ParseException
	 */
	 @Transactional 	
	 @GetMapping("/fetch_scheduled_meeting_app/{departmentId}/{pickDate}")
	 public ResponseEntity<MeetingScheduleListResponse> fetchScheduleMeeting(@ModelAttribute("departmentId") long departmentId,@ModelAttribute("pickDate") String pickDate,HttpSession session,HttpServletRequest request) throws ParseException {
		System.out.println("Employee Scheduled Meetings");
		
		List<MeetingScheduleModel> meetingScheduleList=meetingService.fetchEmployeeWiseScheduledMeeting(departmentId, pickDate);
		
		MeetingScheduleListResponse meetingScheduleListResponse=new MeetingScheduleListResponse();
		
		if(meetingScheduleList==null){
			meetingScheduleListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingScheduleListResponse.setErrorMsg("Mettings Not Found");
		}else{
			meetingScheduleListResponse.setMeetingScheduleList(meetingScheduleList);
			meetingScheduleListResponse.setStatus(Constants.SAVE_SUCCESS);
		}
		
		return new ResponseEntity<MeetingScheduleListResponse>(meetingScheduleListResponse,HttpStatus.OK);
	}
	 /**
	  * <pre>
	  * fetch scheduled meeting by pick-date and employeeDetails id
	  * </pre>
	  * @param employeeDetailsId
	  * @param pickDate
	  * @param session
	  * @param request
	  * @return MeetingListResponse
	  * @throws ParseException
	  */
	 @Transactional @PostMapping("/fetchMeetingsByEmployeeIdAndPickDateForApp/{employeeDetailsId}/{pickDate}")
	 public ResponseEntity<MeetingListResponse> fetchMeetingsByEmployeeIdAndPickDate(@ModelAttribute("employeeDetailsId") long employeeDetailsId,@ModelAttribute("pickDate") String pickDate,HttpSession session,HttpServletRequest request) throws ParseException {
		
		System.out.println("Employee Scheduled Meetings");
		
		MeetingListResponse meetingListResponse=new MeetingListResponse();
		
		List<Meeting> meetingList=meetingService.fetchMeetingListbyEmployeeIdAndDate(employeeDetailsId, pickDate);
		
		if(meetingList==null){
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meetings Not Found");
		}else{
			List<MeetingModel> meetingModelList=new ArrayList<>();
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null, 
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()), 
								meeting.getBusinessName().getEmployeeSM()==null?null:
									new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
			meetingListResponse.setMeetingList(meetingModelList);
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
		return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	}
	 
	 
	 /**
	  * <pre>
	  * fetch scheduled meeting by pick-date and employee id
	  * </pre>
	  * @param employeeDetailsId
	  * @param pickDate
	  * @param session
	  * @param request
	  * @return MeetingListResponse
	  * @throws ParseException
	  */
	 @Transactional 
	 @GetMapping("/fetchMeetingsByEmployeeIdAndPickDateForApp/{employeeId}/{pickDate}")
	 public ResponseEntity<MeetingListResponse> fetchMeetingListByEmployeeIdAndPickDate(@ModelAttribute("employeeId") long employeeId,@ModelAttribute("pickDate") String pickDate,HttpSession session,HttpServletRequest request) throws ParseException {
		
		System.out.println("Employee Scheduled Meetings");
		
		MeetingListResponse meetingListResponse=new MeetingListResponse();
		
		List<Meeting> meetingList=meetingService.fetchMeetingsbyEmployeeIdAndDate(employeeId, pickDate);
		
		if(meetingList==null){
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meetings Not Found");
		}else{
			List<MeetingModel> meetingModelList=new ArrayList<>();
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null,
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()), 
								meeting.getBusinessName().getEmployeeSM()==null?null:
								new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
			meetingListResponse.setMeetingList(meetingModelList);
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
		return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	}
	 
	 
	/**
	 * <pre>
	 * fetch scheduled meeting by pick-date and employee id
	 * </pre>
	 * @param meetingRequest
	 * @param session
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	 @Transactional 
	 @PostMapping("/meetingCompleted")
	 public ResponseEntity<BaseDomain> meetingCompleted(@RequestBody MeetingRequest meetingRequest,HttpSession session,HttpServletRequest request) throws ParseException {
		 BaseDomain baseDomain=new BaseDomain();
		 HttpStatus httpStatus;
		 System.out.println("meetingCompleted");
		 
		 if (meetingRequest.getIsNextMeetScheduled().equalsIgnoreCase("No")) {
			
			 meetingService.meetingCompleteWithReview(meetingRequest);
			
		}else {
			
			meetingService.meetingCompleteWithReview(meetingRequest);
			
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
			
			Meeting meeting=meetingRequest.getNewMeeting();
			long employeeId=meeting.getEmployeeDetails().getEmployee().getEmployeeId();
			String date=sdfDate.format(meeting.getMeetingFromDateTime());
			String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
			String toTime=sdfTime.format(meeting.getMeetingToDateTime());
			
			String msg=meetingService.checkMeetingSchedule(employeeId, date, fromTime, toTime);
	
			if(!msg.equals("")){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(msg);
				return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
			}
			
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getEmployeeDetails().getEmployee().getEmployeeId());
			
			meeting.setEmployeeDetails(employeeDetails);
			
			if(meeting.getBusinessName()!=null){
				meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(meeting.getBusinessName().getBusinessNameId()));
			}
			
			meeting.setAddedDateTime(new Date());
			meeting.setMeetingStatus(Constants.MEETING_PENDING);
			
			meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
			meetingService.saveMeeting(meeting);
		}
		 baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		 httpStatus=HttpStatus.OK;
		 return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	 }
	 /**
	  * <pre>
	  * fetch cancelled meeting list by given range
	  * </pre>
	  * @param rangeRequest
	  * @param request
	  * @return
	  */
	 @Transactional
	 @RequestMapping("/cancelledMeetingsReport")
	 public ResponseEntity<MeetingListResponse> fetchCancelReport(@RequestBody RangeRequest rangeRequest,HttpServletRequest request){
		 
		 MeetingListResponse meetingListResponse=new MeetingListResponse();
		 
		 List<Meeting> meetingList=meetingService.fetchCancelledMeetingList(rangeRequest.getRange(), rangeRequest.getFromDate(), rangeRequest.getToDate());
		 
		 if(meetingList==null){
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meetings Not Found");
		}else{
			List<MeetingModel> meetingModelList=new ArrayList<>();
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null, 
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()), 
								meeting.getBusinessName().getEmployeeSM()==null?null:
									new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
			meetingListResponse.setMeetingList(meetingModelList);
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		 
		 return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	 }
	 
	 /**
	  * <pre>
	  * fetch completed meeting list by given range
	  * </pre>
	  * @param rangeRequest
	  * @param request
	  * @return
	  */
	 @Transactional
	 @RequestMapping("/completeMeetingsReport")
	 public ResponseEntity<MeetingListResponse> fetchCompletedReport(@RequestBody RangeRequest rangeRequest,HttpServletRequest request){
		 
		 MeetingListResponse meetingListResponse=new MeetingListResponse();
		 
		 List<Meeting> meetingList=meetingService.fetchCompletedMeetingList(rangeRequest.getRange(), rangeRequest.getFromDate(), rangeRequest.getToDate());
		 
		 if(meetingList==null){
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meetings Not Found");
		}else{
			List<MeetingModel> meetingModelList=new ArrayList<>();
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null, 
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()), 
								meeting.getBusinessName().getEmployeeSM()==null?null:
									new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
			meetingListResponse.setMeetingList(meetingModelList);
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		 
		 return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	 }
	 
	 /**
	  * <pre>
	  * fetch pending meeting list by given range
	  * </pre>
	  * @param rangeRequest
	  * @param request
	  * @return
	  */
	 @Transactional
	 @RequestMapping("/pendingMeetingsReport")
	 public ResponseEntity<MeetingListResponse> fetchPendingReport(@RequestBody RangeRequest rangeRequest,HttpServletRequest request){
		 
		 MeetingListResponse meetingListResponse=new MeetingListResponse();
		 
		 List<Meeting> meetingList=meetingService.fetchPendingMeetingList(rangeRequest.getRange(), rangeRequest.getFromDate(), rangeRequest.getToDate());
		 
		 if(meetingList==null){
			meetingListResponse.setStatus(Constants.FAILURE_RESPONSE);
			meetingListResponse.setErrorMsg("Meetings Not Found");
		}else{
			List<MeetingModel> meetingModelList=new ArrayList<>();
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null, 
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()), 
								meeting.getBusinessName().getEmployeeSM()==null?null:
									new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
			meetingListResponse.setMeetingList(meetingModelList);
			meetingListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		 
		 return new ResponseEntity<MeetingListResponse>(meetingListResponse,HttpStatus.OK);
	 }
	 /**
	  * <pre>
	  * update old meeting as rescheduled status
	  * create new meeting with new timings
	  * ->reschedule meeting 
	  * </pre>
	  * @param meeting
	  * @return
	  */
	 @Transactional
	 @PostMapping("/rescheduleTheMeeting")
	 public ResponseEntity<BaseDomain> rescheduleTheMeeting(@RequestBody Meeting meeting){
		 BaseDomain baseDomain=new BaseDomain();
		 HttpStatus httpStatus;
		 
		 Meeting meetingold=meetingService.fetchMeetingByMeetingId(meeting.getMeetingId());
		 
		 	SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
			
			long employeeId=meetingold.getEmployeeDetails().getEmployee().getEmployeeId();
			String date=sdfDate.format(meeting.getMeetingFromDateTime());
			String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
			String toTime=sdfTime.format(meeting.getMeetingToDateTime());
			
			String msg=meetingService.checkMeetingSchedule(employeeId, date, fromTime, toTime);
			if(!msg.equals("")){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(msg);
				return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
			}
			
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(meetingold.getEmployeeDetails().getEmployee().getEmployeeId());
			
			meeting.setEmployeeDetails(employeeDetails);
			meeting.setMeeting(meetingold);
			
			if(meetingold.getBusinessName()!=null){
				meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(meetingold.getBusinessName().getBusinessNameId()));
			}

			meeting.setAddedDateTime(new Date());
			meeting.setUpdatedDateTime(null);
			meeting.setMeetingStatus(Constants.MEETING_PENDING);
			meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
			meetingService.saveMeeting(meeting);
			
			meetingold.setMeetingStatus(Constants.MEETING_RESCHEDULE);
			meetingService.updateMeeting(meetingold);
			
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			
			return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
		 
	 }
	 
}
