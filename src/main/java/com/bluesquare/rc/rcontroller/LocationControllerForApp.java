package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.CityModel;
import com.bluesquare.rc.responseEntities.RegionModel;
import com.bluesquare.rc.rest.models.AreaListResponse;
import com.bluesquare.rc.rest.models.CityListResponse;
import com.bluesquare.rc.rest.models.RegionListResponse;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.RegionService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.fetchMaharashtraCitiesForApp
 * 2.fetchRegionByCityIdForApp/{cityId}
 * 3.fetchAreaByRegionIdForApp/{regionId}
 * </pre>
 */
@RestController
public class LocationControllerForApp {

	@Autowired
	CityService cityService;
	
	@Autowired
	StateService stateService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	RegionService regionService;
	
	@Autowired
	State state;	
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Transactional 	@PostMapping("/fetchMaharashtraCitiesForApp")
	public ResponseEntity<CityListResponse> fetchMaharashtraCities(@RequestHeader("Authorization") String token)
	{
		System.out.println("inside fetchMaharashtraCitiesForApp");
		CityListResponse cityListResponse=new CityListResponse();
		HttpStatus httpStatus;	
		
		
		
		
			state=stateService.fetchStateByStateName("Maharashtra");	
			List<City> cityList=cityService.fetchCityByStateIdForWebApp(state.getStateId());
			if(cityList==null)
			{
				cityListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				List<CityModel> cityModelList=new ArrayList<>();
				for(City city : cityList){
					cityModelList.add(new CityModel(city.getCityId(), city.getName()));
				}
				cityListResponse.setCityList(cityModelList);
				cityListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			return new ResponseEntity<CityListResponse>(cityListResponse, httpStatus);
		
				
	}
	
	@Transactional 	@GetMapping("/fetchRegionByCityIdForApp/{cityId}")
	public ResponseEntity<RegionListResponse> fetchRegionByCityId(@RequestHeader("Authorization") String token,@ModelAttribute("cityId") long cityId)
	{
		System.out.println("inside fetchRegionByCityIdForApp");
		RegionListResponse regionListResponse=new RegionListResponse();
		HttpStatus httpStatus;	
		
		
		
		
			
			List<Region> regionList=regionService.fetchSpecifcRegionsForWebApp(cityId);
			if(regionList==null)
			{
				regionListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				List<RegionModel> regionModelList=new ArrayList<>();
				for(Region region : regionList){
					regionModelList.add(new RegionModel(region.getRegionId(),region.getName()));
				}
				regionListResponse.setRegionList(regionModelList);
				regionListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			return new ResponseEntity<RegionListResponse>(regionListResponse, httpStatus);
		
	}
	
	@Transactional 	@GetMapping("/fetchAreaByRegionIdForApp/{regionId}")
	public ResponseEntity<AreaListResponse> fetchAreaByRegionId(@RequestHeader("Authorization") String token,@ModelAttribute("regionId") long regionId)
	{
		AreaListResponse areaListResponse=new AreaListResponse();
		HttpStatus httpStatus;	
	
		
		
			
			List<Area> areaList=areaService.fetchAreaListForWebApp(regionId);
			if(areaList==null)
			{
				areaListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				List<AreaModel> areaModelList=new ArrayList<>();
				for(Area area : areaList){
					areaModelList.add(new AreaModel(area.getAreaId(),area.getName(),area.getPincode()));
				}
				areaListResponse.setAreaList(areaModelList);
				areaListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			return new ResponseEntity<AreaListResponse>(areaListResponse, httpStatus);
		
	}
}
