package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.rest.models.CustomerReportMainResponseModel;
import com.bluesquare.rc.rest.models.CustomerReportRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.OrderDetailsListModel;
import com.bluesquare.rc.rest.models.OrderDetailsModel;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.fetchOrderDetailsForCustomerReportByEmpIdAndDateRange
 * 2.fetchOrderDetailForCustomerReportByBusinessNameId
 * </pre>
 */
@RestController
public class CustomerReportController {

@Autowired
OrderDetails orderDetails;

@Autowired
OrderDetailsDAO orderDetailsDAO;

@Autowired
OrderDetailsService orderDetailsService;

@Autowired
EmployeeDetailsService employeeDetailsService;

@Autowired
JsonWebToken jsonWebToken;	

@Autowired
HttpSession session;

@Autowired
BusinessNameService businessNameService;
/**
 * <pre>
 * customer report filter by employee Id,startDate,endDate 
 * </pre>
 * @param token
 * @param customerReportRequest
 * @return CustomerReportMainResponseModel
 */
@Transactional 	@PostMapping("/fetchOrderDetailsForCustomerReportByEmpIdAndDateRange")
public ResponseEntity<CustomerReportMainResponseModel> fetchOrderDetailByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
	
	
	CustomerReportMainResponseModel customerReportMainResponseModel=new CustomerReportMainResponseModel();
	HttpStatus httpStatus;
	System.out.println("fetchOrderDetailByEmployeeIdAndDateRange");
	
	
	
	
		List<CustomerReportResponse> orderDetailslist=orderDetailsService.fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
		if(orderDetailslist.isEmpty()){
			customerReportMainResponseModel.setStatus(Constants.FAILURE_RESPONSE);
			customerReportMainResponseModel.setErrorMsg("Records Not Found");
			httpStatus=HttpStatus.OK;
		}else{
			customerReportMainResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
			customerReportMainResponseModel.setErrorMsg(Constants.NONE);
			customerReportMainResponseModel.setCustomerReportResponse(orderDetailslist);
	
			httpStatus=HttpStatus.OK;
			
		}
	
	
	return new ResponseEntity<CustomerReportMainResponseModel>(customerReportMainResponseModel , httpStatus);
	

}

/**
 * <pre>
 * fetch order details list by businessNameId and employeeId
 * </pre>
 * @param token
 * @param businessNameId
 * @param employeeId
 * @return OrderDetailsList
 */
@Transactional 	@GetMapping("/fetchOrderDetailForCustomerReportByBusinessNameId/{businessNameId}/{employeeId}")
 public ResponseEntity<OrderDetailsListModel> fetchOrderDetailForCustomerReportByBusinessNameId(@RequestHeader("Authorization") String token,@ModelAttribute("businessNameId") String businessNameId,@ModelAttribute("employeeId") long employeeId){
	
	OrderDetailsListModel orderDetailsListModelResponse =new OrderDetailsListModel();
	HttpStatus httpStatus;
	
	System.out.println("fetchOrderDetailForCustomerReportByBusinessNameId");
	
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
		BusinessName businessName=businessNameService.fetchBusinessForWebApp(businessNameId);
		List<OrderDetails> orderDetailList=orderDetailsService.fetchOrderDetailForCustomerReportByBusinessNameId(businessNameId);
				
				if(orderDetailList==null){
					orderDetailsListModelResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderDetailsListModelResponse.setErrorMsg(Constants.NO_CONTENT);
					httpStatus=HttpStatus.NO_CONTENT;
					}
				else{
					List<OrderDetailsModel> orderDetailsList=new ArrayList<>();
					for(OrderDetails orderDetails : orderDetailList){
						orderDetailsList.add(new OrderDetailsModel(
								orderDetails.getOrderId(), 
								orderDetails.getOrderStatus().getStatus(), 
								orderDetails.getTotalAmountWithTax(), 
								orderDetails.getConfirmTotalAmountWithTax(), 
								orderDetails.getOrderDetailsAddedDatetime()));
					}
					orderDetailsListModelResponse.setStatus(Constants.SUCCESS_RESPONSE);
					orderDetailsListModelResponse.setOrderDetailsList(orderDetailsList);
					orderDetailsListModelResponse.setSalesManName(employeeDetails.getName());
					orderDetailsListModelResponse.setAreaName(businessName.getArea().getName());
					orderDetailsListModelResponse.setShopName(businessName.getShopName());
					orderDetailsListModelResponse.setMobileNumber(businessName.getContact().getMobileNumber());
					httpStatus=HttpStatus.OK;
					
				}
	
	
	
	return new ResponseEntity<OrderDetailsListModel>(orderDetailsListModelResponse,httpStatus);
}

}	

