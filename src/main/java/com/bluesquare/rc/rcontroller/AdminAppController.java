package com.bluesquare.rc.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.models.CompanyListReponse;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.CompanyModels;
import com.bluesquare.rc.rest.models.LocationSectionResponse;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.utils.Constants;


/**
 * <pre>
 * @author Sachin Sharma 31-07-2018 Code Documentation
 * API End Points
 * 1.fetchCountryList
 * 2.saveAndUpdateCountry
 * 3.fetchStateListForApp
 * 
 * </pre>
 */
@RestController
public class AdminAppController {
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	StateService stateService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CityService cityService;
	
	@Transactional
	@GetMapping("/fetchCountryListForApp")
	public ResponseEntity<LocationSectionResponse> fetchCountryList(){
		LocationSectionResponse locationResponse=new LocationSectionResponse();
		HttpStatus httpStatus;
		System.out.println("fetchCountryList");
		
		List<Country> countryList=countryService.fetchCountryListForWebApp();
		if(countryList==null){
			locationResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			locationResponse.setErrorMsg("No Data found");
			
		}else{
			locationResponse.setCountryList(countryList);
			locationResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<LocationSectionResponse>(locationResponse,httpStatus);
		
	}
	
	@Transactional
	@PostMapping("saveAndUpdateCountry")
	public ResponseEntity<BaseDomain>  saveCountry(@RequestBody Country country) {
		System.out.println("in Save Country");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus = null;
		
		
		
		boolean flag = false;
		List<Country> coutryList = countryService.fetchCountryListForWebApp();

		country.setName(
				(Character.toString(country.getName().charAt(0)).toUpperCase() + (country.getName().substring(1)).toLowerCase()));

		if (country.getCountryId() == 0) {

			if (coutryList != null) {
				for (Country coutryFromDb : coutryList) {
					if (coutryFromDb.getName().trim().toUpperCase().equals(country.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}

			if (!flag) {
				
				/* country.setDisable(false); */
				countryService.saveCountryForWeb(country);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				
			}else{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg("Country Name already exist");
				httpStatus=HttpStatus.OK;
			}
		}
		if (country.getCountryId() != 0) {

			if (coutryList != null) {
				for (Country coutryFromDb : coutryList) {
					if (coutryFromDb.getName().trim().toUpperCase().equals(country.getName().trim().toUpperCase())
							&& coutryFromDb.getCountryId() != country.getCountryId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				countryService.updateCountryForWeb(country);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}else{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg("Something went wrong");
				httpStatus=HttpStatus.OK;
			}
		}

		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}


	@Transactional
	@GetMapping("/fetchStateListForApp")
	public ResponseEntity<LocationSectionResponse> fetchStateList(){
		LocationSectionResponse locationResponse=new LocationSectionResponse();
		HttpStatus httpStatus;
		System.out.println("fetchStateList");
		
		List<State> stateList=stateService.fetchAllStateForWebApp();
		if(stateList==null){
			locationResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			locationResponse.setErrorMsg("No Data found");
			
		}else{
			locationResponse.setStateList(stateList);
			locationResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<LocationSectionResponse>(locationResponse,httpStatus);
		
	}
	
	@Transactional
	@PostMapping("/saveAndUpdateState")
	public ResponseEntity<BaseDomain> saveAndUpdateState(@RequestBody State state){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus=null;

		/*System.out.println("in Save State");
		this.country.setCountryId(Long.parseLong(request.getParameter("countryId")));
		this.state.setCountry(this.country);
		this.state.setStateId(Long.parseLong(request.getParameter("stateId")));
		this.state.setCode(request.getParameter("code"));
		this.state.setName(request.getParameter("name"));*/

		boolean flag = false;
		List<State> stateList = stateService.fetchStateByCountryIdforwebapp(state.getCountry().getCountryId());
		state.setName((Character.toString(state.getName().charAt(0)).toUpperCase() + (state.getName().substring(1)).toLowerCase()));

		if (state.getStateId() == 0) {
			if (stateList != null) {
				for (State stateFromDb : stateList) {
					if (stateFromDb.getName().trim().toUpperCase().equals(state.getName().trim().toUpperCase())
							|| stateFromDb.getCode().trim().equals(state.getCode())) {
						flag = true;
						break;
					}
				}
			}

			if (!flag) {
				/* country.setDisable(false); */
				stateService.saveForWebApp(state);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}else{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg("State Name or Code already exist");
				httpStatus=HttpStatus.OK;
			}
		}

		if (state.getStateId() != 0) {

			flag = false;
			if (stateList != null) {
				for (State stateFromDb : stateList) {
					if ((stateFromDb.getName().trim().toUpperCase().equals(state.getName().trim().toUpperCase())
							|| stateFromDb.getCode().trim().equals(state.getCode()))
							&& stateFromDb.getStateId() != state.getStateId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				stateService.updateForWebApp(state);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}else{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg("Something went wrong");
				httpStatus=HttpStatus.OK;
			}
		}

		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	
	}
	
	@Transactional
	@GetMapping("/fetchCompanyListByStateId/{stateId}")
	public ResponseEntity<CompanyListReponse> fetchCompanyListByStateId(@ModelAttribute ("stateId") long stateId){
		CompanyListReponse companyListReponse=new CompanyListReponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchCompanyListByStateId");
		
		List<Company> companies=companyService.fetchCompanyListByStateId(stateId);
		if(companies==null){
			companyListReponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			companyListReponse.setErrorMsg("No Data found");
		}else{
			companyListReponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			companyListReponse.setCompanyList(companies);
		}
		
		return new ResponseEntity<CompanyListReponse>(companyListReponse,httpStatus);
	}
	
	@Transactional
	@GetMapping("/fetchStateListByCountryId/{countryId}")
	public ResponseEntity<LocationSectionResponse> fetchStateListByCountryId(@ModelAttribute("countryId") long countryId){
		LocationSectionResponse locationSectionResponse=new LocationSectionResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchStateListByCountryId");
		
		List<State> states=stateService.fetchStateByCountryIdforwebapp(countryId);
		if(states==null){
			locationSectionResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			locationSectionResponse.setErrorMsg("No Data found");
		}else{
			locationSectionResponse.setStateList(states);
			locationSectionResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<LocationSectionResponse>(locationSectionResponse,httpStatus);
		
	}
	
	@Transactional
	@GetMapping("/fetchCityListByStateId/{stateId}")
	public ResponseEntity<LocationSectionResponse> fetchCityListByStateId(@ModelAttribute("stateId") long stateId){
		LocationSectionResponse locationSectionResponse=new LocationSectionResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchStateListByCountryId");
		
		List<City> cityList=cityService.fetchCityByStateIdForWebApp(stateId);
		if(cityList==null){
			locationSectionResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			locationSectionResponse.setErrorMsg("No Data found");
		}else{
			locationSectionResponse.setCityList(cityList);
			locationSectionResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<LocationSectionResponse>(locationSectionResponse,httpStatus);
		
	}
	
	@Transactional
	@PostMapping("/saveCompanyApp")
	public ResponseEntity<BaseDomain> saveCompanyApp(@RequestBody CompanyModels companyModels){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("save Company App");
		String cityIdList="";
		try {
			for(City c:companyModels.getCityList()){
				 cityIdList+=c.getCityId()+",";
			}
			cityIdList=cityIdList.substring(0, cityIdList.length()-1);
			companyService.saveCompany(companyModels.getCompany(),cityIdList);
			httpStatus=HttpStatus.OK;
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus=HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);;
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	
	@GetMapping("/fetchCompanyByCompanyId/{companyId}")
	public ResponseEntity<CompanyListReponse> fetchCompanyByCompanyId(@ModelAttribute("companyId") long companyId){
		
		CompanyListReponse companyListReponse=new CompanyListReponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchCompanyByCompanyId");
		
		Company company=companyService.fetchCompanyByCompanyId(companyId);
		if(company==null){
			companyListReponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			companyListReponse.setErrorMsg("No Data Found");
		}else{
			companyListReponse.setCompany(company);
			companyListReponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<CompanyListReponse>(companyListReponse,httpStatus);
		
	}
}
