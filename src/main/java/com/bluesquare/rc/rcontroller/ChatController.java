package com.bluesquare.rc.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.EmployeeChatStatus;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.ChatListResponse;
import com.bluesquare.rc.rest.models.ChatSaveResponse;
import com.bluesquare.rc.rest.models.EmployeeChatStatusResponse;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.EmployeeNameAndIdResponse;
import com.bluesquare.rc.service.ChatService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 23-05-2018 Code Documentation
 * API End Points
 * 1.fetchEmployeeListForChat
 * 2.fetchChatRecords/{employeeId}/{firstChatId}/{count}/{lastChatId}
 * 3.saveChat
 * 4.setStatus/{employeeId}/{status}
 * 5.getStatus/{employeeId}
 * 6.deleteChatRecord/{chatId}
 * 7.deleteChatRecordList
 * </pre>
 */
@RestController
public class ChatController {

	@Autowired
	ChatService chatService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	/**
	 * <pre>
	 * employee details list for chat in app which comes under logged employee area  
	 * </pre>
	 * @return EmployeeNameAndIdResponse
	 */
	@Transactional 	@PostMapping("/fetchEmployeeListForChat")
	public ResponseEntity<EmployeeNameAndIdResponse> fetchEmployeeListForChat()
	{		
		System.out.println("inside fetchBusinessTypeListForApp");
		HttpStatus httpStatus;	
		EmployeeNameAndIdResponse employeeNameAndIdResponse=new EmployeeNameAndIdResponse();
		
		List<EmployeeNameAndId> employeeNameAndIdList=employeeDetailsService.fetchEmployeeListForChat();
		
		if(employeeNameAndIdList==null){
			employeeNameAndIdResponse.setStatus(Constants.FAILURE_RESPONSE);
			employeeNameAndIdResponse.setErrorMsg("Data Not Found");
			httpStatus=HttpStatus.OK;
		}else{
			employeeNameAndIdResponse.setEmployeeNameAndIdList(employeeNameAndIdList);
			employeeNameAndIdResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<EmployeeNameAndIdResponse>(employeeNameAndIdResponse, httpStatus);
	}
	/**+
	 * <pre>
	 * fetch chat records by employeeId,firstChatId,count,lastChatId
	 * </pre>
	 * @param employeeId
	 * @param firstChatId
	 * @param count
	 * @param lastChatId
	 * @return
	 */
	@Transactional 	@GetMapping("/fetchChatRecords/{employeeId}/{firstChatId}/{count}/{lastChatId}")
	public ResponseEntity<ChatListResponse> fetchChatRecords(@ModelAttribute("employeeId") long employeeId,
															 @ModelAttribute("firstChatId") long firstChatId,
															 @ModelAttribute("count") int count,
															 @ModelAttribute("lastChatId") long lastChatId)
	{		
		ChatListResponse chatListResponse=new ChatListResponse();
		System.out.println("inside fetchChatRecords");
		HttpStatus httpStatus;	
				
		List<Chat> chatList=chatService.fetchChatRecords(employeeId, firstChatId, count,lastChatId);
		if(chatList==null){
			chatListResponse.setStatus(Constants.FAILURE_RESPONSE);
			chatListResponse.setErrorMsg("Data Not Found");
			httpStatus=HttpStatus.OK;
		}else{
			chatListResponse.setChatList(chatList);
			chatListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<ChatListResponse>(chatListResponse, httpStatus);
	}
	/**
	 * <pre>
	 * save chat and return previous last chat record 
	 * </pre>
	 * @param chat
	 * @return previous last chat record
	 */
	@Transactional 	@PostMapping("/saveChat")
	public ResponseEntity<ChatSaveResponse> saveChat(@RequestBody Chat chat){
		ChatSaveResponse chatSaveResponse=new ChatSaveResponse();
		System.out.println("inside saveChat");
		HttpStatus httpStatus;	

		chat=chatService.saveChat(chat);
		chatSaveResponse.setChatId(chat.getChatId());
		chatSaveResponse.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<ChatSaveResponse>(chatSaveResponse, httpStatus);
	}
	/**
	 * <pre>
	 * set status online,offline,typing by employeeId
	 * </pre>
	 * @param employeeId
	 * @param status
	 * @return
	 */
	@Transactional 	@GetMapping("/setStatus/{employeeId}/{status}")
	public ResponseEntity<BaseDomain> setStatus(@ModelAttribute("employeeId") long employeeId,@ModelAttribute("status") String status){
		BaseDomain baseDomain=new BaseDomain();
		System.out.println("inside setStatus");
		HttpStatus httpStatus;	
		
		chatService.setStatusByEmployeeId(employeeId, status);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	/**
	 * <pre>
	 * fetch employee current chat status (offline,online,typing)
	 * </pre>
	 * @param employeeId
	 * @return
	 */
	@Transactional 	@GetMapping("/getStatus/{employeeId}")
	public ResponseEntity<EmployeeChatStatusResponse> getStatus(@ModelAttribute("employeeId") long employeeId){
		EmployeeChatStatusResponse employeeChatStatusResponse=new EmployeeChatStatusResponse();
		System.out.println("inside deleteChatRecord");
		HttpStatus httpStatus;	
		
		EmployeeChatStatus employeeChatStatus=chatService.getStatusByEmployeeId(employeeId);
		if(employeeChatStatus==null){
			employeeChatStatusResponse.setChatStatus(Constants.CHAT_OFFLINE);
			employeeChatStatusResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}else{
			if(employeeChatStatus.isTypingStatus()){
				employeeChatStatusResponse.setChatStatus("Typing..");
			}else{
				employeeChatStatusResponse.setChatStatus(employeeChatStatus.getStatus());
			}
			employeeChatStatusResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<EmployeeChatStatusResponse>(employeeChatStatusResponse, httpStatus);
	}
	/**
	 * <pre>
	 * delete chat record by chat Id
	 * </pre>
	 * @param chatId
	 * @return
	 */
	@Transactional 	@GetMapping("/deleteChatRecord/{chatId}")
	public ResponseEntity<BaseDomain> deleteChatRecord(@ModelAttribute("chatId") long chatId){
		BaseDomain baseDomain=new BaseDomain();
		System.out.println("inside deleteChatRecord");
		HttpStatus httpStatus;	
		
		chatService.deleteChat(chatId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	/**
	 * <pre>
	 * delete chat records by chat Id array 
	 * </pre>
	 * @param chatId
	 * @return
	 */
	@Transactional 	@GetMapping("/deleteChatRecordList")
	public ResponseEntity<BaseDomain> deleteChatRecordList(@RequestParam(value = "chatArray") String[] chatArray){
		BaseDomain baseDomain=new BaseDomain();
		System.out.println("inside deleteChatRecord");
		HttpStatus httpStatus;	
		for(String chatId : chatArray){
			chatService.deleteChat(Long.parseLong(chatId));
		}
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
}
