package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.rest.models.FetchProductBySupplierResponse;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.rest.models.SupplierResponse;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.fetchSupplierListForApp
 * 2.fetchProductBySupplierId/{supplierId}
 * </pre>
 */
@RestController
public class SupplierController {
	
	@Autowired
	SupplierService supplierService;

	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	//not used
	/*@Transactional 	@PostMapping("/fetchSupplierListForApp")
	public ResponseEntity<SupplierResponse> fetchSupplier(@RequestHeader("Authorization") String token){
		System.out.println("in fetchSupplier");
		
		HttpStatus httpStatus;
		SupplierResponse supplierResponse=new SupplierResponse();
		
		
		
		List<Supplier> list=supplierService.fetchSupplierForWebAppList();
		
		if(list==null){
			supplierResponse.setStatus(Constants.FAILURE_RESPONSE);
			supplierResponse.setErrorMsg(Constants.NONE);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			supplierResponse.setStatus(Constants.SUCCESS_RESPONSE);
			supplierResponse.setErrorMsg(Constants.NONE);
			supplierResponse.setSupplier(list);
			httpStatus=HttpStatus.OK;
		}	
		
		return new ResponseEntity<SupplierResponse>(supplierResponse, httpStatus);
	}*/
	
	/**
	 * <pre>
	 * not used
	 * fetch supplier product list by supplierId with conversion ProductAddInventory model
	 * </pre>
	 * @param token
	 * @param supplierId
	 * @return
	 */
/*	@Transactional 	@GetMapping("/fetchProductBySupplierId/{supplierId}")
	public ResponseEntity<FetchProductBySupplierResponse> fetchProductBySupplier(@RequestHeader("Authorization") String token,@ModelAttribute("supplierId") String supplierId){
		System.out.println("in fetchProductBySupplierId");
		
		HttpStatus httpStatus;
		FetchProductBySupplierResponse fetchProductBySupplierResponse=new FetchProductBySupplierResponse();
		
		
		
		List<ProductAddInventory> supplierProductList=supplierService.fetchProductBySupplierId(supplierId);
		
		if(supplierProductList==null){
			fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
			fetchProductBySupplierResponse.setErrorMsg(Constants.NONE);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			fetchProductBySupplierResponse.setStatus(Constants.SUCCESS_RESPONSE);
			fetchProductBySupplierResponse.setErrorMsg(Constants.NONE);
			fetchProductBySupplierResponse.setProductAddInventories(supplierProductList);			
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<FetchProductBySupplierResponse>(fetchProductBySupplierResponse, httpStatus);
	}*/
	
	
}
