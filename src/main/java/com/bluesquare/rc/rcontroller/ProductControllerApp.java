package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.DailyStockDetailsModel;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.DailyStockReportResponse;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.rest.models.ProductBrandListResponse;
import com.bluesquare.rc.rest.models.ProductCategoryListResponse;
import com.bluesquare.rc.rest.models.ProductListResponse;
import com.bluesquare.rc.rest.models.ProductReponse;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.fetchProductBrandList
 * 2.fetchProductCategoryList
 * 3.fetchProductList -- not used
 * 4.fetchProductByBrandAndCategoryId
 * 5.fetchProductforAddInventory -- not used
 * 6.checkProductQtyAvailable/{productId}/{qty}  -- not used
 * 7.fetchDailyStockRecord
 * </pre>
 */
@RestController
public class ProductControllerApp {

	@Autowired
	 ProductService  productService;
	
	@Autowired
	Product product;
	
	@Autowired
	CategoriesService categoryService;
	
	@Autowired 
	BrandService brandService;

	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	/**
	 * <pre>
	 * fetch brand list
	 * </pre>
	 * @param token
	 * @return ProductBrandListResponse
	 */
	@Transactional 	@PostMapping("/fetchProductBrandList")
	public ResponseEntity<ProductBrandListResponse> productBrandList(@RequestHeader("Authorization") String token){
		System.out.println("in fetchProductBrandList");
		
		HttpStatus httpStatus;
		ProductBrandListResponse productBrandListResponse=new ProductBrandListResponse();
		
		
		
		List<BrandModel> list=brandService.fetchBrandModelListForWebApp();
		
		if(list==null){
			productBrandListResponse.setStatus(Constants.FAILURE_RESPONSE);
			productBrandListResponse.setErrorMsg(Constants.NO_CONTENT);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			productBrandListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			productBrandListResponse.setBrandList(list);
			productBrandListResponse.setErrorMsg(Constants.NONE);
			httpStatus=HttpStatus.OK;			
		}
			
		return new ResponseEntity<ProductBrandListResponse>(productBrandListResponse, httpStatus);
	}
	/**
	 * <pre>
	 * fetch category list
	 * </pre>
	 * @param token
	 * @return ProductCategoryListResponse
	 */
	@Transactional 	@PostMapping("/fetchProductCategoryList")
	public ResponseEntity<ProductCategoryListResponse> productCategoryList(@RequestHeader("Authorization") String token){
		System.out.println("in fetchproductCategoryList");
		
		HttpStatus httpStatus;
		ProductCategoryListResponse productCategoryListResponse=new ProductCategoryListResponse();
		
		
		
		List<CategoriesModel> list1=categoryService.fetchCategoriesModelList();
		
		if(list1==null){
			productCategoryListResponse.setStatus(Constants.FAILURE_RESPONSE);
			productCategoryListResponse.setErrorMsg(Constants.NO_CONTENT);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			productCategoryListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			productCategoryListResponse.setCategoryListInnerResponse(list1);
			productCategoryListResponse.setErrorMsg(Constants.NONE);
			httpStatus=HttpStatus.OK;	
		}
		
		return new ResponseEntity<ProductCategoryListResponse>(productCategoryListResponse, httpStatus);
		
	}
	/**
	 * <pre>
	 * not used
	 * fetch product list with Model conversion
	 * </pre>
	 * @param token
	 * @return ProductListResponse
	 */
	/*@Transactional 	@PostMapping("/fetchProductList")
	public ResponseEntity<ProductListResponse> fetchProductList(@RequestHeader("Authorization") String token){
		System.out.println("in fetchProductList");
		
		HttpStatus httpStatus;
		ProductListResponse productListResponse=new  ProductListResponse();
		
		
		
		List<ProductAddInventory> list=productService.fetchProductList();
		if(list.isEmpty()){
			productListResponse.setStatus(Constants.FAILURE_RESPONSE);
			productListResponse.setErrorMsg(Constants.NO_CONTENT);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			productListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			productListResponse.setErrorMsg(Constants.NONE);
			productListResponse.setListInnerResponse(list);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<ProductListResponse>(productListResponse, httpStatus);
	}*/
	
	@Transactional 	@PostMapping("/fetchProductByBrandAndCategoryId")
	public ResponseEntity<ProductListResponse> fetchBrandAndCategory(@RequestHeader("Authorization") String token,@RequestBody BrandAndCategoryRequest brandAndCategoryRequest ){
		System.out.println("in fetchBrandAndCategory");
		
		HttpStatus httpStatus;
		ProductListResponse productListResponse=new  ProductListResponse();
		
		
		
		List<ProductAddInventory> list=productService.fetchProductByBrandAndCategory(brandAndCategoryRequest);
		if(list==null){
			productListResponse.setStatus(Constants.FAILURE_RESPONSE);
			productListResponse.setErrorMsg(Constants.NO_CONTENT);
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			productListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			productListResponse.setErrorMsg(Constants.NONE);
			productListResponse.setListInnerResponse(list);
			httpStatus=HttpStatus.OK;
		}
		System.out.println(productListResponse);
		return new ResponseEntity<ProductListResponse>(productListResponse, httpStatus);
	}
	
	
	/*@Transactional 	@PostMapping("/fetchProductforAddInventory")
	public ResponseEntity<ProductAddInventoryResponse> fetchProduct(@RequestHeader("Authorization") String token){
		System.out.println("in fetchProductforAddInventory");
		
		HttpStatus httpStatus;
		ProductAddInventoryResponse addInventoryResponse=new ProductAddInventoryResponse();
		
		
		List<ProductAddInventory> list=productService.fetchProductForAddInventory();
		
		if(list==null){
			addInventoryResponse.setStatus(Constants.FAILURE_RESPONSE);
			addInventoryResponse.setErrorMsg(Constants.NONE);
			httpStatus=HttpStatus.NO_CONTENT;	
		}else{
			addInventoryResponse.setStatus(Constants.SUCCESS_RESPONSE);
			addInventoryResponse.setErrorMsg(Constants.NONE);
			addInventoryResponse.setProductAddInventory(list);
			httpStatus=HttpStatus.OK;
		}	
		return new ResponseEntity<ProductAddInventoryResponse>(addInventoryResponse, httpStatus);
	}*/

	/**
	 * <pre>
	 * not used
	 * check requested quantity with current quantity from product
	 * </pre>
	 * @param token
	 * @param productId
	 * @param qty
	 * @return
	 */
	@Transactional 	@GetMapping("/checkProductQtyAvailable/{productId}/{qty}")
	public ResponseEntity<ProductReponse> checkProductQtyAvailable(@RequestHeader("Authorization") String token
													,@ModelAttribute("productId") long productId
													,@ModelAttribute("qty") long qty){
		HttpStatus httpStatus=HttpStatus.OK;
		ProductReponse productReponse=new ProductReponse();
		
		
		Product product=productService.fetchProductForWebApp(productId);
		if(product.getCurrentQuantity()<qty){
			productReponse=new ProductReponse(product);
			productReponse.setStatus(Constants.FAILURE_RESPONSE);
			productReponse.setErrorMsg("Entered Qty exceeds Current qty");
			httpStatus=HttpStatus.OK;
		}else{
			productReponse=new ProductReponse(product);
			productReponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<ProductReponse>(productReponse, httpStatus);
	}
	/**
	 * <pre>
	 * fetch daily stock record by requested date
	 * </pre>
	 * @param token
	 * @param request
	 * @return DailyStockReportResponse
	 */
	@Transactional 	@GetMapping("/fetchDailyStockRecord")
	public ResponseEntity<DailyStockReportResponse> fetchDailyStockRecord(@RequestHeader("Authorization") String token,HttpServletRequest request){
		
		String pickDate=request.getParameter("pickDate");
		DailyStockReportResponse dailyStockReportResponse=new DailyStockReportResponse();
		HttpStatus httpStatus=HttpStatus.OK;
						
		List<DailyStockDetailsModel> dailyStockDetails=productService.fetchDailyStockDetails(
				pickDate, 
				Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()),
				tokenHandlerDAO.getSessionSelectedBranchIds());
		
		if(dailyStockDetails==null){
			dailyStockReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			dailyStockReportResponse.setErrorMsg("Data Not Found");
			httpStatus=HttpStatus.NO_CONTENT;
		}else{
			dailyStockReportResponse.setDailyStockDetails(dailyStockDetails);
			dailyStockReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}	
		
		return new ResponseEntity<DailyStockReportResponse>(dailyStockReportResponse, httpStatus);
	}
}
