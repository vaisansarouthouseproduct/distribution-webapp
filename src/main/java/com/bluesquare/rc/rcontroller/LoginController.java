package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.models.CompanyListReponse;
import com.bluesquare.rc.rest.models.AppVersionCheckResponse;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.LoginRequest;
import com.bluesquare.rc.rest.models.LoginResponse;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.authentication
 * 2.logout/{employeeId}
 * 3.checkAppVersion
 * 4.fetchAllCompanyList
 * </pre>
 */
@RestController
public class LoginController {
	
/*	@Autowired
	Employee employee;*/
	
	@Autowired
	EmployeeService  employeeService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	CompanyService companyService;
	
	/*@Autowired
	EmployeeDetails employeeDetails;*/ 
	
	@Autowired
	JsonWebToken jsonWebToken; 
	
	/***
	 * <pre>
	 * authenticate by userId and password
	 * if userId and password correct then update given token to employee details 
	 * generate JWT token from employeeId, location lists, companyId
	 * </pre>
	 * @param loginRequest
	 * @return
	 */
	@Transactional 	@PostMapping("/authentication")
	public ResponseEntity<LoginResponse> authenticateLogin(@RequestBody LoginRequest loginRequest){
		
		System.out.println("inside authentication");
		
		
		HttpStatus httpStatus;
		LoginResponse loginResponse=new LoginResponse();
		Employee employee=new Employee();
		EmployeeDetails employeeDetails=new EmployeeDetails();
		
			employee=employeeService.loginCredentialsForRC(loginRequest.getUserId(),loginRequest.getPassword());
			
			if(employee==null){
				loginResponse.setStatus(Constants.FAILURE_RESPONSE);
				loginResponse.setErrorMsg(Constants.LOGIN_FAILED);			
				httpStatus=HttpStatus.OK;			
			}
			else if(employee.getDepartment().getName().equalsIgnoreCase("Admin")){
				httpStatus=HttpStatus.OK;     
				loginResponse.setEmployeeName(employeeDetails.getName());
				loginResponse.setEmployee(employee);
				loginResponse.setStatus(Constants.SUCCESS_RESPONSE);
				loginResponse.setErrorMsg(Constants.NONE);
				
				/*List<Area> areaList = employeeDetailsService.fetchAreaByEmployeeId(employee.getEmployeeId());
				
				List<Branch> branchSet=employeeService.fetchBranchListByEmployeeId(employee.getEmployeeId());	
				loginResponse.setBranchList(branchSet);
				Long[] branchIds=new Long[branchSet.size()];
				int i=0;
				for(Branch branch : branchSet){
					branchIds[i]=branch.getBranchId();
					i++;
				}			
				*/
				//create token 
				//String token=jsonWebToken.create(claimMap,companyIdList,branchIds, Constants.ISSUER,employee.getEmployeeId());
				
			}
			
			else{
				
				employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
				if(employeeDetails.isStatus())
				{
					loginResponse.setStatus(Constants.FAILURE_RESPONSE);
					loginResponse.setErrorMsg("This Employee Is Disabled");
					httpStatus=HttpStatus.OK;       
				}
				else
				{
					// clear fcm token from other employee details
					employeeDetailsService.clearToken(loginRequest.getToken(),employee.getEmployeeId());
					
					//update fcm token to employee details
					employeeDetails.setToken(loginRequest.getToken());				
					employeeDetailsService.updateForWebApp(employeeDetails,false);
					
					httpStatus=HttpStatus.OK;     
					loginResponse.setEmployeeName(employeeDetails.getName());
					loginResponse.setEmployee(employee);
					loginResponse.setStatus(Constants.SUCCESS_RESPONSE);
					loginResponse.setErrorMsg(Constants.NONE);
					loginResponse.setUserCode(employeeDetails.getUserCode());
					
					//Fetch Areas for given Employee
					List<Area> areaList = employeeDetailsService.fetchAreaByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
					Set<Long> countryIdSet=new HashSet<>();
					Set<Long> stateIdSet=new HashSet<>();
					Set<Long> cityIdSet=new HashSet<>();
					Set<Long> regionIdSet=new HashSet<>();
					Set<Long> areaIdSet=new HashSet<>();
					
					for(Area area: areaList)
					{
						countryIdSet.add(area.getRegion().getCity().getState().getCountry().getCountryId());
						stateIdSet.add(area.getRegion().getCity().getState().getStateId());
						cityIdSet.add(area.getRegion().getCity().getCityId());
						regionIdSet.add(area.getRegion().getRegionId());
						areaIdSet.add(area.getAreaId());
					}
					
					List<Long> areaIdList=new ArrayList<>();
					List<Long> regionIdList=new ArrayList<>();
					List<Long> cityIdList=new ArrayList<>();
					List<Long> stateIdList=new ArrayList<>();
					List<Long> countryIdList=new ArrayList<>();
					
					areaIdList.addAll(areaIdSet);
					regionIdList.addAll(regionIdSet);
					cityIdList.addAll(cityIdSet);
					stateIdList.addAll(stateIdSet);
					countryIdList.addAll(countryIdSet);
					
					Map<String,List<Long>> claimMap=new HashMap<>();
					claimMap.put("areaIdList", areaIdList);
					claimMap.put("regionIdList", regionIdList);
					claimMap.put("cityIdList", cityIdList);
					claimMap.put("stateIdList", stateIdList);
					claimMap.put("countryIdList", countryIdList);
					
					Long companyId=employee.getCompany().getCompanyId();
					Long[] companyIdList={companyId};
					
					List<Branch> branchSet=employeeService.fetchBranchListByEmployeeId(employee.getEmployeeId());	
					loginResponse.setBranchList(branchSet);
					Long[] branchIds=new Long[branchSet.size()];
					int i=0;
					for(Branch branch : branchSet){
						branchIds[i]=branch.getBranchId();
						i++;
					}			
					
					//create token 
					String token=jsonWebToken.create(claimMap,companyIdList,branchIds, Constants.ISSUER,employee.getEmployeeId());
					
					loginResponse.setToken(token);
				}
			}
		
			
		return new ResponseEntity<LoginResponse>(loginResponse, httpStatus);
	}
	/**
	 * <pre>
	 * logout by employeeId
	 * clear token from employee details
	 * </pre>
	 * @param employeeId
	 * @return
	 */
	@Transactional 	@GetMapping("/logout/{employeeId}")
	public ResponseEntity<BaseDomain> logoutToken(@ModelAttribute("employeeId") long employeeId) {
		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside Logout");

			employeeService.logout(employeeId);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
				
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus); 
	}
	/**
	 * <pre>
	 * check app version with requested appVersion 
	 * </pre>
	 * @param request
	 * @return
	 */
	@Transactional 	@GetMapping("/checkAppVersion")
    public ResponseEntity<AppVersionCheckResponse> checkAppVersion(HttpServletRequest request){
            String appVersion=request.getParameter("appVersion");
            AppVersionCheckResponse appVersionCheckResponse=new AppVersionCheckResponse();
            HttpStatus httpStatus;
            System.out.println("Inside checkAppVersion(Login Controller)");
            try {
                    String appVersionUpdate=employeeService.checkAppVersion(appVersion);
                    
                            appVersionCheckResponse.setAppVersionUpdateAvailable(appVersionUpdate);
                            appVersionCheckResponse.setStatus(Constants.SUCCESS_RESPONSE);
                            httpStatus=HttpStatus.OK;
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("Error Inside checkAppVersion is "+e.toString());
                    appVersionCheckResponse.setStatus(Constants.FAILURE_RESPONSE);
                    httpStatus=HttpStatus.NO_CONTENT;
            }
            
            return new ResponseEntity<AppVersionCheckResponse>(appVersionCheckResponse,httpStatus);
            
            
            
    }
	
	@Transactional
	@GetMapping("fetchAllCompanyList")
	public ResponseEntity<CompanyListReponse> fetchAllCompanyList(){
		
		CompanyListReponse companyListReponse=new CompanyListReponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchAllCompanyList:");
		
		List<Company> companies=companyService.fetchAllCompany();
		if (companies==null) {
			companyListReponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			companyListReponse.setErrorMsg("List not found");
			
		}else{
			companyListReponse.setCompanyList(companies);
			companyListReponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<CompanyListReponse>(companyListReponse,httpStatus);
		
	}
	
	@Transactional
	@PostMapping("adminLogin")
	public ResponseEntity<LoginResponse> adminLogin(@RequestBody LoginRequest loginRequest){
		
		LoginResponse loginResponse=new LoginResponse();
		HttpStatus httpStatus;
		long companyId=loginRequest.getCompanyId();
		List<Area> areaList=employeeDetailsService.fetchAreaListByCompanyId(companyId);
		
		Set<Long> countryIdSet=new HashSet<>();
		Set<Long> stateIdSet=new HashSet<>();
		Set<Long> cityIdSet=new HashSet<>();
		Set<Long> regionIdSet=new HashSet<>();
		Set<Long> areaIdSet=new HashSet<>();
		
		for(Area area: areaList)
		{
			countryIdSet.add(area.getRegion().getCity().getState().getCountry().getCountryId());
			stateIdSet.add(area.getRegion().getCity().getState().getStateId());
			cityIdSet.add(area.getRegion().getCity().getCityId());
			regionIdSet.add(area.getRegion().getRegionId());
			areaIdSet.add(area.getAreaId());
		}
		
		List<Long> areaIdList=new ArrayList<>();
		List<Long> regionIdList=new ArrayList<>();
		List<Long> cityIdList=new ArrayList<>();
		List<Long> stateIdList=new ArrayList<>();
		List<Long> countryIdList=new ArrayList<>();
		
		areaIdList.addAll(areaIdSet);
		regionIdList.addAll(regionIdSet);
		cityIdList.addAll(cityIdSet);
		stateIdList.addAll(stateIdSet);
		countryIdList.addAll(countryIdSet);
		
		Map<String,List<Long>> claimMap=new HashMap<>();
		claimMap.put("areaIdList", areaIdList);
		claimMap.put("regionIdList", regionIdList);
		claimMap.put("cityIdList", cityIdList);
		claimMap.put("stateIdList", stateIdList);
		claimMap.put("countryIdList", countryIdList);
		
		//Long companyId;
		Long[] companyIdList={companyId};
			
		List<BranchModel> branchModelList=branchService.fetchBranchListByCompanyId(companyId);
		Employee employee=employeeDetailsService.getAdminEmployee();
		if(employee==null){
			loginResponse.setStatus(Constants.FAILURE_RESPONSE);
			loginResponse.setErrorMsg(Constants.LOGIN_FAILED);			
			httpStatus=HttpStatus.OK;			
		}
		
		
		Long[] branchIds=new Long[branchModelList.size()];
		int i=0;
		
		List<Branch> branchList=new ArrayList<>();
		
		for(BranchModel branch : branchModelList){
			
			Branch branch2=new Branch();
			branch2.setBranchId(branch.getBranchId());
			branch2.setName(branch.getName());
			
			Company company=new Company();
			company=branch.getCompany();
			
			branch2.setCompany(company);
			branchList.add(branch2);
			
			branchIds[i]=branch.getBranchId();
			i++;
		}	
		loginResponse.setEmployee(employee);
		loginResponse.setBranchList(branchList);
		
		//create token 
		String token=jsonWebToken.create(claimMap,companyIdList,branchIds, Constants.ISSUER,employee.getEmployeeId());
		loginResponse.setToken(token);
		loginResponse.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		return new ResponseEntity<LoginResponse>(loginResponse,httpStatus);
		
	}
}
