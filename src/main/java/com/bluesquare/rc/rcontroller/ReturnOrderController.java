package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.responseEntities.ReturnOrderProductDetailsReport;
import com.bluesquare.rc.responseEntities.ReturnOrderProductModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.FetchOrderDetailsModel;
import com.bluesquare.rc.rest.models.GKReturnOrderReportRequest;
import com.bluesquare.rc.rest.models.GkReturnReportResponse;
import com.bluesquare.rc.rest.models.OrderDetailListByDateRangeRequest;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.PReturnOrderDetailsModel;
import com.bluesquare.rc.rest.models.PReturnOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.PReturnOrderProductDetailsModel;
import com.bluesquare.rc.rest.models.RangeRequest;
import com.bluesquare.rc.rest.models.ReIssueOrderDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeRequest;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderProductDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.rest.models.ReturnOrderResponse;
import com.bluesquare.rc.rest.models.ReturnPermanentModelRequest;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points 
 * 1.fetchReturnOrderList/{orderId}
 * 2.fetchReturnOrderDetailByBusinessIdAndDateRange
 * 3.saveReturnOrder
 * 4.fetchReturnOrderDetailsListByDateRange
 * 5.fetchReturnOrderProductListByReturnOrderProductId/{returnOrderProductId}
 * 6.fetchReturnOrderDetailsForDBReportByEmpIdDateRange
 * 7.fetchReturnOrderProductDetailsByReturnOrderProductIdForReport/{returnOrderProductId}
 * 8.fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId
 * 9.fetchReturnOrderProductListByFilter
 * 10.fetchReIssueProductDetailsForReplacementReport
 * 11.fetchReIssueOrderProductDetailsForReplacementReport/{reIssueOrderId}
 * 12.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange
 * </pre>
 */
@RestController
public class ReturnOrderController {

	@Autowired
	OrderDetails orderDetails;
	@Autowired
	OrderProductDetails orderProductDetails;

	@Autowired
	OrderDetailsDAO orderDetailsDAO;

	@Autowired
	OrderDetailsService orderDetailsService;

	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	/**
	 * <pre>
	 * fetch order details by order Id for check order Id
	 * return taken allowed only if order is delivered
	 * </pre>
	 * @param token
	 * @param orderId
	 * @return ReturnOrderResponse
	 */
	@Transactional 	@GetMapping("/fetchReturnOrderList/{orderId}")
	public ResponseEntity<ReturnOrderResponse> fetchReturnOrderListByOrderId(@RequestHeader("Authorization") String token,
			@ModelAttribute("orderId") String orderId) {

		System.out.println("fetchReturnOrderList");

		ReturnOrderResponse returnOrderResponse = new ReturnOrderResponse();
		HttpStatus httpStatus;

		orderDetails = orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
		if (orderDetails == null) {
			returnOrderResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.OK;
			returnOrderResponse.setErrorMsg("OrderId is invalid");
			return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);

		}

		if (orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED)/* || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING)*/){

			System.out.println("fetchOrderprodutListByOrderId");
			List<OrderProductDetails> list = orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
			
			//this 4 line made for make OrderProductDetails product image null  
			returnOrderResponse.setOrderProductDetails(list);			
			returnOrderResponse.setOrderDetails(orderDetails);
			returnOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;

			return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);

		}

		returnOrderResponse.setErrorMsg("Order not delivered Still");
		returnOrderResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus = HttpStatus.OK;
		return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);
	}
	/**
	 * <pre>
	 * fetch order details list for return taken by businessNameId,date range
	 * </pre>
	 * @param token
	 * @param orderDetailListByDateRangeRequest
	 * @return OrderDetailsList
	 */
	@Transactional 	@PostMapping("/fetchReturnOrderDetailByBusinessIdAndDateRange")
	public ResponseEntity<OrderDetailsList> fetchReturnOrderDetailByBusinessIdAndDateRange(@RequestHeader("Authorization") String token,
			@RequestBody OrderDetailListByDateRangeRequest orderDetailListByDateRangeRequest) {
		System.out.println("fetchReturnOrderDetailByBusinessIdAndDateRange");

		OrderDetailsList orderDetailsResponseList = new OrderDetailsList();
		HttpStatus httpStatus;

		List<OrderDetails> orderDetailsList= orderDetailsService.fetchOrderDetailBybusinessNameIdAndDateRange(
				orderDetailListByDateRangeRequest.getBusinessNameId(), orderDetailListByDateRangeRequest.getFromDate(),
				orderDetailListByDateRangeRequest.getToDate());
		if(orderDetailsList==null)
		{
			orderDetailsResponseList.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailsResponseList.setErrorMsg("Orders Not Available");
			httpStatus = HttpStatus.OK;
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: orderDetailsList){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsResponseList.setOrderDetailsList(fetchOrderDetailsModelList);
			orderDetailsResponseList.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<OrderDetailsList>(orderDetailsResponseList, httpStatus);
	}

	/*@PostMapping("/updateReturnOrder")
	 
	public ResponseEntity<BaseDomain> updateReturnOrder(@RequestBody ReturnOrderProduct returnOrderProduct){
		
		System.out.println("in update ReturnOrder"+returnOrderProduct);
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		ReturnOrderProduct returnOrderProductlist=returnOrderService.
		
		
	}*/
	/**
	 * <pre>
	 * save return order 
	 * order details status change to Constants.ORDER_STATUS_DELIVERED_PENDING
	 * </pre>
	 * @param token
	 * @param returnOrderRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/saveReturnOrder")
	public ResponseEntity<BaseDomain> saveReturnOrder(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderRequest returnOrderRequest){
		
		System.out.println("saveReturnOrder");
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		boolean isPerform=true;
		
		if(returnOrderRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(returnOrderRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(returnOrderRequest.getDbId());
			apiRequestImeiAndDbId.setIsOfflineRequest(returnOrderRequest.getIsOfflineRequest());
			apiRequestImeiAndDbId.setTaskPerform(returnOrderRequest.getTaskPerform());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		
		if(isPerform){
			returnOrderService.save(returnOrderRequest);			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
					
			
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		
		
		
	}
	/**
	 * <pre>
	 * fetch return order list by fromDate,toDate,employeeId,range,areaId
	 * </pre>
	 * @param token
	 * @param returnOrderDateRangeRequest
	 * @return
	 */
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsListByDateRange")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderDetailsListByDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
		
		System.out.println("fetchReturnOrderDetailsListByDateRange");
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		
		
		
		
			//calling the DaoImpl through service layer and passing the element through request model
			List<ReturnOrderProduct> returnOrderProductList=returnOrderService.fetchReturnOrderDetailsListByDateRange(returnOrderDateRangeRequest.getEmployeeId(),returnOrderDateRangeRequest.getFromDate(),returnOrderDateRangeRequest.getToDate(),returnOrderDateRangeRequest.getRange());
			if(returnOrderProductList==null){
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg(Constants.NO_CONTENT);
				
				httpStatus=HttpStatus.OK;
			}else{
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg(Constants.NONE);
				
				List<ReturnOrderProductModel> returnOrderProductModelList=new ArrayList<>();
				for(ReturnOrderProduct returnOrderProduct : returnOrderProductList){
					returnOrderProductModelList.add(new  ReturnOrderProductModel(
							returnOrderProduct.getReturnOrderProductId(), 
							returnOrderProduct.getOrderDetails().getBusinessName().getShopName(), 
							returnOrderProduct.getOrderDetails().getBusinessName().getArea().getName(), 
							returnOrderProduct.getTotalQuantity(), 
							returnOrderProduct.getReturnOrderProductDatetime()));
				}
				
				returnOrderDateRangeResponse.setReturnOrderProductModelList(returnOrderProductModelList);
				httpStatus=HttpStatus.OK;
			}
			
		
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse ,httpStatus);
	}

	@Transactional 	@GetMapping("/fetchReturnOrderProductListByReturnOrderProductId/{returnOrderProductId}")
	
	public ResponseEntity<ReturnOrderProductDetailsReportResponse> fetchReturnOrderProductListByReturnOrderProductId(@RequestHeader("Authorization") String token,@ModelAttribute("returnOrderProductId") String returnOrderProductId){
		
		System.out.println("fetchReturnOrderProductListByReturnOrderProductId");
		ReturnOrderProductDetailsReportResponse returnOrderProductListResponse=new ReturnOrderProductDetailsReportResponse();
		HttpStatus httpStatus;
		
			List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsByReturnOrderProductId(returnOrderProductId);
			 //returnOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetailsList);
			if (returnOrderProductDetailsList == null) {
				returnOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				returnOrderProductListResponse.setErrorMsg("OrderId is invalid");
				
			}else{
				returnOrderProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				
				ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(returnOrderProduct.getOrderDetails().getOrderId());
				fetchOrderDetailsModel.setShopName(returnOrderProduct.getOrderDetails().getBusinessName().getShopName());
				fetchOrderDetailsModel.setMobileNo(returnOrderProduct.getOrderDetails().getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModel.setOrderDate(returnOrderProduct.getOrderDetails().getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setBusinessAddress(returnOrderProduct.getOrderDetails().getBusinessName().getAddress());
				fetchOrderDetailsModel.setReturnOrderId(returnOrderProduct.getReturnOrderProductId());
				fetchOrderDetailsModel.setReturnTotalAmountWithTax(returnOrderProduct.getTotalAmountWithTax());
				fetchOrderDetailsModel.setTotalReturnQuantity(returnOrderProduct.getTotalQuantity());
				returnOrderProductListResponse.setFetchOrderDetailsModel(fetchOrderDetailsModel);
				List<ReturnOrderProductDetailsReport> returnOrderProductDetailsReportList=new ArrayList<>();
				for(ReturnOrderProductDetails returnOrderProductDetails : returnOrderProductDetailsList){
					returnOrderProductDetailsReportList.add(new ReturnOrderProductDetailsReport(
							returnOrderProductDetails.getProduct().getProductName(), 
							returnOrderProductDetails.getReturnQuantity(), 
							returnOrderProductDetails.getIssuedQuantity(), 
							returnOrderProductDetails.getReturnTotalAmountWithTax(), 
							returnOrderProductDetails.getReason(),
							returnOrderProductDetails.getType()));
				}	
	
				returnOrderProductListResponse.setReturnOrderProductDetailsReportList(returnOrderProductDetailsReportList);
			}
			return new ResponseEntity<ReturnOrderProductDetailsReportResponse>(returnOrderProductListResponse,httpStatus);
		
		
	}


	
	/**
	 * <pre>
	 * fetch return order list by employeeId,startDate,endDate,range
	 * </pre>
	 * @param token
	 * @param returnOrderDateRangeRequest
	 * @return
	 */
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsForDBReportByEmpIdDateRange")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderDetailsForDBReportByEmpIdDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest ){
		
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderDetailsForDBReportByEmpIdDateRange");
		
		
		
		
			
			List<ReturnOrderProduct> returnOrderProductList=returnOrderService.fetchReturnOrderProductListForDBReportByEmpIdDateRange
																			   (returnOrderDateRangeRequest.getEmployeeId(),
																				returnOrderDateRangeRequest.getFromDate(),
																				returnOrderDateRangeRequest.getToDate(),
																				returnOrderDateRangeRequest.getRange());
			
			if(returnOrderProductList==null)
			{
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg("No Record Found");
				httpStatus=HttpStatus.OK;
			}else
			{
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
				//returnOrderDateRangeResponse.se
				List<ReturnOrderProductModel> returnOrderProductModelList=new ArrayList<>();
				for(ReturnOrderProduct returnOrderProduct : returnOrderProductList){
					returnOrderProductModelList.add(new  ReturnOrderProductModel(
							returnOrderProduct.getReturnOrderProductId(), 
							returnOrderProduct.getOrderDetails().getBusinessName().getShopName(), 
							returnOrderProduct.getOrderDetails().getBusinessName().getArea().getName(), 
							returnOrderProduct.getTotalQuantity(), 
							returnOrderProduct.getReturnOrderProductDatetime()));
				}
				
				returnOrderDateRangeResponse.setReturnOrderProductModelList(returnOrderProductModelList);
				httpStatus=HttpStatus.OK;
				
			}
		
													
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse,httpStatus);													
	}
	
	
	//here we r fetching ReturnOrderProductDetails By returnOrderProductId For GateKeeper report
	@Transactional 	@GetMapping("/fetchReturnOrderProductDetailsByReturnOrderProductIdForReport/{returnOrderProductId}")
	public ResponseEntity<ReturnOrderProductDetailsReportResponse> fetchReturnOrderProductDetailsByReturnOrderProductIdForReport(@ModelAttribute("returnOrderProductId") String returnOrderProductId){
		
		ReturnOrderProductDetailsReportResponse gKReturnOrderReportResponse=new ReturnOrderProductDetailsReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderProductDetailsByOrderIdForGKReport");
		
			ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
			if(returnOrderProduct==null)
			{
				gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				gKReturnOrderReportResponse.setErrorMsg("No OrderFound");
				httpStatus=HttpStatus.NO_CONTENT;
			}else{
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(returnOrderProduct.getOrderDetails().getOrderId());
				fetchOrderDetailsModel.setShopName(returnOrderProduct.getOrderDetails().getBusinessName().getShopName());
				fetchOrderDetailsModel.setMobileNo(returnOrderProduct.getOrderDetails().getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModel.setOrderDate(returnOrderProduct.getOrderDetails().getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setBusinessAddress(returnOrderProduct.getOrderDetails().getBusinessName().getAddress());
				fetchOrderDetailsModel.setReturnOrderId(returnOrderProduct.getReturnOrderProductId());
				fetchOrderDetailsModel.setReturnTotalAmountWithTax(returnOrderProduct.getTotalAmountWithTax());
				fetchOrderDetailsModel.setTotalReturnQuantity(returnOrderProduct.getTotalQuantity());
				
				List<ReturnOrderProductDetailsReport> returnOrderProductDetailsReportList=new ArrayList<>();
				List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(returnOrderProductId);
				for(ReturnOrderProductDetails returnOrderProductDetails : returnOrderProductDetailsList){
					returnOrderProductDetailsReportList.add(new ReturnOrderProductDetailsReport(
							returnOrderProductDetails.getProduct().getProductName(), 
							returnOrderProductDetails.getReturnQuantity(), 
							returnOrderProductDetails.getIssuedQuantity(), 
							returnOrderProductDetails.getReturnTotalAmountWithTax(), 
							returnOrderProductDetails.getReason(),
							returnOrderProductDetails.getType()));
				}	

				gKReturnOrderReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gKReturnOrderReportResponse.setFetchOrderDetailsModel(fetchOrderDetailsModel);
				gKReturnOrderReportResponse.setReturnOrderProductDetailsReportList(returnOrderProductDetailsReportList);
				httpStatus=HttpStatus.OK;
			}
			
		
	
			return new ResponseEntity<ReturnOrderProductDetailsReportResponse>(gKReturnOrderReportResponse,httpStatus);
	}
	
	//here we r fetching ReturnOrderProductDetails By returnOrderProductId For GateKeeper permanent return
		@Transactional 	@GetMapping("/fetchReturnOrderProductDetailsByReturnOrderProductIdForReturn/{returnOrderProductId}")
		public ResponseEntity<ReturnOrderProductDetailsReportResponse> fetchReturnOrderProductDetailsByOrderIdForGKReport(@ModelAttribute("returnOrderProductId") String returnOrderProductId){
			
			ReturnOrderProductDetailsReportResponse gKReturnOrderReportResponse=new ReturnOrderProductDetailsReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchReturnOrderProductDetailsByOrderIdForGKReport");
			
				ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
				if(returnOrderProduct==null)
				{
					gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					gKReturnOrderReportResponse.setErrorMsg("No OrderFound");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				
				List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(returnOrderProductId);
				//returnOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetailsList);
				if(returnOrderProductDetailsList==null)
				{
					gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					gKReturnOrderReportResponse.setErrorMsg("No ProductDetails found");
					httpStatus=HttpStatus.NO_CONTENT;	
				}	

				gKReturnOrderReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gKReturnOrderReportResponse.setReturnOrderProductDetailsList(returnOrderProductDetailsList);
				gKReturnOrderReportResponse.setReturnOrderProduct(returnOrderProduct);
				httpStatus=HttpStatus.OK;
		

				return new ResponseEntity<ReturnOrderProductDetailsReportResponse>(gKReturnOrderReportResponse,httpStatus);
		}
	
	//here we re fetching Return OrderDetails By EmpId,DateRange and areaId
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId")
	public ResponseEntity<GkReturnReportResponse> fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(@RequestHeader("Authorization") String token,@RequestBody GKReturnOrderReportRequest gKReturnOrderReportRequest){
		
		GkReturnReportResponse gkReturnReportResponse =new GkReturnReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId");
		
		
		
		
			GkReturnReportResponse returnOrderProductList=returnOrderService.fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId
																								(gKReturnOrderReportRequest.getEmployeeId(), 
																								gKReturnOrderReportRequest.getAreaId(), 
																								gKReturnOrderReportRequest.getFromDate(),
																								gKReturnOrderReportRequest.getToDate(), 
																								gKReturnOrderReportRequest.getRange());
			if(returnOrderProductList.getReturnOrderProductList()==null){
				gkReturnReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				gkReturnReportResponse.setErrorMsg("No Return Order Found");
				httpStatus=HttpStatus.NO_CONTENT;
				
			}
			else
			{
				gkReturnReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gkReturnReportResponse.setAreaId(returnOrderProductList.getAreaId());
				gkReturnReportResponse.setReturnOrderProductList(returnOrderProductList.getReturnOrderProductList());
				httpStatus=HttpStatus.OK;
			}
		
		
		return new ResponseEntity<GkReturnReportResponse>(gkReturnReportResponse,httpStatus);
		
	}
	
	//fetchReturnOrderProductListByFilter
	@Transactional 	@PostMapping("/fetchReturnOrderProductListByFilter")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderProductListByFilter(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderProductListByFilter");
		
		
		
		
			returnOrderDateRangeResponse=returnOrderService.fetchReturnOrderProductListByFilter(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getRange(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getAreaId());
			if(returnOrderDateRangeResponse.getReturnOrderProductModelList()==null)
			{
				httpStatus=HttpStatus.OK;
				returnOrderDateRangeResponse.setErrorMsg("List not found");
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
			}
			else
			{
				httpStatus=HttpStatus.OK;
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
				
			}		
			
		
		
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse,httpStatus);
	}
	
	// here we are fetching Reissue product Details for replacement report by empId and dateRange
		@Transactional 	@PostMapping("/fetchReIssueProductDetailsForReplacementReport")
		public ResponseEntity<ReIssueOrderDetailsReportResponse> fetchReIssueProductDetailsForReplacementReport(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest ){
			
			ReIssueOrderDetailsReportResponse replacementReIssueOrderDetailsReportResponse=new ReIssueOrderDetailsReportResponse();
		   	HttpStatus httpStatus;
			System.out.println("fetchReIssueProductDetailsForReplacementReport");
			
			
			
			
				List<ReIssueOrderDetails> reIssueOrderDetailsList=returnOrderService.fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getRange());
				if(reIssueOrderDetailsList==null)
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
				}
				else
				{
					List<ReIssueOrderDetailsReport> reIssueOrderList=new ArrayList<>();
					for(ReIssueOrderDetails reIssueOrderDetails : reIssueOrderDetailsList){
						EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());
						reIssueOrderList.add(new ReIssueOrderDetailsReport(
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId(), 
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getShopName(), 
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getArea().getName(), 
								reIssueOrderDetails.getReIssueOrderId(), 
								employeeDetails.getName(), 
								employeeDetails.getEmployee().getDepartment().getName(), 
								reIssueOrderDetails.getTotalQuantity(), 
								reIssueOrderDetails.getReturnOrderProduct().getTotalQuantity(), 
								reIssueOrderDetails.getReIssueDate(), 
								reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductDatetime(), 
								reIssueOrderDetails.getReIssueDeliveredDate(), 
								reIssueOrderDetails.getStatus(),
								reIssueOrderDetails.getTotalAmountWithTax(),
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getBusinessNameId(),
								reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId(),
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getContact().getMobileNumber()));
					}
					replacementReIssueOrderDetailsReportResponse.setReIssueOrderDetailsList(reIssueOrderList);
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
					
				}
			
			return new ResponseEntity<ReIssueOrderDetailsReportResponse>(replacementReIssueOrderDetailsReportResponse,httpStatus);

		}
		//@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport/{orderId}")
		@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport/{reIssueOrderId}")
		public ResponseEntity<ReIssueOrderProductDetailsReportResponse> fetchReIssueOrderProductDetailsForReplacmentReport(@RequestHeader("Authorization") String token,@ModelAttribute("reIssueOrderId") long reIssueOrderId ){
			
			ReIssueOrderProductDetailsReportResponse replacementReportOrderProductDetailsResponse=new ReIssueOrderProductDetailsReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport");

				ReIssueOrderDetails reIssueOrderDetails=returnOrderService.fetchReIssueOrderDetailsForReplacementReportByOrderId(reIssueOrderId);
				if(reIssueOrderDetails==null)
				{
					replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=returnOrderService.fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(reIssueOrderId);
				//fetchReIssueOrderProductDetailsForReplacementReportByOrderId(orderId);
				//reIssueOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(reIssueOrderProductDetailsList);
								
				EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());

				if(reIssueOrderProductDetailsList==null)
				{
					replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{				
					FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
					fetchOrderDetailsModel.setOrderId(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId());
					fetchOrderDetailsModel.setReturnOrderId(reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId());
					fetchOrderDetailsModel.setShopName(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getShopName());
					fetchOrderDetailsModel.setMobileNo(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getContact().getMobileNumber());
					fetchOrderDetailsModel.setDeliveryPersonName(employeeDetails.getName());
					fetchOrderDetailsModel.setReturnDate(reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductDatetime());
					fetchOrderDetailsModel.setTotalReturnQuantity(reIssueOrderDetails.getReturnOrderProduct().getTotalQuantity());
					fetchOrderDetailsModel.setTotalReIssueQuantity(reIssueOrderDetails.getTotalQuantity());
					fetchOrderDetailsModel.setReIssueTotalAmountWithTax(reIssueOrderDetails.getTotalAmountWithTax());
					fetchOrderDetailsModel.setReturnTotalAmountWithTax(reIssueOrderDetails.getReturnOrderProduct().getTotalAmountWithTax());
					fetchOrderDetailsModel.setReIssueDeliveredDate(reIssueOrderDetails.getReIssueDeliveryDate());
					fetchOrderDetailsModel.setBusinessAddress(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getAddress());
					
					List<ReturnOrderProductDetailsReport> reIssueOrderProductDetailList=new ArrayList<>();
					for(ReIssueOrderProductDetails reIssueOrderProductDetails : reIssueOrderProductDetailsList){
						reIssueOrderProductDetailList.add(new ReturnOrderProductDetailsReport(
								reIssueOrderProductDetails.getProduct().getProductName(), 
								reIssueOrderProductDetails.getReturnQuantity(), 
								reIssueOrderProductDetails.getReIssueQuantity(),
								reIssueOrderProductDetails.getType()));
					}
					
					replacementReportOrderProductDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
					replacementReportOrderProductDetailsResponse.setReIssueOrderDetails(fetchOrderDetailsModel);
					replacementReportOrderProductDetailsResponse.setReIssueOrderProductDetailsList(reIssueOrderProductDetailList);
					httpStatus=HttpStatus.OK;
				}
			
		
			return new ResponseEntity<ReIssueOrderProductDetailsReportResponse>(replacementReportOrderProductDetailsResponse,httpStatus);
		}
		    
		
		/**
		 * <pre>
		 *  fetching OrderDetails For Gk Replacment Report By empId and Date range 
		 * </pre>
		 * @param token
		 * @param returnOrderDateRangeRequest
		 * @return
		 */
		@Transactional 	@PostMapping("/fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange")
		public ResponseEntity<ReIssueOrderDetailsReportResponse> fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
			
			ReIssueOrderDetailsReportResponse replacementReIssueOrderDetailsReportResponse=new ReIssueOrderDetailsReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange");
			
			
			
			
				List<ReIssueOrderDetails> reIssueOrderDetailsList=returnOrderService.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getRange());
				if(reIssueOrderDetailsList==null)
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
					
				}
				else
				{
					List<ReIssueOrderDetailsReport> reIssueOrderList=new ArrayList<>();
					for(ReIssueOrderDetails reIssueOrderDetails : reIssueOrderDetailsList){
						EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());
						reIssueOrderList.add(new ReIssueOrderDetailsReport(
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId(), 
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getShopName(), 
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getArea().getName(), 
								reIssueOrderDetails.getReIssueOrderId(), 
								employeeDetails.getName(), 
								employeeDetails.getEmployee().getDepartment().getName(), 
								reIssueOrderDetails.getTotalQuantity(), 
								reIssueOrderDetails.getReturnOrderProduct().getTotalQuantity(), 
								reIssueOrderDetails.getReIssueDate(), 
								reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductDatetime(), 
								reIssueOrderDetails.getReIssueDeliveredDate(), 
								reIssueOrderDetails.getStatus(),
								reIssueOrderDetails.getTotalAmountWithTax(),
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getBusinessNameId(),
								reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId(),
								reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getContact().getMobileNumber()));
					}
					replacementReIssueOrderDetailsReportResponse.setReIssueOrderDetailsList(reIssueOrderList);
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
					
				}
			
			return new ResponseEntity<ReIssueOrderDetailsReportResponse>(replacementReIssueOrderDetailsReportResponse,httpStatus);
		}
		/**
		 * <pre>
		 *  save permanent return order
		 * </pre>
		 * @param token
		 * @param returnOrderDateRangeRequest
		 * @return
		 */
		@Transactional 	@PostMapping("/savePermanentOrder")
		public ResponseEntity<BaseDomain> savePermanentOrder(@RequestBody ReturnPermanentModelRequest returnPermanentModelRequest){
			
			BaseDomain baseDomain=new BaseDomain();
			HttpStatus httpStatus;
			System.out.println("savePermanentOrder");
			
			orderDetailsService.savePermanentReturnOrder(returnPermanentModelRequest);
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
			return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
		}
		
		/**
		 * <pre>
		 * return order shown by range,startDate,endDate
		 * </pre>
		 * @param request
		 * @param model
		 * @param session
		 * @return ReturnItemReport.jsp
		 */ 
		@Transactional 	@RequestMapping("/permanentReturnOrderReportAPP")
		public ResponseEntity<PReturnOrderProductDetailsListModel> permanentReturnOrderReport(@RequestBody RangeRequest rangeRequest, HttpServletRequest request,Model model,HttpSession session) {
			
			PReturnOrderProductDetailsListModel returnOrderProductDetailsListModel=new PReturnOrderProductDetailsListModel();
			
			String filter=rangeRequest.getRange();
			String startDate=rangeRequest.getFromDate();
			String endDate=rangeRequest.getToDate();
			  
			List<PReturnOrderDetailsModel> pReturnOrderDetailsModelList= orderDetailsService.permanentReturnOrderReport(filter,startDate,endDate);
			if(pReturnOrderDetailsModelList==null){
				returnOrderProductDetailsListModel.setStatus(Constants.FAILURE_RESPONSE);
				returnOrderProductDetailsListModel.setErrorMsg("Data Not Found");
			}else{
				returnOrderProductDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
				returnOrderProductDetailsListModel.setpReturnOrderDetailsModelList(pReturnOrderDetailsModelList);
			}
			
			return new ResponseEntity<PReturnOrderProductDetailsListModel>(returnOrderProductDetailsListModel,HttpStatus.OK);
		}
		
		/**
		 * <pre>
		 * fetch return order product details by return order id
		 * </pre>
		 * @param request
		 * @param model
		 * @param session
		 * @return ReturnItemDetails.jsp
		 */
		@Transactional 	@RequestMapping("/permanentReturnOrderDetailsByReturnOrderIdAPP/{returnOrderProductId}")
		public ResponseEntity<PReturnOrderProductDetailsListModel> permanentReturnOrderDetailsByReturnOrderId(@ModelAttribute("returnOrderProductId") String returnOrderProductId, HttpServletRequest request, Model model,HttpSession session) {
			
			ReturnOrderProductP returnOrderProductP=orderDetailsService.fetchReturnOrderProductPByReturnOrderProductId(returnOrderProductId);
			List<ReturnOrderProductDetailsP> pReturnOrderProductDetailsModelList=orderDetailsService.fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(returnOrderProductId);
			
			List<PReturnOrderProductDetailsModel> returnOrderProductDetailsModelList=new ArrayList<>();
			long srno=1;
			for(ReturnOrderProductDetailsP returnOrderProductDetailsP : pReturnOrderProductDetailsModelList){
				returnOrderProductDetailsModelList.add(new PReturnOrderProductDetailsModel(
						srno, 
						returnOrderProductDetailsP.getProduct().getProductName(), 
						returnOrderProductDetailsP.getIssuedQuantity(),
						returnOrderProductDetailsP.getReturnQuantity(), 
						returnOrderProductDetailsP.getReturnTotalAmountWithTax(),
						returnOrderProductDetailsP.getType(),
						returnOrderProductDetailsP.getReason()));
				srno++;
			}
			
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnOrderProductP.getEmployee().getEmployeeId());
			
			PReturnOrderProductDetailsListModel pReturnOrderProductDetailsListModel=new PReturnOrderProductDetailsListModel();
			pReturnOrderProductDetailsListModel.setReturnOrderDetailsModel(new PReturnOrderDetailsModel(
					0,
					returnOrderProductP.getReturnOrderProductId(), 
					returnOrderProductP.getOrderDetails().getOrderId(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getShopName(),
					returnOrderProductP.getOrderDetails().getBusinessName().getAddress(),
					employeeDetails.getName(), 
					employeeDetails.getAddress(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getContact().getMobileNumber(),
					employeeDetails.getEmployeeDetailsId(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getName(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getName(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getCity().getName(), 
					returnOrderProductP.getTotalQuantity(), 
					returnOrderProductP.getTotalAmount(), 
					returnOrderProductP.getTotalAmountWithTax()-returnOrderProductP.getTotalAmount(), 
					returnOrderProductP.getTotalAmountWithTax(), 
					returnOrderProductP.getOrderDetails().getOrderDetailsAddedDatetime(), 
					returnOrderProductP.getReturnOrderProductDatetime()));
			pReturnOrderProductDetailsListModel.setReturnOrderProductDetailsModelList(returnOrderProductDetailsModelList);
			
			pReturnOrderProductDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
			
			return new ResponseEntity<PReturnOrderProductDetailsListModel>(pReturnOrderProductDetailsListModel,HttpStatus.OK);
		}
}
