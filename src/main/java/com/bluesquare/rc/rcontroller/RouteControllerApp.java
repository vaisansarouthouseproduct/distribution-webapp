package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.models.RouteResponse;
import com.bluesquare.rc.responseEntities.RouteModel;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 10-07-2018 Code Documentation
 * API End Points 
 * 1.fetch_route_list_app
 * </pre>
 */
@RestController
public class RouteControllerApp {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	/**
	 * fetch employee routes
	 * @param model
	 * @param session
	 * @return route list
	 */
	@Transactional
	@RequestMapping("/fetch_route_list_app")
	public ResponseEntity<RouteResponse> fetchRouteList(Model model,HttpSession session) {
		System.out.println("fetch route list");

		RouteResponse routeListResponse = new RouteResponse();
		
		List<Route> routeList=employeeService.fetchRouteListByEmployeeId(tokenHandlerDAO.getAppLoggedEmployeeId());
		if(routeList.isEmpty()){
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			routeListResponse.setErrorMsg("Route List Not Found");
		}else{
			List<RouteModel> routeModelList=new ArrayList<>();
			for(Route route: routeList){
				RouteModel routeModel=new RouteModel();
				routeModel.setRouteId(route.getRouteId());
				routeModel.setRouteGenId(route.getRouteGenId());
				routeModel.setRouteName(route.getRouteName());
				routeModelList.add(routeModel);
			}
			routeListResponse.setRouteList(routeModelList);
			routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}

		return new ResponseEntity<RouteResponse>(routeListResponse, HttpStatus.OK);
	}
}
