package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.NotePad;
import com.bluesquare.rc.responseEntities.NotePadModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.NotepadResponse;
import com.bluesquare.rc.service.NotepadService;
import com.bluesquare.rc.utils.Constants;


/**
 * <pre>
 * @author Sachin Sharma 06-06-2018 Code Documentation
 * API End Points
 * 1.saveNotepad
 * 2."fetchNotepadById/{id}"
 * 3."fetchNotepadListByEmployeeId/{employeeId}"
 * 4.updateNotepad
 * 5.deleteSingleNotepadById/{id}
 * 6.deleteNotepadByIdList
 * </pre>
 */

@RestController
public class NotepadContoller {

	@Autowired 
	NotepadService notepadService;
	
	@Transactional
	@PostMapping("saveNotepad")
	public ResponseEntity<BaseDomain> saveNotePad(@RequestHeader("Authorization") String token,@RequestBody NotePad notePad){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("saveNotePad");
		
		try {
			notepadService.saveNotePad(notePad);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	@Transactional
	@GetMapping("fetchNotepadById/{id}")
	public ResponseEntity<NotepadResponse> fetchNotepadById(@RequestHeader("Authorization") String token,@ModelAttribute("id") long id){
		NotepadResponse notepadResponse=new NotepadResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchNotepadById");
		NotePad notePad=notepadService.fetchNotepadById(id);
		if(notePad==null){
			notepadResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.NO_CONTENT;
			notepadResponse.setErrorMsg("No Data Found");
		}else{
			notepadResponse.setNotePad(new NotePadModel(
					notePad.getNotepadId(), 
					notePad.getNotepadTitle(), 
					notePad.getNotepadText(), 
					notePad.getNotepadAddedDateTime(), 
					notePad.getNotepadUpdatedDateTime()) );
			notepadResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<NotepadResponse>(notepadResponse,httpStatus);
	}
	

	@Transactional
	@GetMapping("fetchNotepadListByEmployeeId/{employeeId}")
	public ResponseEntity<NotepadResponse> fetchNotepadListByEmployeeId(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId){
		NotepadResponse notepadResponse=new NotepadResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchNotepadById");
		List<NotePad> notePadList=notepadService.fetchNotePadListByEmployeeId(employeeId);
		if(notePadList==null){
			notepadResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.NO_CONTENT;
			notepadResponse.setErrorMsg("No Data Found");
		}else{
			List<NotePadModel> notePadModelList=new ArrayList<>();
			for(NotePad notePad: notePadList){
				notePadModelList.add(new NotePadModel(
						notePad.getNotepadId(), 
						notePad.getNotepadTitle(), 
						notePad.getNotepadText(), 
						notePad.getNotepadAddedDateTime(), 
						notePad.getNotepadUpdatedDateTime()) );
			}
			
			notepadResponse.setNotePadList(notePadModelList);
			notepadResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<NotepadResponse>(notepadResponse,httpStatus);
	}
	
	@Transactional
	@PostMapping("updateNotepad")
	public ResponseEntity<BaseDomain> updateNotepad(@RequestHeader("Authorization") String token,@RequestBody NotePad notePad){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("saveNotePad");
		NotePad notePadOld=notepadService.fetchNotepadById(notePad.getNotepadId());
		try {
			notePad.setEmployee(notePadOld.getEmployee());
			notepadService.updateNotePad(notePad);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	@Transactional
	@GetMapping("deleteSingleNotepadById/{id}")
	public ResponseEntity<BaseDomain> deleteSingleNotepadById(@RequestHeader("Authorization") String token,@ModelAttribute("id") long id){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("deleteSingleNotepadById");
		
		try {
			notepadService.deleteNotePadById(id);;
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	@Transactional
	@PostMapping("deleteNotepadByIdList")
	public ResponseEntity<BaseDomain> deleteNotepadByIdList(@RequestHeader("Authorization") String token,@RequestBody List<Long> notepadIdList){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("saveNotePad");
		
		try {
			notepadService.deleteNotepadByIdList(notepadIdList);;
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
}
