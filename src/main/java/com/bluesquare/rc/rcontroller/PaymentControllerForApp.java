package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentMethodResponse;
import com.bluesquare.rc.rest.models.PaymentTakeRequest;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.savePaymentStatus
 * 2.fetchPaymentListByOrderIdForApp/{orderId}
 * </pre>
 */
@RestController
public class PaymentControllerForApp {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	OrderDetails orderDetails;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	/**
	 * <pre>
	 * save payment of salesman order
	 * </pre>
	 * @param token
	 * @param paymentTakeRequest
	 * @return BaseDomain with message
	 */
	@Transactional 	@PostMapping("/savePaymentStatus")
	public ResponseEntity<BaseDomain> savePaymentStatus(@RequestHeader("Authorization") String token,@RequestBody PaymentTakeRequest paymentTakeRequest) {
		BaseDomain baseDomain=new BaseDomain();                                                                                                                                                                                                   
		HttpStatus httpStatus;
		
		boolean isPerform=true;
		
		if(paymentTakeRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
			
			OfflineAPIRequest apiRequestImeiAndDbId=new OfflineAPIRequest();
			apiRequestImeiAndDbId.setImeiNumber(paymentTakeRequest.getImeiNumber());
			apiRequestImeiAndDbId.setDbId(paymentTakeRequest.getDbId());
			apiRequestImeiAndDbId.setTaskPerform(paymentTakeRequest.getTaskPerform());
			apiRequestImeiAndDbId.setIsOfflineRequest(paymentTakeRequest.getIsOfflineRequest());
			
			OfflineAPIRequest apiRequest=orderDetailsService.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequestImeiAndDbId);
			if(apiRequest==null){
				orderDetailsService.saveOfflineAPIrequest(apiRequestImeiAndDbId);
			}else{
				if(apiRequest.getStatus().equalsIgnoreCase(Constants.OFFLINE_STATUS_COMPLETE)){
					isPerform=false;
				}
				
			}
			
			
		}
		if(isPerform){
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(paymentTakeRequest.getOrderId());
			orderDetails=paymentService.savePaymentStatus(paymentTakeRequest,orderDetails);
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			
			if(paymentTakeRequest.getIsOfflineRequest().equalsIgnoreCase("Yes")){
				orderDetailsService.updateTheStatusOfOfflineAPIRequest(paymentTakeRequest.getDbId(), paymentTakeRequest.getImeiNumber(), Constants.OFFLINE_STATUS_COMPLETE);
			}
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
	
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	/*@GetMapping("/fetchCollectionDetailByOrderId/{orderId}")
	public ResponseEntity<CollectionReportResponse> fetchCollectionDetailByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		
		CollectionReportResponse collectionReportResponse=new CollectionReportResponse();
		HttpStatus httpStatus;
		
		if(jsonWebToken.verifyToken(token)){
			collectionReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.UNAUTHORIZED;
			return new ResponseEntity<CollectionReportResponse>(collectionReportResponse, httpStatus);
		}	
		session.setAttribute("authToken", token);
		
		
			collectionReportResponse=paymentService.fetchCollectionDetailsByOrderId(orderId);
			collectionReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
		
		
		return new ResponseEntity<CollectionReportResponse>(collectionReportResponse,httpStatus);
		
	}*/
	/**
	 * <pre>
	 * fetch payment list by order Id
	 * </pre>
	 * @param token
	 * @param orderId
	 * @return PaymentListModel
	 */
	@Transactional 	@GetMapping("/fetchPaymentListByOrderIdForApp/{orderId}")
	public ResponseEntity<PaymentListModel> fetchPaymentListByOrderIdForApp(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		
		PaymentListModel paymentListModel=new PaymentListModel();
		HttpStatus httpStatus;
		
		paymentListModel=paymentService.fetchPaymentListByOrderIdForApp(orderId);
		paymentListModel.setStatus(Constants.SUCCESS_RESPONSE);
		
		httpStatus=HttpStatus.OK;		
		
		return new ResponseEntity<PaymentListModel>(paymentListModel,httpStatus);
	}
	
	@Transactional
	@GetMapping("/fetchPaymentMethodList")
	public ResponseEntity<PaymentMethodResponse> fetchPaymentMethodList(@RequestHeader("Authorization") String token){
		
		PaymentMethodResponse paymentMethodResponse=new PaymentMethodResponse();
		HttpStatus httpStatus;
		
		try {
			List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
			if (paymentMethodList==null) {
				paymentMethodResponse.setErrorMsg("Data not found");
				paymentMethodResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				
			}else{
				paymentMethodResponse.setPaymentMethodList(paymentMethodList);
				paymentMethodResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			paymentMethodResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<PaymentMethodResponse>(paymentMethodResponse,httpStatus);
	}
}
