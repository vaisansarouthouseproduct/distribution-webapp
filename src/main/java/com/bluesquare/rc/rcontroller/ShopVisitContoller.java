package com.bluesquare.rc.rcontroller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.entities.ShopVisit;
import com.bluesquare.rc.entities.ShopVisitImages;
import com.bluesquare.rc.responseEntities.ShopVisitModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.EmployeeImageModal;
import com.bluesquare.rc.rest.models.RangeRequest;
import com.bluesquare.rc.rest.models.ShopVisitResponse;
import com.bluesquare.rc.rest.models.ShopVisitSaveUpdateRequest;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.MeetingService;
import com.bluesquare.rc.service.ShopVisitService;
import com.bluesquare.rc.utils.Constants;


/**
 * <pre>
 * @author Sachin Sharma 02-06-2018 Code Documentation
 * API End Points 
 * 1.saveShopVisitForm
 * 2.fetchShopVisitListByDateRange
 * 3.fetchShopVisitById/{id}
 * 4.updateShopVisit
 * </pre>
 */
@RestController
public class ShopVisitContoller {
	
	@Autowired
	ShopVisitService  shopVisitService;
	
	@Autowired
	MeetingService meetingService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Transactional
	@PostMapping("saveShopVisitForm")
	public ResponseEntity<BaseDomain> saveShopVisitForm(@RequestHeader("Authorization") String token,@RequestBody ShopVisitSaveUpdateRequest shopVisitSaveUpdateRequest){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
	
		
		try {
			ShopVisit shopVisit=shopVisitSaveUpdateRequest.getShopVisit();
			shopVisit.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
			shopVisitService.saveShopVisit(shopVisit);
			if(shopVisitSaveUpdateRequest.getShopVisitImage()!=null){
				shopVisitService.saveShopVisitImage(shopVisitSaveUpdateRequest.getShopVisitImage(), shopVisit);
			}
			if (shopVisit.getIsNextVisit().equalsIgnoreCase("Yes")) {
				Meeting meeting=new Meeting();
				
				meeting.setShopVisit(shopVisit);
				meeting.setMeetingFromDateTime(shopVisit.getNextVisitTime());
				meeting.setMeetingToDateTime(shopVisit.getNextVisitEndTime());
				
				SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
				SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
				
				long employeeId=shopVisit.getEmployee().getEmployeeId();
				String date=sdfDate.format(meeting.getMeetingFromDateTime());
				String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
				String toTime=sdfTime.format(meeting.getMeetingToDateTime());
				
				String msg=meetingService.checkMeetingSchedule(employeeId, date, fromTime, toTime);
				
				if(!msg.equals("")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg(msg);
					return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
				}
				
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				
				meeting.setEmployeeDetails(employeeDetails);
				
				meeting.setMeetingStatus(Constants.MEETING_PENDING);
				meeting.setAddedDateTime(new Date());
				meeting.setUpdatedDateTime(null);
		
				meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
				meetingService.saveMeeting(meeting);
				
				
			}
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<>(baseDomain,httpStatus);
	
	}
	
	@Transactional
	@PostMapping("fetchShopVisitListByDateRange")
	public ResponseEntity<ShopVisitResponse> fetchShopVisitListByDateRange(@RequestHeader("Authorization") String token,@RequestBody RangeRequest shopVisitRequest){
		ShopVisitResponse shopVisitResponse=new ShopVisitResponse();
		HttpStatus httpStatus;
		
		List<ShopVisit> shopVisitList=shopVisitService.fetchShopVisitedByDateRange(shopVisitRequest.getEmployeeId(), shopVisitRequest.getRange(), shopVisitRequest.getFromDate(), shopVisitRequest.getToDate());
		if(shopVisitList==null){
			shopVisitResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			shopVisitResponse.setErrorMsg("No Data Found");
		}else{
			List<ShopVisitModel> shopVisitModelList=new ArrayList<>();
			for(ShopVisit shopVisit : shopVisitList){
				shopVisitModelList.add(new ShopVisitModel(
						shopVisit.getId(), 
						shopVisit.getOwnerName(), 
						shopVisit.getShopName(), 
						shopVisit.getAddress(), 
						shopVisit.getComment(), 
						shopVisit.getContact(), 
						shopVisit.getShopVisitAddeddDatetime(), 
						shopVisit.getNextVisitDate(), 
						shopVisit.getNextVisitTime(), 
						shopVisit.getNextVisitEndTime(), 
						shopVisit.getShopVisitUpdatedDatetime(), 
						shopVisit.getIsNextVisit()));
			}
			shopVisitResponse.setShopVisitList(shopVisitModelList);
			shopVisitResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<ShopVisitResponse>(shopVisitResponse,httpStatus);
	}

	@Transactional
	@GetMapping("fetchShopVisitById/{id}")
	public ResponseEntity<ShopVisitResponse> fetchShopVisitById(@RequestHeader("Authorization") String token,@ModelAttribute("id") long id){
		ShopVisitResponse shopVisitResponse=new ShopVisitResponse();
		HttpStatus httpStatus;
		
		ShopVisit shopVisit=shopVisitService.fetchShopVisitById(id);
		if(shopVisit==null){
			shopVisitResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			shopVisitResponse.setErrorMsg("No Data Found");
		}else{
			shopVisitResponse.setShopVisit(new ShopVisitModel(
					shopVisit.getId(), 
					shopVisit.getOwnerName(), 
					shopVisit.getShopName(), 
					shopVisit.getAddress(), 
					shopVisit.getComment(), 
					shopVisit.getContact(), 
					shopVisit.getShopVisitAddeddDatetime(), 
					shopVisit.getNextVisitDate(), 
					shopVisit.getNextVisitTime(), 
					shopVisit.getNextVisitEndTime(), 
					shopVisit.getShopVisitUpdatedDatetime(), 
					shopVisit.getIsNextVisit()));
			ShopVisitImages shopVisitImages=shopVisitService.fetchShopVisitImage(id);
			
			if(shopVisitImages!=null){
				shopVisitResponse.setShopVisitImage(shopVisitImages.getShopImage());	
			}
			
			shopVisitResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<ShopVisitResponse>(shopVisitResponse,httpStatus);
	}
	/**
	 * not used ... image set on (fetchShopVisitById/{id}) response
	 * fetch employee image by employeeId
	 * @return employeeImage
	 */
	@Transactional 	@PostMapping("/fetchShopImage/{shopVisitId}")
    public ResponseEntity<ShopVisitResponse> fetchEmployeeImage(@ModelAttribute("shopVisitId") long shopVisitId ) {
		ShopVisitResponse shopVisitResponse=new ShopVisitResponse();
		ShopVisitImages shopVisitImages=shopVisitService.fetchShopVisitImage(shopVisitId);
		if(shopVisitImages==null){
			shopVisitResponse.setStatus(Constants.FAILURE_RESPONSE);
		}else{
			shopVisitResponse.setStatus(Constants.SUCCESS_RESPONSE);
			shopVisitResponse.setShopVisitImage(shopVisitImages.getShopImage());
		}		
    	return new ResponseEntity<ShopVisitResponse>(shopVisitResponse,HttpStatus.OK);
    }
	
	@Transactional
	@PostMapping("updateShopVisit")
	public ResponseEntity<BaseDomain> updateShopVisit(@RequestHeader("Authorization") String token,@RequestBody ShopVisitSaveUpdateRequest shopVisitSaveUpdateRequest){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
	
		
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
		
		
		try {
			ShopVisit shopVisit=shopVisitSaveUpdateRequest.getShopVisit();
			ShopVisit shopVisitOld=shopVisitService.fetchShopVisitById(shopVisit.getId());
			shopVisit.setEmployee(shopVisitOld.getEmployee());
			shopVisit.setBranch(shopVisitOld.getBranch());
			/*SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);
			String date=sdfDate.format(shopVisit.getNextVisitDate());
			String fromTime=sdfTime.format(shopVisit.getNextVisitTime());
			String toTime=sdfTime.format(shopVisit.getNextVisitEndTime());
			
			String msg=meetingService.checkMeetingScheduleForUpdate(shopVisit.getEmployee().getEmployeeId(), date, fromTime, toTime);
			if(!msg.equals("")){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(msg);
				return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
			}
			*/
			
			
			if (shopVisit.getIsNextVisit().equalsIgnoreCase("Yes")) {
				Meeting meetingOld=meetingService.fetchMeetingByShopVistedId(shopVisit.getId());
				
				// if part : meeting is not created for the Shop Visit then crate a meeting
				// else part: if meeting is created then update their detail
				
				if (meetingOld==null) {
					Meeting meeting=new Meeting();
					
					meeting.setShopVisit(shopVisit);
					meeting.setMeetingFromDateTime(shopVisit.getNextVisitTime());
					meeting.setMeetingToDateTime(shopVisit.getNextVisitEndTime());
					
					/*SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
					SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.US);*/
					
					long employeeId=shopVisit.getEmployee().getEmployeeId();
					String date1=sdfDate.format(meeting.getMeetingFromDateTime());
					String fromTime1=sdfTime.format(meeting.getMeetingFromDateTime());
					String toTime1=sdfTime.format(meeting.getMeetingToDateTime());
					
					String msg1=meetingService.checkMeetingSchedule(employeeId, date1, fromTime1, toTime1);
					
					if(!msg1.equals("")){
						baseDomain.setStatus(Constants.FAILURE_RESPONSE);
						baseDomain.setErrorMsg(msg1);
						return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
					}
					
					EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
					
					meeting.setEmployeeDetails(employeeDetails);
					
					meeting.setMeetingStatus(Constants.MEETING_PENDING);
					meeting.setAddedDateTime(new Date());
					meeting.setUpdatedDateTime(null);
			
					meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
					meetingService.saveMeeting(meeting);
			
				}else{
					
					Meeting meeting=meetingOld;
					if(meeting.getMeetingStatus().equals(Constants.MEETING_COMPLETE)){
						baseDomain.setStatus(Constants.FAILURE_RESPONSE);
						baseDomain.setErrorMsg("Next Visit is completd,Now You can not update!!");
						return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
					}
					if(meeting.getMeetingStatus().equals(Constants.MEETING_CANCEL)){
						baseDomain.setStatus(Constants.FAILURE_RESPONSE);
						baseDomain.setErrorMsg("Next Visit is cacelled,Now You can not update");
						return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
					}
					meeting.setShopVisit(shopVisit);
					meeting.setMeetingFromDateTime(shopVisit.getNextVisitTime());
					meeting.setMeetingToDateTime(shopVisit.getNextVisitEndTime());
				
					long employeeId=shopVisit.getEmployee().getEmployeeId();
					String date=sdfDate.format(meeting.getMeetingFromDateTime());
					String fromTime=sdfTime.format(meeting.getMeetingFromDateTime());
					String toTime=sdfTime.format(meeting.getMeetingToDateTime());
					
					String msg=meetingService.checkMeetingScheduleForUpdate(employeeId, date, fromTime, toTime,meeting.getMeetingId());
					
					if(!msg.equals("")){
						baseDomain.setStatus(Constants.FAILURE_RESPONSE);
						baseDomain.setErrorMsg(msg);
						return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
					}
					
					EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
					
					meeting.setEmployeeDetails(employeeDetails);
					
					meeting.setMeetingStatus(Constants.MEETING_PENDING);
					meeting.setAddedDateTime(new Date());
					meeting.setUpdatedDateTime(null);
			
					
					meetingService.updateMeeting(meeting);
			
				}
			}
			
			shopVisitService.updateShopVisit(shopVisit);
			if(shopVisitSaveUpdateRequest.getShopVisitImage()!=null){
				shopVisitService.updateShopVisitImage(shopVisitSaveUpdateRequest.getShopVisitImage(), shopVisit);
			}
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		
		return new ResponseEntity<>(baseDomain,httpStatus);
	
	}
}
