/**
 * 
 */
package com.bluesquare.rc.rcontroller;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.EmailSender;

/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * catch rest controller exception and send FORBIDDEN httpStatus with failed message
 * </pre>
 */
@RestControllerAdvice("com.bluesquare.rc.rcontroller")
public class ExceptionHandlerRestController {

	
	@ExceptionHandler(value=Exception.class)
	public ResponseEntity<BaseDomain> saveBusinessName(Exception e,HttpServletRequest request){
				
		System.err.println("Exception url "+ request.getRequestURL());
		System.err.println("Exception Rest-Controller : "+e.toString());
		System.err.println("Exception Rest-Controller Jump To Error : "+e.getStackTrace()[0]);
		
		BaseDomain baseDomain=new BaseDomain();
		baseDomain.setStatus(Constants.FAILURE_RESPONSE);
		baseDomain.setErrorMsg("Something Went Wrong");
		HttpStatus httpStatus=HttpStatus.FORBIDDEN;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	
}
