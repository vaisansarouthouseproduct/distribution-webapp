package com.bluesquare.rc.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.models.BranchListResponse;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.utils.Constants;


/**
 * <pre>
 * @author Sachin Sharma 02-07-2018 Code Documentation
 * API End Points
 * 1.fetchBranchListByEmployeeId
 * 
 * </pre>
 */
@RestController
public class BranchAppController {

	@Autowired
	BranchService branchService;
	
	
	@Transactional
	@GetMapping("/fetchBranchListByEmployeeId/{employeeId}")
	public ResponseEntity<BranchListResponse> fetchBranchListByEmployeeId(@ModelAttribute("employeeId") long employeeId,@RequestHeader("Authorization") String token){
		BranchListResponse branchListResponse=new BranchListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchBranchListByEmployeeId");
		
		List<BranchModel> branchList=branchService.fetchBranchByEmployeeId(employeeId);
		if(branchList==null){
			branchListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			branchListResponse.setErrorMsg("Branch List not found");
		}else{
			branchListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			branchListResponse.setBranchList(branchList);
			httpStatus=HttpStatus.OK;
		}
		
		return new ResponseEntity<BranchListResponse>(branchListResponse, httpStatus);
	}
}
