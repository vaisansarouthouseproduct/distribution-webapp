package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.ProformaOrder;
import com.bluesquare.rc.entities.ProformaOrderProductDetails;
import com.bluesquare.rc.entities.ReturnCounterOrder;
import com.bluesquare.rc.entities.ReturnCounterOrderProducts;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.CounterReturnOrderModel;
import com.bluesquare.rc.models.InvoiceDetails;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.models.ProformaOrderReport;
import com.bluesquare.rc.models.ReturnCounterRequest;
import com.bluesquare.rc.responseEntities.CounterOrderModel;
import com.bluesquare.rc.responseEntities.CounterOrderProductDetailsModel;
import com.bluesquare.rc.rest.models.OrderReportList;

public interface CounterOrderService {
	public String saveCounterOrder(String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo, long transportationId,String vehicalNumber,
			String docketNumber,String transactionRefId,String comment,long paymentMethodId,String discountType,String discountAmount,double trasportationCharge);
	public BillPrintDataModel fetchCounterBillPrintData(String counterOrderId);
	public List<OrderReportList> showCounterOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate);
	public CounterOrder fetchCounterOrder(String counterId);
	public List<CounterOrder> fetchCounterOrderByRange(String businessNameId,String range,String startDate,String endDate);
	public List<OrderProductDetailListForWebApp> fetchCounterOrderProductDetailsForShowOrderDetails(String counterId);
	public List<PaymentCounter> fetchPaymentCounterListByCounterOrderId(String counterOrderId);
	public List<PaymentCounterReport> fetchPaymentCounterReportListByCounterOrderId(String counterOrderId);
	public List<CounterOrderReport> fetchCounterOrderReport(String range,String startDate,String endDate);
	public PaymentDoInfo fetchPaymentInfoByCounterOrderId(String counterOrderId);
	public void savePaymentCounter(PaymentCounter paymentCounter);
	public void updateCounterOrder(CounterOrder counterOrder);
	public void deleteCounterOrder(String counterOrderId);
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetails(String counterId);
	public List<CounterOrderProductDetailsModel> fetchCounterOrderProductDetailsModelList(String counterId);
	public String updateCounterOrderForEdit(String counterOrderId,String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String refAmount,String paymentSituation,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo, long transportationId,String vehicalNumber
			,String docketNumber,String transactionRefId, String comment, long paymentMethodId,String discountType,String discountAmount,double trasportationCharge);
	public void deletePayment(long paymentId);
	public void defineChequeBounced(long paymentId);
	public void updatePayment(PaymentCounter paymentCounter);
	public PaymentCounter fetchPaymentCounterByPaymentCounterId(long paymentCounterId);
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate);
	public List<InvoiceDetails> fetchInvoiceDetails(String startDate ,String endDate);
	public void setBadDebtsOfCounter(String counterOrderId);
	public List<OrderReportList> showCounterOrderReportByBusinessNameIdForBadDebts(String businessNameId);
	public List<OrderReportList> showCounterOrderReportByExternalCustomerCounterOrderIdForBadDebts(String counterOrderId);
	public long saveReturnCounterOrderDetails(ReturnCounterRequest returnCounterRequest);
	public ReturnCounterOrder fetchReturnCounterOrder(long returnCounterOrderId);
	public List<ReturnCounterOrderProducts> fetchReturnCounterOrderProductList(long returnCounterOrderId);
	public List<OrderProductDetailListForWebApp> fetchReturnCounterOrderProductModelList(long returnCounterOrderId);
	public List<CounterReturnOrderModel> counterReturnOrderReport(String range,String startDate,String endDate);
	public BillPrintDataModel fetchReturnCounterBillPrintData(long returnCounterOrderId);
	
	public BillPrintDataModel fetchCounterBillPrintDataForWhatsApp(String counterOrderId,long companyId,long branchId);
	
	//Proforma Order
		public String saveProformaOrder(String productDetailsList, String businessNameId, long transportationId,
				String vehicalNumber, String docketNumber, String transactionRefId, String comment, long paymentMethodId,
				String discountType, String discountAmount,
				String orderComment,double trasportationCharge);
		public ProformaOrder fetchProformaOrder(String proformaOrderId);
		public List<ProformaOrderProductDetails> fetchProformaOrderProductDetails(String proformaOrderId);
		public ProformaOrder fetchProformaOrderById(long id);
		public List<ProformaOrderProductDetails> fetchProformaOrderProductDetailsById(long id);
		public BillPrintDataModel fetchProformaOrderBillPrintData(String proformaOrderId);
		
		public List<ProformaOrder> fetchProformaOrderByRange(String businessNameId, String range, String startDate,String endDate); 
		public List<ProformaOrderReport> fetchProformaOrderReport(String range, String startDate, String endDate);
		public String updateProformaOrder(long proformaId,String productDetailsList, String businessNameId, long transportationId,
				String vehicalNumber, String docketNumber, String transactionRefId, String comment, long paymentMethodId,
				String discountType, String discountAmount,
				String orderComment,double trasportationCharge);
		public void deleteProformaOrderById(long id);
}
