package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.models.ReturnOrderReportModel;
import com.bluesquare.rc.rest.models.GkReturnReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;

public interface ReturnOrderService {
	
	public void update (ReturnOrderProduct returnOrderProduct);
	
	public void save(ReturnOrderRequest returnOrderRequest);
	
	public List<ReturnOrderProduct> fetchReturnOrderDetailsListByDateRange(long employeeId,String fromDate,String toDate,String range);
	public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsByReturnOrderProductId(String returnOrderProductId);
	//public List<ReturnOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(List<ReturnOrderProductDetails> returnOrderProductDetails);
	// Sachin
	   public List<ReturnOrderProduct> fetchReturnOrderProductListForDBReportByEmpIdDateRange(long employeeId,
	            String fromDate, String toDate, String range);
	   public ReturnOrderProduct fetchReturnOrderForGKReportByReturnOrderProductId(String returnOrderProductId);
	   public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(String returnOrderProductId);
	   public GkReturnReportResponse fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(long employeeId,
				long areaId, String fromDate, String toDate, String range);
	   public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilter(long employeeId,String range,String fromDate,String toDate,long areaId);
	   public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilterWeb(long employeeId,String range,String fromDate,String toDate);
	   public ReturnOrderProduct fetchReturnOrderProductByReIssueStatus(String orderId);
	   public ChartDetailsResponse fetchTopReturnOrderProducts(String range,String startDate,String endDate);
	   
	// Replacement Report
	   public List<ReIssueOrderDetails> fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(long employeeId,
				 String fromDate, String toDate, String range);
	   public ReIssueOrderDetails fetchReIssueOrderDetailsForReplacementReportByOrderId(long reIssueOrderId);
	  // public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByOrderId(String orderId);
	   public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(
				long reIssueOrderId);
	   public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange(long employeeId,
				 String fromDate, String toDate, String range);
	   public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(long employeeId, String fromDate, String toDate, String range);
	   public List<ReIssueOrderDetails> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(
				long employeeId, String fromDate, String toDate, String range);
	   /*public List<ReIssueOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(
				List<ReIssueOrderProductDetails> returnOrderProductDetails);*/
	   public List<ReturnOrderReportModel> fetchReturnOrderProductList(String filter,String startDate,String endDate);
	   //public List<ReIssueOrderReport> fetchReIssueOrderReportList();
}
