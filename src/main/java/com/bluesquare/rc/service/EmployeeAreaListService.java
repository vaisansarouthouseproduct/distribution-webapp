package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeAreaList;

public interface EmployeeAreaListService {
	//webapp
	
			public void saveEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId);
			
			public void updateEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId);
			
			public void deleteForWebApp(long employeeAreaListId);
			
			
			
			public List<EmployeeAreaList> fetchEmployeeAreaListByEmployeeDetailsId(long employeeDetailsId);
			public String getAreaIdList(List<EmployeeAreaList> employeeAreaList);
}
