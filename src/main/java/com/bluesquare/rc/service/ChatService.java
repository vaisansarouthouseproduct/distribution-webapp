package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.EmployeeChatStatus;

public interface ChatService {
	public List<Chat> fetchChatRecords(long employeeId,long firstChatId,int count,long lastChatId);
	public void setStatusByEmployeeId(long employeeId,String status);
	public EmployeeChatStatus getStatusByEmployeeId(long employeeId);
	public Chat saveChat(Chat chat);
	public void deleteChat(long chatId);
}
