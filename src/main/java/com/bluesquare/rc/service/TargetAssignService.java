package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.entities.TargetType;
import com.bluesquare.rc.models.TargetLists;
import com.bluesquare.rc.models.TargetsAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.CommissionAssignAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.TargetAssignDetails;
import com.bluesquare.rc.rest.models.TargetMainResponse;
import com.bluesquare.rc.rest.models.TargetRequestModel;

public interface TargetAssignService {

	/* Priod Wise */
	public void saveTargetAssign(TargetAssign targetAssign);
	
	public void updateTargetAssign(TargetAssign targetAssign);

	public void saveTargetAssignedTargets(TargetAssignTargets targetAssignTargets);

	public TargetAssign fetchTargetAssignByTargetAssignId(long targetAssignId);

	public void deleteTargetAssignedTargets(long targetAssignId);

	public List<TargetAssignTargets> fetchTargetAssignTargetsByTargetAssignId(long targetAssignId);
	
	public List<TargetAssignDetails> fetchTargetListByTargetPeriod(long departmentId);

	public List<TargetType> fetchTargetTypesByDepartmentId(long departmentId);
	
	public void deleteTargetAssigned(long targetAssignId);
	
	//public TargetMainResponse fetchTargetAssignByTargetPeriods(TargetRequestModel requestModel);
	
	public void createNextDayTarget(String targetPeriod);
	
	public String getTargetPeriodByEmployee(long employeeId);
	
	public TargetsAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId);
	
	/* Regular Wise */


	
	//public String checkEmployeeAlreadyHaveTargetOrNot(long employeeId);
	
	//public void deleteTargetAssignRegular(long targetAssignId);
	
	//public List<TargetAssignTargets> fetchCommissionAssignListByEmpId(long employeeId);
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabsList(long commissionId);
	
	public List<Long> commissionAssignedEmployeeIds();
}
