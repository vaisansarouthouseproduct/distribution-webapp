package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.RoutePoints;

public interface RouteService {
	public void saveRoute(Route route);

	public void updateRoute(Route route);

	public void saveRoutePointsList(List<RoutePoints> routeList);

	public void deleteRoutePointsList(long routeId);

	public List<Route> fetchRouteList();

	public List<RoutePoints> fetchRoutePointsListByRouteId(long routeId);

	public Route fetchRouteByRouteId(long routeId);

	public RoutePoints fetchRoutePointsByRoutePointsId(long routePointsId);
	
	public Route fetchRouteByRouteName(String routeName);
	
	public Route checkRouteNameDuplicate(String routeName,long routeId) ;
	
	public List<Route> fetchRouteListByRouteIdList(long[] routeIds);
}
