package com.bluesquare.rc.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.entities.EmployeeRoutes;
import com.bluesquare.rc.entities.EmployeeSalary;
import com.bluesquare.rc.entities.Route;

public interface EmployeeService {

	// webApp

	public void saveForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList);
	public void updateForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList);
	
	public void updateForWebApp(Employee employee);
	
	public List<Branch> fetchBranchListByEmployeeId(long employeeId);
	public List<Route> fetchRouteListByEmployeeId(long employeeId);
	public Employee loginCredentialsForRC(String userid, String password);

	public void logout(long employeeId);

	public Employee validate(String username, String password);

	public String checkAppVersion(String appVersion);

	public String checkEmployeeDuplication(String checkText, String type, long employeeDetailsId);
	
	 public void setNotificationCountInSession(HttpSession session);
	
}
