package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Bank;
import com.bluesquare.rc.entities.BankAccount;
import com.bluesquare.rc.entities.BankCBTemplateDesign;
import com.bluesquare.rc.entities.BankChequeBook;
import com.bluesquare.rc.entities.BankChequeEntry;
import com.bluesquare.rc.entities.BankPayeeDetails;

public interface BankingService {
	
	public BankAccount fetchBankAccountByLongId(long bankAccountId);
	
	public void saveBank(Bank bank);
	public void updateBank(Bank bank);
	public List<Bank> fetchBankList();
	public Bank fetchBankByBankId(long bankId);
	
	
	public void savePayeeDetails(BankPayeeDetails bankPayeeDetails);
	public void updatePayeeDetails(BankPayeeDetails bankPayeeDetails);
	public List<BankPayeeDetails> fetchPayeeList();
	public BankPayeeDetails fetchPayeeById(long payeeId);

	// cheque book
	public void saveChequeBook(BankChequeBook bankChequeBook);
	public void updateChequeBook(BankChequeBook bankChequeBook);
	public List<BankChequeBook> fetchChequeBookList();	
	public BankChequeBook fetchBankChequeBookById(long id);
	
	// cheque Book Design Template
	public void saveChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign);
	public void updateChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign);
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignList();
	public BankCBTemplateDesign fetchChequeTemplateDesignById(long id);
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignByBankId(long bankId);
	
	//Cheque Print Management
	public List<String> fetchUnusedChequeNoListByCBId(long chequeBookId);
	public List<BankChequeBook> fetchBankChequeBookListByAcoountId(long accountId);
	
	//Manual cheque Print management
	public void saveManualPrintCheque(BankChequeEntry bankChequeEntry);
	public void updateBankChequeEntry(BankChequeEntry bankChequeEntry);
	
	public BankChequeEntry fetchChequeEntryByChequeNo(String chqNo);
	
	public List<BankChequeEntry> fetchCancelChequeEntryListByCBId(long chequeBookId);
	public List<BankChequeEntry> fetchPrintedChequeEntryListByCBId(long chequeBookId);
	
	
}
