package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.models.BranchModel;

public interface BranchService {

	public List<BranchModel> fetchBranchListByCompanyId(long companyId);
	public List<Branch> fetchBranchListByBrachIdList(long[] branchIds);
	public Branch fetchBranchByBranchId(long branchId);
	public List<BranchModel> fetchBranchByEmployeeId(long employeeId);
}