package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.RouteDAO;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.RoutePoints;
import com.bluesquare.rc.service.RouteService;

@Component
@Transactional
@Service("routeService")
@Qualifier("routeService")
public class RouteServiceImpl implements RouteService{

	@Autowired
	RouteDAO routeDAO;
	
	@Override
	public void saveRoute(Route route) {
		// TODO Auto-generated method stub
		routeDAO.saveRoute(route);
	}

	@Override
	public void updateRoute(Route route) {
		// TODO Auto-generated method stub
		routeDAO.updateRoute(route);
	}

	@Override
	public void saveRoutePointsList(List<RoutePoints> routeList) {
		// TODO Auto-generated method stub
		routeDAO.saveRoutePointsList(routeList);
	}

	@Override
	public void deleteRoutePointsList(long routeId) {
		// TODO Auto-generated method stub
		routeDAO.deleteRoutePointsList(routeId);
	}

	@Override
	public List<Route> fetchRouteList() {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteList();
	}

	@Override
	public List<RoutePoints> fetchRoutePointsListByRouteId(long routeId) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRoutePointsListByRouteId(routeId);
	}

	@Override
	public Route fetchRouteByRouteId(long routeId) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteByRouteId(routeId);
	}

	@Override
	public RoutePoints fetchRoutePointsByRoutePointsId(long routePointsId) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRoutePointsByRoutePointsId(routePointsId);
	}

	@Override
	public Route fetchRouteByRouteName(String routeName) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteByRouteName(routeName);
	}

	@Override
	public List<Route> fetchRouteListByRouteIdList(long[] routeIds) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteListByRouteIdList(routeIds);
	}

	@Override
	public Route checkRouteNameDuplicate(String routeName, long routeId) {
		// TODO Auto-generated method stub
		return routeDAO.checkRouteNameDuplicate(routeName, routeId);
	}

}
