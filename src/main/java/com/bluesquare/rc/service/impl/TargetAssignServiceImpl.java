package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.TargetAssignDAO;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.entities.TargetType;
import com.bluesquare.rc.models.TargetAssignDetails;
import com.bluesquare.rc.models.TargetsAndEmployeeByDeptResponse;
import com.bluesquare.rc.service.TargetAssignService;
@Component
@Transactional
@Service("targetAssignService")
@Qualifier("targetAssignService")
public class TargetAssignServiceImpl implements TargetAssignService {

	@Autowired
	TargetAssignDAO targetAssignDAO;
	
	@Override
	public void saveTargetAssign(TargetAssign targetAssign) {
		// TODO Auto-generated method stub
		targetAssignDAO.saveTargetAssign(targetAssign);
	}

	@Override
	public void updateTargetAssign(TargetAssign targetAssign) {
		// TODO Auto-generated method stub
		targetAssignDAO.updateTargetAssign(targetAssign);
	}

	@Override
	public void saveTargetAssignedTargets(TargetAssignTargets targetAssignTargets) {
		// TODO Auto-generated method stub
		targetAssignDAO.saveTargetAssignedTargets(targetAssignTargets);
	}

	@Override
	public TargetAssign fetchTargetAssignByTargetAssignId(long targetAssignId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetAssignByTargetAssignId(targetAssignId);
	}

	@Override
	public void deleteTargetAssignedTargets(long targetAssignId) {
		// TODO Auto-generated method stub
		targetAssignDAO.deleteTargetAssignedTargets(targetAssignId);
	}

	@Override
	public List<TargetAssignTargets> fetchTargetAssignTargetsByTargetAssignId(long targetAssignId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetAssignTargetsByTargetAssignId(targetAssignId);
	}

	

	/*@Override
	public String checkEmployeeAlreadyHaveTargetOrNot(long employeeId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.checkEmployeeAlreadyHaveTargetOrNot(employeeId);
	}*/

	@Override
	public List<TargetAssignDetails> fetchTargetListByTargetPeriod(long departmentId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetListByTargetPeriod(departmentId);
	}

	@Override
	public List<TargetType> fetchTargetTypesByDepartmentId(long departmentId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetTypesByDepartmentId(departmentId);
	}

	@Override
	public void deleteTargetAssigned(long targetAssignId) {
		// TODO Auto-generated method stub
		targetAssignDAO.deleteTargetAssigned(targetAssignId);
	}

	/*@Override
	public void deleteTargetAssignRegular(long targetAssignId) {
		// TODO Auto-generated method stub
		targetAssignDAO.deleteTargetAssignRegular(targetAssignId);
	}

	

	@Override
	public TargetMainResponse fetchTargetAssignByTargetPeriods(TargetRequestModel requestModel) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetAssignByTargetPeriods(requestModel);
	}*/

	@Override
	public void createNextDayTarget(String targetPeriod) {
		// TODO Auto-generated method stub
		targetAssignDAO.createNextDayTarget(targetPeriod);
	}

	@Override
	public String getTargetPeriodByEmployee(long employeeId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.getTargetPeriodByEmployee(employeeId);
	}

	@Override
	public TargetsAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchTargetsAndEmployeeListByDepartmentId(departmentId);
	}

	/*@Override
	public List<TargetAssignTargets> fetchCommissionAssignListByEmpId(long employeeId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchCommissionAssignListByEmpId(employeeId);
	}*/

	@Override
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabsList(long commissionId) {
		// TODO Auto-generated method stub
		return targetAssignDAO.fetchCommissionAssignTargetSlabsList(commissionId);
	}

	@Override
	public List<Long> commissionAssignedEmployeeIds() {
		// TODO Auto-generated method stub
		return targetAssignDAO.commissionAssignedEmployeeIds();
	}

}
