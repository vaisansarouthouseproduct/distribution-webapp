package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CityDAO;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.service.CityService;

@Component
@Transactional
@Service("cityService")
@Qualifier("cityService")
public class CityServiceimpl implements CityService {

	@Autowired
	CityDAO cityDAO;
	
	@Override
	public List<City> fetchAllCityForWebApp() {
		// TODO Auto-generated method stub
		return cityDAO.fetchAllCityForWebApp();
	}

	@Override
	public void saveForWebApp(City city) {
		cityDAO.saveForWebApp(city);

	}

	@Override
	public void updateForWebApp(City city) {
		cityDAO.updateForWebApp(city);

	}

	@Override
	public City fetchCityForWebApp(long cityId) {
		// TODO Auto-generated method stub
		return cityDAO.fetchCityForWebApp(cityId);
	}

	@Override
	public List<City> fetchCityByStateIdForWebApp(long stateId) {
		// TODO Auto-generated method stub
		return cityDAO.fetchCityByStateIdForWebApp(stateId);
	}

}
