package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeBasicSalaryStatus;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.entities.EmployeeLocation;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.entities.EmployeeSalary;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.models.EmployeeHolidayModel;
import com.bluesquare.rc.models.EmployeeHrmDetailsResponse;
import com.bluesquare.rc.models.EmployeeLastLocation;
import com.bluesquare.rc.models.EmployeePaymentModel;
import com.bluesquare.rc.models.EmployeeSalaryStatus;
import com.bluesquare.rc.models.EmployeeViewModel;
import com.bluesquare.rc.rest.models.EmployeeHolidayDetailsResponse;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.EmployeePaymentDetailsResponse;
import com.bluesquare.rc.rest.models.SalesManReport;
import com.bluesquare.rc.service.EmployeeDetailsService;

@Component
@Transactional
@Service("employeeDetailsService")
@Qualifier("employeeDetailsService")
public class EmployeeDetailsServiceimpl implements EmployeeDetailsService {

	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	
	@Transactional
	public void saveForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveForWebApp(employeeDetails);
	}

	@Transactional
	public void updateForWebApp(EmployeeDetails employeeDetails,boolean updateValid) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateForWebApp(employeeDetails,updateValid);
	}

	

	@Transactional
	public List<EmployeeDetails> employeeDetailsListForWebApp() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.employeeDetailsListForWebApp();
	}

	@Transactional
	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsForWebApp(employeeDetailsId);
	}

	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetailsForView() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsForView();
	}

	@Transactional
	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeSalaryStatusForWebApp(employeeDetailsId);
	}

	@Transactional
	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,
			String range, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.tofilterRangeEmployeeSalaryStatusForWebApp(startDate, endDate, range, employeeDetailsId);
	}

	@Transactional
	public EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(long employeeDetailsId, String filter,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeHolidayModelForWebApp(employeeDetailsId, filter, startDate, endDate);
	}

	@Transactional
	public EmployeeAreaDetails fetchEmployeeAreaDetails(long employeeDetailsId,long companyId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeAreaDetails(employeeDetailsId,companyId);
	}

	@Transactional
	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.bookHolidayForWebApp(employeeHolidays);
	}

	@Transactional
	public String checkHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkHolidayGivenOrNot(startDate, endDate, employeeDetailsId);
	}

	@Transactional
	public void giveIncentives(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.giveIncentives(employeeIncentives);
	}
	
	@Transactional
	public EmployeeIncentives fetchIncentives(long employeeIncentiveId){
		return employeeDetailsDAO.fetchIncentives(employeeIncentiveId);
	}

	@Transactional
	public void givePayment(EmployeeSalary employeeSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.givePayment(employeeSalary);
	}

	@Transactional
	public EmployeePaymentModel openPaymentModel(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.openPaymentModel(employeeDetailsId);
	}

	@Transactional
	public String sendSMSTOEmployee(String employeeDetailsIdList,String smsText,String mobileNumber) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.sendSMSTOEmployee(employeeDetailsIdList, smsText, mobileNumber);
	}

	@Transactional
	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.getEmployeeDetailsByemployeeId(employeeId);
	}

	@Transactional
	public List<Area> fetchAreaByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
	}

	@Transactional
	public List<EmployeeDetails> fetchDBEmployeeDetailByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchDBEmployeeDetailByAreaId(areaId);
	}

	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetail(long employeeDetailId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetail(employeeDetailId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchDeliveryBoyListByBusinessNameAreaId(long)
	 * Oct 14, 20176:04:35 PM
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchDeliveryBoyListByBusinessNameAreaId(long businessNameAreaId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchDeliveryBoyListByBusinessNameAreaId(businessNameAreaId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchSalesManReport()
	 * Oct 23, 201711:14:06 AM
	 */
	@Transactional
	public SalesManReport fetchSalesManReport(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSalesManReport(range,startDate,endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchSMandDBByGateKeeperId(long)
	 * Oct 27, 20177:19:59 PM
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchSMandDBByGateKeeperId(long gateKeeperId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSMandDBByGateKeeperId(gateKeeperId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchSMEmployeeDetailByAreaId(long)
	 * Nov 1, 20178:52:34 PM
	 */
	@Transactional
	public List<EmployeeDetails> fetchSMEmployeeDetailByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSMEmployeeDetailByAreaId(areaId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchDBByGateKeeperId(long)
	 * Nov 7, 201710:17:54 PM
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchDBByGateKeeperId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchDBByGateKeeperId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(java.lang.String)
	 * Nov 11, 20177:57:33 PM
	 */
	@Transactional
	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#saveEmployeeOldBasicSalary(com.bluesquare.rc.entities.EmployeeOldBasicSalary)
	 * Nov 11, 20177:57:33 PM
	 */
	@Transactional
	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveEmployeeOldBasicSalary(employeeOldBasicSalary);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#updateEmployeeOldBasicSalary(com.bluesquare.rc.entities.EmployeeOldBasicSalary)
	 * Nov 11, 20178:04:40 PM
	 */
	@Transactional
	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateEmployeeOldBasicSalary(employeeOldBasicSalary);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchEmployeeIncentiveListByFilter(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * Nov 15, 201711:25:12 AM
	 */
	@Transactional
	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId, String filter,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeIncentiveListByFilter(employeeDetailsId, filter, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#updateIncentive(com.bluesquare.rc.entities.EmployeeIncentives)
	 * Nov 15, 201712:07:06 PM
	 */
	@Transactional
	public String updateIncentive(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.updateIncentive(employeeIncentives);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#updateEmployeeHoliday(com.bluesquare.rc.entities.EmployeeHolidays)
	 * Nov 15, 20172:06:08 PM
	 */
	@Transactional
	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.updateEmployeeHoliday(employeeHolidays);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchEmployeeHolidayByEmployeeHolidayId(long)
	 * Nov 15, 20172:06:08 PM
	 */
	@Transactional
	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeHolidayByEmployeeHolidayId(employeeHolidayId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#checkUpdatingHolidayGivenOrNot(java.lang.String, java.lang.String, java.lang.String, long)
	 * Nov 15, 20174:57:21 PM
	 */
	@Transactional
	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId,
			long employeeHolidayId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkUpdatingHolidayGivenOrNot(startDate, endDate, employeeDetailsId, employeeHolidayId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#fetchEmployeeIncentivesByEmployeeIncentivesId(long)
	 * Nov 17, 201711:57:44 AM
	 */
	@Transactional
	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeIncentivesByEmployeeIncentivesId(employeeIncentivesId);
	}

	

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeDetailsService#saveEmployeeLocation(com.bluesquare.rc.entities.EmployeeLocation)
	 * Dec 20, 201712:22:58 PM
	 */
	@Transactional
	public void saveEmployeeLocation(EmployeeLocation employeeLocation) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveEmployeeLocation(employeeLocation);
	}

	@Transactional
	public List<EmployeeLastLocation> fetchEmployeeLastLocationByDepartmentId(long deparmentId,String date) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeLastLocationByDepartmentId(deparmentId,date);
	}

	@Transactional
	public EmployeeLocation fetchEmployeeLastLocationByEmployeeDetailsId(long employeeDetailsId,String date) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeLastLocationByEmployeeDetailsId(employeeDetailsId,date);
	}

	@Transactional
	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsByDepartmentId(deparmentId);
	}

	@Transactional
	public List<EmployeeLastLocation> fetchEmployeeLocationByEmployeeDetailsId(long employeeDetailsId,String date) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeLocationByEmployeeDetailsId(employeeDetailsId,date);
	}

	@Transactional
	public List<EmployeeNameAndId> fetchEmployeeListForChat() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeListForChat();
	}

	@Override
	public EmployeeSalary fetchEmployeeSalary(long employeeSalaryId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeSalary(employeeSalaryId);
	}

	@Override
	public void updatePayment(EmployeeSalary employeeSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updatePayment(employeeSalary);
	}

	@Override
	public double totalEmployeeeSalaryAmountForProfitAndLoss(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.totalEmployeeeSalaryAmountForProfitAndLoss(startDate, endDate);
	}

	@Override
	public List<EmployeeViewModel> fetchEmployeeDetailsForGkEmployeeView() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsForGkEmployeeView();
	}

	@Override
	public void clearToken(String token,long employeeId) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.clearToken(token, employeeId);
		
	}

	@Override
	public EmployeeHrmDetailsResponse fetchEmployeeLeaveAndBalanceAmountForAPP(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeLeaveAndBalanceAmountForAPP(employeeId);
	}

	@Override
	public EmployeePaymentDetailsResponse fetchEmployeeSalaryDetails(String fromDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeSalaryDetails(fromDate, endDate);
	}

	@Override
	public EmployeeHolidayDetailsResponse fetchEmployeeHolidayDetailsForApp(String filter, String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeHolidayDetailsForApp(filter, startDate, endDate);
	}

	@Override
	public EmployeeProfilePicture fetchEmployeeImage() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeImage();
	}

	@Override
	public void saveEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveEmployeeProfilePicture(employeeProfilePicture);
	}

	@Override
	public void updateEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateEmployeeProfilePicture(employeeProfilePicture);
	}

	@Override
	public List<Area> fetchAreaListByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchAreaListByCompanyId(companyId);
	}

	@Override
	public Employee getAdminEmployee() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.getAdminEmployee();
	}

	@Override
	public List<Object[]> fetchEmployeeLocationIdsDetails(long employeeDetailsId, long companyId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeLocationIdsDetails(employeeDetailsId, companyId);
	}

	

}
