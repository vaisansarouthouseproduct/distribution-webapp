package com.bluesquare.rc.service.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.entities.EmployeeRoutes;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.service.EmployeeService;

@Component
@Transactional
@Service("employeeService")
@Qualifier("employeeService")
public class EmployeeServiceimpl implements EmployeeService{

	@Autowired 
	EmployeeDAO employeeDAO;
	

	

	
	@Override
	public Employee loginCredentialsForRC(String userid, String password) {
		// TODO Auto-generated method stub
		return employeeDAO.loginCredentialsForRC(userid, password);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeService#validate(java.lang.String, java.lang.String)
	 * Nov 6, 20176:44:42 PM
	 */
	@Override
	public Employee validate(String username, String password) {
		// TODO Auto-generated method stub
		return employeeDAO.validate(username, password);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.EmployeeService#logout(long)
	 * Nov 7, 201710:45:04 AM
	 */
	@Override
	public void logout(long employeeId) {
		// TODO Auto-generated method stub
		employeeDAO.logout(employeeId);
	}

	@Override
	public String checkAppVersion(String appVersion) {
		// TODO Auto-generated method stub
		return employeeDAO.checkAppVersion(appVersion);
	}

	@Override
	public String checkEmployeeDuplication(String checkText, String type, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDAO.checkEmployeeDuplication(checkText, type, employeeDetailsId);
	}

	@Override
	public List<Branch> fetchBranchListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDAO.fetchBranchListByEmployeeId(employeeId);
	}

	@Override
	public void updateForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		employeeDAO.updateForWebApp(employee);
	}

	@Override
	public void saveForWebApp(Employee employee, List<EmployeeBranches> employeeBranchesList,
			List<EmployeeRoutes> employeeRoutesList) {
		// TODO Auto-generated method stub
		employeeDAO.saveForWebApp(employee, employeeBranchesList, employeeRoutesList);
	}

	@Override
	public void updateForWebApp(Employee employee, List<EmployeeBranches> employeeBranchesList,
			List<EmployeeRoutes> employeeRoutesList) {
		// TODO Auto-generated method stub
		employeeDAO.updateForWebApp(employee, employeeBranchesList, employeeRoutesList);
	}

	@Override
	public List<Route> fetchRouteListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDAO.fetchRouteListByEmployeeId(employeeId);
	}

	@Override
	public void setNotificationCountInSession(HttpSession session) {
		// TODO Auto-generated method stub
		employeeDAO.setNotificationCountInSession(session);
	}


	

	
}
