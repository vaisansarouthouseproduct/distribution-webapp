package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ShopVisitDAO;
import com.bluesquare.rc.entities.ShopVisit;
import com.bluesquare.rc.entities.ShopVisitImages;
import com.bluesquare.rc.service.ShopVisitService;



@Component
@Transactional
@Service("shopVisitService")
@Qualifier("shopVisitService")
public class ShopVisitServiceImpl implements ShopVisitService {

	
	@Autowired
	ShopVisitDAO shopVisitDAO;
	
	@Override
	public void saveShopVisit(ShopVisit shopVisted) {
		// TODO Auto-generated method stub
		shopVisitDAO.saveShopVisit(shopVisted);
	}

	@Override
	public List<ShopVisit> fetchShopVisitedByDateRange(long employeeId, String range, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return shopVisitDAO.fetchShopVisitedByDateRange(employeeId, range, fromDate, toDate);
	}

	@Override
	public ShopVisit fetchShopVisitById(long visitId) {
		// TODO Auto-generated method stub
		return shopVisitDAO.fetchShopVisitById(visitId);
	}

	@Override
	public void updateShopVisit(ShopVisit shopVisit) {
		// TODO Auto-generated method stub
		shopVisitDAO.updateShopVisit(shopVisit);
	}

	@Override
	public ShopVisitImages fetchShopVisitImage(long shopVisitId) {
		// TODO Auto-generated method stub
		return shopVisitDAO.fetchShopVisitImage(shopVisitId);
	}

	@Override
	public void updateShopVisitImage(String shopImage, ShopVisit shopVisit) {
		// TODO Auto-generated method stub
		shopVisitDAO.updateShopVisitImage(shopImage, shopVisit);
	}

	@Override
	public void saveShopVisitImage(String shopImage, ShopVisit shopVisit) {
		// TODO Auto-generated method stub
		shopVisitDAO.saveShopVisitImage(shopImage, shopVisit);
	}

}
