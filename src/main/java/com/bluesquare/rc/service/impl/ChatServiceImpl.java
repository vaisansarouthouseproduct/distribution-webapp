package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ChatDAO;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.EmployeeChatStatus;
import com.bluesquare.rc.service.ChatService;

@Component
@Transactional
@Service("chatService")
@Qualifier("chatService")
public class ChatServiceImpl implements ChatService {

	@Autowired
	ChatDAO chatDAO;

	@Override
	public List<Chat> fetchChatRecords(long employeeId, long firstChatId, int count,long lastChatId) {
		// TODO Auto-generated method stub
		return chatDAO.fetchChatRecords(employeeId, firstChatId, count,lastChatId);
	}

	@Override
	public void setStatusByEmployeeId(long employeeId, String status) {
		// TODO Auto-generated method stub
		chatDAO.setStatusByEmployeeId(employeeId, status);
	}

	@Override
	public EmployeeChatStatus getStatusByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return chatDAO.getStatusByEmployeeId(employeeId);
	}

	@Override
	public Chat saveChat(Chat chat) {
		// TODO Auto-generated method stub
		return chatDAO.saveChat(chat);
	}

	@Override
	public void deleteChat(long chatId) {
		// TODO Auto-generated method stub
		chatDAO.deleteChat(chatId);
	}
	
	

}
