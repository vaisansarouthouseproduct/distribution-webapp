package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AssetDAO;
import com.bluesquare.rc.entities.AssetsCategory;
import com.bluesquare.rc.service.AssetService;

@Component
@Transactional
@Service("assetService")
@Qualifier("assetService")
public class AssetServiceImpl implements AssetService {
	
	@Autowired
	AssetDAO assetDAO;

	@Override
	public void saveAssetCategoryDetails(AssetsCategory assetsCategory) {
		// TODO Auto-generated method stub
		assetDAO.saveAssetCategoryDetails(assetsCategory);
	}

	@Override
	public void updateAssetCategoryDetails(AssetsCategory assetsCategory) {
		// TODO Auto-generated method stub
		assetDAO.updateAssetCategoryDetails(assetsCategory);
	}

	@Override
	public List<AssetsCategory> fetchAssetCategory() {
		// TODO Auto-generated method stub
		return assetDAO.fetchAssetCategory();
	}

	@Override
	public AssetsCategory fetchAssetsCategoryById(long assetCategoryId) {
		// TODO Auto-generated method stub
		return assetDAO.fetchAssetsCategoryById(assetCategoryId);
	}

}
