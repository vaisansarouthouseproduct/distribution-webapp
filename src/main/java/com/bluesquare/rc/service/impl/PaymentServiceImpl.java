package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.PaymentDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.models.ChequePaymentReportModel;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.CollectionReportPaymentDetails;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.models.PaymentListReport;
import com.bluesquare.rc.models.PaymentPendingList;
import com.bluesquare.rc.models.PaymentReportModel;
import com.bluesquare.rc.rest.models.CollectionReportResponse;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentTakeRequest;
import com.bluesquare.rc.service.PaymentService;

@Component
@Transactional
@Service("paymentService")
@Qualifier("paymentService")
public class PaymentServiceImpl implements PaymentService{

	@Autowired
	PaymentDAO paymentDAO;
	
	@Override
	public OrderDetails savePaymentStatus(PaymentTakeRequest paymentTakeRequest,OrderDetails orderDetails) {
		// TODO Auto-generated method stub
		return paymentDAO.savePaymentStatus(paymentTakeRequest,orderDetails);
	}

	@Override
	public List<PaymentPendingList> fetchPaymentPendingList() {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentPendingList();
	}

/*	@Override
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchLedgerPaymentView(startDate, endDate, range);
	}*/

	@Override
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return paymentDAO.getCollectionReportDetails(startDate, endDate, range);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.PaymentService#fetchCollectionDetailsByOrderId(java.lang.String)
	 * Nov 1, 201710:51:47 AM
	 */
	/*@Override
	public CollectionReportResponse fetchCollectionDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchCollectionDetailsByOrderId(orderId);
	}*/

	@Override
	public List<PaymentListReport> fetchPaymentListByOrderDetailsIdForCollectionReport(String orderDetailsId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentListByOrderDetailsIdForCollectionReport(orderDetailsId);
	}

	@Override
	public PaymentListModel fetchPaymentListByOrderIdForApp(String orderId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentListByOrderIdForApp(orderId);
	}

	@Override
	public void updatePayment(Payment payment) {
		// TODO Auto-generated method stub
		paymentDAO.updatePayment(payment);
	}

	@Override
	public List<PaymentReportModel> fetchPaymentReportModelList(String startDate,String endDate,String range) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentReportModelList(startDate,endDate,range);
	}

	@Override
	public Payment fetchPaymentBYPaymentId(long paymentId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentBYPaymentId(paymentId);
	}

	@Override
	public List<Payment> fetchPaymentList(String startDate,String endDate,String range) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentList(startDate,endDate,range);
	}

	@Override
	public void deletePayment(long paymentId) {
		// TODO Auto-generated method stub
		paymentDAO.deletePayment(paymentId);
	}

	@Override
	public List<Payment> fetchPaymentListByOrderDetailsId(String orderDetailsId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentListByOrderDetailsId(orderDetailsId);
	}

	@Override
	public void resetDueAmountOfPaymentList(String orderId) {
		// TODO Auto-generated method stub
		paymentDAO.resetDueAmountOfPaymentList(orderId);
	}

	@Override
	public List<ChequePaymentReportModel> fetchChequePaymentReportModelList(String startDate, String endDate,
			String range) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchChequePaymentReportModelList(startDate, endDate, range);
	}

	@Override
	public void defineChequeBounced(long paymentId) {
		// TODO Auto-generated method stub
		paymentDAO.defineChequeBounced(paymentId);
	}

	@Override
	public List<PaymentMethod> fetchPaymentMethodList() {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentMethodList();
	}

	@Override
	public PaymentMethod fetchPaymentMethodById(long paymentMethodId) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchPaymentMethodById(paymentMethodId);
	}

	@Override
	public List<CollectionReportPaymentDetails> getPendingPaymentList() {
		// TODO Auto-generated method stub
		return paymentDAO.getPendingPaymentList();
	}

}
