package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BankingDAO;
import com.bluesquare.rc.entities.Bank;
import com.bluesquare.rc.entities.BankAccount;
import com.bluesquare.rc.entities.BankCBTemplateDesign;
import com.bluesquare.rc.entities.BankChequeBook;
import com.bluesquare.rc.entities.BankChequeEntry;
import com.bluesquare.rc.entities.BankPayeeDetails;
import com.bluesquare.rc.service.BankingService;



@Component
@Transactional
@Service("bankingService")
@Qualifier("bankingService")
public class BankingServiceImpl implements BankingService {

	@Autowired
	BankingDAO bankingDAO;
	
	@Override
	public void saveBank(Bank bank) {
		// TODO Auto-generated method stub
		bankingDAO.saveBank(bank);
	}

	@Override
	public void updateBank(Bank bank) {
		// TODO Auto-generated method stub
		bankingDAO.updateBank(bank);
	}

	@Override
	public List<Bank> fetchBankList() {
		// TODO Auto-generated method stub
		return bankingDAO.fetchBankList();
	}

	@Override
	public Bank fetchBankByBankId(long bankId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchBankByBankId(bankId);
	}

	@Override
	public void savePayeeDetails(BankPayeeDetails bankPayeeDetails) {
		// TODO Auto-generated method stub
		bankingDAO.savePayeeDetails(bankPayeeDetails);
	}

	@Override
	public void updatePayeeDetails(BankPayeeDetails bankPayeeDetails) {
		// TODO Auto-generated method stub
		bankingDAO.updatePayeeDetails(bankPayeeDetails);
	}

	@Override
	public List<BankPayeeDetails> fetchPayeeList() {
		// TODO Auto-generated method stub
		return bankingDAO.fetchPayeeList();
	}

	@Override
	public BankPayeeDetails fetchPayeeById(long payeeId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchPayeeById(payeeId);
	}

	@Override
	public void saveChequeBook(BankChequeBook bankChequeBook) {
		// TODO Auto-generated method stub
		bankingDAO.saveChequeBook(bankChequeBook);
	}

	@Override
	public void updateChequeBook(BankChequeBook bankChequeBook) {
		// TODO Auto-generated method stub
		bankingDAO.updateChequeBook(bankChequeBook);
	}

	@Override
	public List<BankChequeBook> fetchChequeBookList() {
		// TODO Auto-generated method stub
		return bankingDAO.fetchChequeBookList();
	}

	@Override
	public BankAccount fetchBankAccountByLongId(long bankAccountId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchBankAccountByLongId(bankAccountId);
	}

	@Override
	public BankChequeBook fetchBankChequeBookById(long id) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchBankChequeBookById(id);
	}

	@Override
	public void saveChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign) {
		// TODO Auto-generated method stub
		bankingDAO.saveChequeTemplateDesign(bankCBTemplateDesign);
	}

	@Override
	public void updateChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign) {
		// TODO Auto-generated method stub
		bankingDAO.updateChequeTemplateDesign(bankCBTemplateDesign);
	}

	@Override
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignList() {
		// TODO Auto-generated method stub
		return bankingDAO.fetchChequeTemplateDesignList();
	}

	@Override
	public BankCBTemplateDesign fetchChequeTemplateDesignById(long id) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchChequeTemplateDesignById(id);
	}

	@Override
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignByBankId(long bankId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchChequeTemplateDesignByBankId(bankId);
	}

	@Override
	public List<String> fetchUnusedChequeNoListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchUnusedChequeNoListByCBId(chequeBookId);
	}

	@Override
	public List<BankChequeBook> fetchBankChequeBookListByAcoountId(long accountId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchBankChequeBookListByAcoountId(accountId);
	}

	@Override
	public void saveManualPrintCheque(BankChequeEntry bankChequeEntry) {
		// TODO Auto-generated method stub
		bankingDAO.saveManualPrintCheque(bankChequeEntry);
	}

	@Override
	public BankChequeEntry fetchChequeEntryByChequeNo(String chqNo) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchChequeEntryByChequeNo(chqNo);
	}


	@Override
	public void updateBankChequeEntry(BankChequeEntry bankChequeEntry) {
		// TODO Auto-generated method stub
		bankingDAO.updateBankChequeEntry(bankChequeEntry);
	}

	@Override
	public List<BankChequeEntry> fetchCancelChequeEntryListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchCancelChequeEntryListByCBId(chequeBookId);
	}

	@Override
	public List<BankChequeEntry> fetchPrintedChequeEntryListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		return bankingDAO.fetchPrintedChequeEntryListByCBId(chequeBookId);
	}


}
