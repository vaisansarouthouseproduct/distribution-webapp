package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.StateDAO;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.service.StateService;


@Component
@Transactional
@Service("stateService")
@Qualifier("stateService")
public class StateServiceimpl implements StateService {

	@Autowired
	StateDAO stateDAO;
	
	@Override
	public List<State> fetchAllStateForWebApp() {
		// TODO Auto-generated method stub
		return stateDAO.fetchAllStateForWebApp();
	}

	@Override
	public void saveForWebApp(State state) {
		stateDAO.saveForWebApp(state);

	}

	@Override
	public void updateForWebApp(State state) {
		stateDAO.updateForWebApp(state);

	}

	@Override
	public State fetchState(long stateId) {
		// TODO Auto-generated method stub
		return stateDAO.fetchState(stateId);
	}

	@Override
	public List<State> fetchStateByCountryIdforwebapp(long countryId) {
		// TODO Auto-generated method stub
		return stateDAO.fetchStateByCountryIdForWebapp(countryId);
	}

	@Override
	public State fetchStateByStateName(String stateName) {
		// TODO Auto-generated method stub
		return stateDAO.fetchStateByStateName(stateName);
	}

}
