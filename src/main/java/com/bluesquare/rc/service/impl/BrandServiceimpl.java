package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BrandDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.service.BrandService;

@Component
@Transactional
@Service("brandService")
@Qualifier("brandService")
public class BrandServiceimpl implements BrandService {

	@Autowired
	BrandDAO brandDAO;
	
	@Override
	public void saveBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		brandDAO.saveBrandForWebApp(brand);
	}

	@Override
	public void updateBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		brandDAO.updateBrandForWebApp(brand);
	}

	@Override
	public Brand fetchBrandForWebApp(long brandId) {
		// TODO Auto-generated method stub
		return brandDAO.fetchBrandForWebApp(brandId);
	}
 
	@Override
	public List<Brand> fetchBrandListForWebApp() {
		// TODO Auto-generated method stub
		return brandDAO.fetchBrandListForWebApp();
	}

	@Override
	public List<BrandModel> fetchBrandModelListForWebApp() {
		// TODO Auto-generated method stub
		return brandDAO.fetchBrandModelListForWebApp();
	}

	@Override
	public BrandModel fetchBrandModelForWebApp(long brandId) {
		// TODO Auto-generated method stub
		return brandDAO.fetchBrandModelForWebApp(brandId);
	}

}
