package com.bluesquare.rc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.SnapShotDAO;
import com.bluesquare.rc.rest.models.DBSnapshotResponse;
import com.bluesquare.rc.rest.models.SnapShotReportResponse;
import com.bluesquare.rc.service.SnapShotService;


@Component
@Transactional
@Service("snapShotService")
@Qualifier("snapShotService")
public class SnapShotServiceImpl implements SnapShotService {
	
	
	@Autowired
	SnapShotDAO snapShotDAO;
	

	@Override
	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return snapShotDAO.fetchRecordForSnapShot(employeeId, fromDate, toDate, range);
	}


	@Override
	public DBSnapshotResponse fetchDBSnapshot(long employeeId, String pickDate) {
		// TODO Auto-generated method stub
		return snapShotDAO.fetchDBSnapshot(employeeId, pickDate);
	}

}
