package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.InventoryDAO;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.InventoryAddedInvoiceModel;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.models.InventoryRequest;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.models.TaxCalculationManageInventory;
import com.bluesquare.rc.rest.models.InventorySaveRequestModel;
import com.bluesquare.rc.service.InventoryService;

@Component
@Transactional
@Service("inventoryService")
@Qualifier("inventoryService")
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	InventoryDAO inventoryDAO;

	@Override
	public List<InventoryProduct> inventoryProductList() {
		// TODO Auto-generated method stub
		return inventoryDAO.inventoryProductList();
	}

	@Override
	public List<SupplierProductList> fetchSupplierListByProductId(long productId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchSupplierListByProductId(productId);
	}

	@Override
	public void addInventory(InventoryRequest inventoryRequest) {
		// TODO Auto-generated method stub
		inventoryDAO.addInventory(inventoryRequest);
	}

	@Override
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchPaymentStatus(inventoryTransactionId);
	}

	@Override
	public Inventory fetchInventory(String inventoryId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventory(inventoryId);
	}

	@Override
	public void givePayment(PaymentPaySupplier paymentPaySupplier) {
		// TODO Auto-generated method stub
		inventoryDAO.givePayment(paymentPaySupplier);
	}

	@Override
	public List<InventoryReportView> fetchInventoryReportView(String supplierId, String startDate, String endDate,
			String range) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryReportView(supplierId, startDate, endDate, range);
	}

	@Override
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchTrasactionDetailsByInventoryId(inventoryTransactionId);
	}

	/*
	 * @Override public List<InventoryProduct>
	 * makeInventoryProductViewProductNull(List<InventoryProduct>
	 * inventoryProductsList) { // TODO Auto-generated method stub return
	 * inventoryDAO.makeInventoryProductViewProductNull(inventoryProductsList);
	 * }
	 */

	@Override
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		inventoryDAO.saveInventoryForApp(inventorySaveRequestModel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * fetchAddedInventoryforGateKeeperReportByDateRange(java.lang.String,
	 * java.lang.String, java.lang.String) Oct 14, 20176:10:49 PM
	 */
	@Override
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchAddedInventoryforGateKeeperReportByDateRange(fromDate, toDate, range);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * fetchInventoryByInventoryTransactionIdforGateKeeperReport(java.lang.
	 * String) Oct 14, 20176:12:20 PM
	 */
	@Override
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(java.
	 * lang.String) Oct 14, 20176:13:58 PM
	 */
	@Override
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * fetchTotalValueOfCurrrentInventory() Oct 31, 20177:02:01 PM
	 */
	@Override
	public double fetchTotalValueOfCurrrentInventory() {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchTotalValueOfCurrrentInventory();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * fetchProductUnderThresholdCount() Oct 31, 20177:22:56 PM
	 */
	@Override
	public long fetchProductUnderThresholdCount() {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchProductUnderThresholdCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluesquare.rc.service.InventoryService#
	 * makeProductImageNullOfInventoryDetailsList(java.util.List) Nov 18,
	 * 20177:30:48 PM
	 */
	/*
	 * @Override public List<InventoryDetails>
	 * makeProductImageNullOfInventoryDetailsList( List<InventoryDetails>
	 * inventoryDetailsList) { // TODO Auto-generated method stub return
	 * inventoryDAO.makeProductImageNullOfInventoryDetailsList(
	 * inventoryDetailsList); }
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluesquare.rc.service.InventoryService#editInventory(com.bluesquare.
	 * rc.entities.Inventory, java.lang.String) Dec 26, 20177:34:48 PM
	 */
	@Override
	public void editInventory(InventoryRequest inventoryRequest) {
		// TODO Auto-generated method stub
		inventoryDAO.editInventory(inventoryRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluesquare.rc.service.InventoryService#deleteInventory(java.lang.
	 * String) Feb 1, 20182:13:56 PM
	 */
	@Override
	public void deleteInventory(String inventoryId) {
		// TODO Auto-generated method stub
		inventoryDAO.deleteInventory(inventoryId);
	}

	@Override
	public void editInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		inventoryDAO.editInventoryForApp(inventorySaveRequestModel);
	}

	@Override
	public List<PaymentPaySupplier> fetchPaymentPaySupplierListByInventoryId(String inventoryId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchPaymentPaySupplierListByInventoryId(inventoryId);
	}

	@Override
	public void deletePaymentPaySupplierListByInventoryId(String paymentPaySupplierId) {
		// TODO Auto-generated method stub
		inventoryDAO.deletePaymentPaySupplierListByInventoryId(paymentPaySupplierId);
	}

	@Override
	public PaymentPaySupplier fetchPaymentPaySupplierListByPaymentPaySupplierId(String paymentPaySupplierId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchPaymentPaySupplierListByPaymentPaySupplierId(paymentPaySupplierId);
	}

	@Override
	public PaymentDoInfo fetchPaymentPaySupplierForEdit(String paymentPaySupplierId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchPaymentPaySupplierForEdit(paymentPaySupplierId);
	}

	@Override
	public void updateSupplierPayment(PaymentPaySupplier paymentPaySupplier) {
		// TODO Auto-generated method stub
		inventoryDAO.updateSupplierPayment(paymentPaySupplier);
	}

	@Override
	public double totalSupplierPaidAmountForProfitAndLoss(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return inventoryDAO.totalSupplierPaidAmountForProfitAndLoss(startDate, endDate);
	}

	@Override
	public InventoryAddedInvoiceModel getInvoiceDetails(String inventoryTsnId) {
		// TODO Auto-generated method stub
		return inventoryDAO.getInvoiceDetails(inventoryTsnId);
	}

}
