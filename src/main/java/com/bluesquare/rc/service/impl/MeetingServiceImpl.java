package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.MeetingDAO;
import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.models.MeetingScheduleModel;
import com.bluesquare.rc.rest.models.MeetingRequest;
import com.bluesquare.rc.service.MeetingService;

@Component
@Transactional
@Service("meetingService")
@Qualifier("meetingService")
public class MeetingServiceImpl implements MeetingService {

	@Autowired
	MeetingDAO meetingDAO;

	@Override
	public void saveMeeting(Meeting meeting) {
		// TODO Auto-generated method stub
		meetingDAO.saveMeeting(meeting);
	}

	@Override
	public void updateMeeting(Meeting meeting) {
		// TODO Auto-generated method stub
		meetingDAO.updateMeeting(meeting);
	}

	@Override
	public void deleteMeeting(long meetingId) {
		// TODO Auto-generated method stub
		meetingDAO.deleteMeeting(meetingId);
	}


	@Override
	public Meeting fetchMeetingByMeetingId(long meetingId) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchMeetingByMeetingId(meetingId);
	}

	@Override
	public List<Meeting> fetchMeetingListbyEmployeeIdAndDate(long employeeDetailsId, String pickDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchMeetingListbyEmployeeIdAndDate(employeeDetailsId, pickDate);
	}

	@Override
	public List<MeetingScheduleModel> fetchEmployeeWiseScheduledMeeting(long departmentId, String pickDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchEmployeeWiseScheduledMeeting(departmentId, pickDate);
	}

	@Override
	public String checkMeetingSchedule(long employeeId, String date, String fromTime, String toTime) {
		// TODO Auto-generated method stub
		return meetingDAO.checkMeetingSchedule(employeeId, date, fromTime, toTime);
	}

	@Override
	public String checkMeetingScheduleForUpdate(long employeeId, String date, String fromTime, String toTime,
			long meetingId) {
		// TODO Auto-generated method stub
		return meetingDAO.checkMeetingScheduleForUpdate(employeeId, date, fromTime, toTime, meetingId);
	}

	@Override
	public List<Meeting> fetchMeetingsbyEmployeeIdAndDate(long employeeId, String pickDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchMeetingsbyEmployeeIdAndDate(employeeId, pickDate);
	}

	@Override
	public void updateCancelStatus(long meetingId, String cancelReason) {
		// TODO Auto-generated method stub
		meetingDAO.updateCancelStatus(meetingId, cancelReason);
	}

	@Override
	public void meetingCompleteWithReview(MeetingRequest meetingRequest) {
		// TODO Auto-generated method stub
		meetingDAO.meetingCompleteWithReview(meetingRequest);
	}

	@Override
	public List<Meeting> fetchCompletedMeetingList(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchCompletedMeetingList(range, startDate, endDate);
	}

	@Override
	public List<Meeting> fetchCancelledMeetingList(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchCancelledMeetingList(range, startDate, endDate);
	}

	@Override
	public List<Meeting> fetchPendingMeetingList(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchPendingMeetingList(range, startDate, endDate);
	}

	@Override
	public Meeting fetchMeetingByShopVistedId(long shopVistedId) {
		// TODO Auto-generated method stub
		return meetingDAO.fetchMeetingByShopVistedId(shopVistedId);
	}

}
