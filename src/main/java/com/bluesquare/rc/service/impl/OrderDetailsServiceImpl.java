package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.CancelReason;
import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CancelOrderRequest;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.GstBillReport;
import com.bluesquare.rc.models.OrderDetailsListOfBusiness;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.models.SalesReportModel;
import com.bluesquare.rc.responseEntities.ReturnFromDeliveryBoyModel;
import com.bluesquare.rc.rest.models.BookOrderFreeProductRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.GkSnapProductResponse;
import com.bluesquare.rc.rest.models.OrderDetailByBusinessNameIdEmployeeIdRequest;
import com.bluesquare.rc.rest.models.OrderDetailsForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentList;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.rest.models.OrderRequest;
import com.bluesquare.rc.rest.models.PReturnOrderDetailsModel;
import com.bluesquare.rc.rest.models.PaymentListRequest;
import com.bluesquare.rc.rest.models.ReIssueDelivered;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.rest.models.ReturnPermanentModelRequest;
import com.bluesquare.rc.service.OrderDetailsService;

@Component
@Transactional
@Service("orderDetailsService")
@Qualifier("orderDetailsService")
public class OrderDetailsServiceImpl implements OrderDetailsService{

	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Override
	public String bookOrder(OrderRequest orderRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.bookOrder(orderRequest);
	}

	@Override
	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsByOrderId(orderDetailsId);
	}
	
	@Override
	public OrderDetails fetchOrderDetailsByOrderIdForApp(String orderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsByOrderIdForApp(orderDetailsId);
	}

	@Override
	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateOrderDetailsPaymentDays(orderDetails);
	}

	@Override
	public OrderDetailsList fetchOrderListByAreaId( long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderListByAreaId( employeeId);
	}

	@Override
	public OrderDetailsList fetchPendingOrderListByAreaId( long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchPendingOrderListByAreaId( employeeId);
	}

	@Override
	public List<OrderReportList> showOrderReport(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.showOrderReport(range,startDate,endDate);
	}


	@Override
	public List<OrderProductDetails> fetchOrderProductDetailByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductDetailByOrderId(orderId);
	}
	

	@Override
	public List<OrderProductDetails> fetchOrderProductDetailByOrderIdForApp(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductDetailByOrderIdForApp(orderId);
	}

	/*@Override
	public List<OrderProductDetails> makeProductImageMNullorderProductDetailsList(
			List<OrderProductDetails> orderProductDetailsList) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.makeProductImageMNullorderProductDetailsList(orderProductDetailsList);
	}*/

	@Override
	public List<OrderProductDetailListForWebApp> orderProductDetailsListForWebApp(String orderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.orderProductDetailsListForWebApp(orderDetailsId);
	}

	@Override
	public List<SupplierOrder> fetchSupplierOrders24hour(String filter,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSupplierOrders24hour(filter,startDate,endDate);
	}

	@Override
	public List<OrderDetailsForPayment> fetchOrderListForPayment(PaymentListRequest paymentListRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderListForPayment(paymentListRequest);
	}

	@Override
	public List<SupplierOrder> fetchSupplierOrders24hourBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSupplierOrders24hourBySupplierId(supplierId);
	}

	@Override
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSupplierOrderDetailsListBySupplierOrderId(supplierOrderId);
	}

	/*@Override
	public List<ReturnOrderResponse> makeProductsImageNullOrderDetailsWithOrderProductDetailsList(
			List<ReturnOrderResponse> returnOrderResponsesList) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.makeProductsImageNullOrderDetailsWithOrderProductDetailsList(returnOrderResponsesList);
	}*/

	@Override
	public List<OrderDetails> fetchOrderDetailBybusinessNameIdAndDateRange(String businessNameId, String fromDate,
			String toDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailBybusinessNameIdAndDateRange(businessNameId, fromDate, toDate);
	}

	@Override
	public List<CustomerReportResponse> fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(employeeId, fromDate, toDate, range);
	}

	@Override
	public List<OrderDetails> fetchOrderDetailForCustomerReportByBusinessNameId(String businessNameId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailForCustomerReportByBusinessNameId(businessNameId);
	}

	@Override
	public List<CustomerReportResponse> fetchTotalCollectionByDateRangeAndEmployeeId(long employeeId, String fromDate,
			String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTotalCollectionByDateRangeAndEmployeeId(employeeId, fromDate, toDate, range);
	}

	@Override
	public String updateBookOrder(OrderRequest orderRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.updateBookOrder(orderRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#issueOrderToDeliverBoy(com.bluesquare.rc.rest.models.OrderIssueRequest)
	 * Oct 16, 20175:48:32 PM
	 */
	@Override
	public String packedOrderToDeliverBoy(OrderIssueRequest orderIssueRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.packedOrderToDeliverBoy(orderIssueRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#confirmIssuedOrderFromDB(java.lang.String)
	 * Oct 16, 20176:59:42 PM
	 */
	@Override
	public String confirmPackedOrderFromDB(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.confirmPackedOrderFromDB(orderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#orderDeliveredAndReturn(com.bluesquare.rc.rest.models.ReturnOrderRequest)
	 * Oct 16, 20177:51:44 PM
	 */
	@Override
	public String orderDeliveredAndReturn(ReturnOrderRequest returnOrderRequest,String appPath) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.orderDeliveredAndReturn(returnOrderRequest,appPath);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderProductIssueDetailsByOrderId(java.lang.String)
	 * Oct 17, 201711:36:56 AM
	 */
	@Override
	public OrderProductIssueDetails fetchOrderProductIssueDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductIssueDetailsByOrderId(orderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsTodaysListByAreaId(long)
	 * Oct 17, 20171:19:54 PM
	 */
/*	@Override
	public List<OrderDetails> fetchOrderDetailsTodaysListByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsTodaysListByAreaId(areaId);
	}

	 (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsPendingListByAreaId(long)
	 * Oct 17, 20171:22:25 PM
	 
	@Override
	public List<OrderDetails> fetchOrderDetailsPendingListByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsPendingListByAreaId(areaId);
	}*/

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(long, long, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 17, 20171:56:22 PM
	 */
	@Override
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(long employeeId,
			long areaId, String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(employeeId, areaId, fromDate, toDate, range);
	}

	

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchTodaysPackedOrderListByAreaIdAndEmployeeId(long, long)
	 * Oct 18, 20171:43:30 PM
	 */
	@Override
	public OrderDetailsList fetchTodaysPackedOrderListByAreaIdAndEmployeeId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTodaysPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchPendingPackedOrderListByAreaIdAndEmployeeId(long, long)
	 * Oct 18, 20171:43:30 PM
	 */
	@Override
	public OrderDetailsList fetchPendingPackedOrderListByAreaIdAndEmployeeId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchPendingPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchPendingIssuedOrderListByAreaIdAndEmployeeId(long, long)
	 * Oct 18, 20171:57:06 PM
	 */
	@Override
	public OrderDetailsList fetchPendingIssuedOrderListByAreaIdAndEmployeeId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchPendingIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(long, long)
	 * Oct 18, 20171:57:06 PM
	 */
	@Override
	public OrderDetailsList fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#reIssueOrderDetails(com.bluesquare.rc.rest.models.ReIssueSaveRequest)
	 * Oct 21, 201710:51:54 AM
	 */
	@Override
	public String reIssueOrderDetails(OrderReIssueRequest orderReIssueRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.reIssueOrderDetails(orderReIssueRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#showOrderReportByBusinessNameId(java.lang.String)
	 * Oct 21, 201711:24:51 AM
	 */
	@Override
	public List<OrderReportList> showOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.showOrderReportByBusinessNameId(businessNameId,range, startDate,endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#showOrderReportByEmployeeDetailsId(java.lang.String)
	 * Oct 23, 201711:53:19 AM
	 */
	@Override
	public List<OrderReportList> showOrderReportByEmployeeSMId(String employeeSMId,String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.showOrderReportByEmployeeSMId(employeeSMId,range,startDate,endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderProductIssueListForIssueReportByOrderId(java.lang.String)
	 * Oct 24, 201711:30:41 AM
	 */
	@Override
	public OrderProductIssueDetails fetchOrderProductIssueListForIssueReportByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductIssueListForIssueReportByOrderId(orderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(long, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 24, 20171:39:53 PM
	 */
	@Override
	public List<OrderProductIssueDetails> fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(long employeeId,
			String fromDate, String toDate, String range, String orderStatus) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(employeeId, fromDate, toDate, range, orderStatus);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsByDateRangeAndEmpId(long, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 26, 20174:40:01 PM
	 */
	@Override
	public List<OrderDetails> fetchOrderDetailsByDateRangeAndEmpId(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsByDateRangeAndEmpId(employeeId, fromDate, toDate, range);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchReplacementIssuedOrdersByEmployeeId(long)
	 * Oct 28, 20172:32:05 PM
	 */
	@Override
	public List<ReIssueOrderDetails> fetchReplacementIssuedOrdersByEmployeeId(long employeeId,String status) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReplacementIssuedOrdersByEmployeeId(employeeId,status);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#doneReIssueAndOrderStatusDelivered(com.bluesquare.rc.rest.models.ReIssueDelivered)
	 * Oct 30, 201712:20:25 PM
	 */
	@Override
	public String doneReIssueAndOrderStatusDelivered(ReIssueDelivered reIssueDelivered) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.doneReIssueAndOrderStatusDelivered(reIssueDelivered);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsForReIssueDelivedByOrderId(java.lang.String)
	 * Oct 30, 201712:44:31 PM
	 */
	

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchTopFiveSalesManByIssuedSale()
	 * Oct 31, 201711:24:58 AM
	 */
	@Override
	public ChartDetailsResponse fetchTopFiveSalesManByIssuedSale(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTopFiveSalesManByIssuedSale(range, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchReIssueOrderDetailsByReIssueOrderDetailsId(long)
	 * Oct 31, 201712:46:06 PM
	 */
	@Override
	public ReIssueOrderDetails fetchReIssueOrderDetailsByReIssueOrderDetailsId(long reIssueOrderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReIssueOrderDetailsByReIssueOrderDetailsId(reIssueOrderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(long)
	 * Oct 31, 201712:46:06 PM
	 */
	@Override
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(
			long reIssueOrderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(reIssueOrderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsForReIssueDelivedByOrderId(long)
	 * Oct 31, 201712:46:06 PM
	 */
	@Override
	public ReIssueOrderProductDetailsListModel fetchOrderDetailsForReIssueDelivedByOrderId(long reIssueOrderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsForReIssueDelivedByOrderId(reIssueOrderId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailForTotalCollectionReportByBusinessNameId(java.lang.String)
	 * Oct 31, 20178:00:03 PM
	 */
	@Override
	public List<OrderDetailsPaymentList> fetchOrderDetailForTotalCollectionReportByBusinessNameId(OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailForTotalCollectionReportByBusinessNameId(orderDetailByBusinessNameIdEmployeeIdRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchTotalSaleForIndexPage()
	 * Nov 1, 201710:25:24 AM
	 */
	@Override
	public double fetchTotalSaleForIndexPage(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTotalSaleForIndexPage(range, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchTotalAmountInvestInMarketForIndexPage()
	 * Nov 1, 201710:47:30 AM
	 */
	@Override
	public double fetchTotalAmountInvestInMarketForIndexPage(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchTotalAmountInvestInMarketForIndexPage(range, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#cancelOrder(java.lang.String, long)
	 * Nov 1, 20174:45:48 PM
	 */
	@Override
	public String cancelOrder(CancelOrderRequest cancelOrderRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.cancelOrder(cancelOrderRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchBillPrintData(java.lang.String)
	 * Nov 9, 20176:23:41 PM
	 */
	@Override
	public BillPrintDataModel fetchBillPrintData(String orderId,long companyId,long branchId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchBillPrintData(orderId, companyId, branchId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderDetailsListOfBusinessByBusinessNameId(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * Nov 18, 201711:12:07 AM
	 */
	@Override
	public OrderDetailsListOfBusiness fetchOrderDetailsListOfBusinessByBusinessNameId(String businessNameId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsListOfBusinessByBusinessNameId(businessNameId, fromDate, toDate, range);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#sendSMSTOShopsUsingOrderId(java.lang.String, java.lang.String)
	 * Nov 27, 20171:15:45 PM
	 */
	@Override
	public String sendSMSTOShopsUsingOrderId(String orderIds, String smsText,String mobileNumber) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.sendSMSTOShopsUsingOrderId(orderIds, smsText, mobileNumber);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(long, long, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 15, 201712:30:19 PM
	 */
	@Override
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(employeeId, fromDate, toDate, range);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchSalesReportModel(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 28, 20175:28:40 PM
	 */
	@Override
	public List<SalesReportModel> fetchSalesReportModel(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSalesReportModel(startDate, endDate, range);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchOrderListByAreaId(long, long)
	 * Dec 29, 20171:05:47 PM
	 */
	@Override
	public OrderDetailsList fetchOrderListByAreaId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderListByAreaId(areaId, employeeId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchPendingOrderListByAreaId(long, long)
	 * Dec 29, 20171:05:47 PM
	 */
	@Override
	public OrderDetailsList fetchPendingOrderListByAreaId(long areaId, long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchPendingOrderListByAreaId(areaId, employeeId);
	}

	@Override
	public String freeProductAddInOrderProductDetails(BookOrderFreeProductRequest bookOrderFreeProductRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.freeProductAddInOrderProductDetails(bookOrderFreeProductRequest);
	}

	@Override
	public List<OrderReportList> cancelOrderReport(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.cancelOrderReport(range, startDate, endDate);
	}

	@Override
	public List<OrderDetails> fetchCancelOrderReportByEmployeeId(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchCancelOrderReportByEmployeeId(employeeId, fromDate, toDate, range);
	}

	@Override
	public void updateEditOrder(String updateOrderProductListId, String orderId) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateEditOrder(updateOrderProductListId, orderId);
	}

	@Override
	public List<ReturnFromDeliveryBoy> fetchReturnFromDeliveryBoyList(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnFromDeliveryBoyList(orderId);
	}

	@Override
	public List<ReturnOrderFromDeliveryBoyReport> fetchReturnOrderFromDeliveryBoyReport(String range, String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnOrderFromDeliveryBoyReport(range, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#updateReturnFromDeliveryBoy(java.lang.String, java.lang.String)
	 * Jan 19, 20185:25:14 PM
	 */
	@Override
	public void updateReturnFromDeliveryBoy(String orderId, String returnFromDeliveryBoyList) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateReturnFromDeliveryBoy(orderId, returnFromDeliveryBoyList);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#updateReturnFromDeliveryBoyForApp(java.util.List)
	 * Jan 31, 20188:30:51 PM
	 */
	@Override
	public void updateReturnFromDeliveryBoyForApp(List<ReturnFromDeliveryBoyModel> returnFromDeliveryBoyList,String returnFromDeliveryBoyMainId) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateReturnFromDeliveryBoyForApp(returnFromDeliveryBoyList, returnFromDeliveryBoyMainId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.OrderDetailsService#fetchGstBillReport(java.lang.String, java.lang.String)
	 * Feb 6, 20181:31:04 PM
	 */
	@Override
	public GstBillReport fetchGstBillReport(String startDate, String endDate,String type) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchGstBillReport(startDate, endDate,type);
	}

	@Override
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk() {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSnapProductDetailsForGk();
	}

	@Override
	public double totalSaleAmountForProfitAndLoss(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.totalSaleAmountForProfitAndLoss(startDate, endDate);
	}

	@Override
	public  List<OrderDetails> fetchLast3MonthOrderListByAreaIdForOfflineMode(long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchLast3MonthOrderListByAreaIdForOfflineMode(employeeId);
	}

	@Override
	public List<CancelReason> fetchCancelReason() {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchCancelReason();
	}

	@Override
	public OfflineAPIRequest fetchofflineApiHandlingByDbIdAndIMEINo(OfflineAPIRequest apiRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchofflineApiHandlingByDbIdAndIMEINo(apiRequest);
	}

	@Override
	public void saveOfflineAPIrequest(OfflineAPIRequest apiRequest) {
		// TODO Auto-generated method stub
		orderDetailsDAO.saveOfflineAPIrequest(apiRequest);
	}

	@Override
	public void updateTheStatusOfOfflineAPIRequest(long dbId,String imeiNo,String status) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateTheStatusOfOfflineAPIRequest(dbId, imeiNo, status);;
	}

	@Override
	public boolean checkTransaportationUsed(long transportationId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.checkTransaportationUsed(transportationId);
	}

	@Override
	public ReturnFromDeliveryBoyMain fetchReturnFromDeliveryBoyMain(String returnFromDeliveryBoyMainId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnFromDeliveryBoyMain(returnFromDeliveryBoyMainId);
	}

	@Override
	public List<OrderReportList> showOrderReportByBusinessNameIdOfBadDebts(String businessNameId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.showOrderReportByBusinessNameIdOfBadDebts(businessNameId);
	}

	@Override
	public ReturnOrderProductP fetchReturnOrderProductPByReturnOrderProductId(String returnOrderProductId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnOrderProductPByReturnOrderProductId(returnOrderProductId);
	}

	@Override
	public List<ReturnOrderProductDetailsP> fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(
			String returnOrderProductId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(returnOrderProductId);
	}

	@Override
	public void savePermanentReturnOrder(ReturnPermanentModelRequest returnPermanentModelRequest) {
		// TODO Auto-generated method stub
		orderDetailsDAO.savePermanentReturnOrder(returnPermanentModelRequest);
	}

	@Override
	public List<PReturnOrderDetailsModel> permanentReturnOrderReport(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.permanentReturnOrderReport(range, startDate, endDate);
	}

	@Override
	public BillPrintDataModel fetchReturnOrderCreditNoteBillPrintData(String returnOrderProductId,long companyId,long branchId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReturnOrderCreditNoteBillPrintData(returnOrderProductId, companyId, branchId);
	}
}
