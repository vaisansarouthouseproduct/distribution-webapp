package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.service.BranchService;

@Component
@Transactional
@Service("branchService")
@Qualifier("branchService")
public class BranchServiceImpl implements BranchService {

	@Autowired
	BranchDAO branchDAO;
	
	
	@Override
	public List<BranchModel> fetchBranchListByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		return branchDAO.fetchBranchListByCompanyId(companyId);
	}

	@Override
	public List<Branch> fetchBranchListByBrachIdList(long[] branchIds) {
		// TODO Auto-generated method stub
		return branchDAO.fetchBranchListByBrachIdList(branchIds);
	}

	@Override
	public Branch fetchBranchByBranchId(long branchId) {
		// TODO Auto-generated method stub
		return branchDAO.fetchBranchByBranchId(branchId);
	}

	@Override
	public List<BranchModel> fetchBranchByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return branchDAO.fetchBranchByEmployeeId(employeeId);
	}

}
