package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BusinessTypeDAO;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;
import com.bluesquare.rc.service.BusinessTypeService;

@Component
@Transactional
@Service("businessTypeService")
@Qualifier("businessTypeService")
public class BusinessTypeServiceimpl implements BusinessTypeService {

	@Autowired
	BusinessTypeDAO businessTypeDAO;
	
	@Override
	public void saveForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		businessTypeDAO.saveForWebApp(businessType);
	}

	@Override
	public void updateForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		businessTypeDAO.updateForWebApp(businessType);
	}

	@Override
	public List<BusinessType> fetchBusinessTypeListForWebApp() {
		// TODO Auto-generated method stub
		return businessTypeDAO.fetchBusinessTypeListForWebApp();
	}

	@Override
	public BusinessType fetchBusinessTypeForWebApp(long businessTypeId) {
		// TODO Auto-generated method stub
		return businessTypeDAO.fetchBusinessTypeForWebApp(businessTypeId);
	}

	@Override
	public List<BusinessTypeModel> fetchBusinessTypeModelList() {
		// TODO Auto-generated method stub
		return businessTypeDAO.fetchBusinessTypeModelList();
	}

}
