package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CountryDAO;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.service.CountryService;


@Component
@Transactional
@Service("countryService")
@Qualifier("countryService")
public class CountryServiceimpl implements CountryService {

	@Autowired
	CountryDAO countryDAO;
	
	@Override
	public List<Country> fetchCountryListForWebApp() {
		// TODO Auto-generated method stub
		return countryDAO.fetchCountryListForWebApp();
	}

	@Override
	public Country fetchCountryForWebApp(long coutryId) {
		// TODO Auto-generated method stub
		return countryDAO.fetchCountryForWebApp(coutryId);
	}

	@Override
	public Country saveCountryForWeb(Country country) {
		// TODO Auto-generated method stub
		return countryDAO.saveCountryForWeb(country);
	}

	@Override
	public Country updateCountryForWeb(Country country) {
		// TODO Auto-generated method stub
		return countryDAO.updateCountryForWeb(country);
	}

	@Override
	public Country fetchIndiaCountry() {
		// TODO Auto-generated method stub
		return countryDAO.fetchIndiaCountry();
	}


}
