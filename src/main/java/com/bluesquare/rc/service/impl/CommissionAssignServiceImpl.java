package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CommissionAssignDAO;
import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.models.CommissionAssignAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.CommissionAssignRequest;
import com.bluesquare.rc.models.EmployeeCommissionModel;
import com.bluesquare.rc.rest.models.CommissionAssignModel;
import com.bluesquare.rc.service.CommissionAssignService;

@Component
@Transactional
@Service("commissionAssignService")
@Qualifier("commissionAssignService")
public class CommissionAssignServiceImpl implements CommissionAssignService {

	@Autowired
	CommissionAssignDAO commissionAssignDAO;
	
	@Override
	public void saveCommissionAssignment(CommissionAssignRequest commissionAssignRequest ) {
		// TODO Auto-generated method stub
		commissionAssignDAO.saveCommissionAssignment(commissionAssignRequest);
	}

	@Override
	public CommissionAssignModel fetchCommissionAssign() {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchCommissionAssign();
	}

	@Override
	public void updateCommissionAssignment(CommissionAssignRequest commissionAssignRequest ) {
		// TODO Auto-generated method stub
		commissionAssignDAO.updateCommissionAssignment(commissionAssignRequest);
	}

	@Override
	public CommissionAssignAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchTargetsAndEmployeeListByDepartmentId(departmentId);
	}

	@Override
	public void disableCommissionAssignment(long commissionAssignId) {
		// TODO Auto-generated method stub
		commissionAssignDAO.disableCommissionAssignment(commissionAssignId);
	}

	@Override
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabs(long commissionAssignId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchCommissionAssignTargetSlabs(commissionAssignId);
	}

	@Override
	public CommissionAssign fetchCommissionAssign(long commissionAssignId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchCommissionAssign(commissionAssignId);
	}

	@Override
	public List<CommissionAssignProducts> fetchCommissionAssignProducts(long commissionAssignId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchCommissionAssignProducts(commissionAssignId);
	}

	@Override
	public boolean checkCommissionAssignNameAlreadyUsedOrNot(String commissionAssignName, long commissionAssignId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.checkCommissionAssignNameAlreadyUsedOrNot(commissionAssignName, commissionAssignId);
	}

	@Override
	public List<EmployeeCommissionModel> fetchEmployeeCommissionModel(long monthId, long yearId) {
		// TODO Auto-generated method stub
		return commissionAssignDAO.fetchEmployeeCommissionModel(monthId, yearId);
	}

}
