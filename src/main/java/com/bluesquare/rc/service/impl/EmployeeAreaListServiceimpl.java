package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeAreaListDAO;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.service.EmployeeAreaListService;

@Component
@Transactional
@Service("employeeAreaListService")
@Qualifier("employeeAreaListService")
public class EmployeeAreaListServiceimpl implements EmployeeAreaListService {

	@Autowired
	EmployeeAreaListDAO employeeAreaListDAO;
	
	@Override
	public void saveEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId) {
		// TODO Auto-generated method stub
		employeeAreaListDAO.saveEmployeeAreasForWebApp(areaIdLists,employeeDetailsId);
	}

	@Override
	public void updateEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId) {
		// TODO Auto-generated method stub
		employeeAreaListDAO.updateEmployeeAreasForWebApp(areaIdLists,employeeDetailsId);
	}

	@Override
	public void deleteForWebApp(long employeeAreaListId) {
		// TODO Auto-generated method stub
		employeeAreaListDAO.deleteForWebApp(employeeAreaListId);
	}

	@Override
	public List<EmployeeAreaList> fetchEmployeeAreaListByEmployeeDetailsId(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeAreaListDAO.fetchEmployeeAreaListByEmployeeDetailsId(employeeDetailsId);
	}

	@Override
	public String getAreaIdList(List<EmployeeAreaList> employeeAreaList) {
		// TODO Auto-generated method stub
		return employeeAreaListDAO.getAreaIdList(employeeAreaList);
	}

}
