package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.NotepadDAO;
import com.bluesquare.rc.entities.NotePad;
import com.bluesquare.rc.service.NotepadService;


@Component
@Transactional
@Service("notepadService")
@Qualifier("notepadService")
public class NotepadServiceImpl implements NotepadService{

	
	@Autowired
	NotepadDAO notepadDAO;
	@Override
	public void saveNotePad(NotePad notePad) {
		// TODO Auto-generated method stub
		notepadDAO.saveNotePad(notePad);
	}

	@Override
	public NotePad fetchNotepadById(long id) {
		// TODO Auto-generated method stub
		return notepadDAO.fetchNotepadById(id);
	}

	@Override
	public List<NotePad> fetchNotePadListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return notepadDAO.fetchNotePadListByEmployeeId(employeeId);
	}

	@Override
	public void updateNotePad(NotePad notePad) {
		// TODO Auto-generated method stub
		notepadDAO.updateNotePad(notePad);
	}

	@Override
	public void deleteNotePadById(long id) {
		// TODO Auto-generated method stub
		notepadDAO.deleteNotePadById(id);
	}

	@Override
	public void deleteNotepadByIdList(List<Long> idList) {
		// TODO Auto-generated method stub
		notepadDAO.deleteNotepadByIdList(idList);
	}

}
