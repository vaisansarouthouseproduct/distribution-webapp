package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.entities.EmployeeLocation;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.entities.EmployeeBasicSalaryStatus;
import com.bluesquare.rc.entities.EmployeeSalary;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.models.EmployeeHolidayModel;
import com.bluesquare.rc.models.EmployeeHrmDetailsResponse;
import com.bluesquare.rc.models.EmployeeLastLocation;
import com.bluesquare.rc.models.EmployeePaymentModel;
import com.bluesquare.rc.models.EmployeeSalaryStatus;
import com.bluesquare.rc.models.EmployeeViewModel;
import com.bluesquare.rc.rest.models.EmployeeHolidayDetailsResponse;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.EmployeePaymentDetailsResponse;
import com.bluesquare.rc.rest.models.SalesManReport;

public interface EmployeeDetailsService {
	// webapp

	public void saveForWebApp(EmployeeDetails employeeDetails);

	public void updateForWebApp(EmployeeDetails employeeDetails, boolean updateValid);

	public List<EmployeeDetails> employeeDetailsListForWebApp();

	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId);

	public List<EmployeeViewModel> fetchEmployeeDetailsForView();

	public List<EmployeeViewModel> fetchEmployeeDetailsForGkEmployeeView();

	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId);

	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,
			String range, long employeeDetailsId);

	EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(long employeeDetailsId, String filter, String startDate,
			String endDate);

	public EmployeeAreaDetails fetchEmployeeAreaDetails(long employeeDetailsId, long companyId);

	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays);

	public String checkHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId);

	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId,
			long employeeHolidayId);

	public void giveIncentives(EmployeeIncentives employeeIncentives);

	public EmployeeIncentives fetchIncentives(long employeeIncentiveId);

	public void givePayment(EmployeeSalary employeeSalary);

	public EmployeeSalary fetchEmployeeSalary(long employeeSalaryId);

	public void updatePayment(EmployeeSalary employeeSalary);

	public EmployeePaymentModel openPaymentModel(long employeeDetailsId);

	public String sendSMSTOEmployee(String employeeDetailsIdList, String smsText, String mobileNumber);

	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId);

	public void clearToken(String token, long employeeId);

	public List<Area> fetchAreaByEmployeeId(long employeeId);

	public List<EmployeeDetails> fetchDBEmployeeDetailByAreaId(long areaId);

	public List<EmployeeDetails> fetchSMEmployeeDetailByAreaId(long areaId);

	public List<EmployeeViewModel> fetchEmployeeDetail(long employeeDetailId);

	public List<EmployeeNameAndId> fetchDeliveryBoyListByBusinessNameAreaId(long businessNameAreaId);

	public SalesManReport fetchSalesManReport(String range, String startDate, String endDate);

	public List<EmployeeNameAndId> fetchSMandDBByGateKeeperId(long gateKeeperId);

	public List<EmployeeNameAndId> fetchDBByGateKeeperId(long employeeId);

	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId);

	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);

	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);

	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId, String filter,
			String startDate, String endDate);

	public String updateIncentive(EmployeeIncentives employeeIncentives);

	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays);

	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId);

	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId);

	public List<EmployeeLastLocation> fetchEmployeeLastLocationByDepartmentId(long deparmentId,String date);

	public List<EmployeeLastLocation> fetchEmployeeLocationByEmployeeDetailsId(long employeeDetailsId,String date);

	public EmployeeLocation fetchEmployeeLastLocationByEmployeeDetailsId(long employeeDetailsId,String date);

	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId);

	public void saveEmployeeLocation(EmployeeLocation employeeLocation);

	public List<EmployeeNameAndId> fetchEmployeeListForChat();

	public double totalEmployeeeSalaryAmountForProfitAndLoss(String startDate, String endDate);

	public EmployeeHrmDetailsResponse fetchEmployeeLeaveAndBalanceAmountForAPP(long employeeId);

	public EmployeePaymentDetailsResponse fetchEmployeeSalaryDetails(String fromDate, String endDate);

	public EmployeeHolidayDetailsResponse fetchEmployeeHolidayDetailsForApp(String filter, String startDate,
			String endDate);

	public void saveEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture);

	public void updateEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture);

	public EmployeeProfilePicture fetchEmployeeImage();
	
	public List<Area> fetchAreaListByCompanyId(long companyId);
	public Employee getAdminEmployee();
	public List<Object[]> fetchEmployeeLocationIdsDetails(long employeeDetailsId,long companyId);
}
