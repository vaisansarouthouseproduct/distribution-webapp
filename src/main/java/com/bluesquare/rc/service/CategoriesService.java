package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.responseEntities.CategoriesModel;

public interface CategoriesService {
	//webapp

			public void saveCategoriesForWebApp(Categories categories);
			public void updateCategoriesForWebApp(Categories categories);
			public Categories fetchCategoriesForWebApp(long categoriesId);
			public List<Categories> fetchCategoriesListForWebApp();
			public List<CategoriesModel> fetchCategoriesModelList();
}
