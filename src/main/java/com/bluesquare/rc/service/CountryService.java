package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Country;

public interface CountryService {

	public List<Country> fetchCountryListForWebApp();
	public Country fetchCountryForWebApp(long coutryId);
	//public List<Country> fetchCountryListForBranchConfig();
	public Country saveCountryForWeb(Country country);
	public Country updateCountryForWeb(Country country);
	public Country fetchIndiaCountry();
}
