package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.models.LedgerPaymentView;

public interface LedgerService {
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate,String range);
}
