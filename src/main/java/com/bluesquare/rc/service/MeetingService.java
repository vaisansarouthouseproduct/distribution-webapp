package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.models.MeetingScheduleModel;
import com.bluesquare.rc.rest.models.MeetingRequest;

public interface MeetingService {
	public void saveMeeting(Meeting meeting);

	public void updateMeeting(Meeting meeting);

	public void deleteMeeting(long meetingId);

	public void updateCancelStatus(long meetingId, String cancelReason);

	public Meeting fetchMeetingByMeetingId(long meetingId);

	public List<Meeting> fetchMeetingListbyEmployeeIdAndDate(long employeeDetailsId, String pickDate);

	public List<MeetingScheduleModel> fetchEmployeeWiseScheduledMeeting(long departmentId, String pickDate);

	public String checkMeetingSchedule(long employeeId, String date, String fromTime, String toTime);

	public String checkMeetingScheduleForUpdate(long employeeId, String date, String fromTime, String toTime,
			long meetingId);

	public List<Meeting> fetchMeetingsbyEmployeeIdAndDate(long employeeId, String pickDate);

	public void meetingCompleteWithReview(MeetingRequest meetingRequest);

	public List<Meeting> fetchCompletedMeetingList(String range, String startDate, String endDate);

	public List<Meeting> fetchCancelledMeetingList(String range, String startDate, String endDate);

	public List<Meeting> fetchPendingMeetingList(String range, String startDate, String endDate);
	
	public Meeting fetchMeetingByShopVistedId(long shopVistedId);

}
