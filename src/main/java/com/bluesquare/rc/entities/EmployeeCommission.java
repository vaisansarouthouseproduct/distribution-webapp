package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_commission")
@Component
public class EmployeeCommission {

	@Id
	@Column(name = "employee_commission_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeCommissionId;

	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datetime;

	public long getEmployeeCommissionId() {
		return employeeCommissionId;
	}

	public void setEmployeeCommissionId(long employeeCommissionId) {
		this.employeeCommissionId = employeeCommissionId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	@Override
	public String toString() {
		return "EmployeeCommission [employeeCommissionId=" + employeeCommissionId + ", employeeDetails="
				+ employeeDetails + ", totalAmount=" + totalAmount + ", datetime=" + datetime + "]";
	}

}
