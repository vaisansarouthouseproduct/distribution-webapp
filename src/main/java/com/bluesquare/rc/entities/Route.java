package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "route")
@Component
public class Route {

	@Id
	@Column(name = "route_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long routeId;

	@Column(name = "route_name")
	private String routeName;

	@Column(name = "route_gen_id")
	private String routeGenId;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;

	@Column(name = "added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDatetime;

	@Column(name = "updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updatedDatetime;

	@Column(name = "deleted_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date deleteDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	

	public long getRouteId() {
		return routeId;
	}

	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteGenId() {
		return routeGenId;
	}

	public void setRouteGenId(String routeGenId) {
		this.routeGenId = routeGenId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(Date addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getDeleteDatetime() {
		return deleteDatetime;
	}

	public void setDeleteDatetime(Date deleteDatetime) {
		this.deleteDatetime = deleteDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Route [routeId=" + routeId + ", routeName=" + routeName + ", routeGenId=" + routeGenId + ", status="
				+ status + ", addedDatetime=" + addedDatetime + ", updatedDatetime=" + updatedDatetime
				+ ", deleteDatetime=" + deleteDatetime + ", company=" + company + "]";
	}

	

}
