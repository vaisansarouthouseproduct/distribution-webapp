package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "supplier_order")
@Component
public class SupplierOrder {
	@Id
	@Column(name = "supplier_order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int supplierOrderPkId;	
	
	@Column(name = "supplier_order_id")
	private String supplierOrderId;
	
	@Column(name="total_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmount;
	
	@Column(name="total_amount_with_tax", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmountWithTax;
	
	@Column(name = "total_quantity")
	private long totalQuantity;
	
	@Column(name="supplier_order_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date supplierOrderDatetime;
	
	@Column(name="supplier_order_update_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date supplierOrderUpdateDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;
	
	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getSupplierOrderId() {
		return supplierOrderId;
	}

	public void setSupplierOrderId(String supplierOrderId) {
		this.supplierOrderId = supplierOrderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public Date getSupplierOrderDatetime() {
		return supplierOrderDatetime;
	}

	public void setSupplierOrderDatetime(Date supplierOrderDatetime) {
		this.supplierOrderDatetime = supplierOrderDatetime;
	}

	public Date getSupplierOrderUpdateDatetime() {
		return supplierOrderUpdateDatetime;
	}

	public void setSupplierOrderUpdateDatetime(Date supplierOrderUpdateDatetime) {
		this.supplierOrderUpdateDatetime = supplierOrderUpdateDatetime;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getSupplierOrderPkId() {
		return supplierOrderPkId;
	}

	public void setSupplierOrderPkId(int supplierOrderPkId) {
		this.supplierOrderPkId = supplierOrderPkId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SupplierOrder [supplierOrderPkId=" + supplierOrderPkId + ", supplierOrderId=" + supplierOrderId
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity="
				+ totalQuantity + ", supplierOrderDatetime=" + supplierOrderDatetime + ", supplierOrderUpdateDatetime="
				+ supplierOrderUpdateDatetime + ", company=" + company + ", status=" + status + ", branch=" + branch
				+ "]";
	}

	
}
