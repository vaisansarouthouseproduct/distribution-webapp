package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_routes")
@Component
public class EmployeeRoutes {

	@Id
	@Column(name = "employee_route_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeRouteId;
	
	@ManyToOne
    @JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
    @JoinColumn(name = "route_id")
	private Route route;

	public long getEmployeeRouteId() {
		return employeeRouteId;
	}

	public void setEmployeeRouteId(long employeeRouteId) {
		this.employeeRouteId = employeeRouteId;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "EmployeeRoutes [employeeRouteId=" + employeeRouteId + ", employee=" + employee + ", route=" + route
				+ "]";
	}	
}
