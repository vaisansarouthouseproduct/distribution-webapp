package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "shop_visted")
@Component
public class ShopVisit {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "owner_name")
	private String ownerName;
	
	@Column(name = "shop_name")
	private String shopName;
	
	@Column(name = "address", columnDefinition = "LONGTEXT")
	private String address;
	
	@Column(name = "comment", columnDefinition = "LONGTEXT")
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="contact_id")
	private Contact contact;
	
	@Column(name = "shop_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date shopVisitAddeddDatetime;
	
	@Column(name = "next_visit_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date nextVisitDate;
	
	@Column(name = "next_visit_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date nextVisitTime;
	
	@Column(name = "next_visit_end_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date nextVisitEndTime;
	
	@Column(name = "shop_visit_updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date shopVisitUpdatedDatetime;
	
	@Column(name="is_next_visit")
	private String isNextVisit;
	
/*	@Lob
	@Column(name = "shop_image",columnDefinition="LONGTEXT")
	private String shopImage;*/

	@ManyToOne
	@JoinColumn(name="employee_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Date getShopVisitAddeddDatetime() {
		return shopVisitAddeddDatetime;
	}

	public void setShopVisitAddeddDatetime(Date shopVisitAddeddDatetime) {
		this.shopVisitAddeddDatetime = shopVisitAddeddDatetime;
	}

	public Date getNextVisitDate() {
		return nextVisitDate;
	}

	public void setNextVisitDate(Date nextVisitDate) {
		this.nextVisitDate = nextVisitDate;
	}

	public Date getNextVisitTime() {
		return nextVisitTime;
	}

	public void setNextVisitTime(Date nextVisitTime) {
		this.nextVisitTime = nextVisitTime;
	}

	public Date getNextVisitEndTime() {
		return nextVisitEndTime;
	}

	public void setNextVisitEndTime(Date nextVisitEndTime) {
		this.nextVisitEndTime = nextVisitEndTime;
	}

	public Date getShopVisitUpdatedDatetime() {
		return shopVisitUpdatedDatetime;
	}

	public void setShopVisitUpdatedDatetime(Date shopVisitUpdatedDatetime) {
		this.shopVisitUpdatedDatetime = shopVisitUpdatedDatetime;
	}

	public String getIsNextVisit() {
		return isNextVisit;
	}

	public void setIsNextVisit(String isNextVisit) {
		this.isNextVisit = isNextVisit;
	}

/*	public String getShopImage() {
		return shopImage;
	}

	public void setShopImage(String shopImage) {
		this.shopImage = shopImage;
	}*/

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "ShopVisit [id=" + id + ", ownerName=" + ownerName + ", shopName=" + shopName + ", address=" + address
				+ ", comment=" + comment + ", contact=" + contact + ", shopVisitAddeddDatetime="
				+ shopVisitAddeddDatetime + ", nextVisitDate=" + nextVisitDate + ", nextVisitTime=" + nextVisitTime
				+ ", nextVisitEndTime=" + nextVisitEndTime + ", shopVisitUpdatedDatetime=" + shopVisitUpdatedDatetime
				+ ", isNextVisit=" + isNextVisit + ", employee=" + employee + ", branch="
				+ branch + "]";
	}

	
	
}
