package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name="bank_payee_details")
@Component
public class BankPayeeDetails {


	@Id
	@Column(name="payee_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long payeeId;
	
	@Column(name="payee_name")
	private String payeeName;
	
	@Column(name="payee_address")
	private String payeeAddress;
	
	
	
	@Column(name="payee_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date payeeAddedDatetime;
	
	@Column(name="payee_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date payeeUpdatedDatetime;
	

	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;


	public long getPayeeId() {
		return payeeId;
	}


	public void setPayeeId(long payeeId) {
		this.payeeId = payeeId;
	}


	public String getPayeeName() {
		return payeeName;
	}


	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}


	public String getPayeeAddress() {
		return payeeAddress;
	}


	public void setPayeeAddress(String payeeAddress) {
		this.payeeAddress = payeeAddress;
	}


	public Date getPayeeAddedDatetime() {
		return payeeAddedDatetime;
	}


	public void setPayeeAddedDatetime(Date payeeAddedDatetime) {
		this.payeeAddedDatetime = payeeAddedDatetime;
	}


	public Date getPayeeUpdatedDatetime() {
		return payeeUpdatedDatetime;
	}


	public void setPayeeUpdatedDatetime(Date payeeUpdatedDatetime) {
		this.payeeUpdatedDatetime = payeeUpdatedDatetime;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	@Override
	public String toString() {
		return "BankPayeeDetails [payeeId=" + payeeId + ", payeeName=" + payeeName + ", payeeAddress=" + payeeAddress
				+ ", payeeAddedDatetime=" + payeeAddedDatetime + ", payeeUpdatedDatetime=" + payeeUpdatedDatetime
				+ ", company=" + company + "]";
	}

	
	

}
