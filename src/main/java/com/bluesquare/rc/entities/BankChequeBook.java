package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name="bank_cheque_book")
@Component
public class BankChequeBook {

	@Id
	@Column(name="chequebook_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long chequeBookId;
	
	@ManyToOne
	@JoinColumn(name="bank_account_id")
	private BankAccount bankAccount;
	
	@ManyToOne
	@JoinColumn(name="cheque_template_id")
	private BankCBTemplateDesign bankCBTemplateDesign;
	
	@Column(name="chq_book_name")
	private String chqBookName;
	
	@Column(name="no_of_leaves")
	private long numberOfLeaves;
	
	@Column(name="no_of_unused_leaves")
	private long numberOfUnusedLeaves;
	
	@Column(name="srt_chq_no")
	private String srtChqNo;
	
	@Column(name="end_cheque_no")
	private String endChqNo;
	
	@Column(name="chq_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date chqBookAddedDateTime;
	
	
	@Column(name="chq_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date chqBookUpdateddateTime;


	public long getChequeBookId() {
		return chequeBookId;
	}


	public void setChequeBookId(long chequeBookId) {
		this.chequeBookId = chequeBookId;
	}


	public BankAccount getBankAccount() {
		return bankAccount;
	}


	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}


	public String getChqBookName() {
		return chqBookName;
	}


	public void setChqBookName(String chqBookName) {
		this.chqBookName = chqBookName;
	}


	public long getNumberOfLeaves() {
		return numberOfLeaves;
	}


	public void setNumberOfLeaves(long numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}


	public long getNumberOfUnusedLeaves() {
		return numberOfUnusedLeaves;
	}


	public void setNumberOfUnusedLeaves(long numberOfUnusedLeaves) {
		this.numberOfUnusedLeaves = numberOfUnusedLeaves;
	}


	public String getSrtChqNo() {
		return srtChqNo;
	}


	public void setSrtChqNo(String srtChqNo) {
		this.srtChqNo = srtChqNo;
	}


	public String getEndChqNo() {
		return endChqNo;
	}


	public void setEndChqNo(String endChqNo) {
		this.endChqNo = endChqNo;
	}


	public Date getChqBookAddedDateTime() {
		return chqBookAddedDateTime;
	}


	public void setChqBookAddedDateTime(Date chqBookAddedDateTime) {
		this.chqBookAddedDateTime = chqBookAddedDateTime;
	}


	public Date getChqBookUpdateddateTime() {
		return chqBookUpdateddateTime;
	}


	public void setChqBookUpdateddateTime(Date chqBookUpdateddateTime) {
		this.chqBookUpdateddateTime = chqBookUpdateddateTime;
	}


	public BankCBTemplateDesign getBankCBTemplateDesign() {
		return bankCBTemplateDesign;
	}


	public void setBankCBTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign) {
		this.bankCBTemplateDesign = bankCBTemplateDesign;
	}


	@Override
	public String toString() {
		return "BankChequeBook [chequeBookId=" + chequeBookId + ", bankAccount=" + bankAccount
				+ ", bankCBTemplateDesign=" + bankCBTemplateDesign + ", chqBookName=" + chqBookName
				+ ", numberOfLeaves=" + numberOfLeaves + ", numberOfUnusedLeaves=" + numberOfUnusedLeaves
				+ ", srtChqNo=" + srtChqNo + ", endChqNo=" + endChqNo + ", chqBookAddedDateTime=" + chqBookAddedDateTime
				+ ", chqBookUpdateddateTime=" + chqBookUpdateddateTime + "]";
	}


	

	
}
