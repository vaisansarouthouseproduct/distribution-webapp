package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "brand")
@Component
public class Brand {

	@Id
	@Column(name = "brand_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long brandId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name="brand_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date brandAddedDatetime;
	
	@Column(name="brand_update_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date brandUpdateDatetime;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrandAddedDatetime() {
		return brandAddedDatetime;
	}

	public void setBrandAddedDatetime(Date brandAddedDatetime) {
		this.brandAddedDatetime = brandAddedDatetime;
	}

	public Date getBrandUpdateDatetime() {
		return brandUpdateDatetime;
	}

	public void setBrandUpdateDatetime(Date brandUpdateDatetime) {
		this.brandUpdateDatetime = brandUpdateDatetime;
	}

	@Override
	public String toString() {
		return "Brand [brandId=" + brandId + ", name=" + name + ", brandAddedDatetime=" + brandAddedDatetime
				+ ", brandUpdateDatetime=" + brandUpdateDatetime + ", company=" + company + ", branch=" + branch + "]";
	}

	

	
}
