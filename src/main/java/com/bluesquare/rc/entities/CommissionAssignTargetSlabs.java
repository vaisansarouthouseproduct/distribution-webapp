package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "commission_assign_target_slab")
@Component
public class CommissionAssignTargetSlabs {

	@Id
	@Column(name = "commission_assign_slab_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long commissionAssignTargetSlabId;

	@Column(name = "from_value")
	private float fromValue;

	@Column(name = "to_value")
	private float toValue;

	@Column(name = "commission_type")
	private String commissionType;

	@Column(name = "targetValue")
	private float targetValue;

	@ManyToOne
	@JoinColumn(name = "commission_assign_id")
	private CommissionAssign commissionAssign;

	public long getCommissionAssignTargetSlabId() {
		return commissionAssignTargetSlabId;
	}

	public void setCommissionAssignTargetSlabId(long commissionAssignTargetSlabId) {
		this.commissionAssignTargetSlabId = commissionAssignTargetSlabId;
	}

	public float getFromValue() {
		return fromValue;
	}

	public void setFromValue(float fromValue) {
		this.fromValue = fromValue;
	}

	public float getToValue() {
		return toValue;
	}

	public void setToValue(float toValue) {
		this.toValue = toValue;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public float getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(float targetValue) {
		this.targetValue = targetValue;
	}

	public CommissionAssign getCommissionAssign() {
		return commissionAssign;
	}

	public void setCommissionAssign(CommissionAssign commissionAssign) {
		this.commissionAssign = commissionAssign;
	}

	@Override
	public String toString() {
		return "CommissionAssignTargetSlabs [commissionAssignTargetSlabId=" + commissionAssignTargetSlabId
				+ ", fromValue=" + fromValue + ", toValue=" + toValue + ", commissionType=" + commissionType
				+ ", targetValue=" + targetValue + ", commissionAssign=" + commissionAssign + "]";
	}

}
