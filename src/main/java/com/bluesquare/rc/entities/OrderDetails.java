package com.bluesquare.rc.entities;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "order_details")
@Component

public class OrderDetails {

	@Id
	@Column(name = "order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderPkId;

	@Column(name = "order_id")
	private String orderId;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;

	@ManyToOne
	@JoinColumn(name = "employee_id_sm")
	private Employee employeeSM;

	@ManyToOne
	@JoinColumn(name = "employee_id_cancel")
	private Employee employeeIdCancel;

	@Column(name = "cancel_reason")
	private String cancelReason;

	@ManyToOne
	@JoinColumn(name = "business_name_id")
	private BusinessName businessName;

	@Column(name = "payment_period_days")
	private long paymentPeriodDays;

	@ManyToOne
	@JoinColumn(name = "order_status_id")
	private OrderStatus orderStatus;

	@Column(name = "old_order_status")
	private String oldOrderStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "bad_debts_status")
	private boolean badDebtsStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "paid_status")
	private boolean payStatus;

	@Column(name = "issued_total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double issuedTotalAmount;

	@Column(name = "issued_total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double issuedTotalAmountWithTax;

	@Column(name = "issued_total_quantity")
	private long issuedTotalQuantity;

	@Column(name = "order_details_payment_take_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date orderDetailsPaymentTakeDatetime;

	@Column(name = "order_details_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date orderDetailsAddedDatetime;

	@Column(name = "cancel_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date cancelDate;

	@Column(name = "issue_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date issueDate;

	@Column(name = "packed_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date packedDate;

	@Column(name = "delivery_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date deliveryDate;

	@Column(name = "confirm_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date confirmDate;

	@Column(name = "confirm_total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double confirmTotalAmount;

	@Column(name = "confirm_total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double confirmTotalAmountWithTax;

	@Column(name = "confirm_total_quantity")
	private long confirmTotalQuantity;

	@Column(name = "invoice_number")
	private String invoiceNumber;

	@ManyToOne
	@JoinColumn(name = "transportation_id")
	private Transportation transportation;

	@Column(name = "vehicle_no")
	private String vehicleNo;

	@Column(name = "docket_no")
	private String docketNo;
	
	@Column(name = "transportation_charges", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double transportationCharges;

	public long getOrderPkId() {
		return orderPkId;
	}

	public void setOrderPkId(long orderPkId) {
		this.orderPkId = orderPkId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Employee getEmployeeSM() {
		return employeeSM;
	}

	public void setEmployeeSM(Employee employeeSM) {
		this.employeeSM = employeeSM;
	}

	public Employee getEmployeeIdCancel() {
		return employeeIdCancel;
	}

	public void setEmployeeIdCancel(Employee employeeIdCancel) {
		this.employeeIdCancel = employeeIdCancel;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public long getPaymentPeriodDays() {
		return paymentPeriodDays;
	}

	public void setPaymentPeriodDays(long paymentPeriodDays) {
		this.paymentPeriodDays = paymentPeriodDays;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOldOrderStatus() {
		return oldOrderStatus;
	}

	public void setOldOrderStatus(String oldOrderStatus) {
		this.oldOrderStatus = oldOrderStatus;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public double getIssuedTotalAmount() {
		return issuedTotalAmount;
	}

	public void setIssuedTotalAmount(double issuedTotalAmount) {
		this.issuedTotalAmount = issuedTotalAmount;
	}

	public double getIssuedTotalAmountWithTax() {
		return issuedTotalAmountWithTax;
	}

	public void setIssuedTotalAmountWithTax(double issuedTotalAmountWithTax) {
		this.issuedTotalAmountWithTax = issuedTotalAmountWithTax;
	}

	public long getIssuedTotalQuantity() {
		return issuedTotalQuantity;
	}

	public void setIssuedTotalQuantity(long issuedTotalQuantity) {
		this.issuedTotalQuantity = issuedTotalQuantity;
	}

	public Date getOrderDetailsPaymentTakeDatetime() {
		return orderDetailsPaymentTakeDatetime;
	}

	public void setOrderDetailsPaymentTakeDatetime(Date orderDetailsPaymentTakeDatetime) {
		this.orderDetailsPaymentTakeDatetime = orderDetailsPaymentTakeDatetime;
	}

	public Date getOrderDetailsAddedDatetime() {
		return orderDetailsAddedDatetime;
	}

	public void setOrderDetailsAddedDatetime(Date orderDetailsAddedDatetime) {
		this.orderDetailsAddedDatetime = orderDetailsAddedDatetime;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getPackedDate() {
		return packedDate;
	}

	public void setPackedDate(Date packedDate) {
		this.packedDate = packedDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public double getConfirmTotalAmount() {
		return confirmTotalAmount;
	}

	public void setConfirmTotalAmount(double confirmTotalAmount) {
		this.confirmTotalAmount = confirmTotalAmount;
	}

	public double getConfirmTotalAmountWithTax() {
		return confirmTotalAmountWithTax;
	}

	public void setConfirmTotalAmountWithTax(double confirmTotalAmountWithTax) {
		this.confirmTotalAmountWithTax = confirmTotalAmountWithTax;
	}

	public long getConfirmTotalQuantity() {
		return confirmTotalQuantity;
	}

	public void setConfirmTotalQuantity(long confirmTotalQuantity) {
		this.confirmTotalQuantity = confirmTotalQuantity;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Transportation getTransportation() {
		return transportation;
	}

	public void setTransportation(Transportation transportation) {
		this.transportation = transportation;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public boolean isBadDebtsStatus() {
		return badDebtsStatus;
	}

	public void setBadDebtsStatus(boolean badDebtsStatus) {
		this.badDebtsStatus = badDebtsStatus;
	}
	
	
	

	public double getTransportationCharges() {
		return transportationCharges;
	}

	public void setTransportationCharges(double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	

	@Override
	public String toString() {
		return "OrderDetails [orderPkId=" + orderPkId + ", orderId=" + orderId + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", employeeSM="
				+ employeeSM + ", employeeIdCancel=" + employeeIdCancel + ", cancelReason=" + cancelReason
				+ ", businessName=" + businessName + ", paymentPeriodDays=" + paymentPeriodDays + ", orderStatus="
				+ orderStatus + ", oldOrderStatus=" + oldOrderStatus + ", badDebtsStatus=" + badDebtsStatus
				+ ", payStatus=" + payStatus + ", issuedTotalAmount=" + issuedTotalAmount
				+ ", issuedTotalAmountWithTax=" + issuedTotalAmountWithTax + ", issuedTotalQuantity="
				+ issuedTotalQuantity + ", orderDetailsPaymentTakeDatetime=" + orderDetailsPaymentTakeDatetime
				+ ", orderDetailsAddedDatetime=" + orderDetailsAddedDatetime + ", cancelDate=" + cancelDate
				+ ", issueDate=" + issueDate + ", packedDate=" + packedDate + ", deliveryDate=" + deliveryDate
				+ ", confirmDate=" + confirmDate + ", confirmTotalAmount=" + confirmTotalAmount
				+ ", confirmTotalAmountWithTax=" + confirmTotalAmountWithTax + ", confirmTotalQuantity="
				+ confirmTotalQuantity + ", invoiceNumber=" + invoiceNumber + ", transportation=" + transportation
				+ ", vehicleNo=" + vehicleNo + ", docketNo=" + docketNo + ", transportationCharges="
				+ transportationCharges + "]";
	}



	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}

}
