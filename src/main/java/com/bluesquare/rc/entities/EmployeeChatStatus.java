package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_chat_status")
@Component
public class EmployeeChatStatus {

	@Id
	@Column(name = "employee_chat_status_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeChatStatusId;
	
	@Column(name = "status")
	private String status;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="typing_status")
	private boolean typingStatus;
	
	@ManyToOne
    @JoinColumn(name = "employee_id")
	private Employee employee;

	public long getEmployeeChatStatusId() {
		return employeeChatStatusId;
	}

	public void setEmployeeChatStatusId(long employeeChatStatusId) {
		this.employeeChatStatusId = employeeChatStatusId;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public boolean isTypingStatus() {
		return typingStatus;
	}

	public void setTypingStatus(boolean typingStatus) {
		this.typingStatus = typingStatus;
	}

	@Override
	public String toString() {
		return "EmployeeChatStatus [employeeChatStatusId=" + employeeChatStatusId + ", status=" + status
				+ ", typingStatus=" + typingStatus + ", employee=" + employee + "]";
	}

	
	
}
