package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_branches")
@Component
public class EmployeeBranches {

	@Id
	@Column(name = "employee_branch_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeBranchId;
	
	@ManyToOne
    @JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
    @JoinColumn(name = "branch_id")
	private Branch branch;

	public long getEmployeeBranchId() {
		return employeeBranchId;
	}

	public void setEmployeeBranchId(long employeeBranchId) {
		this.employeeBranchId = employeeBranchId;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return "EmployeeBranches [employeeBranchId=" + employeeBranchId + ", employee=" + employee + ", branch="
				+ branch + "]";
	}
	
	
}
