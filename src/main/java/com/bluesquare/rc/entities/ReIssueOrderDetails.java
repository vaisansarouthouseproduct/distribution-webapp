package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "reissue_order_details")
@Component

public class ReIssueOrderDetails {

	@Id
	@Column(name = "reissue_order_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long reIssueOrderId;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmount;
	
	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;
	
	@Column(name ="reissue_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date reIssueDate;
	
	@Column(name ="reissue_delivery_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date reIssueDeliveryDate;
	
	@Column(name ="reissue_delivered_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date reIssueDeliveredDate;
	
	@ManyToOne
	@JoinColumn(name = "reissued_to_db")
	private Employee employeeSMDB;
	
	@ManyToOne
	@JoinColumn(name = "reissued_by_gk")
	private Employee employeeGK;
	
	@Column(name = "order_reissue_status")
	private String status;
	
	@ManyToOne
	@JoinColumn(name="return_order_product_id")
	ReturnOrderProduct returnOrderProduct;
/*	
	@ManyToOne
	@JoinColumn(name="transportation_id")
	private Transportation transportation;
	
	@Column(name="vehicle_no")
	private String vehicleNo;
	
	@Column(name="docket_no")
	private String docketNo;*/

	public long getReIssueOrderId() {
		return reIssueOrderId;
	}

	public void setReIssueOrderId(long reIssueOrderId) {
		this.reIssueOrderId = reIssueOrderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getReIssueDate() {
		return reIssueDate;
	}

	public void setReIssueDate(Date reIssueDate) {
		this.reIssueDate = reIssueDate;
	}

	public Date getReIssueDeliveryDate() {
		return reIssueDeliveryDate;
	}

	public void setReIssueDeliveryDate(Date reIssueDeliveryDate) {
		this.reIssueDeliveryDate = reIssueDeliveryDate;
	}

	public Date getReIssueDeliveredDate() {
		return reIssueDeliveredDate;
	}

	public void setReIssueDeliveredDate(Date reIssueDeliveredDate) {
		this.reIssueDeliveredDate = reIssueDeliveredDate;
	}

	public Employee getEmployeeSMDB() {
		return employeeSMDB;
	}

	public void setEmployeeSMDB(Employee employeeSMDB) {
		this.employeeSMDB = employeeSMDB;
	}

	public Employee getEmployeeGK() {
		return employeeGK;
	}

	public void setEmployeeGK(Employee employeeGK) {
		this.employeeGK = employeeGK;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}

	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetails [reIssueOrderId=" + reIssueOrderId + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", reIssueDate="
				+ reIssueDate + ", reIssueDeliveryDate=" + reIssueDeliveryDate + ", reIssueDeliveredDate="
				+ reIssueDeliveredDate + ", employeeSMDB=" + employeeSMDB + ", employeeGK=" + employeeGK + ", status="
				+ status + ", returnOrderProduct=" + returnOrderProduct + "]";
	}

/*	public Transportation getTransportation() {
		return transportation;
	}

	public void setTransportation(Transportation transportation) {
		this.transportation = transportation;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}
	@Override
	public String toString() {
		return "ReIssueOrderDetails [reIssueOrderId=" + reIssueOrderId + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", reIssueDate="
				+ reIssueDate + ", reIssueDeliveryDate=" + reIssueDeliveryDate + ", reIssueDeliveredDate="
				+ reIssueDeliveredDate + ", employeeSMDB=" + employeeSMDB + ", employeeGK=" + employeeGK + ", status="
				+ status + ", returnOrderProduct=" + returnOrderProduct + ", transportation=" + transportation
				+ ", vehicleNo=" + vehicleNo + ", docketNo=" + docketNo + "]";
	}*/


	
	
}
