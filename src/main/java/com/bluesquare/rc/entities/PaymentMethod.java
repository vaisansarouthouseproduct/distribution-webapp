package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * <pre>
 * @author Sachin Sharma 17-07-2018	Code Documentation
 * <pre>
 * Class representation of @Table(name = "payment_method")
 */

@Entity
@Table(name="payment_method")
@Component
public class PaymentMethod {
	
	@Id
	@Column(name = "payment_method_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long paymentMethodId;
	
	@Column(name="payment_method_name")
	private String paymentMethodName;

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	@Override
	public String toString() {
		return "PaymentMethod [paymentMethodId=" + paymentMethodId + ", paymentMethodName=" + paymentMethodName + "]";
	}
	
	

}
