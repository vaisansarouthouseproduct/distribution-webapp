package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "commission_assign")
@Component
public class CommissionAssign {

	@Id
	@Column(name = "commission_assign_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long commissionAssignId;

	@ManyToOne
	@JoinColumn(name = "target_type_id")
	private TargetType targetType;

	@Column(name = "target_period")
	private String targetPeriod;

	@Column(name = "commision_assign_name")
	private String commissionAssignName;

	@Column(name = "amount_or_qty_type_slab")
	private String amountOrQtyTypeSlab;

	@Column(name = "added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDatetime;

	@Column(name = "updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updatedDatetime;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "includeTax")
	private boolean includeTax;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "cascading_commission")
	private boolean cascadingCommission;

	/*@Column(name = "prodcut_slab_type")
	private String prodcutSlabType;*/

	public long getCommissionAssignId() {
		return commissionAssignId;
	}

	public void setCommissionAssignId(long commissionAssignId) {
		this.commissionAssignId = commissionAssignId;
	}

	public TargetType getTargetType() {
		return targetType;
	}

	public void setTargetType(TargetType targetType) {
		this.targetType = targetType;
	}

	public String getTargetPeriod() {
		return targetPeriod;
	}

	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}

	public String getCommissionAssignName() {
		return commissionAssignName;
	}

	public void setCommissionAssignName(String commissionAssignName) {
		this.commissionAssignName = commissionAssignName;
	}

	public Date getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(Date addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isIncludeTax() {
		return includeTax;
	}

	public void setIncludeTax(boolean includeTax) {
		this.includeTax = includeTax;
	}

	public boolean isCascadingCommission() {
		return cascadingCommission;
	}

	public void setCascadingCommission(boolean cascadingCommission) {
		this.cascadingCommission = cascadingCommission;
	}

	public String getAmountOrQtyTypeSlab() {
		return amountOrQtyTypeSlab;
	}

	public void setAmountOrQtyTypeSlab(String amountOrQtyTypeSlab) {
		this.amountOrQtyTypeSlab = amountOrQtyTypeSlab;
	}

/*	public String getProdcutSlabType() {
		return prodcutSlabType;
	}

	public void setProdcutSlabType(String prodcutSlabType) {
		this.prodcutSlabType = prodcutSlabType;
	}*/

	@Override
	public String toString() {
		return "CommissionAssign [commissionAssignId=" + commissionAssignId + ", targetType=" + targetType
				+ ", targetPeriod=" + targetPeriod + ", commissionAssignName=" + commissionAssignName
				+ ", amountOrQtyTypeSlab=" + amountOrQtyTypeSlab + ", addedDatetime=" + addedDatetime
				+ ", updatedDatetime=" + updatedDatetime + ", company=" + company + ", status=" + status
				+ ", includeTax=" + includeTax + ", cascadingCommission=" + cascadingCommission +  "]";
	}

}
