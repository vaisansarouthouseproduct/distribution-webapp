package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "counter_order")
@Component
public class CounterOrder {

	@Id
	@Column(name = "counter_order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long counterOrderPKId;

	@Column(name = "counter_order_id")
	private String counterOrderId;

	@ManyToOne
	@JoinColumn(name = "employee_gk_id")
	private Employee employeeGk;

	@ManyToOne
	@JoinColumn(name = "business_id")
	private BusinessName businessName;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_mob_number")
	private String customerMobileNumber;

	@Column(name = "customer_gst_number")
	private String customerGstNumber;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;

	@Column(name = "date_of_order_taken")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfOrderTaken;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "paid_status")
	private boolean payStatus;

	@Column(name = "payment_due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paymentDueDate;

	@Column(name = "invoice_number")
	private String invoiceNumber;

	@ManyToOne
	@JoinColumn(name = "order_status_id")
	private OrderStatus orderStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "bad_debts_status")
	private boolean badDebtsStatus;

	@Column(name = "total_amount_due_baddebts", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountBadDebtsDue;

	@Column(name = "bad_Debts_disable_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date badDebtsDatetime;

	@ManyToOne
	@JoinColumn(name = "transportation_id")
	private Transportation transportation;

	@Column(name = "vehicle_no")
	private String vehicleNo;

	@Column(name = "docket_no")
	private String docketNo;

	@Column(name = "transportation_charges", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double transportationCharges;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;

	@Column(name = "discount_type")
	private String discountType;

	@Column(name = "discount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double discount;

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public long getCounterOrderPKId() {
		return counterOrderPKId;
	}

	public void setCounterOrderPKId(long counterOrderPKId) {
		this.counterOrderPKId = counterOrderPKId;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public Employee getEmployeeGk() {
		return employeeGk;
	}

	public void setEmployeeGk(Employee employeeGk) {
		this.employeeGk = employeeGk;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerGstNumber() {
		return customerGstNumber;
	}

	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getDateOfOrderTaken() {
		return dateOfOrderTaken;
	}

	public void setDateOfOrderTaken(Date dateOfOrderTaken) {
		this.dateOfOrderTaken = dateOfOrderTaken;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Transportation getTransportation() {
		return transportation;
	}

	public void setTransportation(Transportation transportation) {
		this.transportation = transportation;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public boolean getBadDebtsStatus() {
		return badDebtsStatus;
	}

	public void setBadDebtsStatus(boolean badDebtsStatus) {
		this.badDebtsStatus = badDebtsStatus;
	}

	public double getTotalAmountBadDebtsDue() {
		return totalAmountBadDebtsDue;
	}

	public void setTotalAmountBadDebtsDue(double totalAmountBadDebtsDue) {
		this.totalAmountBadDebtsDue = totalAmountBadDebtsDue;
	}

	public Date getBadDebtsDatetime() {
		return badDebtsDatetime;
	}

	public void setBadDebtsDatetime(Date badDebtsDatetime) {
		this.badDebtsDatetime = badDebtsDatetime;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTransportationCharges() {
		return transportationCharges;
	}

	public void setTransportationCharges(double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	@Override
	public String toString() {
		return "CounterOrder [counterOrderPKId=" + counterOrderPKId + ", counterOrderId=" + counterOrderId
				+ ", employeeGk=" + employeeGk + ", businessName=" + businessName + ", customerName=" + customerName
				+ ", customerMobileNumber=" + customerMobileNumber + ", customerGstNumber=" + customerGstNumber
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity="
				+ totalQuantity + ", dateOfOrderTaken=" + dateOfOrderTaken + ", payStatus=" + payStatus
				+ ", paymentDueDate=" + paymentDueDate + ", invoiceNumber=" + invoiceNumber + ", orderStatus="
				+ orderStatus + ", badDebtsStatus=" + badDebtsStatus + ", totalAmountBadDebtsDue="
				+ totalAmountBadDebtsDue + ", badDebtsDatetime=" + badDebtsDatetime + ", transportation="
				+ transportation + ", vehicleNo=" + vehicleNo + ", docketNo=" + docketNo + ", transportationCharges="
				+ transportationCharges + ", branch=" + branch + ", discountType=" + discountType + ", discount="
				+ discount + "]";
	}

}
