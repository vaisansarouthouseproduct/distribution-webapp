package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "supplier_order_details")
@Component
public class SupplierOrderDetails {

	@Id
	@Column(name = "supplier_order_details_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long supplierOrderDetailsId;
	
	@ManyToOne
	@JoinColumn(name="supplier_id")
	private Supplier supplier;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private OrderUsedProduct product;
	
	@Column(name="quantity")
	private long quantity;
	
	@Column(name="supplier_rate", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double supplierRate;
	
	@Column(name="total_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmount;
	
	@Column(name="total_amount_with_tax", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmountWithTax;
	
	@ManyToOne
	@JoinColumn(name="supplier_order_id")
	private SupplierOrder supplierOrder;

	public long getSupplierOrderDetailsId() {
		return supplierOrderDetailsId;
	}

	public void setSupplierOrderDetailsId(long supplierOrderDetailsId) {
		this.supplierOrderDetailsId = supplierOrderDetailsId;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getSupplierRate() {
		return supplierRate;
	}

	public void setSupplierRate(double supplierRate) {
		this.supplierRate = supplierRate;
	}

	public SupplierOrder getSupplierOrder() {
		return supplierOrder;
	}

	public void setSupplierOrder(SupplierOrder supplierOrder) {
		this.supplierOrder = supplierOrder;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	@Override
	public String toString() {
		return "SupplierOrderDetails [supplierOrderDetailsId=" + supplierOrderDetailsId + ", supplier=" + supplier
				+ ", product=" + product + ", quantity=" + quantity + ", supplierRate=" + supplierRate
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", supplierOrder="
				+ supplierOrder + "]";
	}
	
}
