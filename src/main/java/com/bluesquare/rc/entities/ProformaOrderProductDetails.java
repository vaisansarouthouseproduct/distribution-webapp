package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "proforma_order_product_details")
@Component
public class ProformaOrderProductDetails {

	@Id
	@Column(name = "proforma_order_product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long proformaOrderProductId;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;

	@Column(name = "purchase_quantity")
	private long purchaseQuantity;
	
	/*@Column(name = "return_quantity")
	private long returnQuantity;*/

	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double sellingRate;

	@Column(name = "purchase_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double purchaseAmount;

	@Column(name = "type")
	private String type;

	@Column(name = "discount_type")
	private String discountType;

	@Column(name = "discount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double discount;

	@Column(name = "discount_per", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double discountPer;

	/*
	 * @Column(name = "discount_type_old") private String discountTypeOld;
	 * 
	 * @Column(name = "discount_old", precision = 19, scale = 2,
	 * columnDefinition = "DECIMAL(19,2)") private double discountOld;
	 * 
	 * @Column(name = "discount_per_old", precision = 19, scale = 2,
	 * columnDefinition = "DECIMAL(19,2)") private double discountPerOld;
	 * 
	 * @Column(name = "purchase_amount_old", precision = 19, scale = 2,
	 * columnDefinition = "DECIMAL(19,2)") private double purchaseAmountOld;
	 */

	@ManyToOne
	@JoinColumn(name = "proforma_order_id")
	private ProformaOrder proformaOrder;

	public long getProformaOrderProductId() {
		return proformaOrderProductId;
	}

	public void setProformaOrderProductId(long proformaOrderProductId) {
		this.proformaOrderProductId = proformaOrderProductId;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public long getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(long purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}


	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getDiscountPer() {
		return discountPer;
	}

	public void setDiscountPer(double discountPer) {
		this.discountPer = discountPer;
	}

	public ProformaOrder getProformaOrder() {
		return proformaOrder;
	}

	public void setProformaOrder(ProformaOrder proformaOrder) {
		this.proformaOrder = proformaOrder;
	}

	@Override
	public String toString() {
		return "ProformaOrderProductDetails [proformaOrderProductId=" + proformaOrderProductId + ", product=" + product
				+ ", purchaseQuantity=" + purchaseQuantity + ", sellingRate=" + sellingRate + ", purchaseAmount="
				+ purchaseAmount + ", type=" + type + ", discountType=" + discountType + ", discount=" + discount
				+ ", discountPer=" + discountPer + ", proformaOrder=" + proformaOrder + "]";
	}

	
}
