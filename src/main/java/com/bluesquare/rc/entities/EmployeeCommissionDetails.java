package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_commission_details")
@Component
public class EmployeeCommissionDetails {

	@Id
	@Column(name = "employee_commission_details_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeCommissionDetailsId;

	@Column(name = "completed_value", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double completedValue;

	@Column(name = "commission_value", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double commissionValue;

	@ManyToOne
	@JoinColumn(name = "commission_assign_id")
	private CommissionAssign commissionAssign;

	@ManyToOne
	@JoinColumn(name = "employee_commission_id")
	private EmployeeCommission employeeCommission;

	public long getEmployeeCommissionDetailsId() {
		return employeeCommissionDetailsId;
	}

	public void setEmployeeCommissionDetailsId(long employeeCommissionDetailsId) {
		this.employeeCommissionDetailsId = employeeCommissionDetailsId;
	}

	public double getCompletedValue() {
		return completedValue;
	}

	public void setCompletedValue(double completedValue) {
		this.completedValue = completedValue;
	}

	public double getCommissionValue() {
		return commissionValue;
	}

	public void setCommissionValue(double commissionValue) {
		this.commissionValue = commissionValue;
	}

	public EmployeeCommission getEmployeeCommission() {
		return employeeCommission;
	}

	public void setEmployeeCommission(EmployeeCommission employeeCommission) {
		this.employeeCommission = employeeCommission;
	}

	public CommissionAssign getCommissionAssign() {
		return commissionAssign;
	}

	public void setCommissionAssign(CommissionAssign commissionAssign) {
		this.commissionAssign = commissionAssign;
	}

	@Override
	public String toString() {
		return "EmployeeCommissionDetails [employeeCommissionDetailsId=" + employeeCommissionDetailsId
				+ ", completedValue=" + completedValue + ", commissionValue=" + commissionValue + ", commissionAssign="
				+ commissionAssign + ", employeeCommission=" + employeeCommission + "]";
	}

}
