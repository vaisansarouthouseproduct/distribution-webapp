package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "supplier_product_list")
@Component
public class SupplierProductList {
	@Id
	@Column(name = "supplier_product_list_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long supplierProductListId;
	
	@ManyToOne
	@JoinColumn(name="supplier_id")
	private Supplier supplier;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	@Column(name="supplier_rate", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double supplierRate;

	public long getSupplierProductListId() {
		return supplierProductListId;
	}

	public void setSupplierProductListId(long supplierProductListId) {
		this.supplierProductListId = supplierProductListId;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public double getSupplierRate() {
		return supplierRate;
	}

	public void setSupplierRate(double supplierRate) {
		this.supplierRate = supplierRate;
	}

	@Override
	public String toString() {
		return "SupplierProductList [supplierProductListId=" + supplierProductListId + ", supplier=" + supplier
				+ ", product=" + product + ", supplierRate=" + supplierRate + "]";
	}

	
	
	
	
	
}
