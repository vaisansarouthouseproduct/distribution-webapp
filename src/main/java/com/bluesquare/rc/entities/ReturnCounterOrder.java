package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "return_counter_order")
public class ReturnCounterOrder {

	@Id
	@Column(name = "return_counter_order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long returnCounterOrderId;

	@Column(name = "return_counter_order_id")
	private String returnCounterOrderGenId;

	@ManyToOne
	@JoinColumn(name = "return_emp_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "order_details_id")
	private CounterOrder counterOrder;

	@Column(name = "return_order_product_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfReturn;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;

	@Column(name = "discount_type")
	private String discountType;

	@Column(name = "discount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double discount;

	@Column(name = "invoice_number")
	private String invoiceNumber;

	@Column(name = "comment")
	private String comment;

	public long getReturnCounterOrderId() {
		return returnCounterOrderId;
	}

	public void setReturnCounterOrderId(long returnCounterOrderId) {
		this.returnCounterOrderId = returnCounterOrderId;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public CounterOrder getCounterOrder() {
		return counterOrder;
	}

	public void setCounterOrder(CounterOrder counterOrder) {
		this.counterOrder = counterOrder;
	}

	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getReturnCounterOrderGenId() {
		return returnCounterOrderGenId;
	}

	public void setReturnCounterOrderGenId(String returnCounterOrderGenId) {
		this.returnCounterOrderGenId = returnCounterOrderGenId;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ReturnCounterOrder [returnCounterOrderId=" + returnCounterOrderId + ", returnCounterOrderGenId="
				+ returnCounterOrderGenId + ", employee=" + employee + ", counterOrder=" + counterOrder
				+ ", dateOfReturn=" + dateOfReturn + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", discountType=" + discountType
				+ ", discount=" + discount + ", invoiceNumber=" + invoiceNumber + ", comment=" + comment + "]";
	}

}
