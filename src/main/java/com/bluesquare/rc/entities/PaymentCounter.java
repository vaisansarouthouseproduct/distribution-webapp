package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "payment_counter")
@Component
public class PaymentCounter {
	
	@Id
	@Column(name = "payment_counter_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long paymentCounterId;
	
	/*@Column(name = "total_amount_paid", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountPaid;*/
	
	@Column(name = "current_amount_paid", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double currentAmountPaid;
	
	@Column(name = "current_amount_refund", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double currentAmountRefund;
	
	/*@Column(name = "balance_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double balanceAmount;*/

	@Column(name = "paid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paidDate;

	@Column(name = "due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dueDate;
	
	@Column(name = "last_due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date lastDueDate;
	
	@Column(name = "pay_type")
	private String payType;

	@Column(name = "cheque_number")
	private String chequeNumber;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "cheque_date")
	private Date chequeDate;
	
	@ManyToOne
	@JoinColumn(name = "counter_order_id")
	private CounterOrder counterOrder;
	
	@ManyToOne
	@JoinColumn(name = "employee_id_pay_take")
	private Employee employee;
	
	@ManyToOne
	@JoinColumn(name="payment_method")
	private PaymentMethod paymentMethod;
	
	@Column(name="transaction_ref_no")
	private String transactionRefNo;
	
	@Column(name="comment")
	private String comment;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "cheque_clear_status")
	private boolean chequeClearStatus;

	public long getPaymentCounterId() {
		return paymentCounterId;
	}

	public void setPaymentCounterId(long paymentCounterId) {
		this.paymentCounterId = paymentCounterId;
	}

	/*public double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}*/

	public double getCurrentAmountPaid() {
		return currentAmountPaid;
	}

	public void setCurrentAmountPaid(double currentAmountPaid) {
		this.currentAmountPaid = currentAmountPaid;
	}

	/*public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}*/

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public CounterOrder getCounterOrder() {
		return counterOrder;
	}

	public void setCounterOrder(CounterOrder counterOrder) {
		this.counterOrder = counterOrder;
	}

	public double getCurrentAmountRefund() {
		return currentAmountRefund;
	}

	public void setCurrentAmountRefund(double currentAmountRefund) {
		this.currentAmountRefund = currentAmountRefund;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isChequeClearStatus() {
		return chequeClearStatus;
	}

	public void setChequeClearStatus(boolean chequeClearStatus) {
		this.chequeClearStatus = chequeClearStatus;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "PaymentCounter [paymentCounterId=" + paymentCounterId + ", currentAmountPaid=" + currentAmountPaid
				+ ", currentAmountRefund=" + currentAmountRefund + ", paidDate=" + paidDate + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", payType=" + payType + ", chequeNumber=" + chequeNumber
				+ ", bankName=" + bankName + ", chequeDate=" + chequeDate + ", counterOrder=" + counterOrder
				+ ", employee=" + employee + ", paymentMethod=" + paymentMethod + ", transactionRefNo="
				+ transactionRefNo + ", comment=" + comment + ", status=" + status + ", chequeClearStatus="
				+ chequeClearStatus + "]";
	}

	
	

	/*@Override
	public String toString() {
		return "PaymentCounter [paymentCounterId=" + paymentCounterId + ", totalAmountPaid=" + totalAmountPaid
				+ ", currentAmountPaid=" + currentAmountPaid + ", currentAmountRefund=" + currentAmountRefund
				+ ", balanceAmount=" + balanceAmount + ", paidDate=" + paidDate + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", payType=" + payType + ", chequeNumber=" + chequeNumber
				+ ", bankName=" + bankName + ", chequeDate=" + chequeDate + ", counterOrder=" + counterOrder
				+ ", status=" + status + ", chequeClearStatus=" + chequeClearStatus + "]";
	}*/

	
	
}
