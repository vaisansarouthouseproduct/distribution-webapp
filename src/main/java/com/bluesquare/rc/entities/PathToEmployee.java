package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "path_to_employee")
@Component
public class PathToEmployee {

	@Id
	@Column(name = "path_to_employee_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long pathToEmployeeId;

	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;
	
	@ManyToOne
	@JoinColumn(name = "route_id")
	private Route route;

	public long getPathToEmployeeId() {
		return pathToEmployeeId;
	}

	public void setPathToEmployeeId(long pathToEmployeeId) {
		this.pathToEmployeeId = pathToEmployeeId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "PathToEmployee [pathToEmployeeId=" + pathToEmployeeId + ", employeeDetails=" + employeeDetails
				+ ", route=" + route + "]";
	}
	
	
}
