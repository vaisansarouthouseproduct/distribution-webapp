package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name="daily_stock_details")
public class DailyStockDetails {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	@Column(name="opening_stock")
	private long openingStock;
	
	@Column(name="added_stock")
	private long addedStock;
	
	@Column(name="dispatched_stock")
	private long dispatchedStock;
	
	
	@Column(name="closing_stock")
	private long closingStock;
	
	@Column(name="date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(long openingStock) {
		this.openingStock = openingStock;
	}

	public long getAddedStock() {
		return addedStock;
	}

	public void setAddedStock(long addedStock) {
		this.addedStock = addedStock;
	}

	public long getDispatchedStock() {
		return dispatchedStock;
	}

	public void setDispatchedStock(long dispatchedStock) {
		this.dispatchedStock = dispatchedStock;
	}

	public long getClosingStock() {
		return closingStock;
	}

	public void setClosingStock(long closingStock) {
		this.closingStock = closingStock;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "DailyStockDetails [id=" + id + ", product=" + product + ", openingStock=" + openingStock
				+ ", addedStock=" + addedStock + ", dispatchedStock=" + dispatchedStock + ", closingStock="
				+ closingStock + ", date=" + date + ", branch=" + branch + "]";
	}

	
	
}
