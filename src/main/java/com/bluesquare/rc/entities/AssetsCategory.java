package com.bluesquare.rc.entities;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "asset_category")
@Component
public class AssetsCategory {

	
	@Id
	@Column(name = "asset_category_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long assetCategoryId;

	@Column(name = "asset_name")
	private String assetCategoryName;

	@Column(name = "asset_category_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date productAddedDatetime;

	@Column(name = "asset_category_updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date productQuantityUpdatedDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public long getAssetCategoryId() {
		return assetCategoryId;
	}

	public void setAssetCategoryId(long assetCategoryId) {
		this.assetCategoryId = assetCategoryId;
	}

	public String getAssetCategoryName() {
		return assetCategoryName;
	}

	public void setAssetCategoryName(String assetCategoryName) {
		this.assetCategoryName = assetCategoryName;
	}

	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}

	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}

	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}

	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "AssetsCategory [assetCategoryId=" + assetCategoryId + ", assetCategoryName=" + assetCategoryName
				+ ", productAddedDatetime=" + productAddedDatetime + ", productQuantityUpdatedDatetime="
				+ productQuantityUpdatedDatetime + ", company=" + company + "]";
	}
	
	
	
	
}
