package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "bank_acc_temp")
@Component
public class BankAccountTempTransaction {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="txn_id")
	private String transactionId;
	
	@Column(name="party_name")
	private String partyName;
	
	@Column(name="credit")
	private double credit;
	
	@Column(name="debit")
	private double debit;
	
	@Column(name="pay_mode")
	private String payMode;
	
	@Column(name="ref_no")
	private String referenceNo;
	
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="inserted_date")
	private String insertedDate;
	
	@Column(name="cheque_date")
	private String chequeDate;
	
	@ManyToOne
	@JoinColumn(name="bank_account_id")
	private BankAccount bankAccount;
	
	@Column(name="txn_status")
	private String transactionStatus;
	
	@Column(name="reason")
	private String reason;
	
	@Column(name="updated_date")
	private String updatedDate;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	
	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "BankAccountTempTransaction [id=" + id + ", transactionId=" + transactionId + ", partyName=" + partyName
				+ ", credit=" + credit + ", debit=" + debit + ", payMode=" + payMode + ", referenceNo=" + referenceNo
				+ ", bankName=" + bankName + ", insertedDate=" + insertedDate + ", chequeDate=" + chequeDate
				+ ", bankAccount=" + bankAccount + ", transactionStatus=" + transactionStatus + ", reason=" + reason
				+ ", updatedDate=" + updatedDate + "]";
	}



	
}
