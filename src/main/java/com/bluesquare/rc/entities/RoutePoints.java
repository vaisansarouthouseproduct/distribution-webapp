package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "route_list")
@Component
public class RoutePoints {

	@Id
	@Column(name = "route_points_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long routePointsId;
	
	@Column(name = "route_address")
	private String routeAddress;
	
	@Column(name = "longitude")
	private String longitude;

	@Column(name = "latitude")
	private String latitude;
	
	@ManyToOne
	@JoinColumn(name = "route_id")
	private Route route;

	public long getRoutePointsId() {
		return routePointsId;
	}

	public void setRoutePointsId(long routePointsId) {
		this.routePointsId = routePointsId;
	}

	public String getRouteAddress() {
		return routeAddress;
	}

	public void setRouteAddress(String routeAddress) {
		this.routeAddress = routeAddress;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "RoutePoints [routePointsId=" + routePointsId + ", routeAddress=" + routeAddress + ", longitude="
				+ longitude + ", latitude=" + latitude + ", route=" + route + "]";
	}

	
}
