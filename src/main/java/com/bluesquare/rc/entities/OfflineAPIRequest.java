package com.bluesquare.rc.entities;



import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "offline_api_request")
@Component
public class OfflineAPIRequest {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long id;
	
	@Column(name="added_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDateTime;
	
	
	
	@Column(name = "is_offline_request")
	private String isOfflineRequest;
	
	@Column(name="imei_no")
	private String imeiNumber;
	
	@Column(name="db_id")
	private long dbId;
	
	@Column(name="status")
	private String status;

	@Column(name="task")
	private String taskPerform;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(Date addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	

	public String getIsOfflineRequest() {
		return isOfflineRequest;
	}

	public void setIsOfflineRequest(String isOfflineRequest) {
		this.isOfflineRequest = isOfflineRequest;
	}

	public String getImeiNumber() {
		return imeiNumber;
	}

	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}

	public long getDbId() {
		return dbId;
	}

	public void setDbId(long dbId) {
		this.dbId = dbId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaskPerform() {
		return taskPerform;
	}

	public void setTaskPerform(String taskPerform) {
		this.taskPerform = taskPerform;
	}

	@Override
	public String toString() {
		return "OfflineAPIRequest [id=" + id + ", addedDateTime=" + addedDateTime + ", isOfflineRequest="
				+ isOfflineRequest + ", imeiNumber=" + imeiNumber + ", dbId=" + dbId + ", status=" + status
				+ ", taskPerform=" + taskPerform + "]";
	}


	
	
	
}
