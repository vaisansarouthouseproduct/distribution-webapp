package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "notepad")
@Component

public class NotePad {

	
	@Id
	@Column(name = "notepad_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long notepadId;
	
	@Column(name = "title")
	private String notepadTitle;
	
	
	@Column(name = "text", columnDefinition = "LONGTEXT")
	private String notepadText;
	
	@ManyToOne
    @JoinColumn(name = "employee_id")
	private Employee employee;
	
	
	@Column(name = "notepad_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date notepadAddedDateTime;
	
	@Column(name = "notepad_updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date notepadUpdatedDateTime;

	public long getNotepadId() {
		return notepadId;
	}

	public void setNotepadId(long notepadId) {
		this.notepadId = notepadId;
	}

	public String getNotepadTitle() {
		return notepadTitle;
	}

	public void setNotepadTitle(String notepadTitle) {
		this.notepadTitle = notepadTitle;
	}

	public String getNotepadText() {
		return notepadText;
	}

	public void setNotepadText(String notepadText) {
		this.notepadText = notepadText;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getNotepadAddedDateTime() {
		return notepadAddedDateTime;
	}

	public void setNotepadAddedDateTime(Date notepadAddedDateTime) {
		this.notepadAddedDateTime = notepadAddedDateTime;
	}

	public Date getNotepadUpdatedDateTime() {
		return notepadUpdatedDateTime;
	}

	public void setNotepadUpdatedDateTime(Date notepadUpdatedDateTime) {
		this.notepadUpdatedDateTime = notepadUpdatedDateTime;
	}

	@Override
	public String toString() {
		return "NotePad [notepadId=" + notepadId + ", notepadTitle=" + notepadTitle + ", notepadText=" + notepadText
				+ ", employee=" + employee + ", notepadAddedDateTime=" + notepadAddedDateTime
				+ ", notepadUpdatedDateTime=" + notepadUpdatedDateTime + "]";
	}


	
	
}
