package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "order_product_details")
@Component

public class OrderProductDetails {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderProductDetailsid;

	@Column(name = "purchase_quantity")
	private long purchaseQuantity;

	@Column(name = "details")
	private String details;

	@Column(name = "issued_quantity")
	private long issuedQuantity;

	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double sellingRate;

	@Column(name = "purchase_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double purchaseAmount;

	@Column(name = "issue_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double issueAmount;

	@Column(name = "confirm_quantity")
	private long confirmQuantity;

	@Column(name = "confirm_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double confirmAmount;

	@ManyToOne
	@JoinColumn(name = "order_details_id")
	private OrderDetails orderDetails;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;

	@Column(name = "type")
	private String type;

	public long getOrderProductDetailsid() {
		return orderProductDetailsid;
	}

	public void setOrderProductDetailsid(long orderProductDetailsid) {
		this.orderProductDetailsid = orderProductDetailsid;
	}

	public long getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(long purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public double getIssueAmount() {
		return issueAmount;
	}

	public void setIssueAmount(double issueAmount) {
		this.issueAmount = issueAmount;
	}

	public long getConfirmQuantity() {
		return confirmQuantity;
	}

	public void setConfirmQuantity(long confirmQuantity) {
		this.confirmQuantity = confirmQuantity;
	}

	public double getConfirmAmount() {
		return confirmAmount;
	}

	public void setConfirmAmount(double confirmAmount) {
		this.confirmAmount = confirmAmount;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "OrderProductDetails [orderProductDetailsid=" + orderProductDetailsid + ", purchaseQuantity="
				+ purchaseQuantity + ", details=" + details + ", issuedQuantity=" + issuedQuantity + ", sellingRate="
				+ sellingRate + ", purchaseAmount=" + purchaseAmount + ", issueAmount=" + issueAmount
				+ ", confirmQuantity=" + confirmQuantity + ", confirmAmount=" + confirmAmount + ", orderDetails="
				+ orderDetails + ", product=" + product + ", type=" + type + "]";
	}

}
