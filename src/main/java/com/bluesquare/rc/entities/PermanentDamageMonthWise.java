package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "permanent_damage_month_wise")
@Component

public class PermanentDamageMonthWise {
	@Id
	@Column(name = "permanent_damage_month_wise_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long permanentDamageMonthWiseId;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "total_qty_damage")
	private long totalQuantityDamage;

	@Column(name = "damage_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date damageDate;

	public long getPermanentDamageMonthWiseId() {
		return permanentDamageMonthWiseId;
	}

	public void setPermanentDamageMonthWiseId(long permanentDamageMonthWiseId) {
		this.permanentDamageMonthWiseId = permanentDamageMonthWiseId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getTotalQuantityDamage() {
		return totalQuantityDamage;
	}

	public void setTotalQuantityDamage(long totalQuantityDamage) {
		this.totalQuantityDamage = totalQuantityDamage;
	}

	public Date getDamageDate() {
		return damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	@Override
	public String toString() {
		return "PermanentDamageMonthWise [permanentDamageMonthWiseId=" + permanentDamageMonthWiseId + ", product="
				+ product + ", totalQuantityDamage=" + totalQuantityDamage + ", damageDate=" + damageDate + "]";
	}

}
