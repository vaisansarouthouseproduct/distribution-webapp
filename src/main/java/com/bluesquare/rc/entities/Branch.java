package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "branch")
@Component
public class Branch {

	@Id
	@Column(name = "branch_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long branchId;

	@Column(name = "name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@Column(name = "balance", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double balance;

	@Column(name = "code")
	private String code;

	@Column(name = "address")
	private String address;

	@Column(name = "address1")
	private String address1;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "gst_no")
	private String gstNo;

	@Column(name = "pan_no")
	private String panNo;

	@Column(name = "mob_no")
	private String mobNo;

	@Column(name = "phone_no")
	private String phoneNo;

	@Column(name = "email")
	private String email;

	@Column(name = "website")
	private String website;

	@Column(name = "state")
	private String state;

	@Column(name = "state_code")
	private String stateCode;

	@Column(name = "terms")
	private String terms;

	@Column(name = "bill_icon")
	private String billIcon;

	@Column(name = "gst_support")
	private boolean gstSupport = true;

	@Column(name = "min_invoice_row")
	private int minInvoiceCount;

	@Column(name = "display_bank_details")
	private boolean displayBankDetails = true;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "acc_name")
	private String accName;

	@Column(name = "acc_no")
	private String accNo;

	@Column(name = "ifsc_code")
	private String ifscCode;

	@Column(name = "display_bank_details1")
	private boolean displayBankDetails1 = true;

	@Column(name = "bank_name1")
	private String bankName1;

	@Column(name = "acc_name1")
	private String accName1;

	@Column(name = "acc_no1")
	private String accNo1;

	@Column(name = "ifsc_code1")
	private String ifscCode1;

	@Column(name = "small_bill")
	private boolean smallBill = false;

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getBillIcon() {
		return billIcon;
	}

	public void setBillIcon(String billIcon) {
		this.billIcon = billIcon;
	}

	public boolean isGstSupport() {
		return gstSupport;
	}

	public void setGstSupport(boolean gstSupport) {
		this.gstSupport = gstSupport;
	}

	public int getMinInvoiceCount() {
		return minInvoiceCount;
	}

	public void setMinInvoiceCount(int minInvoiceCount) {
		this.minInvoiceCount = minInvoiceCount;
	}

	public boolean isDisplayBankDetails() {
		return displayBankDetails;
	}

	public void setDisplayBankDetails(boolean displayBankDetails) {
		this.displayBankDetails = displayBankDetails;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public boolean isDisplayBankDetails1() {
		return displayBankDetails1;
	}

	public void setDisplayBankDetails1(boolean displayBankDetails1) {
		this.displayBankDetails1 = displayBankDetails1;
	}

	public String getBankName1() {
		return bankName1;
	}

	public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}

	public String getAccName1() {
		return accName1;
	}

	public void setAccName1(String accName1) {
		this.accName1 = accName1;
	}

	public String getAccNo1() {
		return accNo1;
	}

	public void setAccNo1(String accNo1) {
		this.accNo1 = accNo1;
	}

	public String getIfscCode1() {
		return ifscCode1;
	}

	public void setIfscCode1(String ifscCode1) {
		this.ifscCode1 = ifscCode1;
	}

	public boolean isSmallBill() {
		return smallBill;
	}

	public void setSmallBill(boolean smallBill) {
		this.smallBill = smallBill;
	}

	@Override
	public String toString() {
		return "Branch [branchId=" + branchId + ", name=" + name + ", company=" + company + ", balance=" + balance
				+ ", code=" + code + ", address=" + address + ", address1=" + address1 + ", companyName=" + companyName
				+ ", gstNo=" + gstNo + ", panNo=" + panNo + ", mobNo=" + mobNo + ", phoneNo=" + phoneNo + ", email="
				+ email + ", website=" + website + ", state=" + state + ", stateCode=" + stateCode + ", terms=" + terms
				+ ", billIcon=" + billIcon + ", gstSupport=" + gstSupport + ", minInvoiceCount=" + minInvoiceCount
				+ ", displayBankDetails=" + displayBankDetails + ", bankName=" + bankName + ", accName=" + accName
				+ ", accNo=" + accNo + ", ifscCode=" + ifscCode + ", displayBankDetails1=" + displayBankDetails1
				+ ", bankName1=" + bankName1 + ", accName1=" + accName1 + ", accNo1=" + accNo1 + ", ifscCode1="
				+ ifscCode1 + ", smallBill=" + smallBill + "]";
	}

}
