package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_areas")
@Component
public class EmployeeAreaList {

	@Id
	@Column(name = "employee_areas_details_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long employeeAreaListId;
	
	@ManyToOne
    @JoinColumn(name = "area_id")
	private Area area;
	
	@ManyToOne
    @JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;
	
	public long getEmployeeAreaListId() {
		return employeeAreaListId;
	}

	public void setEmployeeAreaListId(long employeeAreaListId) {
		this.employeeAreaListId = employeeAreaListId;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	@Override
	public String toString() {
		return "EmployeeAreaList [employeeAreaListId=" + employeeAreaListId + ", area=" + area + ", employeeDetails="
				+ employeeDetails + "]";
	}
}
