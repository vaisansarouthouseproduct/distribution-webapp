package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="app_version")
@Component
public class AppVersion {

	@Id
	@Column(name = "app_version_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long appVersionId;
	
	@Column(name = "app_version")
	private String appVersion;

	public long getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(long appVersionId) {
		this.appVersionId = appVersionId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	@Override
	public String toString() {
		return "AppVersion [appVersionId=" + appVersionId + ", appVersion=" + appVersion + "]";
	}
	
	
	
}
