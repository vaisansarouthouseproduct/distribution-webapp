package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "shop_visit_image")
@Component
public class ShopVisitImages {
	@Id
	@Column(name = "shop_visit_image_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long shopVisitImagesId;

	@ManyToOne
	@JoinColumn(name = "shop_visit_id")
	private ShopVisit shopVisit;

	@Lob
	@Column(name = "shop_image", columnDefinition = "LONGTEXT")
	private String shopImage;

	public long getShopVisitImagesId() {
		return shopVisitImagesId;
	}

	public void setShopVisitImagesId(long shopVisitImagesId) {
		this.shopVisitImagesId = shopVisitImagesId;
	}

	public ShopVisit getShopVisit() {
		return shopVisit;
	}

	public void setShopVisit(ShopVisit shopVisit) {
		this.shopVisit = shopVisit;
	}

	public String getShopImage() {
		return shopImage;
	}

	public void setShopImage(String shopImage) {
		this.shopImage = shopImage;
	}

	@Override
	public String toString() {
		return "ShopVisitImages [shopVisitImagesId=" + shopVisitImagesId + ", shopVisit=" + shopVisit + ", shopImage="
				+ shopImage + "]";
	}

}
