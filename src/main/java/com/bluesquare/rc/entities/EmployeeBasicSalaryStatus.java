/**
 * 
 */
package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

/**
 * @author aNKIT
 *
 */
@Entity
@Table(name = "employee_basic_salary_status")
@Component
public class EmployeeBasicSalaryStatus {

	@Id
	@Column(name = "employee_basic_salary_status_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeBasicSalaryStatusId;
	
	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;
	
	@Column(name = "basic_salary", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double basicSalary;
	
	@Column(name="start_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	
	@Column(name="end_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;

	

	public long getEmployeeBasicSalaryStatusId() {
		return employeeBasicSalaryStatusId;
	}

	public void setEmployeeBasicSalaryStatusId(long employeeBasicSalaryStatusId) {
		this.employeeBasicSalaryStatusId = employeeBasicSalaryStatusId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "EmployeeBasicSalaryStatus [employeeBasicSalaryStatusId=" + employeeBasicSalaryStatusId
				+ ", employeeDetails=" + employeeDetails + ", basicSalary=" + basicSalary + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}

	

	
	
	
}
