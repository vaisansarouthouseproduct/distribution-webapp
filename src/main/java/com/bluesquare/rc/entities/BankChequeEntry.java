package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name="bank_cheque_book_entry")
@Component
public class BankChequeEntry {
	
	@Id
	@Column(name="bank_cheque_book_entry_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="cheque_no")
	private String chequeNo;
	
	@Column(name="cheque_amount")
	private double chequeAmt;
	
	@Column(name="cheque_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date chequeDate;
	
	@ManyToOne
	@JoinColumn(name="payee_id")
	private BankPayeeDetails bankPayeeDetails;
	
	@ManyToOne
	@JoinColumn(name="bank_cheque_book_id")
	private BankChequeBook bankChequeBook;
	
	@Column(name="payee_name")
	private String payeeName;
	
	//cheque status cancel or proceed 
	@Column(name="cheque_status")
	private String chequeStatus;

	@Column(name="remark")
	private String remark;
	
	@Column(name="bank_cheque_book_entry_added_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date chequeAddedDate;
	
	@Column(name="is_emi_cheque")
	private String isEMICheque;
	
	
	@Column(name="cancel_reason")
	private String cancelReason;
	
	@Column(name="cancel_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date cancelDate;
	
	
	//cheque type mean A/c payee or Bearer
	@Column(name="cheque_type")
	private String chequeType;
	
	//print type i.e cheque manual print or print from cheque book
	@Column(name="print_type")
	private String printType;
	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getChequeNo() {
		return chequeNo;
	}


	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}


	public double getChequeAmt() {
		return chequeAmt;
	}


	public void setChequeAmt(double chequeAmt) {
		this.chequeAmt = chequeAmt;
	}


	public Date getChequeDate() {
		return chequeDate;
	}


	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}


	public BankPayeeDetails getBankPayeeDetails() {
		return bankPayeeDetails;
	}


	public void setBankPayeeDetails(BankPayeeDetails bankPayeeDetails) {
		this.bankPayeeDetails = bankPayeeDetails;
	}


	public BankChequeBook getBankChequeBook() {
		return bankChequeBook;
	}


	public void setBankChequeBook(BankChequeBook bankChequeBook) {
		this.bankChequeBook = bankChequeBook;
	}


	public String getPayeeName() {
		return payeeName;
	}


	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}


	public String getChequeStatus() {
		return chequeStatus;
	}


	public void setChequeStatus(String chequeStatus) {
		this.chequeStatus = chequeStatus;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Date getChequeAddedDate() {
		return chequeAddedDate;
	}


	public void setChequeAddedDate(Date chequeAddedDate) {
		this.chequeAddedDate = chequeAddedDate;
	}


	public String getChequeType() {
		return chequeType;
	}


	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}
	
	
	public String getPrintType() {
		return printType;
	}


	public void setPrintType(String printType) {
		this.printType = printType;
	}


	
	public Date getCancelDate() {
		return cancelDate;
	}


	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	


	public String getCancelReason() {
		return cancelReason;
	}


	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	
	


	public String getIsEMICheque() {
		return isEMICheque;
	}


	public void setIsEMICheque(String isEMICheque) {
		this.isEMICheque = isEMICheque;
	}


	@Override
	public String toString() {
		return "BankChequeEntry [id=" + id + ", chequeNo=" + chequeNo + ", chequeAmt=" + chequeAmt + ", chequeDate="
				+ chequeDate + ", bankPayeeDetails=" + bankPayeeDetails + ", bankChequeBook=" + bankChequeBook
				+ ", payeeName=" + payeeName + ", chequeStatus=" + chequeStatus + ", remark=" + remark
				+ ", chequeAddedDate=" + chequeAddedDate + ", isEMICheque=" + isEMICheque + ", cancelReason="
				+ cancelReason + ", cancelDate=" + cancelDate + ", chequeType=" + chequeType + ", printType="
				+ printType + "]";
	}


	

	
	

}

