package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeHolidays.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeHolidays.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "target_assign")
@Component
public class TargetAssign {

	@Id
	@Column(name = "target_assign_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long targetAssignId;

	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;

	@Column(name = "added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDatetime;

	@Column(name = "updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updatedDatetime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public long getTargetAssignId() {
		return targetAssignId;
	}

	public void setTargetAssignId(long targetAssignId) {
		this.targetAssignId = targetAssignId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public Date getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(Date addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TargetAssign [targetAssignId=" + targetAssignId + ", employeeDetails=" + employeeDetails
				+ ", addedDatetime=" + addedDatetime + ", updatedDatetime=" + updatedDatetime + ", status=" + status
				+ ", branch=" + branch + "]";
	}

}
