package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "cancel_reason")
@Component
public class CancelReason {
	
	@Id
	@Column(name = "cancel_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long cancelId;
	
	@Column(name = "reason")
	private String reason;

	public long getCancelId() {
		return cancelId;
	}

	public void setCancelId(long cancelId) {
		this.cancelId = cancelId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "CancelReason [cancelId=" + cancelId + ", reason=" + reason + "]";
	}
	
	

}
