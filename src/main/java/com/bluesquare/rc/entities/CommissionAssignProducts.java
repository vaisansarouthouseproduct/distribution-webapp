package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "commission_assign_products")
@Component
public class CommissionAssignProducts {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@ManyToOne
	@JoinColumn(name = "commission_assign_id")
	private CommissionAssign commissionAssign;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public CommissionAssign getCommissionAssign() {
		return commissionAssign;
	}

	public void setCommissionAssign(CommissionAssign commissionAssign) {
		this.commissionAssign = commissionAssign;
	}

	@Override
	public String toString() {
		return "CommissionAssignProducts [id=" + id + ", product=" + product + ", commissionAssign=" + commissionAssign
				+ "]";
	}

}
