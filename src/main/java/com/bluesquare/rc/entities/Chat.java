package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "chat")
@Component
public class Chat {
	@Id
	@Column(name = "chat_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long chatId;

	@Column(name = "msg")
	private String msg;

	@Column(name = "img_base64", columnDefinition = "LONGTEXT")
	private String strBase64;

	@Column(name = "type")
	private String type;

	@ManyToOne
	@JoinColumn(name = "employee_id_from")
	private Employee employeeFrom;

	@ManyToOne
	@JoinColumn(name = "employee_id_to")
	private Employee employeeTo;

	@Column(name = "date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateTime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "delete_status")
	private boolean deleteStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "today_first_record")
	private boolean todayFirstRecord;

	public long getChatId() {
		return chatId;
	}

	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Employee getEmployeeFrom() {
		return employeeFrom;
	}

	public void setEmployeeFrom(Employee employeeFrom) {
		this.employeeFrom = employeeFrom;
	}

	public Employee getEmployeeTo() {
		return employeeTo;
	}

	public void setEmployeeTo(Employee employeeTo) {
		this.employeeTo = employeeTo;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(boolean deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public String getStrBase64() {
		return strBase64;
	}

	public void setStrBase64(String strBase64) {
		this.strBase64 = strBase64;
	}

	public boolean isTodayFirstRecord() {
		return todayFirstRecord;
	}

	public void setTodayFirstRecord(boolean todayFirstRecord) {
		this.todayFirstRecord = todayFirstRecord;
	}

	@Override
	public String toString() {
		return "Chat [chatId=" + chatId + ", msg=" + msg + ", strBase64=" + strBase64 + ", type=" + type
				+ ", employeeFrom=" + employeeFrom + ", employeeTo=" + employeeTo + ", dateTime=" + dateTime
				+ ", deleteStatus=" + deleteStatus + ", todayFirstRecord=" + todayFirstRecord + "]";
	}
}
