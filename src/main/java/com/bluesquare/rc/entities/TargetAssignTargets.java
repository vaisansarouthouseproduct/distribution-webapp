package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "target_assign_targets")
@Component
public class TargetAssignTargets {

	@Id
	@Column(name = "target_assign_target_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long targetAssignTargetsId;

	@ManyToOne
	@JoinColumn(name = "commission_assign_id")
	private CommissionAssign commissionAssign;

	@Column(name = "target_value", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double targetValue;

	@ManyToOne
	@JoinColumn(name = "target_assign_id")
	private TargetAssign targetAssign;

	public long getTargetAssignTargetsId() {
		return targetAssignTargetsId;
	}

	public void setTargetAssignTargetsId(long targetAssignTargetsId) {
		this.targetAssignTargetsId = targetAssignTargetsId;
	}

	public CommissionAssign getCommissionAssign() {
		return commissionAssign;
	}

	public void setCommissionAssign(CommissionAssign commissionAssign) {
		this.commissionAssign = commissionAssign;
	}

	public double getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(double targetValue) {
		this.targetValue = targetValue;
	}

	public TargetAssign getTargetAssign() {
		return targetAssign;
	}

	public void setTargetAssign(TargetAssign targetAssign) {
		this.targetAssign = targetAssign;
	}

	@Override
	public String toString() {
		return "TargetAssignTargets [targetAssignTargetsId=" + targetAssignTargetsId + ", commissionAssign="
				+ commissionAssign + ", targetValue=" + targetValue + ", targetAssign=" + targetAssign + "]";
	}

}
