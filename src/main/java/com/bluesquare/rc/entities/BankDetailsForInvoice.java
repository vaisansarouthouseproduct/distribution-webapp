package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bank_details_for_invoice")

public class BankDetailsForInvoice {

	@Id
	@Column(name = "bank_details_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long bankDetailsId;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "account_name")
	private String accountName;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "ifsc")
	private String ifsc;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public long getBankDetailsId() {
		return bankDetailsId;
	}

	public void setBankDetailsId(long bankDetailsId) {
		this.bankDetailsId = bankDetailsId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "BankDetailsForInvoice [bankDetailsId=" + bankDetailsId + ", bankName=" + bankName + ", accountName="
				+ accountName + ", accountNumber=" + accountNumber + ", ifsc=" + ifsc + ", company=" + company + "]";
	}

}
