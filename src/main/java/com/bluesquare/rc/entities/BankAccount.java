package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "bank_account")
@Component
public class BankAccount {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="acc_holder_name")
	private String accountHolderName;
	
	@Column(name="acc_number")
	private String accountNumber;
	
	@Column(name="acc_type")
	private String accountType;
	
	
	@ManyToOne
	@JoinColumn(name="bank_id")
	private Bank bank;
	
	@Column(name="ifsc_code")
	private String ifscCode;
	
	@Column(name="branch_code")
	private String branchCode;
	
	@Column(name="bank_address")
	private String bankAddress;
	
	@Column(name="min_balance")
	private double minimumBalance;
	
	@Column(name="actual_balance")
	private double actualBalance;
	
	@Column(name="drawable_balance")
	private double drawableBalance;

	@Column(name="ch_issued_amt")
	private double chequeIssuedAmount;
	
	@Column(name="ch_deposited_amt")
	private double chequeDepositedAmount;
	
	@Column(name="inserted_date")
	private String insertedDate;
	
	@Column(name="updated_date")
	private String updatedDate;
	
	@Column(name="status")
	private boolean status;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	@Column(name="is_primary")
	private boolean primary;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public double getMinimumBalance() {
		return minimumBalance;
	}

	public void setMinimumBalance(double minimumBalance) {
		this.minimumBalance = minimumBalance;
	}

	public double getActualBalance() {
		return actualBalance;
	}

	public void setActualBalance(double actualBalance) {
		this.actualBalance = actualBalance;
	}

	public double getDrawableBalance() {
		return drawableBalance;
	}

	public void setDrawableBalance(double drawableBalance) {
		this.drawableBalance = drawableBalance;
	}

	public double getChequeIssuedAmount() {
		return chequeIssuedAmount;
	}

	public void setChequeIssuedAmount(double chequeIssuedAmount) {
		this.chequeIssuedAmount = chequeIssuedAmount;
	}

	public double getChequeDepositedAmount() {
		return chequeDepositedAmount;
	}

	public void setChequeDepositedAmount(double chequeDepositedAmount) {
		this.chequeDepositedAmount = chequeDepositedAmount;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	@Override
	public String toString() {
		return "BankAccount [id=" + id + ", accountHolderName=" + accountHolderName + ", accountNumber=" + accountNumber
				+ ", accountType=" + accountType + ", bank=" + bank + ", ifscCode=" + ifscCode + ", branchCode="
				+ branchCode + ", bankAddress=" + bankAddress + ", minimumBalance=" + minimumBalance
				+ ", actualBalance=" + actualBalance + ", drawableBalance=" + drawableBalance + ", chequeIssuedAmount="
				+ chequeIssuedAmount + ", chequeDepositedAmount=" + chequeDepositedAmount + ", insertedDate="
				+ insertedDate + ", updatedDate=" + updatedDate + ", status=" + status + ", company=" + company
				+ ", primary=" + primary + ", branch=" + branch + "]";
	}

	
	


	
	

	
}
