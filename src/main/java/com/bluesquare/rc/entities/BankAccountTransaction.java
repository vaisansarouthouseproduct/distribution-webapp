package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "bank_acc_txn")
@Component
public class BankAccountTransaction {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="txn_id")
	private String transactionId;
	
	@Column(name="party_name")
	private String partyName;
	
	@Column(name="opening")
	private double opening;
	
	@Column(name="credit")
	private double credit;
	
	@Column(name="debit")
	private double debit;
	
	@Column(name="closing")
	private double closing;
	
	@Column(name="pay_mode")
	private String payMode;
	
	@Column(name="ref_no")
	private String referenceNo;
	
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="inserted_date")
	private String insertedDate;
	
	
	@Column(name="cheque_date")
	private String chequeDate;
	
	@ManyToOne
	@JoinColumn(name="bank_account_id")
	private BankAccount bankAccount;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public double getOpening() {
		return opening;
	}

	public void setOpening(double opening) {
		this.opening = opening;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public double getClosing() {
		return closing;
	}

	public void setClosing(double closing) {
		this.closing = closing;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	@Override
	public String toString() {
		return "BankAccountTransaction [id=" + id + ", transactionId=" + transactionId + ", partyName=" + partyName
				+ ", opening=" + opening + ", credit=" + credit + ", debit=" + debit + ", closing=" + closing
				+ ", payMode=" + payMode + ", referenceNo=" + referenceNo + ", bankName=" + bankName + ", insertedDate="
				+ insertedDate + ", chequeDate=" + chequeDate + ", bankAccount=" + bankAccount + "]";
	}

}
