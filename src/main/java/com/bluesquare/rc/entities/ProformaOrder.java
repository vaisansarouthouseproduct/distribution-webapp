package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "proforma_order")
@Component
public class ProformaOrder {

	@Id
	@Column(name = "proforma_order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long proformaOrderPKId;

	@Column(name = "proforma_order_id")
	private String proformaOrderId;

	@ManyToOne
	@JoinColumn(name = "employee_gk_id")
	private Employee employeeGk;

	@ManyToOne
	@JoinColumn(name = "business_id")
	private BusinessName businessName;

	/*@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_mob_number")
	private String customerMobileNumber;

	@Column(name = "customer_gst_number")
	private String customerGstNumber;*/

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;

	@Column(name = "date_of_order_taken")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfOrderTaken;

	/*@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "paid_status")
	private boolean payStatus;

	@Column(name = "payment_due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paymentDueDate;

	@Column(name = "invoice_number")
	private String invoiceNumber;

	@ManyToOne
	@JoinColumn(name = "order_status_id")
	private OrderStatus orderStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "bad_debts_status")
	private boolean badDebtsStatus;

	@Column(name = "total_amount_due_baddebts", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountBadDebtsDue;

	@Column(name = "bad_Debts_disable_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date badDebtsDatetime;*/

	@ManyToOne
	@JoinColumn(name = "transportation_id")
	private Transportation transportation;

	@Column(name = "vehicle_no")
	private String vehicleNo;

	@Column(name = "docket_no")
	private String docketNo;
	
	@Column(name = "transportation_charges", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double transportationCharges;
	

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;

	@Column(name = "discount_type")
	private String discountType;

	@Column(name = "discount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double discount;
	
	@Column(name="order_comment")
	private String orderComment;

	public long getProformaOrderPKId() {
		return proformaOrderPKId;
	}

	public void setProformaOrderPKId(long proformaOrderPKId) {
		this.proformaOrderPKId = proformaOrderPKId;
	}

	public String getProformaOrderId() {
		return proformaOrderId;
	}

	public void setProformaOrderId(String proformaOrderId) {
		this.proformaOrderId = proformaOrderId;
	}

	public Employee getEmployeeGk() {
		return employeeGk;
	}

	public void setEmployeeGk(Employee employeeGk) {
		this.employeeGk = employeeGk;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getDateOfOrderTaken() {
		return dateOfOrderTaken;
	}

	public void setDateOfOrderTaken(Date dateOfOrderTaken) {
		this.dateOfOrderTaken = dateOfOrderTaken;
	}

	public Transportation getTransportation() {
		return transportation;
	}

	public void setTransportation(Transportation transportation) {
		this.transportation = transportation;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public double getTransportationCharges() {
		return transportationCharges;
	}

	public void setTransportationCharges(double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getOrderComment() {
		return orderComment;
	}

	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}

	@Override
	public String toString() {
		return "ProformaOrder [proformaOrderPKId=" + proformaOrderPKId + ", proformaOrderId=" + proformaOrderId
				+ ", employeeGk=" + employeeGk + ", businessName=" + businessName + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity
				+ ", dateOfOrderTaken=" + dateOfOrderTaken + ", transportation=" + transportation + ", vehicleNo="
				+ vehicleNo + ", docketNo=" + docketNo + ", transportationCharges=" + transportationCharges
				+ ", branch=" + branch + ", discountType=" + discountType + ", discount=" + discount + ", orderComment="
				+ orderComment + "]";
	}
}
