package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "return_order_product_details")
@Component

public class ReturnOrderProductDetails {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "issued_quantity")
	private long issuedQuantity;

	@Column(name = "return_quantity")
	private long returnQuantity;

	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double sellingRate;

	@Column(name = "return_total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double returnTotalAmountWithTax;

	@Column(name = "reason")
	private String reason;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;

	@Column(name = "type")
	private String type;

	@ManyToOne
	@JoinColumn(name = "return_order_product_id")
	private ReturnOrderProduct returnOrderProduct;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getReturnTotalAmountWithTax() {
		return returnTotalAmountWithTax;
	}

	public void setReturnTotalAmountWithTax(double returnTotalAmountWithTax) {
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}

	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ReturnOrderProductDetails [id=" + id + ", issuedQuantity=" + issuedQuantity + ", returnQuantity="
				+ returnQuantity + ", sellingRate=" + sellingRate + ", returnTotalAmountWithTax="
				+ returnTotalAmountWithTax + ", reason=" + reason + ", product=" + product + ", type=" + type
				+ ", returnOrderProduct=" + returnOrderProduct + "]";
	}
}
