package com.bluesquare.rc.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "company")
@Component
public class Company {

	@Id
	@Column(name = "company_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long companyId;
	
	@Column(name = "company_name")
	private String companyName;
			
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "gstin_no")
	private String gstinno;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "pan_number")
	private String panNumber;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "password")
	private String password;
	
	@ManyToOne
    @JoinColumn(name = "contact_id")
	private Contact contact;
	
	/*@Column(name="balance" ,precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double balance;*/
	
	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGstinno() {
		return gstinno;
	}

	public void setGstinno(String gstinno) {
		this.gstinno = gstinno;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", companyName=" + companyName + ", userId=" + userId + ", gstinno="
				+ gstinno + ", address=" + address + ", panNumber=" + panNumber + ", password=" + password
				+ ", contact=" + contact + "]";
	}

	/*public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", companyName=" + companyName + ", userId=" + userId + ", gstinno="
				+ gstinno + ", address=" + address + ", panNumber=" + panNumber + ", password=" + password
				+ ", contact=" + contact + ", balance=" + balance + "]";
	}*/
	
	

	
}
