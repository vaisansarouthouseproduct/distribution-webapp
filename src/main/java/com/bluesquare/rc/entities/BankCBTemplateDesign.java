package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name="bank_cb_template_design")
@Component
public class BankCBTemplateDesign {

	@Id
	@Column(name="bank_cb_design_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="template_name")
	private String templateName;
	
	@ManyToOne
	@JoinColumn(name="bank_id")
	private Bank bank;
	
	@Column(name = "template_img_base64", columnDefinition = "LONGTEXT")
	private String templateImgBase64;
	
	@Column(name="template_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date templateAddedDatetime;
	
	@Column(name="template_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date templateUpdatedDatetime;
	
	
	//Line or box formate
	@Column(name="date_format")
	private String dateFormate;
	
	// Cheque Date Position
	
	@Column(name="chq_date_top")
	private double chqDateTop;
	
	@Column(name="chq_date_left")
	private double chqDateLeft;
	
	@Column(name="chq_date_width")
	private double chqDateWidth;
	
	@Column(name="chq_date_height")
	private double chqDateHeight;
	
	/*@Column(name="d1_top")
	private double d1Top;
	
	@Column(name="d1_left")
	private double d1left;
	
	@Column(name="d1_width")
	private double d1Width;
	
	@Column(name="d2_top")
	private double d2Top;
	
	@Column(name="d2_left")
	private double d2left;
	
	@Column(name="d2_width")
	private double d2Width;
	
	@Column(name="m1_top")
	private double m1Top;
	
	@Column(name="m1_left")
	private double m1left;
	
	@Column(name="m1_width")
	private double m1Width;
	
	@Column(name="m2_top")
	private double m2Top;
	
	@Column(name="m2_left")
	private double m2left;
	
	@Column(name="m2_width")
	private double m2Width;
	
	@Column(name="y1_top")
	private double y1Top;
	
	@Column(name="y1_left")
	private double y1left;
	
	@Column(name="y1_width")
	private double y1Width;
	
	@Column(name="y2_top")
	private double y2Top;
	
	@Column(name="y2_left")
	private double y2left;
	
	@Column(name="y2_width")
	private double y2Width;
	
	@Column(name="y3_top")
	private double y3Top;
	
	@Column(name="y3_left")
	private double y3left;
	
	@Column(name="y3_width")
	private double y3Width;

	
	@Column(name="y4_top")
	private double y4Top;
	
	@Column(name="y4_left")
	private double y4left;
	
	@Column(name="y4_width")
	private double y4Width;*/
	
	// Cheque Date Position end
	
	@Column(name="payee_name_top")
	private double payeeNameTop;
	
	@Column(name="payee_name_left")
	private double payeeNameLeft;
	
	@Column(name="payee_name_width")
	private double payeeNameWidth;
	

	@Column(name="amount_top")
	private double amountTop;
	
	@Column(name="amount_left")
	private double amountLeft;
	
	@Column(name="amount_width")
	private double amountWidth;
	
	
	@Column(name="amount_in_word_top")
	private double amountInWordTop;
	
	@Column(name="amount_in_word_left")
	private double amountInWordLeft;
	
	@Column(name="amount_in_word_width")
	private double amountInWordWidth;
	
	
	@Column(name="ac_payee_top")
	private double acPayeeTop;
	
	@Column(name="ac_payee_left")
	private double acPayeeLeft;
	
	@Column(name="ac_payee_width")
	private double acPayeeWidth;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public String getTemplateImgBase64() {
		return templateImgBase64;
	}

	public void setTemplateImgBase64(String templateImgBase64) {
		this.templateImgBase64 = templateImgBase64;
	}

	public Date getTemplateAddedDatetime() {
		return templateAddedDatetime;
	}

	public void setTemplateAddedDatetime(Date templateAddedDatetime) {
		this.templateAddedDatetime = templateAddedDatetime;
	}

	public Date getTemplateUpdatedDatetime() {
		return templateUpdatedDatetime;
	}

	public void setTemplateUpdatedDatetime(Date templateUpdatedDatetime) {
		this.templateUpdatedDatetime = templateUpdatedDatetime;
	}

	public String getDateFormate() {
		return dateFormate;
	}

	public void setDateFormate(String dateFormate) {
		this.dateFormate = dateFormate;
	}

	public double getChqDateTop() {
		return chqDateTop;
	}

	public void setChqDateTop(double chqDateTop) {
		this.chqDateTop = chqDateTop;
	}

	public double getChqDateLeft() {
		return chqDateLeft;
	}

	public void setChqDateLeft(double chqDateLeft) {
		this.chqDateLeft = chqDateLeft;
	}

	public double getChqDateWidth() {
		return chqDateWidth;
	}

	public void setChqDateWidth(double chqDateWidth) {
		this.chqDateWidth = chqDateWidth;
	}

	public double getPayeeNameTop() {
		return payeeNameTop;
	}

	public void setPayeeNameTop(double payeeNameTop) {
		this.payeeNameTop = payeeNameTop;
	}

	public double getPayeeNameLeft() {
		return payeeNameLeft;
	}

	public void setPayeeNameLeft(double payeeNameLeft) {
		this.payeeNameLeft = payeeNameLeft;
	}

	public double getPayeeNameWidth() {
		return payeeNameWidth;
	}

	public void setPayeeNameWidth(double payeeNameWidth) {
		this.payeeNameWidth = payeeNameWidth;
	}

	public double getAmountTop() {
		return amountTop;
	}

	public void setAmountTop(double amountTop) {
		this.amountTop = amountTop;
	}

	public double getAmountLeft() {
		return amountLeft;
	}

	public void setAmountLeft(double amountLeft) {
		this.amountLeft = amountLeft;
	}

	public double getAmountWidth() {
		return amountWidth;
	}

	public void setAmountWidth(double amountWidth) {
		this.amountWidth = amountWidth;
	}

	public double getAmountInWordTop() {
		return amountInWordTop;
	}

	public void setAmountInWordTop(double amountInWordTop) {
		this.amountInWordTop = amountInWordTop;
	}

	public double getAmountInWordLeft() {
		return amountInWordLeft;
	}

	public void setAmountInWordLeft(double amountInWordLeft) {
		this.amountInWordLeft = amountInWordLeft;
	}

	public double getAmountInWordWidth() {
		return amountInWordWidth;
	}

	public void setAmountInWordWidth(double amountInWordWidth) {
		this.amountInWordWidth = amountInWordWidth;
	}

	public double getAcPayeeTop() {
		return acPayeeTop;
	}

	public void setAcPayeeTop(double acPayeeTop) {
		this.acPayeeTop = acPayeeTop;
	}

	public double getAcPayeeLeft() {
		return acPayeeLeft;
	}

	public void setAcPayeeLeft(double acPayeeLeft) {
		this.acPayeeLeft = acPayeeLeft;
	}

	public double getAcPayeeWidth() {
		return acPayeeWidth;
	}

	public void setAcPayeeWidth(double acPayeeWidth) {
		this.acPayeeWidth = acPayeeWidth;
	}

	public double getChqDateHeight() {
		return chqDateHeight;
	}

	public void setChqDateHeight(double chqDateHeight) {
		this.chqDateHeight = chqDateHeight;
	}

	@Override
	public String toString() {
		return "BankCBTemplateDesign [id=" + id + ", templateName=" + templateName + ", bank=" + bank
				+ ", templateImgBase64=" + templateImgBase64 + ", templateAddedDatetime=" + templateAddedDatetime
				+ ", templateUpdatedDatetime=" + templateUpdatedDatetime + ", dateFormate=" + dateFormate
				+ ", chqDateTop=" + chqDateTop + ", chqDateLeft=" + chqDateLeft + ", chqDateWidth=" + chqDateWidth
				+ ", chqDateHeight=" + chqDateHeight + ", payeeNameTop=" + payeeNameTop + ", payeeNameLeft="
				+ payeeNameLeft + ", payeeNameWidth=" + payeeNameWidth + ", amountTop=" + amountTop + ", amountLeft="
				+ amountLeft + ", amountWidth=" + amountWidth + ", amountInWordTop=" + amountInWordTop
				+ ", amountInWordLeft=" + amountInWordLeft + ", amountInWordWidth=" + amountInWordWidth
				+ ", acPayeeTop=" + acPayeeTop + ", acPayeeLeft=" + acPayeeLeft + ", acPayeeWidth=" + acPayeeWidth
				+ "]";
	}


	
}
