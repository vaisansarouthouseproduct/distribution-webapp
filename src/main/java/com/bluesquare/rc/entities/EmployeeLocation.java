package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_location")
@Component
public class EmployeeLocation {
	@Id
	@Column(name = "employee_location_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeLocationId;

	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "location_info")
	private String locationInfo;

	@Column(name = "address")
	private String address;

	
	@Column(name = "datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datetime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="new_route_status")
	private boolean isNewRoute;

	public long getEmployeeLocationId() {
		return employeeLocationId;
	}

	public void setEmployeeLocationId(long employeeLocationId) {
		this.employeeLocationId = employeeLocationId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLocationInfo() {
		return locationInfo;
	}

	public void setLocationInfo(String locationInfo) {
		this.locationInfo = locationInfo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public boolean isNewRoute() {
		return isNewRoute;
	}

	public void setNewRoute(boolean isNewRoute) {
		this.isNewRoute = isNewRoute;
	}

	@Override
	public String toString() {
		return "EmployeeLocation [employeeLocationId=" + employeeLocationId + ", employeeDetails=" + employeeDetails
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", locationInfo=" + locationInfo
				+ ", address=" + address + ", datetime=" + datetime + ", isNewRoute=" + isNewRoute + "]";
	}
		
}
