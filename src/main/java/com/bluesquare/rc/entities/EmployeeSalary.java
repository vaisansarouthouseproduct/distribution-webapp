package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_salary")
@Component
public class EmployeeSalary {

	@Id
	@Column(name = "employee_salary_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeSalaryId;

	@ManyToOne
	@JoinColumn(name = "employeeDetails_id")
	private EmployeeDetails employeeDetails;/*
											 * 
											 * @Column(name = "incentive")
											 * private double incentive;
											 */

	@Column(name = "paying_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double payingAmount;

	@Column(name = "cheque_number")
	private String chequeNumber;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "cheque_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date chequeDate;

	@Column(name = "paying_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date payingDate;

	@Column(name = "comment")
	private String comment;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;

	@ManyToOne
	@JoinColumn(name = "payment_method")
	private PaymentMethod PaymentMethod;

	@Column(name = "trasaction_refrence_number")
	private String transactionReferenceNumber;

	@Column(name = "pay_type")
	private String payType;

	public long getEmployeeSalaryId() {
		return employeeSalaryId;
	}

	public void setEmployeeSalaryId(long employeeSalaryId) {
		this.employeeSalaryId = employeeSalaryId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public double getPayingAmount() {
		return payingAmount;
	}

	public void setPayingAmount(double payingAmount) {
		this.payingAmount = payingAmount;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public Date getPayingDate() {
		return payingDate;
	}

	public void setPayingDate(Date payingDate) {
		this.payingDate = payingDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public PaymentMethod getPaymentMethod() {
		return PaymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		PaymentMethod = paymentMethod;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Override
	public String toString() {
		return "EmployeeSalary [employeeSalaryId=" + employeeSalaryId + ", employeeDetails=" + employeeDetails
				+ ", payingAmount=" + payingAmount + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName
				+ ", chequeDate=" + chequeDate + ", payingDate=" + payingDate + ", comment=" + comment + ", status="
				+ status + ", PaymentMethod=" + PaymentMethod + ", transactionReferenceNumber="
				+ transactionReferenceNumber + ", payType=" + payType + "]";
	}

}
