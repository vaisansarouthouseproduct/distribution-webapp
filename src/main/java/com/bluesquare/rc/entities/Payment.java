package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "payment")
@Component
public class Payment {

	@Id
	@Column(name = "payment_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long paymentId;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private OrderDetails orderDetails;
	
	@ManyToOne
	@JoinColumn(name = "employee_id_pay_take")
	private Employee employee;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "due_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double dueAmount;

	@Column(name = "due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dueDate;
	
	@Column(name = "last_due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date lastDueDate;

	@Column(name = "paid_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double paidAmount;

	@Column(name = "paid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paidDate;

	@Column(name = "pay_type")
	private String payType;

	@Column(name = "cheque_number")
	private String chequeNumber;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "cheque_date")
	private Date chequeDate;
	
	@ManyToOne
	@JoinColumn(name="payment_method")
	private PaymentMethod PaymentMethod;
	
	@Column(name="trasaction_refrence_number")
	private String transactionReferenceNumber;
	
	@Column(name="comment")
	private String comment;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "cheque_clear_status")
	private boolean chequeClearStatus;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public boolean isChequeClearStatus() {
		return chequeClearStatus;
	}

	public void setChequeClearStatus(boolean chequeClearStatus) {
		this.chequeClearStatus = chequeClearStatus;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	

	public PaymentMethod getPaymentMethod() {
		return PaymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		PaymentMethod = paymentMethod;
	}


	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	@Override
	public String toString() {
		return "Payment [paymentId=" + paymentId + ", orderDetails=" + orderDetails + ", employee=" + employee
				+ ", totalAmount=" + totalAmount + ", dueAmount=" + dueAmount + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", paidAmount=" + paidAmount + ", paidDate=" + paidDate
				+ ", payType=" + payType + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName + ", chequeDate="
				+ chequeDate + ", PaymentMethod=" + PaymentMethod + ", transactionReferenceNumber="
				+ transactionReferenceNumber + ", comment=" + comment + ", status=" + status + ", chequeClearStatus="
				+ chequeClearStatus + "]";
	}


	
}