package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "meeting")
@Component
public class Meeting {

	@Id
	@Column(name = "meeting_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long meetingId;

	@ManyToOne
	@JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;

	@ManyToOne
	@JoinColumn(name = "business_name_id")
	private BusinessName businessName;

	@Column(name = "owner_name")
	private String ownerName;

	@Column(name = "shop_name")
	private String shopName;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "address", columnDefinition = "LONGTEXT")
	private String address;

	@Column(name = "meeting_venue", columnDefinition = "LONGTEXT")
	private String meetingVenue;
	
	@Column(name = "cancel_reason")
	private String cancelReason;
	
	@Column(name = "meeting_status")
	private String meetingStatus;
	
	@Column(name = "meeting_review")
	private String meetingReview;


	@Column(name = "meeting_from_date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date meetingFromDateTime;

	@Column(name = "meeting_to_date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date meetingToDateTime;

	@Column(name = "added_date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDateTime;

	@Column(name = "updated_date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updatedDateTime;
	
	@Column(name = "cancel_date_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date cancelDateTime;
	
	@ManyToOne
	@JoinColumn(name="meeting")
	private Meeting meeting;
	
	@ManyToOne
	@JoinColumn(name="shop_visit_id")
	private ShopVisit shopVisit;
	
	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;	
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public Meeting() {
		// TODO Auto-generated constructor stub
	}

	public Meeting(Meeting meeting) {
		super();
		this.employeeDetails = meeting.getEmployeeDetails();
		this.businessName = meeting.getBusinessName();
		this.ownerName = meeting.getOwnerName();
		this.shopName = meeting.getShopName();
		this.mobileNumber = meeting.getMobileNumber();
		this.address = meeting.getAddress();
		this.meetingVenue = meeting.getMeetingVenue();
		this.cancelReason = meeting.getCancelReason();
		this.meetingStatus = meeting.getMeetingStatus();
	}

	public long getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(long meetingId) {
		this.meetingId = meetingId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMeetingVenue() {
		return meetingVenue;
	}

	public void setMeetingVenue(String meetingVenue) {
		this.meetingVenue = meetingVenue;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getMeetingStatus() {
		return meetingStatus;
	}

	public void setMeetingStatus(String meetingStatus) {
		this.meetingStatus = meetingStatus;
	}

	public String getMeetingReview() {
		return meetingReview;
	}

	public void setMeetingReview(String meetingReview) {
		this.meetingReview = meetingReview;
	}

	public Date getMeetingFromDateTime() {
		return meetingFromDateTime;
	}

	public void setMeetingFromDateTime(Date meetingFromDateTime) {
		this.meetingFromDateTime = meetingFromDateTime;
	}

	public Date getMeetingToDateTime() {
		return meetingToDateTime;
	}

	public void setMeetingToDateTime(Date meetingToDateTime) {
		this.meetingToDateTime = meetingToDateTime;
	}

	public Date getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(Date addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public Date getCancelDateTime() {
		return cancelDateTime;
	}

	public void setCancelDateTime(Date cancelDateTime) {
		this.cancelDateTime = cancelDateTime;
	}

	public Meeting getMeeting() {
		return meeting;
	}

	public void setMeeting(Meeting meeting) {
		this.meeting = meeting;
	}

	public ShopVisit getShopVisit() {
		return shopVisit;
	}

	public void setShopVisit(ShopVisit shopVisit) {
		this.shopVisit = shopVisit;
	}

	@Override
	public String toString() {
		return "Meeting [meetingId=" + meetingId + ", employeeDetails=" + employeeDetails + ", businessName="
				+ businessName + ", ownerName=" + ownerName + ", shopName=" + shopName + ", mobileNumber="
				+ mobileNumber + ", address=" + address + ", meetingVenue=" + meetingVenue + ", cancelReason="
				+ cancelReason + ", meetingStatus=" + meetingStatus + ", meetingReview=" + meetingReview
				+ ", meetingFromDateTime=" + meetingFromDateTime + ", meetingToDateTime=" + meetingToDateTime
				+ ", addedDateTime=" + addedDateTime + ", updatedDateTime=" + updatedDateTime + ", cancelDateTime="
				+ cancelDateTime + ", meeting=" + meeting + ", shopVisit=" + shopVisit + ", branch=" + branch + "]";
	}

	

	
}
