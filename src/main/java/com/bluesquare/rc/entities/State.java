package com.bluesquare.rc.entities;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "state")
@Component

public class State {


	@Id
	@Column(name = "state_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long stateId;
	
	/*@OneToMany(mappedBy = "state", cascade = CascadeType.ALL)
	private Set<City> city ;
*/
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
    @JoinColumn(name = "country_id")
	private Country country;

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "State [stateId=" + stateId + ", code=" + code + ", name=" + name + ", country=" + country + "]";
	}

}
