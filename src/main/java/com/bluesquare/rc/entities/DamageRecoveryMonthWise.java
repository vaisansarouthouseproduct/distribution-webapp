package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "damage_recovery_month_wise")
@Component

public class DamageRecoveryMonthWise {
	@Id
	@Column(name = "damage_recovery_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long damageRecoveryId;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "qty_damage")
	private long quantityDamage;
	
	@Column(name = "qty_given")
	private long quantityGiven;
	
	@Column(name = "qty_received")
	private long quantityReceived;
	
	@Column(name = "qty_not_claimed")
	private long quantityNotClaimed;

	@Column(name = "datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datetime;	

	public long getDamageRecoveryId() {
		return damageRecoveryId;
	}

	public void setDamageRecoveryId(long damageRecoveryId) {
		this.damageRecoveryId = damageRecoveryId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getQuantityDamage() {
		return quantityDamage;
	}

	public void setQuantityDamage(long quantityDamage) {
		this.quantityDamage = quantityDamage;
	}

	public long getQuantityGiven() {
		return quantityGiven;
	}

	public void setQuantityGiven(long quantityGiven) {
		this.quantityGiven = quantityGiven;
	}

	public long getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(long quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public long getQuantityNotClaimed() {
		return quantityNotClaimed;
	}

	public void setQuantityNotClaimed(long quantityNotClaimed) {
		this.quantityNotClaimed = quantityNotClaimed;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		return "DamageRecoveryMonthWise [damageRecoveryId=" + damageRecoveryId + ", product=" + product
				+ ", quantityDamage=" + quantityDamage + ", quantityGiven=" + quantityGiven + ", quantityReceived="
				+ quantityReceived + ", quantityNotClaimed=" + quantityNotClaimed + ", datetime=" + datetime + "]";
	}
	
}
