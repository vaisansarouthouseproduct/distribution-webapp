package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_profile_pic")
@Component
public class EmployeeProfilePicture {
	@Id
	@Column(name = "employee_profile_picture_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long employeeProfilePictureId;
	
	@ManyToOne
    @JoinColumn(name = "employee_id")
	private Employee employee;
	
	@Column(name = "user_img_base64", columnDefinition = "LONGTEXT")
	private String userImgBase64;

	public long getEmployeeProfilePictureId() {
		return employeeProfilePictureId;
	}

	public void setEmployeeProfilePictureId(long employeeProfilePictureId) {
		this.employeeProfilePictureId = employeeProfilePictureId;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getUserImgBase64() {
		return userImgBase64;
	}

	public void setUserImgBase64(String userImgBase64) {
		this.userImgBase64 = userImgBase64;
	}

	@Override
	public String toString() {
		return "EmployeeProfilePicture [employeeProfilePictureId=" + employeeProfilePictureId + ", employee=" + employee
				+ ", userImgBase64=" + userImgBase64 + "]";
	}
	
	
}
