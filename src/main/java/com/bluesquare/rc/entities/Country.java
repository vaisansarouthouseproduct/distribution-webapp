package com.bluesquare.rc.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "country")
@Component
public class Country {
	
	@Id
	@Column(name = "country_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long countryId;
	
	/*@OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
	private Set<State> state ;*/
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "short_name")
	private String shortName;

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Override
	public String toString() {
		return "Country [countryId=" + countryId + ", name=" + name + ", shortName=" + shortName + "]";
	}
	

	
	

}
