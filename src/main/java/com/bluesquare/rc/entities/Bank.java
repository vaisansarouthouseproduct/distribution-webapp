package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;


@Entity
@Table(name="bank")
@Component
public class Bank {
	
	@Id
	@Column(name="bank_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long bankId;
	
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="bank_short_name")
	private String shortName;
	
	
	
	@Column(name="bank_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date bankAddedDatetime;
	
	@Column(name="bank_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date bankUpdatedDatetime;
	

	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;


	public long getBankId() {
		return bankId;
	}


	public void setBankId(long bankId) {
		this.bankId = bankId;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getShortName() {
		return shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public Date getBankAddedDatetime() {
		return bankAddedDatetime;
	}


	public void setBankAddedDatetime(Date bankAddedDatetime) {
		this.bankAddedDatetime = bankAddedDatetime;
	}


	public Date getBankUpdatedDatetime() {
		return bankUpdatedDatetime;
	}


	public void setBankUpdatedDatetime(Date bankUpdatedDatetime) {
		this.bankUpdatedDatetime = bankUpdatedDatetime;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	@Override
	public String toString() {
		return "Bank [bankId=" + bankId + ", bankName=" + bankName + ", shortName=" + shortName + ", bankAddedDatetime="
				+ bankAddedDatetime + ", bankUpdatedDatetime=" + bankUpdatedDatetime + ", company=" + company + "]";
	}


	
}
