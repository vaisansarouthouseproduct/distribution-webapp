package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "permanent_damage_details")
@Component

public class PermanentDamageDetails {

	@Id
	@Column(name = "permanent_damage_details_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long permanentDamageDetailsId;

	@ManyToOne
	@JoinColumn(name = "permanent_damage_month_wise_id")
	private PermanentDamageMonthWise permanentDamageMonthWise;

	@Column(name = "qty_damage")
	private long quantityDamage;

	@Column(name = "reject_qty_reason")
	private String rejectQuantityReason;

	@Column(name = "damage_from")
	private String damageFrom;

	@Column(name = "damage_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date damageDate;

	public long getPermanentDamageDetailsId() {
		return permanentDamageDetailsId;
	}

	public void setPermanentDamageDetailsId(long permanentDamageDetailsId) {
		this.permanentDamageDetailsId = permanentDamageDetailsId;
	}

	public PermanentDamageMonthWise getPermanentDamageMonthWise() {
		return permanentDamageMonthWise;
	}

	public void setPermanentDamageMonthWise(PermanentDamageMonthWise permanentDamageMonthWise) {
		this.permanentDamageMonthWise = permanentDamageMonthWise;
	}

	public long getQuantityDamage() {
		return quantityDamage;
	}

	public void setQuantityDamage(long quantityDamage) {
		this.quantityDamage = quantityDamage;
	}

	public String getRejectQuantityReason() {
		return rejectQuantityReason;
	}

	public void setRejectQuantityReason(String rejectQuantityReason) {
		this.rejectQuantityReason = rejectQuantityReason;
	}

	public Date getDamageDate() {
		return damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	public String getDamageFrom() {
		return damageFrom;
	}

	public void setDamageFrom(String damageFrom) {
		this.damageFrom = damageFrom;
	}

	@Override
	public String toString() {
		return "PermanentDamageDetails [permanentDamageDetailsId=" + permanentDamageDetailsId
				+ ", permanentDamageMonthWise=" + permanentDamageMonthWise + ", quantityDamage=" + quantityDamage
				+ ", rejectQuantityReason=" + rejectQuantityReason + ", damageFrom=" + damageFrom + ", damageDate="
				+ damageDate + "]";
	}

}
