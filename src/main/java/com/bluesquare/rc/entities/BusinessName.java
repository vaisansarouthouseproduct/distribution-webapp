package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "business_name")
@Component

public class BusinessName {

	@Id
	@Column(name = "business_name_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long businessNamePkId;

	@Column(name = "business_name_id")
	private String businessNameId;

	@Column(name = "shop_name")
	private String shopName;

	@Column(name = "owner_name")
	private String ownerName;

	@Column(name = "comment")
	private String comment;

	@Column(name = "other")
	private String other;

	@Column(name = "credit_limit", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double creditLimit;

	@Column(name = "gstin_number")
	private String gstinNumber;

	@ManyToOne
	@JoinColumn(name = "businesstype_id")
	private BusinessType businessType;

	@Column(name = "address")
	private String address;

	@Column(name = "tax_type")
	private String taxType;

	@ManyToOne
	@JoinColumn(name = "contact_id")
	private Contact contact;

	@ManyToOne
	@JoinColumn(name = "area_id")
	private Area area;

	@ManyToOne
	@JoinColumn(name = "employee_id_sm")
	private Employee employeeSM;

	@Column(name = "business_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date businessAddedDatetime;

	@Column(name = "business_updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date businessUpdatedDatetime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "bad_debts_status")
	private boolean badDebtsStatus;

	@Column(name = "amount_loss", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double amountLoss;

	@Column(name = "bad_Debts_disable_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date badDebtsDatetime;

	@Column(name = "business_disable_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date disableDatetime;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;

	@Column(name = "amount_pending", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double amountPending;

	public BusinessName() {
		// TODO Auto-generated constructor stub
	}

	public BusinessName(String shopName, String ownerName, String comment, String other, double creditLimit,
			String gstinNumber, BusinessType businessType, String address, String taxType, Contact contact, Area area,
			Employee employeeSM, Date businessAddedDatetime, Date businessUpdatedDatetime, boolean status,
			boolean badDebtsStatus, double amountLoss, Date badDebtsDatetime, Date disableDatetime, Company company,
			Branch branch, double amountPending) {
		this.shopName = shopName;
		this.ownerName = ownerName;
		this.comment = comment;
		this.other = other;
		this.creditLimit = creditLimit;
		this.gstinNumber = gstinNumber;
		this.businessType = businessType;
		this.address = address;
		this.taxType = taxType;
		this.contact = contact;
		this.area = area;
		this.employeeSM = employeeSM;
		this.businessAddedDatetime = businessAddedDatetime;
		this.businessUpdatedDatetime = businessUpdatedDatetime;
		this.status = status;
		this.badDebtsStatus = badDebtsStatus;
		this.amountLoss = amountLoss;
		this.badDebtsDatetime = badDebtsDatetime;
		this.disableDatetime = disableDatetime;
		this.company = company;
		this.branch = branch;
		this.amountPending = amountPending;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public long getBusinessNamePkId() {
		return businessNamePkId;
	}

	public void setBusinessNamePkId(long businessNamePkId) {
		this.businessNamePkId = businessNamePkId;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getGstinNumber() {
		return gstinNumber;
	}

	public void setGstinNumber(String gstinNumber) {
		this.gstinNumber = gstinNumber;
	}

	public BusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(BusinessType businessType) {
		this.businessType = businessType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Date getBusinessAddedDatetime() {
		return businessAddedDatetime;
	}

	public void setBusinessAddedDatetime(Date businessAddedDatetime) {
		this.businessAddedDatetime = businessAddedDatetime;
	}

	public Date getBusinessUpdatedDatetime() {
		return businessUpdatedDatetime;
	}

	public void setBusinessUpdatedDatetime(Date businessUpdatedDatetime) {
		this.businessUpdatedDatetime = businessUpdatedDatetime;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getDisableDatetime() {
		return disableDatetime;
	}

	public void setDisableDatetime(Date disableDatetime) {
		this.disableDatetime = disableDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Employee getEmployeeSM() {
		return employeeSM;
	}

	public void setEmployeeSM(Employee employeeSM) {
		this.employeeSM = employeeSM;
	}

	public boolean getBadDebtsStatus() {
		return badDebtsStatus;
	}

	public void setBadDebtsStatus(boolean badDebtsStatus) {
		this.badDebtsStatus = badDebtsStatus;
	}

	public double getAmountLoss() {
		return amountLoss;
	}

	public void setAmountLoss(double amountLoss) {
		this.amountLoss = amountLoss;
	}

	public Date getBadDebtsDatetime() {
		return badDebtsDatetime;
	}

	public void setBadDebtsDatetime(Date badDebtsDatetime) {
		this.badDebtsDatetime = badDebtsDatetime;
	}

	public double getAmountPending() {
		return amountPending;
	}

	public void setAmountPending(double amountPending) {
		this.amountPending = amountPending;
	}

	@Override
	public String toString() {
		return "BusinessName [businessNamePkId=" + businessNamePkId + ", businessNameId=" + businessNameId
				+ ", shopName=" + shopName + ", ownerName=" + ownerName + ", comment=" + comment + ", other=" + other
				+ ", creditLimit=" + creditLimit + ", gstinNumber=" + gstinNumber + ", businessType=" + businessType
				+ ", address=" + address + ", taxType=" + taxType + ", contact=" + contact + ", area=" + area
				+ ", employeeSM=" + employeeSM + ", businessAddedDatetime=" + businessAddedDatetime
				+ ", businessUpdatedDatetime=" + businessUpdatedDatetime + ", status=" + status + ", badDebtsStatus="
				+ badDebtsStatus + ", amountLoss=" + amountLoss + ", badDebtsDatetime=" + badDebtsDatetime
				+ ", disableDatetime=" + disableDatetime + ", company=" + company + ", branch=" + branch
				+ ", amountPending=" + amountPending + "]";
	}

}
