package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "target_type")
@Component
public class TargetType {

	@Id
	@Column(name = "target_type_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long targetTypeId;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "value_type")
	private String valueType; //percentage,value,both
	
	@ManyToOne
    @JoinColumn(name = "department_id")
	private Department department;
	
	@ManyToOne
    @JoinColumn(name = "company_id")
	private Company company;
	
	@Column(name = "status")
	private boolean status;

	@Column(name="added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date addedDatetime;
	
	@Column(name="update_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date updateDatetime;
	
	@Column(name="disable_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date disableDatetime;

	public long getTargetTypeId() {
		return targetTypeId;
	}

	public void setTargetTypeId(long targetTypeId) {
		this.targetTypeId = targetTypeId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(Date addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public Date getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Date getDisableDatetime() {
		return disableDatetime;
	}

	public void setDisableDatetime(Date disableDatetime) {
		this.disableDatetime = disableDatetime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	@Override
	public String toString() {
		return "TargetType [targetTypeId=" + targetTypeId + ", type=" + type + ", valueType=" + valueType
				+ ", department=" + department + ", company=" + company + ", status=" + status + ", addedDatetime="
				+ addedDatetime + ", updateDatetime=" + updateDatetime + ", disableDatetime=" + disableDatetime + "]";
	}

	
	
	
}
