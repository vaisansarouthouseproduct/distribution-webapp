package com.bluesquare.rc.responseEntities;

public class ReturnFromDeliveryBoyModel {

	private long returnFromDeliveryBoyId;

	private String productName;

	private long productId;

	private long returnQuantity;

	private long issuedQuantity;

	private long deliveryQuantity;

	private long damageQuantity;

	private long nonDamageQuantity;

	private String type;

	private String reason;

	public ReturnFromDeliveryBoyModel() {
		// TODO Auto-generated constructor stub
	}

	public ReturnFromDeliveryBoyModel(long returnFromDeliveryBoyId, String productName, long productId,
			long returnQuantity, long issuedQuantity, long deliveryQuantity, long damageQuantity,
			long nonDamageQuantity, String type) {
		super();
		this.returnFromDeliveryBoyId = returnFromDeliveryBoyId;
		this.productName = productName;
		this.productId = productId;
		this.returnQuantity = returnQuantity;
		this.issuedQuantity = issuedQuantity;
		this.deliveryQuantity = deliveryQuantity;
		this.damageQuantity = damageQuantity;
		this.nonDamageQuantity = nonDamageQuantity;
		this.type = type;
	}

	public long getReturnFromDeliveryBoyId() {
		return returnFromDeliveryBoyId;
	}

	public void setReturnFromDeliveryBoyId(long returnFromDeliveryBoyId) {
		this.returnFromDeliveryBoyId = returnFromDeliveryBoyId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public long getDeliveryQuantity() {
		return deliveryQuantity;
	}

	public void setDeliveryQuantity(long deliveryQuantity) {
		this.deliveryQuantity = deliveryQuantity;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public long getNonDamageQuantity() {
		return nonDamageQuantity;
	}

	public void setNonDamageQuantity(long nonDamageQuantity) {
		this.nonDamageQuantity = nonDamageQuantity;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "ReturnFromDeliveryBoyModel [returnFromDeliveryBoyId=" + returnFromDeliveryBoyId + ", productName="
				+ productName + ", productId=" + productId + ", returnQuantity=" + returnQuantity + ", issuedQuantity="
				+ issuedQuantity + ", deliveryQuantity=" + deliveryQuantity + ", damageQuantity=" + damageQuantity
				+ ", nonDamageQuantity=" + nonDamageQuantity + ", type=" + type + ", reason=" + reason + "]";
	}

}
