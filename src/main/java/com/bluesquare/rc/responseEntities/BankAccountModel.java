package com.bluesquare.rc.responseEntities;

public class BankAccountModel {

	private long id;

	private String accountHolderName;

	private String accountNumber;

	private String accountType;

	
	private long bankId;
	private String bankName;
	private String bankShortName;

	private String ifscCode;

	private String branchCode;

	private String bankAddress;

	private double minimumBalance;

	private double actualBalance;

	private double drawableBalance;

	private double chequeIssuedAmount;

	private double chequeDepositedAmount;

	private String insertedDate;

	private String updatedDate;

	private boolean status;

	private boolean primary;
	
	

	public BankAccountModel(long id, String accountHolderName, String accountNumber, String accountType, long bankId,
			String bankName, String bankShortName, String ifscCode, String branchCode, String bankAddress,
			double minimumBalance, double actualBalance, double drawableBalance, double chequeIssuedAmount,
			double chequeDepositedAmount, String insertedDate, String updatedDate, boolean status, boolean primary) {
		super();
		this.id = id;
		this.accountHolderName = accountHolderName;
		this.accountNumber = accountNumber;
		this.accountType = accountType;
		this.bankId = bankId;
		this.bankName = bankName;
		this.bankShortName = bankShortName;
		this.ifscCode = ifscCode;
		this.branchCode = branchCode;
		this.bankAddress = bankAddress;
		this.minimumBalance = minimumBalance;
		this.actualBalance = actualBalance;
		this.drawableBalance = drawableBalance;
		this.chequeIssuedAmount = chequeIssuedAmount;
		this.chequeDepositedAmount = chequeDepositedAmount;
		this.insertedDate = insertedDate;
		this.updatedDate = updatedDate;
		this.status = status;
		this.primary = primary;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankShortName() {
		return bankShortName;
	}

	public void setBankShortName(String bankShortName) {
		this.bankShortName = bankShortName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public double getMinimumBalance() {
		return minimumBalance;
	}

	public void setMinimumBalance(double minimumBalance) {
		this.minimumBalance = minimumBalance;
	}

	public double getActualBalance() {
		return actualBalance;
	}

	public void setActualBalance(double actualBalance) {
		this.actualBalance = actualBalance;
	}

	public double getDrawableBalance() {
		return drawableBalance;
	}

	public void setDrawableBalance(double drawableBalance) {
		this.drawableBalance = drawableBalance;
	}

	public double getChequeIssuedAmount() {
		return chequeIssuedAmount;
	}

	public void setChequeIssuedAmount(double chequeIssuedAmount) {
		this.chequeIssuedAmount = chequeIssuedAmount;
	}

	public double getChequeDepositedAmount() {
		return chequeDepositedAmount;
	}

	public void setChequeDepositedAmount(double chequeDepositedAmount) {
		this.chequeDepositedAmount = chequeDepositedAmount;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	@Override
	public String toString() {
		return "BankAccountModel [id=" + id + ", accountHolderName=" + accountHolderName + ", accountNumber="
				+ accountNumber + ", accountType=" + accountType + ", bankId=" + bankId + ", bankName=" + bankName
				+ ", bankShortName=" + bankShortName + ", ifscCode=" + ifscCode + ", branchCode=" + branchCode
				+ ", bankAddress=" + bankAddress + ", minimumBalance=" + minimumBalance + ", actualBalance="
				+ actualBalance + ", drawableBalance=" + drawableBalance + ", chequeIssuedAmount=" + chequeIssuedAmount
				+ ", chequeDepositedAmount=" + chequeDepositedAmount + ", insertedDate=" + insertedDate
				+ ", updatedDate=" + updatedDate + ", status=" + status + ", primary=" + primary + "]";
	}

	
	


}
