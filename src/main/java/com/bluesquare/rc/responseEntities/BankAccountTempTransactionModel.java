package com.bluesquare.rc.responseEntities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

public class BankAccountTempTransactionModel {

	private long id;

	private String transactionId;

	private String partyName;

	private double credit;

	private double debit;

	private String payMode;

	private String referenceNo;

	private String bankName;

	private String insertedDate;

	private String chequeDate;

	private String transactionStatus;

	private String reason;

	private String updatedDate;

	public BankAccountTempTransactionModel(long id, String transactionId, String partyName, double credit, double debit,
			String payMode, String referenceNo, String bankName, String insertedDate, String chequeDate,
			String transactionStatus, String reason, String updatedDate) {
		super();
		this.id = id;
		this.transactionId = transactionId;
		this.partyName = partyName;
		this.credit = credit;
		this.debit = debit;
		this.payMode = payMode;
		this.referenceNo = referenceNo;
		this.bankName = bankName;
		this.insertedDate = insertedDate;
		this.chequeDate = chequeDate;
		this.transactionStatus = transactionStatus;
		this.reason = reason;
		this.updatedDate = updatedDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "BankAccountTempTransactionModel [id=" + id + ", transactionId=" + transactionId + ", partyName="
				+ partyName + ", credit=" + credit + ", debit=" + debit + ", payMode=" + payMode + ", referenceNo="
				+ referenceNo + ", bankName=" + bankName + ", insertedDate=" + insertedDate + ", chequeDate="
				+ chequeDate + ", transactionStatus=" + transactionStatus + ", reason=" + reason + ", updatedDate="
				+ updatedDate + "]";
	}

}
