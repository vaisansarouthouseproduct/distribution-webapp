package com.bluesquare.rc.responseEntities;

import java.util.Date;

import com.bluesquare.rc.entities.EmployeeDetails;

public class MeetingModel {

	private long meetingId;

	private EmployeeDetails employeeDetails;

	private BusinessNameModel businessName;

	private String ownerName;

	private String shopName;

	private String mobileNumber;

	private String address;

	private String meetingVenue;

	private String cancelReason;

	private String meetingStatus;

	private String meetingReview;

	private Date meetingFromDateTime;

	private Date meetingToDateTime;

	private Date addedDateTime;

	private Date updatedDateTime;

	private Date cancelDateTime;

	public MeetingModel(long meetingId, EmployeeDetails employeeDetails, BusinessNameModel businessName, String ownerName,
			String shopName, String mobileNumber, String address, String meetingVenue, String cancelReason,
			String meetingStatus, String meetingReview, Date meetingFromDateTime, Date meetingToDateTime,
			Date addedDateTime, Date updatedDateTime, Date cancelDateTime) {
		super();
		this.meetingId = meetingId;
		this.employeeDetails = employeeDetails;
		this.businessName = businessName;
		this.ownerName = ownerName;
		this.shopName = shopName;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.meetingVenue = meetingVenue;
		this.cancelReason = cancelReason;
		this.meetingStatus = meetingStatus;
		this.meetingReview = meetingReview;
		this.meetingFromDateTime = meetingFromDateTime;
		this.meetingToDateTime = meetingToDateTime;
		this.addedDateTime = addedDateTime;
		this.updatedDateTime = updatedDateTime;
		this.cancelDateTime = cancelDateTime;
	}

	public long getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(long meetingId) {
		this.meetingId = meetingId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public BusinessNameModel getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessNameModel businessName) {
		this.businessName = businessName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMeetingVenue() {
		return meetingVenue;
	}

	public void setMeetingVenue(String meetingVenue) {
		this.meetingVenue = meetingVenue;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getMeetingStatus() {
		return meetingStatus;
	}

	public void setMeetingStatus(String meetingStatus) {
		this.meetingStatus = meetingStatus;
	}

	public String getMeetingReview() {
		return meetingReview;
	}

	public void setMeetingReview(String meetingReview) {
		this.meetingReview = meetingReview;
	}

	public Date getMeetingFromDateTime() {
		return meetingFromDateTime;
	}

	public void setMeetingFromDateTime(Date meetingFromDateTime) {
		this.meetingFromDateTime = meetingFromDateTime;
	}

	public Date getMeetingToDateTime() {
		return meetingToDateTime;
	}

	public void setMeetingToDateTime(Date meetingToDateTime) {
		this.meetingToDateTime = meetingToDateTime;
	}

	public Date getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(Date addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public Date getCancelDateTime() {
		return cancelDateTime;
	}

	public void setCancelDateTime(Date cancelDateTime) {
		this.cancelDateTime = cancelDateTime;
	}

	@Override
	public String toString() {
		return "Meeting [meetingId=" + meetingId + ", businessName=" + businessName + ", ownerName=" + ownerName
				+ ", shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", address=" + address
				+ ", meetingVenue=" + meetingVenue + ", cancelReason=" + cancelReason + ", meetingStatus="
				+ meetingStatus + ", meetingReview=" + meetingReview + ", meetingFromDateTime=" + meetingFromDateTime
				+ ", meetingToDateTime=" + meetingToDateTime + ", addedDateTime=" + addedDateTime + ", updatedDateTime="
				+ updatedDateTime + ", cancelDateTime=" + cancelDateTime + "]";
	}

}
