package com.bluesquare.rc.responseEntities;

public class DailyStockDetailsModel {

	private String productName;

	private long openingStock;

	private long addedStock;

	private long dispatchedStock;

	private long closingStock;

	public DailyStockDetailsModel(String productName, long openingStock, long addedStock, long dispatchedStock,
			long closingStock) {
		super();
		this.productName = productName;
		this.openingStock = openingStock;
		this.addedStock = addedStock;
		this.dispatchedStock = dispatchedStock;
		this.closingStock = closingStock;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(long openingStock) {
		this.openingStock = openingStock;
	}

	public long getAddedStock() {
		return addedStock;
	}

	public void setAddedStock(long addedStock) {
		this.addedStock = addedStock;
	}

	public long getDispatchedStock() {
		return dispatchedStock;
	}

	public void setDispatchedStock(long dispatchedStock) {
		this.dispatchedStock = dispatchedStock;
	}

	public long getClosingStock() {
		return closingStock;
	}

	public void setClosingStock(long closingStock) {
		this.closingStock = closingStock;
	}

	@Override
	public String toString() {
		return "DailyStockDetailsModel [productName=" + productName + ", openingStock=" + openingStock + ", addedStock="
				+ addedStock + ", dispatchedStock=" + dispatchedStock + ", closingStock=" + closingStock + "]";
	}

}
