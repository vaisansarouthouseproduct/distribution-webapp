package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class DamageDefineModel {

	private long damageDefineId;

	private String orderId;

	private String productName;

	private String collectingPerson;

	private String departmentName;

	private long qty;

	private Date date;

	private String reason;

	public DamageDefineModel(long damageDefineId, String orderId, String productName, String collectingPerson,
			String departmentName, long qty, Date date, String reason) {
		super();
		this.damageDefineId = damageDefineId;
		this.orderId = orderId;
		this.productName = productName;
		this.collectingPerson = collectingPerson;
		this.departmentName = departmentName;
		this.qty = qty;
		this.date = date;
		this.reason = reason;
	}

	public long getDamageDefineId() {
		return damageDefineId;
	}

	public void setDamageDefineId(long damageDefineId) {
		this.damageDefineId = damageDefineId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCollectingPerson() {
		return collectingPerson;
	}

	public void setCollectingPerson(String collectingPerson) {
		this.collectingPerson = collectingPerson;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public long getQty() {
		return qty;
	}

	public void setQty(long qty) {
		this.qty = qty;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "DamageDefineModel [damageDefineId=" + damageDefineId + ", orderId=" + orderId + ", productName="
				+ productName + ", collectingPerson=" + collectingPerson + ", departmentName=" + departmentName
				+ ", qty=" + qty + ", date=" + date + ", reason=" + reason + "]";
	}

}
