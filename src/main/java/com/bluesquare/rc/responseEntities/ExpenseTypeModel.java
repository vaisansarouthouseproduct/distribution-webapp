package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class ExpenseTypeModel {

	private long expenseTypeId;

	private String name;

	private Date addedDate;

	private Date updatedDate;

	public ExpenseTypeModel(long expenseTypeId, String name, Date addedDate, Date updatedDate) {
		super();
		this.expenseTypeId = expenseTypeId;
		this.name = name;
		this.addedDate = addedDate;
		this.updatedDate = updatedDate;
	}

	public long getExpenseTypeId() {
		return expenseTypeId;
	}

	public void setExpenseTypeId(long expenseTypeId) {
		this.expenseTypeId = expenseTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "ExpenseTypeModel [expenseTypeId=" + expenseTypeId + ", name=" + name + ", addedDate=" + addedDate
				+ ", updatedDate=" + updatedDate + "]";
	}

}
