package com.bluesquare.rc.responseEntities;

import java.util.Date;

import com.bluesquare.rc.entities.Contact;

public class ShopVisitModel {

	private long id;

	private String ownerName;

	private String shopName;

	private String address;

	private String comment;

	private Contact contact;

	private Date shopVisitAddeddDatetime;

	private Date nextVisitDate;

	private Date nextVisitTime;

	private Date nextVisitEndTime;

	private Date shopVisitUpdatedDatetime;

	private String isNextVisit;

	public ShopVisitModel() {
		// TODO Auto-generated constructor stub
	}
	
	public ShopVisitModel(long id, String ownerName, String shopName, String address, String comment, Contact contact,
			Date shopVisitAddeddDatetime, Date nextVisitDate, Date nextVisitTime, Date nextVisitEndTime,
			Date shopVisitUpdatedDatetime, String isNextVisit) {
		super();
		this.id = id;
		this.ownerName = ownerName;
		this.shopName = shopName;
		this.address = address;
		this.comment = comment;
		this.contact = contact;
		this.shopVisitAddeddDatetime = shopVisitAddeddDatetime;
		this.nextVisitDate = nextVisitDate;
		this.nextVisitTime = nextVisitTime;
		this.nextVisitEndTime = nextVisitEndTime;
		this.shopVisitUpdatedDatetime = shopVisitUpdatedDatetime;
		this.isNextVisit = isNextVisit;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Date getShopVisitAddeddDatetime() {
		return shopVisitAddeddDatetime;
	}

	public void setShopVisitAddeddDatetime(Date shopVisitAddeddDatetime) {
		this.shopVisitAddeddDatetime = shopVisitAddeddDatetime;
	}

	public Date getNextVisitDate() {
		return nextVisitDate;
	}

	public void setNextVisitDate(Date nextVisitDate) {
		this.nextVisitDate = nextVisitDate;
	}

	public Date getNextVisitTime() {
		return nextVisitTime;
	}

	public void setNextVisitTime(Date nextVisitTime) {
		this.nextVisitTime = nextVisitTime;
	}

	public Date getNextVisitEndTime() {
		return nextVisitEndTime;
	}

	public void setNextVisitEndTime(Date nextVisitEndTime) {
		this.nextVisitEndTime = nextVisitEndTime;
	}

	public Date getShopVisitUpdatedDatetime() {
		return shopVisitUpdatedDatetime;
	}

	public void setShopVisitUpdatedDatetime(Date shopVisitUpdatedDatetime) {
		this.shopVisitUpdatedDatetime = shopVisitUpdatedDatetime;
	}

	public String getIsNextVisit() {
		return isNextVisit;
	}

	public void setIsNextVisit(String isNextVisit) {
		this.isNextVisit = isNextVisit;
	}

	@Override
	public String toString() {
		return "ShopVisit [id=" + id + ", ownerName=" + ownerName + ", shopName=" + shopName + ", address=" + address
				+ ", comment=" + comment + ", contact=" + contact + ", shopVisitAddeddDatetime="
				+ shopVisitAddeddDatetime + ", nextVisitDate=" + nextVisitDate + ", nextVisitTime=" + nextVisitTime
				+ ", nextVisitEndTime=" + nextVisitEndTime + ", shopVisitUpdatedDatetime=" + shopVisitUpdatedDatetime
				+ ", isNextVisit=" + isNextVisit + "]";
	}

}
