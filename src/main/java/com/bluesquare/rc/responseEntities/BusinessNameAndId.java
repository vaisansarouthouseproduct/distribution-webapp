package com.bluesquare.rc.responseEntities;

public class BusinessNameAndId {

	private String businessNameId;
	private String shopName;

	public BusinessNameAndId(String businessNameId, String shopName) {
		super();
		this.businessNameId = businessNameId;
		this.shopName = shopName;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Override
	public String toString() {
		return "BusinessNameAndId [businessNameId=" + businessNameId + ", shopName=" + shopName + "]";
	}

}
