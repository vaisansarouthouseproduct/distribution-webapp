package com.bluesquare.rc.responseEntities;

public class AreaModel {

	private long areaId;

	private String name;

	private long pincode;

	private RegionModel region;

	public AreaModel(long areaId, String name, long pincode, RegionModel region) {
		super();
		this.areaId = areaId;
		this.name = name;
		this.pincode = pincode;
		this.region = region;
	}

	public AreaModel(long areaId, String name, long pincode) {
		super();
		this.areaId = areaId;
		this.name = name;
		this.pincode = pincode;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public RegionModel getRegion() {
		return region;
	}

	public void setRegion(RegionModel region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "AreaModel [areaId=" + areaId + ", name=" + name + ", pincode=" + pincode + ", region=" + region + "]";
	}

}