package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class SupplierOrderModel {

	private int supplierOrderPkId;

	private String supplierOrderId;

	private double totalAmount;

	private double totalAmountWithTax;

	private long totalQuantity;

	private Date supplierOrderDatetime;

	private Date supplierOrderUpdateDatetime;

	private boolean status;
	
	

	public SupplierOrderModel(int supplierOrderPkId, String supplierOrderId, double totalAmount,
			double totalAmountWithTax, long totalQuantity, Date supplierOrderDatetime, Date supplierOrderUpdateDatetime,
			boolean status) {
		super();
		this.supplierOrderPkId = supplierOrderPkId;
		this.supplierOrderId = supplierOrderId;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalQuantity = totalQuantity;
		this.supplierOrderDatetime = supplierOrderDatetime;
		this.supplierOrderUpdateDatetime = supplierOrderUpdateDatetime;
		this.status = status;
	}

	public String getSupplierOrderId() {
		return supplierOrderId;
	}

	public void setSupplierOrderId(String supplierOrderId) {
		this.supplierOrderId = supplierOrderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public Date getSupplierOrderDatetime() {
		return supplierOrderDatetime;
	}

	public void setSupplierOrderDatetime(Date supplierOrderDatetime) {
		this.supplierOrderDatetime = supplierOrderDatetime;
	}

	public Date getSupplierOrderUpdateDatetime() {
		return supplierOrderUpdateDatetime;
	}

	public void setSupplierOrderUpdateDatetime(Date supplierOrderUpdateDatetime) {
		this.supplierOrderUpdateDatetime = supplierOrderUpdateDatetime;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public int getSupplierOrderPkId() {
		return supplierOrderPkId;
	}

	public void setSupplierOrderPkId(int supplierOrderPkId) {
		this.supplierOrderPkId = supplierOrderPkId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SupplierOrder [supplierOrderPkId=" + supplierOrderPkId + ", supplierOrderId=" + supplierOrderId
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity="
				+ totalQuantity + ", supplierOrderDatetime=" + supplierOrderDatetime + ", supplierOrderUpdateDatetime="
				+ supplierOrderUpdateDatetime + ", status=" + status + "]";
	}

}
