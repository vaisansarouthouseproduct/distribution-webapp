package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class ReIssueOrderDetailsModel {
	private String orderId;
	private String returnId;
	private String shopName;
	private String address;
	private String mobileNo;
	private Date dateOfReturn;
	private long resIssueId;
	private long totalReIssueQuantity;
	private long totalReturnQuantity;

	public ReIssueOrderDetailsModel(String orderId, String returnId, String shopName, String address, String mobileNo,
			Date dateOfReturn, long resIssueId, long totalReIssueQuantity, long totalReturnQuantity) {
		super();
		this.orderId = orderId;
		this.returnId = returnId;
		this.shopName = shopName;
		this.address = address;
		this.mobileNo = mobileNo;
		this.dateOfReturn = dateOfReturn;
		this.resIssueId = resIssueId;
		this.totalReIssueQuantity = totalReIssueQuantity;
		this.totalReturnQuantity = totalReturnQuantity;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getReturnId() {
		return returnId;
	}

	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}

	public long getResIssueId() {
		return resIssueId;
	}

	public void setResIssueId(long resIssueId) {
		this.resIssueId = resIssueId;
	}

	public long getTotalReIssueQuantity() {
		return totalReIssueQuantity;
	}

	public void setTotalReIssueQuantity(long totalReIssueQuantity) {
		this.totalReIssueQuantity = totalReIssueQuantity;
	}

	public long getTotalReturnQuantity() {
		return totalReturnQuantity;
	}

	public void setTotalReturnQuantity(long totalReturnQuantity) {
		this.totalReturnQuantity = totalReturnQuantity;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsModel [orderId=" + orderId + ", returnId=" + returnId + ", shopName=" + shopName
				+ ", address=" + address + ", mobileNo=" + mobileNo + ", dateOfReturn=" + dateOfReturn + ", resIssueId="
				+ resIssueId + ", totalReIssueQuantity=" + totalReIssueQuantity + ", totalReturnQuantity="
				+ totalReturnQuantity + "]";
	}

}