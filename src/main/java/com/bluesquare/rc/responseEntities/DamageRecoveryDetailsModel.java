package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class DamageRecoveryDetailsModel {

	private long damageRecoveryDetailsPkId;

	private String damageRecoveryDetailsId;

	private SupplierModel supplier;

	private long quantityGiven;

	private long quantityReceived;

	private long quantityNotClaimed;

	private Date givenDate;

	private Date receiveDate;

	private boolean receiveStatus;

	private String rejectQuantityReason;

	public DamageRecoveryDetailsModel(long damageRecoveryDetailsPkId, String damageRecoveryDetailsId,
			SupplierModel supplier, long quantityGiven, long quantityReceived, long quantityNotClaimed, Date givenDate,
			Date receiveDate, boolean receiveStatus, String rejectQuantityReason) {
		super();
		this.damageRecoveryDetailsPkId = damageRecoveryDetailsPkId;
		this.damageRecoveryDetailsId = damageRecoveryDetailsId;
		this.supplier = supplier;
		this.quantityGiven = quantityGiven;
		this.quantityReceived = quantityReceived;
		this.quantityNotClaimed = quantityNotClaimed;
		this.givenDate = givenDate;
		this.receiveDate = receiveDate;
		this.receiveStatus = receiveStatus;
		this.rejectQuantityReason = rejectQuantityReason;
	}

	public long getDamageRecoveryDetailsPkId() {
		return damageRecoveryDetailsPkId;
	}

	public void setDamageRecoveryDetailsPkId(long damageRecoveryDetailsPkId) {
		this.damageRecoveryDetailsPkId = damageRecoveryDetailsPkId;
	}

	public String getDamageRecoveryDetailsId() {
		return damageRecoveryDetailsId;
	}

	public void setDamageRecoveryDetailsId(String damageRecoveryDetailsId) {
		this.damageRecoveryDetailsId = damageRecoveryDetailsId;
	}

	public SupplierModel getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierModel supplier) {
		this.supplier = supplier;
	}

	public long getQuantityGiven() {
		return quantityGiven;
	}

	public void setQuantityGiven(long quantityGiven) {
		this.quantityGiven = quantityGiven;
	}

	public long getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(long quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public long getQuantityNotClaimed() {
		return quantityNotClaimed;
	}

	public void setQuantityNotClaimed(long quantityNotClaimed) {
		this.quantityNotClaimed = quantityNotClaimed;
	}

	public Date getGivenDate() {
		return givenDate;
	}

	public void setGivenDate(Date givenDate) {
		this.givenDate = givenDate;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public boolean isReceiveStatus() {
		return receiveStatus;
	}

	public void setReceiveStatus(boolean receiveStatus) {
		this.receiveStatus = receiveStatus;
	}

	public String getRejectQuantityReason() {
		return rejectQuantityReason;
	}

	public void setRejectQuantityReason(String rejectQuantityReason) {
		this.rejectQuantityReason = rejectQuantityReason;
	}

	@Override
	public String toString() {
		return "DamageRecoveryDetailsModel [damageRecoveryDetailsPkId=" + damageRecoveryDetailsPkId
				+ ", damageRecoveryDetailsId=" + damageRecoveryDetailsId + ", supplier=" + supplier + ", quantityGiven="
				+ quantityGiven + ", quantityReceived=" + quantityReceived + ", quantityNotClaimed="
				+ quantityNotClaimed + ", givenDate=" + givenDate + ", receiveDate=" + receiveDate + ", receiveStatus="
				+ receiveStatus + ", rejectQuantityReason=" + rejectQuantityReason + "]";
	}

}
