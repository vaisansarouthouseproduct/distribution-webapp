package com.bluesquare.rc.responseEntities;

public class RegionModel {

	private long regionId;

	private String name;

	private CityModel city;

	public RegionModel(long regionId, String name, CityModel city) {
		super();
		this.regionId = regionId;
		this.name = name;
		this.city = city;
	}

	public RegionModel(long regionId, String name) {
		super();
		this.regionId = regionId;
		this.name = name;
	}

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CityModel getCity() {
		return city;
	}

	public void setCity(CityModel city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "RegionModel [regionId=" + regionId + ", name=" + name + ", city=" + city + "]";
	}

}