package com.bluesquare.rc.responseEntities;

import com.bluesquare.rc.entities.OrderUsedProduct;

public class OrderProductDetailsModel {

	private long orderProductDetailsid;

	private long purchaseQuantity;

	private String details;

	private long issuedQuantity;

	private double sellingRate;

	private double purchaseAmount;

	private double issueAmount;

	private long confirmQuantity;

	private double confirmAmount;

	private OrderUsedProduct product;

	private String productName;

	private String type;

	public OrderProductDetailsModel() {
		// TODO Auto-generated constructor stub
	}

	public OrderProductDetailsModel(long orderProductDetailsid, long purchaseQuantity, String details,
			long issuedQuantity, double sellingRate, double purchaseAmount, double issueAmount, long confirmQuantity,
			double confirmAmount, OrderUsedProduct product, String productName, String type) {
		super();
		this.orderProductDetailsid = orderProductDetailsid;
		this.purchaseQuantity = purchaseQuantity;
		this.details = details;
		this.issuedQuantity = issuedQuantity;
		this.sellingRate = sellingRate;
		this.purchaseAmount = purchaseAmount;
		this.issueAmount = issueAmount;
		this.confirmQuantity = confirmQuantity;
		this.confirmAmount = confirmAmount;
		this.product = product;
		this.productName = productName;
		this.type = type;
	}

	public long getOrderProductDetailsid() {
		return orderProductDetailsid;
	}

	public void setOrderProductDetailsid(long orderProductDetailsid) {
		this.orderProductDetailsid = orderProductDetailsid;
	}

	public long getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(long purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public double getIssueAmount() {
		return issueAmount;
	}

	public void setIssueAmount(double issueAmount) {
		this.issueAmount = issueAmount;
	}

	public long getConfirmQuantity() {
		return confirmQuantity;
	}

	public void setConfirmQuantity(long confirmQuantity) {
		this.confirmQuantity = confirmQuantity;
	}

	public double getConfirmAmount() {
		return confirmAmount;
	}

	public void setConfirmAmount(double confirmAmount) {
		this.confirmAmount = confirmAmount;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "OrderProductDetailsModel [orderProductDetailsid=" + orderProductDetailsid + ", purchaseQuantity="
				+ purchaseQuantity + ", details=" + details + ", issuedQuantity=" + issuedQuantity + ", sellingRate="
				+ sellingRate + ", purchaseAmount=" + purchaseAmount + ", issueAmount=" + issueAmount
				+ ", confirmQuantity=" + confirmQuantity + ", confirmAmount=" + confirmAmount + ", product=" + product
				+ ", productName=" + productName + ", type=" + type + "]";
	}

}
