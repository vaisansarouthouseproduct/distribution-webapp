package com.bluesquare.rc.responseEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

public class NotePadModel {

	private long notepadId;

	private String notepadTitle;

	private String notepadText;

	private Date notepadAddedDateTime;

	private Date notepadUpdatedDateTime;

	public NotePadModel() {
		// TODO Auto-generated constructor stub
	}
	
	public NotePadModel(long notepadId, String notepadTitle, String notepadText, Date notepadAddedDateTime,
			Date notepadUpdatedDateTime) {
		super();
		this.notepadId = notepadId;
		this.notepadTitle = notepadTitle;
		this.notepadText = notepadText;
		this.notepadAddedDateTime = notepadAddedDateTime;
		this.notepadUpdatedDateTime = notepadUpdatedDateTime;
	}

	public long getNotepadId() {
		return notepadId;
	}

	public void setNotepadId(long notepadId) {
		this.notepadId = notepadId;
	}

	public String getNotepadTitle() {
		return notepadTitle;
	}

	public void setNotepadTitle(String notepadTitle) {
		this.notepadTitle = notepadTitle;
	}

	public String getNotepadText() {
		return notepadText;
	}

	public void setNotepadText(String notepadText) {
		this.notepadText = notepadText;
	}

	public Date getNotepadAddedDateTime() {
		return notepadAddedDateTime;
	}

	public void setNotepadAddedDateTime(Date notepadAddedDateTime) {
		this.notepadAddedDateTime = notepadAddedDateTime;
	}

	public Date getNotepadUpdatedDateTime() {
		return notepadUpdatedDateTime;
	}

	public void setNotepadUpdatedDateTime(Date notepadUpdatedDateTime) {
		this.notepadUpdatedDateTime = notepadUpdatedDateTime;
	}

	@Override
	public String toString() {
		return "NotePad [notepadId=" + notepadId + ", notepadTitle=" + notepadTitle + ", notepadText=" + notepadText
				+ ", notepadAddedDateTime=" + notepadAddedDateTime + ", notepadUpdatedDateTime="
				+ notepadUpdatedDateTime + "]";
	}

}
