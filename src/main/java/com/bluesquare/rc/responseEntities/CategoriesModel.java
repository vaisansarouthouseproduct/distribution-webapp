package com.bluesquare.rc.responseEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

public class CategoriesModel {

	private long categoryId;

	private String categoryName;

	private String categoryDescription;

	private String hsnCode;

	private float cgst;

	private float sgst;

	private float igst;

	private Date categoryDate;

	private Date categoryUpdateDate;

	public CategoriesModel(long categoryId, String categoryName, String categoryDescription, String hsnCode, float cgst,
			float sgst, float igst, Date categoryDate, Date categoryUpdateDate) {
		super();
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.categoryDescription = categoryDescription;
		this.hsnCode = hsnCode;
		this.cgst = cgst;
		this.sgst = sgst;
		this.igst = igst;
		this.categoryDate = categoryDate;
		this.categoryUpdateDate = categoryUpdateDate;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public float getCgst() {
		return cgst;
	}

	public void setCgst(float cgst) {
		this.cgst = cgst;
	}

	public float getSgst() {
		return sgst;
	}

	public void setSgst(float sgst) {
		this.sgst = sgst;
	}

	public float getIgst() {
		return igst;
	}

	public void setIgst(float igst) {
		this.igst = igst;
	}

	public Date getCategoryDate() {
		return categoryDate;
	}

	public void setCategoryDate(Date categoryDate) {
		this.categoryDate = categoryDate;
	}

	public Date getCategoryUpdateDate() {
		return categoryUpdateDate;
	}

	public void setCategoryUpdateDate(Date categoryUpdateDate) {
		this.categoryUpdateDate = categoryUpdateDate;
	}

	@Override
	public String toString() {
		return "CategoriesModel [categoryId=" + categoryId + ", categoryName=" + categoryName + ", categoryDescription="
				+ categoryDescription + ", hsnCode=" + hsnCode + ", cgst=" + cgst + ", sgst=" + sgst + ", igst=" + igst
				+ ", categoryDate=" + categoryDate + ", categoryUpdateDate=" + categoryUpdateDate + "]";
	}

}
