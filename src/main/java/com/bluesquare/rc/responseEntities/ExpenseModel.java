package com.bluesquare.rc.responseEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ExpenseModel {

	private long expensePkId;

	private String expenseId;

	private String reference;

	private double amount;

	private String type;

	private String chequeNumber;

	private String bankName;

	private Date addDate;

	private Date updateDate;

	private long expenseTypeId;

	private long paymentMethodId;

	private String transactionReferenceNumber;

	private String comment;

	public ExpenseModel(long expensePkId, String expenseId, String reference, double amount, String type,
			String chequeNumber, String bankName, Date addDate, Date updateDate, long expenseTypeId,
			long paymentMethodId, String transactionReferenceNumber, String comment) {
		super();
		this.expensePkId = expensePkId;
		this.expenseId = expenseId;
		this.reference = reference;
		this.amount = amount;
		this.type = type;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.addDate = addDate;
		this.updateDate = updateDate;
		this.expenseTypeId = expenseTypeId;
		this.paymentMethodId = paymentMethodId;
		this.transactionReferenceNumber = transactionReferenceNumber;
		this.comment = comment;
	}

	public long getExpensePkId() {
		return expensePkId;
	}

	public void setExpensePkId(long expensePkId) {
		this.expensePkId = expensePkId;
	}

	public String getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public long getExpenseTypeId() {
		return expenseTypeId;
	}

	public void setExpenseTypeId(long expenseTypeId) {
		this.expenseTypeId = expenseTypeId;
	}

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ExpenseModel [expensePkId=" + expensePkId + ", expenseId=" + expenseId + ", reference=" + reference
				+ ", amount=" + amount + ", type=" + type + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName
				+ ", addDate=" + addDate + ", updateDate=" + updateDate + ", expenseTypeId=" + expenseTypeId
				+ ", paymentMethodId=" + paymentMethodId + ", transactionReferenceNumber=" + transactionReferenceNumber
				+ ", comment=" + comment + "]";
	}

}
