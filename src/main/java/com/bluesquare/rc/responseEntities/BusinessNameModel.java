package com.bluesquare.rc.responseEntities;

import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;

public class BusinessNameModel {

	private long businessNamePkId;

	private String businessNameId;

	private String shopName;

	private String ownerName;

	private double creditLimit;

	private String gstinNumber;

	private BusinessTypeModel businessType;

	private String address;

	private String taxType;

	private Contact contact;

	private AreaModel area;

	private EmployeeNameAndId employeeSM;

	private boolean status;

	private boolean badDebtsStatus;

	public BusinessNameModel(long businessNamePkId, String businessNameId, String shopName, String ownerName,
			double creditLimit, String gstinNumber, BusinessTypeModel businessType, String address, String taxType,
			Contact contact, AreaModel area, EmployeeNameAndId employeeSM, boolean status, boolean badDebtsStatus) {
		super();
		this.businessNamePkId = businessNamePkId;
		this.businessNameId = businessNameId;
		this.shopName = shopName;
		this.ownerName = ownerName;
		this.creditLimit = creditLimit;
		this.gstinNumber = gstinNumber;
		this.businessType = businessType;
		this.address = address;
		this.taxType = taxType;
		this.contact = contact;
		this.area = area;
		this.employeeSM = employeeSM;
		this.status = status;
		this.badDebtsStatus = badDebtsStatus;
	}

	public long getBusinessNamePkId() {
		return businessNamePkId;
	}

	public void setBusinessNamePkId(long businessNamePkId) {
		this.businessNamePkId = businessNamePkId;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getGstinNumber() {
		return gstinNumber;
	}

	public void setGstinNumber(String gstinNumber) {
		this.gstinNumber = gstinNumber;
	}

	public BusinessTypeModel getBusinessType() {
		return businessType;
	}

	public void setBusinessType(BusinessTypeModel businessType) {
		this.businessType = businessType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public AreaModel getArea() {
		return area;
	}

	public void setArea(AreaModel area) {
		this.area = area;
	}

	public EmployeeNameAndId getEmployeeSM() {
		return employeeSM;
	}

	public void setEmployeeSM(EmployeeNameAndId employeeSM) {
		this.employeeSM = employeeSM;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isBadDebtsStatus() {
		return badDebtsStatus;
	}

	public void setBadDebtsStatus(boolean badDebtsStatus) {
		this.badDebtsStatus = badDebtsStatus;
	}

	@Override
	public String toString() {
		return "BusinessNameModel [businessNamePkId=" + businessNamePkId + ", businessNameId=" + businessNameId
				+ ", shopName=" + shopName + ", ownerName=" + ownerName + ", creditLimit=" + creditLimit
				+ ", gstinNumber=" + gstinNumber + ", businessType=" + businessType + ", address=" + address
				+ ", taxType=" + taxType + ", contact=" + contact + ", area=" + area + ", employeeSM=" + employeeSM
				+ ", status=" + status + ", badDebtsStatus=" + badDebtsStatus + "]";
	}

}
