package com.bluesquare.rc.responseEntities;

import com.bluesquare.rc.entities.Country;

public class StateModel {

	private long stateId;

	private String code;

	private String name;

	private Country country;

	public StateModel(long stateId, String code, String name) {
		super();
		this.stateId = stateId;
		this.code = code;
		this.name = name;
	}

	public StateModel(long stateId, String code, String name, Country country) {
		super();
		this.stateId = stateId;
		this.code = code;
		this.name = name;
		this.country = country;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "StateModel [stateId=" + stateId + ", code=" + code + ", name=" + name + ", country=" + country + "]";
	}

	
}
