package com.bluesquare.rc.responseEntities;

import java.util.Date;

import com.bluesquare.rc.entities.Contact;

public class EmployeeDetailsModel {

	private long employeeDetailsId;

	private String employeeDetailsGenId;

	private String name;

	private String address;

	private double basicSalary;

	private Contact contact;

	private Date employeeDetailsAddedDatetime;

	private Date employeeDetailsUpdatedDatetime;

	private boolean status;

	private Date employeeDetailsDisableDatetime;

	public EmployeeDetailsModel(long employeeDetailsId, String employeeDetailsGenId, String name, String address,
			double basicSalary, Contact contact, Date employeeDetailsAddedDatetime, Date employeeDetailsUpdatedDatetime,
			boolean status, Date employeeDetailsDisableDatetime) {
		super();
		this.employeeDetailsId = employeeDetailsId;
		this.employeeDetailsGenId = employeeDetailsGenId;
		this.name = name;
		this.address = address;
		this.basicSalary = basicSalary;
		this.contact = contact;
		this.employeeDetailsAddedDatetime = employeeDetailsAddedDatetime;
		this.employeeDetailsUpdatedDatetime = employeeDetailsUpdatedDatetime;
		this.status = status;
		this.employeeDetailsDisableDatetime = employeeDetailsDisableDatetime;
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getEmployeeDetailsGenId() {
		return employeeDetailsGenId;
	}

	public void setEmployeeDetailsGenId(String employeeDetailsGenId) {
		this.employeeDetailsGenId = employeeDetailsGenId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Date getEmployeeDetailsAddedDatetime() {
		return employeeDetailsAddedDatetime;
	}

	public void setEmployeeDetailsAddedDatetime(Date employeeDetailsAddedDatetime) {
		this.employeeDetailsAddedDatetime = employeeDetailsAddedDatetime;
	}

	public Date getEmployeeDetailsUpdatedDatetime() {
		return employeeDetailsUpdatedDatetime;
	}

	public void setEmployeeDetailsUpdatedDatetime(Date employeeDetailsUpdatedDatetime) {
		this.employeeDetailsUpdatedDatetime = employeeDetailsUpdatedDatetime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getEmployeeDetailsDisableDatetime() {
		return employeeDetailsDisableDatetime;
	}

	public void setEmployeeDetailsDisableDatetime(Date employeeDetailsDisableDatetime) {
		this.employeeDetailsDisableDatetime = employeeDetailsDisableDatetime;
	}

	@Override
	public String toString() {
		return "EmployeeDetailsModel [employeeDetailsId=" + employeeDetailsId + ", employeeDetailsGenId="
				+ employeeDetailsGenId + ", name=" + name + ", address=" + address + ", basicSalary=" + basicSalary
				+ ", contact=" + contact + ", employeeDetailsAddedDatetime=" + employeeDetailsAddedDatetime
				+ ", employeeDetailsUpdatedDatetime=" + employeeDetailsUpdatedDatetime + ", status=" + status
				+ ", employeeDetailsDisableDatetime=" + employeeDetailsDisableDatetime + "]";
	}

}
