package com.bluesquare.rc.responseEntities;

import java.util.Date;

import com.bluesquare.rc.entities.Product;

public class ProductModel {

	private long productId;

	private String productName;

	private String productCode;

	private CategoriesModel categories;

	private BrandModel brand;

	private ProductModel product;

	private float rate;

	/* private String productContentType; */

	private String productDescription;

	private long threshold;

	private long currentQuantity;

	private long damageQuantity;

	private long freeQuantity;

	private Date productAddedDatetime;

	private Date productQuantityUpdatedDatetime;

	private String productBarcode;
	
	public ProductModel() {
		// TODO Auto-generated constructor stub
	}

	public ProductModel(long productId, String productName, String productCode, CategoriesModel categories,
			BrandModel brand, float rate, String productDescription, long threshold, long currentQuantity,
			long damageQuantity, long freeQuantity, Date productAddedDatetime, Date productQuantityUpdatedDatetime,
			String productBarcode, ProductModel product) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCode = productCode;
		this.categories = categories;
		this.brand = brand;
		this.rate = rate;
		this.productDescription = productDescription;
		this.threshold = threshold;
		this.currentQuantity = currentQuantity;
		this.damageQuantity = damageQuantity;
		this.freeQuantity = freeQuantity;
		this.productAddedDatetime = productAddedDatetime;
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
		this.productBarcode = productBarcode;
		this.product = product;
	}

	public String getProductBarcode() {
		return productBarcode;
	}

	public void setProductBarcode(String productBarcode) {
		this.productBarcode = productBarcode;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public CategoriesModel getCategories() {
		return categories;
	}

	public void setCategories(CategoriesModel categories) {
		this.categories = categories;
	}

	public BrandModel getBrand() {
		return brand;
	}

	public void setBrand(BrandModel brand) {
		this.brand = brand;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public long getThreshold() {
		return threshold;
	}

	public void setThreshold(long threshold) {
		this.threshold = threshold;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public long getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(long freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}

	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}

	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}

	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductModel [productId=" + productId + ", productName=" + productName + ", productCode=" + productCode
				+ ", categories=" + categories + ", brand=" + brand + ", product=" + product + ", rate=" + rate
				+ ", productDescription=" + productDescription + ", threshold=" + threshold + ", currentQuantity="
				+ currentQuantity + ", damageQuantity=" + damageQuantity + ", freeQuantity=" + freeQuantity
				+ ", productAddedDatetime=" + productAddedDatetime + ", productQuantityUpdatedDatetime="
				+ productQuantityUpdatedDatetime + ", productBarcode=" + productBarcode + "]";
	}

}
