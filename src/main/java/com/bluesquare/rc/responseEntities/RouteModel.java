package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class RouteModel {

	private long routeId;

	private String routeName;

	private String routeGenId;

	private boolean status;

	private Date addedDatetime;

	private Date updatedDatetime;

	private Date deleteDatetime;

	
	public long getRouteId() {
		return routeId;
	}

	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteGenId() {
		return routeGenId;
	}

	public void setRouteGenId(String routeGenId) {
		this.routeGenId = routeGenId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(Date addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getDeleteDatetime() {
		return deleteDatetime;
	}

	public void setDeleteDatetime(Date deleteDatetime) {
		this.deleteDatetime = deleteDatetime;
	}

	@Override
	public String toString() {
		return "Route [routeId=" + routeId + ", routeName=" + routeName + ", routeGenId=" + routeGenId + ", status="
				+ status + ", addedDatetime=" + addedDatetime + ", updatedDatetime=" + updatedDatetime
				+ ", deleteDatetime=" + deleteDatetime + "]";
	}

}
