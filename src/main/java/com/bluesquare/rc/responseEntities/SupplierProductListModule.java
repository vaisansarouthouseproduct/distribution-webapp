package com.bluesquare.rc.responseEntities;

public class SupplierProductListModule {

	private float igst;

	private String supplierId;

	private String supplierName;

	private long productId;

	private String productName;

	private long categoryId;
	private String categoryName;

	private long brandId;
	private String brandName;

	private double supplierRate;

	public SupplierProductListModule(float igst, String supplierId, String supplierName, long productId,
			String productName, long categoryId, String categoryName, long brandId, String brandName,
			double supplierRate) {
		super();
		this.igst = igst;
		this.supplierId = supplierId;
		this.supplierName = supplierName;
		this.productId = productId;
		this.productName = productName;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.brandId = brandId;
		this.brandName = brandName;
		this.supplierRate = supplierRate;
	}

	public float getIgst() {
		return igst;
	}

	public void setIgst(float igst) {
		this.igst = igst;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public double getSupplierRate() {
		return supplierRate;
	}

	public void setSupplierRate(double supplierRate) {
		this.supplierRate = supplierRate;
	}

	@Override
	public String toString() {
		return "SupplierProductListModule [igst=" + igst + ", supplierId=" + supplierId + ", supplierName="
				+ supplierName + ", productId=" + productId + ", productName=" + productName + ", categoryId="
				+ categoryId + ", categoryName=" + categoryName + ", brandId=" + brandId + ", brandName=" + brandName
				+ ", supplierRate=" + supplierRate + "]";
	}

}
