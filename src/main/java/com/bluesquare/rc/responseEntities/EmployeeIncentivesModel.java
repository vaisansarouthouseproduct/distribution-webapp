package com.bluesquare.rc.responseEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class EmployeeIncentivesModel {

	private long employeeIncentiveId;

	private double incentiveAmount;

	private String reason;

	private Date incentiveGivenDate;

	public EmployeeIncentivesModel(long employeeIncentiveId, double incentiveAmount, String reason,
			Date incentiveGivenDate) {
		super();
		this.employeeIncentiveId = employeeIncentiveId;
		this.incentiveAmount = incentiveAmount;
		this.reason = reason;
		this.incentiveGivenDate = incentiveGivenDate;
	}

	public long getEmployeeIncentiveId() {
		return employeeIncentiveId;
	}

	public void setEmployeeIncentiveId(long employeeIncentiveId) {
		this.employeeIncentiveId = employeeIncentiveId;
	}

	public double getIncentiveAmount() {
		return incentiveAmount;
	}

	public void setIncentiveAmount(double incentiveAmount) {
		this.incentiveAmount = incentiveAmount;
	}

	public Date getIncentiveGivenDate() {
		return incentiveGivenDate;
	}

	public void setIncentiveGivenDate(Date incentiveGivenDate) {
		this.incentiveGivenDate = incentiveGivenDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "EmployeeIncentives [employeeIncentiveId=" + employeeIncentiveId + ", incentiveAmount=" + incentiveAmount
				+ ", reason=" + reason + ", incentiveGivenDate=" + incentiveGivenDate + "]";
	}

}
