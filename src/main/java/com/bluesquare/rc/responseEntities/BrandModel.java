package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class BrandModel {

	private long brandId;

	private String name;

	private Date brandAddedDatetime;

	private Date brandUpdateDatetime;

	public BrandModel(long brandId, String name, Date brandAddedDatetime, Date brandUpdateDatetime) {
		super();
		this.brandId = brandId;
		this.name = name;
		this.brandAddedDatetime = brandAddedDatetime;
		this.brandUpdateDatetime = brandUpdateDatetime;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrandAddedDatetime() {
		return brandAddedDatetime;
	}

	public void setBrandAddedDatetime(Date brandAddedDatetime) {
		this.brandAddedDatetime = brandAddedDatetime;
	}

	public Date getBrandUpdateDatetime() {
		return brandUpdateDatetime;
	}

	public void setBrandUpdateDatetime(Date brandUpdateDatetime) {
		this.brandUpdateDatetime = brandUpdateDatetime;
	}

	@Override
	public String toString() {
		return "BrandModel [brandId=" + brandId + ", name=" + name + ", brandAddedDatetime=" + brandAddedDatetime
				+ ", brandUpdateDatetime=" + brandUpdateDatetime + "]";
	}

}
