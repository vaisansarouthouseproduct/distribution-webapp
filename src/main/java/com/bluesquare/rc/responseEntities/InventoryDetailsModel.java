package com.bluesquare.rc.responseEntities;

import com.bluesquare.rc.entities.OrderUsedProduct;

public class InventoryDetailsModel {

	private long inventoryDetailsId;

	private OrderUsedProduct product;

	private float rate;

	private long quantity;

	private double amount;

	private double discountAmount;

	private double discountPercentage;

	private String discountType;

	private double amountBeforeDiscount;
	private String discountOnMRP;

	public InventoryDetailsModel(long inventoryDetailsId, OrderUsedProduct product, float rate, long quantity,
			double amount, double discountAmount, double discountPercentage, String discountType,
			double amountBeforeDiscount, String discountOnMRP) {
		super();
		this.inventoryDetailsId = inventoryDetailsId;
		this.product = product;
		this.rate = rate;
		this.quantity = quantity;
		this.amount = amount;
		this.discountAmount = discountAmount;
		this.discountPercentage = discountPercentage;
		this.discountType = discountType;
		this.amountBeforeDiscount = amountBeforeDiscount;
		this.discountOnMRP = discountOnMRP;
	}

	public long getInventoryDetailsId() {
		return inventoryDetailsId;
	}

	public void setInventoryDetailsId(long inventoryDetailsId) {
		this.inventoryDetailsId = inventoryDetailsId;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getAmountBeforeDiscount() {
		return amountBeforeDiscount;
	}

	public void setAmountBeforeDiscount(double amountBeforeDiscount) {
		this.amountBeforeDiscount = amountBeforeDiscount;
	}
	
	

	public String getDiscountOnMRP() {
		return discountOnMRP;
	}

	public void setDiscountOnMRP(String discountOnMRP) {
		this.discountOnMRP = discountOnMRP;
	}

	@Override
	public String toString() {
		return "InventoryDetailsModel [inventoryDetailsId=" + inventoryDetailsId + ", product=" + product + ", rate="
				+ rate + ", quantity=" + quantity + ", amount=" + amount + ", discountAmount=" + discountAmount
				+ ", discountPercentage=" + discountPercentage + ", discountType=" + discountType
				+ ", amountBeforeDiscount=" + amountBeforeDiscount + ", discountOnMRP=" + discountOnMRP + "]";
	}

	

}
