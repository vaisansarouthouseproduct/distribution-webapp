package com.bluesquare.rc.responseEntities;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


public class BusinessTypeModel {

	private long businessTypeId;

	private String name;

	
	
	public BusinessTypeModel(long businessTypeId, String name) {
		super();
		this.businessTypeId = businessTypeId;
		this.name = name;
	}

	public long getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(long businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BusinessTypeModel [businessTypeId=" + businessTypeId + ", name=" + name + "]";
	}

}
