package com.bluesquare.rc.responseEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.Contact;

public class SupplierModel {

	private long supplierPKId;
	private String supplierId;
	private String name;
	private Contact contact;
	private String address;
	private String gstinNo;
	private String taxType;
	private Date supplierAddedDatetime;
	private Date supplierUpdatedDatetime;
	//private StateModel stateModel;

	
	public SupplierModel(long supplierPKId, String supplierId, String name, Contact contact, String address,
			String gstinNo, String taxType, Date supplierAddedDatetime, Date supplierUpdatedDatetime) {
		super();
		this.supplierPKId = supplierPKId;
		this.supplierId = supplierId;
		this.name = name;
		this.contact = contact;
		this.address = address;
		this.gstinNo = gstinNo;
		this.taxType = taxType;
		this.supplierAddedDatetime = supplierAddedDatetime;
		this.supplierUpdatedDatetime = supplierUpdatedDatetime;
	}



	public SupplierModel(long supplierPKId, String supplierId, String name, Contact contact) {
		super();
		this.supplierPKId = supplierPKId;
		this.supplierId = supplierId;
		this.name = name;
		this.contact = contact;
	}
	
	

	public long getSupplierPKId() {
		return supplierPKId;
	}

	public void setSupplierPKId(long supplierPKId) {
		this.supplierPKId = supplierPKId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGstinNo() {
		return gstinNo;
	}

	public void setGstinNo(String gstinNo) {
		this.gstinNo = gstinNo;
	}

	public Date getSupplierAddedDatetime() {
		return supplierAddedDatetime;
	}

	public void setSupplierAddedDatetime(Date supplierAddedDatetime) {
		this.supplierAddedDatetime = supplierAddedDatetime;
	}

	public Date getSupplierUpdatedDatetime() {
		return supplierUpdatedDatetime;
	}

	public void setSupplierUpdatedDatetime(Date supplierUpdatedDatetime) {
		this.supplierUpdatedDatetime = supplierUpdatedDatetime;
	}
	
	

	public String getTaxType() {
		return taxType;
	}



	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}



	@Override
	public String toString() {
		return "SupplierModel [supplierPKId=" + supplierPKId + ", supplierId=" + supplierId + ", name=" + name
				+ ", contact=" + contact + ", address=" + address + ", gstinNo=" + gstinNo + ", taxType=" + taxType
				+ ", supplierAddedDatetime=" + supplierAddedDatetime + ", supplierUpdatedDatetime="
				+ supplierUpdatedDatetime + "]";
	}



	
}
