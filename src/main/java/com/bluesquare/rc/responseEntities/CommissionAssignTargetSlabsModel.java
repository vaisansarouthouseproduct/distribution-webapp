package com.bluesquare.rc.responseEntities;

public class CommissionAssignTargetSlabsModel {

	private long commissionAssignTargetSlabId;

	private float from;

	private float to;

	private String commissionType;

	private float targetValue;

	private String targetType;

	
	
	public CommissionAssignTargetSlabsModel(long commissionAssignTargetSlabId, float from, float to,
			String commissionType, float targetValue, String targetType) {
		super();
		this.commissionAssignTargetSlabId = commissionAssignTargetSlabId;
		this.from = from;
		this.to = to;
		this.commissionType = commissionType;
		this.targetValue = targetValue;
		this.targetType = targetType;
	}

	public long getCommissionAssignTargetSlabId() {
		return commissionAssignTargetSlabId;
	}

	public void setCommissionAssignTargetSlabId(long commissionAssignTargetSlabId) {
		this.commissionAssignTargetSlabId = commissionAssignTargetSlabId;
	}

	public float getFrom() {
		return from;
	}

	public void setFrom(float from) {
		this.from = from;
	}

	public float getTo() {
		return to;
	}

	public void setTo(float to) {
		this.to = to;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public float getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(float targetValue) {
		this.targetValue = targetValue;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	@Override
	public String toString() {
		return "CommissionAssignTargetSlabs [commissionAssignTargetSlabId=" + commissionAssignTargetSlabId + ", from="
				+ from + ", to=" + to + ", commissionType=" + commissionType + ", targetValue=" + targetValue
				+ ", targetType=" + targetType + "]";
	}

}
