package com.bluesquare.rc.responseEntities;

public class BankAccountTransactionModel {

	private long id;

	private String transactionId;

	private String partyName;

	private double opening;

	private double credit;

	private double debit;

	private double closing;

	private String payMode;

	private String referenceNo;

	private String bankName;

	private String insertedDate;

	private String chequeDate;

	public BankAccountTransactionModel(long id, String transactionId, String partyName, double opening, double credit,
			double debit, double closing, String payMode, String referenceNo, String bankName, String insertedDate,
			String chequeDate) {
		super();
		this.id = id;
		this.transactionId = transactionId;
		this.partyName = partyName;
		this.opening = opening;
		this.credit = credit;
		this.debit = debit;
		this.closing = closing;
		this.payMode = payMode;
		this.referenceNo = referenceNo;
		this.bankName = bankName;
		this.insertedDate = insertedDate;
		this.chequeDate = chequeDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public double getOpening() {
		return opening;
	}

	public void setOpening(double opening) {
		this.opening = opening;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public double getClosing() {
		return closing;
	}

	public void setClosing(double closing) {
		this.closing = closing;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInsertedDate() {
		return insertedDate;
	}

	public void setInsertedDate(String insertedDate) {
		this.insertedDate = insertedDate;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	@Override
	public String toString() {
		return "BankAccountTransaction [id=" + id + ", transactionId=" + transactionId + ", partyName=" + partyName
				+ ", opening=" + opening + ", credit=" + credit + ", debit=" + debit + ", closing=" + closing
				+ ", payMode=" + payMode + ", referenceNo=" + referenceNo + ", bankName=" + bankName + ", insertedDate="
				+ insertedDate + ", chequeDate=" + chequeDate + "]";
	}

}
