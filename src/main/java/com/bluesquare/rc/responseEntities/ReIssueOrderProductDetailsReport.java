package com.bluesquare.rc.responseEntities;

public class ReIssueOrderProductDetailsReport {

	private String productName;
	private long returnQuantity;
	private long replaceQuantity;
	private String type;

	public ReIssueOrderProductDetailsReport(String productName, long returnQuantity, long replaceQuantity,
			String type) {
		super();
		this.productName = productName;
		this.returnQuantity = returnQuantity;
		this.replaceQuantity = replaceQuantity;
		this.type = type;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public long getReplaceQuantity() {
		return replaceQuantity;
	}

	public void setReplaceQuantity(long replaceQuantity) {
		this.replaceQuantity = replaceQuantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ReIssueOrderProductDetailsReport [productName=" + productName + ", returnQuantity=" + returnQuantity
				+ ", replaceQuantity=" + replaceQuantity + ", type=" + type + "]";
	}

}
