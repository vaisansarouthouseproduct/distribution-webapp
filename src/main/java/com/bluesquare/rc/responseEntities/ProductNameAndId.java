package com.bluesquare.rc.responseEntities;

public class ProductNameAndId {
	private long productId;

	private String productName;

	public ProductNameAndId(long productId, String productName) {
		super();
		this.productId = productId;
		this.productName = productName;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "ProductNameAndId [productId=" + productId + ", productName=" + productName + "]";
	}

}
