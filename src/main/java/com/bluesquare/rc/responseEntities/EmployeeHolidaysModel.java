package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class EmployeeHolidaysModel {

	private long employeeHolidaysId;

	private String reason;

	private EmployeeDetailsModel employeeDetails;

	private Date fromDate;

	private boolean paidHoliday;

	private Date toDate;

	private Date givenHolidayDate;

	private boolean status;

	
	
	public EmployeeHolidaysModel(long employeeHolidaysId, String reason, EmployeeDetailsModel employeeDetails,
			Date fromDate, boolean paidHoliday, Date toDate, Date givenHolidayDate, boolean status) {
		super();
		this.employeeHolidaysId = employeeHolidaysId;
		this.reason = reason;
		this.employeeDetails = employeeDetails;
		this.fromDate = fromDate;
		this.paidHoliday = paidHoliday;
		this.toDate = toDate;
		this.givenHolidayDate = givenHolidayDate;
		this.status = status;
	}

	public long getEmployeeHolidaysId() {
		return employeeHolidaysId;
	}

	public void setEmployeeHolidaysId(long employeeHolidaysId) {
		this.employeeHolidaysId = employeeHolidaysId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public EmployeeDetailsModel getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetailsModel employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getGivenHolidayDate() {
		return givenHolidayDate;
	}

	public void setGivenHolidayDate(Date givenHolidayDate) {
		this.givenHolidayDate = givenHolidayDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isPaidHoliday() {
		return paidHoliday;
	}

	public void setPaidHoliday(boolean paidHoliday) {
		this.paidHoliday = paidHoliday;
	}

	@Override
	public String toString() {
		return "EmployeeHolidays [employeeHolidaysId=" + employeeHolidaysId + ", reason=" + reason
				+ ", employeeDetails=" + employeeDetails + ", fromDate=" + fromDate + ", paidHoliday=" + paidHoliday
				+ ", toDate=" + toDate + ", givenHolidayDate=" + givenHolidayDate + ", status=" + status + "]";
	}

}
