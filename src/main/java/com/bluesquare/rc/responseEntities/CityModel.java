package com.bluesquare.rc.responseEntities;

public class CityModel {

	private long cityId;

	private String name;

	private StateModel state;

	public CityModel(long cityId, String name) {
		super();
		this.cityId = cityId;
		this.name = name;
	}

	public CityModel(long cityId, String name, StateModel state) {
		super();
		this.cityId = cityId;
		this.name = name;
		this.state = state;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StateModel getState() {
		return state;
	}

	public void setState(StateModel state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CityModel [cityId=" + cityId + ", name=" + name + ", state=" + state + "]";
	}


}