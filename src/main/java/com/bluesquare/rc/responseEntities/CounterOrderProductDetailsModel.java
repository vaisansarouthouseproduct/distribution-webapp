package com.bluesquare.rc.responseEntities;

import com.bluesquare.rc.entities.OrderUsedProduct;

public class CounterOrderProductDetailsModel {

	private OrderUsedProduct product;

	private long purchaseQuantity;

	private double sellingRate;

	private double purchaseAmount;

	private String type;

	public CounterOrderProductDetailsModel(OrderUsedProduct product, long purchaseQuantity, double sellingRate,
			double purchaseAmount, String type) {
		super();
		this.product = product;
		this.purchaseQuantity = purchaseQuantity;
		this.sellingRate = sellingRate;
		this.purchaseAmount = purchaseAmount;
		this.type = type;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public long getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(long purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "CounterOrderProductDetailsModel [product=" + product + ", purchaseQuantity=" + purchaseQuantity
				+ ", sellingRate=" + sellingRate + ", purchaseAmount=" + purchaseAmount + ", type=" + type + "]";
	}

}
