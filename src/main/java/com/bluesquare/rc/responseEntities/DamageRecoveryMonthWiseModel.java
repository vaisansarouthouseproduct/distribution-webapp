package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class DamageRecoveryMonthWiseModel {

	private long damageRecoveryId;

	private ProductModel product;

	private long quantityDamage;

	private long quantityGiven;

	private long quantityReceived;

	private long quantityNotClaimed;

	private Date datetime;

	
	
	public DamageRecoveryMonthWiseModel(long damageRecoveryId, ProductModel product, long quantityDamage,
			long quantityGiven, long quantityReceived, long quantityNotClaimed, Date datetime) {
		super();
		this.damageRecoveryId = damageRecoveryId;
		this.product = product;
		this.quantityDamage = quantityDamage;
		this.quantityGiven = quantityGiven;
		this.quantityReceived = quantityReceived;
		this.quantityNotClaimed = quantityNotClaimed;
		this.datetime = datetime;
	}

	public long getDamageRecoveryId() {
		return damageRecoveryId;
	}

	public void setDamageRecoveryId(long damageRecoveryId) {
		this.damageRecoveryId = damageRecoveryId;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public long getQuantityDamage() {
		return quantityDamage;
	}

	public void setQuantityDamage(long quantityDamage) {
		this.quantityDamage = quantityDamage;
	}

	public long getQuantityGiven() {
		return quantityGiven;
	}

	public void setQuantityGiven(long quantityGiven) {
		this.quantityGiven = quantityGiven;
	}

	public long getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(long quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public long getQuantityNotClaimed() {
		return quantityNotClaimed;
	}

	public void setQuantityNotClaimed(long quantityNotClaimed) {
		this.quantityNotClaimed = quantityNotClaimed;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		return "DamageRecoveryMonthWise [damageRecoveryId=" + damageRecoveryId + ", product=" + product
				+ ", quantityDamage=" + quantityDamage + ", quantityGiven=" + quantityGiven + ", quantityReceived="
				+ quantityReceived + ", quantityNotClaimed=" + quantityNotClaimed + ", datetime=" + datetime + "]";
	}

}
