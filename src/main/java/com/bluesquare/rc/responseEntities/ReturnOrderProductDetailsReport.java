package com.bluesquare.rc.responseEntities;

public class ReturnOrderProductDetailsReport {

	private ProductModel productModel;
	private double sellingRate;
	private long returnId;

	private String productName;
	private long returnQuantity;
	private long issuedQuantity;
	private long reIssuedQuantity;
	private double returnTotalAmountWithTax;
	private String reason;
	private String type;

	public ReturnOrderProductDetailsReport(String productName, long returnQuantity, long issuedQuantity,
			double returnTotalAmountWithTax, String reason, String type) {
		super();
		this.productName = productName;
		this.returnQuantity = returnQuantity;
		this.issuedQuantity = issuedQuantity;
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
		this.reason = reason;
		this.type = type;
	}

	public ReturnOrderProductDetailsReport(String productName, long returnQuantity, long reIssuedQuantity,
			String type) {
		super();
		this.productName = productName;
		this.returnQuantity = returnQuantity;
		this.reIssuedQuantity = reIssuedQuantity;
		this.type = type;
	}

	public ReturnOrderProductDetailsReport(ProductModel productModel, double sellingRate, long returnId,
			long returnQuantity, long issuedQuantity, double returnTotalAmountWithTax, String reason, String type) {
		super();
		this.productModel = productModel;
		this.sellingRate = sellingRate;
		this.returnId = returnId;
		this.returnQuantity = returnQuantity;
		this.issuedQuantity = issuedQuantity;
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
		this.reason = reason;
		this.type = type;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public double getReturnTotalAmountWithTax() {
		return returnTotalAmountWithTax;
	}

	public void setReturnTotalAmountWithTax(double returnTotalAmountWithTax) {
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getReIssuedQuantity() {
		return reIssuedQuantity;
	}

	public void setReIssuedQuantity(long reIssuedQuantity) {
		this.reIssuedQuantity = reIssuedQuantity;
	}

	public ProductModel getProductModel() {
		return productModel;
	}

	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public long getReturnId() {
		return returnId;
	}

	public void setReturnId(long returnId) {
		this.returnId = returnId;
	}

	@Override
	public String toString() {
		return "ReturnOrderProductDetailsReport [productModel=" + productModel + ", sellingRate=" + sellingRate
				+ ", returnId=" + returnId + ", productName=" + productName + ", returnQuantity=" + returnQuantity
				+ ", issuedQuantity=" + issuedQuantity + ", reIssuedQuantity=" + reIssuedQuantity
				+ ", returnTotalAmountWithTax=" + returnTotalAmountWithTax + ", reason=" + reason + ", type=" + type
				+ "]";
	}

}
