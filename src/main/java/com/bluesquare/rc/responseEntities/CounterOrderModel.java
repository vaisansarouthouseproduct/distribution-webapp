package com.bluesquare.rc.responseEntities;

import javax.persistence.Column;

public class CounterOrderModel {
	private long counterOrderPKId;

	private String counterOrderId;

	private String businessNameId;
	
	private String ShopName;
	
	private String mobileNumber;

	private String customerName;

	private String customerMobileNumber;

	private String customerGstNumber;

	private double totalAmount;

	private double totalAmountWithTax;

	private long totalQuantity;

	private TransportationModel transportationModel;

	private String vehicleNo;

	private String docketNo;

	private String discountType;

	private double discount;

	public CounterOrderModel(long counterOrderPKId, String counterOrderId, String businessNameId, String customerName,
			String customerMobileNumber, String customerGstNumber, double totalAmount, double totalAmountWithTax,
			long totalQuantity, TransportationModel transportationModel, String vehicleNo, String docketNo,
			String discountType, double discount) {
		super();
		this.counterOrderPKId = counterOrderPKId;
		this.counterOrderId = counterOrderId;
		this.businessNameId = businessNameId;
		this.customerName = customerName;
		this.customerMobileNumber = customerMobileNumber;
		this.customerGstNumber = customerGstNumber;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalQuantity = totalQuantity;
		this.transportationModel = transportationModel;
		this.vehicleNo = vehicleNo;
		this.docketNo = docketNo;
		this.discountType = discountType;
		this.discount = discount;
	}

	public long getCounterOrderPKId() {
		return counterOrderPKId;
	}

	public void setCounterOrderPKId(long counterOrderPKId) {
		this.counterOrderPKId = counterOrderPKId;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerGstNumber() {
		return customerGstNumber;
	}

	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public TransportationModel getTransportationModel() {
		return transportationModel;
	}

	public void setTransportationModel(TransportationModel transportationModel) {
		this.transportationModel = transportationModel;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "CounterOrderModel [counterOrderPKId=" + counterOrderPKId + ", counterOrderId=" + counterOrderId
				+ ", businessNameId=" + businessNameId + ", customerName=" + customerName + ", customerMobileNumber="
				+ customerMobileNumber + ", customerGstNumber=" + customerGstNumber + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity
				+ ", transportationModel=" + transportationModel + ", vehicleNo=" + vehicleNo + ", docketNo=" + docketNo
				+ ", discountType=" + discountType + ", discount=" + discount + "]";
	}

}
