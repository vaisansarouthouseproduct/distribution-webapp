package com.bluesquare.rc.responseEntities;

public class SupplierOrderDetailsModel {

	private long supplierOrderDetailsId;

	private SupplierModel supplier;

	private ProductModel product;

	private long quantity;

	private double supplierRate;

	private double totalAmount;

	private double totalAmountWithTax;

	public SupplierOrderDetailsModel(long supplierOrderDetailsId, SupplierModel supplier, ProductModel product,
			long quantity, double supplierRate, double totalAmount, double totalAmountWithTax) {
		super();
		this.supplierOrderDetailsId = supplierOrderDetailsId;
		this.supplier = supplier;
		this.product = product;
		this.quantity = quantity;
		this.supplierRate = supplierRate;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getSupplierOrderDetailsId() {
		return supplierOrderDetailsId;
	}

	public void setSupplierOrderDetailsId(long supplierOrderDetailsId) {
		this.supplierOrderDetailsId = supplierOrderDetailsId;
	}

	public SupplierModel getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierModel supplier) {
		this.supplier = supplier;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getSupplierRate() {
		return supplierRate;
	}

	public void setSupplierRate(double supplierRate) {
		this.supplierRate = supplierRate;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	@Override
	public String toString() {
		return "SupplierOrderDetails [supplierOrderDetailsId=" + supplierOrderDetailsId + ", supplier=" + supplier
				+ ", product=" + product + ", quantity=" + quantity + ", supplierRate=" + supplierRate
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + "]";
	}

}
