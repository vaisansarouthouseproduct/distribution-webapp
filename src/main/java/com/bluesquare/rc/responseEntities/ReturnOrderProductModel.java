package com.bluesquare.rc.responseEntities;

import java.util.Date;

public class ReturnOrderProductModel {

	public String returnOrderProductId;
	public String shopName;
	public String areaName;
	public long totalQuantity;
	public Date returnOrderProductDateTime;

	public ReturnOrderProductModel(String returnOrderProductId, String shopName, String areaName, long totalQuantity,
			Date returnOrderProductDateTime) {
		super();
		this.returnOrderProductId = returnOrderProductId;
		this.shopName = shopName;
		this.areaName = areaName;
		this.totalQuantity = totalQuantity;
		this.returnOrderProductDateTime = returnOrderProductDateTime;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getReturnOrderProductDateTime() {
		return returnOrderProductDateTime;
	}

	public void setReturnOrderProductDateTime(Date returnOrderProductDateTime) {
		this.returnOrderProductDateTime = returnOrderProductDateTime;
	}

	@Override
	public String toString() {
		return "ReturnOrderProductModel [returnOrderProductId=" + returnOrderProductId + ", shopName=" + shopName
				+ ", areaName=" + areaName + ", totalQuantity=" + totalQuantity + ", returnOrderProductDateTime="
				+ returnOrderProductDateTime + "]";
	}

}
