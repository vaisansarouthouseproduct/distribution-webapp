package com.bluesquare.rc.responseEntities;

public class TransportationModel {
	private long id;

	private String transportName;

	private String mobNo;

	private String gstNo;

	private String vehicleNo;

	public TransportationModel(long id, String transportName, String mobNo, String gstNo, String vehicleNo) {
		super();
		this.id = id;
		this.transportName = transportName;
		this.mobNo = mobNo;
		this.gstNo = gstNo;
		this.vehicleNo = vehicleNo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransportName() {
		return transportName;
	}

	public void setTransportName(String transportName) {
		this.transportName = transportName;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	@Override
	public String toString() {
		return "TransportationModel [id=" + id + ", transportName=" + transportName + ", mobNo=" + mobNo + ", gstNo="
				+ gstNo + ", vehicleNo=" + vehicleNo + "]";
	}

}
