package com.bluesquare.rc.responseEntities;

public class RoutePointsModel {

	private long routePointsId;
	
	private String routeAddress;

	private String longitude;

	private String latitude;

	private RouteModel route;

	
	public RoutePointsModel(long routePointsId, String routeAddress, String longitude, String latitude,
			RouteModel route) {
		super();
		this.routePointsId = routePointsId;
		this.routeAddress = routeAddress;
		this.longitude = longitude;
		this.latitude = latitude;
		this.route = route;
	}

	public long getRoutePointsId() {
		return routePointsId;
	}

	public void setRoutePointsId(long routePointsId) {
		this.routePointsId = routePointsId;
	}

	public String getRouteAddress() {
		return routeAddress;
	}

	public void setRouteAddress(String routeAddress) {
		this.routeAddress = routeAddress;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public RouteModel getRoute() {
		return route;
	}

	public void setRoute(RouteModel route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "RoutePoints [routePointsId=" + routePointsId + ", routeAddress=" + routeAddress + ", longitude="
				+ longitude + ", latitude=" + latitude + ", route=" + route + "]";
	}

	
}
