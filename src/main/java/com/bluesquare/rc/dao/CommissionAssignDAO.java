package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.EmployeeCommission;
import com.bluesquare.rc.entities.EmployeeCommissionDetails;
import com.bluesquare.rc.models.CommissionAssignAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.CommissionAssignRequest;
import com.bluesquare.rc.models.EmployeeCommissionModel;
import com.bluesquare.rc.rest.models.CommissionAssignModel;

public interface CommissionAssignDAO {
	public void saveCommissionAssignment(CommissionAssignRequest commissionAssignRequest );
	public void updateCommissionAssignment(CommissionAssignRequest commissionAssignRequest );
	public void disableCommissionAssignment(long commissionAssignId);
	public CommissionAssignModel fetchCommissionAssign();
	public CommissionAssignAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId);
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabs(long commissionAssignId);
	public CommissionAssignTargetSlabs findIncentiveAmountByCompleteTargetValue(double completeValue,long commissionAssignId);
	public CommissionAssign fetchCommissionAssign(long commissionAssignId);
	public List<CommissionAssignProducts> fetchCommissionAssignProducts(long commissionAssignId);
	public boolean checkCommissionAssignNameAlreadyUsedOrNot(String commissionAssignName,long commissionAssignId);
	public EmployeeCommission fetchEmployeeCommissionByMonthAndYear(long monthId,long yearId, long employeeDetailsId);
	public List<EmployeeCommissionDetails> fetchEmployeeCommissionDetails(long employeeCommissionId);
	public List<EmployeeCommissionModel> fetchEmployeeCommissionModel(long monthId,long yearId);
}


