package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.responseEntities.BrandModel;

public interface BrandDAO {

	public void saveBrandForWebApp(Brand brand);
	public void updateBrandForWebApp(Brand brand);
	public Brand fetchBrandForWebApp(long brandId);
	public List<Brand> fetchBrandListForWebApp();	
	public List<BrandModel> fetchBrandModelListForWebApp();
	public BrandModel fetchBrandModelForWebApp(long brandId);
}
