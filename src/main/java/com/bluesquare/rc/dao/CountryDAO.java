package com.bluesquare.rc.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.entities.Country;

public interface CountryDAO { //extends DAO {
	
	//WebApp methods
	
	public List<Country> fetchCountryListForWebApp();
	public Country fetchCountryForWebApp(long coutryId);
	//public List<Country> fetchCountryListForBranchConfig();
	public Country saveCountryForWeb(Country country);
	public Country updateCountryForWeb(Country country);
	public Country fetchIndiaCountry();
}
