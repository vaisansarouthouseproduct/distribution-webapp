package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.NotePad;

public interface NotepadDAO {
	
	public void saveNotePad(NotePad notePad);
	public NotePad fetchNotepadById(long id);
	public List<NotePad> fetchNotePadListByEmployeeId(long employeeId);
	public void updateNotePad(NotePad notePad);
	public void deleteNotePadById(long id);
	public void deleteNotepadByIdList(List<Long> idList);

}
