package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Department;

public interface DepartmentDAO {
	
	//webApp
		public void saveForWebApp(Department department);

		public void updateForWebApp(Department department);
		
		public List<Department> fetchDepartmentListForWebApp();
		
		public Department fetchDepartmentForWebApp(long departmentId);
}
