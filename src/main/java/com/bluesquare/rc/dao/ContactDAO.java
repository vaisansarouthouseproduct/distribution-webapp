package com.bluesquare.rc.dao;

import com.bluesquare.rc.entities.Contact;

public interface ContactDAO {

	public void save(Contact contact);

	public void update(Contact contact);

	public void delete(long id);

}
