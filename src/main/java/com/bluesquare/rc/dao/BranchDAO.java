package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.models.BranchModel;

public interface BranchDAO {

	public List<BranchModel> fetchBranchListByCompanyId(long companyId);
	public List<Branch> fetchBranchListByBrachIdList(long[] branchIds);
	public Branch fetchBranchByBranchId(long branchId);
	public void updateBranch(Branch branch);
	public List<BranchModel> fetchBranchByEmployeeId(long employeeId);
	
}
