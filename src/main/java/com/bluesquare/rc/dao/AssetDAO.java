package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.AssetsCategory;

public interface AssetDAO {
	
	// Asset Category ManageMent
		public void saveAssetCategoryDetails(AssetsCategory assetsCategory);
		public void updateAssetCategoryDetails(AssetsCategory assetsCategory);
		public List<AssetsCategory> fetchAssetCategory();
		public AssetsCategory fetchAssetsCategoryById(long assetCategoryId);

}
