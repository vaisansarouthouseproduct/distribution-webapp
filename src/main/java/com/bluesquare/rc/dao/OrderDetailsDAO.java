package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.CancelReason;
import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.OrderStatus;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CancelOrderRequest;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.GstBillReport;
import com.bluesquare.rc.models.OrderDetailsListOfBusiness;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.models.SalesReportModel;
import com.bluesquare.rc.responseEntities.ReturnFromDeliveryBoyModel;
import com.bluesquare.rc.rest.models.BookOrderFreeProductRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.GkSnapProductResponse;
import com.bluesquare.rc.rest.models.OrderDetailByBusinessNameIdEmployeeIdRequest;
import com.bluesquare.rc.rest.models.OrderDetailsForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentList;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.rest.models.OrderRequest;
import com.bluesquare.rc.rest.models.PReturnOrderDetailsModel;
import com.bluesquare.rc.rest.models.PaymentListRequest;
import com.bluesquare.rc.rest.models.ReIssueDelivered;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.rest.models.ReturnPermanentModelRequest;

public interface OrderDetailsDAO {

	public String bookOrder(OrderRequest orderRequest);
	public String freeProductAddInOrderProductDetails(BookOrderFreeProductRequest bookOrderFreeProductRequest);
	// public String issueOrderToDeliverBoy(OrderIssueRequest
	// orderIssueRequest,OrderDetails orderDetails);

	public String updateBookOrder(OrderRequest orderRequest);

	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId);
	
	public OrderDetails fetchOrderDetailsByOrderIdForApp(String orderDetailsId);

	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails);

	public OrderDetailsList fetchOrderListByAreaId(long employeeId);
	public OrderDetailsList fetchPendingOrderListByAreaId(long employeeId);
	
	public OrderDetailsList fetchOrderListByAreaId(long areaId , long employeeId);
	public OrderDetailsList fetchPendingOrderListByAreaId(long areaId , long employeeId);

	//public List<OrderDetails> fetchOrderDetailsTodaysListByAreaId(long areaId);
	
	public List<OrderReportList> showOrderReport(String range,String startDate,String endDate);
	public List<OrderReportList> showOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate);
	public List<OrderReportList> showOrderReportByEmployeeSMId(String employeeSMId,String range,String startDate,String endDate);


	public List<OrderProductDetails> fetchOrderProductDetailByOrderId(String orderId);
	public List<OrderProductDetails> fetchOrderProductDetailByOrderIdForApp(String orderId);

	/*public List<OrderProductDetails> makeProductImageMNullorderProductDetailsList(
			List<OrderProductDetails> orderProductDetailsList);*/

	public List<OrderProductDetailListForWebApp> orderProductDetailsListForWebApp(String orderDetailsId);

	public List<SupplierOrder> fetchSupplierOrders24hour(String filter, String startDate, String endDate);

	public List<OrderDetailsForPayment> fetchOrderListForPayment(PaymentListRequest paymentListRequest);

	public List<SupplierOrder> fetchSupplierOrders24hourBySupplierId(String supplierId);

	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId);

	/*public List<ReturnOrderResponse> makeProductsImageNullOrderDetailsWithOrderProductDetailsList(
			List<ReturnOrderResponse> returnOrderResponsesList);*/

	public List<OrderDetails> fetchOrderDetailBybusinessNameIdAndDateRange(String businessNameId, String fromDate,
			String toDate);

	public List<CustomerReportResponse> fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range);

	public List<OrderDetails> fetchOrderDetailForCustomerReportByBusinessNameId(String businessNameId);

	public List<CustomerReportResponse> fetchTotalCollectionByDateRangeAndEmployeeId(long employeeId, String fromDate,
			String toDate, String range);

	public String packedOrderToDeliverBoy(OrderIssueRequest orderIssueRequest);

	public String confirmPackedOrderFromDB(String orderId);
	
	public String orderDeliveredAndReturn(ReturnOrderRequest returnOrderRequest,String appPath);
	
	public OrderProductIssueDetails fetchOrderProductIssueDetailsByOrderId(String orderId);
	
	//public List<OrderDetails> fetchOrderDetailsPendingListByAreaId(long areaId);
	
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(long employeeId,
			long areaId, String fromDate, String toDate,String range);
	
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(long employeeId, String fromDate, String toDate,String range);
	public String cancelOrder(CancelOrderRequest cancelOrderRequest);
	public OrderDetailsList fetchTodaysPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId);
	public OrderDetailsList fetchPendingPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId);
	public OrderDetailsList fetchPendingIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId);
	public OrderDetailsList fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId);
	
	public String reIssueOrderDetails(OrderReIssueRequest orderReIssueRequest);
	public OrderProductIssueDetails fetchOrderProductIssueListForIssueReportByOrderId(
			String orderId);
	//public List<ReIssueOrderProductDetails> fetchReIssuffeOrderProductDetailsList(String orderId);
	public List<OrderProductIssueDetails> fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(long employeeId,
			String fromDate, String toDate, String range, String orderStatus);
	
	// sachin 26/10/2017
	public List<OrderDetails> fetchOrderDetailsByDateRangeAndEmpId(long employeeId,String fromDate,String toDate,String range);
	public List<ReIssueOrderDetails> fetchReplacementIssuedOrdersByEmployeeId(long employeeId,String status);
	public String doneReIssueAndOrderStatusDelivered(ReIssueDelivered reIssueDelivered);
	public ReIssueOrderDetails fetchReIssueOrderDetailsByReIssueOrderDetailsId(long reIssueOrderId);
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(long reIssueOrderId);
	public ReIssueOrderProductDetailsListModel fetchOrderDetailsForReIssueDelivedByOrderId(long reIssueOrderId);
	//public ChartDetailsResponse fetchTopFiveSalesManByIssuedSale();
	public List<OrderDetailsPaymentList> fetchOrderDetailForTotalCollectionReportByBusinessNameId(OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest);
	public double fetchTotalSaleForIndexPage(String range,String startDate,String endDate);
	public double fetchTotalAmountInvestInMarketForIndexPage(String range,String startDate,String endDate);
	public BillPrintDataModel fetchBillPrintData(String orderId,long companyId,long branchId);
	public OrderDetailsListOfBusiness fetchOrderDetailsListOfBusinessByBusinessNameId(String businessNameId,String fromDate, String toDate,  String range);
	public String sendSMSTOShopsUsingOrderId(String orderIds,String smsText,String mobileNumber);
	public List<SalesReportModel> fetchSalesReportModel(String startDate, String endDate,String range);
	public List<OrderReportList> cancelOrderReport(String range,String startDate,String endDate);
	public List<OrderDetails> fetchCancelOrderReportByEmployeeId(long employeeId, String fromDate, String toDate,
			String range);
	public void updateEditOrder(String updateOrderProductListId,String orderId);
	public List<ReturnFromDeliveryBoy> fetchReturnFromDeliveryBoyList(String orderId);
	public ReturnFromDeliveryBoyMain fetchReturnFromDeliveryBoyMain(String returnFromDeliveryBoyMainId);
	public List<ReturnOrderFromDeliveryBoyReport> fetchReturnOrderFromDeliveryBoyReport(String range,String startDate,String endDate);
	public void updateReturnFromDeliveryBoy(String orderId,String returnFromDeliveryBoyList);
	public void updateReturnFromDeliveryBoyForApp(List<ReturnFromDeliveryBoyModel>  returnFromDeliveryBoyList,String returnFromDeliveryBoyMainId);
	public String updateOrderUsedProductCurrentQuantity();
	public void deleteOrderUsedProduct(OrderUsedProduct orderUsedProduct);
	public OrderStatus fetchOrderStatus(String orderStatusName);
	public ReIssueOrderDetails fetchReIssueOrderDetails(String returnOrderProductId);
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk();
	
	public GstBillReport fetchGstBillReport(String startDate,String endDate,String type);
	public ChartDetailsResponse fetchTopFiveSalesManByIssuedSale(String range,String startDate,String endDate);
	
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate);
	
	// api for Offline mode
	public List<OrderDetails> fetchLast3MonthOrderListByAreaIdForOfflineMode(long employeeId);
	public List<CancelReason> fetchCancelReason();
	public OfflineAPIRequest fetchofflineApiHandlingByDbIdAndIMEINo(OfflineAPIRequest apiRequest);
	public void saveOfflineAPIrequest(OfflineAPIRequest apiRequest);
	public void updateTheStatusOfOfflineAPIRequest(long dbId,String imeiNo,String status);
	public boolean checkTransaportationUsed(long transportationId);
	public int findNumberOfOrderTakenByDateRange(String startDate,String endDate,long employeeId);
	public double totalSaleAmountForTarget(String startDate,String endDate,long employeeId,boolean includeTax);
	public double findNumberOfProductSalesByDateRangeAndEmployeeId(String startDate,String endDate,long employeeId,long productId,boolean includeTax);
	public long findNumberOfProductSalesByDateRangeAndEmployeeIdForQuantity(String startDate,String endDate,long employeeId,long productId);
	public List<OrderDetails>  fetchOrderDetailsBybusinessNameId(String businessNameId);
	public List<OrderReportList> showOrderReportByBusinessNameIdOfBadDebts(String businessNameId);
	public List<OrderDetails>  fetchOrderDetailsBybusinessNameIdForView(String businessNameId);
	
	public ReturnOrderProductP fetchReturnOrderProductPByReturnOrderProductId(String returnOrderProductId);
	public List<ReturnOrderProductDetailsP> fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(String returnOrderProductId);
	public void savePermanentReturnOrder(ReturnPermanentModelRequest returnPermanentModelRequest);
	public List<PReturnOrderDetailsModel> permanentReturnOrderReport(String range,String startDate,String endDate);
	public BillPrintDataModel fetchReturnOrderCreditNoteBillPrintData(String returnOrderProductId,long companyId,long branchId);
}
