package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Bank;
import com.bluesquare.rc.entities.BankAccount;
import com.bluesquare.rc.entities.BankAccountTempTransaction;
import com.bluesquare.rc.entities.BankAccountTransaction;
import com.bluesquare.rc.entities.BankCBTemplateDesign;
import com.bluesquare.rc.entities.BankChequeBook;
import com.bluesquare.rc.entities.BankChequeEntry;
import com.bluesquare.rc.entities.BankPayeeDetails;

public interface BankingDAO {
	public void insertAccount(BankAccount bankAccount);
	
	public void insertTempTransaction(BankAccountTempTransaction bankAccountTempTransaction);
	
	public void insertTransaction(BankAccountTransaction bankAccountTransaction);
	
	public List<BankAccount> fetchBankAccounts();
	
	public List<BankAccountTempTransaction> fetchTempTransactions(long bankAccountId,String fromDate,String toDate,String transactionStatus,int transactionCount);
	
	public List<BankAccountTransaction> fetchTransactions(long bankAccountId,String fromDate,String toDate,int transactionCount);
	
	public BankAccount fetchBankAccountById(String id);
	
	public BankAccountTempTransaction fetchTempTransactionById(String id);
	
	public BankAccountTransaction fetchTransactionById(String id);
	
	public void updateAccount(BankAccount bankAccount);
	
	public void updateTempTransaction(BankAccountTempTransaction bankAccountTempTransaction);

	public void changePrimaryStatusForOtherAccounts(String id);
	
	public boolean accountNumberUsed(String bankAccountNumber);
	
	public BankAccount fetchBankAccountByLongId(long bankAccountId);
	
	//Bank Name Management
	public void saveBank(Bank bank);
	public void updateBank(Bank bank);
	public List<Bank> fetchBankList();
	public Bank fetchBankByBankId(long bankId);
	
	// Payee ManageMent
	public void savePayeeDetails(BankPayeeDetails bankPayeeDetails);
	public void updatePayeeDetails(BankPayeeDetails bankPayeeDetails);
	public List<BankPayeeDetails> fetchPayeeList();
	public BankPayeeDetails fetchPayeeById(long payeeId);
	
	// cheque book managament
	public void saveChequeBook(BankChequeBook bankChequeBook);
	public void updateChequeBook(BankChequeBook bankChequeBook);
	public List<BankChequeBook> fetchChequeBookList();
	public BankChequeBook fetchBankChequeBookById(long id);
	
	
	//Cheque Book Design Template Management
	
	public void saveChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign);
	public void updateChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign);
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignList();
	public BankCBTemplateDesign fetchChequeTemplateDesignById(long id);
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignByBankId(long bankId);
	
	//Print Cheque Management
	public List<String> fetchUnusedChequeNoListByCBId(long chequeBookId);
	public List<BankChequeBook> fetchBankChequeBookListByAcoountId(long accountId);
	
	
	// save Cheque Print entry
	public void saveManualPrintCheque(BankChequeEntry bankChequeEntry);
	public void updateBankChequeEntry(BankChequeEntry bankChequeEntry);
	
	public BankChequeEntry fetchChequeEntryByChequeNo(String chqNo);
	
	public List<BankChequeEntry> fetchCancelChequeEntryListByCBId(long chequeBookId);
	public List<BankChequeEntry> fetchPrintedChequeEntryListByCBId(long chequeBookId);
	
	
	
}
