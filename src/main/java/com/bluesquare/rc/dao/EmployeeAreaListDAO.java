package com.bluesquare.rc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.SupplierProductList;


public interface EmployeeAreaListDAO {

		//webapp
	
		public void saveEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId);
		
		public void updateEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId);
		
		public void deleteForWebApp(long employeeAreaListId);
		
		
		
		public List<EmployeeAreaList> fetchEmployeeAreaListByEmployeeDetailsId(long employeeDetailsId);
		
		public String getAreaIdList(List<EmployeeAreaList> employeeAreaList);
		
		
}
