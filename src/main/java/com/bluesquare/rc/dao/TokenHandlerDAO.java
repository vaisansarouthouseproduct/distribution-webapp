package com.bluesquare.rc.dao;

public interface TokenHandlerDAO {
	public String modifyQueryForSelectedCompaniesAccessForOtherEntities(String hql);
	public String getSessionSelectedCompaniesIds();
	public long getAppLoggedEmployeeId();
	public String getEndTextForSMS();
	public String createNewToken(String token,long branchId) throws Exception;
	public long getSessionSelectedBranchIds();
}
