package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.State;

public interface StateDAO { //extends DAO {

	public State fetchStateByStateName(String stateName);
	
	//webApp
	public List<State> fetchAllStateForWebApp();
	
	public void saveForWebApp(State state);

	public void updateForWebApp(State state);
	
	public State fetchState(long stateId);
	
	public List<State> fetchStateByCountryIdForWebapp(long countryId);
	
	//public List<State> fetchStateByCountryIdForBranchConfig(long countryId);
}
