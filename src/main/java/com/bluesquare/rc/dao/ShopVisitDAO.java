package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.ShopVisit;
import com.bluesquare.rc.entities.ShopVisitImages;

public interface ShopVisitDAO {

	
	public void saveShopVisit(ShopVisit shopVisted);
	public List<ShopVisit> fetchShopVisitedByDateRange(long employeeId,String range,String fromDate,String toDate);
	public ShopVisit fetchShopVisitById(long visitId);
	public void updateShopVisit(ShopVisit shopVisit);
	public int findNoOfShopVisitBetweenTwoDates(String startDate,String endDate,long employeeId);
	
	public ShopVisitImages fetchShopVisitImage(long shopVisitId);
	public void updateShopVisitImage(String shopImage,ShopVisit shopVisit);
	public void saveShopVisitImage(String shopImage,ShopVisit shopVisit);
}
