package com.bluesquare.rc.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeRoutes;
import com.bluesquare.rc.entities.Route;

public interface EmployeeDAO {


	public Employee validate(String username, String password);


	public EmployeeDetails getEmployeeDetails(long employeeId);

	public Employee loginCredentialsForRC(String userid, String password);
	
	public void logout(long employeeId);
	
	public void saveForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList);
	public void updateForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList);
	
	public void updateForWebApp(Employee employee);
	
	public List<Branch> fetchBranchListByEmployeeId(long employeeId);
	public List<Route> fetchRouteListByEmployeeId(long employeeId);
		
	public String checkEmployeeDuplication(String checkText,String type,long employeeDetailsId);
	
	 public String checkAppVersion(String appVersion);
	 
	 public void setNotificationCountInSession(HttpSession session);
}