package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Area;

public interface AreaDAO extends DAO{

	
	//webapp
	public void saveForWebApp(Area area);

	public void updateForWebApp(Area area);

	public List<Area> fetchAllAreaForWebApp();
	
	public List<Area> fetchAreaListByRegionIdForWebApp(long regionId);
	
	public Area fetchAreaForWebApp(long id);
	
	public List<Object[]> fetchLocationIdsForWebApp();
	
	public boolean checkAreaDuplication(long pincode,long areaId);
	
	public String fetchAreaListByEmployeeId(long employeeId);
	
	public String modifyQueryForSelectedAccessForOtherEntities(String hql);
	
	public String getSessionAreaIdsForOtherEntities();
}
