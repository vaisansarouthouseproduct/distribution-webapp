package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Region;

public interface RegionDAO extends DAO{

	
	
	//webapp
	public void saveForWebApp(Region region);

	public void updateForWebApp(Region region);
	
	public List<Region> fetchAllRegionForWebApp();

	public List<Region> fetchSpecifcRegionsForWebApp(long cityId);
		
	public Region fetchRegionForWebApp(long region);

}
