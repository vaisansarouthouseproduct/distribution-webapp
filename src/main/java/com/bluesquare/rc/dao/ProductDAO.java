package com.bluesquare.rc.dao;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.bluesquare.rc.entities.DailyStockDetails;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.PermanentDamageDetails;
import com.bluesquare.rc.entities.PermanentDamageMonthWise;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.responseEntities.DailyStockDetailsModel;
import com.bluesquare.rc.responseEntities.ProductNameAndId;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.ProductAddInventory;

public interface ProductDAO {


	public void Update(Product product);
	
	//webapp
	public Product findProductNameExist(String productName,long productId);
	public void saveProductForWebApp(MultipartFile file,Product Product);
	public void saveProduct(Product product);
	public void updateProductForWebApp(MultipartFile file,Product Product);
	public void updateProductForWebApp(Product product);
	public Product fetchProductForWebApp(long ProductId);
	public Product fetchProductForImage(long productId,long companyId,long brandId);
	public Product setNoImageToProductForImage(Product product);
	public List<Product> fetchProductListForWebApp();
	public List<ProductNameAndId> fetchProductNameAndIdList();
	public List<ProductViewList> fetchProductViewListForWebApp();
	public ProductViewList fetchProductViewListForWebApp(long ProductId);
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId);
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId);
	//public List<Product> makeProductImageNull(List<Product> productList);
	//public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists);
	public List<Product> fetchProductListBySupplierId(String supplierId);
	public List<ProductAddInventory> fetchProductList();
	public List<ProductAddInventory> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest );
	public List<ProductAddInventory> fetchProductForAddInventory();
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId);
	public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo);
	public  List<Product> fetchProductByCategoryIdForWebApp(long categoryId);
	public CalculateProperTaxModel calculateProperAmountModel(double mrp,double igstPer);
	
	//DailyStockDetails Maintain
	public void creatFirstRecordOfDailyStockReport();
	public List<DailyStockDetails> fetchTodaysDailyStockDetails();
	public List<DailyStockDetailsModel> fetchDailyStockDetails(String pickDate,long companyId,long brandId);
	public DailyStockDetails fetchDailyStockReportByProductIdAndCurrentDate(long productId);
	public void updateDailyStockExchange(long productId,long qty,boolean isAdd );
	public void saveDamageDefine(DamageDefine damageDefine);
	public List<DamageDefine> fetchDamageDefineList(String range,String startDate,String endDate);
	
	//Damage Product recovery functions
	public List<DamageRecoveryMonthWise> fetchDamageRecoveryDayWise(String startMonth,String startYear,String endMonth,String endYear);
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryMonthWise(long productId,long damageQuantity);
	public List<DamageRecoveryDetails> fetchDamageRecoveryDetailsByDamageRecoveryId(long damageRecoveryId);
	public DamageRecoveryDetails fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(String damageRecoveryDetailsId);
	public void updateDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails);
	public void saveDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails,long damageRecoveryId);
	public DamageRecoveryMonthWise fetchDamageRecoveryMonthWiseByProductId(long productId);
	public DamageRecoveryMonthWise fetchDamageRecoveryMonthWiseByDamageRecoveryId(long damageRecoveryId);
	
	//Permanent Damage
	public List<PermanentDamageDetails> fetchPermanentDamageDetailsListByPermanentDamageMonthWiseId(long permanentDamageMonthWiseId);
	public PermanentDamageMonthWise fetchPermanentDamageMonthWiseByPermanentDamageMonthWiseId(long productId);
	public List<PermanentDamageMonthWise> fetchPermanentDamageMonthWiseByMonth(long year,long monthNumber);
	public void savePermanenetDamageDetails(PermanentDamageDetails permanentDamageDetails,long productId);
	
	public String checkProductBarcodeDuplication(String checkText,String type,long productId);
	public Product fetchProductByBarcodeNumber(String barcodeNo);
}
