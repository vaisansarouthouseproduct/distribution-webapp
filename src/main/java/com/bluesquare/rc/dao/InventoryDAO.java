package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.InventoryAddedInvoiceModel;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.models.InventoryRequest;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.models.TaxCalculationManageInventory;
import com.bluesquare.rc.rest.models.InventorySaveRequestModel;

public interface InventoryDAO {
	public List<InventoryProduct> inventoryProductList();
	public List<SupplierProductList> fetchSupplierListByProductId(long productId);
	public void addInventory(InventoryRequest inventoryRequest);
	public void editInventory(InventoryRequest inventoryRequest);
	public Inventory fetchInventory(String inventoryId);
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId);
	public void givePayment(PaymentPaySupplier paymentPaySupplier);
	public void updateSupplierPayment(PaymentPaySupplier paymentPaySupplier);
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range);
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId);
	//public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList);
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel);
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,
			String toDate, String range);
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId);
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId);
	//public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(List<InventoryDetails> inventoryDetailsList);
	public double fetchTotalValueOfCurrrentInventory();
	public long fetchProductUnderThresholdCount();
	public void deleteInventory(String inventoryId);
	public void editInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel);
	public List<PaymentPaySupplier> fetchPaymentPaySupplierListByInventoryId(String inventoryId);
	public void deletePaymentPaySupplierListByInventoryId(String paymentPaySupplierId);
	public PaymentPaySupplier fetchPaymentPaySupplierListByPaymentPaySupplierId(String paymentPaySupplierId);
	public PaymentDoInfo fetchPaymentPaySupplierForEdit(String paymentPaySupplierId);
	public double totalSupplierPaidAmountForProfitAndLoss(String startDate,String endDate);
	public InventoryAddedInvoiceModel getInvoiceDetails(String inventoryTsnId);
}
