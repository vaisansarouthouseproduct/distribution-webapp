package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.responseEntities.TransportationModel;


public interface TransportationDAO {

	public Transportation fetchById(String id);
	
	public List<Transportation> fetchAll();

	public void save(Transportation transportation);
	
	public void update(Transportation transportation);
	
	public void delete(Transportation transportation);
	
	public List<TransportationModel> fetchTransportationModelAll();
}
