package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.ShopVisitDAO;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ShopVisit;
import com.bluesquare.rc.entities.ShopVisitImages;
import com.bluesquare.rc.utils.DatePicker;


/***
 * <pre>
 * @author Sachin Sharma 02-06-2018 Code Documentation
 * API End Points
 * 1.saveShopVisit(ShopVisit ShopVisit)
 * 2.fetchShopVisitedByDateRange(long employeeId, String range, String fromDate, String toDate) 
 * 3.
 * 4.
 * 5.public int findNoOfShopVisitBetweenTwoDates(String startDate,String endDate,long employeeId)
 * </pre>
 */

@Repository("shopVisitDAO")
@Component
public class ShopVisitDAOImpl extends TokenHandler implements ShopVisitDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	ContactDAO contactDAO;

	@Transactional
	public void saveShopVisit(ShopVisit shopVisit) {
		// TODO Auto-generated method stub
		shopVisit.setShopVisitAddeddDatetime(new Date());
		contactDAO.save(shopVisit.getContact());
		Employee employee=employeeDetailsDAO.getEmployeeDetailsByemployeeId(shopVisit.getEmployee().getEmployeeId()).getEmployee();
		shopVisit.setEmployee(employee);
		sessionFactory.getCurrentSession().save(shopVisit);
	}
	
	public void saveShopVisitImage(String shopImage,ShopVisit shopVisit){
		ShopVisitImages shopVisitImages=new ShopVisitImages();
		shopVisitImages.setShopImage(shopImage);
		shopVisitImages.setShopVisit(shopVisit);
		sessionFactory.getCurrentSession().save(shopVisitImages);
	}

	public void updateShopVisitImage(String shopImage,ShopVisit shopVisit){
		ShopVisitImages shopVisitImages=fetchShopVisitImage(shopVisit.getId());
		if(shopVisitImages==null){
			ShopVisitImages shopVisitImageNew=new ShopVisitImages();
			shopVisitImageNew.setShopImage(shopImage);
			shopVisitImageNew.setShopVisit(shopVisit);
			sessionFactory.getCurrentSession().save(shopVisitImageNew);
			
		}else{
			shopVisitImages.setShopImage(shopImage);
			shopVisitImages.setShopVisit(shopVisit);
			sessionFactory.getCurrentSession().update(shopVisitImages);	
		}
		
	}
	
	public ShopVisitImages fetchShopVisitImage(long shopVisitId){
		String hql="from ShopVisitImages where shopVisit.id="+shopVisitId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ShopVisitImages> shopVisitImages=(List<ShopVisitImages>)query.list();
		if(shopVisitImages.isEmpty()){
			return null;
		}
		return shopVisitImages.get(0);
	}
	
	
	@Transactional
	public List<ShopVisit> fetchShopVisitedByDateRange(long employeeId, String range, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query=null;
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				if(range.equals("range"))
				{
					hql="from ShopVisit where  employee.employeeId="+employeeId+" And (date(shopVisitAddeddDatetime)>='"+fromDate+"' And date(shopVisitAddeddDatetime)<='"+toDate+"')";
				}
				else if(range.equals("last7days"))
				{
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from ShopVisit where employee.employeeId="+employeeId+" And date(shopVisitAddeddDatetime)>='"+dateFormat.format(cal.getTime())+"'";
				}
				else if(range.equals("today"))
				{
					hql="from ShopVisit where employee.employeeId="+employeeId+" And date(shopVisitAddeddDatetime)=date(CURRENT_DATE())";
				}
				else if(range.equals("last1month"))
				{
					hql="from ShopVisit where employee.employeeId="+employeeId+" And (date(shopVisitAddeddDatetime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(shopVisitAddeddDatetime)<='"+DatePicker.getLastMonthLastDate()+"')";					
				}else if(range.equals("last3months")){
					cal.add(Calendar.MONTH, -6);
					hql="from ShopVisit where employee.employeeId="+employeeId+" And (date(shopVisitAddeddDatetime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(shopVisitAddeddDatetime)<='"+DatePicker.getLast3MonthLastDate()+"')";
				}
				else if(range.equals("pickDate")){
					hql="from ShopVisit where employee.employeeId="+employeeId+" And date(shopVisitAddeddDatetime)='"+fromDate+"'";
				}
				else if(range.equals("viewAll")){
					hql="from ShopVisit where employee.employeeId="+employeeId;
				}
				else if(range.equals("currentMonth"))
				{
					hql="from ShopVisit where employee.employeeId="+employeeId+" And (date(shopVisitAddeddDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(shopVisitAddeddDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
				}
				hql+=" and employee.company.companyId="+getSessionSelectedCompaniesIds();
				hql+=" and branch.branchId="+getSessionSelectedBranchIds();
				query=sessionFactory.getCurrentSession().createQuery(hql);
				List<ShopVisit> shopVisitList=(List<ShopVisit>)query.list();
				if(shopVisitList.isEmpty()){
					return null;
				}
				
				return shopVisitList;
	}

	@Transactional
	public ShopVisit fetchShopVisitById(long visitId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query=null;
		hql="from ShopVisit where id="+visitId;
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ShopVisit> shopVisit=(List<ShopVisit>)query.list();
		if(shopVisit.isEmpty()){
			return null;
		}
		return shopVisit.get(0);
	}

	@Transactional
	public void updateShopVisit(ShopVisit shopVisit){
		Contact contact=shopVisit.getContact();
		contact=(Contact)sessionFactory.getCurrentSession().merge(contact);
		sessionFactory.getCurrentSession().update(contact);
		
		shopVisit=(ShopVisit)sessionFactory.getCurrentSession().merge(shopVisit);
		sessionFactory.getCurrentSession().update(shopVisit);
	}
	/**
	 * <pre>
	 * find how many shop visit by start ans end date and employeeId
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param employeeId
	 * @return no of visits
	 */
	@Transactional
	public int findNoOfShopVisitBetweenTwoDates(String startDate,String endDate,long employeeId){
		String hql="from ShopVisit where employee.employeeId="+employeeId;
			   hql+=" and date(shopVisitAddeddDatetime)<='"+startDate+"' and date(shopVisitAddeddDatetime)>='"+endDate+"'";
			   hql+=" and employee.company.companyId="+getSessionSelectedCompaniesIds();
			   //hql+=" and branch.branchId="+getSessionSelectedBranchIds();
	    Query query=sessionFactory.getCurrentSession().createQuery(hql);
	    List<ShopVisit> shopVisitList=(List<ShopVisit>)query.list();
	    if(shopVisitList.isEmpty()){
	    	return 0;
	    }else{
	    	return shopVisitList.size();
	    }
	}
	
}
