package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.FetchSupplierList;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.utils.SendSMS;
import com.bluesquare.rc.utils.SupplierIdGenerator;
/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of SupplierDAO
 * 1.fetchSupplierForWebAppList()
 * 2.fetchSupplier(String supplierId)
 * 3.saveSupplier(Supplier supplier)
 * 4.updateSupplier(Supplier supplier)
 * 5.saveSupplierProductList(String productIdList,String supplierId)
 * 6.updateSupplierProductList(String productIdList,String supplierId)
 * 7.fetchSupplierForWebApp()
 * 8.fetchSupplierProductListBySupplierIdForWebApp(String supplierId)
 * 9.updateSupplierProductList(SupplierProductList supplierProductList)
 * 10.fetchSupplierProductListForChangeUnitPriceByTaxSlab(long categoryId)
 * 11.fetchSupplierProductRate(long productId,String supplierId)
 * 12.fetchSupplierByProductIdAndSupplierId(long productId,String supplierId)
 * 13.fetchProductBySupplierId(String supplierId)
 * 14.sendSMSTOSupplier(String supplierIds,String smsText, String mobileNumber)
 * 15.checkSupplierDuplication(String checkText,String type,String supplierId)
 * </pre>
 */
@Component
public class SupplierDAOimpl extends TokenHandler implements SupplierDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	Company company;
	
	@Autowired
	Supplier supplier;

	@Autowired
	SupplierProductList supplierProductList;
	
	@Autowired
	Product product;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired 
	AreaDAO areaDAO;
	
	@Autowired
	SupplierIdGenerator supplierIdGenerator;
	
	@Autowired
	ContactDAO contactDAO;
	
	

	/**
	 * <pre>
	 * fetch supplier list which belongs to logged user company
	 * @return Supplier list
	 * </pre>
	 */
	@Transactional
	public List<Supplier> fetchSupplierForWebAppList() {
		String hql="from Supplier where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<Supplier> list=(List<Supplier>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}
	
	/**
	 * <pre>
	 * fetch supplier list which belongs to logged user company
	 * @return SupplierModel list
	 * </pre>
	 */
	@Transactional
	public List<SupplierModel> fetchSupplierModelList() {
		
		List<Supplier> list=fetchSupplierForWebAppList();
		if(list==null)
		{
			return null;
		}
		List<SupplierModel> supplierModelList=new ArrayList<>();
		for(Supplier supplier : list){
			supplierModelList.add(new SupplierModel(supplier.getSupplierPKId(), supplier.getSupplierId(), supplier.getName(),supplier.getContact()));
		}
		return supplierModelList;
	}
	/**
	 * <pre>
	 * fetch supplier by supplier id which belongs to logged user company
	 * @param supplierId
	 * @return Supplier
	 * </pre>
	 */
	@Transactional
	public Supplier fetchSupplier(String supplierId) {
		
		String hql="from Supplier where supplierId='"+supplierId+"'";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<Supplier> list=(List<Supplier>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}
	/**
	 * <pre>
	 * fetch supplier by supplier id which belongs to logged user company
	 * @param supplier
	 * @return Supplier
	 * </pre>
	 */
	@Transactional
	public Supplier saveSupplier(Supplier supplier) {

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		supplier.setCompany(company);
		
		sessionFactory.getCurrentSession().save(supplier.getContact());
		 
		supplier.setSupplierId(supplierIdGenerator.generateSupplierId());
		sessionFactory.getCurrentSession().save(supplier);
		
		return null;
	}
	/**
	 * <pre>
	 * set company
	 * update supplier contact
	 * update supplier 
	 * @param supplier
	 * @return Supplier
	 * </pre>
	 */
	@Transactional
	public Supplier updateSupplier(Supplier supplier) {
		
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		supplier.setCompany(company);
		
		
		contactDAO.update(supplier.getContact());
		
		supplier=(Supplier)sessionFactory.getCurrentSession().merge(supplier);
		sessionFactory.getCurrentSession().update(supplier);
		return null;
	}
	/**
	 * <pre>
	 * save supplier product list against given supplier id
	 * @param productIdList
	 * @param supplierId
	 * @return Supplier
	 * </pre>
	 */
	@Transactional
	public Supplier saveSupplierProductList(String productIdList,String supplierId) {
		// TODO Auto-generated method stub
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			SupplierProductList supplierProductList=new SupplierProductList();
					
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierProductList.setProduct(product);
			supplierProductList.setSupplierRate(Double.parseDouble(prdutIdAndRate[1]));
			
			supplier=fetchSupplier(supplierId);
			supplierProductList.setSupplier(supplier);
			
			sessionFactory.getCurrentSession().save(supplierProductList);
		}
		return null;
	}
	/**
	 * <pre>
	 * delete old supplier product list
	 * update supplier product list against given supplier id
	 * @param productIdList
	 * @param supplierId
	 * @return Supplier
	 * </pre>
	 */
	@Transactional
	public Supplier updateSupplierProductList(String productIdList,String supplierId) {
		
		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		
		List<SupplierProductList> list=query.list();
		Iterator<SupplierProductList> itr=list.iterator();
		while(itr.hasNext())
		{
			SupplierProductList supplierProductList=itr.next();
			supplierProductList=(SupplierProductList)sessionFactory.getCurrentSession().merge(supplierProductList);
			sessionFactory.getCurrentSession().delete(supplierProductList);
		}		
		// TODO Auto-generated method stub
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			SupplierProductList supplierProductList=new SupplierProductList();
					
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierProductList.setProduct(product);
			supplierProductList.setSupplierRate(Double.parseDouble(prdutIdAndRate[1]));
			
			supplier=fetchSupplier(supplierId);
			supplierProductList.setSupplier(supplier);
			
			sessionFactory.getCurrentSession().save(supplierProductList);
		}
		
		return null;
	}
	/**
	 * <pre>
	 * fetch supplier list using  FetchSupplierList model
	 * @return FetchSupplierList list
	 * </pre>
	 */
	@Transactional
	public List<FetchSupplierList> fetchSupplierForWebApp() {
		List<Supplier> list=fetchSupplierForWebAppList();
		if(list==null)
		{
			return null;
		}
		Iterator<Supplier> itr=list.iterator();
		List<FetchSupplierList> fetchSupplierList=new ArrayList<FetchSupplierList>();
		long srNo=0;
		while(itr.hasNext())
		{
			Supplier supplier=itr.next();
			srNo++;
			fetchSupplierList.add(new FetchSupplierList(supplier.getSupplierId(), 
														srNo, 
														supplier.getName(), 
														supplier.getContact().getEmailId(), 
														supplier.getContact().getMobileNumber(), 
														supplier.getAddress(), 
														supplier.getGstinNo(),
														supplier.getSupplierAddedDatetime(),
														supplier.getSupplierUpdatedDatetime()));
			
		}
		
		
		return fetchSupplierList;
	}
	/**
	 * <pre>
	 * supplier product list by supplier id
	 * @param supplierId
	 * @return SupplierProductList
	 * </pre>
	 */
	@Transactional
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId) {
		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}
	/**
	 * <pre>
	 * update supplier product rate when change category percentage
	 * @param supplierProductList
	 * </pre>
	 */
	@Transactional
	public void updateSupplierProductList(SupplierProductList supplierProductList){
		supplierProductList=(SupplierProductList)sessionFactory.getCurrentSession().merge(supplierProductList);
		sessionFactory.getCurrentSession().update(supplierProductList);
	} 
	/**
	 * <pre>
	 * fetch supplier product list by supplier id 
	 * @param categoryId
	 * </pre>
	 */
	@Transactional
	public List<SupplierProductList> fetchSupplierProductListForChangeUnitPriceByTaxSlab(long categoryId) {
		String hql="from SupplierProductList where 1=1 "
				+" and product.categories.categoryId="+categoryId
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}

	/*@Transactional
	public List<SupplierProductList> makeSupplierProductListNullForWebApp(
			List<SupplierProductList> supplierProductList) {
		
			List<SupplierProductList> sPList=new ArrayList<>();
			
			Iterator<SupplierProductList> itr=supplierProductList.iterator();
			while(itr.hasNext())
			{
				SupplierProductList supplierPrdctList=itr.next();
				supplierPrdctList.getProduct().setProductImage(null);
				sPList.add(supplierPrdctList);
			}			

		return sPList;
	}*/
	/**
	 * <pre>
	 * get product id list in string
	 * @param supplierProductList list
	 * @return product id list
	 * </pre>
	 */
	@Transactional
	public String getProductIdList(List<SupplierProductList> supplierProductList) {
		List<SupplierProductList> sPList=new ArrayList<>();
		
		Iterator<SupplierProductList> itr=supplierProductList.iterator();
		String productIds="";
		while(itr.hasNext())
		{
			SupplierProductList supplierPrdctList=itr.next();
			productIds=productIds+supplierPrdctList.getProduct().getProductId()+"-"+supplierPrdctList.getSupplierRate()+",";
			sPList.add(supplierPrdctList);
		}			

		productIds=productIds.substring(0, productIds.length() - 1);
	return productIds;
	}
	/**
	 * <pre>
	 * fetch supplierProduct by supplier and product id
	 * @param productId
	 * @param supplierId
	 * @return SupplierProductList
	 * </pre>
	 */
	@Transactional
	public SupplierProductList fetchSupplierProductRate(long productId,String supplierId) {
		
		String hql="from SupplierProductList where product.productId="+productId+" and supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		return list.get(0);
	}
	/**
	 * <pre>
	 * fetch supplierProduct by supplier and product id
	 * @param productId
	 * @param supplierId
	 * @return SupplierProductList
	 * </pre>
	 */
	@Transactional
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId,String supplierId) {
		
		String hql="from SupplierProductList where product.productId="+productId+" and supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		
		return list.get(0);
	}
	/**
	 * <pre>
	 * fetch supplierProduct by supplier id
	 * @param supplierId
	 * @return SupplierProductList
	 * </pre>
	 */
	@Transactional
	public List<ProductAddInventory> fetchProductBySupplierId(String supplierId){
		
		List<ProductAddInventory> productAddInventories=new ArrayList<ProductAddInventory>();
		
		
		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<SupplierProductList> list= (List<SupplierProductList>) query.list();
		
		if(list.isEmpty()){
			return null;
		}
		
		Iterator<SupplierProductList> iterator=list.iterator();
		while(iterator.hasNext()){
		SupplierProductList supplierProductList=iterator.next();
		Categories category=supplierProductList.getProduct().getCategories();
		Brand brand=supplierProductList.getProduct().getBrand();
		ProductAddInventory addInventory=new ProductAddInventory();
		addInventory.setCurrentQuantity(supplierProductList.getProduct().getCurrentQuantity());
		addInventory.setProductBrandId(supplierProductList.getProduct().getBrand().getBrandId());
		addInventory.setProductCategoryId(supplierProductList.getProduct().getCategories().getCategoryId());
		addInventory.setProductCode(supplierProductList.getProduct().getProductCode());
		addInventory.setProductId(supplierProductList.getProduct().getProductId());
		addInventory.setProductName(supplierProductList.getProduct().getProductName());
		addInventory.setRate(supplierProductList.getProduct().getRate());
		addInventory.setProductThreshold(supplierProductList.getProduct().getThreshold());
		addInventory.setSupplierRate(supplierProductList.getSupplierRate());
		addInventory.setCategories(new CategoriesModel(
				category.getCategoryId(), 
				category.getCategoryName(), 
				category.getCategoryDescription(), 
				category.getHsnCode(), 
				category.getCgst(), 
				category.getSgst(), 
				category.getIgst(), 
				category.getCategoryDate(), 
				category.getCategoryUpdateDate()));
		addInventory.setBrand(new BrandModel(
				brand.getBrandId(), 
				brand.getName(), 
				brand.getBrandAddedDatetime(), 
				brand.getBrandUpdateDatetime()));
		addInventory.setBranch(supplierProductList.getSupplier().getBranch());
		productAddInventories.add(addInventory);
		}
		return productAddInventories;
	}
	/**
	 * <pre>
	 * send sms to supplier 
	 * if one supplier id get then send sms to given mobile number 
	 * else send on registered mobile number
	 * @param supplierIds
	 * @param smsText
	 * @param mobileNumber
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String sendSMSTOSupplier(String supplierIds,String smsText, String mobileNumber) {

		try {
			String endText=getEndTextForSMS();	
			
			String[] supplierIds2 = supplierIds.split(",");
			
			if(supplierIds2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+supplierIds2[0]);
				return "Success";
			}
			
			for(int i=0; i<supplierIds2.length; i++)
			{
				supplier=fetchSupplier(supplierIds2[i]);
				SendSMS.sendSMS(Long.parseLong(supplier.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+supplierIds2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	/**
	 * <pre>
	 * validation when supplier add / update
	 * 
	 * @param checkText
	 * @param type
	 * @param supplierId
	 * </pre>
	 */
	@Transactional
	public String checkSupplierDuplication(String checkText,String type,String supplierId){
		
		String hql="";
		if(type.equals("name")){
			hql="from Supplier where name='"+checkText+"'";
		}else if(type.equals("gstNo")){
			hql="from Supplier where gstinNo='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from Supplier where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from Supplier where contact.emailId='"+checkText+"'";
		}		
		if(!supplierId.equals("0")){
			hql+=" and supplierId<>'"+supplierId+"'";
		}
		hql+=" and company.companyId="+getSessionSelectedCompaniesIds()
			+" and branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Supplier> supplierList=(List<Supplier>)query.list();
		if(supplierList.isEmpty()){
			return "Success";
		}
		return "Failed";		
	}
	
	
}
