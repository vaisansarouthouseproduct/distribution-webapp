package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.DepartmentDAO;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Department;
/**
 * <pre>
 * @author Sachin Pawar 24-05-2018 Code Documentation
 * provides Implementation for following methods of DepartmentDAO
 * 1.saveForWebApp(Department department)
 * 2.updateForWebApp(Department department)
 * 3.fetchDepartmentListForWebApp()
 * 4.fetchDepartmentForWebApp(long departmentId)
 * </pre>
 */
@Repository("departmentDAO")

@Component
public class DepartmentDAOImpl extends TokenHandler implements DepartmentDAO {

	
	@Autowired
	SessionFactory sessionFactory;

	

	@Transactional
	public void saveForWebApp(Department department) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(department);
	}

	@Transactional
	public void updateForWebApp(Department department) {
		// TODO Auto-generated method stub
		department=(Department)sessionFactory.getCurrentSession().merge(department);
		sessionFactory.getCurrentSession().update(department);
	}

	@Transactional
	public List<Department> fetchDepartmentListForWebApp() {
		String hql = "from Department where name<>'Admin'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Department> departmentList = (List<Department>) query.list();

		if (departmentList.isEmpty()) {
			return null;
		}
		return departmentList;
	}

	@Transactional
	public Department fetchDepartmentForWebApp(long departmentId) {
		// TODO Auto-generated method stub
		return (Department)sessionFactory.getCurrentSession().get(Department.class, departmentId);
	}
	

}
