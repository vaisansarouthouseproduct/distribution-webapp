package com.bluesquare.rc.dao.impl;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.CountryDAO;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.utils.AllAccess;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.bluesquare.rc.utils.InvoiceNumberGenerate;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;

/**
 * <pre>
 * @author Sachin Pawar 24-05-2018 Code Documentation
 * provides Implementation for following methods of CounterOrderDAO
 * 1.fetchCountryListForWebApp()
 * 2.fetchCountryForWebApp(long coutryId)
 * 3.saveCountryForWeb(Country country)
 * 4.updateCountryForWeb(Country country)
 * 5.fetchIndiaCountry()
 * </pre>
 */
@Repository("countryDAO")

@Component
public class CountryDAOImpl extends TokenHandler implements CountryDAO {

	@Autowired
	HttpSession session;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	public CountryDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public CountryDAOImpl() { }

	
	/**
	 * <pre>
	 * fetch all country list
	 * @return country list
	 * </pre>
	 */
	@Transactional
	public List<Country> fetchCountryListForWebApp() {
			String hql = "from Country where 1=1 ";
			//hql=modifyQueryForSelectedAccess(hql);
			
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			System.out.println("Query : "+query);
			@SuppressWarnings("unchecked")
			List<Country> countries = (List<Country>) query.list();
			
			if (countries.isEmpty()) {
				return null;
			}
			
			return countries;
		}
	
	@Transactional
	public Country fetchCountryForWebApp(long coutryId) {
		String hql = "from Country where countryId=" + coutryId;
		//hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Country> countryList = (List<Country>) query.list();
		if (countryList.isEmpty()) {
			return null;
		}

		return countryList.get(0);
	}

	@Transactional
	public Country saveCountryForWeb(Country country) {
		sessionFactory.getCurrentSession().save(country);
		return null;
	}

	@Transactional
	public Country updateCountryForWeb(Country country) {
		country=(Country)sessionFactory.getCurrentSession().merge(country);
		sessionFactory.getCurrentSession().update(country);
		return null;
	}

	@Transactional
	public Country fetchIndiaCountry() {
		try {
			String hql = "from Country where name='India'";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);

			
			@SuppressWarnings("unchecked")
			List<Country> list = (List<Country>) query.list();
			Iterator<Country> iterator = list.iterator();
			if(list.isEmpty())
			{
				return null;
			}

			return list.get(0);
		} catch (Exception e) {
			System.out.println(" fetchCountry Error : " + e.toString());
			return null;
		}
	}

	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long countryId : allAccess.getCountryIdList())
		{
			ids+=countryId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and countryId in ("+ids+")";
		
		return hql;
	}*/

	/*@Override
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] countryIdList=decodedJWT.getClaim("countryIdList").asArray(longClass);
				
				String ids="";
				for(Long countryId : countryIdList)
				{
					ids+=countryId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and countryId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}*/
	
}
