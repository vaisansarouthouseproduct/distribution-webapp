package com.bluesquare.rc.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AddressDAO;
import com.bluesquare.rc.entities.Address;

/**
 * <pre>
 * @author Sachin Pawar	19-03-2018 Code Documentation
 * 
 * Provides Implementation for following methods of AddressDAO
 * 1. save
 * 2. update
 * 3. delete
 * </pre>
 */

@Repository("addressDAO")
@Component
public class AddressDAOImpl extends TokenHandler implements AddressDAO {

	@Autowired
	SessionFactory sessionFactory;

	/**
	 * save address
	 * @param address
	 */
	@Transactional
	public void save(Address address) {
		sessionFactory.getCurrentSession().save(address);

	}

	/**
	 * update address
	 * @param address
	 */
	@Transactional
	public void update(Address address) {
		address=(Address)sessionFactory.getCurrentSession().merge(address);
		sessionFactory.getCurrentSession().update(address);
	}

	/**
	 * delete address by id
	 * @param address
	 */
	@Transactional
	public void delete(long id) {
		Address addressToDelete = new Address();
		addressToDelete.setAddressId(id);
		addressToDelete=(Address)sessionFactory.getCurrentSession().merge(addressToDelete);
		sessionFactory.getCurrentSession().delete(addressToDelete);
	}
}
