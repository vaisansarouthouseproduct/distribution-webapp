package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.entities.AppVersion;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeRoutes;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.models.CollectionReportPaymentDetails;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.PaymentService;

/**
 * <pre>
 * @author Sachin Pawar 24-05-2018 Code Documentation
 * provides Implementation for following methods of EmployeeDAO
 * 1.validate(String username, String password)
 * 2.checkAppVersion(String appVersion)
 * 3.getEmployeeDetails(long employeeId
 * 4.loginCredentialsForRC(String userid, String password)
 * 8.logout(long employeeId)
 * 9.saveForWebApp(Employee employee)
 * 10.updateForWebApp(Employee employee)
 * 11.checkEmployeeDuplication(String checkText,String type,long employeeDetailsId)
 * 12.fetchBranchListByEmployeeId(long employeeId)
 * </pre>
 */
@Repository("employeeDAO")

@Component
public class EmployeeDAOImpl extends TokenHandler implements EmployeeDAO{

	@Autowired 
	SessionFactory sessionFactory;
	
	@Autowired
	Employee employee;	
	
	@Autowired
	Company company;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	InventoryService inventoryService;
	
	public EmployeeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public EmployeeDAOImpl() { }

	/**
	 * <pre>
	 * validate employee by username and password for login
	 * @param username
	 * @param password
	 * @return Employee
	 * </pre>
	 */
	@Transactional
	public Employee validate(String username, String password) {
		String hql = "from Employee where userId= :user and password=:pass";// and department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("user", username);
		query.setParameter("pass", password);
		
		@SuppressWarnings("unchecked")
		List<Employee> list = (List<Employee>) query.list();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	/**
	 * <pre>
	 * check app version with database store version and show for update
	 * @param appVersion
	 * </pre>
	 */
	@Transactional
    public String checkAppVersion(String appVersion){
            // TODO Auto-generated method stub
            
            String hql="from AppVersion ";
            Query query=sessionFactory.getCurrentSession().createQuery(hql);
            List<AppVersion> appVersions=(List<AppVersion>)query.list();
            if(appVersions.get(0).getAppVersion().equalsIgnoreCase(appVersion)){
                    return "UptoDate";
            }
            return "Update available";
    }

	/**
	 * <pre>
	 * get EmployeeDetails by employee Id
	 * @param employeeId
	 * @return EmployeeDetails
	 * </pre>
	 */
	@Transactional
	public EmployeeDetails getEmployeeDetails(long employeeId) {
		String hql = "from EmployeeDetails where employee=" + employeeId+
				" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList= (List<EmployeeDetails>) query.list();
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}
	/**
	 * <pre>
	 * check employee by username and password for login
	 * @param username
	 * @param password
	 * @return Employee
	 * </pre>
	 */
	@Transactional
	public Employee loginCredentialsForRC(String userid, String password) {
		
		try {
			String hql = "from Employee where userId= :user and password=:pass";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("user", userid);
			query.setParameter("pass", password);
			
			@SuppressWarnings("unchecked")
			List<Employee> listEmp=(List<Employee>) query.list();
			if(listEmp.isEmpty()){
				return null;
			}
			return listEmp.get(0);
		} catch (Exception e) {
			System.out.println("loginCredentialsForRC Error : " + e.toString());
			return null;
		}
				
	}
	/**
	 * <pre>
	 * logout from app and clear own token from database
	 * @param employeeId
	 * </pre>
	 */
	@Transactional
	public void logout(long employeeId) {
		// TODO Auto-generated method stub

		try {
			String hql = "from EmployeeDetails where employee.employeeId =" + employeeId;
			hql+=" and employee.company.companyId="+getSessionSelectedCompaniesIds();
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeDetails> employeeDetails = (List<EmployeeDetails>) query.list();
			employeeDetails.get(0).setToken(null);
			EmployeeDetails employeeDetails2=employeeDetails.get(0);  
			
			employeeDetails2=(EmployeeDetails)sessionFactory.getCurrentSession().merge(employeeDetails2);
			sessionFactory.getCurrentSession().update(employeeDetails2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
	

	/**
	 * <pre>
	 * save employee
	 * save employee branches
	 * save employee routes
	 * @param employee
	 * @param employeeBranchesList
	 * 
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		employee.setCompany(company);
		sessionFactory.getCurrentSession().save(employee);
		
		for(EmployeeBranches employeeBranches: employeeBranchesList){
			employeeBranches.setEmployee(employee);
			sessionFactory.getCurrentSession().save(employeeBranches);
		}
		for(EmployeeRoutes employeeRoutes: employeeRoutesList){
			employeeRoutes.setEmployee(employee);
			sessionFactory.getCurrentSession().save(employeeRoutes);
		}
	}
	/**
	 * <pre>
	 * update employee
	 * update employee branches
	 * @param employee
	 * @param employeeBranchesList
	 * 
	 * </pre>
	 */
	@Transactional
	public void updateForWebApp(Employee employee,List<EmployeeBranches> employeeBranchesList,List<EmployeeRoutes> employeeRoutesList) {
		// TODO Auto-generated method stub
		employee=(Employee)sessionFactory.getCurrentSession().merge(employee);
		sessionFactory.getCurrentSession().update(employee);
		
		String hql="delete from EmployeeBranches where employee.employeeId="+employee.getEmployeeId();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
		
		for(EmployeeBranches employeeBranches: employeeBranchesList){
			employeeBranches.setEmployee(employee);
			sessionFactory.getCurrentSession().save(employeeBranches);
		}	
		
		hql="delete from EmployeeRoutes where employee.employeeId="+employee.getEmployeeId();
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
		
		for(EmployeeRoutes employeeRoutes: employeeRoutesList){
			employeeRoutes.setEmployee(employee);
			sessionFactory.getCurrentSession().save(employeeRoutes);
		}		
		
	}
	@Transactional
	public void updateForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		employee=(Employee)sessionFactory.getCurrentSession().merge(employee);
		sessionFactory.getCurrentSession().update(employee);		
		
	}
	/**
	 * fetch branches of employee by employeeId
	 * @param employeeId
	 * @return Branch list
	 */
	public List<Branch> fetchBranchListByEmployeeId(long employeeId){
		String hql="select branch from EmployeeBranches eb where eb.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Branch> branchList=(List<Branch>)query.list();
		return branchList;
	}
	
	/**
	 * fetch routes of employee by employeeId
	 * @param employeeId
	 * @return Branch list
	 */
	public List<Route> fetchRouteListByEmployeeId(long employeeId){
		String hql="select route from EmployeeRoutes eb where eb.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> branchList=(List<Route>)query.list();
		return branchList;
	}
	
	/**
	 * <pre>
	 * check employee duplication when insert new employee
	 * userId also with company login
	 * mobile number
	 * emailId
	 * @param checkText
	 * @param type
	 * @param employeeDetailsId
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String checkEmployeeDuplication(String checkText,String type,long employeeDetailsId){
		
		String hql="";
		if(type.equals("userId")){
			hql="from EmployeeDetails where employee.userId='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from EmployeeDetails where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from EmployeeDetails where contact.emailId='"+checkText+"'";
		}		
		if(employeeDetailsId!=0){
			hql+=" and employeeDetailsId<>'"+employeeDetailsId+"'";
		}
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeList=(List<EmployeeDetails>)query.list();
		
		if(type.equals("userId")){
			hql="from Company where userId='"+checkText+"'";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Company> companyList=(List<Company>)query.list();
			
			if(employeeList.isEmpty() && companyList.isEmpty()){
				return "Success";
			}else{
				return "Failed";
			}
		}
		
		if(employeeList.isEmpty()){
			return "Success";
		}
		
		return "Failed";		
	}


	// Set the product under thresold and pending payment count in session. 
	
		public void setNotificationCountInSession(HttpSession session){
			// To Show the pending Payment Order Count in notification
			 List<CollectionReportPaymentDetails> collectionReportPaymentDetailsList=paymentService.getPendingPaymentList();
			 long pendingCounterOrderPaymentCount=0;
			 if(collectionReportPaymentDetailsList!=null){
				 pendingCounterOrderPaymentCount  =collectionReportPaymentDetailsList.size();
			 }
			 
			
			 //to show the Product under threshold count
				List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
				List<InventoryProduct> inventoryProductsListNew=new ArrayList<>();
				if(inventoryProductsList!=null){
					long srno=1;		
					for(InventoryProduct inventoryProduct :inventoryProductsList)
					{
						ProductModel product=inventoryProduct.getProduct();
						if(product.getCurrentQuantity()<=product.getThreshold())
						{
							inventoryProduct.setSrno(srno++);
							inventoryProductsListNew.add(inventoryProduct);
						}
					}
				}
				
				
				
				
				session.setAttribute("productUnderThesoldCount", inventoryProductsListNew.size());
				session.setAttribute("pendingCounterOrderPaymentCount", pendingCounterOrderPaymentCount);
				
				
		}
}