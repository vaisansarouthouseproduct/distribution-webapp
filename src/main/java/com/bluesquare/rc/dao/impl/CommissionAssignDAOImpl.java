package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CommissionAssignDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.TargetAssignDAO;
import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeCommission;
import com.bluesquare.rc.entities.EmployeeCommissionDetails;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.entities.TargetType;
import com.bluesquare.rc.models.CommissionAssignAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.CommissionAssignRequest;
import com.bluesquare.rc.models.EmployeeCommissionDetailsModel;
import com.bluesquare.rc.models.EmployeeCommissionModel;
import com.bluesquare.rc.rest.models.CommissionAssignModel;
import com.bluesquare.rc.rest.models.CommissionAssignResModel;
import com.bluesquare.rc.rest.models.TargetTypeModel;

@Repository("commissionAssignDAO")
public class CommissionAssignDAOImpl extends TokenHandler implements CommissionAssignDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	TargetAssignDAO targetAssignDAO;
	
	@Transactional
	public void saveCommissionAssignment(CommissionAssignRequest commissionAssignRequest ){
		
		CommissionAssign commissionAssign=commissionAssignRequest.getCommissionAssign();
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList=commissionAssignRequest.getCommissionAssignTargetSlabsList();
		List<CommissionAssignProducts> commissionAssignProductsList=commissionAssignRequest.getCommissionAssignProductsList();
		
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		commissionAssign.setCompany(company);
		commissionAssign.setAddedDatetime(new Date());
		commissionAssign.setUpdatedDatetime(null);
		sessionFactory.getCurrentSession().save(commissionAssign);
		
		TargetType targetType=fetchTargetTypeByTargetTypeName(commissionAssign.getTargetType().getType());
		if(targetType.getType().trim().equals("Product Wise Target")){
			for(CommissionAssignProducts commissionAssignProducts : commissionAssignProductsList){
				commissionAssignProducts.setCommissionAssign(commissionAssign);
				sessionFactory.getCurrentSession().save(commissionAssignProducts);
			}
		}
		
		for(CommissionAssignTargetSlabs commissionAssignTargetSlabs : commissionAssignTargetSlabsList){
			commissionAssignTargetSlabs.setCommissionAssign(commissionAssign);
			sessionFactory.getCurrentSession().save(commissionAssignTargetSlabs);
		}
	}
	
	@Transactional
	public void updateCommissionAssignment(CommissionAssignRequest commissionAssignRequest ){
		CommissionAssign commissionAssign=commissionAssignRequest.getCommissionAssign();
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList=commissionAssignRequest.getCommissionAssignTargetSlabsList();
		List<CommissionAssignProducts> commissionAssignProductsList=commissionAssignRequest.getCommissionAssignProductsList();
		
		CommissionAssign commissionAssignNew=fetchCommissionAssign(commissionAssign.getCommissionAssignId());
		commissionAssignNew.setTargetPeriod(commissionAssign.getTargetPeriod());
		commissionAssignNew.setTargetType(commissionAssign.getTargetType());
		commissionAssignNew.setCommissionAssignName(commissionAssign.getCommissionAssignName());
		commissionAssignNew.setUpdatedDatetime(new Date());
		commissionAssignNew.setCascadingCommission(commissionAssign.isCascadingCommission());
		commissionAssignNew.setIncludeTax(commissionAssign.isIncludeTax());
		commissionAssignNew.setAmountOrQtyTypeSlab(commissionAssign.getAmountOrQtyTypeSlab());
		//commissionAssignNew.setProdcutSlabType(commissionAssign.getAmountOrQtyTypeSlab());
		commissionAssignNew=(CommissionAssign)sessionFactory.getCurrentSession().merge(commissionAssignNew);
		sessionFactory.getCurrentSession().update(commissionAssignNew);
		
		TargetType targetType=fetchTargetTypeByTargetTypeName(commissionAssign.getTargetType().getType());
		if(targetType.getType().trim().equals("Product Wise Target")){
			String hql="delete from CommissionAssignProducts where commissionAssign.commissionAssignId="+commissionAssign.getCommissionAssignId();
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			query.executeUpdate();
			
			for(CommissionAssignProducts commissionAssignProducts : commissionAssignProductsList){
				commissionAssignProducts.setCommissionAssign(commissionAssign);
				sessionFactory.getCurrentSession().save(commissionAssignProducts);
			}
		}
		
		String hql="delete from CommissionAssignTargetSlabs where commissionAssign.commissionAssignId="+commissionAssign.getCommissionAssignId();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
		
		for(CommissionAssignTargetSlabs commissionAssignTargetSlabs : commissionAssignTargetSlabsList){
				commissionAssignTargetSlabs.setCommissionAssign(commissionAssign);
				sessionFactory.getCurrentSession().save(commissionAssignTargetSlabs);
		}
		
	}
	
	@Transactional
	public void disableCommissionAssignment(long commissionAssignId){
		
		CommissionAssign commissionAssignNew=fetchCommissionAssign(commissionAssignId);
		commissionAssignNew.setStatus(true);
		commissionAssignNew=(CommissionAssign)sessionFactory.getCurrentSession().merge(commissionAssignNew);
		sessionFactory.getCurrentSession().update(commissionAssignNew);
	}
	
	@Transactional
	public CommissionAssignModel fetchCommissionAssign(){
		CommissionAssignModel commissionAssignModel=new CommissionAssignModel();
		
		String hql=" from CommissionAssign com where com.status=false and  com.company.companyId="+getSessionSelectedCompaniesIds()+"";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssign> commissionAssignList=(List<CommissionAssign>)query.list();
		if(commissionAssignList.isEmpty())
		{
			commissionAssignModel.setCommissionAssignList(null);
			return commissionAssignModel;
		}
		List<CommissionAssignRequest> commissionAssignMainList=new ArrayList<>();
		for(CommissionAssign commissionAssign: commissionAssignList){
			CommissionAssignRequest commissionAssignRequest=new CommissionAssignRequest();
			CommissionAssignResModel commissionAssignResModel=new CommissionAssignResModel(
					commissionAssign.getCommissionAssignId(), 
					new TargetTypeModel(
							commissionAssign.getTargetType().getTargetTypeId(), 
							commissionAssign.getTargetType().getType(), 
							commissionAssign.getTargetType().getValueType()), 
					commissionAssign.getTargetPeriod(), 
					commissionAssign.getCommissionAssignName()); 
			commissionAssignRequest.setCommissionAssign1(commissionAssignResModel);
			//commissionAssignRequest.setCommissionAssignTargetSlabsList(fetchCommissionAssignTargetSlabs(commissionAssign.getCommissionAssignId()));
			commissionAssignMainList.add(commissionAssignRequest);
		}
		commissionAssignModel.setCommissionAssignList(commissionAssignMainList);
		return commissionAssignModel;
	}
	
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabs(long commissionAssignId){
		
		String hql=" from CommissionAssignTargetSlabs com where commissionAssign.commissionAssignId="+commissionAssignId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabs=(List<CommissionAssignTargetSlabs>)query.list();
		return commissionAssignTargetSlabs;
	}
	
	@Transactional
	public CommissionAssign fetchCommissionAssign(long commissionAssignId){
		String hql="from CommissionAssign where commissionAssignId="+commissionAssignId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssign> commissionAssignList=(List<CommissionAssign>)query.list();
		return commissionAssignList.get(0);
	}
	
	@Transactional
	public List<CommissionAssign> fetchCommissionAssignBuyDepartmentId(long departmentId){
		String hql="from CommissionAssign where status=false and targetType.department.departmentId="+departmentId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssign> commissionAssignList=(List<CommissionAssign>)query.list();
		if(commissionAssignList.isEmpty()){
			return null;
		}
		return commissionAssignList;
	}
	
	@Transactional
	public CommissionAssignAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId){
		List<CommissionAssign> commissionAssignList=fetchCommissionAssignBuyDepartmentId(departmentId);
		List<EmployeeDetails> employeeDetailsList=employeeDetailsDAO.fetchEmployeeDetailsByDepartmentId(departmentId);
		
		return new CommissionAssignAndEmployeeByDeptResponse(commissionAssignList, employeeDetailsList);
	} 
	
	@Transactional
	public CommissionAssignTargetSlabs findIncentiveAmountByCompleteTargetValue(double completeValue,long commissionAssignId){
		String hql="from CommissionAssignTargetSlabs where commissionAssign.status=false and commissionAssign.commissionAssignId="+commissionAssignId
				  +" and fromValue<='"+completeValue+"' and toValue>='"+completeValue+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabs=(List<CommissionAssignTargetSlabs>)query.list();
		if(commissionAssignTargetSlabs.isEmpty()){
			return null;
		}
		return commissionAssignTargetSlabs.get(0);
	}
	
	@Transactional
	public List<CommissionAssignProducts> fetchCommissionAssignProducts(long commissionAssignId){
		
		String hql=" from CommissionAssignProducts com where commissionAssign.commissionAssignId="+commissionAssignId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssignProducts> commissionAssignProducts=(List<CommissionAssignProducts>)query.list();
		return commissionAssignProducts;
	}
	
	@Transactional
	public boolean checkCommissionAssignNameAlreadyUsedOrNot(String commissionAssignName,long commissionAssignId){
		String hql="from CommissionAssign where commissionAssignName='"+commissionAssignName+"'";
		if(commissionAssignId!=0){
			hql+= " and commissionAssignId<>"+commissionAssignId;
		}			
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssign> commissionAssignList=(List<CommissionAssign>)query.list();
		if(commissionAssignList.isEmpty())
		{
			return false;
		}
		return true;
	}
	
	/**
	 * <pre>
	 * fetch target type by target type id
	 * @return TargetType
	 * </pre>
	 */
	@Transactional
	public TargetType fetchTargetTypeByTargetTypeId(long targetTypeId){
		String hql="from TargetType where targetTypeId="+targetTypeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetType> targetTypeList=(List<TargetType>)query.list();
		if(targetTypeList.isEmpty()){
			return null;
		}
		return targetTypeList.get(0);
	}
	
	/**
	 * <pre>
	 * fetch target type by target type name
	 * @return TargetType
	 * </pre>
	 */
	@Transactional
	public TargetType fetchTargetTypeByTargetTypeName(String name){
		String hql="from TargetType where type='"+name.trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetType> targetTypeList=(List<TargetType>)query.list();
		if(targetTypeList.isEmpty()){
			return null;
		}
		return targetTypeList.get(0);
	}
	
	/**
	 * <pre>
	 * fetch EmployeeCommission main details by month and year
	 * @return EmployeeCommission
	 * </pre>
	 */
	@Transactional
	public EmployeeCommission fetchEmployeeCommissionByMonthAndYear(long monthId, long yearId, long employeeDetailsId) {
		String hql="from EmployeeCommission where year(datetime)="+yearId+" and month(datetime)="+monthId+" and employeeDetails.employeeDetailsId="+employeeDetailsId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeCommission> employeeCommissionList=(List<EmployeeCommission>)query.list();
		if(employeeCommissionList.isEmpty()){
			return null;
		}
		return employeeCommissionList.get(0);
	}
	
	/**
	 * <pre>
	 * fetch EmployeeCommission main details by month and year and employeeDetailsId
	 * @return EmployeeCommission
	 * </pre>
	 */
	@Transactional
	public EmployeeCommission fetchEmployeeCommissionByMonthAndYearAndEmployeeDetailsId(long monthId, long yearId,long employeeDetailsId) {
		String hql="from EmployeeCommission where year(datetime)="+yearId+" and month(datetime)="+monthId+" and employeeDetails.employeeDetailsId="+employeeDetailsId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeCommission> employeeCommissionList=(List<EmployeeCommission>)query.list();
		if(employeeCommissionList.isEmpty()){
			return null;
		}
		return employeeCommissionList.get(0);
	}
	
	/**
	 * <pre>
	 * fetch EmployeeCommissionDetails by employeeCommissionId
	 * @return EmployeeCommissionDetails list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeCommissionDetails> fetchEmployeeCommissionDetails(long employeeCommissionId) {
		String hql="from EmployeeCommissionDetails where employeeCommission.employeeCommissionId="+employeeCommissionId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeCommissionDetails> employeeCommissionDetailsList=(List<EmployeeCommissionDetails>)query.list();
		if(employeeCommissionDetailsList.isEmpty()){
			return null;
		}
		return employeeCommissionDetailsList;
	}
	/**
	 * <pre>
	 * fetch employee commission details
	 * </pre>
	 * @param monthId
	 * @param yearId
	 * @return
	 */
	@Transactional
	public List<EmployeeCommissionModel> fetchEmployeeCommissionModel(long monthId,long yearId){
		List<EmployeeCommissionModel> employeeCommissionModelList=new ArrayList<>();
		
		List<EmployeeDetails> employeeDetailsList=employeeDetailsDAO.fetchEmployeeDetailsList();
		
		long srno=1;
		if(employeeDetailsList!=null){
		   for(EmployeeDetails employeeDetails : employeeDetailsList){
			   List<EmployeeCommissionDetailsModel> employeeCommissionDetailsModelList=new ArrayList<>();
			   EmployeeCommission employeeCommission=fetchEmployeeCommissionByMonthAndYearAndEmployeeDetailsId(monthId, yearId, employeeDetails.getEmployeeDetailsId());
			   if(employeeCommission==null){
				   
				   TargetAssign targetAssignList=targetAssignDAO.fetchTargetAssignByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
				   if(targetAssignList!=null){
					   List<TargetAssignTargets> targetAssignTargetList=targetAssignDAO.fetchTargetAssignTargetsByTargetAssignId(targetAssignList.getTargetAssignId());
					 
					   for(TargetAssignTargets  targetAssignTargets :  targetAssignTargetList){
						   employeeCommissionDetailsModelList.add(new EmployeeCommissionDetailsModel(
								   targetAssignTargets.getCommissionAssign().getCommissionAssignName(), 
								   targetAssignTargets.getCommissionAssign().getAmountOrQtyTypeSlab(), 
								   0, 
								   0,
								   0));
					   }
				   }
				   employeeCommissionModelList.add(new EmployeeCommissionModel(
						   srno, 
						   employeeDetails.getName(), 
						   employeeDetails.getEmployee().getDepartment().getName(),
						   employeeCommissionDetailsModelList, 
						   0));
			   }else{
				   
				   List<EmployeeCommissionDetails> employeeCommissionDetailsList=fetchEmployeeCommissionDetails(employeeCommission.getEmployeeCommissionId());
				   /*
				   for(EmployeeCommissionDetails employeeCommissionDetails :  employeeCommissionDetailsList){
					   employeeCommissionDetailsModelList.add(new EmployeeCommissionDetailsModel(
							   employeeCommissionDetails.getCommissionAssign().getCommissionAssignName(), 
							   employeeCommissionDetails.getCommissionAssign().getAmountOrQtyTypeSlab(), 
							   employeeCommissionDetails.getCompletedValue(), 
							   employeeCommissionDetails.getCommissionValue(),
							   employeeCommissionDetails.getCommissionAssign().getCommissionAssignId()));
				   }*/
				   
				   TargetAssign targetAssignList=targetAssignDAO.fetchTargetAssignByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
				   if(targetAssignList!=null){
					   List<TargetAssignTargets> targetAssignTargetList=targetAssignDAO.fetchTargetAssignTargetsByTargetAssignId(targetAssignList.getTargetAssignId());
					 
					   for(TargetAssignTargets  targetAssignTargets :  targetAssignTargetList){
						   
						   boolean isAlreadyAdded=true;
						   for(EmployeeCommissionDetails employeeCommissionDetails : employeeCommissionDetailsList){
							   if(targetAssignTargets.getCommissionAssign().getCommissionAssignId()==employeeCommissionDetails.getCommissionAssign().getCommissionAssignId()){
								   employeeCommissionDetailsModelList.add(new EmployeeCommissionDetailsModel(
										   employeeCommissionDetails.getCommissionAssign().getCommissionAssignName(), 
										   employeeCommissionDetails.getCommissionAssign().getAmountOrQtyTypeSlab(), 
										   employeeCommissionDetails.getCompletedValue(), 
										   employeeCommissionDetails.getCommissionValue(),
										   employeeCommissionDetails.getCommissionAssign().getCommissionAssignId()));
								   isAlreadyAdded=false;
								   break;
							   }
						   }
						   if(isAlreadyAdded){
							   employeeCommissionDetailsModelList.add(new EmployeeCommissionDetailsModel(
									   targetAssignTargets.getCommissionAssign().getCommissionAssignName(), 
									   targetAssignTargets.getCommissionAssign().getAmountOrQtyTypeSlab(), 
									   0, 
									   0,
									   0));
						   }
					   }
				   }
				   
				   employeeCommissionModelList.add(new EmployeeCommissionModel(
						   srno, 
						   employeeDetails.getName(), 
						   employeeDetails.getEmployee().getDepartment().getName(),
						   employeeCommissionDetailsModelList, 
						   employeeCommission.getTotalAmount()));
			   }
			   srno++;
		   }
		}
		if(employeeCommissionModelList.isEmpty()){
			return null;
		}
		return employeeCommissionModelList;
	}
	
	
}
