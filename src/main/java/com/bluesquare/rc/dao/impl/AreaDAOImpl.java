package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;
/***
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * Provides Implementation for following methods of AreaDAO
 * 1.saveForWebApp(Area area)
 * 2.updateForWebApp(Area area)
 * 3.fetchAllAreaForWebApp()
 * 4.fetchAreaListByRegionIdForWebApp(long regionId)
 * 5.checkAreaDuplication(long pincode,long areaId)
 * 6.fetchAreaForWebApp(long areaId)
 * 7.fetchAreaListByEmployeeId(long employeeId)
 * 8.modifyQueryForSelectedAccess(String hql)
 * 10.modifyQueryForSelectedAccessForOtherEntities(String hql)
 * 11.getSessionAreaIdsForOtherEntities()
 * 12.List<Object[]> fetchLocationIdsForWebApp()
 * </pre>
 */

@Repository("areaDAO")
@Component
public class AreaDAOImpl extends TokenHandler implements AreaDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	public AreaDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public AreaDAOImpl() {
	}

	
	/**
	 * <pre>
	 * company set in area
	 * </pre> 
	 * save area 
	 * @param area
	 * @return
	 */
	@Transactional
	public void saveForWebApp(Area area) {
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		area.setCompany(company);
		sessionFactory.getCurrentSession().save(area);
		
	}
	/**
	 * <pre>
	 * update area
	 * </pre>
	 * @param area
	 * @return
	 */
	@Transactional
	public void updateForWebApp(Area area) {
		area=(Area)sessionFactory.getCurrentSession().merge(area);
		sessionFactory.getCurrentSession().update(area);		
	}
	/**
	 * <pre>
	 * fetch all area list by company and 
	 * if app login then logged user areas only
	 * else panel login then all company area access
	 * </pre>
	 * @param 
	 * @return area list
	 */
	@Transactional
	public List<Area> fetchAllAreaForWebApp() {
		String hql = "from Area where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList;
	}
	/**
	 * <pre>
	 * area list by region id
	 * if app login then logged user areas only
	 * else panel login then all company areas access
	 * </pre>
	 * @param regionId
	 * @return
	 */
	@Transactional
	public List<Area> fetchAreaListByRegionIdForWebApp(long regionId) {
		String hql = "from Area where region=" + regionId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList;
	}
	/**
	 * <pre>
	 * check area pincode duplication
	 * in logged user companyId 
	 * </pre>
	 * @param pincode
	 * @param areaId
	 * @return true/false
	 */
	@Transactional
	public boolean checkAreaDuplication(long pincode,long areaId) {
		String hql = "from Area where pincode=" + pincode;
		
		if(areaId!=0){
			hql+=" and areaId<>"+areaId;
		}
		
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return false;
		}
		return true;
	}
	/**
	 * <pre>
	 * fetch area by areaId in logged user company
	 * </pre>
	 * @param areaId
	 * @return area
	 */
	@Transactional
	public Area fetchAreaForWebApp(long areaId) {
		String hql = "from Area where areaId=" + areaId;
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList.get(0);
	}

	/**
	 * <pre>
	 * fetch areaList in string by employeeId
	 * </pre>
	 * @param employeeId
	 * @return areaList
	 */
	@Transactional
	public String fetchAreaListByEmployeeId(long employeeId) {
		String hql="";
		Query query;
		hql="from EmployeeAreaList where employeeDetails.employee.employeeId="+employeeId;		
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeAreaList> employeeAreaList=(List<EmployeeAreaList>)query.list();
		
		List<Area> areaList=new ArrayList<>();
		for(EmployeeAreaList employeeAreaList1:employeeAreaList)
		{
			areaList.add(employeeAreaList1.getArea());
		}
		String areaConcation="";
		for(Area areaList1:areaList)
		{
			areaConcation+=areaList1.getName()+",";
		}
		
		areaConcation=areaConcation.substring(0, areaConcation.length() - 1);
		return areaConcation;
	}

	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long areaId : allAccess.getAreaIdList())
		{
			ids+=areaId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and areaId in ("+ids+")";
		
		return hql;
	}*/
	/**
	 * <pre>
	 * modify query and add areaId condition
	 * if app login then token decode and add area condition
	 * else nothing add in query
	 * </pre>
	 * @param hql
	 * @return String hql edited
	 */
	@Transactional
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and areaId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	/**
	 * <pre>
	 * modify query and add areaId condition
	 * if app login then token decode and add area condition
	 * else nothing add in query
	 * </pre>
	 * @param hql
	 * @return String hql
	 */
	@Transactional
	public String modifyQueryForSelectedAccessForOtherEntities(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and area.areaId in ("+ids+") ";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	/**
	 * <pre>
	 * modify query and add areaId condition
	 * if app login then token decode and add area condition
	 * else taken area id list from session and make area condition
	 * </pre>
	 * @param
	 * @return String -> area join with comma
	 */
	@Transactional
	public String getSessionAreaIdsForOtherEntities() {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList()){
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
					DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
					
					@SuppressWarnings("unchecked")
					Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
					Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
					
					String ids="";
					for(Long areaId : areaIdList){
						ids+=areaId+",";
					}
					ids=ids.substring(0, ids.length()-1);
					
					return ids;
				} catch (JWTDecodeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}
	}
	/**
	 * <pre>
	 * fetch areaId,regionId,cityId,stateId,countryId list by company and 
	 * if app login then logged user areas only
	 * else panel login then all company area access
	 * </pre>
	 * @param 
	 * @return area list
	 */
	@Transactional
	public List<Object[]> fetchLocationIdsForWebApp() {
		String hql = "select areaId,region.regionId,region.city.cityId,region.city.state.stateId,region.city.state.country.countryId from Area where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Object[]> locationIdsList = (List<Object[]>) query.list();
		if (locationIdsList.isEmpty()) {
			return null;
		}
		return locationIdsList;
	}
}
