package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.PaymentDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.OrderStatus;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.models.BusinessBadDebts;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReport;
import com.bluesquare.rc.models.CustomerReportView;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BusinessNameAndId;
import com.bluesquare.rc.responseEntities.BusinessNameModel;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;
import com.bluesquare.rc.responseEntities.CityModel;
import com.bluesquare.rc.responseEntities.RegionModel;
import com.bluesquare.rc.responseEntities.StateModel;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.utils.BusinessNameIdGenerator;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.ReturnFromDeliveryBoyGenerator;
import com.bluesquare.rc.utils.SendSMS;

/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * provides Implementation for following methods of BusinessNameDAO
 * 1.saveBusinessName(BusinessName businessName)
 * 2.saveForWebApp(BusinessName businessName)
 * 3.updateForWebApp(BusinessName businessName)
 * 4.getBusinessNameListForWebApp()
 * 5.fetchBusinessForWebApp(String businessNameId)
 * 6.businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId)
 * 7.fetchBusinessNameList()
 * 8.sendSMSTOShops(String shopsId,String smsText,String mobileNumber)
 * 9.fetchBusinessNameByAreaId(long areaId)
 * 10.fetchBusinessNameByAreaIds(String areaIds)
 * 11.fetchBusinessNameForReport(String range,String startDate,String endDate)
 * 12.checkBusinessDuplication(String checkText,String type,String businessNameId)
 * 13.fetchBusinessNameListByEmployeeId(long)
 * </pre>
 */
@Repository("businessNameDAO")

@Component
public class BusinessNameDAOImpl extends TokenHandler implements BusinessNameDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	BusinessName businessName;
	
	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	BusinessNameIdGenerator businessNameIdGenerator;
	
	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;

	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	PaymentDAO paymentDAO;
	
	@Autowired
	ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator;
	
	/**
	 * <pre>
	 * set logged user company in businessName
	 * set inter/intra 
	 * set generated id
	 * save contact
	 * save business
	 * send sms
	 * 
	 * @param businessName
	 * @return
	 * </pre>
	 */
	@Transactional
	public void saveBusinessName(BusinessName businessName) {
		// TODO Auto-generated method stub
		
		Employee employeeSM=new Employee();
		employeeSM.setEmployeeId(getAppLoggedEmployeeId());
		businessName.setEmployeeSM(employeeSM);
		
		company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessName.setCompany(company);
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.save(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.save(businessName.getContact());
		businessName.setBusinessNameId(businessNameIdGenerator.generateBusinessNameId());
		businessName.setBusinessAddedDatetime(new Date());
		businessName.setBusinessUpdatedDatetime(new Date());
		businessName.setStatus(false);
		businessName.setDisableDatetime(new Date());
		
		sessionFactory.getCurrentSession().save(businessName);
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+company.getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId();
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * <pre>
	 * set logged user company in businessName
	 * set inter/intra 
	 * set generated id
	 * save contact
	 * save business
	 * send sms
	 * 
	 * @param businessName
	 * @return
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
				
		company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessName.setCompany(company);
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.save(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.save(businessName.getContact());
		
		businessName.setBusinessNameId(businessNameIdGenerator.generateBusinessNameId());
		
		sessionFactory.getCurrentSession().save(businessName);
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+company.getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId();
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * <pre>
	 * set inter/intra 
	 * set generated id
	 * save contact
	 * save business
	 * send sms
	 * 
	 * @param businessName
	 * @return
	 * </pre>
	 */
	@Transactional
	public void updateForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.update(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.update(businessName.getContact());
		
		businessName=(BusinessName)sessionFactory.getCurrentSession().merge(businessName);
		sessionFactory.getCurrentSession().update(businessName);
		
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+businessName.getCompany().getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId()+", Your Account Edited SuccessFully ";
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * <pre>
	 * fetch businessName list which come under 
	 * logged user areas and company
	 * @param
	 * @return businessName list
	 * </pre>
	 */
	@Transactional
	public List<BusinessName> getBusinessNameListForWebApp() {
		String hql = "from BusinessName where 1=1 ";
		hql=areaDAO.modifyQueryForSelectedAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);		
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();

		getBusinessNameAndIdList();
		
		if (businessNameList.isEmpty()) {
			return null;
		}
		return businessNameList;
	}
	/**
	 * <pre>
	 * fetch businessName list which come under 
	 * logged user areas and company
	 * @param
	 * @return businessName list
	 * </pre>
	 */
	@Transactional
	public List<BusinessName> getBusinessNameListForApp() {
		String hql = "from BusinessName where 1=1 and status=false and badDebtsStatus=false  ";
		hql=areaDAO.modifyQueryForSelectedAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);		
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();

		if (businessNameList.isEmpty()) {
			return null;
		}
		return businessNameList;
	}
	/**
	 * <pre>
	 * fetch business name and id list
	 * @return {@link BusinessNameAndId}
	 * </pre>
	 */
	@Transactional
	public List<BusinessNameAndId> getBusinessNameAndIdList() {
		String hql = "from BusinessName where 1=1 ";
		hql+=" and area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+") ";
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+") ";	
		hql+=" and branch.branchId in ("+getSessionSelectedBranchIds()+") ";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();

		if (businessNameList.isEmpty()) {
			return null;
		}
		List<BusinessNameAndId> businessNameAndIds=new ArrayList<>();
		for(BusinessName  businessName : businessNameList){
			businessNameAndIds.add(new BusinessNameAndId(businessName.getBusinessNameId(), businessName.getShopName()));
		}
		return businessNameAndIds;
	}
	/**
	 * <pre>
	 * fetch business details by business name id 
	 * also condition with logged user areas and company
	 * @param businessNameId
	 * @return businessName
	 * </pre>
	 */
	@Transactional
	public BusinessName fetchBusinessForWebApp(String businessNameId) {
		String hql = "from BusinessName where businessNameId='" + businessNameId+"'";
		hql=areaDAO.modifyQueryForSelectedAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();
		if (businessNameList.isEmpty()) {
			return null;
		}
		return businessNameList.get(0);
	}
	/**
	 * <pre>
	 * fetch business list with businessTypeId and areaId 
	 * @param businessTypeId
	 * @param areaId 
	 * @return businessName list
	 * </pre>
	 */
	@Transactional
    public List<BusinessName> businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId) {

        Query query = null;
        String hql;
        
        if (areaId == 0 && businessTypeId == 0) {
            return null;
        } else if (businessTypeId == 0) {

            hql = "from BusinessName where area.areaId=" + areaId;

        } else if (areaId == 0) {
            hql = "from BusinessName where businessType.businessTypeId=" + businessTypeId;

        } else {
            hql = "from BusinessName where businessType.businessTypeId=" + businessTypeId + " and  area.areaId="+ areaId;
        }

        hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
        hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
        
        query = sessionFactory.getCurrentSession().createQuery(hql);
        List<BusinessName> businessNameList = (List<BusinessName>) query.list();
        if (businessNameList.isEmpty()) {
            return null;

        }
        return businessNameList;

    }
	/**
	 * <pre>
	 * fetch businessName list which come under 
	 * logged user areas and company
	 * calculate pending amount
	 * @param
	 * @return BusinessNameList
	 * </pre>
	 */
	@Transactional
	public List<BusinessNameList> fetchBusinessNameList() {
		List<BusinessNameList> businessNameLists=new ArrayList<>();
		String hql = "from BusinessName where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<BusinessName> businessNameList = (List<BusinessName>) query.list();
        
        if (businessNameList.isEmpty()) {
            return null;
        }
        double duePayment=0;
        for(BusinessName businessName : businessNameList)
        {
        	duePayment=0;
        	
        	if(businessName.getBadDebtsStatus()){
        		duePayment=businessName.getAmountLoss();
        	}else{
        	
	        	String hql2="from OrderDetails where payStatus=false and businessName.businessNameId='"+businessName.getBusinessNameId()+"' and "+
	        			" orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"
	        			+ " and businessName.company.companyId="+getSessionSelectedCompaniesIds()
	        			+ " and businessName.branch.branchId in ("+getSessionSelectedBranchIdsInQuery()+")";
	        	Query query2 = sessionFactory.getCurrentSession().createQuery(hql2);
	        	List<OrderDetails> orderDetailsList=(List<OrderDetails>)query2.list();
	        	for(OrderDetails orderDetails : orderDetailsList)
	        	{
	        		if(orderDetails.isPayStatus()==false)
	        		{
	        			String hql3="from Payment where status=false and orderDetails.orderId='"+orderDetails.getOrderId()+"'"
	        					+ " and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds()
	        					+ " and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIdsInQuery()+")";
	                	Query query3 = sessionFactory.getCurrentSession().createQuery(hql3);
	                	List<Payment> paymentList=(List<Payment>)query3.list();
	                	if(paymentList.isEmpty())
	                	{
	                		duePayment=+duePayment+orderDetails.getIssuedTotalAmountWithTax();
	                	}
	                	else
	                	{
	                		duePayment=+duePayment+paymentList.get(0).getDueAmount();
	                	}
	                	
	        		}
	        	}
	        	
	        	List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByBusinessNameIdForView(businessName.getBusinessNameId());
	        	if(counterOrderList!=null){
	        		double totalPaid=0;
	        		for(CounterOrder counterOrder : counterOrderList){
	        			List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
	        			if(paymentCounterList==null){
	        				totalPaid=0;
	        			}else{
		        			for(PaymentCounter paymentCounter2: paymentCounterList){
								totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
							}
	        			}
	        			duePayment+=counterOrder.getTotalAmountWithTax()-totalPaid;
	        		}
	        	}    		
	    	}
        	
        	businessNameLists.add(new BusinessNameList(new BusinessNameModel(
					businessName.getBusinessNamePkId(), 
					businessName.getBusinessNameId(), 
					businessName.getShopName(), 
					businessName.getOwnerName(), 
					businessName.getCreditLimit(), 
					businessName.getGstinNumber(), 
					new BusinessTypeModel(businessName.getBusinessType().getBusinessTypeId(), businessName.getBusinessType().getName()), 
					businessName.getAddress(), 
					businessName.getTaxType(), 
					businessName.getContact(), 
					new AreaModel(
							businessName.getArea().getAreaId(), 
							businessName.getArea().getName(), 
							businessName.getArea().getPincode(),
							new RegionModel(
									businessName.getArea().getRegion().getRegionId(), 
									businessName.getArea().getRegion().getName(), 
									new CityModel(
											businessName.getArea().getRegion().getCity().getCityId(), 
											businessName.getArea().getRegion().getCity().getName()))), 
					businessName.getEmployeeSM()==null?null:
					new EmployeeNameAndId(businessName.getEmployeeSM().getEmployeeId(), employeeDetailsDAO.getEmployeeDetailsByemployeeId(businessName.getEmployeeSM().getEmployeeId()).getName()), 
					businessName.getStatus(),
					businessName.getBadDebtsStatus()), duePayment));
        }
        
		return businessNameLists;
	}
	
	/**
	 * <pre>
	 * send sms to business according given shop Ids
	 * if shop id is one then send sms on given number 
	 * otherwise register number
	 * @param shopsId'
	 * @param smsText
	 * @param mobileNumber
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String sendSMSTOShops(String shopsId,String smsText,String mobileNumber) {

		try {
			String endText=getEndTextForSMS();			
				
			String[] shopsId2 = shopsId.split(",");
			
			if(shopsId2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+shopsId2[0]);
				return "Success";
			}
			
			for(int i=0; i<shopsId2.length; i++)
			{
				businessName=fetchBusinessForWebApp(shopsId2[i]);
				SendSMS.sendSMS(Long.parseLong(businessName.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+shopsId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	/**
	 * <pre>
	 * fetch businessName list by areaId
	 * @param areaId
	 * @return BusinessName list
	 * </pre>
	 */
	@Transactional
	public List<BusinessName> fetchBusinessNameByAreaId(long areaId) {
		
		String hql="from BusinessName where area.areaId="+areaId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		if(businessNameList.isEmpty()){
			return null;
		}
		
		return businessNameList;
	}
	/**
	 * <pre>
	 * fetch businessName by areaId
	 * @param areaIds
	 * @return businessName list
	 * </pre>
	 */
	@Transactional
	public List<BusinessName> fetchBusinessNameByAreaIds(String areaIds) {
		
		String hql="from BusinessName where area.areaId in ("+areaIds+") ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		if(businessNameList.isEmpty()){
			return null;
		}
		
		return businessNameList;
	}
	/**
	 * <pre>
	 * fetch business list by range, start date,end date
	 * find total amount of business did in salesman order and counter order
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return {@link CustomerReportView}
	 * </pre>
	 */
	@Transactional
	public CustomerReportView fetchBusinessNameForReport(String range,String startDate,String endDate){
		
		List<CustomerReport> customerReportList=new ArrayList<>();
		
		String hql="from BusinessName where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		long srno=1;
		double totalAmountAllBusiness=0;
		for(BusinessName businessName : businessNameList){
			double totalAmount=0;
			long noOfOrders=0;
			//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
			List<OrderReportList> orderReportList=orderDetailsDAO.showOrderReportByBusinessNameId(businessName.getBusinessNameId(), range, startDate, endDate);
			List<OrderReportList> counterOrderReportList=counterOrderDAO.showCounterOrderReportByBusinessNameId(businessName.getBusinessNameId(), range, startDate, endDate);
			
			//counterOrders add in salesman orders
			if(orderReportList!=null && counterOrderReportList!=null){
				orderReportList.addAll(counterOrderReportList);
			}else if(orderReportList==null && counterOrderReportList!=null){
				orderReportList=new ArrayList<>();
				orderReportList.addAll(counterOrderReportList);
			}
			
			
			if(orderReportList!=null)
			{
				for(OrderReportList orderReport:orderReportList)
				{
					totalAmount+=orderReport.getAmountWithTax();
				}
				noOfOrders=orderReportList.size();
			}
			customerReportList.add(new CustomerReport(srno, 
													businessName.getBusinessNameId(), 
													businessName.getShopName(), 
													totalAmount, 
													businessName.getArea().getName(), 
													businessName.getArea().getRegion().getName(), 
													businessName.getArea().getRegion().getCity().getName(), 
													businessName.getArea().getRegion().getCity().getState().getName(), 
													businessName.getArea().getRegion().getCity().getState().getCountry().getName(), 
													businessName.getBusinessAddedDatetime(),
													noOfOrders));
			srno++;
			totalAmountAllBusiness+=totalAmount;
		}
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);		
		List<Area> areaList=areaDAO.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
		if(areaList!=null){
			for(Area area: areaList){
				areaModelList.add(new AreaModel(
						area.getAreaId(), 
						area.getName(), 
						area.getPincode(), 
						new RegionModel(
								area.getRegion().getRegionId(), 
								area.getRegion().getName(), 
								new CityModel(
										area.getRegion().getCity().getCityId(), 
										area.getRegion().getCity().getName(), 
										new StateModel(
												area.getRegion().getCity().getState().getStateId(), 
												area.getRegion().getCity().getState().getCode(), 
												area.getRegion().getCity().getState().getName(), 
												area.getRegion().getCity().getState().getCountry())))));
			}
		}
		
		return new CustomerReportView(totalAmountAllBusiness, customerReportList,areaModelList );
	}
	/**
	 * <pre>
	 * check unique or not at time adding / updating business in 
	 * telephoneNumber,gstNo,mobileNumber,emailId
	 * @param checkText
	 * @param type
	 * @param businessNameId
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String checkBusinessDuplication(String checkText,String type,String businessNameId){
		
		/*String hql="";
		if(type.equals("telephoneNumber")){
			hql="from BusinessName where contact.telephoneNumber='"+checkText+"'";
		}else if(type.equals("gstNo")){
			hql="from BusinessName where gstinNumber='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from BusinessName where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from BusinessName where contact.emailId='"+checkText+"'";
		}		
		if(!businessNameId.equals("0")){
			hql+=" and businessNameId<>'"+businessNameId+"'";
		}
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		//hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> supplierList=(List<BusinessName>)query.list();
		if(supplierList.isEmpty()){*/
			return "Success";
		/*}
		return "Failed";*/		
	}


	@Transactional
	public List<BusinessName> fetchBusinessNameListByEmployeeId(long employeeId) {
		
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
				
		List<Long> areaIds = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        areaIds.add(area.getAreaId());
	    }
	    
	    String hql="from BusinessName where area.areaId in (:areaIds)";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
	    Query query=sessionFactory.getCurrentSession().createQuery(hql);
	    query.setParameterList("areaIds", areaIds);
		
	    List<BusinessName> businessNameList=(List<BusinessName>)query.list();
	    if(businessNameList.isEmpty()){
	    	return null;
	    }
	    
	    return businessNameList;
	}
	/**
	 * <pre>
	 * find number of business added in range and by employee
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param employeeId
	 * @return no of business added
	 */
	@Transactional
	public int findNumberOfBusinessAddedByDateRange(String startDate,String endDate,long employeeId){
		String hql="from BusinessName where employeeSM.employeeId=" +employeeId + "  And (date(businessAddedDatetime)>='"+startDate+"'  "
				+ "And date(businessAddedDatetime)<='"+endDate+"')";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		//hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		hql+= " order by businessAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		
		if(businessNameList.isEmpty()){
			return 0;
		}else{
			return businessNameList.size();
		}
	}
	
	@Transactional
	public void setBusinessAsBadDebts(String businessNameId){
		double totalAmountDue=0;
		BusinessName businessName=fetchBusinessForWebApp(businessNameId);
		businessName.setBadDebtsStatus(true);
		businessName.setStatus(true);
		
		List<OrderDetails> orderDetailsList= orderDetailsDAO.fetchOrderDetailsBybusinessNameId(businessNameId);
		//cancel order process
		//get cancel order status details
		OrderStatus orderStatus=orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED);
		if(orderDetailsList!=null){
			for(OrderDetails orderDetails : orderDetailsList){
				
				List<Payment> paymentList=paymentDAO.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				
				
				// fetch order product details by orderId
				List<OrderProductDetails>  orderProductDetailList=orderDetailsDAO.fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				orderDetails.setOldOrderStatus(orderDetails.getOrderStatus().getStatus());
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED)){
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(orderDetails.getEmployeeSM());
					orderDetails.setCancelReason("Business Set As Bad Debts");
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetails.getIssuedQuantity());
						
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), true);
						
						
						//deleteOrderUsedProduct(orderProductDetails.getProduct());
					}	
				}else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED)){
					OrderProductIssueDetails orderProductIssueDetails=orderDetailsDAO.fetchOrderProductIssueDetailsByOrderId(orderDetails.getOrderId());
					
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(orderProductIssueDetails.getEmployeeGK());
					orderDetails.setCancelReason("Business Set As Bad Debts");
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetails.getIssuedQuantity());
						
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), true);						
						//deleteOrderUsedProduct(orderProductDetails.getProduct());
					}	
				}else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED)){
					OrderProductIssueDetails orderProductIssueDetails=orderDetailsDAO.fetchOrderProductIssueDetailsByOrderId(orderDetails.getOrderId());
					
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(orderProductIssueDetails.getEmployeeDB());
					orderDetails.setCancelReason("Business Set As Bad Debts");
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						
						ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
								orderProductDetails.getProduct(), 
								orderProductDetails.getSellingRate(), 
								orderProductDetails.getIssuedQuantity(), 
								orderProductDetails.getIssuedQuantity(),
								0,
								0,
								0, 
								orderProductDetails.getType());
						returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
					}
					
					long totalReturnQuantity=0;
					long totalIssuedQuantity=0;
					long totalDeliveryQuantity=0;
					long totalDamageQuantity=0;
					long totalNonDamageQuantity=0;

					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
						
						totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
						totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
						totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
						totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
						totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
					}
					Company company=new Company();
					company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					
					Branch branch=new Branch();
					branch.setBranchId(getSessionSelectedBranchIds());
					ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
							totalReturnQuantity, 
							totalIssuedQuantity, 
							totalDeliveryQuantity, 
							totalDamageQuantity, 
							totalNonDamageQuantity,  
							orderProductIssueDetails.getEmployeeGK(), 
							orderProductIssueDetails.getEmployeeDB(), 
							orderDetails, 
							new Date(), 
							false,
							company,
							branch);

					returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
					sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
					
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
						returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
						sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
					}					
					
				}
			
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING)){
				/*	ReturnOrderProduct returnOrderProduct= returnOrderDAO.fetchReturnOrderProductByReIssueStatus(orderDetails.getOrderId());
					if(returnOrderProduct.getReIssueStatus().equals(Constants.RETURN_ORDER_PENDING)){
						//set status to cancel
						ReIssueOrderDetails reIssueOrderDetails=orderDetailsDAO.fetchReIssueOrderDetails(returnOrderProduct.getReturnOrderProductId());
						reIssueOrderDetails.setStatus(Constants.RETURN_ORDER_CANCEL);
						reIssueOrderDetails=(ReIssueOrderDetails)sessionFactory.getCurrentSession().merge(reIssueOrderDetails);
						sessionFactory.getCurrentSession().update(reIssueOrderDetails);
					}else{
						//set status to cancel
						returnOrderProduct.setReIssueStatus(Constants.RETURN_ORDER_CANCEL);
						returnOrderProduct=(ReturnOrderProduct)sessionFactory.getCurrentSession().merge(returnOrderProduct);
						sessionFactory.getCurrentSession().update(returnOrderProduct);
					}*/
					if(paymentList!=null){
						totalAmountDue+=paymentList.get(0).getDueAmount();
					}else{
						totalAmountDue+=orderDetails.getIssuedTotalAmountWithTax();
					}
					orderDetails.setBadDebtsStatus(true);
				}else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED)){
					if(paymentList!=null){
						totalAmountDue+=paymentList.get(0).getDueAmount();
					}else{
						totalAmountDue+=orderDetails.getIssuedTotalAmountWithTax();
					}
					orderDetails.setBadDebtsStatus(true);
				}
				//here update order product current quantity
				orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
				
				orderDetailsDAO.updateOrderDetailsPaymentDays(orderDetails);
			}
		}
		
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByBusinessNameIdForView(businessNameId);
		
		if(counterOrderList!=null){
			for(CounterOrder counterOrder : counterOrderList){
				List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
				double totalPaid=0;
				if(paymentCounterList!=null){					
					for(PaymentCounter paymentCounter2: paymentCounterList){
						totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
					}					
				}
				totalAmountDue+=counterOrder.getTotalAmountWithTax()-totalPaid;
				counterOrder.setBadDebtsStatus(true);
				counterOrderDAO.updateCounterOrder(counterOrder);;
				counterOrderDAO.deleteCounterOrder(counterOrder.getCounterOrderId());
			}
		}
		
		businessName.setAmountLoss(totalAmountDue);
		businessName.setBadDebtsDatetime(new Date());
		businessName=(BusinessName)sessionFactory.getCurrentSession().merge(businessName);
		sessionFactory.getCurrentSession().update(businessName);
		
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Your Account Disabled by "+businessName.getCompany().getCompanyName();
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * <pre>
	 * fetch bad debts list
	 * </pre> 
	 * @return {@link BusinessBadDebts}
	 */
	@Transactional
	public List<BusinessBadDebts> fetchBusinessBadDebtsList(String year){
		
		List<BusinessBadDebts> businessBadDebtList=new ArrayList<>();
		
		String hql="from BusinessName where badDebtsStatus=true";
		hql+=" and area.areaId in ("+getSessionSelectedAccessAreaIds()+")"
		+" and YEAR(badDebtsDatetime)='"+year+"'";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();

		int srno=1;
		if(!businessNameList.isEmpty()){
		
			for(BusinessName businessName: businessNameList){
				int totalNumberOfOrder=0;
				List<OrderDetails> orderDetailsList= orderDetailsDAO.fetchOrderDetailsBybusinessNameIdForView(businessName.getBusinessNameId());
				
				if(orderDetailsList==null){
					totalNumberOfOrder=0;
				}else{
					totalNumberOfOrder=orderDetailsList.size();
				}
				
				List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByBusinessNameId(businessName.getBusinessNameId());
				if(counterOrderList==null){
				}else{
					totalNumberOfOrder+=counterOrderList.size();
				}
				
				businessBadDebtList.add(new BusinessBadDebts(
						srno, 
						businessName.getBusinessNameId(), 
						businessName.getShopName(), 
						businessName.getContact().getMobileNumber(), 
						businessName.getAmountLoss(), 
						new AreaModel(
								businessName.getArea().getAreaId(), 
								businessName.getArea().getName(), 
								businessName.getArea().getPincode(), 
								new RegionModel(
										businessName.getArea().getRegion().getRegionId(), 
										businessName.getArea().getRegion().getName(), 
										null)),
						totalNumberOfOrder,
						businessName.getStatus(),
						"NA",
						businessName.getBadDebtsDatetime()));
				srno++;
			}
		}
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByExternalCustomerForView(year);
		if(counterOrderList!=null){
			for(CounterOrder counterOrder : counterOrderList){

				businessBadDebtList.add(new BusinessBadDebts(
						srno, 
						"NA", 
						counterOrder.getCustomerName(), 
						counterOrder.getCustomerMobileNumber(), 
						counterOrder.getTotalAmountBadDebtsDue(), 
						null, 
						1,
						true,
						counterOrder.getCounterOrderId(),
						counterOrder.getBadDebtsDatetime()));
			}
		}
		if(businessBadDebtList.isEmpty()){
			return null;
		}
		return businessBadDebtList;
	}
}
