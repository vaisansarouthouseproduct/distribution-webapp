package com.bluesquare.rc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.RegionDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.DailyStockDetails;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.utils.AllAccess;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;
/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of RegionDAO
 * 1.saveForWebApp(Region region)
 * 2.updateForWebApp(Region region)
 * 3.fetchAllRegionForWebApp()
 * 4.fetchSpecifcRegionsForWebApp(long cityId)
 * 5.fetchRegionForWebApp(long regionId)
 * 6.modifyQueryForSelectedAccess(String hql)
 * </pre>
 */
@Repository("regionDAO")

@Component
public class RegionDAOImpl extends TokenHandler implements RegionDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	/**
	 * <pre>
	 * set logged user company 
	 * save region
	 * @param region
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(Region region) {
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		region.setCompany(company);
		sessionFactory.getCurrentSession().save(region);
		
	}

	@Transactional
	public void updateForWebApp(Region region) {
		// TODO Auto-generated method stub
		
		region=(Region)sessionFactory.getCurrentSession().merge(region);
		sessionFactory.getCurrentSession().update(region);
	}
 /**
  * <pre>
  * fetch region list which belongs to logged user company and its regions
  * @return Region list
  * </pre>
  */
	@Transactional
	public List<Region> fetchAllRegionForWebApp() {
		String hql = "from Region where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Region> regions = (List<Region>) query.list();
		if (regions.isEmpty()) {
			return null;
		}
		return regions;
	}
	/**
	  * <pre>
	  * fetch region list which belongs to logged user company , its regions, cityId
	  * @return Region list
	  * </pre>
	  */
	@Transactional
	public List<Region> fetchSpecifcRegionsForWebApp(long cityId) {
		String hql = "from Region where city=" + cityId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Region> regions = (List<Region>) query.list();
		if (regions.isEmpty()) {
			return null;
		} 
		return regions;
	}
	
	@Transactional
	public Region fetchRegionForWebApp(long regionId) {
		String hql = "from Region where regionId=" + regionId;
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Region> regionList = (List<Region>) query.list();
		if (regionList.isEmpty()) {
			return null;
		}

		return regionList.get(0);
	}

	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long regionId : allAccess.getRegionIdList())
		{
			ids+=regionId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and regionId in ("+ids+")";
		
		return hql;
	}*/
    /**
     * <pre>
     * modify query 
     * if token found then nothing change query otherwise take region list from token and joing region condition to query
     * @param hql
     * @return modified query
     * </pre>
     */
	@Transactional
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			hql+="";
			
			return hql;
			
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] regionIdList=decodedJWT.getClaim("regionIdList").asArray(longClass);
				
				String ids="";
				for(Long regionId : regionIdList)
				{
					ids+=regionId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and regionId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

}
