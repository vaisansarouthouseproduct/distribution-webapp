package com.bluesquare.rc.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.utils.CollectionConvertor;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;
/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of TokenHandlerDAO
 * 1.modifyQueryForSelectedCompaniesAccessForOtherEntities(String hql)
 * 2.getSessionSelectedCompaniesIds()
 * 3.getAppLoggedEmployeeId()
 * 4.getEndTextForSMS()
 * </pre>
 */
@Repository("tokenHandlerDAO")
@Component
public class TokenHandler implements TokenHandlerDAO{

	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	@Autowired
	CompanyDAO companyDAO;
	
	public String token="NA";
	/**
	 * <pre>
	 * modify query and add company condition
	 * if token found then decode token and then add company condition
	 * else take from session and make company condition 
	 * @param hql
	 * @return edited hql query
	 * </pre>
	 */
	public String modifyQueryForSelectedCompaniesAccessForOtherEntities(String hql){
		//get if request from app
		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> companyIds=(List<Long>)session.getAttribute("selectedCompanyIds");
			
			String ids="";
			if(companyIds.get(0)!=-1){
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
			}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}
			ids=ids.substring(0, ids.length()-1);
			
			hql+=" and company.companyId in ("+ids+") ";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] companyIds=decodedJWT.getClaim("jwtSelectedCompanyIds").asArray(longClass);
				
				String ids="";
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and company.companyId in ("+ids+") ";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
		}
	}
	/**
	 * <pre>
	 * modify query and add company condition
	 * if token found then decode token and then add company condition
	 * else take from session and make company condition 
	 * @param hql
	 * @return edited hql query
	 * </pre>
	 */
	public String getSessionSelectedCompaniesIds()
	{
		//get if request from app
   		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> companyIds=(List<Long>)session.getAttribute("selectedCompanyIds");
			
			String ids="";
			//if(companyIds.get(0)!=-1){
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
			/*}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}*/
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] companyIds=decodedJWT.getClaim("jwtSelectedCompanyIds").asArray(longClass);
				
				String ids="";
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				return ids;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
		}
	}

	/***
	 * App logged employee id get from token
	 */
	@Transactional
	public long getAppLoggedEmployeeId() {
		token=(String)session.getAttribute("authToken");
		if(token!=null){
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class longClass = (Class) Class.forName("java.lang.Long");
				Long employeeId=decodedJWT.getClaim("employeeId").asLong();
				
				return employeeId;
			} catch (Exception e) {
				e.printStackTrace();
				return 0;
			}
		}else{
			EmployeeDetails  employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails"); 
			if(employeeDetails==null){
				return 0;
			}else{
				return employeeDetails.getEmployee().getEmployeeId();
			}
			
		}
		
	}
	/**
	 * <pre>
	 * set sms end text according logged user
	 * @return edited hql query
	 * </pre>
	 */
	@Transactional
	public String getEndTextForSMS() {
		String endText="";
		
		String loginType=(String)session.getAttribute("loginType");
		if(loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
			endText=" \n --"+employeeDetails.getName()+"(GK-"+employeeDetails.getEmployee().getCompany().getCompanyName()+")";
			
		}else if(loginType.equals(Constants.ADMIN)){
			
			endText=" \n --Admin";
			
		}else if(loginType.equals(Constants.COMPANY_ADMIN)){
			
			Company company=(Company)session.getAttribute("companyDetails");
			endText=" \n --"+company.getCompanyName();
			
		}
		return endText;
	}

	/**
	 * <pre>
	 * create new token for new branch and sent to app
	 * @param token
	 * @param branchId
	 * @return string token
	 * </pre>
	 */
	@Transactional
	public String createNewToken(String token,long branchId) throws Exception{
		
		DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
		
		Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
		Long employeeId=decodedJWT.getClaim("employeeId").asLong();
		Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
		Long[] regionIdList=decodedJWT.getClaim("regionIdList").asArray(longClass);
		Long[] cityIdList=decodedJWT.getClaim("cityIdList").asArray(longClass); 
		Long[] stateIdList=decodedJWT.getClaim("stateIdList").asArray(longClass);
		Long[] countryIdList=decodedJWT.getClaim("countryIdList").asArray(longClass);		 
		
		Map<String,List<Long>> claimMap=new HashMap<>();
		claimMap.put("areaIdList", CollectionConvertor.arrayToList(areaIdList));
		claimMap.put("regionIdList", CollectionConvertor.arrayToList(regionIdList));
		claimMap.put("cityIdList", CollectionConvertor.arrayToList(cityIdList));
		claimMap.put("stateIdList", CollectionConvertor.arrayToList(stateIdList));
		claimMap.put("countryIdList", CollectionConvertor.arrayToList(countryIdList));
		
		Long[] companyIds=decodedJWT.getClaim("jwtSelectedCompanyIds").asArray(longClass);
		Long[] branchIds=new Long[]{branchId};//decodedJWT.getClaim("jwtSelectedBranchIds").asArray(longClass);		
		
		JsonWebToken jsonWebToken=new JsonWebToken();
		token = jsonWebToken.create(claimMap, companyIds, branchIds, Constants.ISSUER, employeeId);
		
		return token;
	}
	/**
	 * <pre>
	 * get session selected branch ids in query
	 * </pre>
	 * 
	 * @return query with branch id
	 */
	public String getSessionSelectedBranchIdsInQuery()
	{
		//get if request from app
   		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> branchIds=(List<Long>)session.getAttribute("selectedBranchIds");
			
			String ids="";
			//if(companyIds.get(0)!=-1){
				for(Long branchId : branchIds)
				{
					ids+=branchId+",";
				}
			/*}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}*/
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] branchIds=decodedJWT.getClaim("jwtSelectedBranchIds").asArray(longClass);
				
				String ids="";
				for(Long branchId : branchIds)
				{
					ids+=branchId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				return ids;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	
	/**
	 * <pre>
	 * get session selected branch ids in query
	 * </pre>
	 * 
	 * @return query with branch id
	 */
	public String modifyQueryAccordingSessionSelectedBranchIds(String hql)
	{
		//get if request from app
   		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> branchIds=(List<Long>)session.getAttribute("selectedBranchIds");
			
			//String ids="";
			//if(companyIds.get(0)!=-1){
				/*for(Long branchId : branchIds)
				{
					ids+=branchId+",";
				}*/
			/*}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}*/
			//ids=ids.substring(0, ids.length()-1);
			
			return hql+=" and branch.branchId in ("+branchIds.get(0)+") ";
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] branchIds=decodedJWT.getClaim("jwtSelectedBranchIds").asArray(longClass);
				
				/*String ids="";
				for(Long branchId : branchIds)
				{
					ids+=branchId+",";
				}
				ids=ids.substring(0, ids.length()-1);*/
				
				return hql+=" and branch.branchId in ("+branchIds[0]+") ";
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
		}
	}
	
	public long getSessionSelectedBranchIds()
	{
		//get if request from app
   		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> branchIds=(List<Long>)session.getAttribute("selectedBranchIds");
			
			return branchIds.get(0);
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] branchIds=decodedJWT.getClaim("jwtSelectedBranchIds").asArray(longClass);
				
				return branchIds[0];
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public String getSessionSelectedAccessAreaIds()
	{
		SelectedAccess SelectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			String areaIds="";
			for(long areaId : SelectedAccess.getAreaIdList())
			{
				areaIds+=areaId+",";
			}
			areaIds=areaIds.substring(0, areaIds.length()-1);
			return areaIds;
		
	}
	/*public String modifyQueryForSelectedAccessForOtherEntities(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList())
			{
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			hql+=" and area.areaId in ("+ids+")";

			return "";
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and area.areaId in ("+ids+")";
				
				return "";
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	public String getSessionSelectedIds()
	{
		token=(String)session.getAttribute("authToken");
		if(token==null){
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList())
			{
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				Class<Long> longClass = (Class<Long>) Class.forName("Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				return ids;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}*/
}
