package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BusinessTypeDAO;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;

/***
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * provides Implementation for following methods of BusinessTypeDAO
 * 1.saveForWebApp(BusinessType businessType)
 * 2.updateForWebApp(BusinessType businessType)
 * 3.fetchBusinessTypeListForWebApp()
 * 4.fetchBusinessTypeForWebApp(long businessTypeId)
 * </pre>
 */
@Repository("businessTypeDAO")

@Component
public class BusinessTypeDAOImpl extends TokenHandler implements BusinessTypeDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	public BusinessTypeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public BusinessTypeDAOImpl() {
	}
	/**
	 * <pre>
	 * set logged user company in businessType
	 * save businessType
	 * @param businessType
	 * @return
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessType.setCompany(company);
		
		sessionFactory.getCurrentSession().save(businessType);
	}
	/**
	 * <pre>
	 * update businessType
	 * @param businessType
	 * </pre>
	 */
	@Transactional
	public void updateForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		businessType=(BusinessType)sessionFactory.getCurrentSession().merge(businessType);
		sessionFactory.getCurrentSession().update(businessType);
	}
	/**
	 * <pre>
	 * fetch businessType list which mapped with logged user company
	 * @return businessType list
	 * </pre>
	 */
	@Transactional
	public List<BusinessType> fetchBusinessTypeListForWebApp() {
		String hql = "from BusinessType where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessType> businessTypeList = (List<BusinessType>) query.list();

		if (businessTypeList.isEmpty()) {
			return null;
		}
		return businessTypeList;
	}
	/**
	 * <pre>
	 * fetch businessType list which mapped with logged user company
	 * @return businessTypeModel list
	 * </pre>
	 */
	@Transactional
	public List<BusinessTypeModel> fetchBusinessTypeModelList() {
		String hql = "from BusinessType where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessType> businessTypeList = (List<BusinessType>) query.list();

		if (businessTypeList.isEmpty()) {
			return null;
		}
		List<BusinessTypeModel> businessTypeModelList=new ArrayList<BusinessTypeModel>();
		for(BusinessType businessType : businessTypeList){
			businessTypeModelList.add(new BusinessTypeModel(businessType.getBusinessTypeId(), businessType.getName()));
		}
		return businessTypeModelList;
	}
	/**
	 * <pre>
	 * fetch business type by business typeId and check its logged users company mapped or not
	 * @param businessTypeId
	 * @return BusinessType
	 * </pre>
	 */
	@Transactional
	public BusinessType fetchBusinessTypeForWebApp(long businessTypeId) {
		String hql = "from BusinessType where businessTypeId=" + businessTypeId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessType> businessTypeList = (List<BusinessType>) query.list();
		if (businessTypeList.isEmpty()) {
			return null;
		}

		return businessTypeList.get(0);
	}

}
