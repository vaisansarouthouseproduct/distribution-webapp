package com.bluesquare.rc.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Contact;
/**
 * <pre>
 * @author Sachin Pawar 23-05-2018 Code Documentation
 * provides Implementation for following methods of ContactDAO
 * 1.save(Contact contact)
 * 2.update(Contact contact)
 * 3.delete(long id)
 * </pre>
 */
@Repository("contactDAO")


@Component
public class ContactDAOImpl extends TokenHandler implements ContactDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public ContactDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public ContactDAOImpl() { }
	/**
	 * <pre>
	 * save contact details (mobile number,telephone number, emailId)
	 * @param contact
	 * </pre>
	 */
	@Transactional
	public void save(Contact contact) {
		sessionFactory.getCurrentSession().save(contact);
	}
	/**
	 * <pre>
	 * update contact details (mobile number,telephone number, emailId)
	 * @param contact
	 * </pre>
	 */
	@Transactional
	public void update(Contact contact) {
		contact=(Contact)sessionFactory.getCurrentSession().merge(contact);
		sessionFactory.getCurrentSession().update(contact);
	}
	/**
	 * <pre>
	 * delete contact 
	 * @param id
	 * </pre>
	 */
	//not using
	@Transactional
	public void delete(long id) {
		Contact ContactToDelete = new Contact();
		ContactToDelete.setContactId(id);
		ContactToDelete=(Contact)sessionFactory.getCurrentSession().merge(ContactToDelete);
		sessionFactory.getCurrentSession().delete(ContactToDelete);
	}

}
