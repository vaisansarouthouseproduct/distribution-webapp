package com.bluesquare.rc.dao.impl;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.InventoryDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.InventoryAddedInvoiceModel;
import com.bluesquare.rc.models.InventoryDetailsModel;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.models.InventoryRequest;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.rest.models.InventorySaveRequestModel;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.InventoryTransactionIdGenerator;
import com.bluesquare.rc.utils.MathUtils;
import com.bluesquare.rc.utils.NumberToWordsConverter;
import com.bluesquare.rc.utils.RoundOff;
/**
 * <pre>
 * @author Sachin Pawar 25-05-2018 Code Documentation
 * provides Implementation for following methods of InventoryDAO
 * 1.inventoryProductList()
 * 2.fetchSupplierListByProductId(long productId)
 * 3.addInventory(Inventory inventory,String productIdList)
 * 4.editInventory(Inventory inventory,String productIdList)
 * 5.fetchInventory(String inventoryId)
 * 6.fetchPaymentStatus(String inventoryTransactionId)
 * 7.fetchPaymentPaySupplierForEdit(String paymentPaySupplierId)
 * 8.givePayment(PaymentPaySupplier paymentPaySupplier)
 * 9.updateSupplierPayment(PaymentPaySupplier paymentPaySupplier)
 * 10.updatePaymentSupplierDueAmount(String inventoryTransactionId)
 * 11.fetchInventoryReportView(String supplierId,String startDate,String endDate, String range)
 * 12.fetchPaymentPaySupplierListByInventoryId(String inventoryId)
 * 13.fetchPaymentPaySupplierListByPaymentPaySupplierId(String paymentPaySupplierId)
 * 14.deletePaymentPaySupplierListByInventoryId(String paymentPaySupplierId)
 * 15.fetchTrasactionDetailsByInventoryId(String inventoryTransactionId)
 * 16.saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel)
 * 17.editInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel)
 * 18.fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,String toDate, String range)
 * 19.fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId)
 * 20.fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId)
 * 21.fetchTotalValueOfCurrrentInventory()
 * 22.fetchProductUnderThresholdCount()
 * 23.deleteInventory(String inventoryId)
 * 24.totalSupplierPaidAmountForProfitAndLoss(String startDate,String endDate)
 * </pre>
 */
@Repository("inventoryDAO")
@Component
public class InventoryDAOImpl extends TokenHandler implements InventoryDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	InventoryTransactionIdGenerator inventoryTransactionIdGenerator;
	
	@Autowired
	SupplierDAO supplierDAO;
	
	@Autowired
	BranchDAO branchDAO;
	
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");
	
	public InventoryDAOImpl(	SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	public InventoryDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * <pre>
	 * {@link CalculateProperTaxModel} used for calculate mrp and tax
	 * @return InventoryProduct list
	 * </pre>
	 */
	@Transactional
	public List<InventoryProduct> inventoryProductList() {

		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory); 
		
		List<InventoryProduct> inventoryProductsList=new ArrayList<>();
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		if(productList==null)
		{
			return null;
		}
		long srno=1;
		for(Product product : productList)
		{
			/*double igst=product.getCategories().getIgst();
			double rate=product.getRate();
			double rateWithTax= ((rate*igst)/100)+rate;
			rateWithTax=Double.parseDouble(new DecimalFormat("###").format(rateWithTax));
			double curQty=product.getCurrentQuantity();
			double taxableAmount=curQty*rate;
			double taxAmount=curQty*rateWithTax;
			double tax=	taxAmount-taxableAmount;*/
			
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			double taxableTotal = product.getCurrentQuantity() * calculateProperTaxModel.getUnitprice();
			double igstamount=product.getCurrentQuantity()*calculateProperTaxModel.getIgst();
			double cgstamount =product.getCurrentQuantity()*calculateProperTaxModel.getCgst();
			double sgstamount=product.getCurrentQuantity()*calculateProperTaxModel.getSgst();		
			double total=product.getCurrentQuantity()*calculateProperTaxModel.getMrp();
			
			inventoryProductsList.add(new InventoryProduct(
															srno,
															 new ProductModel(
																		product.getProductId(), 
																		product.getProductName(), 
																		product.getProductCode(), 
																		new CategoriesModel(
																				product.getCategories().getCategoryId(), 
																				product.getCategories().getCategoryName(), 
																				product.getCategories().getCategoryDescription(), 
																				product.getCategories().getHsnCode(), 
																				product.getCategories().getCgst(), 
																				product.getCategories().getSgst(), 
																				product.getCategories().getIgst(), 
																				product.getCategories().getCategoryDate(), 
																				product.getCategories().getCategoryUpdateDate()), 
																		new BrandModel(
																				product.getBrand().getBrandId(), 
																				product.getBrand().getName(), 
																				product.getBrand().getBrandAddedDatetime(), 
																				product.getBrand().getBrandUpdateDatetime()), 
																		product.getRate(), 
																		/*product.getProductContentType(),*/ 
																		product.getProductDescription(), 
																		product.getThreshold(), 
																		product.getCurrentQuantity(), 
																		product.getDamageQuantity(), 
																		product.getFreeQuantity(), 
																		product.getProductAddedDatetime(), 
																		product.getProductQuantityUpdatedDatetime(),
																		product.getProductBarcode(),
																		null), 
															calculateProperTaxModel.getMrp(), 
															taxableTotal, 
															igstamount,
															total));
			srno++;
		}
		
		return inventoryProductsList;
	}

	/**
	 * <pre>
	 * fetch supplier product details by product id
	 * @param productId
	 * @return SupplierProductList list
	 * </pre>
	 */
	@Transactional
	public List<SupplierProductList> fetchSupplierListByProductId(long productId) {

		String hql="from SupplierProductList where product.productId="+productId+
				   " and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierProductList> supplierProductLists=(List<SupplierProductList>)query.list();
		if(supplierProductLists.isEmpty())
		{
			return null;
		}
		
		return supplierProductLists;
	}
	/**
	 * <pre>
	 * set employee or company in inventory basis of who is logged in
	 * increase current inventory quantity 
	 * update daily stock details product wise
	 * 
	 * save inventory 
	 * save InventoryProductDetails list
	 * @param inventory
	 * @param productIdList
	 * </pre>
	 */
	@Transactional
	public void addInventory(InventoryRequest inventoryRequest)
	{
		Inventory inventory=inventoryRequest.getInventory();
		List<InventoryDetailsModel> inventoryDetailsList=inventoryRequest.getInventoryDetailsList();
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		Company company=(Company)session.getAttribute("companyDetails");
		
		if(employeeDetails==null){
			inventory.setCompany(company);
		}else{
			inventory.setEmployee(employeeDetails.getEmployee());
		}
		
		inventory.setSupplier(supplierDAO.fetchSupplier(inventory.getSupplier().getSupplierId()));
		
		//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		inventory.setInventoryAddedDatetime(new Date());
		inventory.setDiscountOnMRP(inventory.getDiscountOnMRP());
		inventory.setInventoryTransactionId(inventoryTransactionIdGenerator.generateInventoryTransactionId());
//		double totalAmount=0;
//		double totalAmountWithTax=0;
//	    long totalQuantity=0;
//		
//		String[] prdutIdAndRateList = productIdList.split(",");
//		
//		for(int i=0; i<prdutIdAndRateList.length; i++)
//		{
//			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
//			
//			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
//			totalAmount=totalAmount+(Long.parseLong(prdutIdAndRate[1])*supplierProductList.getSupplierRate());
//			
//			Product product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
//			float igst=product.getCategories().getIgst();
//			double rateWithTax=supplierProductList.getSupplierRate()+((igst*supplierProductList.getSupplierRate())/100);
//			rateWithTax=Float.parseFloat(new DecimalFormat("###").format(rateWithTax));
//			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(rateWithTax, product.getCategories().getIgst());
//			
//			totalAmountWithTax=totalAmountWithTax+(rateWithTax*Long.parseLong(prdutIdAndRate[1]));
//			totalQuantity+=Long.parseLong(prdutIdAndRate[1]);
//		}
		
		

//		inventory.setTotalQuantity(totalQuantity);
//		inventory.setTotalAmount(totalAmount);
//		inventory.setTotalAmountTax(totalAmountWithTax);
		inventory.setPayStatus(false);
		sessionFactory.getCurrentSession().save(inventory);
		
//		prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<inventoryDetailsList.size(); i++)
		{
			InventoryDetailsModel inventoryDetailsModel=inventoryDetailsList.get(i);
			InventoryDetails inventoryDetails=new InventoryDetails();
			
			inventoryDetails.setInventory(inventory);
			
//			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			Product product=productDAO.fetchProductForWebApp(inventoryDetailsModel.getProduct().getProductId());
			
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			orderUsedBrand.setCompany(company);
			orderUsedBrand.setBranch(branch);
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst(),
					company,
					branch);
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					/*product.getProductImage(),
					product.getProductContentType(),*/
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity(),
					company,
					branch);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			inventoryDetails.setProduct(orderUsedProduct);
			
			inventoryDetails.setQuantity(inventoryDetailsModel.getQuantity());
			
//			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
//			
//			float igst=product.getCategories().getIgst();
//			double ratewithtax=supplierProductList.getSupplierRate()+(igst*supplierProductList.getSupplierRate())/100;
//			ratewithtax=Double.parseDouble(new DecimalFormat("###").format(ratewithtax));
//			System.out.println("MRP : "+ratewithtax);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(ratewithtax, product.getCategories().getIgst());
			
			inventoryDetails.setRate(inventoryDetailsModel.getRate());			
			
			inventoryDetails.setAmount(inventoryDetailsModel.getAmount());
			
			product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetailsModel.getQuantity());
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetailsModel.getQuantity(), true);
			productDAO.Update(product);
			
			inventoryDetails.setDiscountType(inventoryDetailsModel.getDiscountType());
			inventoryDetails.setDiscountAmount(inventoryDetailsModel.getDiscountAmount());
			inventoryDetails.setDiscountPercentage(inventoryDetailsModel.getDiscountPercentage());
			inventoryDetails.setAmountBeforeDiscount(inventoryDetailsModel.getAmountBeforeDiscount());
			inventoryDetails.setDiscountOnMRP(inventoryDetailsModel.getDiscountOnMRP());
			sessionFactory.getCurrentSession().save(inventoryDetails);
		}
		
		//here update order product current quantity
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
	}
	
	/**
	 * <pre>
	 * update inventory
	 * 
	 * delete previous inventoryDetails List
	 * cut current quantity when delete inventory details
	 * update daily stock details product wise
	 * 
	 * save inventory details list
	 * add current quantity
	 * update daily stock details product wise
	 *  
	 * @param inventory
	 * @param productIdList
	 * </pre>
	 */
	@Transactional
	public void editInventory(InventoryRequest inventoryRequest)
	{
		//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		
		Inventory inventory=inventoryRequest.getInventory();
		Inventory inventoryOld=fetchInventory(inventory.getInventoryTransactionId());
		inventory.setCompany(inventoryOld.getCompany());
		inventory.setEmployee(inventoryOld.getEmployee());
		inventory.setPayStatus(inventoryOld.isPayStatus());
		inventory.setInventoryAddedDatetime(inventoryOld.getInventoryAddedDatetime());
		inventory.setSupplier(supplierDAO.fetchSupplier(inventory.getSupplier().getSupplierId()));
		inventory.setInventoryTransactionPkId(inventoryOld.getInventoryTransactionPkId());
		inventory.setDiscountOnMRP(inventory.getDiscountOnMRP());
		List<InventoryDetailsModel> inventoryDetailsList=inventoryRequest.getInventoryDetailsList();
		
//		double totalAmount=0;
//		double totalAmountWithTax=0;
//	    long totalQuantity=0;
//		
//		String[] prdutIdAndRateList = productIdList.split(",");
		
//		for(int i=0; i<prdutIdAndRateList.length; i++)
//		{
//			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
//			
//			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
//			totalAmount=totalAmount+(Long.parseLong(prdutIdAndRate[1])*supplierProductList.getSupplierRate());
//			
//			Product product=new Product();
//			product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
//			float igst=product.getCategories().getIgst();
//			double rateWithTax=supplierProductList.getSupplierRate()+((igst*supplierProductList.getSupplierRate())/100);
//			rateWithTax=Float.parseFloat(new DecimalFormat("###").format(rateWithTax));
//			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(rateWithTax, product.getCategories().getIgst());
//			
//			totalAmountWithTax=totalAmountWithTax+(rateWithTax*Long.parseLong(prdutIdAndRate[1]));
//			totalQuantity+=Long.parseLong(prdutIdAndRate[1]);
//		}

//		inventory.setTotalQuantity(totalQuantity);
//		inventory.setTotalAmount(totalAmount);
//		inventory.setTotalAmountTax(totalAmountWithTax);
//		inventory.setPayStatus(false);
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		
		Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
		
		List<InventoryDetails> inventoryDetailsListOld=fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		for(InventoryDetails inventoryDetails:inventoryDetailsListOld)
		{
			Product product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProduct().getProductId());
			product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
			
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
			productDAO.Update(product);
			
			//here update order product current quantity -- not required bcos it will done below
			//orderDetailsDAO.updateOrderUsedProductCurrentQuantity(product);
			
			orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
			inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
			sessionFactory.getCurrentSession().delete(inventoryDetails);
		}
		
//		prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<inventoryDetailsList.size(); i++)
		{
			InventoryDetailsModel inventoryDetailsModel=inventoryDetailsList.get(i);
			InventoryDetails inventoryDetails=new InventoryDetails();
			
			inventoryDetails.setInventory(inventory);
			
//			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			Product product=productDAO.fetchProductForWebApp(inventoryDetailsModel.getProduct().getProductId());
						
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			orderUsedBrand.setCompany(company);
			orderUsedBrand.setBranch(branch);
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst(),
					company,
					branch);
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					/*product.getProductImage(),
					product.getProductContentType(),*/
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity(),
					company,
					branch);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			inventoryDetails.setProduct(orderUsedProduct);
			
			inventoryDetails.setQuantity(inventoryDetailsModel.getQuantity());
			
//			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
//			
//			float igst=product.getCategories().getIgst();
//			double ratewithtax=supplierProductList.getSupplierRate()+(igst*supplierProductList.getSupplierRate())/100;
//			ratewithtax=Double.parseDouble(new DecimalFormat("###").format(ratewithtax));
//			System.out.println("MRP : "+ratewithtax);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(ratewithtax, product.getCategories().getIgst());
			
			inventoryDetails.setRate(inventoryDetailsModel.getRate());			
			
			inventoryDetails.setAmount(inventoryDetailsModel.getAmount());
			
			product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetailsModel.getQuantity());
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetailsModel.getQuantity(), true);
			productDAO.Update(product);
						
			inventoryDetails.setDiscountType(inventoryDetailsModel.getDiscountType());
			inventoryDetails.setDiscountAmount(inventoryDetailsModel.getDiscountAmount());
			inventoryDetails.setDiscountPercentage(inventoryDetailsModel.getDiscountPercentage());
			inventoryDetails.setAmountBeforeDiscount(inventoryDetailsModel.getAmountBeforeDiscount());
			inventoryDetails.setDiscountOnMRP(inventoryDetailsModel.getDiscountOnMRP());
			sessionFactory.getCurrentSession().save(inventoryDetails);
		}
		
		//here update order product current quantity
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
	}

	@Transactional
	public Inventory fetchInventory(String inventoryId) {		
		Inventory inventory=fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryId);
		return inventory;
	}
	/**
	 * <pre>
	 * fetch payment details according inventory transaction(transaction id , supplier name,amount paid,amount unpaid,total amount)
	 * @param inventoryTransactionId
	 * </pre>
	 */
	@Transactional
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId)
	{		
		Inventory inventory=fetchInventory(inventoryTransactionId);
				
		String id=inventory.getSupplier().getSupplierId();
		String name=inventory.getSupplier().getName();
		String inventoryId=inventory.getInventoryTransactionId();
		double amountPaid=0.0d;
		double roundOffAmt=RoundOff.findRoundOffAmount(inventory.getTotalAmountTax());
		double amountUnPaid=roundOffAmt;
		double totalAmount=roundOffAmt;
		String type="supplier";
		String url="giveSupplierOrderPayment";
		
		if(inventory.isPayStatus()==false)
		{
			List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventoryTransactionId);
			
			if(paymentPaySupplierList!=null)
			{
				amountUnPaid=paymentPaySupplierList.get(0).getDueAmount();
				amountPaid=totalAmount-amountUnPaid;
			}
		}else{
			amountPaid=roundOffAmt;
			amountUnPaid=0;
		}
		//new PaymentDoInfo(pkId, id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
		return new PaymentDoInfo(0,id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
	}
	/**
	 * <pre>
	 * fetch supplier payment details by paymentPaySupplierId for edit
	 * @param paymentPaySupplierId
	 * @return PaymentDoInfo
	 * </pre>
	 */
	@Transactional
	public PaymentDoInfo fetchPaymentPaySupplierForEdit(String paymentPaySupplierId){
		PaymentPaySupplier paymentPaySupplier=fetchPaymentPaySupplierListByPaymentPaySupplierId(paymentPaySupplierId);
		PaymentDoInfo paymentDoInfo=fetchPaymentStatus(paymentPaySupplier.getInventory().getInventoryTransactionId());
		
		paymentDoInfo.setPaidAmount(paymentPaySupplier.getPaidAmount());
		paymentDoInfo.setBankName(paymentPaySupplier.getBankName());
		paymentDoInfo.setCheckNumber(paymentPaySupplier.getChequeNumber());
		try {
			paymentDoInfo.setCheckDate(new SimpleDateFormat("yyyy-MM-dd").format(paymentPaySupplier.getChequeDate()));
		} catch (Exception e) {
			paymentDoInfo.setCheckDate("");
		}
		paymentDoInfo.setPaymentId(paymentPaySupplier.getPaymentPayId());
		paymentDoInfo.setPayType(paymentPaySupplier.getPayType());
		paymentDoInfo.setUrl("updatePaymentSupplier");
		paymentDoInfo.setPayType(paymentPaySupplier.getPayType());
		if(paymentPaySupplier.getPayType().equals(Constants.OTHER_PAY_STATUS)){
			paymentDoInfo.setPaymentMethodId(paymentPaySupplier.getPaymentMethod().getPaymentMethodId());
			paymentDoInfo.setTransactionRefNo(paymentPaySupplier.getTransactionReferenceNumber());
		}
		paymentDoInfo.setComment(paymentPaySupplier.getComment());
		return paymentDoInfo;
	}
	/**
	 * <pre>
	 * save supplier payment 
	 * make entry in ledger debit side
	 * @param paymentPaySupplier
	 * </pre>
	 */
	@Transactional
	public void givePayment(PaymentPaySupplier paymentPaySupplier)
	{
		Inventory inventory=paymentPaySupplier.getInventory();
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		sessionFactory.getCurrentSession().save(paymentPaySupplier);
		
		//ledger entry create
		String payMode="";
		if(paymentPaySupplier.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(paymentPaySupplier.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=paymentPaySupplier.getBankName()+"-"+paymentPaySupplier.getChequeNumber();
		}else{
			payMode=paymentPaySupplier.getPaymentMethod().getPaymentMethodName()+"-"+paymentPaySupplier.getTransactionReferenceNumber();
		}
		
		double debit=paymentPaySupplier.getPaidAmount();
		Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
		Ledger ledger=new Ledger(
					payMode, 
					paymentPaySupplier.getPaidDate(), 
					null, 
					paymentPaySupplier.getInventory().getInventoryTransactionId()+" Payment", 
					paymentPaySupplier.getInventory().getSupplier().getName(), 
					debit, 
					0, 
					0, 
					paymentPaySupplier,
					branch
				);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
		
	}
	/**
	 * <pre>
	 * update supplier payment 
	 * update entry in ledger debit side
	 * @param paymentPaySupplier
	 * </pre>
	 */
	@Transactional
	public void updateSupplierPayment(PaymentPaySupplier paymentPaySupplier)
	{
		Inventory inventory=paymentPaySupplier.getInventory();
		
		
		paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
		sessionFactory.getCurrentSession().update(paymentPaySupplier);
		
		//reset due amount according updataion of paid amount
		updatePaymentSupplierDueAmount(paymentPaySupplier.getInventory().getInventoryTransactionId());
		
		//ledger update
		String payMode="";
		if(paymentPaySupplier.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(paymentPaySupplier.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=paymentPaySupplier.getBankName()+"-"+paymentPaySupplier.getChequeNumber();
		}else{
			payMode=paymentPaySupplier.getPaymentMethod().getPaymentMethodName()+"-"+paymentPaySupplier.getTransactionReferenceNumber();
		}
		
		double balance=0,debit=0,debitOld=0,credit=0;		
		Ledger ledger=ledgerDAO.fetchLedger("supplier", String.valueOf(paymentPaySupplier.getPaymentPayId()));
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			debitOld=ledger.getDebit();
			debit=paymentPaySupplier.getPaidAmount();			
			balance=balance-debit;
			
			ledger.setBalance(balance);
			ledger.setDebit(debit);
			ledger.setPayMode(payMode);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//ledger update done 
		
		//paystatus set when update any payment change
		List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventory.getInventoryTransactionId());
		if(paymentPaySupplierList.get(0).getDueAmount()==0)
		{
			inventory.setPayStatus(true);
		}
		else
		{
			inventory.setPayStatus(false);
		}
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		
	}
	/**
	 * <pre>
	 * reset due amount according updataion of paid amount
	 * @param inventoryTransactionId
	 * </pre>
	 */
	@Transactional
	public void updatePaymentSupplierDueAmount(String inventoryTransactionId){
		Inventory inventory=fetchInventory(inventoryTransactionId);
		double totalAmountWithTax=inventory.getTotalAmountTax();
		
		List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventoryTransactionId);
		if(paymentPaySupplierList!=null){
			Collections.reverse(paymentPaySupplierList);
			for(PaymentPaySupplier paymentPaySupplier: paymentPaySupplierList){
				totalAmountWithTax-=paymentPaySupplier.getPaidAmount();
				paymentPaySupplier.setDueAmount(totalAmountWithTax);
				
				paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
				sessionFactory.getCurrentSession().update(paymentPaySupplier);
			}
		}
	}
	
	/**
	 * <pre>
	 * fetch inventory details view 
	 * if supplier is not zero then all inventory related logged user company 
	 * else supplier id wise 
	 * 
	 * paid/unpaid/partially paid status find using PaymentPaySupplier list
	 * 
	 * @param supplierId
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @return InventoryReportView list
	 * </pre>
	 */
	@Transactional
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range)
	{
		List<InventoryReportView> inventoryReportViews=new ArrayList<>();
		
		String hql="";
		Query query;
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		/**
		 * here only show inventory details which payment unpaid till current date
		 * 
		 * */
		if(supplierId.equals(""))
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"')";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"')";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"')";
			}
			else if(range.equals("lastMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLastMonthLastDate()+"')";
			}
			else if(range.equals("last3Months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"')";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = date(CURRENT_DATE()) )";
			}
			else if(range.equals("currentMonth")){
				cal.add(Calendar.MONTH, -12);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where 1=1 ";
			}
			else if(range.equals("pickDate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"')";
			}
		}
		else
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("lastMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("last3Months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("currentMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')  and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where payStatus=false and supplier.supplierId='"+supplierId+"'";    
			}
			else if(range.equals("pickdate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"') and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = DATE(CURRENT_DATE()) ) and supplier.supplierId='"+supplierId+"'";
			}
		}		
		// and payStatus=false //remove bcos its show for edit payment
		hql+=" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
		hql+=" and supplier.branch.branchId in ("+getSessionSelectedBranchIds()+") ";
		hql+=" order by inventoryAddedDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		long srno=1;
		for(Inventory inventory:inventoryList)
		{
			double amountPaid=0;
			double amountUnPaid=0;
			
			List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventory.getInventoryTransactionId());
			
			double roundOff=RoundOff.findRoundOffAmount(inventory.getTotalAmountTax());

			if(paymentPaySupplierList==null)
			{
				amountUnPaid=roundOff;
			}
			else
			{
				for(PaymentPaySupplier paymentPaySupplier: paymentPaySupplierList){
					amountPaid+=paymentPaySupplier.getPaidAmount();
				}
				amountUnPaid=roundOff-amountPaid;
			}
			
			String payStatus="";
			
			if(amountPaid>=roundOff)
			{
				payStatus="Paid";
			}
			else if(amountUnPaid<roundOff && amountUnPaid>0)
			{
				payStatus="Partially Paid";
			}
			else
			{
				payStatus="UnPaid";
			}
			
			//who made this inventory
			String byUserName="";
			if(inventory.getCompany()!=null){
				byUserName="Company";
			}else{
				byUserName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(inventory.getEmployee().getEmployeeId()).getName();
			}			
			inventoryReportViews.add(new InventoryReportView(
					srno, 
					inventory.getInventoryTransactionId(), 
					new SupplierModel(
							inventory.getSupplier().getSupplierPKId(), 
							inventory.getSupplier().getSupplierId(), 
							inventory.getSupplier().getName(), 
							inventory.getSupplier().getContact()),
					inventory.getTotalQuantity(), 
					inventory.getTotalAmount(), 
					inventory.getTotalAmountTax(), 
					amountPaid, 
					amountUnPaid, 
					inventory.getInventoryAddedDatetime(),
					inventory.getInventoryPaymentDatetime(), 
					byUserName,
					payStatus,
					inventory.getBillDate(),
					inventory.getBillNumber(),
					inventory.getDiscountAmount(),
					inventory.getDiscountPercentage(),
					inventory.getDiscountType(),
					inventory.getTotalAmountBeforeDiscount(),
					inventory.getTotalAmountTaxBeforeDiscount(),
					inventory.getDiscountGiven(),
					inventory.getDiscountOnMRP()));
			srno++;
		}
		
		return inventoryReportViews;
	}
	/**
	 * <pre>
	 * fetch payment details according inventory transaction id
	 * @param inventoryId
	 * </pre>
	 */
	@Transactional
	public List<PaymentPaySupplier> fetchPaymentPaySupplierListByInventoryId(String inventoryId) {
		String hql="from PaymentPaySupplier where status=false and inventory.inventoryTransactionId='"+inventoryId+"'"+
				" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds()+
				" and inventory.supplier.branch.branchId="+getSessionSelectedBranchIds()+
				" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		if(paymentPaySupplierList.isEmpty()){
			return null;
		}
		return paymentPaySupplierList;
	}
	
	@Transactional
	public PaymentPaySupplier fetchPaymentPaySupplierListByPaymentPaySupplierId(String paymentPaySupplierId) {
		String hql="from PaymentPaySupplier where status=false and paymentPayId="+paymentPaySupplierId+
				" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds()+
				" and inventory.supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		if(paymentPaySupplierList.isEmpty()){
			return null;
		}
		return paymentPaySupplierList.get(0);
	}
	/**
	 * <pre>
	 * delete supplier payment 
	 * delete entry in ledger debit side
	 * change after ledger records balance according previous record
	 * @param paymentPaySupplier
	 * </pre>
	 */
	@Transactional
	public void deletePaymentPaySupplierListByInventoryId(String paymentPaySupplierId) {
		
		PaymentPaySupplier paymentPaySupplier=fetchPaymentPaySupplierListByPaymentPaySupplierId(paymentPaySupplierId);
		paymentPaySupplier.setStatus(true);
		
		paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
		sessionFactory.getCurrentSession().update(paymentPaySupplier);
		
		updatePaymentSupplierDueAmount(paymentPaySupplier.getInventory().getInventoryTransactionId());

		//delete ledger
		double balance=0,debit=0,debitOld=0,credit=0;
		
		Ledger ledger=ledgerDAO.fetchLedger("supplier", String.valueOf(paymentPaySupplier.getPaymentPayId()));
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			debitOld=ledger.getDebit();
			balance=balance+debitOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		
		//paystatus set when update any payment change
		Inventory inventory=fetchInventory(paymentPaySupplier.getInventory().getInventoryTransactionId());
		List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventory.getInventoryTransactionId());
		if(paymentPaySupplierList!=null){
			if(paymentPaySupplierList.get(0).getDueAmount()==0)
			{
				inventory.setPayStatus(true);
			}
			else
			{
				inventory.setPayStatus(false);
			}
			
		}else{
			inventory.setPayStatus(false);
		}
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
	}
	
	@Transactional
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId)
	{
		String hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId+"'"+
					" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds()+
					" and inventory.supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailList=(List<InventoryDetails>)query.list();
		
		return inventoryDetailList;
	}
	
	/*public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList){
		
		List<InventoryProduct> inventoryProducts=new ArrayList<>();
		
		for(InventoryProduct inventoryProduct : inventoryProductsList)
		{
			inventoryProduct.getProduct().setProductImage(null);
			inventoryProducts.add(inventoryProduct);			
		}
		
		return inventoryProducts;
	}*/
	/**
	 * <pre>
	 * set employee  in inventory basis of who is logged in
	 * increase current inventory quantity 
	 * update daily stock details product wise
	 * 
	 * save inventory 
	 * save InventoryProductDetails list
	 * @param inventory
	 * @param productIdList
	 * </pre>
	 */
	@Transactional
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
				
			Inventory inventory=inventorySaveRequestModel.getInventory();			
			
			Supplier supplier=supplierDAO.fetchSupplier(inventory.getSupplier().getSupplierId());
			inventory.setSupplier(supplier);
			
			//set logged employee to inventory as adding user
			Long employeeId=getAppLoggedEmployeeId();
			Employee employee=new Employee();
			employee.setEmployeeId(employeeId);
			inventory.setEmployee(employee);
			
			inventory.setInventoryAddedDatetime(new Date());	
			try {
				inventory.setBillDate(simpleDateFormat.parse(inventorySaveRequestModel.getBillDate()));
				inventory.setInventoryPaymentDatetime(simpleDateFormat.parse(inventorySaveRequestModel.getPaymentDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			inventory.setInventoryTransactionId(inventoryTransactionIdGenerator.generateInventoryTransactionId());
			inventory.setPayStatus(false);
			sessionFactory.getCurrentSession().save(inventory);  // here we are saveing the inventory object
			
			// here we are saveing the Listof InventoryDetails 
			List<InventoryDetails> finalListToSave=inventorySaveRequestModel.getInventoryDetails();
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			for(InventoryDetails inventoryDetails:finalListToSave)
			{
				Product product=new Product();
				product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProductId());
				
				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				orderUsedBrand.setCompany(company);
				orderUsedBrand.setBranch(branch);
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(),
						company,
						branch);
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						/*product.getProductImage(),
						product.getProductContentType(),*/
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity(),
						company,
						branch);
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				inventoryDetails.setProduct(orderUsedProduct);
				
				product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), true);
				productDAO.Update(product);
				
				inventoryDetails.setInventory(inventory);
				sessionFactory.getCurrentSession().save(inventoryDetails);
			}
			
			//here update order product current quantity
			orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	
	}
	/**
	 * <pre>
	 * update inventory
	 * 
	 * delete previous inventoryDetails List
	 * cut current quantity when delete inventory details
	 * update daily stock details product wise
	 * 
	 * save inventory details list
	 * add current quantity
	 * update daily stock details product wise
	 *  
	 * @param inventory
	 * @param productIdList
	 * </pre>
	 */
	@Transactional
	public void editInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		
			//old inventory 
			Inventory inverntoryOld=fetchInventory(inventorySaveRequestModel.getInventory().getInventoryTransactionId());
			
			Inventory inventory=inventorySaveRequestModel.getInventory();
			inventory.setInventoryTransactionPkId(inverntoryOld.getInventoryTransactionPkId());
			inventory.setCompany(inverntoryOld.getCompany());
			inventory.setSupplier(inverntoryOld.getSupplier());
			//inventory.setPayStatus(false);
			inventory.setEmployee(inverntoryOld.getEmployee());
			inventory.setInventoryAddedDatetime(inverntoryOld.getInventoryAddedDatetime());	
			try {
				inventory.setBillDate(simpleDateFormat.parse(inventorySaveRequestModel.getBillDate()));
				inventory.setInventoryPaymentDatetime(simpleDateFormat.parse(inventorySaveRequestModel.getPaymentDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			}
			inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
			sessionFactory.getCurrentSession().update(inventory);  // here we are saveing the inventory object
			
			Product product=new Product();
			
			//old inventory details list
			List<InventoryDetails> inventoryDetailsList=fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventorySaveRequestModel.getInventory().getInventoryTransactionId());
			for(InventoryDetails inventoryDetails: inventoryDetailsList){
				
				product=inventoryDetails.getProduct().getProduct();
				product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
				productDAO.Update(product);
				
				orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
				inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
				sessionFactory.getCurrentSession().delete(inventoryDetails);
				
			}
			
			
			// here we are saveing the Listof InventoryDetails 
			List<InventoryDetails> finalListToSave=inventorySaveRequestModel.getInventoryDetails();
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			for(InventoryDetails inventoryDetails:finalListToSave)
			{
				product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProductId());

				
				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				orderUsedBrand.setCompany(company);
				orderUsedBrand.setBranch(branch);
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(),
						company,
						branch);
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						/*product.getProductImage(),
						product.getProductContentType(),*/
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity(),
						company,
						branch);
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				inventoryDetails.setProduct(orderUsedProduct);
				
				product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), true);
				productDAO.Update(product);
				
				inventoryDetails.setInventory(inventory);
				sessionFactory.getCurrentSession().save(inventoryDetails);
			}
			
			//here update order product current quantity
			orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * fetch inventory details by range,startdate,enddate for app gatekeeper
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * @return Inventory list
	 * </pre>
	 */
	@Transactional
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,
			String toDate, String range) {
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance(); 
	
		if(range.equals("range")){
			hql="from Inventory where date(inventoryAddedDatetime)>='"+fromDate+"' And date(inventoryAddedDatetime)<='"+toDate+"'"; 
			}else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		
			}else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
		
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("pickDate")){
				hql="from Inventory where  date(inventoryAddedDatetime)='"+fromDate +"'";
			}
			else if(range.equals("currentDate"))
            {
                hql="from Inventory where  date(inventoryAddedDatetime)=date(CURRENT_DATE())";
            }
			else if(range.equals("viewAll"))
			{
				hql="from Inventory where 1=1 ";
			}
		
			hql+=" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
			hql+=" and supplier.branch.branchId="+getSessionSelectedBranchIds();
			hql+="  order by inventoryAddedDatetime desc";

			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Inventory> inventoryAddedList=(List<Inventory>)query.list();
			
			if(inventoryAddedList.isEmpty()){
				return null;
			}
			return inventoryAddedList;					
	}
	@Transactional
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		
		hql="from Inventory where inventoryTransactionId='"+inventoryTransactionId +"'"+
				" and supplier.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and supplier.branch.branchId="+getSessionSelectedBranchIds();
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		
		if(inventoryList.isEmpty()){
			return null;
		}
		return inventoryList.get(0);
	}
	@Transactional
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		
		hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId +"'"+
			" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and inventory.supplier.branch.branchId="+getSessionSelectedBranchIds();
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailsList=(List<InventoryDetails>)query.list();
		if(inventoryDetailsList.isEmpty()){
			return null;
		}
		
		
		return inventoryDetailsList;
	}
	
	/*public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(List<InventoryDetails> inventoryDetailsList)
	{
		List<InventoryDetails> inventoryDetailsList2=new ArrayList<>();
		
		for(InventoryDetails inventoryDetails: inventoryDetailsList)
		{
			inventoryDetails.getProduct().setProductImage(null);
			inventoryDetails.getProduct().getProduct().setProductImage(null);
			inventoryDetailsList2.add(inventoryDetails);
		}
		return inventoryDetailsList2;
	}*/
	/**
	 * <pre>
	 * total current amount of current total quantity of all product 
	 * @return total inventory amount
	 * </per>
	 */
	@Transactional
	public double fetchTotalValueOfCurrrentInventory()
	{
		////ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAO.fetchProductListForWebApp();
		
		double totalValueOfCurrrentInventory=0;
		if(productsList!=null){
			for(Product product: productsList)
			{
				double rate=0;
				
				rate = product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
				
				totalValueOfCurrrentInventory+=product.getCurrentQuantity()*Double.parseDouble(new DecimalFormat("###").format(rate));
			}
		}
		return totalValueOfCurrrentInventory;
	}
	/**
	 * <pre>
	 * find which product under threshold quantity
	 * @return count of under threshold quantity products 
	 * </pre>
	 */
	@Transactional
	public long fetchProductUnderThresholdCount(){
		
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAO.fetchProductListForWebApp();
		
		long productUnderThresholdCount=0;
		if(productsList!=null){			
			for(Product product: productsList)
			{
				if(product.getCurrentQuantity()<=product.getThreshold())
				{
					productUnderThresholdCount++;
				}
			}
		}
		return productUnderThresholdCount;
	}
	/**
	 * <pre>
	 * delete inventory
	 * cut inventory details product quantity from current inventory quantity
	 * </pre>
	 */
	@Transactional
	public void deleteInventory(String inventoryId)
	{
		Inventory inventory=fetchInventory(inventoryId);
		
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		Product product;
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<InventoryDetails> inventoryDetailsList=fetchTrasactionDetailsByInventoryId(inventoryId);
		for(InventoryDetails inventoryDetails: inventoryDetailsList)
		{
			product=inventoryDetails.getProduct().getProduct();
			product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
			productDAO.Update(product);
			
			orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
			inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
			sessionFactory.getCurrentSession().delete(inventoryDetails);
		}
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().delete(inventory);
		
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * find total amount of payment done between startDate and endDate
	 * @param startDate
	 * @param endDate
	 * @return total Paid amount which between startDate and endDate
	 * </pre>
	 */
	@Transactional
	public double totalSupplierPaidAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="from PaymentPaySupplier where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') and status=false and inventory.supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and inventory.supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		double totalAmountPaid=0;
		for(PaymentPaySupplier paymentPaySupplier : paymentPaySupplierList){
			totalAmountPaid+=paymentPaySupplier.getPaidAmount();
		}
		return totalAmountPaid;
	}
	
	/**
	 * inventory added invoice making data
	 * @param inventoryTsnId
	 * @return
	 */
	@Transactional
	public InventoryAddedInvoiceModel getInvoiceDetails(String inventoryTsnId){
		
		Inventory inventory=fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryTsnId);
		
		Company company;
		if(inventory.getCompany()==null){
			company=inventory.getEmployee().getCompany();
		}else{
			company=inventory.getCompany();
		}
		
		String coTelNo="NA";
		if(company.getContact().getTelephoneNumber()!=null &&
				company.getContact().getMobileNumber() !=null){
			if(!company.getContact().getTelephoneNumber().equals("") &&
					!company.getContact().getMobileNumber().equals("")){
				coTelNo=company.getContact().getTelephoneNumber()+"/"+company.getContact().getMobileNumber();
			}
		}
		if(company.getContact().getTelephoneNumber()!=null &&
				company.getContact().getMobileNumber() ==null){
			if(!company.getContact().getMobileNumber().equals("")){
				coTelNo=company.getContact().getTelephoneNumber();
			}
		}
		if(company.getContact().getTelephoneNumber()==null &&
				company.getContact().getMobileNumber() !=null){
			if(!company.getContact().getMobileNumber().equals("")){
				coTelNo=company.getContact().getMobileNumber();
			}
		}
		
		String emailId=company.getContact().getEmailId();
		if(emailId==null){
			emailId="NA";
		}
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
		
		String mobNoAndTelNo="NA";
		if(inventory.getSupplier().getContact().getTelephoneNumber()!=null &&
				inventory.getSupplier().getContact().getMobileNumber() !=null){
			if(!inventory.getSupplier().getContact().getTelephoneNumber().equals("") &&
					!inventory.getSupplier().getContact().getMobileNumber().equals("")){
				mobNoAndTelNo=inventory.getSupplier().getContact().getTelephoneNumber()+"/"+inventory.getSupplier().getContact().getMobileNumber();
			}
		}
		if(inventory.getSupplier().getContact().getTelephoneNumber()!=null &&
				inventory.getSupplier().getContact().getMobileNumber() ==null){
			if(!inventory.getSupplier().getContact().getTelephoneNumber().equals("") ){
				mobNoAndTelNo=inventory.getSupplier().getContact().getTelephoneNumber();
			}
		}
		if(inventory.getSupplier().getContact().getTelephoneNumber()==null &&
				inventory.getSupplier().getContact().getMobileNumber() !=null){
			if(!inventory.getSupplier().getContact().getMobileNumber().equals("")){
				mobNoAndTelNo=inventory.getSupplier().getContact().getMobileNumber();
			}
		}
		

		DecimalFormat decimalFormatInt=new DecimalFormat("###");
		List<InventoryDetails> inventoryDetailsList=fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventoryTsnId);
		// srNo, productName, hsnCode,
		// taxSlab, MRP, Qty, totalAmt,
		// discPer, discAmt, newTotalAmt
		List<Map<String, Object>> productList=new ArrayList<>();
		long srNo=1;
		for(InventoryDetails inventoryDetails : inventoryDetailsList){
			Map<String, Object> productData=new HashMap<>();
			productData.put("srNo", srNo);
			productData.put("productName", inventoryDetails.getProduct().getProductName());
			productData.put("hsnCode", inventoryDetails.getProduct().getCategories().getHsnCode());
			productData.put("taxSlab", decimalFormatInt.format(inventoryDetails.getProduct().getCategories().getIgst()));
			productData.put("MRP", inventoryDetails.getRate());
			productData.put("Qty", inventoryDetails.getQuantity());
			productData.put("totalAmt", inventoryDetails.getAmountBeforeDiscount());
			productData.put("discPer", ((inventoryDetails.getDiscountType().equals("PERCENTAGE"))?inventoryDetails.getDiscountPercentage():0));
			productData.put("discAmt", ((inventoryDetails.getDiscountType().equals("AMOUNT"))?inventoryDetails.getDiscountAmount():0));
			productData.put("newTotalAmt", inventoryDetails.getAmount());
			productList.add(productData);
			srNo++;
		}
		
		/* roundOff */
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		//find round of amount
		float totalAmountAfterRoundOff=Float.parseFloat(decimalFormat.format(inventory.getTotalAmountTax()));
		totalAmountAfterRoundOff=Float.parseFloat(decimalFormat.format(totalAmountAfterRoundOff));
		float decimalAmount=totalAmountAfterRoundOff-(int)totalAmountAfterRoundOff;
		float roundOff;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOff=0.0f;
			roundOfAmount=""+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff+roundOff;
		}
		else if(decimalAmount>=0.5)
		{
			roundOff=1-decimalAmount;
			roundOfAmount="+"+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff+roundOff;
		}
		else
		{
			roundOff=Float.parseFloat(decimalFormat.format(decimalAmount));
			roundOfAmount="-"+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff-roundOff;
		}	
		
		String totalAmountAfterRoundOffInWord=NumberToWordsConverter.convertWithPaisa(Float.parseFloat(decimalFormat.format(totalAmountAfterRoundOff)));
		
		List<Map<String, Object>>taxList=new ArrayList<>(); // hsnCode, taxableValue, 
		// cgstRate, cgstAmt, 
		//sgstRate, sgstAmt,
		// igstRate, igstAmt
		
		Set<OrderUsedCategories> categoriesUsed=new HashSet<>();
		for(InventoryDetails inventoryDetails : inventoryDetailsList){
			categoriesUsed.add(inventoryDetails.getProduct().getCategories());
		}
		
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		float totalTaxableAmt=0,
		totalCgstAmt=0,
		totalSgstAmt=0,
		totalIgstAmt=0;
		
		//category wise taxslab and amount find  
		Set<String> categoriesHsnCodeList=new HashSet<>();
		for(OrderUsedCategories categories: categoriesUsed)
		{
			String hsncode="";			
			float taxableValue=0;
			float cgstPercentage=0; 
			float igstPercentage=0;			
			float sgstPercentage=0;
			float cgstRate=0; 
			float igstRate=0;
			float sgstRate=0;
			
			float cGSTAmount=0;
			float iGSTAmount=0;
			float sGSTAmount=0;
			
			boolean gotDuplicateHsnCode=false;
			
			for(String hsnCode : categoriesHsnCodeList)
			{
				if(categories.getHsnCode().equals(hsnCode))
				{
					gotDuplicateHsnCode=true;
				}
			}
			if(gotDuplicateHsnCode)
			{
				continue;
			}
			
			categoriesHsnCodeList.add(categories.getHsnCode());
			
			for(InventoryDetails inventoryDetails : inventoryDetailsList)
			{
				if(categories.getHsnCode().equals(inventoryDetails.getProduct().getCategories().getHsnCode()))
				{
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(inventoryDetails.getAmount(), 
							inventoryDetails.getProduct().getCategories().getIgst());
					taxableValue+=calculateProperTaxModel.getUnitprice();
					
					float igst=inventoryDetails.getProduct().getCategories().getIgst();
					float cgst=inventoryDetails.getProduct().getCategories().getCgst();
					float sgst=inventoryDetails.getProduct().getCategories().getSgst();
					float rate=calculateProperTaxModel.getUnitprice();
					long purchaseQuantity=inventoryDetails.getQuantity();
					
					if(inventory.getSupplier().getTaxType().equals(Constants.INTRA_STATUS))
					{
						cGSTAmount+=calculateProperTaxModel.getCgst();
						iGSTAmount+=0;
						sGSTAmount+=calculateProperTaxModel.getSgst();
					}
					else
					{
						cGSTAmount+=0;
						iGSTAmount+=calculateProperTaxModel.getIgst();
						sGSTAmount+=0;						
					}
					cgstPercentage=inventoryDetails.getProduct().getCategories().getCgst();
					igstPercentage=inventoryDetails.getProduct().getCategories().getIgst();	
					sgstPercentage=inventoryDetails.getProduct().getCategories().getSgst();
					hsncode=inventoryDetails.getProduct().getCategories().getHsnCode();
				}
			}
			
			// hsnCode, taxableValue, 
			// cgstRate, cgstAmt, 
			//sgstRate, sgstAmt,
			// igstRate, igstAmt
			Map<String, Object> taxData=new HashMap<>();
			taxData.put("hsnCode", hsncode);
			taxData.put("taxableValue", decimalFormat.format(taxableValue));
			taxData.put("cgstRate", decimalFormat.format(cgstPercentage));
			taxData.put("cgstAmt", decimalFormatThreeDigit.format(cGSTAmount));
			taxData.put("sgstRate", decimalFormat.format(sgstPercentage));
			taxData.put("sgstAmt", decimalFormatThreeDigit.format(sGSTAmount));
			taxData.put("igstRate", decimalFormat.format(igstPercentage));
			taxData.put("igstAmt", decimalFormat.format(iGSTAmount));
			taxList.add(taxData);
			
			totalTaxableAmt+=taxableValue;
			totalCgstAmt+=cGSTAmount;
			totalSgstAmt+=sGSTAmount;
			totalIgstAmt+=iGSTAmount;			
		}
		
		
		/**
		 * calculate category wise block under discount and net amount 
		 */
		float discTaxableAmt=0, 
		discCgstAmt=0, 
		discSgstAmt=0, 
		discIgstAmt=0, 
		netTaxableAmt=0, 
		netCgstAmt=0, 
		netSgstAmt=0, 
		netIgstAmt=0;
		float totalAmountTax=Float.parseFloat(decimalFormat.format(inventory.getTotalAmountTaxBeforeDiscount()));
		if(inventory.getDiscountGiven()){
			if (inventory.getDiscountType().equals("AMOUNT")) {
				
				float discountAmtCut=Float.parseFloat(decimalFormat.format(inventory.getDiscountAmount()));
				
				discTaxableAmt=((discountAmtCut/totalAmountTax)*totalTaxableAmt);
				discCgstAmt=((discountAmtCut/totalAmountTax)*totalCgstAmt);
				discIgstAmt=((discountAmtCut/totalAmountTax)*totalIgstAmt);
				discSgstAmt=((discountAmtCut/totalAmountTax)*totalSgstAmt);
				
				netTaxableAmt=totalTaxableAmt-discTaxableAmt;
				netCgstAmt=totalCgstAmt-discCgstAmt;
				netSgstAmt=totalSgstAmt-discSgstAmt;
				netIgstAmt=totalIgstAmt-discIgstAmt;
				
			}else{
				float discountPerCut=Float.parseFloat(decimalFormat.format(inventory.getDiscountPercentage()));
				
				discTaxableAmt=(discountPerCut*totalTaxableAmt)/100;
				discCgstAmt=(discountPerCut*totalCgstAmt)/100;
				discIgstAmt=(discountPerCut*totalIgstAmt)/100;
				discSgstAmt=(discountPerCut*totalSgstAmt)/100;
				
				netTaxableAmt=totalTaxableAmt-discTaxableAmt;
				netCgstAmt=totalCgstAmt-discCgstAmt;
				netSgstAmt=totalSgstAmt-discSgstAmt;
				netIgstAmt=totalIgstAmt-discIgstAmt;
			}
		}else{
			netTaxableAmt=totalTaxableAmt-discTaxableAmt;
			 netCgstAmt=totalCgstAmt-discCgstAmt;
			 netSgstAmt=totalSgstAmt-discSgstAmt;
			 netIgstAmt=totalIgstAmt-discIgstAmt;
		}
		
		String netTaxAmtInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(netIgstAmt, 2)));
		
		return new InventoryAddedInvoiceModel(
				company.getCompanyName(), 
				company.getAddress(), 
				coTelNo, 
				company.getGstinno(), 
				emailId, 
				inventory.getBillNumber(), 
				dateFormat.format(inventory.getBillDate()), 
				inventory.getSupplier().getName(), 
				inventory.getSupplier().getAddress(), 
				"27", 
				mobNoAndTelNo, 
				inventory.getSupplier().getGstinNo(), 
				inventory.getInventoryTransactionId(), 
				dateFormat.format(inventory.getInventoryAddedDatetime()), 
				productList, // srNo, productName, hsnCode,
							// taxSlab, MRP, Qty, totalAmt,
							// discPer, discAmt, newTotalAmt
				inventory.getTotalQuantity()+"", 
				decimalFormat.format(inventory.getTotalAmountTaxBeforeDiscount())+"", 
				decimalFormat.format(inventory.getDiscountPercentage()),
				decimalFormat.format(inventory.getDiscountAmount()),
				inventory.getDiscountType(), 
				decimalFormat.format(inventory.getTotalAmountTax())+"", 
				roundOfAmount, 
				totalAmountAfterRoundOff+"", 
				totalAmountAfterRoundOffInWord, 
				taxList, // hsnCode, taxableValue, 
						// cgstRate, cgstAmt, 
						//sgstRate, sgstAmt,
						// igstRate, igstAmt
				decimalFormat.format(totalTaxableAmt), 
				decimalFormatThreeDigit.format(totalCgstAmt), 
				decimalFormatThreeDigit.format(totalSgstAmt), 
				decimalFormat.format(totalIgstAmt), 
				decimalFormat.format(discTaxableAmt), 
				decimalFormatThreeDigit.format(discCgstAmt), 
				decimalFormatThreeDigit.format(discSgstAmt), 
				decimalFormat.format(discIgstAmt), 
				decimalFormat.format(netTaxableAmt), 
				decimalFormatThreeDigit.format(netCgstAmt), 
				decimalFormatThreeDigit.format(netSgstAmt), 
				decimalFormat.format(netIgstAmt), 
				netTaxAmtInWord);
	}
	
}
