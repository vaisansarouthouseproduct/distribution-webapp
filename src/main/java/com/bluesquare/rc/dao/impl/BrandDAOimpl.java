package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BrandDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.responseEntities.BrandModel;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation 
 * provides Implementation for following methods of BrandDAO
 * 1.saveBrandForWebApp(Brand brand)
 * 2.updateBrandForWebApp(Brand brand)
 * 3.fetchBrandForWebApp(long brandId
 * 4.fetchBrandListForWebApp()
 * </pre>
 */
@Repository("brandDAO")
@Component
public class BrandDAOimpl extends TokenHandler implements BrandDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	AreaDAO areaDAO;
		
	@Autowired
	Company company;
	
	/**
	 * <pre>
	 * set logger user companyId 
	 * save brand 
	 * @param brand
	 * @return
	 * </pre>
	 */
	@Transactional
	public void saveBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		brand.setCompany(company);
		sessionFactory.getCurrentSession().save(brand);
	}
	/**
	 * <pre>
	 * update brand
	 * @param brand 
	 * @return
	 * </pre>
	 */
	@Transactional
	public void updateBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		brand=(Brand)sessionFactory.getCurrentSession().merge(brand);
		sessionFactory.getCurrentSession().update(brand);
	}
	/**
	 * <pre>
	 * fetch brand details by brand Id
	 * in logger user company 
	 * @param brandId
	 * @return Brand
	 * </pre>
	 */
	@Transactional
	public Brand fetchBrandForWebApp(long brandId) {
		String hql="from Brand where brandId="+brandId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list.get(0);
	}
	
	/**
	 * <pre>
	 * fetch brand details by brand Id
	 * in logger user company 
	 * @param brandId
	 * @return Brand
	 * </pre>
	 */
	@Transactional
	public BrandModel fetchBrandModelForWebApp(long brandId) {
		Brand brand=fetchBrandForWebApp(brandId);
		return new BrandModel(brandId, brand.getName(), brand.getBrandAddedDatetime(), brand.getBrandUpdateDatetime());
	}
	
	/**
	 * <pre>
	 * fetch brand list with come under logged user company
	 * @param
	 * @return  brand list
	 * </pre>
	 */
	@Transactional
	public List<Brand> fetchBrandListForWebApp() {
		String hql="from Brand where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		hql+=" order by name";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list;
	}
	
	/**
	 * <pre>
	 * fetch brand list with come under logged user company
	 * @param
	 * @return   list
	 * </pre>
	 */
	@Transactional
	public List<BrandModel> fetchBrandModelListForWebApp() {
		
		List<Brand> list=fetchBrandListForWebApp();
		if(list==null)
		{
			return null;
		}		
		List<BrandModel> brandModelList=new ArrayList<>();
		for(Brand brand : list){
			brandModelList.add(new BrandModel(
					brand.getBrandId(), 
					brand.getName(), 
					brand.getBrandAddedDatetime(), 
					brand.getBrandUpdateDatetime()));
		}
		return brandModelList;
	}

}
