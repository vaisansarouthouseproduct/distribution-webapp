package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.CategoriesDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.responseEntities.CategoriesModel;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * provides Implementation for following methods of CategoriesDAO
 * 1.saveCategoriesForWebApp(Categories categories
 * 2.updateCategoriesForWebApp(Categories categories)
 * 3.fetchCategoriesForWebApp(long categoriesId)
 * 4.fetchCategoriesListForWebApp()
 * </pre>
 */
@Repository("categoriesDAO")
@Component
public class CategoriesDAOImpl extends TokenHandler implements CategoriesDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	public CategoriesDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public CategoriesDAOImpl() {
	}

	
	/***
	 * <pre>
	 * set company in category
	 * and save category
	 * @param categories
	 * </pre>
	 */
	@Transactional
	public void saveCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		categories.setCompany(company);
		sessionFactory.getCurrentSession().save(categories);
	}
	/**
	 * <pre>
	 * update category
	 * @param categories
	 * </pre>
	 */
	@Transactional
	public void updateCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		categories=(Categories)sessionFactory.getCurrentSession().merge(categories);
		sessionFactory.getCurrentSession().update(categories);
	}
	/**
	 * <pre>
	 * fetch category by categoryId
	 * checking mapping with logged user company
	 * @param categoriesId
	 * @return Categories
	 * <pre>
	 */ 
	@Transactional
	public Categories fetchCategoriesForWebApp(long categoriesId) {
		String hql = "from Categories where categoryId=" + categoriesId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();
		if (categoriesList.isEmpty()) {
			return null;
		}

		return categoriesList.get(0);
	}
	/**
	 * <pre>
	 * fetch category list which mapped with logged user company
	 * @return category list
	 * </pre>
	 */
	@Transactional
	public List<Categories> fetchCategoriesListForWebApp() {
		String hql = "from Categories where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		hql+=" order by categoryName";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();

		if (categoriesList.isEmpty()) {
			return null;
		}
		return categoriesList;
	}
	/**
	 * <pre>
	 * fetch category list which mapped with logged user company
	 * @return categoryModel list
	 * </pre>
	 */
	@Transactional
	public List<CategoriesModel> fetchCategoriesModelList() {
		
		List<Categories> categoriesList = fetchCategoriesListForWebApp();

		if (categoriesList==null) {
			return null;
		}
		List<CategoriesModel> categoriesModelList=new ArrayList<>(); 
		for(Categories categories : categoriesList){
			categoriesModelList.add(new CategoriesModel(
					categories.getCategoryId(), 
					categories.getCategoryName(), 
					categories.getCategoryDescription(), 
					categories.getHsnCode(), 
					categories.getCgst(),
					categories.getSgst(), 
					categories.getIgst(), 
					categories.getCategoryDate(), 
					categories.getCategoryUpdateDate()));
		}
		return categoriesModelList;
	}

}
