package com.bluesquare.rc.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.NotepadDAO;
import com.bluesquare.rc.entities.NotePad;

/**
 * <pre>
 * @author Sachin Sharma 06-06-2018 Code Documentation
 * provides Implementation for following methods of CityDAO
 * 1.saveNotePad(NotePad notePad)
 * 2.fetchNotepadById(long id) 
 * 3.fetchNotePadListByEmployeeId(long employeeId)
 * 4.updateNotePad(NotePad notePad) 
 * 5.deleteNotePadById(long id) 
 * 6.
 * </pre>
 */


@Repository("notepadDAO")
@Component
public class NotepadDAOImpl implements NotepadDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	
	@Transactional
	public void saveNotePad(NotePad notePad) {
		// TODO Auto-generated method stub
		notePad.setNotepadAddedDateTime(new Date());
		notePad.setNotepadUpdatedDateTime(new Date());
		sessionFactory.getCurrentSession().save(notePad);
		
	}

	@Transactional
	public NotePad fetchNotepadById(long id) {
		// TODO Auto-generated method stub
		String hql="";
		Query query=null;
		hql="from NotePad where notepadId="+id;
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<NotePad> notepad=(List<NotePad>)query.list();
		if(notepad.isEmpty()){
			return null;
		}
		
		return notepad.get(0);
	}

	@Transactional
	public List<NotePad> fetchNotePadListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query=null;
		hql="from NotePad where employee.employeeId="+employeeId +" order by notepadUpdatedDateTime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<NotePad> notepadList=(List<NotePad>)query.list();
		if(notepadList.isEmpty()){
			return null;
		}
		return notepadList;
	}

	@Transactional
	public void updateNotePad(NotePad notePad) {
		// TODO Auto-generated method stub
		notePad.setNotepadUpdatedDateTime(new Date());
		notePad=(NotePad)sessionFactory.getCurrentSession().merge(notePad);
		sessionFactory.getCurrentSession().update(notePad);
	}

	@Transactional
	public void deleteNotePadById(long id) {
		// TODO Auto-generated method stub
		NotePad notePad=fetchNotepadById(id);
		notePad=(NotePad)sessionFactory.getCurrentSession().merge(notePad);
		sessionFactory.getCurrentSession().delete(notePad);
	}

	@Transactional
	public void deleteNotepadByIdList(List<Long> idList) {
		// TODO Auto-generated method stub
		for(Long id:idList){
			NotePad notePad=fetchNotepadById(id);
			notePad=(NotePad)sessionFactory.getCurrentSession().merge(notePad);
			sessionFactory.getCurrentSession().delete(notePad);
		}
		
	}

}
