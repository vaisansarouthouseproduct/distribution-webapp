package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AssetDAO;
import com.bluesquare.rc.entities.AssetsCategory;
import com.bluesquare.rc.entities.Company;

@Repository("assetDAO")
@Component
public class AssetDAOImpl extends TokenHandler implements AssetDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	@Transactional
	public void saveAssetCategoryDetails(AssetsCategory assetsCategory) {
		// TODO Auto-generated method stub
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		assetsCategory.setCompany(company);
		sessionFactory.getCurrentSession().save(assetsCategory);
		
	}

	@Transactional
	public void updateAssetCategoryDetails(AssetsCategory assetsCategory) {
		// TODO Auto-generated method stub
		assetsCategory=(AssetsCategory)sessionFactory.getCurrentSession().merge(assetsCategory);
		sessionFactory.getCurrentSession().update(assetsCategory);
	}

	@Transactional
	public List<AssetsCategory> fetchAssetCategory() {
		// TODO Auto-generated method stub
		String hql = "from AssetsCategory where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<AssetsCategory> assetsCategoryList = (List<AssetsCategory>) query.list();

		if (assetsCategoryList.isEmpty()) {
			return null;
		}
		return assetsCategoryList;
	}

	@Transactional
	public AssetsCategory fetchAssetsCategoryById(long assetCategoryId) {
		
		String hql = "from AssetsCategory where payeeId= "+assetCategoryId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<AssetsCategory> assetsCategory=(List<AssetsCategory>)query.list();
		
		if(assetsCategory.isEmpty()){
			return null;
		}
		return assetsCategory.get(0);
	}
	

}
