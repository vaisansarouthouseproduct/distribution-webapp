package com.bluesquare.rc.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.OrderStatusDAO;
import com.bluesquare.rc.dao.PaymentDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BankDetailsForInvoice;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ProformaOrder;
import com.bluesquare.rc.entities.ProformaOrderProductDetails;
import com.bluesquare.rc.entities.ReturnCounterOrder;
import com.bluesquare.rc.entities.ReturnCounterOrderProducts;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.CategoryWiseAmountForBill;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.CounterReturnOrderModel;
import com.bluesquare.rc.models.InvoiceDetails;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.models.ProductListForBill;
import com.bluesquare.rc.models.ProformaOrderReport;
import com.bluesquare.rc.models.ReturnCounterRequest;
import com.bluesquare.rc.responseEntities.CounterOrderModel;
import com.bluesquare.rc.responseEntities.CounterOrderProductDetailsModel;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.CounterOrderIdGenerator;
import com.bluesquare.rc.utils.CreditNoteInvoiceNumberGenerate;
import com.bluesquare.rc.utils.DateDiff;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.InvoiceNumberGenerate;
import com.bluesquare.rc.utils.MathUtils;
import com.bluesquare.rc.utils.NumberToWordsConverter;
import com.bluesquare.rc.utils.ReturnCounterOrderIdGenerator;
import com.bluesquare.rc.utils.ReturnFromDeliveryBoyGenerator;

/**
 * <pre>
 * @author Sachin Pawar 23-05-2018 Code Documentation
 * provides Implementation for following methods of CounterOrderDAO
 * 1.saveCounterOrder(String roductDetailsList,
					  String businessNameId,
					  String paidAmount,
					  String balAmount,
					  String dueDate,String payType,
					  String paymentType,
					  String bankName,
					  String chequeNumber,
					  String chequeDate,
					  String custName,
					  String mobileNo,
					  String gstNo, 
					  long transportationId,
					  String vehicalNumber,
					  String docketNumber)
 *2.updateCounterOrderForEdit(String counterOrderId,
 							  String roductDetailsList,
							  String businessNameId,
							  String paidAmount,
							  String balAmount,
							  String refAmount,
							  String paymentSituation,
							  String dueDate,
							  String payType,
							  String paymentType,
							  String bankName,
							  String chequeNumber,
							  String chequeDate,
							  String custName,
							  String mobileNo,
							  String gstNo, 
							  long transportationId,
							  String vehicalNumber,
							  String docketNumber)
 * 3.fetchCounterBillPrintData(String counterOrderId)
 * 4.fetchCounterOrderProductDetails(String counterId)
 * 5.fetchCounterOrderProductDetailsForShowOrderDetails(String counterId)
 * 6.fetchCounterOrderByRange(String businessNameId,String range,String startDate,String endDate)
 * 7.showCounterOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate)
 * 8.fetchCounterOrderReport(String range,String startDate,String endDate)
 * 9.savePaymentCounter(PaymentCounter paymentCounter)
 * 10.fetchPaymentCounterListByCounterOrderId(String counterOrderId)
 * 11.fetchPaymentCounterReportListByCounterOrderId(String counterOrderId)
 * 12.fetchPaymentInfoByCounterOrderId(String counterOrderId)
 * 13.deleteCounterOrder(String counterOrderId)
 * 14.fetchPaymentCounterList(String startDate,String endDate,String range)
 * 15.fetchPaymentCounterListForChequeReport(String startDate,String endDate,String range)
 * 16.fetchPaymentCounterByPaymentCounterId(long paymentCounterId)
 * 17.updatePayment(PaymentCounter paymentCounter)
 * 18.deletePayment(long paymentId)
 * 19.defineChequeBounced(long paymentId)
 * 20.totalSaleAmountForProfitAndLoss(String startDate,String endDate)
 * 21.fetchInvoiceDetails(String startDate ,String endDate)
 * 22.fetchCounterOrderByBusinessNameId(String businessNameId)
 * 23.fetchCounterOrderByBusinessNameIdForView(String businessNameId)
 * 24.fetchCounterOrderByExternalCustomerForView(String year)
 * 25.setBadDebtsOfCounter(String counterOrderId)
 * 26.showCounterOrderReportByBusinessNameIdForBadDebts(String businessNameId)
 * 27.showCounterOrderReportByExternalCustomerCounterOrderIdForBadDebts(String counterOrderId)
 * 28.saveReturnCounterOrderDetails(ReturnCounterRequest returnCounterRequest)
 * 29.fetchReturnCounterOrder(long returnCounterOrderId)
 * 30.fetchReturnCounterOrderProductList(long returnCounterOrderId)
 * 31.fetchReturnCounterOrderProductModelList(long returnCounterOrderId)
 * 32.counterReturnOrderReport(String range,String startDate,String endDate)
 * 33.fetchReturnCounterBillPrintData(long returnCounterOrderId)
 * </pre>
 */
@Repository("counterOrderDAO")
@Component
public class CounterOrderDAOImpl extends TokenHandler implements CounterOrderDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	OrderStatusDAO orderStatusDAO;
	
	@Autowired
	CounterOrder counterOrder;
	
	@Autowired
	BusinessName businessName;
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	BusinessNameDAO businessNameDAO;
	
	@Autowired
	CounterOrderIdGenerator counterOrderIdGenerator; 
	
	@Autowired
	InvoiceNumberGenerate invoiceNumberGenerate;
	
	@Autowired 
	BranchDAO branchDAO;
	
	@Autowired
	PaymentDAO paymentDAO;
	

	@Autowired
	ReturnCounterOrderIdGenerator returnCounterOrderIdGenerator;
	
	@Autowired
	ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator;
	
	@Autowired
	CreditNoteInvoiceNumberGenerate creditNoteInvoiceNumberGenerate;
	
//	@Autowired
//	BankDetailsForInvoiceDAO bankDetailsForInvoiceDAO;
	
	/**
	 * <pre>
	 * saving counter order 
	 * in counter order can give added businesses or outside customer which details take at time of order 
	 * and Transportation details(Transport select,vehical number,docket number) also fill or not
	 * and may be payment partial/credit/full done same time
	 * and if payment give then same entry go into ledger
	 * 
	 * main CounterOrder
	 * ->count totalQuantity,totalAmount,totalAmountWithTax on basis of order product details
	 * ->business/CutomerInfo 
	 * ->set transportation details
	 * ->set logged user 
	 * ->set due date if credit/partial pay
	 * 
	 * sub order product details saving
	 * -> deduct order quantity from current quantity
	 * -> update deducted quantity in to daily stock details
	 * -> make new orderUsedBrand,orderUsedCategory,orderUsedProduct
	 * -> save it
	 * 
	 * payment section
	 * -> Instant Pay(Full pay)
	 *    ->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount 
	 * -> Credit Pay (Full Amount Due)
	 * 	  ->Due Date Set
	 * -> Partially Pay (Some Amount Paid)
	 * 	  ->Due Date Set
	 *    ->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount
	 * </pre>
	 */
	@Transactional
	public String saveCounterOrder(String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo, long transportationId,String vehicalNumber,
			String docketNumber,String transactionRefId,String comment,long paymentMethodId,String discountType,String discountAmount,double trasportationCharge) {

		counterOrder=new CounterOrder();
		String productDataList[]=roductDetailsList.split("-");
		double totalAmount=0,totalAmountWithTax=0;
		long totalQuantity=0;
		
		List<CounterOrderProductDetails> counterOrderProductDetailsList=new ArrayList<>();
		
		for(String productData : productDataList){
			String productDataSplit[]=productData.split(",");
			
			String productId=productDataSplit[0];
			String qty=productDataSplit[1];	
			String mrp=productDataSplit[2];
			String total=productDataSplit[3];
			String type=productDataSplit[4];
			
			String discount=productDataSplit[5];
			String discountAmt=productDataSplit[6];
			String discountTyp=productDataSplit[7];

			String discountOld=productDataSplit[8]; 
			String discountAmtOld=productDataSplit[9];
			String totalOld=productDataSplit[10];
			
			String discountTypOld=productDataSplit[11];
			
			String totalUP=productDataSplit[12];
			
			Product product=productDAO.fetchProductForWebApp(Long.parseLong(productId));
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()-Long.parseLong(qty));
			productDAO.Update(product);
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp), product.getCategories().getIgst());
			
			totalAmount+=Float.parseFloat(totalUP);
			totalAmountWithTax+=Float.parseFloat(total);
			totalQuantity+=Long.parseLong(qty);
			
			CounterOrderProductDetails counterOrderProductDetails=new CounterOrderProductDetails(); 
			counterOrderProductDetails.setDiscount(Double.parseDouble(discountAmt));
			counterOrderProductDetails.setDiscountPer(Double.parseDouble(discount));
			counterOrderProductDetails.setDiscountType(discountTyp);
		/*	counterOrderProductDetails.setDiscountOld(Double.parseDouble(discountAmtOld));
			counterOrderProductDetails.setDiscountPerOld(Double.parseDouble(discountOld));
			counterOrderProductDetails.setDiscountType(discountTyp);
			counterOrderProductDetails.setDiscountTypeOld(discountTypOld);*/
			
			
			Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			orderUsedBrand.setCompany(company);
			orderUsedBrand.setBranch(branch);
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst(),
					company,
					branch);
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					/*product.getProductImage(),
					product.getProductContentType(),*/
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity(),
					company,
					branch);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			counterOrderProductDetails.setProduct(orderUsedProduct);
			counterOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
		//	counterOrderProductDetails.setPurchaseAmountOld(Float.parseFloat(totalOld));
			counterOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
			counterOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
			counterOrderProductDetails.setType(type);
			
			counterOrderProductDetailsList.add(counterOrderProductDetails);
		}
		
		double discountAmtCut=0,discountPerCut=0;
		if(discountType.equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=Double.parseDouble(discountAmount);
			discountPerCut=(Double.parseDouble(discountAmount)/totalAmountWithTax)*100;			
		}else{
			discountPerCut=Double.parseDouble(discountAmount);
			discountAmtCut=(Double.parseDouble(discountAmount)*totalAmountWithTax)/100;
		}
		
		totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
		totalAmount=MathUtils.round(totalAmount, 2);
		
		totalAmountWithTax=totalAmountWithTax-discountAmtCut;
		totalAmountWithTax=MathUtils.round(totalAmountWithTax, 2);
		
		if(businessNameId.equals("0")){
			counterOrder.setCustomerGstNumber(gstNo);
			counterOrder.setCustomerMobileNumber(mobileNo);
			counterOrder.setCustomerName(custName);			
		}else{
			businessName=businessNameDAO.fetchBusinessForWebApp(businessNameId);
			counterOrder.setBusinessName(businessName);
		}
		//CounterOrderIdGenerator counterOrderIdGenerator=new CounterOrderIdGenerator(sessionFactory);
		counterOrder.setCounterOrderId(counterOrderIdGenerator.generate());
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");	
		counterOrder.setEmployeeGk(employeeDetails.getEmployee());
		
		counterOrder.setDateOfOrderTaken(new Date());
		counterOrder.setDiscount(Double.parseDouble(discountAmount));
		counterOrder.setDiscountType(discountType);
		
		//InvoiceNumberGenerate invoiceNumberGenerate=new InvoiceNumberGenerate(sessionFactory);
		counterOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {	
			if(paymentType.equals("PartialPay") || paymentType.equals("Credit")){
			counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
			}
		} catch (ParseException e) {}
		
		counterOrder.setPayStatus(false);
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));
		counterOrder.setTotalAmount(totalAmount);
		counterOrder.setTotalAmountWithTax(totalAmountWithTax);
		counterOrder.setTotalQuantity(totalQuantity);
		
		if(transportationId!=0){
			Transportation transportation=new Transportation();
			transportation.setId(transportationId);
			counterOrder.setTransportation(transportation);
			counterOrder.setVehicleNo(vehicalNumber);
			counterOrder.setTransportationCharges(trasportationCharge);
			if(docketNumber.trim().equals("")){
				docketNumber=null;
			}
			counterOrder.setDocketNo(docketNumber);
		}
		counterOrder.setBranch(branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds()));
		sessionFactory.getCurrentSession().save(counterOrder);
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			counterOrderProductDetails.setCounterOrder(counterOrder);
			sessionFactory.getCurrentSession().save(counterOrderProductDetails);
		}
		
		//payment section start
		PaymentCounter paymentCounter=new PaymentCounter();	
		
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);
		
		if(paymentType.equals("InstantPay")){
			paymentCounter.setCounterOrder(counterOrder);
			//paymentCounter.setBalanceAmount(0);
			//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
			paymentCounter.setCurrentAmountPaid(totalAmountWithTax);
			paymentCounter.setCurrentAmountRefund(0);
			//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
			paymentCounter.setLastDueDate(new Date());
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(payType);
			if(payType.equals("Cash")){				
			}else if(payType.equals("Cheque")){
				paymentCounter.setBankName(bankName);
				paymentCounter.setChequeNumber(chequeNumber);
				try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
			}else{
				paymentCounter.setTransactionRefNo(transactionRefId);
				paymentCounter.setComment(comment);
				paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
			}
			paymentCounter.setChequeClearStatus(true);
		
			sessionFactory.getCurrentSession().save(paymentCounter);
			String payMode="";
			if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
				payMode="Cash";
			}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
				payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
			}else{
				payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
			}
			//ledger entry create
			double credit=paymentCounter.getCurrentAmountPaid();
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			Ledger ledger=new Ledger(
										payMode,
										paymentCounter.getPaidDate(), 
										null, 
										paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
										(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
										0, 
										credit, 
										0, 
										paymentCounter,
										branch
									);
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
			
			counterOrder.setPayStatus(true);
			sessionFactory.getCurrentSession().update(counterOrder);
			
		}else if(paymentType.equals("Credit")){
			//No Payment
		}else{ //PartialPay
			paymentCounter.setCounterOrder(counterOrder);
			//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
			try { paymentCounter.setDueDate(dateFormat.parse(dueDate)); 
				  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
			} catch (Exception e) {}
			paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
			paymentCounter.setCurrentAmountRefund(0);
			//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(payType);
			if(payType.equals("Cash")){				
			}else if(payType.equals("Cheque")){
				paymentCounter.setBankName(bankName);
				paymentCounter.setChequeNumber(chequeNumber);
				try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
			}else{
				paymentCounter.setTransactionRefNo(transactionRefId);
				paymentCounter.setComment(comment);
				paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
			}
			paymentCounter.setChequeClearStatus(true);
			sessionFactory.getCurrentSession().save(paymentCounter);
			
			String payMode="";
			if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
				payMode="Cash";
			}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
				payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
			}else{
				payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
			}
			
			//ledger entry create
			double credit=paymentCounter.getCurrentAmountPaid();
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			Ledger ledger=new Ledger(
						payMode, 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						0, 
						credit, 
						0, 
						paymentCounter,
						branch
					);
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
		}/*
		if(paymentCounter.getCounterOrder().getBusinessName()!=null){
			BusinessName businessName=businessNameDAO.fetchBusinessForWebApp(paymentCounter.getCounterOrder().getBusinessName().getBusinessNameId());
			paymentCounter.getCounterOrder().setBusinessName(businessName);
		}*/
		
		
		//payment section end
		
		//update order used product update current quantity
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		return counterOrder.getCounterOrderId();
	}
	/**
	 * <pre>
	 * updating counter order 
	 * in counter order can give added businesses or outside customer which details take at time of order 
	 * and Transportation details(Transport select,vehical number,docket number) also fill or not
	 * and may be payment partial/credit/full done same time
	 * and if payment give then same entry go into ledger
	 * 
	 * main CounterOrder
	 * ->count totalQuantity,totalAmount,totalAmountWithTax on basis of order product details
	 * ->business/CutomerInfo 
	 * ->set transportation details
	 * ->set logged user 
	 * ->set due date if credit/partial pay
	 * 
	 * sub order product details saving
	 * -> delete old order product details and increase product quantity according it,Daily stock details changes  
	 * -> deduct order quantity from current quantity
	 * -> update deducted quantity in to daily stock details
	 * -> make new orderUsedBrand,orderUsedCategory,orderUsedProduct
	 * -> save it
	 * 
	 * payment section
	 * ->Refund
	 *   assumed all refund giving same time
	 * 	 save refund to payment as refund 
	 * ->Previous Partially / Full paid
	 * 	 1.if full paid
	 * 		-> Instant Pay(Full pay)
	 *    		->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount 
	 * 		-> Credit Pay (Full Amount Due)
	 * 	  		->Due Date Set
	 * 		-> Partially Pay (Some Amount Paid)
	 * 	  		->Due Date Set
	 *    	->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount
	 *   2.Partially paid
	 * 		-> Instant Pay(Full pay)
	 *    		->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount 
	 * 		-> Credit Pay (Full Amount Due)
	 * 	  		->Due Date Set
	 * 		-> Partially Pay (Some Amount Paid)
	 * 	  		->Due Date Set
	 *    	->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount
	 * ->Previous Credit/Full Amount Due
	 * 		-> Instant Pay(Full pay)
	 *    		->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount 
	 * 		-> Credit Pay (Full Amount Due)
	 * 	  		->Due Date Set
	 * 		-> Partially Pay (Some Amount Paid)
	 * 	  		->Due Date Set
	 *    	->Ledger Entry in Credit Side and Increase Company Balance According Paid Amount
	 * </pre>
	 */
	@Transactional
	public String updateCounterOrderForEdit(String counterOrderId,String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String refAmount,String paymentSituation,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo, long transportationId,String vehicalNumber
			,String docketNumber,String transactionRefId, String comment, long paymentMethodId,String discountType,String discountAmount,double trasportationCharge) {

		counterOrder=fetchCounterOrder(counterOrderId);
		
		if(transportationId!=0){
			Transportation transportation=new Transportation();
			transportation.setId(transportationId);
			counterOrder.setTransportation(transportation);
			counterOrder.setVehicleNo(vehicalNumber);
			if(docketNumber.trim().equals("")){
				docketNumber=null;
			}
			counterOrder.setDocketNo(docketNumber);
		}else{
			counterOrder.setTransportation(null);
			counterOrder.setVehicleNo(null);
			counterOrder.setDocketNo(null);
		}
		counterOrder.setTransportationCharges(trasportationCharge);
		counterOrder.setDiscount(Double.parseDouble(discountAmount));
		counterOrder.setDiscountType(discountType);

		double oldTotalAmountWithTax=counterOrder.getTotalAmountWithTax();
		List<CounterOrderProductDetails> counterOrderProductDetailsListOld=fetchCounterOrderProductDetails(counterOrderId);
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
				
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsListOld) {
			
			Product product=counterOrderProductDetails.getProduct().getProduct();
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()+counterOrderProductDetails.getPurchaseQuantity());
			productDAO.Update(product);
			
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), counterOrderProductDetails.getPurchaseQuantity(), true);
			
			orderDetailsDAO.deleteOrderUsedProduct(counterOrderProductDetails.getProduct());
			
			counterOrderProductDetails=(CounterOrderProductDetails)sessionFactory.getCurrentSession().merge(counterOrderProductDetails);
			sessionFactory.getCurrentSession().delete(counterOrderProductDetails);
		}
		
		String productDataList[]=roductDetailsList.split("-");
		double totalAmount=0,totalAmountWithTax=0;
		long totalQuantity=0;
		
		List<CounterOrderProductDetails> counterOrderProductDetailsList=new ArrayList<>();
		
		for(String productData : productDataList){
			String productDataSplit[]=productData.split(",");
			
			String productId=productDataSplit[0];
			String qty=productDataSplit[1];	
			String mrp=productDataSplit[2];
			String total=productDataSplit[3];
			String type=productDataSplit[4];
			
			String discount=productDataSplit[5];
			String discountAmt=productDataSplit[6];
			String discountTyp=productDataSplit[7];

			String discountOld=productDataSplit[8]; 
			String discountAmtOld=productDataSplit[9];
			String totalOld=productDataSplit[10];
			
			String discountTypOld=productDataSplit[11];
			
			String totalUP=productDataSplit[12];
			
			Product product=productDAO.fetchProductForWebApp(Long.parseLong(productId));
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()-Long.parseLong(qty));
			productDAO.Update(product);
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp), product.getCategories().getIgst());
			
			totalAmount+=Float.parseFloat(totalUP);
			totalAmountWithTax+=Float.parseFloat(total);
			totalQuantity+=Long.parseLong(qty);
			
			CounterOrderProductDetails counterOrderProductDetails=new CounterOrderProductDetails(); 
			counterOrderProductDetails.setDiscount(Double.parseDouble(discountAmt));
			counterOrderProductDetails.setDiscountPer(Double.parseDouble(discount));
			counterOrderProductDetails.setDiscountType(discountTyp);
		/*	counterOrderProductDetails.setDiscountOld(Double.parseDouble(discountAmtOld));
			counterOrderProductDetails.setDiscountPerOld(Double.parseDouble(discountOld));
			counterOrderProductDetails.setDiscountType(discountTyp);
			counterOrderProductDetails.setDiscountTypeOld(discountTypOld);*/
			
			
			Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			orderUsedBrand.setCompany(company);
			orderUsedBrand.setBranch(branch);
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst(),
					company,
					branch);
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					/*product.getProductImage(),
					product.getProductContentType(),*/
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity(),
					company,
					branch);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			counterOrderProductDetails.setProduct(orderUsedProduct);
			counterOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
			//counterOrderProductDetails.setPurchaseAmountOld(Float.parseFloat(totalOld));
			counterOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
			counterOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
			counterOrderProductDetails.setType(type);
			
			counterOrderProductDetailsList.add(counterOrderProductDetails);
		}
		
		double discountAmtCut=0,discountPerCut=0;
		if(discountType.equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=Double.parseDouble(discountAmount);
			discountPerCut=(Double.parseDouble(discountAmount)/totalAmountWithTax)*100;			
		}else{
			discountPerCut=Double.parseDouble(discountAmount);
			discountAmtCut=(Double.parseDouble(discountAmount)*totalAmountWithTax)/100;
		}
		
		totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
		totalAmount=MathUtils.round(totalAmount, 2);
		
		totalAmountWithTax=totalAmountWithTax-discountAmtCut;
		totalAmountWithTax=MathUtils.round(totalAmountWithTax, 2);
		
		if(businessNameId.equals("0")){
			counterOrder.setCustomerGstNumber(gstNo);
			counterOrder.setCustomerMobileNumber(mobileNo);
			counterOrder.setCustomerName(custName);	
			
			counterOrder.setBusinessName(null);
		}else{
			counterOrder.setCustomerGstNumber(null);
			counterOrder.setCustomerMobileNumber(null);
			counterOrder.setCustomerName(null);
			
			businessName=businessNameDAO.fetchBusinessForWebApp(businessNameId);
			counterOrder.setBusinessName(businessName);
		}
		
	/*	EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");	
		counterOrder.setEmployeeGk(employeeDetails.getEmployee());*/
		
		//counterOrder.setDateOfOrderTaken(new Date());
		
		/*InvoiceNumberGenerate invoiceNumberGenerate=new InvoiceNumberGenerate(sessionFactory);
		counterOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());*/
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {	
			if(!dueDate.equals("")) {
				if(paymentCounterList==null){
					counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
				}else{
					double totalPaid=0;
					for(PaymentCounter paymentCounter2: paymentCounterList){
						totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
					}
					if(totalPaid<totalAmountWithTax){
						counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
					}
				}
			}
		} catch (ParseException e) {}
		
		//counterOrder.setPayStatus(false);
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));
		counterOrder.setTotalAmount(totalAmount);
		counterOrder.setTotalAmountWithTax(totalAmountWithTax);
		counterOrder.setTotalQuantity(totalQuantity);
		
		counterOrder.setDiscount(Double.parseDouble(discountAmount));
		counterOrder.setDiscountType(discountType);
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			counterOrderProductDetails.setCounterOrder(counterOrder);
			sessionFactory.getCurrentSession().save(counterOrderProductDetails);
		}
		
		//payment section start
		boolean doLedgerEntry=true;
		PaymentCounter paymentCounter=new PaymentCounter();	
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);
		if(paymentSituation.equals("Refund")){
			if(Float.parseFloat(refAmount)!=0){
				paymentCounter.setCounterOrder(counterOrder);
				//paymentCounter.setBalanceAmount(0);
				//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
				paymentCounter.setCurrentAmountRefund(Float.parseFloat(refAmount));
				//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
				paymentCounter.setPaidDate(new Date());
				paymentCounter.setPayType(payType);
				if(payType.equals("Cash")){				
				}else if(payType.equals("Cheque")){
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(chequeNumber);
					try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
				}else{
					paymentCounter.setTransactionRefNo(transactionRefId);
					paymentCounter.setComment(comment);
					paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
				}
				if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
					paymentCounter.setChequeClearStatus(true);
					sessionFactory.getCurrentSession().save(paymentCounter);
				}
				counterOrder.setPayStatus(true);
				counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
				sessionFactory.getCurrentSession().update(counterOrder);
				
				doLedgerEntry=true;
			}else{
				doLedgerEntry=false;
			}
			/*counterOrder.setRefundAmount(Float.parseFloat(refAmount));
			counterOrder.setPaymentDueDate(null);
			counterOrder.setPayStatus(true);
			sessionFactory.getCurrentSession().update(counterOrder);*/
		}else{
			if(paymentCounterList!=null){
				double totalPaid=0;
				for(PaymentCounter paymentCounter2: paymentCounterList){
					totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
				}
				
				if(totalPaid==oldTotalAmountWithTax){
					double extraPay=totalAmountWithTax-oldTotalAmountWithTax;
					 	
					if(paymentType.equals("InstantPay")){
						
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(0);
						//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(extraPay);
						//paymentCounter.setTotalAmountPaid(totalAmountWithTax); 
						paymentCounter.setLastDueDate(new Date());							
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else if(payType.equals("Cheque")){
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}else{
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						counterOrder.setPayStatus(true);
						counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
						sessionFactory.getCurrentSession().update(counterOrder);
						
						doLedgerEntry=true;
						
					}else if(paymentType.equals("Credit")){
						//No Payment
						doLedgerEntry=false;
					}else{ //PartialPay
						
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
						try { paymentCounter.setDueDate(dateFormat.parse(dueDate));  
						  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
							} catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
						//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else if(payType.equals("Cash")){
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}else{
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						doLedgerEntry=true;
					}
					
					
				}else{
					
					double partiallyPaid=totalPaid;//paymentCounterList.get(0).getTotalAmountPaid();
					double extraPay=totalAmountWithTax-partiallyPaid;					
						
					if(paymentType.equals("InstantPay")){
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(0);						 
						paymentCounter.setLastDueDate(new Date());					
						paymentCounter.setCurrentAmountPaid(extraPay);
						//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else if(payType.equals("Cheque")){
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}else{
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						counterOrder.setPayStatus(true);
						counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
						sessionFactory.getCurrentSession().update(counterOrder);
						doLedgerEntry=true;
					}else if(paymentType.equals("Credit")){
						//No Payment
						doLedgerEntry=false;
					}else{ //PartialPay
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
						try { paymentCounter.setDueDate(dateFormat.parse(dueDate));  
						  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
							} catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
						//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else if(payType.equals("Cheque")){
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}else{
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}	
						doLedgerEntry=true;
					}
				}
				
				/*if(!paymentType.equals("Credit")){
					Collections.reverse(paymentCounterList);
					paymentCounterList.add(paymentCounter);
					double remainingAmt=totalAmountWithTax;
					for(PaymentCounter paymentCounter2 : paymentCounterList){
						if(paymentCounter2.getCurrentAmountPaid()==0){
							remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
							paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
						}else{
							remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
							paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
						}
						paymentCounter2.setBalanceAmount(remainingAmt);
						paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
						sessionFactory.getCurrentSession().update(paymentCounter2);
					}
				}*/
			}else{
						
				if(paymentType.equals("InstantPay")){
					paymentCounter.setCounterOrder(counterOrder);
					//paymentCounter.setBalanceAmount(0);
					//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
					paymentCounter.setCurrentAmountPaid(totalAmountWithTax);
					//paymentCounter.setTotalAmountPaid(totalAmountWithTax); 
					paymentCounter.setLastDueDate(new Date());
					paymentCounter.setPaidDate(new Date());
					paymentCounter.setPayType(payType);
					if(payType.equals("Cash")){				
					}else if(payType.equals("Cheque")){
						paymentCounter.setBankName(bankName);
						paymentCounter.setChequeNumber(chequeNumber);
						try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					}else{
						paymentCounter.setTransactionRefNo(transactionRefId);
						paymentCounter.setComment(comment);
						paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
					}
					if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
						paymentCounter.setChequeClearStatus(true);
						sessionFactory.getCurrentSession().save(paymentCounter);
					}
					counterOrder.setPayStatus(true);
					counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
					sessionFactory.getCurrentSession().update(counterOrder);
					doLedgerEntry=true;
				}else if(paymentType.equals("Credit")){
					//No Payment
					doLedgerEntry=false;
				}else{ //PartialPay
					paymentCounter.setCounterOrder(counterOrder);
					//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
					try { paymentCounter.setDueDate(dateFormat.parse(dueDate)); 
					  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
						 } catch (Exception e) {}
					paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
					//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
					paymentCounter.setPaidDate(new Date());
					paymentCounter.setPayType(payType);
					if(payType.equals("Cash")){				
					}else if(payType.equals("Cheque")){
						paymentCounter.setBankName(bankName);
						paymentCounter.setChequeNumber(chequeNumber);
						try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					}else{
						paymentCounter.setTransactionRefNo(transactionRefId);
						paymentCounter.setComment(comment);
						paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
					}
					if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
						paymentCounter.setChequeClearStatus(true);
						sessionFactory.getCurrentSession().save(paymentCounter);
					}		
					doLedgerEntry=true;
				}
			}
		}		
				
		if(doLedgerEntry){
			//ledger entry create		
			String payMode="";
			if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
				payMode="Cash";
			}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
				payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
			}else{
				payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
			}
			double credit=paymentCounter.getCurrentAmountPaid();
			double debit=paymentCounter.getCurrentAmountRefund();
			Ledger ledger=null;
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			if(debit==0){
				ledger=new Ledger(
						payMode, 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						0, 
						credit, 
						0, 
						paymentCounter,
						branch
					);
			}else{
				ledger=new Ledger(
						(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Refund", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						debit, 
						0, 
						0, 
						paymentCounter,
						branch
					);
			}
			
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
		}
		//payment section end
		
		//update order used product update current quantity
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		return counterOrder.getCounterOrderId();
	}
	/***
	 * <pre>
	 * make counter order bill details for generate invoice pdf
	 * @param counterOrderId
	 * @return BillPrintDataModel
	 * </pre>
	 */
	@Transactional
	public BillPrintDataModel fetchCounterBillPrintData(String counterOrderId)
	{
		
		//get counter order Details
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		
		//get counter order product list 
		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterOrderId);
		
		BusinessName businessName=counterOrder.getBusinessName();
		
		String invoiceNumber=counterOrder.getInvoiceNumber();
		String orderDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		String deliveryDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		
		List<ProductListForBill> productListForBillList=new ArrayList<>();
		
		float totalAmountWithoutTax=0;
		
		float cGSTAmount=0;
		float iGSTAmount=0;
		float sGSTAmount=0;
		
		long totalQuantity=0;
		float totalAmountWithTax=0;
		String totalAmountWithTaxInWord="";
		
		List<CategoryWiseAmountForBill> categoryWiseAmountForBills=new ArrayList<>();
		
		float totalAmount=0;
		float taxAmount=0;
		String taxAmountInWord="";
		
		float totalCGSTAmount=0;
		float totalIGSTAmount=0;
		float totalSGSTAmount=0;
		
		//DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		//DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		long srno=1;
		//create product wise details
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{ 
			long qty=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
			double total=qty*counterOrderProductDetails.getSellingRate();
			double discAmt=0;
			if(counterOrderProductDetails.getDiscountType().equals("Amount")){
				discAmt=counterOrderProductDetails.getDiscount();
			}else{
				discAmt=(total*counterOrderProductDetails.getDiscountPer())/100;
			}
			total=total-discAmt;
			
		//	float amountWithoutTax=0;
			CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(total, 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			CalculateProperTaxModel  calculateProperTaxModel2=productDAO.calculateProperAmountModel(counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{	
				//amountWithoutTax=calculateProperTaxModel.getUnitprice();
			
				
				
				/*
				float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
				float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
				float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
				
				float rate=calculateProperTaxModel.getUnitprice();*/
				long issuedQuantity=1;
				
				if(businessName!=null){
					if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
					{
						cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
						iGSTAmount+=0;
						sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
					}
					else
					{
						cGSTAmount+=0;
						iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
						sGSTAmount+=0;						
					}
				}else{//external customer - assumed intra tax type
					cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
					iGSTAmount+=0;
					sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
				}
			} 
			
			totalQuantity+=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
			
			float discountAmt=0;
			//double productamountWithTax=counterOrderProductDetails.getProduct().getRate()*counterOrderProductDetails.getPurchaseQuantity();
			//discountAmt=(float)(productamountWithTax*counterOrderProductDetails.getDiscountPer())/100;
			double totalGrossAmount=counterOrderProductDetails.getSellingRate()*(counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity());
			
			if(counterOrderProductDetails.getDiscountType().equals("Amount")){
				discountAmt=(float)counterOrderProductDetails.getDiscount();
			}else{
				discountAmt=(float)(totalGrossAmount*counterOrderProductDetails.getDiscountPer()/100);
			}
			
			float totalAmtWithTax=(float)(counterOrderProductDetails.getSellingRate()*counterOrderProductDetails.getPurchaseQuantity());
			productListForBillList.add(new ProductListForBill(
					String.valueOf(srno),
					counterOrderProductDetails.getProduct().getProductName()+(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "-(Free)" :""), 
					counterOrderProductDetails.getProduct().getCategories().getHsnCode(),
					String.valueOf(((long)counterOrderProductDetails.getProduct().getCategories().getIgst())),
					String.valueOf(counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity()), 
					(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0" :counterOrderProductDetails.getSellingRate()+" "), 
					String.valueOf(MathUtils.roundFromFloat((float)((counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity())*counterOrderProductDetails.getSellingRate()),2)),
					String.valueOf(MathUtils.roundFromFloat((float)discountAmt,2)),
					String.valueOf(MathUtils.roundFromFloat((float)counterOrderProductDetails.getDiscountPer(),2)),
					String.valueOf(MathUtils.roundFromFloat((float)totalGrossAmount-discountAmt,2))
					));
			totalAmountWithoutTax+=MathUtils.roundFromFloat((float)totalGrossAmount-discountAmt,2);  
			srno++;
			//double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
			//totalAmountWithTax+=amountWithoutTax+tax;	
			
		}
		//total amount with tax basis of product details
		for(ProductListForBill productListForBill2 :productListForBillList)
		{
			totalAmountWithTax+=Double.parseDouble(productListForBill2.getNetPayable());
		}
		totalAmountWithTax=MathUtils.roundFromFloat(totalAmountWithTax,2);
		cGSTAmount=MathUtils.roundFromFloat(cGSTAmount,3);
		iGSTAmount=MathUtils.roundFromFloat(iGSTAmount,2);
		sGSTAmount=MathUtils.roundFromFloat(sGSTAmount,3);
		
		//totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;
		
		//get unique category 
		Set<OrderUsedCategories> categoriesList=new HashSet<>();		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{
				categoriesList.add(counterOrderProductDetails.getProduct().getCategories());
			}
		}
		
		//category wise taxslab and amount find  
		Set<String> categoriesHsnCodeList=new HashSet<>();
		for(OrderUsedCategories categories: categoriesList)
		{
			String hsncode="";			
			float taxableValue=0;
			float cgstPercentage=0; 
			float igstPercentage=0;			
			float sgstPercentage=0;
			float cgstRate=0; 
			float igstRate=0;
			float sgstRate=0;
			
			cGSTAmount=0;
			iGSTAmount=0;
			sGSTAmount=0;
			
			boolean gotDuplicateHsnCode=false;
			
			for(String hsnCode : categoriesHsnCodeList)
			{
				if(categories.getHsnCode().equals(hsnCode))
				{
					gotDuplicateHsnCode=true;
				}
			}
			if(gotDuplicateHsnCode)
			{
				continue;
			}
			
			categoriesHsnCodeList.add(categories.getHsnCode());
			
			for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
			{
				if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					if(categories.getHsnCode().equals(counterOrderProductDetails.getProduct().getCategories().getHsnCode()))
					{
						long qty=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
						double total=qty*counterOrderProductDetails.getSellingRate();
						double discAmt=0;
						if(counterOrderProductDetails.getDiscountType().equals("Amount")){
							discAmt=counterOrderProductDetails.getDiscount();
						}else{
							discAmt=(total*counterOrderProductDetails.getDiscountPer())/100;
						}
						total=total-discAmt;
						
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(total, 
								   counterOrderProductDetails.getProduct().getCategories().getIgst());
						taxableValue+=calculateProperTaxModel.getUnitprice();
						
						/*float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
						float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
						float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
						float rate=calculateProperTaxModel.getUnitprice();*/
						long issuedQuantity=1;
						
						if(businessName!=null){
							if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
							{
								cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
								iGSTAmount+=0;
								sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
							}
							else
							{
								cGSTAmount+=0;
								iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
								sGSTAmount+=0;						
							}
						}else{//external customer - assumed intra tax type
							cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
							iGSTAmount+=0;
							sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
						}
						cgstPercentage=counterOrderProductDetails.getProduct().getCategories().getCgst();
						igstPercentage=counterOrderProductDetails.getProduct().getCategories().getIgst();	
						sgstPercentage=counterOrderProductDetails.getProduct().getCategories().getSgst();
						hsncode=counterOrderProductDetails.getProduct().getCategories().getHsnCode();
					}
				}
			}
			
			categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(
					hsncode, 
					MathUtils.roundFromFloat(taxableValue,2)+"", 
					MathUtils.roundFromFloat(cgstPercentage,3)+"", 
					MathUtils.roundFromFloat(cGSTAmount,3)+"", 
					MathUtils.roundFromFloat(igstPercentage,2)+"", 
					MathUtils.roundFromFloat(iGSTAmount,2)+"", 
					MathUtils.roundFromFloat(sgstPercentage,3)+"", 
					MathUtils.roundFromFloat(sGSTAmount,3)+""));
			
			totalAmount+=taxableValue;
			totalCGSTAmount+=cGSTAmount;
			totalSGSTAmount+=sGSTAmount;
			totalIGSTAmount+=iGSTAmount;			
		}
		taxAmount=(
				(MathUtils.roundFromFloat(totalSGSTAmount,3))+
				(MathUtils.roundFromFloat(totalCGSTAmount,3))+
				(MathUtils.roundFromFloat(totalIGSTAmount,2)));
		//total amount without tax in word
		//taxAmountInWord=NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount,2)));
		
		totalAmountWithTax=(MathUtils.roundFromFloat(totalAmountWithTax,2));
		//find round of amount
		/*float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax;
		float roundOf;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOf=0.0f;
			roundOfAmount=""+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else if(decimalAmount>=0.5)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="+"+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else
		{
			roundOf=decimalAmount;
			roundOfAmount="-"+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/	
		
		double discountAmtCut=0,discountPerCut=0;
		if(counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=counterOrder.getDiscount();
			discountPerCut=(counterOrder.getDiscount()/totalAmountWithTax)*100;			
		}else{
			discountPerCut=counterOrder.getDiscount();
			discountAmtCut=(counterOrder.getDiscount()*totalAmountWithTax)/100;
		}
		
		/*totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
		totalAmount=MathUtils.round(totalAmount, 0);*/
		
		double totalAmountWithTaxWithDisc=totalAmountWithTax-discountAmtCut;
		
		float roundOffAmount=(float)(MathUtils.round(totalAmountWithTaxWithDisc,0)-MathUtils.round(totalAmountWithTaxWithDisc,2));
		String roundAmt="";
		
		if(roundOffAmount>0){
			roundAmt="+"+roundOffAmount;
		}else if(roundOffAmount<0){
			roundAmt=roundOffAmount+"";
		}else{			
			roundAmt="0";
		}
		String totalAmountWithTaxRoundOff=MathUtils.round(totalAmountWithTaxWithDisc,0)+"";
		
		double totalAmountWithTaxWithDiscNonRoundOff=totalAmountWithTaxWithDisc;
		totalAmountWithTaxWithDisc=MathUtils.round(totalAmountWithTaxWithDisc, 0);
		/*if(decimalAmount>0.0f)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/
		
		totalAmountWithTaxInWord=NumberToWordsConverter.convertWithPaisa((float)MathUtils.round(totalAmountWithTaxWithDisc,0));
		
		
		discountAmtCut=MathUtils.round(discountAmtCut,2);
		discountPerCut=MathUtils.round(discountPerCut,2);
		totalAmountWithTaxWithDiscNonRoundOff=MathUtils.round(totalAmountWithTaxWithDiscNonRoundOff,2);
		
		/**
		 * calculate category wise block under discount and net amount 
		 */
		double totalAmountDisc=0;
		double totalCGSTAmountDisc=0;
		double totalIGSTAmountDisc=0;
		double totalSGSTAmountDisc=0;		
		double netTotalAmount=0;
		double netTotalCGSTAmount=0;
		double netTotalIGSTAmount=0;
		double netTotalSGSTAmount=0;
		if (counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
			
			 totalAmountDisc=((discountAmtCut/totalAmountWithTax)*totalAmount);
			 totalCGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalCGSTAmount);
			 totalIGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalIGSTAmount);
			 totalSGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalSGSTAmount);
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
			
		}else{
			 totalAmountDisc=(discountPerCut*totalAmount)/100;
			 totalCGSTAmountDisc=(discountPerCut*totalCGSTAmount)/100;
			 totalIGSTAmountDisc=(discountPerCut*totalIGSTAmount)/100;
			 totalSGSTAmountDisc=(discountPerCut*totalSGSTAmount)/100;
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
		}
		
		taxAmount = ((MathUtils.roundFromFloat((float)netTotalSGSTAmount, 3)) + (MathUtils.roundFromFloat((float)netTotalCGSTAmount, 3))
				+ (MathUtils.roundFromFloat((float)netTotalIGSTAmount, 2)));		
		taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));
		
			if(businessName!=null){
				return new BillPrintDataModel(
											counterOrderId,
											invoiceNumber, 
											"",
											"",
											orderDate, 
											null,
											businessName, 
											null,
											deliveryDate, 
											productListForBillList, 
											MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
											MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
											MathUtils.roundFromFloat(totalSGSTAmount,3)+"", 
											roundAmt,
											MathUtils.roundFromFloat(totalAmountWithoutTax,2)+"",
											String.valueOf(totalQuantity), 
											MathUtils.roundFromFloat(totalAmountWithTax,2)+"", 
											String.valueOf(discountAmtCut),
											String.valueOf(discountPerCut),
											String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
											String.valueOf(totalAmountWithTaxWithDisc),
											totalAmountWithTaxRoundOff,
											totalAmountWithTaxInWord, 
											categoryWiseAmountForBills, 
											MathUtils.roundFromFloat(totalAmount,2)+"", 
											taxAmountInWord, 
											MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
											MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
											MathUtils.roundFromFloat(totalSGSTAmount,3)+"",
											MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
											MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
											MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
											MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
											MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
											MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
											MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
											MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
											(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getTransportName(),
											(counterOrder.getVehicleNo()==null)?"NA":counterOrder.getVehicleNo(),
											(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getGstNo(),
											(counterOrder.getDocketNo()==null)?"NA":counterOrder.getDocketNo(),
													"",
											counterOrder.getTransportationCharges());
			}else {
				return new BillPrintDataModel(
						counterOrderId,
						invoiceNumber, 
						"",
						"",
						orderDate, 
						null,
						counterOrder.getCustomerName(),
						counterOrder.getCustomerMobileNumber(),
						counterOrder.getCustomerGstNumber(),
						counterOrder.getEmployeeGk(),
						deliveryDate, 
						productListForBillList, 
						MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
						MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
						MathUtils.roundFromFloat(totalSGSTAmount,3)+"", 
						roundAmt,
						MathUtils.roundFromFloat(totalAmountWithoutTax,2)+"",
						String.valueOf(totalQuantity), 
						MathUtils.roundFromFloat(totalAmountWithTax,2)+"", 
						String.valueOf(discountAmtCut),
						String.valueOf(discountPerCut),
						String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
						String.valueOf(totalAmountWithTaxWithDisc),
						totalAmountWithTaxRoundOff,
						totalAmountWithTaxInWord, 
						categoryWiseAmountForBills, 
						MathUtils.roundFromFloat(totalAmount,2)+"", 
						taxAmountInWord, 
						MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
						MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
						MathUtils.roundFromFloat(totalSGSTAmount,3)+"",
						MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
						MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
						MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
						MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
						MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
						MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
						MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
						MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
						(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getTransportName(),
						(counterOrder.getVehicleNo()==null)?"NA":counterOrder.getVehicleNo(),
						(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getGstNo(),
						(counterOrder.getDocketNo()==null)?"NA":counterOrder.getDocketNo(),
						"",
						counterOrder.getTransportationCharges());
			}
	}

	
	/***
	 * <pre>
	 * make counter order bill details for generate invoice pdf
	 * @param counterOrderId
	 * @return BillPrintDataModel
	 * </pre>
	 */
	@Transactional
	public BillPrintDataModel fetchCounterBillPrintDataForWhatsApp(String counterOrderId,long companyId,long branchId)
	{
		
		//get counter order Details
		CounterOrder counterOrder=fetchCounterOrderForWhatsApp(counterOrderId,companyId,branchId);
		
		//get counter order product list 
		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetailsForWhatsApp(counterOrderId,companyId,branchId);
		
		BusinessName businessName=counterOrder.getBusinessName();
		
		String invoiceNumber=counterOrder.getInvoiceNumber();
		String orderDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		String deliveryDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		
		List<ProductListForBill> productListForBillList=new ArrayList<>();
		
		float totalAmountWithoutTax=0;
		
		float cGSTAmount=0;
		float iGSTAmount=0;
		float sGSTAmount=0;
		
		long totalQuantity=0;
		float totalAmountWithTax=0;
		String totalAmountWithTaxInWord="";
		
		List<CategoryWiseAmountForBill> categoryWiseAmountForBills=new ArrayList<>();
		
		float totalAmount=0;
		float taxAmount=0;
		String taxAmountInWord="";
		
		float totalCGSTAmount=0;
		float totalIGSTAmount=0;
		float totalSGSTAmount=0;
		
		//DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		//DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		long srno=1;
		//create product wise details
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{ 
			long qty=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
			double total=qty*counterOrderProductDetails.getSellingRate();
			double discAmt=0;
			if(counterOrderProductDetails.getDiscountType().equals("Amount")){
				discAmt=counterOrderProductDetails.getDiscount();
			}else{
				discAmt=(total*counterOrderProductDetails.getDiscountPer())/100;
			}
			total=total-discAmt;
			
		//	float amountWithoutTax=0;
			CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(total, 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			CalculateProperTaxModel  calculateProperTaxModel2=productDAO.calculateProperAmountModel(counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{	
				//amountWithoutTax=calculateProperTaxModel.getUnitprice();
			
				
				
				/*
				float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
				float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
				float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
				
				float rate=calculateProperTaxModel.getUnitprice();*/
				long issuedQuantity=1;
				
				if(businessName!=null){
					if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
					{
						cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
						iGSTAmount+=0;
						sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
					}
					else
					{
						cGSTAmount+=0;
						iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
						sGSTAmount+=0;						
					}
				}else{//external customer - assumed intra tax type
					cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
					iGSTAmount+=0;
					sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
				}
			} 
			
			totalQuantity+=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
			
			float discountAmt=0;
			//double productamountWithTax=counterOrderProductDetails.getProduct().getRate()*counterOrderProductDetails.getPurchaseQuantity();
			//discountAmt=(float)(productamountWithTax*counterOrderProductDetails.getDiscountPer())/100;
			double totalGrossAmount=counterOrderProductDetails.getSellingRate()*(counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity());
			
			if(counterOrderProductDetails.getDiscountType().equals("Amount")){
				discountAmt=(float)counterOrderProductDetails.getDiscount();
			}else{
				discountAmt=(float)(totalGrossAmount*counterOrderProductDetails.getDiscountPer()/100);
			}
			
			float totalAmtWithTax=(float)(counterOrderProductDetails.getSellingRate()*counterOrderProductDetails.getPurchaseQuantity());
			productListForBillList.add(new ProductListForBill(
					String.valueOf(srno),
					counterOrderProductDetails.getProduct().getProductName()+(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "-(Free)" :""), 
					counterOrderProductDetails.getProduct().getCategories().getHsnCode(),
					String.valueOf(((long)counterOrderProductDetails.getProduct().getCategories().getIgst())),
					String.valueOf(counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity()), 
					(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0" :counterOrderProductDetails.getSellingRate()+" "), 
					String.valueOf(MathUtils.roundFromFloat((float)((counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity())*counterOrderProductDetails.getSellingRate()),2)),
					String.valueOf(MathUtils.roundFromFloat((float)discountAmt,2)),
					String.valueOf(MathUtils.roundFromFloat((float)counterOrderProductDetails.getDiscountPer(),2)),
					String.valueOf(MathUtils.roundFromFloat((float)totalGrossAmount-discountAmt,2))
					));
			totalAmountWithoutTax+=MathUtils.roundFromFloat((float)totalGrossAmount-discountAmt,2);  
			srno++;
			//double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
			//totalAmountWithTax+=amountWithoutTax+tax;	
			
		}
		//total amount with tax basis of product details
		for(ProductListForBill productListForBill2 :productListForBillList)
		{
			totalAmountWithTax+=Double.parseDouble(productListForBill2.getNetPayable());
		}
		totalAmountWithTax=MathUtils.roundFromFloat(totalAmountWithTax,2);
		cGSTAmount=MathUtils.roundFromFloat(cGSTAmount,3);
		iGSTAmount=MathUtils.roundFromFloat(iGSTAmount,2);
		sGSTAmount=MathUtils.roundFromFloat(sGSTAmount,3);
		
		//totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;
		
		//get unique category 
		Set<OrderUsedCategories> categoriesList=new HashSet<>();		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{
				categoriesList.add(counterOrderProductDetails.getProduct().getCategories());
			}
		}
		
		//category wise taxslab and amount find  
		Set<String> categoriesHsnCodeList=new HashSet<>();
		for(OrderUsedCategories categories: categoriesList)
		{
			String hsncode="";			
			float taxableValue=0;
			float cgstPercentage=0; 
			float igstPercentage=0;			
			float sgstPercentage=0;
			float cgstRate=0; 
			float igstRate=0;
			float sgstRate=0;
			
			cGSTAmount=0;
			iGSTAmount=0;
			sGSTAmount=0;
			
			boolean gotDuplicateHsnCode=false;
			
			for(String hsnCode : categoriesHsnCodeList)
			{
				if(categories.getHsnCode().equals(hsnCode))
				{
					gotDuplicateHsnCode=true;
				}
			}
			if(gotDuplicateHsnCode)
			{
				continue;
			}
			
			categoriesHsnCodeList.add(categories.getHsnCode());
			
			for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
			{
				if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					if(categories.getHsnCode().equals(counterOrderProductDetails.getProduct().getCategories().getHsnCode()))
					{
						long qty=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
						double total=qty*counterOrderProductDetails.getSellingRate();
						double discAmt=0;
						if(counterOrderProductDetails.getDiscountType().equals("Amount")){
							discAmt=counterOrderProductDetails.getDiscount();
						}else{
							discAmt=(total*counterOrderProductDetails.getDiscountPer())/100;
						}
						total=total-discAmt;
						
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(total, 
								   counterOrderProductDetails.getProduct().getCategories().getIgst());
						taxableValue+=calculateProperTaxModel.getUnitprice();
						
						/*float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
						float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
						float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
						float rate=calculateProperTaxModel.getUnitprice();*/
						long issuedQuantity=1;
						
						if(businessName!=null){
							if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
							{
								cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
								iGSTAmount+=0;
								sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
							}
							else
							{
								cGSTAmount+=0;
								iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
								sGSTAmount+=0;						
							}
						}else{//external customer - assumed intra tax type
							cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
							iGSTAmount+=0;
							sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
						}
						cgstPercentage=counterOrderProductDetails.getProduct().getCategories().getCgst();
						igstPercentage=counterOrderProductDetails.getProduct().getCategories().getIgst();	
						sgstPercentage=counterOrderProductDetails.getProduct().getCategories().getSgst();
						hsncode=counterOrderProductDetails.getProduct().getCategories().getHsnCode();
					}
				}
			}
			
			categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(
					hsncode, 
					MathUtils.roundFromFloat(taxableValue,2)+"", 
					MathUtils.roundFromFloat(cgstPercentage,3)+"", 
					MathUtils.roundFromFloat(cGSTAmount,3)+"", 
					MathUtils.roundFromFloat(igstPercentage,2)+"", 
					MathUtils.roundFromFloat(iGSTAmount,2)+"", 
					MathUtils.roundFromFloat(sgstPercentage,3)+"", 
					MathUtils.roundFromFloat(sGSTAmount,3)+""));
			
			totalAmount+=taxableValue;
			totalCGSTAmount+=cGSTAmount;
			totalSGSTAmount+=sGSTAmount;
			totalIGSTAmount+=iGSTAmount;			
		}
		taxAmount=(
				(MathUtils.roundFromFloat(totalSGSTAmount,3))+
				(MathUtils.roundFromFloat(totalCGSTAmount,3))+
				(MathUtils.roundFromFloat(totalIGSTAmount,2)));
		//total amount without tax in word
		//taxAmountInWord=NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount,2)));
		
		totalAmountWithTax=(MathUtils.roundFromFloat(totalAmountWithTax,2));
		//find round of amount
		/*float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax;
		float roundOf;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOf=0.0f;
			roundOfAmount=""+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else if(decimalAmount>=0.5)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="+"+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else
		{
			roundOf=decimalAmount;
			roundOfAmount="-"+MathUtils.roundFromFloat(roundOf,2);
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/	
		
		double discountAmtCut=0,discountPerCut=0;
		if(counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=counterOrder.getDiscount();
			discountPerCut=(counterOrder.getDiscount()/totalAmountWithTax)*100;			
		}else{
			discountPerCut=counterOrder.getDiscount();
			discountAmtCut=(counterOrder.getDiscount()*totalAmountWithTax)/100;
		}
		
		/*totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
		totalAmount=MathUtils.round(totalAmount, 0);*/
		
		double totalAmountWithTaxWithDisc=totalAmountWithTax-discountAmtCut;
		
		float roundOffAmount=(float)(MathUtils.round(totalAmountWithTaxWithDisc,0)-MathUtils.round(totalAmountWithTaxWithDisc,2));
		String roundAmt="";
		
		if(roundOffAmount>0){
			roundAmt="+"+roundOffAmount;
		}else if(roundOffAmount<0){
			roundAmt=roundOffAmount+"";
		}else{			
			roundAmt="0";
		}
		String totalAmountWithTaxRoundOff=MathUtils.round(totalAmountWithTaxWithDisc,0)+"";
		
		double totalAmountWithTaxWithDiscNonRoundOff=totalAmountWithTaxWithDisc;
		totalAmountWithTaxWithDisc=MathUtils.round(totalAmountWithTaxWithDisc, 0);
		/*if(decimalAmount>0.0f)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/
		
		totalAmountWithTaxInWord=NumberToWordsConverter.convertWithPaisa((float)MathUtils.round(totalAmountWithTaxWithDisc,0));
		
		
		discountAmtCut=MathUtils.round(discountAmtCut,2);
		discountPerCut=MathUtils.round(discountPerCut,2);
		totalAmountWithTaxWithDiscNonRoundOff=MathUtils.round(totalAmountWithTaxWithDiscNonRoundOff,2);
		
		/**
		 * calculate category wise block under discount and net amount 
		 */
		double totalAmountDisc=0;
		double totalCGSTAmountDisc=0;
		double totalIGSTAmountDisc=0;
		double totalSGSTAmountDisc=0;		
		double netTotalAmount=0;
		double netTotalCGSTAmount=0;
		double netTotalIGSTAmount=0;
		double netTotalSGSTAmount=0;
		if (counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
			
			 totalAmountDisc=((discountAmtCut/totalAmountWithTax)*totalAmount);
			 totalCGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalCGSTAmount);
			 totalIGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalIGSTAmount);
			 totalSGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalSGSTAmount);
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
			
		}else{
			 totalAmountDisc=(discountPerCut*totalAmount)/100;
			 totalCGSTAmountDisc=(discountPerCut*totalCGSTAmount)/100;
			 totalIGSTAmountDisc=(discountPerCut*totalIGSTAmount)/100;
			 totalSGSTAmountDisc=(discountPerCut*totalSGSTAmount)/100;
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
		}
		
		taxAmount = ((MathUtils.roundFromFloat((float)netTotalSGSTAmount, 3)) + (MathUtils.roundFromFloat((float)netTotalCGSTAmount, 3))
				+ (MathUtils.roundFromFloat((float)netTotalIGSTAmount, 2)));		
		taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));
		
			if(businessName!=null){
				return new BillPrintDataModel(
											counterOrderId,
											invoiceNumber, 
											"",
											"",
											orderDate, 
											null,
											businessName, 
											null,
											deliveryDate, 
											productListForBillList, 
											MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
											MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
											MathUtils.roundFromFloat(totalSGSTAmount,3)+"", 
											roundAmt,
											MathUtils.roundFromFloat(totalAmountWithoutTax,2)+"",
											String.valueOf(totalQuantity), 
											MathUtils.roundFromFloat(totalAmountWithTax,2)+"", 
											String.valueOf(discountAmtCut),
											String.valueOf(discountPerCut),
											String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
											String.valueOf(totalAmountWithTaxWithDisc),
											totalAmountWithTaxRoundOff,
											totalAmountWithTaxInWord, 
											categoryWiseAmountForBills, 
											MathUtils.roundFromFloat(totalAmount,2)+"", 
											taxAmountInWord, 
											MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
											MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
											MathUtils.roundFromFloat(totalSGSTAmount,3)+"",
											MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
											MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
											MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
											MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
											MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
											MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
											MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
											MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
											(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getTransportName(),
											(counterOrder.getVehicleNo()==null)?"NA":counterOrder.getVehicleNo(),
											(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getGstNo(),
											(counterOrder.getDocketNo()==null)?"NA":counterOrder.getDocketNo(),
													"",
											counterOrder.getTransportationCharges());
			}else {
				return new BillPrintDataModel(
						counterOrderId,
						invoiceNumber, 
						"",
						"",
						orderDate, 
						null,
						counterOrder.getCustomerName(),
						counterOrder.getCustomerMobileNumber(),
						counterOrder.getCustomerGstNumber(),
						counterOrder.getEmployeeGk(),
						deliveryDate, 
						productListForBillList, 
						MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
						MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
						MathUtils.roundFromFloat(totalSGSTAmount,3)+"", 
						roundAmt,
						MathUtils.roundFromFloat(totalAmountWithoutTax,2)+"",
						String.valueOf(totalQuantity), 
						MathUtils.roundFromFloat(totalAmountWithTax,2)+"", 
						String.valueOf(discountAmtCut),
						String.valueOf(discountPerCut),
						String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
						String.valueOf(totalAmountWithTaxWithDisc),
						totalAmountWithTaxRoundOff,
						totalAmountWithTaxInWord, 
						categoryWiseAmountForBills, 
						MathUtils.roundFromFloat(totalAmount,2)+"", 
						taxAmountInWord, 
						MathUtils.roundFromFloat(totalCGSTAmount,3)+"", 
						MathUtils.roundFromFloat(totalIGSTAmount,2)+"", 
						MathUtils.roundFromFloat(totalSGSTAmount,3)+"",
						MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
						MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
						MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
						MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
						MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
						MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
						MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
						MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
						(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getTransportName(),
						(counterOrder.getVehicleNo()==null)?"NA":counterOrder.getVehicleNo(),
						(counterOrder.getTransportation()==null)?"NA":counterOrder.getTransportation().getGstNo(),
						(counterOrder.getDocketNo()==null)?"NA":counterOrder.getDocketNo(),
						"",
						counterOrder.getTransportationCharges());
			}
	}

	@Transactional
	public void updateCounterOrder(CounterOrder counterOrder) {
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}

	@Transactional
	public CounterOrder fetchCounterOrder(String counterId) {
		// TODO Auto-generated method stub
		String hql="from CounterOrder where counterOrderId='"+counterId+"'"+
					" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		if(counterOrderList.isEmpty()){
			return null;
		}
		return counterOrderList.get(0);
	}
	
	@Transactional
	public CounterOrder fetchCounterOrderForWhatsApp(String counterId, long companyId,long branchId) {
		// TODO Auto-generated method stub
		String hql="from CounterOrder where counterOrderId='"+counterId+"'"+
					" and employeeGk.company.companyId in ("+companyId+")"+
					" and branch.branchId in ("+branchId+") ";
		//hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		//hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		if(counterOrderList.isEmpty()){
			return null;
		}
		return counterOrderList.get(0);
	}
	
	@Transactional
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetails(String counterId) {
		String hql="from CounterOrderProductDetails where counterOrder.counterOrderId='"+counterId+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrderProductDetails> CounterOrderProductDetailsList=(List<CounterOrderProductDetails>)query.list();
		if(CounterOrderProductDetailsList.isEmpty()){
			return null;
		}
		
		return CounterOrderProductDetailsList;
	}
	
	@Transactional
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetailsForWhatsApp(String counterId,long companyId,long branchId) {
		String hql="from CounterOrderProductDetails where counterOrder.counterOrderId='"+counterId+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+companyId;
		hql+=" and counterOrder.branch.branchId in ("+branchId+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrderProductDetails> CounterOrderProductDetailsList=(List<CounterOrderProductDetails>)query.list();
		if(CounterOrderProductDetailsList.isEmpty()){
			return null;
		}
		
		return CounterOrderProductDetailsList;
	}
	
	
	/**
	 * <pre>
	 * fetch couter order product details except counter order object
	 * </pre>
	 * @param counterId
	 * @return {@link CounterOrderProductDetailsModel} list
	 */
	@Transactional
	public List<CounterOrderProductDetailsModel> fetchCounterOrderProductDetailsModelList(String counterId) {
		String hql="from CounterOrderProductDetails where counterOrder.counterOrderId='"+counterId+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrderProductDetails> counterOrderProductDetailsList=(List<CounterOrderProductDetails>)query.list();
		if(counterOrderProductDetailsList.isEmpty()){
			return null;
		}
		List<CounterOrderProductDetailsModel> counterOrderProductDetailsModelList=new ArrayList<>();
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			counterOrderProductDetailsModelList.add(new CounterOrderProductDetailsModel(
					counterOrderProductDetails.getProduct(), 
					counterOrderProductDetails.getPurchaseQuantity(), 
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getPurchaseAmount(), 
					counterOrderProductDetails.getType()));
		}
		return counterOrderProductDetailsModelList;
	}
	
	@Transactional
	public List<OrderProductDetailListForWebApp> fetchCounterOrderProductDetailsForShowOrderDetails(String counterId) {

		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterId);
		if(counterOrderProductDetailsList==null){
			return null;
		}
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebAppList=new ArrayList<>();
		long srno=1;
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
		/*	
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			float baseAmount=(float)counterOrderProductDetails.getSellingRate()*counterOrderProductDetails.getPurchaseQuantity();
			//float totalAmountWithoutTax=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
			
			CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(counterOrderProductDetails.getPurchaseAmount(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			CalculateProperTaxModel  calculateProperTaxModel2=productDAO.calculateProperAmountModel(counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			
			float discountAmt=0;
			//double productamountWithTax=counterOrderProductDetails.getProduct().getRate()*counterOrderProductDetails.getPurchaseQuantity();
			//discountAmt=(float)(productamountWithTax*counterOrderProductDetails.getDiscountPer())/100;
			//discountAmt=(MathUtils.roundFromFloat(calculateProperTaxModel2.getUnitprice(),2)*counterOrderProductDetails.getPurchaseQuantity())-calculateProperTaxModel.getUnitprice();
			double totalAmount=counterOrderProductDetails.getSellingRate()*counterOrderProductDetails.getPurchaseQuantity();
			discountAmt=(float)(totalAmount*counterOrderProductDetails.getDiscountPer()/100);*/
			
			long qty=counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity();
			double total=qty*counterOrderProductDetails.getSellingRate();
			double discAmt=0;
			if(counterOrderProductDetails.getDiscountType().equals("Amount")){
				discAmt=counterOrderProductDetails.getDiscount();
			}else{
				discAmt=(total*counterOrderProductDetails.getDiscountPer())/100;
			}
			total=total-discAmt;
				
			orderProductDetailListForWebAppList.add(new OrderProductDetailListForWebApp(
					srno,
					counterOrderProductDetails.getCounterOrder().getCounterOrderId(), 
					counterOrderProductDetails.getProduct().getCategories().getCategoryName(),
					counterOrderProductDetails.getProduct().getBrand().getName(),
					counterOrderProductDetails.getProduct().getProductName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId()).getName(), 
					counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(), 
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getPurchaseQuantity(), 
					counterOrderProductDetails.getReturnQuantity(),
					counterOrderProductDetails.getDiscountPer(), 
					discAmt,
					total,
					counterOrderProductDetails.getSellingRate()*qty,
					counterOrderProductDetails.getCounterOrder().getDateOfOrderTaken(), 
					counterOrderProductDetails.getType(),
					""));
			srno++;
		}
		return orderProductDetailListForWebAppList;
	}
	/**
	 * <pre>
	 * fetch counter order by businessNameId, range, startDate, endDate
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate 
	 * @return  CounterOrder list
	 * </pre>
	 */
	@Transactional
	public List<CounterOrder> fetchCounterOrderByRange(String businessNameId,String range,String startDate,String endDate){
		
		//if gatekeeper logged then show his area counter order only 
		//according added by user areas  
		String areaListArray="";
		if(getAppLoggedEmployeeId()!=0){
			List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(getAppLoggedEmployeeId());		
			
			//List<Area> areaList=new ArrayList<>();
			for(EmployeeAreaList employeeAreaList: employeeAreaLists)
			{
				//areaList.add(employeeAreaList.getArea());
				areaListArray+=employeeAreaList.getArea().getAreaId()+",";
			}
			areaListArray=areaListArray.substring(0, areaListArray.length()-1);
		}
		
		String hql="";
		Calendar cal = Calendar.getInstance();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("today")) {
			hql="from CounterOrder where date(dateOfOrderTaken) = date(CURRENT_DATE()) ";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from CounterOrder where date(dateOfOrderTaken) = '"+dateFormat.format(cal.getTime())+"' ";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from CounterOrder where date(dateOfOrderTaken) > '"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("currentMonth")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("lastMonth")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLast3MonthLastDate()+"') ";
		}
		else if (range.equals("last6Months")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLast6MonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+startDate+"' and date(dateOfOrderTaken) <= '"+endDate+"') ";
		}
		else if (range.equals("pickDate")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) = '"+startDate+"') ";
		}
		else if (range.equals("viewAll")) {
			hql="from CounterOrder where 1=1 ";
		}/*else if (range.equals("TopProducts")) {
			hql="from CounterOrder where 1=1 ";
		}*/
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		//if gatekeeper logged
		if(getAppLoggedEmployeeId()!=0){
			if(businessNameId!=null){
				hql+=" and employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList where area.areaId in ("+areaListArray+")) "+
						 " and businessName.businessNameId='"+businessNameId+"'"+
						 "  order by dateOfOrderTaken desc";
			}else{
				hql+=" and employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList where area.areaId in ("+areaListArray+")) "+
						 "  order by dateOfOrderTaken desc";
			}
		}else{ //if company/admin logged
			if(businessNameId!=null){
				hql+=" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
					 " and businessName.businessNameId='"+businessNameId+"'"+
					 "  order by dateOfOrderTaken desc";
			}else{
				hql+=" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
						 "  order by dateOfOrderTaken desc";
			}
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		if(counterOrderList.isEmpty()){
			return null;
		}
		return counterOrderList;
	}
	/**
	 * <pre>
	 * counter order list for see businessName wise orders 
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showCounterOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate)
	{
		
		List<CounterOrder> counterOrderlist=fetchCounterOrderByRange(businessNameId, range, startDate, endDate);
		if(counterOrderlist==null)
		{
			return null;
		}
		
		//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(CounterOrder counterOrder : counterOrderlist){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss");
			
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			orderStatusSM="--";
			orderStatusSMDate="--";
			orderStatusSMTime="--";
			orderStatusGK=counterOrder.getOrderStatus().getStatus();
			orderStatusGKDate=simpleDateFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusGKTime= simpleTimeFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusDB = "--";
			orderStatusDBDate = "--";
			orderStatusDBTime= "--";
			amount=counterOrder.getTotalAmount();
			amountWithTax=counterOrder.getTotalAmountWithTax();
			quantity=counterOrder.getTotalQuantity();
					
			list2.add(new OrderReportList(srno,
					counterOrder.getCounterOrderId(),
					amount,
					amountWithTax,
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getArea().getName():"NA",
					quantity, 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getShopName():counterOrder.getCustomerName(), 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getBusinessNameId():"NA", 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(),
					(counterOrder.getPaymentDueDate()!=null)?DateDiff.findDifferenceInTwoDatesByDay(counterOrder.getDateOfOrderTaken(), counterOrder.getPaymentDueDate()):0, 
					counterOrder.getDateOfOrderTaken(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							Constants.ORDER_STATUS_DELIVERED));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * fetch counter order report for See Counter Order Details,Edit,Delete,Make Payment
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return CounterOrderReport list
	 * </pre>
	 */
	@Transactional
	public List<CounterOrderReport> fetchCounterOrderReport(String range,String startDate,String endDate){
	
		List<CounterOrder> counterOrderlist=fetchCounterOrderByRange(null, range, startDate, endDate);
		if(counterOrderlist==null)
		{
			return null;
		}
		
		List<CounterOrderReport> counterOrderReportList=new ArrayList<>();
		int srno=01;
		for(CounterOrder counterOrder : counterOrderlist){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			
			String customerName;
			String mobileNumber;
			if(counterOrder.getBusinessName()==null){
				customerName=counterOrder.getCustomerName();
				mobileNumber=counterOrder.getCustomerMobileNumber();
			}else{
				customerName=counterOrder.getBusinessName().getShopName();
				mobileNumber=counterOrder.getBusinessName().getContact().getMobileNumber();
			}
			
			String paymentDate;
			if(counterOrder.getPaymentDueDate()==null){
				paymentDate="NA";
			}else{
				paymentDate=dateFormat.format(counterOrder.getPaymentDueDate());
			}
			
			String paymentStatus;
			List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
			if(paymentCounterList==null){
				paymentStatus="UnPaid";
			}else{
				double totalAmountPaid=0;
				for(PaymentCounter paymentCounter: paymentCounterList){
					totalAmountPaid+=paymentCounter.getCurrentAmountPaid()-paymentCounter.getCurrentAmountRefund();
				}
				totalAmountPaid=(totalAmountPaid>0)?totalAmountPaid:0;
				if(totalAmountPaid>=counterOrder.getTotalAmountWithTax()){
					paymentStatus="Paid";
				}else if(totalAmountPaid==0){
					paymentStatus="UnPaid";
				}
				else{
					paymentStatus="Partially Paid";
				}
			}
			String orderTakenDate=dateFormat.format(counterOrder.getDateOfOrderTaken());
			String orderTakenTime=timeFormat.format(counterOrder.getDateOfOrderTaken());
			
			boolean editable=false;
			Calendar calendarEndDate=Calendar.getInstance();		
			calendarEndDate.set(Calendar.DAY_OF_MONTH, calendarEndDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			calendarEndDate=removeTime(calendarEndDate);
			System.out.println(calendarEndDate.getTime());
			
			Calendar calCounterOrder=Calendar.getInstance();
			calCounterOrder.setTime(counterOrder.getDateOfOrderTaken());	
			calCounterOrder=removeTime(calCounterOrder);
			System.out.println(counterOrder.getCounterOrderId());
			
			List<ReturnCounterOrder> returnCounterOrderList=fetchReturnCounterOrderByCounterOrderId(counterOrder.getCounterOrderId());
			
			int currentMonthNumber=Calendar.getInstance().get(Calendar.MONTH);
			int currentYearNumber=Calendar.getInstance().get(Calendar.YEAR);
			
			int orderMonthNumber=calCounterOrder.get(Calendar.MONTH);
			int orderYearNumber=calCounterOrder.get(Calendar.YEAR);
			
			if((calCounterOrder.before(calendarEndDate) || calCounterOrder.equals(calendarEndDate)) 
					&& currentMonthNumber==orderMonthNumber 
					&& currentYearNumber==orderYearNumber
					&& returnCounterOrderList==null){
				editable=true;
			}
			
			counterOrderReportList.add(new CounterOrderReport(srno,
															counterOrder.getCounterOrderId(), 
															employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(), 
															customerName, 
															counterOrder.getTotalAmount(), 
															counterOrder.getTotalAmountWithTax(),
															counterOrder.getTotalQuantity(),
															counterOrder.getOrderStatus().getStatus(),
															paymentDate, 
															paymentStatus, 
															orderTakenDate, 
															orderTakenTime,
															(counterOrder.getBusinessName()==null),
													        editable,
													        mobileNumber));
			srno++;
		}
		
		return counterOrderReportList;
	}	
	public static Calendar removeTime(Calendar cal) {  
          
        cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);  
        return cal; 
    }
	
	@Transactional
	public List<ReturnCounterOrder> fetchReturnCounterOrderByCounterOrderId(String counterOrderId){
		String hql="from ReturnCounterOrder where counterOrder.counterOrderId='"+counterOrderId+"'";
		
		String areaListArray="";
		if(getAppLoggedEmployeeId()!=0){
			List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(getAppLoggedEmployeeId());		
			
			//List<Area> areaList=new ArrayList<>();
			for(EmployeeAreaList employeeAreaList: employeeAreaLists)
			{
				//areaList.add(employeeAreaList.getArea());
				areaListArray+=employeeAreaList.getArea().getAreaId()+",";
			}
			areaListArray=areaListArray.substring(0, areaListArray.length()-1);
		}
				
		//if gatekeeper logged
		if(getAppLoggedEmployeeId()!=0){
			hql+=" and counterOrder.employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList "+
						 " where area.areaId in ("+areaListArray+")) "+
						 " and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+") " +
						 "  order by dateOfReturn desc";
		}else{ 
			//if company/admin logged			
			hql+=" and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
					 " and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+") " +
						 "  order by dateOfReturn desc";
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnCounterOrder> returnCounterOrderList=(List<ReturnCounterOrder>)query.list();
		if(returnCounterOrderList.isEmpty()){
			return null;
		}
		
		return returnCounterOrderList;
	}
	
	/**
	 * <pre>
	 * save payment of counter order and entry in ledger credit side
	 * @param paymentCounter
	 * </pre>
	 */
	@Transactional
	public void savePaymentCounter(PaymentCounter paymentCounter){
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);

		sessionFactory.getCurrentSession().save(paymentCounter);
		String payMode="";
		if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
		}else{
			payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
		}
		//ledger entry create
		double credit=paymentCounter.getCurrentAmountPaid();
		Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
		Ledger ledger=new Ledger(
									payMode, 
									paymentCounter.getPaidDate(), 
									paymentCounter.getChequeDate(), 
									paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
									(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
									0, 
									credit, 
									0, 
									paymentCounter,
									branch
								);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
		
	}
	
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterListByCounterOrderId(String counterOrderId){
		
		String hql="from PaymentCounter where status=false and counterOrder.counterOrderId='"+counterOrderId+"'"
				+ " and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
			   hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		       hql+= " order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	/**
	 * <pre>
	 * fetch payment counter list for edit,delete payment
	 * @param counterOrderId
	 * @return PaymentCounterReport list
	 * </pre>
	 */
	@Transactional
	public List<PaymentCounterReport> fetchPaymentCounterReportListByCounterOrderId(String counterOrderId){
		
		String hql="from PaymentCounter where status=false and counterOrder.counterOrderId='"+counterOrderId+"' "
				+ " and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
				hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
				hql+= " order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		
		List<PaymentCounterReport> paymentCounterReportList=new ArrayList<>();
		for(PaymentCounter paymentCounter: paymentCounterList){
			paymentCounterReportList.add(new PaymentCounterReport(paymentCounter.getPaymentCounterId(),
																paymentCounter.getCurrentAmountPaid(), 
																paymentCounter.getCurrentAmountRefund(), 
																paymentCounter.getPaidDate(), 
																paymentCounter.getDueDate(), 
																paymentCounter.getLastDueDate(), 
																paymentCounter.getPayType(), 
																paymentCounter.getChequeNumber(), 
																paymentCounter.getBankName(), 
																paymentCounter.getChequeDate(),
																paymentCounter.getCounterOrder().getBusinessName()==null?false:true,
																paymentCounter.getCounterOrder().getBusinessName()==null?"":paymentCounter.getCounterOrder().getBusinessName().getBusinessNameId(),
																paymentCounter.getCounterOrder().getCustomerName(),
																paymentCounter.getCounterOrder().getCounterOrderId(),
																paymentCounter.getCounterOrder().getTotalAmountWithTax(), 
																employeeDetailsDAO.getEmployeeDetailsByemployeeId(paymentCounter.getEmployee().getEmployeeId()).getName(), 
																paymentCounter.isStatus(), 
																paymentCounter.isChequeClearStatus(),
																paymentCounter.getTransactionRefNo(),
																paymentCounter.getComment(),
																(paymentCounter.getPaymentMethod()!=null)?paymentCounter.getPaymentMethod().getPaymentMethodName():null));
		}
		
		return paymentCounterReportList;
	}
	
	@Transactional
	public PaymentDoInfo fetchPaymentInfoByCounterOrderId(String counterOrderId){
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		
		String id;
		String name;
		String inventoryId=counterOrder.getCounterOrderId();
		double amountPaid=0;
		double amountUnPaid;
		double totalAmount=counterOrder.getTotalAmountWithTax();
		String type="counter";
		String url="giveCounterOrderPayment";
		
		if(counterOrder.getBusinessName()==null){
			id=null;
			name=counterOrder.getCustomerName();
		}else{
			id=counterOrder.getBusinessName().getBusinessNameId();
			name=counterOrder.getBusinessName().getShopName();
		}
		
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		if(paymentCounterList==null){
			amountPaid=0;
			amountUnPaid=counterOrder.getTotalAmountWithTax();
		}else{
			for(PaymentCounter paymentCounter: paymentCounterList){
				amountPaid+=paymentCounter.getCurrentAmountPaid()-paymentCounter.getCurrentAmountRefund();
			}
			amountPaid=((amountPaid>0)?amountPaid:0);
			//amountPaid=paymentCounterList.get(0).getTotalAmountPaid();
			amountUnPaid=counterOrder.getTotalAmountWithTax()-amountPaid;
		}
		
		return new PaymentDoInfo(0,id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
	}
	/**
	 * <pre>
	 * move to return from delivery boy section for define damage and non damage
	 * disable counter order payment and also delete ledger entry
	 * and according that other ledger entries change for balance amount 
	 * @param counterOrderId
	 * </pre>
	 */
	@Transactional
	public void deleteCounterOrder(String counterOrderId){
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterOrderId);
		//List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		
		//return product add in return from delivery boy section for define damage and non damage 
		List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			
			/*Product product=counterOrderProductDetails.getProduct().getProduct();
			product.setCurrentQuantity(product.getCurrentQuantity()+counterOrderProductDetails.getPurchaseQuantity());
			productDAO.Update(product);
			
			productDAO.updateDailyStockExchange(product.getProductId(), product.getCurrentQuantity(), true);
			
			orderDetailsDAO.deleteOrderUsedProduct(counterOrderProductDetails.getProduct());
			
			sessionFactory.getCurrentSession().delete(counterOrderProductDetails);*/
			
			ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
					counterOrderProductDetails.getProduct(), 
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getPurchaseQuantity(), 
					counterOrderProductDetails.getPurchaseQuantity(),
					0,
					0,
					0, 
					counterOrderProductDetails.getType());
			returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
		}
		
		
		long totalReturnQuantity=0;
		long totalIssuedQuantity=0;
		long totalDeliveryQuantity=0;
		long totalDamageQuantity=0;
		long totalNonDamageQuantity=0;

		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			
			totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
			totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
			totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
			totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
			totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
		}
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		
		Branch branch=new Branch();
		branch.setBranchId(getSessionSelectedBranchIds());
		ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
				totalReturnQuantity, 
				totalIssuedQuantity, 
				totalDeliveryQuantity, 
				totalDamageQuantity, 
				totalNonDamageQuantity,  
				counterOrder.getEmployeeGk(), 
				counterOrder.getEmployeeGk(), 
				counterOrder, 
				new Date(), 
				false,
				company,
				branch);

		returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
		sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
		
		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
			sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
		}	
				
		//payment		
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		if(paymentCounterList!=null){
			for(PaymentCounter paymentCounter : paymentCounterList){
				//delete ledger
				double balance=0,debit=0,creditOld=0,credit=0;
				Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
				
				ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
				sessionFactory.getCurrentSession().delete(ledger);	
				if(ledger!=null){
					balance=ledger.getBalance();
					creditOld=ledger.getCredit();
					balance=balance-creditOld;
					
					ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
				}
				//delete ledger end
			}
		}
		/*if(paymentCounterList!=null){
			for(PaymentCounter paymentCounter: paymentCounterList){
				sessionFactory.getCurrentSession().delete(paymentCounter);
			}
		}*/
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	/**
	 * <pre>
	 * fetch payment list according startDate,endDate,range given
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @return paymentCounter list
	 * </pre>
	 */
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterList(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from PaymentCounter where date(paidDate)=date(CURRENT_DATE()) ";					
		}else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from PaymentCounter where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from PaymentCounter where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from PaymentCounter where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from PaymentCounter where MONTH(paidDate)=MONTH(CURRENT_DATE) ";
		}
		else if (range.equals("range")) {
			hql="from PaymentCounter where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from PaymentCounter where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and status=false and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		hql+=" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	/**
	 * <pre>
	 * fetch payment list according startDate,endDate,range given for cheque report
	 * where can define cheque bounced
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @return paymentCounter list
	 * </pre>
	 */
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterListForChequeReport(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from PaymentCounter where date(paidDate)=date(CURRENT_DATE()) ";					
		}else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from PaymentCounter where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from PaymentCounter where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from PaymentCounter where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from PaymentCounter where MONTH(paidDate)=MONTH(CURRENT_DATE) ";
		}
		else if (range.equals("range")) {
			hql="from PaymentCounter where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from PaymentCounter where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and payType!='Cash' and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")  ";
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		hql+=" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	
	@Transactional
	public PaymentCounter fetchPaymentCounterByPaymentCounterId(long paymentCounterId){
		String hql="from PaymentCounter where paymentCounterId="+paymentCounterId;
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		return paymentCounterList.get(0);
	}
	/**
	 * <pre>
	 * update payment details and according that make changes in ledger
	 * @param paymentCounter
	 * </pre> 
	 */
	@Transactional
	public void updatePayment(PaymentCounter paymentCounter){
		updateCounterOrder(paymentCounter.getCounterOrder());
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//ledger update
		
		String payMode="";
		if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
		}else{
			payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
		}
		
		double balance=0,debit=0,creditOld=0,credit=0;		
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			
			creditOld=ledger.getCredit();
			credit=paymentCounter.getCurrentAmountPaid();	
			balance=balance+credit;
			
			ledger.setBalance(balance);
			ledger.setCredit(credit);
			ledger.setPayMode(payMode);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//ledger update done
		
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			double amountPaid=0;
			for(PaymentCounter paymentCounter2: paymentCounterList){
				amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			double remainingAmt=counterOrder.getTotalAmountWithTax()-amountPaid;
			if(remainingAmt>0){
				counterOrder.setPayStatus(false);
			}
		}
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	/**
	 * <pre>
	 * disable payment and same ledger delete and 
	 * change other ledger entries balance amount and company balance  
	 * @param paymentId
	 * </pre>
	 */
	@Transactional
	public void deletePayment(long paymentId){
		
		
		
		PaymentCounter paymentCounter=fetchPaymentCounterByPaymentCounterId(paymentId);
		paymentCounter.setStatus(true);
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//assign due date of payment after delete
		String hql="select max(paymentCounterId) from PaymentCounter where counterOrder.counterOrderId='"+paymentCounter.getCounterOrder().getCounterOrderId()+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
			}
		}
		//end
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			
			counterOrder.setPayStatus(false);
			
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			
		}
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	/**
	 * <pre>
	 * define check bounced 
	 * disable payment entry
	 * delete ledger entry and according that change ledger entry balance
	 * update company balance
	 * @param paymentId
	 * </pre>
	 */
	@Transactional
	public void defineChequeBounced(long paymentId){
		PaymentCounter paymentCounter=fetchPaymentCounterByPaymentCounterId(paymentId);
		paymentCounter.setStatus(true);
		paymentCounter.setChequeClearStatus(false);
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//assign due date of payment after delete
		String hql="select max(paymentCounterId) from PaymentCounter where counterOrder.counterOrderId='"+paymentCounter.getCounterOrder().getCounterOrderId()+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
			}
		}
		//end
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			
			counterOrder.setPayStatus(false);
			
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			
		}
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	/***
	 * <pre>
	 * total counter order sale amount 
	 * @param startDate
	 * @param endDate
	 * @return total counter sale amount
	 * </pre>
	 */
	@Transactional
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="select sum(totalAmountWithTax) from CounterOrder where (date(dateOfOrderTaken)>= '"+startDate+"' and date(dateOfOrderTaken)<='"+endDate+"')";
		hql+=" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
		hql+=" and employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Double> totalAmountWithTaxList=(List<Double>)query.list();
		if(totalAmountWithTaxList.get(0)==null){
			return 0;
		}

		/*
		double totalSale=0;
		for(CounterOrder counterOrder: counterOrderList){
			totalSale+=counterOrder.getTotalAmountWithTax();
		}*/
		
		return totalAmountWithTaxList.get(0);
	}
	/**
	 * <pre>
	 * fetch invoice details list from salesman and counter order
	 * @param startDate
	 * @param endDate 
	 * </pre>
	 */
	@Transactional
	public List<InvoiceDetails> fetchInvoiceDetails(String startDate ,String endDate){
		List<InvoiceDetails> invoiceDetails=new ArrayList<>();
		
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(getAppLoggedEmployeeId());
			
		List<Long> areaIds = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        areaIds.add(area.getAreaId());
	    }
		
		String hql="from CounterOrder where 1=1 "
				+ " and  date(dateOfOrderTaken) <= '"+startDate+"' and date(dateOfOrderTaken) >= '"+endDate+"'";
		hql+=" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
		hql+=" and employeeGk.employeeId in "
				+ "(select employee.employeeId from EmployeeDetails where employeeDetailsId in "
				+ "(select employeeDetails.employeeDetailsId from EmployeeAreaList where area.areaId in (:ids))"
				+ ")";
		hql+=" and employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and branch.branchId in ("+getSessionSelectedBranchIds()+") order by counterOrderPKId desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", areaIds);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		
		hql="from OrderDetails where  date(orderDetailsAddedDatetime) <= '"+startDate+"' and date(orderDetailsAddedDatetime) >= '"+endDate+"'"
				+ " and businessName.area.areaId in (:ids)) and orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"')"
				+" and businessName.company.companyId="+getSessionSelectedCompaniesIds()
				+" and businessName.branch.branchId="+getSessionSelectedBranchIds()
				+" order by orderPkId desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", areaIds);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		
		for(CounterOrder counterOrder : counterOrderList){
			if(counterOrder.getTransportation()==null){
				Transportation transportation=new Transportation();
				transportation.setId(0);
				invoiceDetails.add(new InvoiceDetails(counterOrder.getInvoiceNumber(), 
													counterOrder.getCounterOrderId(), 
													new TransportationModel(
															transportation.getId(), 
															transportation.getTransportName(), 
															transportation.getMobNo(), 
															transportation.getGstNo(), 
															transportation.getVehicleNo()), 
													counterOrder.getVehicleNo(), 
													counterOrder.getDocketNo(),
													counterOrder.getTransportationCharges()));

			}else{
				invoiceDetails.add(new InvoiceDetails(counterOrder.getInvoiceNumber(), 
													counterOrder.getCounterOrderId(), 													
													new TransportationModel(
															counterOrder.getTransportation().getId(), 
															counterOrder.getTransportation().getTransportName(), 
															counterOrder.getTransportation().getMobNo(), 
															counterOrder.getTransportation().getGstNo(), 
															counterOrder.getTransportation().getVehicleNo()), 
													counterOrder.getVehicleNo(), 
													counterOrder.getDocketNo(),
													counterOrder.getTransportationCharges()));
			}
		}
		for(OrderDetails orderDetails : orderDetailsList){
			if(orderDetails.getTransportation()==null){
				Transportation transportation=new Transportation();
				transportation.setId(0);
				invoiceDetails.add(new InvoiceDetails(orderDetails.getInvoiceNumber(), 
													orderDetails.getOrderId(), 
													new TransportationModel(
															transportation.getId(), 
															transportation.getTransportName(), 
															transportation.getMobNo(), 
															transportation.getGstNo(), 
															transportation.getVehicleNo()), 
													orderDetails.getVehicleNo(), 
													orderDetails.getDocketNo(),
													orderDetails.getTransportationCharges()));
			}else{
				invoiceDetails.add(new InvoiceDetails(orderDetails.getInvoiceNumber(), 
													orderDetails.getOrderId(), 
													new TransportationModel(
															orderDetails.getTransportation().getId(), 
															orderDetails.getTransportation().getTransportName(), 
															orderDetails.getTransportation().getMobNo(), 
															orderDetails.getTransportation().getGstNo(), 
															orderDetails.getTransportation().getVehicleNo()), 
													orderDetails.getVehicleNo(), 
													orderDetails.getDocketNo(),
													orderDetails.getTransportationCharges()));
			}
		}
		return invoiceDetails;
	}
	
	/**
	 * <pre>
	 * fetch counter order by businessNameId and payStatus=false
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate 
	 * @return  CounterOrder list
	 * </pre>
	 */
	@Transactional
	public List<CounterOrder> fetchCounterOrderByBusinessNameId(String businessNameId){
		
		/*String hql="from CounterOrder where payStatus=false "+
				   " and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		hql+=" and businessName.businessNameId='"+businessNameId+"' order by dateOfOrderTaken desc";*/
		String hql="from CounterOrder where payStatus=false "
				   +" and badDebtsStatus=true "
				   + " and orderStatus.status='"+Constants.ORDER_STATUS_CANCELED+"'";
		hql+=" and businessName.businessNameId='"+businessNameId+"' "+
			" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				   " order by dateOfOrderTaken desc";
			
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		
		if(counterOrderList.isEmpty()){
			return null;
		}
		
		return counterOrderList;
	}
	
	/**
	 * <pre>
	 * fetch counter order by businessNameId and payStatus=false for view 
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate 
	 * @return  CounterOrder list
	 * </pre>
	 */
	@Transactional
	public List<CounterOrder> fetchCounterOrderByBusinessNameIdForView(String businessNameId){
		
		String hql="from CounterOrder where payStatus=false "
				   /*" and badDebtsStatus=true "*/
				   + " and orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED+"'";
		hql+=" and businessName.businessNameId='"+businessNameId+"' "+
			" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				   " order by dateOfOrderTaken desc";
			
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		
		if(counterOrderList.isEmpty()){
			return null;
		}
		
		return counterOrderList;
	}
	
	/**
	 * <pre>
	 * fetch counter order by businessNameId and payStatus=false for view 
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate 
	 * @return  CounterOrder list
	 * </pre>
	 */
	@Transactional
	public List<CounterOrder> fetchCounterOrderByExternalCustomerForView(String year){
		
		String hql="from CounterOrder where payStatus=false "
				  +" and badDebtsStatus=true "
				  +" and YEAR(badDebtsDatetime)='"+year+"'"
				  +" and businessName is null order by badDebtsDatetime desc";
			
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		
		if(counterOrderList.isEmpty()){
			return null;
		}
		
		return counterOrderList;
	}
	
	/**
	 * set bad debt of counter order of external customer
	 * @param counterOrderId
	 */
	@Transactional
	public void setBadDebtsOfCounter(String counterOrderId){
	
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		double totalAmountDue=counterOrder.getTotalAmountWithTax();
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		if(paymentCounterList!=null){
			double totalPaid=0;
			for(PaymentCounter paymentCounter2: paymentCounterList){
				totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
			totalAmountDue=counterOrder.getTotalAmountWithTax()-totalPaid;
		}
		counterOrder.setTotalAmountBadDebtsDue(totalAmountDue);
		counterOrder.setBadDebtsStatus(true);
		counterOrder.setBadDebtsDatetime(new Date());
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
		updateCounterOrder(counterOrder);;
		//deleteCounterOrder(counterOrder.getCounterOrderId());
	}
	
	/**
	 * <pre>
	 * counter order list for see businessName wise orders for BadDebts report
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showCounterOrderReportByBusinessNameIdForBadDebts(String businessNameId)
	{
		
		List<CounterOrder> counterOrderlist=fetchCounterOrderByBusinessNameId(businessNameId);
		if(counterOrderlist==null)
		{
			return null;
		}
		
		//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(CounterOrder counterOrder : counterOrderlist){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss");
			
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			orderStatusSM="--";
			orderStatusSMDate="--";
			orderStatusSMTime="--";
			orderStatusGK=Constants.ORDER_STATUS_DELIVERED;
			orderStatusGKDate=simpleDateFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusGKTime= simpleTimeFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusDB = "--";
			orderStatusDBDate = "--";
			orderStatusDBTime= "--";
			amount=counterOrder.getTotalAmount();
			amountWithTax=counterOrder.getTotalAmountWithTax();
			quantity=counterOrder.getTotalQuantity();
					
			list2.add(new OrderReportList(srno,
					counterOrder.getCounterOrderId(),
					amount,
					amountWithTax,
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getArea().getName():"NA",
					quantity, 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getShopName():counterOrder.getCustomerName(), 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getBusinessNameId():"NA", 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(),
					(counterOrder.getPaymentDueDate()!=null)?DateDiff.findDifferenceInTwoDatesByDay(counterOrder.getDateOfOrderTaken(), counterOrder.getPaymentDueDate()):0, 
					counterOrder.getDateOfOrderTaken(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							Constants.ORDER_STATUS_DELIVERED));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * counter order list for see businessName wise orders for BadDebts report
	 * @param businessNameId
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showCounterOrderReportByExternalCustomerCounterOrderIdForBadDebts(String counterOrderId)
	{
		
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss");
			
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			orderStatusSM="--";
			orderStatusSMDate="--";
			orderStatusSMTime="--";
			orderStatusGK=Constants.ORDER_STATUS_DELIVERED;
			orderStatusGKDate=simpleDateFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusGKTime= simpleTimeFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusDB = "--";
			orderStatusDBDate = "--";
			orderStatusDBTime= "--";
			amount=counterOrder.getTotalAmount();
			amountWithTax=counterOrder.getTotalAmountWithTax();
			quantity=counterOrder.getTotalQuantity();
					
			list2.add(new OrderReportList(srno,
					counterOrder.getCounterOrderId(),
					amount,
					amountWithTax,
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getArea().getName():"NA",
					quantity, 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getShopName():counterOrder.getCustomerName(), 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getBusinessNameId():"NA", 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(),
					(counterOrder.getPaymentDueDate()!=null)?DateDiff.findDifferenceInTwoDatesByDay(counterOrder.getDateOfOrderTaken(), counterOrder.getPaymentDueDate()):0, 
					counterOrder.getDateOfOrderTaken(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							Constants.ORDER_STATUS_DELIVERED));
			srno++;
		
		return list2;
	}
	/**
	 * <pre>
	 * save return order 
	 * edit counter order
	 * refund save in payment counter
	 * make return from delivery boy product for damage and non damage define
	 * </pre>
	 * @param returnCounterRequest
	 * @return nothing
	 */
	@Transactional
	public long saveReturnCounterOrderDetails(ReturnCounterRequest returnCounterRequest){
		
		// save return counter order main
				ReturnCounterOrder returnCounterOrder = returnCounterRequest.getReturnCounterOrder();
				CounterOrder counterOrder = fetchCounterOrder(returnCounterOrder.getCounterOrder().getCounterOrderId());
				returnCounterOrder.setCounterOrder(counterOrder);

				double amountUnPaid = counterOrder.getTotalAmountWithTax(),
						amountWithTax = counterOrder.getTotalAmountWithTax();

				returnCounterOrder.setReturnCounterOrderGenId(returnCounterOrderIdGenerator.generate());
				returnCounterOrder.setInvoiceNumber(creditNoteInvoiceNumberGenerate.generateInvoiceNumber());
				returnCounterOrder.setDateOfReturn(new Date());
				sessionFactory.getCurrentSession().save(returnCounterOrder);

				// save returned order products
				List<ReturnCounterOrderProducts> returnCounterOrderProductsList = returnCounterRequest
						.getReturnCounterOrderProductsList();
				/*
				 * for(ReturnCounterOrderProducts returnCounterOrderProducts :
				 * returnCounterOrderProductsList){
				 * returnCounterOrderProducts.setReturnCounterOrder(returnCounterOrder);
				 * sessionFactory.getCurrentSession().save(returnCounterOrderProducts);
				 * }
				 */

				// edit counter order products
				long totalQuantity = 0;
				double totalAmount = 0, totalAmountWithTax = 0;
				List<CounterOrderProductDetails> counterOrderProductDetailsList = fetchCounterOrderProductDetails(
						returnCounterOrder.getCounterOrder().getCounterOrderId());
				for (int i = 0; i < counterOrderProductDetailsList.size(); i++) {
					CounterOrderProductDetails counterOrderProductDetails = counterOrderProductDetailsList.get(i);
					for (int j = 0; j < returnCounterOrderProductsList.size(); j++) {
						ReturnCounterOrderProducts returnCounterOrderProducts = returnCounterOrderProductsList.get(j);
						if (counterOrderProductDetails.getProduct().getProduct().getProductId() == returnCounterOrderProducts
								.getProduct().getProduct().getProductId()
								&& returnCounterOrderProducts.getType().equals(counterOrderProductDetails.getType())) {
							counterOrderProductDetails.setPurchaseQuantity(counterOrderProductDetails.getPurchaseQuantity()
									- returnCounterOrderProducts.getReturnQuantity());
							double purchaseAmt = counterOrderProductDetails.getPurchaseQuantity()
									* counterOrderProductDetails.getSellingRate();
							double discountAmt = (counterOrderProductDetails.getDiscountPer() * purchaseAmt) / 100;
							counterOrderProductDetails.setDiscount(discountAmt);
							counterOrderProductDetails.setPurchaseAmount(purchaseAmt - discountAmt);
							counterOrderProductDetails.setReturnQuantity(counterOrderProductDetails.getReturnQuantity()
									+ returnCounterOrderProducts.getReturnQuantity());

							/*
							 * if(counterOrderProductDetails.getPurchaseQuantity()==0){
							 * counterOrderProductDetails=(CounterOrderProductDetails)
							 * sessionFactory.getCurrentSession().merge(
							 * counterOrderProductDetails);
							 * sessionFactory.getCurrentSession().delete(
							 * counterOrderProductDetails); }else{
							 */
							counterOrderProductDetails = (CounterOrderProductDetails) sessionFactory.getCurrentSession()
									.merge(counterOrderProductDetails);
							sessionFactory.getCurrentSession().update(counterOrderProductDetails);
							// }

							returnCounterOrderProducts.setReturnCounterOrder(returnCounterOrder);
							returnCounterOrderProducts.setProduct(counterOrderProductDetails.getProduct());
							sessionFactory.getCurrentSession().save(returnCounterOrderProducts);
						}
					}
					CalculateProperTaxModel calculateProperTaxModel = productDAO.calculateProperAmountModel(
							counterOrderProductDetails.getPurchaseAmount(),
							counterOrderProductDetails.getProduct().getCategories().getIgst());
					totalAmount += calculateProperTaxModel.getUnitprice();
					totalAmountWithTax += counterOrderProductDetails.getPurchaseAmount();
					totalQuantity += counterOrderProductDetails.getPurchaseQuantity();
				}
				// if all counter order product return then order defined as cancelled
				// and define return
				// counterOrderProductDetailsList=fetchCounterOrderProductDetails(returnCounterOrder.getCounterOrder().getCounterOrderId());
				// if(counterOrderProductDetailsList!=null){

				if (totalAmountWithTax != 0) {
					// whole discount assign
					double discountAmtCut = 0, discountPerCut = 0;
					if (counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
						/*discountAmtCut = counterOrder.getDiscount();
						discountPerCut = (counterOrder.getDiscount() / totalAmountWithTax) * 100;*/
						
						discountPerCut = findPercentage(counterOrder.getCounterOrderId());
						discountAmtCut = (discountPerCut * totalAmountWithTax) / 100;
					} else {
						discountPerCut = counterOrder.getDiscount();
						discountAmtCut = (counterOrder.getDiscount() * totalAmountWithTax) / 100;				
					}

					totalAmount = totalAmount - ((totalAmount * discountPerCut) / 100);
					totalAmount = MathUtils.round(totalAmount, 0);

					//totalAmountWithTax = totalAmountWithTax - discountAmtCut;
					totalAmountWithTax = counterOrder.getTotalAmountWithTax()-returnCounterOrder.getTotalAmountWithTax();//MathUtils.round(totalAmountWithTax, 0);
				}

				counterOrder.setTotalAmount(totalAmount);
				counterOrder.setTotalAmountWithTax(totalAmountWithTax);
				counterOrder.setTotalQuantity(totalQuantity);

				if (counterOrder.getTotalQuantity() == 0) {
					counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
				}

				counterOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(counterOrder);
				sessionFactory.getCurrentSession().update(counterOrder);
		/*}else{
			counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
			counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
			sessionFactory.getCurrentSession().update(counterOrder);
		}*/
		
		
		// refund amount set in payment section
		
		/*List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
		double totalPaid=0;
		for(PaymentCounter paymentCounter2: paymentCounterList){
			totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
		}*/
		
		if(returnCounterRequest.getRefAmount()>0){
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			
			//double refAmount=totalPaid-totalAmountWithTax;
			PaymentCounter paymentCounter=new PaymentCounter();
			Employee employee=new Employee(); 
			employee.setEmployeeId(getAppLoggedEmployeeId());
			paymentCounter.setEmployee(employee);
			paymentCounter.setCounterOrder(counterOrder);
			paymentCounter.setCurrentAmountRefund(returnCounterRequest.getRefAmount());
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(returnCounterRequest.getPayType());
			if(returnCounterRequest.getPayType().equals("Cash")){				
			}else if(returnCounterRequest.getPayType().equals("Cheque")){
				paymentCounter.setBankName(returnCounterRequest.getBankName());
				paymentCounter.setChequeNumber(returnCounterRequest.getChequeNumber());
				try { paymentCounter.setChequeDate(dateFormat.parse(returnCounterRequest.getChequeDate())); } catch (Exception e) {}
			}else{
				paymentCounter.setTransactionRefNo(returnCounterRequest.getTransactionRef());
				paymentCounter.setComment(returnCounterRequest.getComment());
				paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(returnCounterRequest.getPaymentMethodId()));
			}
			if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
				paymentCounter.setChequeClearStatus(true);
				sessionFactory.getCurrentSession().save(paymentCounter);
			}
			counterOrder.setPayStatus(true);
			counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
			sessionFactory.getCurrentSession().update(counterOrder);
			
			//ledger entry create		
			double debit=paymentCounter.getCurrentAmountRefund();
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			String payMode="";
			if(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)){
				payMode="Cash";
			}else if(paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
				payMode=paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber();
			}else{
				payMode=paymentCounter.getPaymentMethod().getPaymentMethodName()+"-"+paymentCounter.getTransactionRefNo();
			}
			
			Ledger ledger=new Ledger(
						payMode, 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Refund", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						debit, 
						0, 
						0, 
						paymentCounter,
						branch
					);
			ledgerDAO.createLedgerEntry(ledger);
		}
		
		//return product add in return from delivery boy section for define damage and non damage 
		List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();		
		for(ReturnCounterOrderProducts returnCounterOrderProducts : returnCounterOrderProductsList)
		{
			
			ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
					returnCounterOrderProducts.getProduct(), 
					returnCounterOrderProducts.getSellingRate(), 
					returnCounterOrderProducts.getReturnQuantity(), 
					returnCounterOrderProducts.getIssuedQuantity(),
					returnCounterOrderProducts.getIssuedQuantity()-returnCounterOrderProducts.getReturnQuantity(),
					0,
					0, 
					returnCounterOrderProducts.getType());
			returnFromDeliveryBoyList.add(returnFromDeliveryBoy);

		}
		
		long totalReturnQuantity=0;
		long totalIssuedQuantity=0;
		long totalDeliveryQuantity=0;
		long totalDamageQuantity=0;
		long totalNonDamageQuantity=0;

		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			
			totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
			totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
			totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
			totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
			totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
		}
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		
		Branch branch=new Branch();
		branch.setBranchId(getSessionSelectedBranchIds());
		
		ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
				totalReturnQuantity, 
				totalIssuedQuantity, 
				totalDeliveryQuantity, 
				totalDamageQuantity, 
				totalNonDamageQuantity,  
				counterOrder.getEmployeeGk(), 
				counterOrder.getEmployeeGk(), 
				counterOrder, 
				new Date(), 
				false,
				company,
				branch);

		returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
		sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
		
		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
			sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
		}	
		
		return returnCounterOrder.getReturnCounterOrderId();
	}
	
	
	//find percentage of discountType = Amount
		public double findPercentage(String counterOrderId){
			CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
			List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=fetchCounterOrderProductDetailsForShowOrderDetails(counterOrderId);
			
			double totalAmountWithTaxMain=0;
			for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
			{
				totalAmountWithTaxMain+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			}
			
			totalAmountWithTaxMain=MathUtils.round(totalAmountWithTaxMain, 2);
			
			String discountType=counterOrder.getDiscountType();
			double discount=counterOrder.getDiscount();
			double mainPercentage=0;
			
			if(discountType.equals("Amount")){
				mainPercentage=(
					(discount)/(totalAmountWithTaxMain)	
				)*100;
			}else{
				mainPercentage=(discount);
			}
			
			return MathUtils.round(mainPercentage, 2);
		}
	
	@Transactional
	public ReturnCounterOrder fetchReturnCounterOrder(long returnCounterOrderId){
		String hql="from ReturnCounterOrder where returnCounterOrderId="+returnCounterOrderId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnCounterOrder> returnCounterOrderList=(List<ReturnCounterOrder>)query.list();
		return returnCounterOrderList.get(0);
	}
	
	@Transactional
	public List<ReturnCounterOrderProducts> fetchReturnCounterOrderProductList(long returnCounterOrderId){
		String hql="from ReturnCounterOrderProducts where returnCounterOrder.returnCounterOrderId="+returnCounterOrderId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnCounterOrderProducts> returnCounterOrderProductList=(List<ReturnCounterOrderProducts>)query.list();
		return returnCounterOrderProductList;
	}
	
	@Transactional
	public List<OrderProductDetailListForWebApp> fetchReturnCounterOrderProductModelList(long returnCounterOrderId){
		List<ReturnCounterOrderProducts> returnCounterOrderProductList=fetchReturnCounterOrderProductList(returnCounterOrderId);
		List<OrderProductDetailListForWebApp> orderProductDetailList=new ArrayList<>();
		long srno=1;
		for(ReturnCounterOrderProducts returnCounterOrderProducts : returnCounterOrderProductList){
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnCounterOrderProducts.getReturnCounterOrder().getEmployee().getEmployeeId());
			orderProductDetailList.add(new OrderProductDetailListForWebApp(
										srno, 
										returnCounterOrderProducts.getReturnCounterOrder().getReturnCounterOrderGenId(), 
										returnCounterOrderProducts.getProduct().getCategories().getCategoryName(),
										returnCounterOrderProducts.getProduct().getBrand().getName(),
										returnCounterOrderProducts.getProduct().getProductName(),
										employeeDetails.getName(), 
										employeeDetails.getEmployee().getEmployeeId(), 
										employeeDetails.getEmployeeDetailsGenId(),
										returnCounterOrderProducts.getSellingRate(),
										returnCounterOrderProducts.getReturnQuantity(), 
										0,
										returnCounterOrderProducts.getDiscountPer(), 
										returnCounterOrderProducts.getDiscount(),
										returnCounterOrderProducts.getReturnTotalAmountWithTax(),
										returnCounterOrderProducts.getSellingRate()*returnCounterOrderProducts.getReturnQuantity(),
										returnCounterOrderProducts.getReturnCounterOrder().getDateOfReturn(), 
										returnCounterOrderProducts.getType(),
										returnCounterOrderProducts.getReason()));
			srno++;
		}
		
		return orderProductDetailList;
	}
	
	/**
	 * <pre>
	 * fetch counter return order by range,startDate,endDate
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return PReturnOrderDetailsModel list
	 * </pre>
	 */
	@Transactional
	public List<CounterReturnOrderModel> counterReturnOrderReport(String range,String startDate,String endDate)
	{
		List<CounterReturnOrderModel> counterReturnOrderModelList=new ArrayList<>();
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from ReturnCounterOrder where date(dateOfReturn) >= '"+startDate+"' and date(dateOfReturn) <= '"+endDate+"' ";
		}
		else if(range.equals("today"))
		{
			hql="from ReturnCounterOrder where date(dateOfReturn) = date(CURRENT_DATE()) ";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from ReturnCounterOrder where date(dateOfReturn) = '"+simpleDateFormat.format(cal.getTime())+"' ";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReturnCounterOrder where date(dateOfReturn) >= '"+simpleDateFormat.format(cal.getTime())+"' ";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from ReturnCounterOrder where (date(dateOfReturn) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(dateOfReturn) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from ReturnCounterOrder where (date(dateOfReturn) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(dateOfReturn) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if(range.equals("last3Months"))
		{
			hql="from ReturnCounterOrder where (date(dateOfReturn) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(dateOfReturn) <= '"+DatePicker.getLast3MonthLastDate()+"') ";
		}
		else if(range.equals("pickDate"))
		{
			hql="from ReturnCounterOrder where date(dateOfReturn) = '"+startDate+"' ";
		}
		else if(range.equals("viewAll"))
		{
			hql="from ReturnCounterOrder where 1=1 ";
		}

		String areaListArray="";
		if(getAppLoggedEmployeeId()!=0){
			List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(getAppLoggedEmployeeId());		
			
			//List<Area> areaList=new ArrayList<>();
			for(EmployeeAreaList employeeAreaList: employeeAreaLists)
			{
				//areaList.add(employeeAreaList.getArea());
				areaListArray+=employeeAreaList.getArea().getAreaId()+",";
			}
			areaListArray=areaListArray.substring(0, areaListArray.length()-1);
		}
				
		//if gatekeeper logged
		if(getAppLoggedEmployeeId()!=0){
			hql+=" and counterOrder.employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList "+
						 " where area.areaId in ("+areaListArray+")) "+
						 " and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+") " +
						 "  order by dateOfReturn desc";
		}else{ 
			//if company/admin logged			
			hql+=" and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
					 " and counterOrder.branch.branchId in ("+getSessionSelectedBranchIds()+") " +
						 "  order by dateOfReturn desc";
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnCounterOrder> list=(List<ReturnCounterOrder>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		long srno=1;
		for(ReturnCounterOrder  returnCounterOrder : list){
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnCounterOrder.getEmployee().getEmployeeId());
			counterReturnOrderModelList.add(new CounterReturnOrderModel(
					srno, 
					returnCounterOrder.getReturnCounterOrderId(), 
					returnCounterOrder.getCounterOrder().getCounterOrderId(), 
					employeeDetails.getName(), 
					(returnCounterOrder.getCounterOrder().getBusinessName()==null)?returnCounterOrder.getCounterOrder().getCustomerName():returnCounterOrder.getCounterOrder().getBusinessName().getShopName(),
					returnCounterOrder.getReturnCounterOrderGenId(),
					returnCounterOrder.getTotalQuantity(),
					returnCounterOrder.getTotalAmount(), 
					returnCounterOrder.getTotalAmountWithTax(),
					returnCounterOrder.getDateOfReturn()));
			srno++;
		}
		
		return counterReturnOrderModelList;
	}
	
	/***
	 * <pre>
	 * make return counter order bill details for generate invoice pdf
	 * @param counterOrderId
	 * @return BillPrintDataModel
	 * </pre>
	 */
	@Transactional
	public BillPrintDataModel fetchReturnCounterBillPrintData(long returnCounterOrderId)
	{

		// get counter order Details
		ReturnCounterOrder returnCounterOrder = fetchReturnCounterOrder(returnCounterOrderId);

		// get counter order product list
		List<ReturnCounterOrderProducts> returnCounterOrderProductList = fetchReturnCounterOrderProductList(
				returnCounterOrderId);

		BusinessName businessName = returnCounterOrder.getCounterOrder().getBusinessName();

		String invoiceNumber = returnCounterOrder.getCounterOrder().getInvoiceNumber();
		String orderDate = new SimpleDateFormat("dd-MM-yyyy")
				.format(returnCounterOrder.getCounterOrder().getDateOfOrderTaken());
		String returnDate = new SimpleDateFormat("dd-MM-yyyy").format(returnCounterOrder.getDateOfReturn());
		String deliveryDate = new SimpleDateFormat("dd-MM-yyyy")
				.format(returnCounterOrder.getCounterOrder().getDateOfOrderTaken());

		List<ProductListForBill> productListForBillList = new ArrayList<>();

		float totalAmountWithoutTax = 0;

		float cGSTAmount = 0;
		float iGSTAmount = 0;
		float sGSTAmount = 0;

		long totalQuantity = 0;
		float totalAmountWithTax = 0;
		String totalAmountWithTaxInWord = "";

		List<CategoryWiseAmountForBill> categoryWiseAmountForBills = new ArrayList<>();

		float totalAmount = 0;
		float taxAmount = 0;
		String taxAmountInWord = "";

		float totalCGSTAmount = 0;
		float totalIGSTAmount = 0;
		float totalSGSTAmount = 0;

		// DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		// DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		long srno = 1;
		// create product wise details
		for (ReturnCounterOrderProducts returnCounterOrderProducts : returnCounterOrderProductList) {
			long qty = returnCounterOrderProducts.getReturnQuantity();
			double total = qty * returnCounterOrderProducts.getSellingRate();
			double discAmt = 0;//(total * returnCounterOrderProducts.getDiscountPer()) / 100;
			if(returnCounterOrderProducts.getDiscountType().equals("Amount")){
				discAmt = (float)returnCounterOrderProducts.getDiscount();
			}else{
				discAmt = (float)(total * returnCounterOrderProducts.getDiscountPer()) / 100;
			}
			total = total - discAmt;

			// float amountWithoutTax=0;
			CalculateProperTaxModel calculateProperTaxModel = productDAO.calculateProperAmountModel(total,
					returnCounterOrderProducts.getProduct().getCategories().getIgst());
			CalculateProperTaxModel calculateProperTaxModel2 = productDAO.calculateProperAmountModel(
					returnCounterOrderProducts.getSellingRate(),
					returnCounterOrderProducts.getProduct().getCategories().getIgst());

			if (returnCounterOrderProducts.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
				// amountWithoutTax=calculateProperTaxModel.getUnitprice();

				/*
				 * float
				 * igst=counterOrderProductDetails.getProduct().getCategories().
				 * getIgst(); float
				 * cgst=counterOrderProductDetails.getProduct().getCategories().
				 * getCgst(); float
				 * sgst=counterOrderProductDetails.getProduct().getCategories().
				 * getSgst();
				 * 
				 * float rate=calculateProperTaxModel.getUnitprice();
				 */
				long issuedQuantity = 1;

				if (businessName != null) {
					if (businessName.getTaxType().equals(Constants.INTRA_STATUS)) {
						cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
						iGSTAmount += 0;
						sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
					} else {
						cGSTAmount += 0;
						iGSTAmount += calculateProperTaxModel.getIgst() * issuedQuantity;
						sGSTAmount += 0;
					}
				} else {// external customer - assumed intra tax type
					cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
					iGSTAmount += 0;
					sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
				}
			}

			totalQuantity += returnCounterOrderProducts.getReturnQuantity();

			float discountAmt = 0;
			// double
			// productamountWithTax=counterOrderProductDetails.getProduct().getRate()*counterOrderProductDetails.getPurchaseQuantity();
			// discountAmt=(float)(productamountWithTax*counterOrderProductDetails.getDiscountPer())/100;
			double totalGrossAmount = returnCounterOrderProducts.getSellingRate()
					* (returnCounterOrderProducts.getReturnQuantity());
			discountAmt = 0;//(float) (totalGrossAmount * returnCounterOrderProducts.getDiscountPer() / 100);
			if(returnCounterOrderProducts.getDiscountType().equals("Amount")){
				discountAmt = (float)returnCounterOrderProducts.getDiscount();
			}else{
				discountAmt = (float)(totalGrossAmount * returnCounterOrderProducts.getDiscountPer()) / 100;
			}
			float totalAmtWithTax = (float) (returnCounterOrderProducts.getSellingRate()
					* returnCounterOrderProducts.getReturnQuantity());
			productListForBillList
					.add(new ProductListForBill(String.valueOf(srno),
							returnCounterOrderProducts.getProduct().getProductName() + (returnCounterOrderProducts
									.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE) ? "-(Free)" : ""),
							returnCounterOrderProducts.getProduct().getCategories().getHsnCode(),
							String.valueOf(((long) returnCounterOrderProducts.getProduct().getCategories().getIgst())),
							String.valueOf(returnCounterOrderProducts.getReturnQuantity()),
							(returnCounterOrderProducts.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE) ? "0"
									: returnCounterOrderProducts.getSellingRate() + " "),
							String.valueOf(
									MathUtils.roundFromFloat((float) ((returnCounterOrderProducts.getReturnQuantity())
											* returnCounterOrderProducts.getSellingRate()), 2)),
							String.valueOf(MathUtils.roundFromFloat((float) discountAmt, 2)),
							String.valueOf(
									MathUtils.roundFromFloat((float) returnCounterOrderProducts.getDiscountPer(), 2)),
							String.valueOf(MathUtils.roundFromFloat((float) totalGrossAmount - discountAmt, 2))));
			totalAmountWithoutTax += MathUtils.roundFromFloat((float) totalGrossAmount - discountAmt, 2);
			srno++;
			// double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
			// totalAmountWithTax+=amountWithoutTax+tax;

		}
		// total amount with tax basis of product details
		for (ProductListForBill productListForBill2 : productListForBillList) {
			totalAmountWithTax += Double.parseDouble(productListForBill2.getNetPayable());
		}
		totalAmountWithTax = MathUtils.roundFromFloat(totalAmountWithTax, 2);
		cGSTAmount = MathUtils.roundFromFloat(cGSTAmount, 3);
		iGSTAmount = MathUtils.roundFromFloat(iGSTAmount, 2);
		sGSTAmount = MathUtils.roundFromFloat(sGSTAmount, 3);

		// totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;

		// get unique category
		Set<OrderUsedCategories> categoriesList = new HashSet<>();
		for (ReturnCounterOrderProducts returnCounterOrderProducts : returnCounterOrderProductList) {
			if (returnCounterOrderProducts.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
				categoriesList.add(returnCounterOrderProducts.getProduct().getCategories());
			}
		}

		// category wise taxslab and amount find
		Set<String> categoriesHsnCodeList = new HashSet<>();
		for (OrderUsedCategories categories : categoriesList) {
			String hsncode = "";
			float taxableValue = 0;
			float cgstPercentage = 0;
			float igstPercentage = 0;
			float sgstPercentage = 0;
			float cgstRate = 0;
			float igstRate = 0;
			float sgstRate = 0;

			cGSTAmount = 0;
			iGSTAmount = 0;
			sGSTAmount = 0;

			boolean gotDuplicateHsnCode = false;

			for (String hsnCode : categoriesHsnCodeList) {
				if (categories.getHsnCode().equals(hsnCode)) {
					gotDuplicateHsnCode = true;
				}
			}
			if (gotDuplicateHsnCode) {
				continue;
			}

			categoriesHsnCodeList.add(categories.getHsnCode());

			for (ReturnCounterOrderProducts returnCounterOrderProducts : returnCounterOrderProductList) {
				if (returnCounterOrderProducts.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
					if (categories.getHsnCode()
							.equals(returnCounterOrderProducts.getProduct().getCategories().getHsnCode())) {
						long qty = returnCounterOrderProducts.getReturnQuantity();
						double total = qty * returnCounterOrderProducts.getSellingRate();
						double discAmt = 0;//(total * returnCounterOrderProducts.getDiscountPer()) / 100;
						if(returnCounterOrderProducts.getDiscountType().equals("Amount")){
							discAmt = returnCounterOrderProducts.getDiscount();
						}else{
							discAmt = (total * returnCounterOrderProducts.getDiscountPer()) / 100;
						}
						total = total - discAmt;

						CalculateProperTaxModel calculateProperTaxModel = productDAO.calculateProperAmountModel(total,
								returnCounterOrderProducts.getProduct().getCategories().getIgst());
						taxableValue += calculateProperTaxModel.getUnitprice();

						/*
						 * float igst=counterOrderProductDetails.getProduct().
						 * getCategories().getIgst(); float
						 * cgst=counterOrderProductDetails.getProduct().
						 * getCategories().getCgst(); float
						 * sgst=counterOrderProductDetails.getProduct().
						 * getCategories().getSgst(); float
						 * rate=calculateProperTaxModel.getUnitprice();
						 */
						long issuedQuantity = 1;

						if (businessName != null) {
							if (businessName.getTaxType().equals(Constants.INTRA_STATUS)) {
								cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
								iGSTAmount += 0;
								sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
							} else {
								cGSTAmount += 0;
								iGSTAmount += calculateProperTaxModel.getIgst() * issuedQuantity;
								sGSTAmount += 0;
							}
						} else {// external customer - assumed intra tax type
							cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
							iGSTAmount += 0;
							sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
						}
						cgstPercentage = returnCounterOrderProducts.getProduct().getCategories().getCgst();
						igstPercentage = returnCounterOrderProducts.getProduct().getCategories().getIgst();
						sgstPercentage = returnCounterOrderProducts.getProduct().getCategories().getSgst();
						hsncode = returnCounterOrderProducts.getProduct().getCategories().getHsnCode();
					}
				}
			}

			categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(hsncode,
					MathUtils.roundFromFloat(taxableValue, 2) + "", MathUtils.roundFromFloat(cgstPercentage, 2) + "",
					MathUtils.roundFromFloat(cGSTAmount, 3) + "", MathUtils.roundFromFloat(igstPercentage, 2) + "",
					MathUtils.roundFromFloat(iGSTAmount, 2) + "", MathUtils.roundFromFloat(sgstPercentage, 2) + "",
					MathUtils.roundFromFloat(sGSTAmount, 3) + ""));

			totalAmount += taxableValue;
			totalCGSTAmount += cGSTAmount;
			totalSGSTAmount += sGSTAmount;
			totalIGSTAmount += iGSTAmount;
		}
		taxAmount = ((MathUtils.roundFromFloat(totalSGSTAmount, 2)) + (MathUtils.roundFromFloat(totalCGSTAmount, 2))
				+ (MathUtils.roundFromFloat(totalIGSTAmount, 3)));
		// total amount without tax in word
		//taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));

		totalAmountWithTax = (MathUtils.roundFromFloat(totalAmountWithTax, 2));
		// find round of amount
		/*
		 * float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax; float
		 * roundOf; String roundOfAmount=""; if(decimalAmount==0) {
		 * roundOf=0.0f; roundOfAmount=""+MathUtils.roundFromFloat(roundOf,2);
		 * totalAmountWithTax=totalAmountWithTax+roundOf; } else
		 * if(decimalAmount>=0.5) { roundOf=1-decimalAmount;
		 * roundOfAmount="+"+MathUtils.roundFromFloat(roundOf,2);
		 * totalAmountWithTax=totalAmountWithTax+roundOf; } else {
		 * roundOf=decimalAmount;
		 * roundOfAmount="-"+MathUtils.roundFromFloat(roundOf,2);
		 * totalAmountWithTax=totalAmountWithTax-roundOf; }
		 */

		double discountAmtCut = 0, discountPerCut = 0;
		if (returnCounterOrder.getCounterOrder().getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
			discountAmtCut = returnCounterOrder/*.getCounterOrder()*/.getDiscount();
			discountPerCut = (returnCounterOrder/*.getCounterOrder()*/.getDiscount() / totalAmountWithTax) * 100;
		} else {
			discountPerCut = returnCounterOrder.getCounterOrder().getDiscount();
			discountAmtCut = (returnCounterOrder.getCounterOrder().getDiscount() * totalAmountWithTax) / 100;
		}

		/*
		 * totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
		 * totalAmount=MathUtils.round(totalAmount, 0);
		 */

		double totalAmountWithTaxWithDisc = totalAmountWithTax - discountAmtCut;

		float roundOffAmount = (float) (MathUtils.round(totalAmountWithTaxWithDisc, 0)
				- MathUtils.round(totalAmountWithTaxWithDisc, 2));
		String roundAmt = "";

		if (roundOffAmount > 0) {
			roundAmt = "+" + roundOffAmount;
		} else if (roundOffAmount < 0) {
			roundAmt = roundOffAmount + "";
		} else {
			roundAmt = "0";
		}
		String totalAmountWithTaxRoundOff = MathUtils.round(totalAmountWithTaxWithDisc, 0) + "";

		double totalAmountWithTaxWithDiscNonRoundOff = totalAmountWithTaxWithDisc;
		totalAmountWithTaxWithDisc = MathUtils.round(totalAmountWithTaxWithDisc, 0);
		/*
		 * if(decimalAmount>0.0f) { roundOf=1-decimalAmount;
		 * roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
		 * totalAmountWithTax=totalAmountWithTax-roundOf; }
		 */

		totalAmountWithTaxInWord = NumberToWordsConverter
				.convertWithPaisa((float) MathUtils.round(totalAmountWithTaxWithDisc, 0));

		discountAmtCut = MathUtils.round(discountAmtCut, 2);
		discountPerCut = MathUtils.round(discountPerCut, 2);
		totalAmountWithTaxWithDiscNonRoundOff = MathUtils.round(totalAmountWithTaxWithDiscNonRoundOff, 2);

//		List<BankDetailsForInvoice> bankDetailsForInvoiceList = bankDetailsForInvoiceDAO
//				.fetchBankDetailsForInvoiceList();
		
		/**
		 * calculate category wise block under discount and net amount 
		 */
		double totalAmountDisc=0;
		double totalCGSTAmountDisc=0;
		double totalIGSTAmountDisc=0;
		double totalSGSTAmountDisc=0;		
		double netTotalAmount=0;
		double netTotalCGSTAmount=0;
		double netTotalIGSTAmount=0;
		double netTotalSGSTAmount=0;
		if (returnCounterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
			
			 totalAmountDisc=((discountAmtCut/totalAmountWithTax)*totalAmount);
			 totalCGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalCGSTAmount);
			 totalIGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalIGSTAmount);
			 totalSGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalSGSTAmount);
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
			
		}else{
			 totalAmountDisc=(discountPerCut*totalAmount)/100;
			 totalCGSTAmountDisc=(discountPerCut*totalCGSTAmount)/100;
			 totalIGSTAmountDisc=(discountPerCut*totalIGSTAmount)/100;
			 totalSGSTAmountDisc=(discountPerCut*totalSGSTAmount)/100;
			
			 netTotalAmount=totalAmount-totalAmountDisc;
			 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
			 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
			 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
		}
		
		taxAmount = ((MathUtils.roundFromFloat((float)netTotalSGSTAmount, 3)) + (MathUtils.roundFromFloat((float)netTotalCGSTAmount, 3))
				+ (MathUtils.roundFromFloat((float)netTotalIGSTAmount, 2)));		
		taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));

		if (businessName != null) {
			return new BillPrintDataModel(returnCounterOrder.getCounterOrder().getCounterOrderId(), invoiceNumber,
					returnCounterOrder.getInvoiceNumber(), returnDate, orderDate, null, businessName, null,
					deliveryDate, productListForBillList, MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
					MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
					MathUtils.roundFromFloat(totalSGSTAmount, 3) + "", roundAmt,
					MathUtils.roundFromFloat(totalAmountWithoutTax, 2) + "", String.valueOf(totalQuantity),
					MathUtils.roundFromFloat(totalAmountWithTax, 2) + "", String.valueOf(discountAmtCut),
					String.valueOf(discountPerCut), String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
					String.valueOf(totalAmountWithTaxWithDisc), totalAmountWithTaxRoundOff, totalAmountWithTaxInWord,
					categoryWiseAmountForBills, MathUtils.roundFromFloat(totalAmount, 2) + "", taxAmountInWord,
					MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
					MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
					MathUtils.roundFromFloat(totalSGSTAmount, 3) + "",
					MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
					MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
					MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
					MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
					MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
					MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
					MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
					MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
					"", "", "", "", returnCounterOrder.getComment(),
					0
					);
		} else {
			return new BillPrintDataModel(returnCounterOrder.getCounterOrder().getCounterOrderId(), invoiceNumber,
					returnCounterOrder.getInvoiceNumber(), returnDate, orderDate, null,
					returnCounterOrder.getCounterOrder().getCustomerName(),
					returnCounterOrder.getCounterOrder().getCustomerMobileNumber(),
					returnCounterOrder.getCounterOrder().getCustomerGstNumber(),
					returnCounterOrder.getCounterOrder().getEmployeeGk(), 
					deliveryDate, productListForBillList, MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
					MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
					MathUtils.roundFromFloat(totalSGSTAmount, 3) + "", roundAmt,
					MathUtils.roundFromFloat(totalAmountWithoutTax, 2) + "", String.valueOf(totalQuantity),
					MathUtils.roundFromFloat(totalAmountWithTax, 2) + "", String.valueOf(discountAmtCut),
					String.valueOf(discountPerCut), String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
					String.valueOf(totalAmountWithTaxWithDisc), totalAmountWithTaxRoundOff, totalAmountWithTaxInWord,
					categoryWiseAmountForBills, MathUtils.roundFromFloat(totalAmount, 2) + "", taxAmountInWord,
					MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
					MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
					MathUtils.roundFromFloat(totalSGSTAmount, 3) + "",
					MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
					MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
					MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
					MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
					MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
					MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
					MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
					MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
					"", "", "", "", returnCounterOrder.getComment(),0);
		}
	}
	
	
	
	//Proforma Order
		//save Proforma Order
			@Transactional
			public String saveProformaOrder(String productDetailsList, String businessNameId, long transportationId,
					String vehicalNumber, String docketNumber, String transactionRefId, String comment, long paymentMethodId,
					String discountType, String discountAmount,
					String orderComment,double trasportationCharge) {

				ProformaOrder proformaOrder = new ProformaOrder();
				proformaOrder.setOrderComment(orderComment);
				String productDataList[] = productDetailsList.split("-");
				double totalAmount = 0, totalAmountWithTax = 0;
				long totalQuantity = 0;

				List<ProformaOrderProductDetails> proformaOrderProductDetailsList = new ArrayList<>();

				for (String productData : productDataList) {
					String productDataSplit[] = productData.split(",");

					String productId = productDataSplit[0];
					String qty = productDataSplit[1];
					String mrp = productDataSplit[2];
					String total = productDataSplit[3];
					String type = productDataSplit[4];

					String discount = productDataSplit[5];
					String discountAmt = productDataSplit[6];
					String discountTyp = productDataSplit[7];

					String discountOld = productDataSplit[8];
					String discountAmtOld = productDataSplit[9];
					String totalOld = productDataSplit[10];

					String discountTypOld = productDataSplit[11];

					String totalUP = productDataSplit[12];

					Product product = productDAO.fetchProductForWebApp(Long.parseLong(productId));

					// less Current quantity
					/*product.setCurrentQuantity(product.getCurrentQuantity() - Long.parseLong(qty));
					productDAO.Update(product);*/
					// update daily stock
					/*productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);*/

					// CalculateProperTaxModel
					// calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp),
					// product.getCategories().getIgst());

					totalAmount += Float.parseFloat(totalUP);
					totalAmountWithTax += Float.parseFloat(total);
					totalQuantity += Long.parseLong(qty);

					ProformaOrderProductDetails proformaOrderProductDetails = new ProformaOrderProductDetails();
					proformaOrderProductDetails.setDiscount(Double.parseDouble(discountAmt));
					proformaOrderProductDetails.setDiscountPer(Double.parseDouble(discount));
					proformaOrderProductDetails.setDiscountType(discountTyp);
					/*
					 * counterOrderProductDetails.setDiscountOld(Double.parseDouble(
					 * discountAmtOld));
					 * counterOrderProductDetails.setDiscountPerOld(Double.parseDouble(
					 * discountOld));
					 * 
					 * counterOrderProductDetails.setDiscountTypeOld(discountTypOld);
					 */

					Company company = companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());

					OrderUsedBrand orderUsedBrand = new OrderUsedBrand();
					orderUsedBrand.setName(product.getBrand().getName());
					orderUsedBrand.setCompany(company);
					orderUsedBrand.setBranch(branch);
					sessionFactory.getCurrentSession().save(orderUsedBrand);

					OrderUsedCategories orderUsedCategories = new OrderUsedCategories(product.getCategories().getCategoryName(),
							product.getCategories().getHsnCode(), product.getCategories().getCgst(),
							product.getCategories().getSgst(), product.getCategories().getIgst(), company, branch);
					sessionFactory.getCurrentSession().save(orderUsedCategories);

					OrderUsedProduct orderUsedProduct = new OrderUsedProduct(product, product.getProductName(),
							product.getProductCode(), orderUsedCategories, orderUsedBrand, product.getRate(),
							/* product.getProductImage(), */
							/* product.getProductContentType(), */
							product.getThreshold(), product.getCurrentQuantity(), product.getDamageQuantity(), company, branch);

					sessionFactory.getCurrentSession().save(orderUsedProduct);

					proformaOrderProductDetails.setProduct(orderUsedProduct);
					proformaOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
					// counterOrderProductDetails.setPurchaseAmountOld(Float.parseFloat(totalOld));
					proformaOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
					proformaOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
					proformaOrderProductDetails.setType(type);

					proformaOrderProductDetailsList.add(proformaOrderProductDetails);
				}

				double discountAmtCut = 0, discountPerCut = 0;
				if (discountType.equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
					discountAmtCut = Double.parseDouble(discountAmount);
					discountPerCut = (Double.parseDouble(discountAmount) / totalAmountWithTax) * 100;
				} else {
					discountPerCut = Double.parseDouble(discountAmount);
					discountAmtCut = (Double.parseDouble(discountAmount) * totalAmountWithTax) / 100;
				}

				totalAmount = totalAmount - ((totalAmount * discountPerCut) / 100);
				totalAmount = MathUtils.round(totalAmount, 0);

				totalAmountWithTax = totalAmountWithTax - discountAmtCut;
				totalAmountWithTax = MathUtils.round(totalAmountWithTax, 0);

				if (businessNameId.equals("0")) {
					/*proformaOrder.setCustomerGstNumber(gstNo);
					proformaOrder.setCustomerMobileNumber(mobileNo);
					proformaOrder.setCustomerName(custName);*/
				} else {

					businessName = businessNameDAO.fetchBusinessForWebApp(businessNameId);
					proformaOrder.setBusinessName(businessName);
				}
				// CounterOrderIdGenerator counterOrderIdGenerator=new
				// CounterOrderIdGenerator(sessionFactory);
				proformaOrder.setProformaOrderId(counterOrderIdGenerator.generateProformaOrderId());

				EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
				proformaOrder.setEmployeeGk(employeeDetails.getEmployee());

				proformaOrder.setDateOfOrderTaken(new Date());
				proformaOrder.setDiscount(Double.parseDouble(discountAmount));
				proformaOrder.setDiscountType(discountType);

				// InvoiceNumberGenerate invoiceNumberGenerate=new
				// InvoiceNumberGenerate(sessionFactory);
				/*proformaOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());*/
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				/*try {
					if (paymentType.equals("PartialPay") || paymentType.equals("Credit")) {
						proformaOrder.setPaymentDueDate(dateFormat.parse(dueDate));
					}
				} catch (ParseException e) {
				}*/

				/*proformaOrder.setPayStatus(false);
				proformaOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));*/
				proformaOrder.setTotalAmount(totalAmount);
				proformaOrder.setTotalAmountWithTax(totalAmountWithTax);
				proformaOrder.setTotalQuantity(totalQuantity);

				if (transportationId != 0) {
					Transportation transportation = new Transportation();
					transportation.setId(transportationId);
					proformaOrder.setTransportation(transportation);
					proformaOrder.setVehicleNo(vehicalNumber);
					proformaOrder.setTransportationCharges(trasportationCharge);
					if (docketNumber.trim().equals("")) {
						docketNumber = null;
					}
					proformaOrder.setDocketNo(docketNumber);
				}
				proformaOrder.setBranch(branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds()));
				sessionFactory.getCurrentSession().save(proformaOrder);

				Calendar calendar = Calendar.getInstance();
				/*if (!businessNameId.equals("0")) {
					BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
					businessLedgerDetails.setBalanceAmount(totalAmountWithTax);
					businessLedgerDetails.setCounterOrder(proformaOrder);
					businessLedgerDetails.setCreditAmount(0);
					businessLedgerDetails.setDate(calendar.getTime());
					businessLedgerDetails.setDebitAmount(totalAmountWithTax);
					businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
					businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);
				}*/

				for (ProformaOrderProductDetails proformaOrderProductDetails : proformaOrderProductDetailsList) {
					proformaOrderProductDetails.setProformaOrder(proformaOrder);
					sessionFactory.getCurrentSession().save(proformaOrderProductDetails);
				}
				/*
				 * //Payment Details And ledger Details Commented For Proforma Order Start
				 * 
				 * double balance = totalAmountWithTax;
				if (paymentType.equals("InstantPay")) {
					paidAmount = (totalAmountWithTax) + "";
				}
				if (isUsableBalanceUsed.equals("true") && !paymentType.equals("Credit")) {

					double busiPendAmt = Double.parseDouble(usableBalance);
					double paidAmt = Double.parseDouble(paidAmount);
					double amountUsed = 0;
					if (busiPendAmt > totalAmountWithTax) {
						amountUsed = totalAmountWithTax;
					} else {
						amountUsed = busiPendAmt;
					}
					// deduct balance from business
					businessNameDAO.addRemoveBusinessPendingAmount("deduct", amountUsed, businessNameId);

					// business pending amount in out storing
					BusinessPendingAmountInOut businessPendingAmountInOut = new BusinessPendingAmountInOut(
							proformaOrder.getBusinessName(), amountUsed, "-", Constants.BUSINESS_PENDING_REFUND,
							proformaOrder.getCounterOrderId() + " Take Payment", new Date());
					businessPendingAmountInOut.setNonDirectRefundStatus(true);
					businessNameDAO.saveBusinessPendingAmountInOut(businessPendingAmountInOut);

					PaymentCounter paymentCounterPendingAmt = new PaymentCounter();
					Employee employee = new Employee();
					employee.setEmployeeId(getAppLoggedEmployeeId());
					paymentCounterPendingAmt.setEmployee(employee);
					paymentCounterPendingAmt.setCounterOrder(proformaOrder);
					// paymentCounter.setBalanceAmount(0);
					// try { paymentCounter.setDueDate(null); } catch (Exception e) {}
					paymentCounterPendingAmt.setCurrentAmountPaid(amountUsed);
					paymentCounterPendingAmt.setCurrentAmountRefund(0);
					// paymentCounter.setTotalAmountPaid(totalAmountWithTax);
					paymentCounterPendingAmt.setLastDueDate(new Date());
					paymentCounterPendingAmt.setPaidDate(new Date());
					paymentCounterPendingAmt.setPayType("Wallet");
					paymentCounterPendingAmt.setChequeClearStatus(true);

					sessionFactory.getCurrentSession().save(paymentCounterPendingAmt);

					balance = totalAmountWithTax - paymentCounterPendingAmt.getCurrentAmountPaid();

					String payMode = "Wallet";
					// ledger entry create
					
					 * double credit = paymentCounterPendingAmt.getCurrentAmountPaid();
					 * Branch branch =
					 * branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
					 * Ledger ledger = new Ledger(payMode,
					 * paymentCounterPendingAmt.getPaidDate(), null,
					 * paymentCounterPendingAmt.getCounterOrder().getCounterOrderId() +
					 * " Payment",
					 * (paymentCounterPendingAmt.getCounterOrder().getBusinessName() ==
					 * null) ?
					 * paymentCounterPendingAmt.getCounterOrder().getCustomerName() :
					 * paymentCounterPendingAmt.getCounterOrder().getBusinessName().
					 * getShopName(), 0, credit, 0, paymentCounterPendingAmt, branch);
					 * ledgerDAO.createLedgerEntry(ledger);
					 
					// ledger entry created

					if (proformaOrder.getBusinessName() != null) {
						calendar.add(Calendar.SECOND, 1);

						BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
						businessLedgerDetails.setBalanceAmount(balance);
						businessLedgerDetails.setCounterOrder(proformaOrder);
						businessLedgerDetails.setCreditAmount(paymentCounterPendingAmt.getCurrentAmountPaid());
						businessLedgerDetails.setDate(calendar.getTime());
						businessLedgerDetails.setDebitAmount(0);
						businessLedgerDetails.setPaymentCounter(paymentCounterPendingAmt);
						businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
						businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);

					}

					
					 * if(paymentType.equals("PartialPay")){ double
					 * totalPayingAmt=paidAmt+amountUsed;
					 * if(totalPayingAmt==totalAmountWithTax){ paymentType="InstantPay";
					 * }else{ paymentType="PartialPay"; paidAmount=totalPayingAmt+""; }
					 * }
					 

					if (paymentType.equals("InstantPay")) {
						paidAmount = (totalAmountWithTax - amountUsed) + "";
					}
					if (Float.parseFloat(paidAmount) == 0 && paymentType.equals("InstantPay")) {
						proformaOrder.setPayStatus(true);
						proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
						sessionFactory.getCurrentSession().update(proformaOrder);
					}
				}

				// payment section start
				PaymentCounter paymentCounter = new PaymentCounter();

				Employee employee = new Employee();
				employee.setEmployeeId(getAppLoggedEmployeeId());
				paymentCounter.setEmployee(employee);
				if (Float.parseFloat(paidAmount) != 0) {
					if (paymentType.equals("InstantPay")) {
						balance -= Double.parseDouble(paidAmount);
						paymentCounter.setCounterOrder(proformaOrder);
						// paymentCounter.setBalanceAmount(0);
						// try { paymentCounter.setDueDate(null); } catch (Exception e)
						// {}
						paymentCounter.setCurrentAmountPaid(Double.parseDouble(paidAmount));
						paymentCounter.setCurrentAmountRefund(0);
						// paymentCounter.setTotalAmountPaid(totalAmountWithTax);
						paymentCounter.setLastDueDate(new Date());
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if (payType.equals("Cash")) {
						} else if (payType.equals("Cheque")) {
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try {
								paymentCounter.setChequeDate(dateFormat.parse(chequeDate));
							} catch (Exception e) {
							}
						} else {
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						paymentCounter.setChequeClearStatus(true);

						sessionFactory.getCurrentSession().save(paymentCounter);
						String payMode = "";
						if (paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)) {
							payMode = "Cash";
						} else if (paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)) {
							payMode = paymentCounter.getBankName() + "-" + paymentCounter.getChequeNumber();
						} else {
							payMode = paymentCounter.getPaymentMethod().getPaymentMethodName() + "-"
									+ paymentCounter.getTransactionRefNo();
						}
						// ledger entry create
						double credit = paymentCounter.getCurrentAmountPaid();
						Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
						Ledger ledger = new Ledger(payMode, paymentCounter.getPaidDate(), null,
								paymentCounter.getCounterOrder().getCounterOrderId() + " Payment",
								(paymentCounter.getCounterOrder().getBusinessName() == null)
										? paymentCounter.getCounterOrder().getCustomerName()
										: paymentCounter.getCounterOrder().getBusinessName().getShopName(),
								0, credit, 0, paymentCounter, branch);
						ledgerDAO.createLedgerEntry(ledger);
						// ledger entry created

						proformaOrder.setPayStatus(true);
						proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
						sessionFactory.getCurrentSession().update(proformaOrder);

						if (proformaOrder.getBusinessName() != null) {
							calendar.add(Calendar.SECOND, 1);

							BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
							businessLedgerDetails.setBalanceAmount(balance);
							businessLedgerDetails.setCounterOrder(proformaOrder);
							businessLedgerDetails.setCreditAmount(paymentCounter.getCurrentAmountPaid());
							businessLedgerDetails.setDate(calendar.getTime());
							businessLedgerDetails.setDebitAmount(0);
							businessLedgerDetails.setPaymentCounter(paymentCounter);
							businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
							businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);

						}
					} else if (paymentType.equals("Credit")) {
						// No Payment

					} else { // PartialPay
						balance -= Float.parseFloat(paidAmount);
						paymentCounter.setCounterOrder(proformaOrder);
						// paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
						try {
							paymentCounter.setDueDate(dateFormat.parse(dueDate));
							paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
						} catch (Exception e) {
						}
						paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setCurrentAmountRefund(0);
						// paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if (payType.equals("Cash")) {
						} else if (payType.equals("Cheque")) {
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try {
								paymentCounter.setChequeDate(dateFormat.parse(chequeDate));
							} catch (Exception e) {
							}
						} else {
							paymentCounter.setTransactionRefNo(transactionRefId);
							paymentCounter.setComment(comment);
							paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
						}
						paymentCounter.setChequeClearStatus(false);
						sessionFactory.getCurrentSession().save(paymentCounter);

						String payMode = "";
						if (paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)) {
							payMode = "Cash";
						} else if (paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)) {
							payMode = paymentCounter.getBankName() + "-" + paymentCounter.getChequeNumber();
						} else {
							payMode = paymentCounter.getPaymentMethod().getPaymentMethodName() + "-"
									+ paymentCounter.getTransactionRefNo();
						}

						// ledger entry create
						double credit = paymentCounter.getCurrentAmountPaid();
						Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
						Ledger ledger = new Ledger(payMode, paymentCounter.getPaidDate(), null,
								paymentCounter.getCounterOrder().getCounterOrderId() + " Payment",
								(paymentCounter.getCounterOrder().getBusinessName() == null)
										? paymentCounter.getCounterOrder().getCustomerName()
										: paymentCounter.getCounterOrder().getBusinessName().getShopName(),
								0, credit, 0, paymentCounter, branch);
						ledgerDAO.createLedgerEntry(ledger);
						// ledger entry created

						proformaOrder.setPayStatus(false);
						proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
						sessionFactory.getCurrentSession().update(proformaOrder);

						if (proformaOrder.getBusinessName() != null) {
							calendar.add(Calendar.SECOND, 1);

							BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
							businessLedgerDetails.setBalanceAmount(balance);
							businessLedgerDetails.setCounterOrder(proformaOrder);
							businessLedgerDetails.setCreditAmount(paymentCounter.getCurrentAmountPaid());
							businessLedgerDetails.setDate(calendar.getTime());
							businessLedgerDetails.setDebitAmount(0);
							businessLedgerDetails.setPaymentCounter(paymentCounter);
							businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
							businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);
						}
					}

				}
				
				 * if(paymentCounter.getCounterOrder().getBusinessName()!=null){
				 * BusinessName
				 * businessName=businessNameDAO.fetchBusinessForWebApp(paymentCounter.
				 * getCounterOrder().getBusinessName().getBusinessNameId());
				 * paymentCounter.getCounterOrder().setBusinessName(businessName); }
				 

				// payment section end

				// update order used product update current quantity
				orderDetailsDAO.updateOrderUsedProductCurrentQuantity();


			//Payment Details And ledger Details Commented For Proforma Order End here
	*/			return proformaOrder.getProformaOrderId();
			}

		
			
			@Transactional
			public void deleteProformaProductDetailsById(long id){
				
				String hql="delete from ProformaOrderProductDetails where proformaOrder.proformaOrderPKId="+id;
				Query query=sessionFactory.getCurrentSession().createQuery(hql);
				query.executeUpdate();
						
				
			}
			//Update proforma Order
			//Proforma Order
				@Transactional
				public String updateProformaOrder(long proformaId,String productDetailsList, String businessNameId, long transportationId,
						String vehicalNumber, String docketNumber, String transactionRefId, String comment, long paymentMethodId,
						String discountType, String discountAmount,
						String orderComment,double trasportationCharge) {

					ProformaOrder proformaOrder = fetchProformaOrderById(proformaId);
					proformaOrder.setOrderComment(orderComment);
					String productDataList[] = productDetailsList.split("-");
					double totalAmount = 0, totalAmountWithTax = 0;
					long totalQuantity = 0;

					deleteProformaProductDetailsById(proformaId);
					List<ProformaOrderProductDetails> proformaOrderProductDetailsList = new ArrayList<>();

					for (String productData : productDataList) {
						String productDataSplit[] = productData.split(",");

						String productId = productDataSplit[0];
						String qty = productDataSplit[1];
						String mrp = productDataSplit[2];
						String total = productDataSplit[3];
						String type = productDataSplit[4];

						String discount = productDataSplit[5];
						String discountAmt = productDataSplit[6];
						String discountTyp = productDataSplit[7];

						String discountOld = productDataSplit[8];
						String discountAmtOld = productDataSplit[9];
						String totalOld = productDataSplit[10];

						String discountTypOld = productDataSplit[11];

						String totalUP = productDataSplit[12];

						Product product = productDAO.fetchProductForWebApp(Long.parseLong(productId));

						// less Current quantity
						/*product.setCurrentQuantity(product.getCurrentQuantity() - Long.parseLong(qty));
						productDAO.Update(product);*/
						// update daily stock
						/*productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);*/

						// CalculateProperTaxModel
						// calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp),
						// product.getCategories().getIgst());

						totalAmount += Float.parseFloat(totalUP);
						totalAmountWithTax += Float.parseFloat(total);
						totalQuantity += Long.parseLong(qty);

						ProformaOrderProductDetails proformaOrderProductDetails = new ProformaOrderProductDetails();
						proformaOrderProductDetails.setDiscount(Double.parseDouble(discountAmt));
						proformaOrderProductDetails.setDiscountPer(Double.parseDouble(discount));
						proformaOrderProductDetails.setDiscountType(discountTyp);
						/*
						 * counterOrderProductDetails.setDiscountOld(Double.parseDouble(
						 * discountAmtOld));
						 * counterOrderProductDetails.setDiscountPerOld(Double.parseDouble(
						 * discountOld));
						 * 
						 * counterOrderProductDetails.setDiscountTypeOld(discountTypOld);
						 */

						Company company = companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
						Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());

						OrderUsedBrand orderUsedBrand = new OrderUsedBrand();
						orderUsedBrand.setName(product.getBrand().getName());
						orderUsedBrand.setCompany(company);
						orderUsedBrand.setBranch(branch);
						sessionFactory.getCurrentSession().save(orderUsedBrand);

						OrderUsedCategories orderUsedCategories = new OrderUsedCategories(product.getCategories().getCategoryName(),
								product.getCategories().getHsnCode(), product.getCategories().getCgst(),
								product.getCategories().getSgst(), product.getCategories().getIgst(), company, branch);
						sessionFactory.getCurrentSession().save(orderUsedCategories);

						OrderUsedProduct orderUsedProduct = new OrderUsedProduct(product, product.getProductName(),
								product.getProductCode(), orderUsedCategories, orderUsedBrand, product.getRate(),
								/* product.getProductImage(), */
								/* product.getProductContentType(), */
								product.getThreshold(), product.getCurrentQuantity(), product.getDamageQuantity(), company, branch);

						sessionFactory.getCurrentSession().save(orderUsedProduct);

						proformaOrderProductDetails.setProduct(orderUsedProduct);
						proformaOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
						// counterOrderProductDetails.setPurchaseAmountOld(Float.parseFloat(totalOld));
						proformaOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
						proformaOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
						proformaOrderProductDetails.setType(type);

						proformaOrderProductDetailsList.add(proformaOrderProductDetails);
					}

					double discountAmtCut = 0, discountPerCut = 0;
					if (discountType.equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
						discountAmtCut = Double.parseDouble(discountAmount);
						discountPerCut = (Double.parseDouble(discountAmount) / totalAmountWithTax) * 100;
					} else {
						discountPerCut = Double.parseDouble(discountAmount);
						discountAmtCut = (Double.parseDouble(discountAmount) * totalAmountWithTax) / 100;
					}

					totalAmount = totalAmount - ((totalAmount * discountPerCut) / 100);
					totalAmount = MathUtils.round(totalAmount, 0);

					totalAmountWithTax = totalAmountWithTax - discountAmtCut;
					totalAmountWithTax = MathUtils.round(totalAmountWithTax, 0);

					if (businessNameId.equals("0")) {
						/*proformaOrder.setCustomerGstNumber(gstNo);
						proformaOrder.setCustomerMobileNumber(mobileNo);
						proformaOrder.setCustomerName(custName);*/
					} else {

						businessName = businessNameDAO.fetchBusinessForWebApp(businessNameId);
						proformaOrder.setBusinessName(businessName);
					}
					// CounterOrderIdGenerator counterOrderIdGenerator=new
					// CounterOrderIdGenerator(sessionFactory);
					
					//No Need to generate new ProformaId while updating
					//proformaOrder.setProformaOrderId(counterOrderIdGenerator.generateProformaOrderId());

					EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
					proformaOrder.setEmployeeGk(employeeDetails.getEmployee());

					proformaOrder.setDateOfOrderTaken(new Date());
					proformaOrder.setDiscount(Double.parseDouble(discountAmount));
					proformaOrder.setDiscountType(discountType);

					// InvoiceNumberGenerate invoiceNumberGenerate=new
					// InvoiceNumberGenerate(sessionFactory);
					/*proformaOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());*/
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					/*try {
						if (paymentType.equals("PartialPay") || paymentType.equals("Credit")) {
							proformaOrder.setPaymentDueDate(dateFormat.parse(dueDate));
						}
					} catch (ParseException e) {
					}*/

					/*proformaOrder.setPayStatus(false);
					proformaOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));*/
					proformaOrder.setTotalAmount(totalAmount);
					proformaOrder.setTotalAmountWithTax(totalAmountWithTax);
					proformaOrder.setTotalQuantity(totalQuantity);

					if (transportationId != 0) {
						Transportation transportation = new Transportation();
						transportation.setId(transportationId);
						proformaOrder.setTransportation(transportation);
						proformaOrder.setVehicleNo(vehicalNumber);
						proformaOrder.setTransportationCharges(trasportationCharge);
						if (docketNumber.trim().equals("")) {
							docketNumber = null;
						}
						proformaOrder.setDocketNo(docketNumber);
					}
					proformaOrder.setBranch(branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds()));
					sessionFactory.getCurrentSession().update(proformaOrder);

					Calendar calendar = Calendar.getInstance();
					/*if (!businessNameId.equals("0")) {
						BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
						businessLedgerDetails.setBalanceAmount(totalAmountWithTax);
						businessLedgerDetails.setCounterOrder(proformaOrder);
						businessLedgerDetails.setCreditAmount(0);
						businessLedgerDetails.setDate(calendar.getTime());
						businessLedgerDetails.setDebitAmount(totalAmountWithTax);
						businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
						businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);
					}*/

					for (ProformaOrderProductDetails proformaOrderProductDetails : proformaOrderProductDetailsList) {
						proformaOrderProductDetails.setProformaOrder(proformaOrder);
						sessionFactory.getCurrentSession().save(proformaOrderProductDetails);
					}
					/*
					 * //Payment Details And ledger Details Commented For Proforma Order Start
					 * 
					 * double balance = totalAmountWithTax;
					if (paymentType.equals("InstantPay")) {
						paidAmount = (totalAmountWithTax) + "";
					}
					if (isUsableBalanceUsed.equals("true") && !paymentType.equals("Credit")) {

						double busiPendAmt = Double.parseDouble(usableBalance);
						double paidAmt = Double.parseDouble(paidAmount);
						double amountUsed = 0;
						if (busiPendAmt > totalAmountWithTax) {
							amountUsed = totalAmountWithTax;
						} else {
							amountUsed = busiPendAmt;
						}
						// deduct balance from business
						businessNameDAO.addRemoveBusinessPendingAmount("deduct", amountUsed, businessNameId);

						// business pending amount in out storing
						BusinessPendingAmountInOut businessPendingAmountInOut = new BusinessPendingAmountInOut(
								proformaOrder.getBusinessName(), amountUsed, "-", Constants.BUSINESS_PENDING_REFUND,
								proformaOrder.getCounterOrderId() + " Take Payment", new Date());
						businessPendingAmountInOut.setNonDirectRefundStatus(true);
						businessNameDAO.saveBusinessPendingAmountInOut(businessPendingAmountInOut);

						PaymentCounter paymentCounterPendingAmt = new PaymentCounter();
						Employee employee = new Employee();
						employee.setEmployeeId(getAppLoggedEmployeeId());
						paymentCounterPendingAmt.setEmployee(employee);
						paymentCounterPendingAmt.setCounterOrder(proformaOrder);
						// paymentCounter.setBalanceAmount(0);
						// try { paymentCounter.setDueDate(null); } catch (Exception e) {}
						paymentCounterPendingAmt.setCurrentAmountPaid(amountUsed);
						paymentCounterPendingAmt.setCurrentAmountRefund(0);
						// paymentCounter.setTotalAmountPaid(totalAmountWithTax);
						paymentCounterPendingAmt.setLastDueDate(new Date());
						paymentCounterPendingAmt.setPaidDate(new Date());
						paymentCounterPendingAmt.setPayType("Wallet");
						paymentCounterPendingAmt.setChequeClearStatus(true);

						sessionFactory.getCurrentSession().save(paymentCounterPendingAmt);

						balance = totalAmountWithTax - paymentCounterPendingAmt.getCurrentAmountPaid();

						String payMode = "Wallet";
						// ledger entry create
						
						 * double credit = paymentCounterPendingAmt.getCurrentAmountPaid();
						 * Branch branch =
						 * branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
						 * Ledger ledger = new Ledger(payMode,
						 * paymentCounterPendingAmt.getPaidDate(), null,
						 * paymentCounterPendingAmt.getCounterOrder().getCounterOrderId() +
						 * " Payment",
						 * (paymentCounterPendingAmt.getCounterOrder().getBusinessName() ==
						 * null) ?
						 * paymentCounterPendingAmt.getCounterOrder().getCustomerName() :
						 * paymentCounterPendingAmt.getCounterOrder().getBusinessName().
						 * getShopName(), 0, credit, 0, paymentCounterPendingAmt, branch);
						 * ledgerDAO.createLedgerEntry(ledger);
						 
						// ledger entry created

						if (proformaOrder.getBusinessName() != null) {
							calendar.add(Calendar.SECOND, 1);

							BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
							businessLedgerDetails.setBalanceAmount(balance);
							businessLedgerDetails.setCounterOrder(proformaOrder);
							businessLedgerDetails.setCreditAmount(paymentCounterPendingAmt.getCurrentAmountPaid());
							businessLedgerDetails.setDate(calendar.getTime());
							businessLedgerDetails.setDebitAmount(0);
							businessLedgerDetails.setPaymentCounter(paymentCounterPendingAmt);
							businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
							businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);

						}

						
						 * if(paymentType.equals("PartialPay")){ double
						 * totalPayingAmt=paidAmt+amountUsed;
						 * if(totalPayingAmt==totalAmountWithTax){ paymentType="InstantPay";
						 * }else{ paymentType="PartialPay"; paidAmount=totalPayingAmt+""; }
						 * }
						 

						if (paymentType.equals("InstantPay")) {
							paidAmount = (totalAmountWithTax - amountUsed) + "";
						}
						if (Float.parseFloat(paidAmount) == 0 && paymentType.equals("InstantPay")) {
							proformaOrder.setPayStatus(true);
							proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
							sessionFactory.getCurrentSession().update(proformaOrder);
						}
					}

					// payment section start
					PaymentCounter paymentCounter = new PaymentCounter();

					Employee employee = new Employee();
					employee.setEmployeeId(getAppLoggedEmployeeId());
					paymentCounter.setEmployee(employee);
					if (Float.parseFloat(paidAmount) != 0) {
						if (paymentType.equals("InstantPay")) {
							balance -= Double.parseDouble(paidAmount);
							paymentCounter.setCounterOrder(proformaOrder);
							// paymentCounter.setBalanceAmount(0);
							// try { paymentCounter.setDueDate(null); } catch (Exception e)
							// {}
							paymentCounter.setCurrentAmountPaid(Double.parseDouble(paidAmount));
							paymentCounter.setCurrentAmountRefund(0);
							// paymentCounter.setTotalAmountPaid(totalAmountWithTax);
							paymentCounter.setLastDueDate(new Date());
							paymentCounter.setPaidDate(new Date());
							paymentCounter.setPayType(payType);
							if (payType.equals("Cash")) {
							} else if (payType.equals("Cheque")) {
								paymentCounter.setBankName(bankName);
								paymentCounter.setChequeNumber(chequeNumber);
								try {
									paymentCounter.setChequeDate(dateFormat.parse(chequeDate));
								} catch (Exception e) {
								}
							} else {
								paymentCounter.setTransactionRefNo(transactionRefId);
								paymentCounter.setComment(comment);
								paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
							}
							paymentCounter.setChequeClearStatus(true);

							sessionFactory.getCurrentSession().save(paymentCounter);
							String payMode = "";
							if (paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)) {
								payMode = "Cash";
							} else if (paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)) {
								payMode = paymentCounter.getBankName() + "-" + paymentCounter.getChequeNumber();
							} else {
								payMode = paymentCounter.getPaymentMethod().getPaymentMethodName() + "-"
										+ paymentCounter.getTransactionRefNo();
							}
							// ledger entry create
							double credit = paymentCounter.getCurrentAmountPaid();
							Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
							Ledger ledger = new Ledger(payMode, paymentCounter.getPaidDate(), null,
									paymentCounter.getCounterOrder().getCounterOrderId() + " Payment",
									(paymentCounter.getCounterOrder().getBusinessName() == null)
											? paymentCounter.getCounterOrder().getCustomerName()
											: paymentCounter.getCounterOrder().getBusinessName().getShopName(),
									0, credit, 0, paymentCounter, branch);
							ledgerDAO.createLedgerEntry(ledger);
							// ledger entry created

							proformaOrder.setPayStatus(true);
							proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
							sessionFactory.getCurrentSession().update(proformaOrder);

							if (proformaOrder.getBusinessName() != null) {
								calendar.add(Calendar.SECOND, 1);

								BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
								businessLedgerDetails.setBalanceAmount(balance);
								businessLedgerDetails.setCounterOrder(proformaOrder);
								businessLedgerDetails.setCreditAmount(paymentCounter.getCurrentAmountPaid());
								businessLedgerDetails.setDate(calendar.getTime());
								businessLedgerDetails.setDebitAmount(0);
								businessLedgerDetails.setPaymentCounter(paymentCounter);
								businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
								businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);

							}
						} else if (paymentType.equals("Credit")) {
							// No Payment

						} else { // PartialPay
							balance -= Float.parseFloat(paidAmount);
							paymentCounter.setCounterOrder(proformaOrder);
							// paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
							try {
								paymentCounter.setDueDate(dateFormat.parse(dueDate));
								paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
							} catch (Exception e) {
							}
							paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
							paymentCounter.setCurrentAmountRefund(0);
							// paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
							paymentCounter.setPaidDate(new Date());
							paymentCounter.setPayType(payType);
							if (payType.equals("Cash")) {
							} else if (payType.equals("Cheque")) {
								paymentCounter.setBankName(bankName);
								paymentCounter.setChequeNumber(chequeNumber);
								try {
									paymentCounter.setChequeDate(dateFormat.parse(chequeDate));
								} catch (Exception e) {
								}
							} else {
								paymentCounter.setTransactionRefNo(transactionRefId);
								paymentCounter.setComment(comment);
								paymentCounter.setPaymentMethod(paymentDAO.fetchPaymentMethodById(paymentMethodId));
							}
							paymentCounter.setChequeClearStatus(false);
							sessionFactory.getCurrentSession().save(paymentCounter);

							String payMode = "";
							if (paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS)) {
								payMode = "Cash";
							} else if (paymentCounter.getPayType().equals(Constants.CHEQUE_PAY_STATUS)) {
								payMode = paymentCounter.getBankName() + "-" + paymentCounter.getChequeNumber();
							} else {
								payMode = paymentCounter.getPaymentMethod().getPaymentMethodName() + "-"
										+ paymentCounter.getTransactionRefNo();
							}

							// ledger entry create
							double credit = paymentCounter.getCurrentAmountPaid();
							Branch branch = branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
							Ledger ledger = new Ledger(payMode, paymentCounter.getPaidDate(), null,
									paymentCounter.getCounterOrder().getCounterOrderId() + " Payment",
									(paymentCounter.getCounterOrder().getBusinessName() == null)
											? paymentCounter.getCounterOrder().getCustomerName()
											: paymentCounter.getCounterOrder().getBusinessName().getShopName(),
									0, credit, 0, paymentCounter, branch);
							ledgerDAO.createLedgerEntry(ledger);
							// ledger entry created

							proformaOrder.setPayStatus(false);
							proformaOrder = (CounterOrder) sessionFactory.getCurrentSession().merge(proformaOrder);
							sessionFactory.getCurrentSession().update(proformaOrder);

							if (proformaOrder.getBusinessName() != null) {
								calendar.add(Calendar.SECOND, 1);

								BusinessLedgerDetails businessLedgerDetails = new BusinessLedgerDetails();
								businessLedgerDetails.setBalanceAmount(balance);
								businessLedgerDetails.setCounterOrder(proformaOrder);
								businessLedgerDetails.setCreditAmount(paymentCounter.getCurrentAmountPaid());
								businessLedgerDetails.setDate(calendar.getTime());
								businessLedgerDetails.setDebitAmount(0);
								businessLedgerDetails.setPaymentCounter(paymentCounter);
								businessLedgerDetails.setOrderType(Constants.BL_COUNTER_ORDER);
								businessNameDAO.addBusinessLedgerEntry(businessLedgerDetails, businessNameId);
							}
						}

					}
					
					 * if(paymentCounter.getCounterOrder().getBusinessName()!=null){
					 * BusinessName
					 * businessName=businessNameDAO.fetchBusinessForWebApp(paymentCounter.
					 * getCounterOrder().getBusinessName().getBusinessNameId());
					 * paymentCounter.getCounterOrder().setBusinessName(businessName); }
					 

					// payment section end

					// update order used product update current quantity
					orderDetailsDAO.updateOrderUsedProductCurrentQuantity();


				//Payment Details And ledger Details Commented For Proforma Order End here
		*/			return proformaOrder.getProformaOrderId();
				}

				
			//Fetch Proforma Order Details By proformaOrderId
			@Transactional
			public ProformaOrder fetchProformaOrder(String proformaOrderId) {
				// TODO Auto-generated method stub
				String hql = "from ProformaOrder where proformaOrderId='" + proformaOrderId + "'"
						+ " and employeeGk.company.companyId in (" + getSessionSelectedCompaniesIds() + ")";
				hql = modifyQueryAccordingSessionSelectedBranchIds(hql);
				hql = modifyQueryAccordingSessionSelectedBranchIds(hql);
				Query query = sessionFactory.getCurrentSession().createQuery(hql);
				List<ProformaOrder> proformaOrderList = (List<ProformaOrder>) query.list();
				if (proformaOrderList.isEmpty()) {
					return null;
				}
				return proformaOrderList.get(0);
			}
			
			//Fetch Proforma Order Product List By proforma Od
			@Transactional
			public List<ProformaOrderProductDetails> fetchProformaOrderProductDetails(String proformaOrderId) {
				String hql = "from ProformaOrderProductDetails where proformaOrder.proformaOrderId='" + proformaOrderId + "'";
				hql += " and proformaOrder.employeeGk.company.companyId=" + getSessionSelectedCompaniesIds();
				hql += " and proformaOrder.branch.branchId in (" + getSessionSelectedBranchIds() + ")";
				Query query = sessionFactory.getCurrentSession().createQuery(hql);
				List<ProformaOrderProductDetails> proformaOrderProductDetailsList = (List<ProformaOrderProductDetails>)query.list();
				if (proformaOrderProductDetailsList.isEmpty()) {
					return null;
				}

				return proformaOrderProductDetailsList;
			}
			
			//Fetch Proforma Order Details By proformaOrderId
					@Transactional
					public ProformaOrder fetchProformaOrderById(long id) {
						// TODO Auto-generated method stub
						String hql = "from ProformaOrder where proformaOrderPKId='" + id + "'"
								+ " and employeeGk.company.companyId in (" + getSessionSelectedCompaniesIds() + ")";
						hql = modifyQueryAccordingSessionSelectedBranchIds(hql);
						hql = modifyQueryAccordingSessionSelectedBranchIds(hql);
						Query query = sessionFactory.getCurrentSession().createQuery(hql);
						List<ProformaOrder> proformaOrderList = (List<ProformaOrder>) query.list();
						if (proformaOrderList.isEmpty()) {
							return null;
						}
						return proformaOrderList.get(0);
					}
					
					//Fetch Proforma Order Product List By proforma Od
					@Transactional
					public List<ProformaOrderProductDetails> fetchProformaOrderProductDetailsById(long id) {
						String hql = "from ProformaOrderProductDetails where proformaOrder.proformaOrderPKId='" + id + "'";
						hql += " and proformaOrder.employeeGk.company.companyId=" + getSessionSelectedCompaniesIds();
						hql += " and proformaOrder.branch.branchId in (" + getSessionSelectedBranchIds() + ")";
						Query query = sessionFactory.getCurrentSession().createQuery(hql);
						List<ProformaOrderProductDetails> proformaOrderProductDetailsList = (List<ProformaOrderProductDetails>)query.list();
						if (proformaOrderProductDetailsList.isEmpty()) {
							return null;
						}

						return proformaOrderProductDetailsList;
					}
			
			
			
			//Proforma Order Invoice
				@Transactional
				public BillPrintDataModel fetchProformaOrderBillPrintData(String proformaOrderId) {

					// get counter order Details
					ProformaOrder proformaOrder = fetchProformaOrder(proformaOrderId);

					// get counter order product list
					List<ProformaOrderProductDetails> proformaOrderProductDetailsList = fetchProformaOrderProductDetails(proformaOrderId);
					BusinessName businessName = null;
					if (proformaOrder.getBusinessName() != null) {
						businessName = proformaOrder.getBusinessName();
					}

					String invoiceNumber = "--";
					String orderDate = new SimpleDateFormat("dd-MM-yyyy").format(proformaOrder.getDateOfOrderTaken());
					String deliveryDate = new SimpleDateFormat("dd-MM-yyyy").format(proformaOrder.getDateOfOrderTaken());

					List<ProductListForBill> productListForBillList = new ArrayList<>();

					float totalAmountWithoutTax = 0;

					float cGSTAmount = 0;
					float iGSTAmount = 0;
					float sGSTAmount = 0;

					long totalQuantity = 0;
					float totalAmountWithTax = 0;
					String totalAmountWithTaxInWord = "";

					List<CategoryWiseAmountForBill> categoryWiseAmountForBills = new ArrayList<>();

					float totalAmount = 0;
					float taxAmount = 0;
					String taxAmountInWord = "";

					float totalCGSTAmount = 0;
					float totalIGSTAmount = 0;
					float totalSGSTAmount = 0;

					// DecimalFormat decimalFormat=new DecimalFormat("#0.00");
					// DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
					long srno = 1;
					// create product wise details
					for (ProformaOrderProductDetails proformaOrderProductDetails : proformaOrderProductDetailsList) {
						long qty = proformaOrderProductDetails.getPurchaseQuantity();
						double total = qty * proformaOrderProductDetails.getSellingRate();
						double discAmt = 0;//(total * proformaOrderProductDetails.getDiscountPer()) / 100;
						if(proformaOrderProductDetails.getDiscountType().equals("Amount")){
							discAmt = (float)proformaOrderProductDetails.getDiscount();
						}else{
							discAmt = (float)(total * proformaOrderProductDetails.getDiscountPer()) / 100;
						}
						total = total - discAmt;

						// float amountWithoutTax=0;
						CalculateProperTaxModel calculateProperTaxModel = productDAO.calculateProperAmountModel(total,
								proformaOrderProductDetails.getProduct().getCategories().getIgst());
						CalculateProperTaxModel calculateProperTaxModel2 = productDAO.calculateProperAmountModel(
								proformaOrderProductDetails.getSellingRate(),
								proformaOrderProductDetails.getProduct().getCategories().getIgst());

						if (proformaOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
							// amountWithoutTax=calculateProperTaxModel.getUnitprice();

							/*
							 * float
							 * igst=counterOrderProductDetails.getProduct().getCategories().
							 * getIgst(); float
							 * cgst=counterOrderProductDetails.getProduct().getCategories().
							 * getCgst(); float
							 * sgst=counterOrderProductDetails.getProduct().getCategories().
							 * getSgst();
							 * 
							 * float rate=calculateProperTaxModel.getUnitprice();
							 */
							long issuedQuantity = 1;

							if (businessName != null) {
								if (businessName.getTaxType().equals(Constants.INTRA_STATUS)) {
									cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
									iGSTAmount += 0;
									sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
								} else {
									cGSTAmount += 0;
									iGSTAmount += calculateProperTaxModel.getIgst() * issuedQuantity;
									sGSTAmount += 0;
								}
							} else {// external customer - assumed intra tax type
								cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
								iGSTAmount += 0;
								sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
							}
						}

						totalQuantity += proformaOrderProductDetails.getPurchaseQuantity();

						float discountAmt = 0;
						// double
						// productamountWithTax=counterOrderProductDetails.getProduct().getRate()*counterOrderProductDetails.getPurchaseQuantity();
						// discountAmt=(float)(productamountWithTax*counterOrderProductDetails.getDiscountPer())/100;
						double totalGrossAmount = proformaOrderProductDetails.getSellingRate()
								* (proformaOrderProductDetails.getPurchaseQuantity());
						discountAmt =0; //(float) (totalGrossAmount * proformaOrderProductDetails.getDiscountPer() / 100);
						if(proformaOrderProductDetails.getDiscountType().equals("Amount")){
							discountAmt = (float)proformaOrderProductDetails.getDiscount();
						}else{
							discountAmt = (float)(totalGrossAmount * proformaOrderProductDetails.getDiscountPer()) / 100;
						}
						float totalAmtWithTax = (float) (proformaOrderProductDetails.getSellingRate()
								* proformaOrderProductDetails.getPurchaseQuantity());
						productListForBillList
								.add(new ProductListForBill(String.valueOf(srno),
										proformaOrderProductDetails.getProduct().getProductName() + (proformaOrderProductDetails
												.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE) ? "-(Free)" : ""),
										proformaOrderProductDetails.getProduct().getCategories().getHsnCode(),
										String.valueOf(((long) proformaOrderProductDetails.getProduct().getCategories().getIgst())),
										String.valueOf(proformaOrderProductDetails.getPurchaseQuantity()),
										(proformaOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE) ? "0"
												: proformaOrderProductDetails.getSellingRate() + " "),
										String.valueOf(
												MathUtils.roundFromFloat((float) ((proformaOrderProductDetails.getPurchaseQuantity())
														* proformaOrderProductDetails.getSellingRate()), 2)),
										String.valueOf(MathUtils.roundFromFloat((float) discountAmt, 2)),
										String.valueOf(
												MathUtils.roundFromFloat((float) proformaOrderProductDetails.getDiscountPer(), 2)),
										String.valueOf(MathUtils.roundFromFloat((float) totalGrossAmount - discountAmt, 2))));
						totalAmountWithoutTax += MathUtils.roundFromFloat((float) totalGrossAmount - discountAmt, 2);
						srno++;
						// double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
						// totalAmountWithTax+=amountWithoutTax+tax;

					}
					// total amount with tax basis of product details
					for (ProductListForBill productListForBill2 : productListForBillList) {
						totalAmountWithTax += Double.parseDouble(productListForBill2.getNetPayable());
					}
					totalAmountWithTax = MathUtils.roundFromFloat(totalAmountWithTax, 2);
					cGSTAmount = MathUtils.roundFromFloat(cGSTAmount, 3);
					iGSTAmount = MathUtils.roundFromFloat(iGSTAmount, 2);
					sGSTAmount = MathUtils.roundFromFloat(sGSTAmount, 3);

					// totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;

					// get unique category
					Set<OrderUsedCategories> categoriesList = new HashSet<>();
					for (ProformaOrderProductDetails proformaOrderProductDetails : proformaOrderProductDetailsList) {
						if (proformaOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
							categoriesList.add(proformaOrderProductDetails.getProduct().getCategories());
						}
					}

					// category wise taxslab and amount find
					Set<String> categoriesHsnCodeList = new HashSet<>();
					for (OrderUsedCategories categories : categoriesList) {
						String hsncode = "";
						float taxableValue = 0;
						float cgstPercentage = 0;
						float igstPercentage = 0;
						float sgstPercentage = 0;
						float cgstRate = 0;
						float igstRate = 0;
						float sgstRate = 0;

						cGSTAmount = 0;
						iGSTAmount = 0;
						sGSTAmount = 0;

						boolean gotDuplicateHsnCode = false;

						for (String hsnCode : categoriesHsnCodeList) {
							if (categories.getHsnCode().equals(hsnCode)) {
								gotDuplicateHsnCode = true;
							}
						}
						if (gotDuplicateHsnCode) {
							continue;
						}

						categoriesHsnCodeList.add(categories.getHsnCode());

						for (ProformaOrderProductDetails proformaOrderProductDetails : proformaOrderProductDetailsList) {
							if (proformaOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE)) {
								if (categories.getHsnCode()
										.equals(proformaOrderProductDetails.getProduct().getCategories().getHsnCode())) {
									long qty = proformaOrderProductDetails.getPurchaseQuantity();
									double total = qty * proformaOrderProductDetails.getSellingRate();
									double discAmt=0;
									if(proformaOrderProductDetails.getDiscountType().equals("Amount")){
										discAmt = proformaOrderProductDetails.getDiscount();
									}else{
										discAmt = (total * proformaOrderProductDetails.getDiscountPer()) / 100;
									}
									total = total - discAmt;

									CalculateProperTaxModel calculateProperTaxModel = productDAO.calculateProperAmountModel(total,
											proformaOrderProductDetails.getProduct().getCategories().getIgst());
									taxableValue += calculateProperTaxModel.getUnitprice();

									/*
									 * float igst=counterOrderProductDetails.getProduct().
									 * getCategories().getIgst(); float
									 * cgst=counterOrderProductDetails.getProduct().
									 * getCategories().getCgst(); float
									 * sgst=counterOrderProductDetails.getProduct().
									 * getCategories().getSgst(); float
									 * rate=calculateProperTaxModel.getUnitprice();
									 */
									long issuedQuantity = 1;

									if (businessName != null) {
										if (businessName.getTaxType().equals(Constants.INTRA_STATUS)) {
											cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
											iGSTAmount += 0;
											sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
										} else {
											cGSTAmount += 0;
											iGSTAmount += calculateProperTaxModel.getIgst() * issuedQuantity;
											sGSTAmount += 0;
										}
									} else {// external customer - assumed intra tax type
										cGSTAmount += calculateProperTaxModel.getCgst() * issuedQuantity;
										iGSTAmount += 0;
										sGSTAmount += calculateProperTaxModel.getSgst() * issuedQuantity;
									}
									cgstPercentage = proformaOrderProductDetails.getProduct().getCategories().getCgst();
									igstPercentage = proformaOrderProductDetails.getProduct().getCategories().getIgst();
									sgstPercentage = proformaOrderProductDetails.getProduct().getCategories().getSgst();
									hsncode = proformaOrderProductDetails.getProduct().getCategories().getHsnCode();
								}
							}
						}

						categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(hsncode,
								MathUtils.roundFromFloat(taxableValue, 2) + "", MathUtils.roundFromFloat(cgstPercentage, 2) + "",
								MathUtils.roundFromFloat(cGSTAmount, 3) + "", MathUtils.roundFromFloat(igstPercentage, 2) + "",
								MathUtils.roundFromFloat(iGSTAmount, 2) + "", MathUtils.roundFromFloat(sgstPercentage, 2) + "",
								MathUtils.roundFromFloat(sGSTAmount, 3) + ""));

						totalAmount += taxableValue;
						totalCGSTAmount += cGSTAmount;
						totalSGSTAmount += sGSTAmount;
						totalIGSTAmount += iGSTAmount;
					}
					taxAmount = ((MathUtils.roundFromFloat(totalSGSTAmount, 2)) + (MathUtils.roundFromFloat(totalCGSTAmount, 2))
							+ (MathUtils.roundFromFloat(totalIGSTAmount, 3)));
					// total amount without tax in word
					//taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));

					totalAmountWithTax = (MathUtils.roundFromFloat(totalAmountWithTax, 2));
					// find round of amount
					/*
					 * float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax; float
					 * roundOf; String roundOfAmount=""; if(decimalAmount==0) {
					 * roundOf=0.0f; roundOfAmount=""+MathUtils.roundFromFloat(roundOf,2);
					 * totalAmountWithTax=totalAmountWithTax+roundOf; } else
					 * if(decimalAmount>=0.5) { roundOf=1-decimalAmount;
					 * roundOfAmount="+"+MathUtils.roundFromFloat(roundOf,2);
					 * totalAmountWithTax=totalAmountWithTax+roundOf; } else {
					 * roundOf=decimalAmount;
					 * roundOfAmount="-"+MathUtils.roundFromFloat(roundOf,2);
					 * totalAmountWithTax=totalAmountWithTax-roundOf; }
					 */

					double discountAmtCut = 0, discountPerCut = 0;
					if (proformaOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
						discountAmtCut = proformaOrder.getDiscount();
						discountPerCut = (proformaOrder.getDiscount() / totalAmountWithTax) * 100;
					} else {
						discountPerCut = proformaOrder.getDiscount();
						discountAmtCut = (proformaOrder.getDiscount() * totalAmountWithTax) / 100;
					}

					/*
					 * totalAmount=totalAmount-((totalAmount*discountPerCut)/100);
					 * totalAmount=MathUtils.round(totalAmount, 0);
					 */

					double totalAmountWithTaxWithDisc = totalAmountWithTax - discountAmtCut;

					float roundOffAmount = (float) (MathUtils.round(totalAmountWithTaxWithDisc, 0)
							- MathUtils.round(totalAmountWithTaxWithDisc, 2));
					String roundAmt = "";

					if (roundOffAmount > 0) {
						roundAmt = "+" + roundOffAmount;
					} else if (roundOffAmount < 0) {
						roundAmt = roundOffAmount + "";
					} else {
						roundAmt = "0";
					}
					String totalAmountWithTaxRoundOff = MathUtils.round(totalAmountWithTaxWithDisc, 0) + "";

					double totalAmountWithTaxWithDiscNonRoundOff = totalAmountWithTaxWithDisc;
					totalAmountWithTaxWithDisc = MathUtils.round(totalAmountWithTaxWithDisc, 0);
					/*
					 * if(decimalAmount>0.0f) { roundOf=1-decimalAmount;
					 * roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
					 * totalAmountWithTax=totalAmountWithTax-roundOf; }
					 */

					totalAmountWithTaxInWord = NumberToWordsConverter
							.convertWithPaisa((float) MathUtils.round(totalAmountWithTaxWithDisc, 0));

					discountAmtCut = MathUtils.round(discountAmtCut, 2);
					discountPerCut = MathUtils.round(discountPerCut, 2);
					totalAmountWithTaxWithDiscNonRoundOff = MathUtils.round(totalAmountWithTaxWithDiscNonRoundOff, 2);

					//Bank Details is not shown in Bluesquare Invoice
					/*List<BankDetailsForInvoice> bankDetailsForInvoiceList = bankDetailsForInvoiceDAO
							.fetchBankDetailsForInvoiceList();*/

					/**
					 * calculate category wise block under discount and net amount 
					 */
					double totalAmountDisc=0;
					double totalCGSTAmountDisc=0;
					double totalIGSTAmountDisc=0;
					double totalSGSTAmountDisc=0;		
					double netTotalAmount=0;
					double netTotalCGSTAmount=0;
					double netTotalIGSTAmount=0;
					double netTotalSGSTAmount=0;
					if (proformaOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
						
						 totalAmountDisc=((discountAmtCut/totalAmountWithTax)*totalAmount);
						 totalCGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalCGSTAmount);
						 totalIGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalIGSTAmount);
						 totalSGSTAmountDisc=((discountAmtCut/totalAmountWithTax)*totalSGSTAmount);
						
						 netTotalAmount=totalAmount-totalAmountDisc;
						 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
						 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
						 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
						
					}else{
						 totalAmountDisc=(discountPerCut*totalAmount)/100;
						 totalCGSTAmountDisc=(discountPerCut*totalCGSTAmount)/100;
						 totalIGSTAmountDisc=(discountPerCut*totalIGSTAmount)/100;
						 totalSGSTAmountDisc=(discountPerCut*totalSGSTAmount)/100;
						
						 netTotalAmount=totalAmount-totalAmountDisc;
						 netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
						 netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
						 netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
					}
					
					taxAmount = ((MathUtils.roundFromFloat((float)netTotalSGSTAmount, 3)) + (MathUtils.roundFromFloat((float)netTotalCGSTAmount, 3))
							+ (MathUtils.roundFromFloat((float)netTotalIGSTAmount, 2)));		
					taxAmountInWord = NumberToWordsConverter.convertWithPaisa((MathUtils.roundFromFloat(taxAmount, 2)));

					if (businessName != null) {
						
						/*return new BillPrintDataModel(orderNumber,invoiceNumber, creditNoteNumber,returnDate, orderDate,addressLineList,businessName, employeeGKCounterOrder,
						 * 								 deliveryDate, productListForBill, cGSTAmount,
						 * 								 iGSTAmount,
						 * 								 sGSTAmount, roundOffAmount,
						 * 								 totalAmountWithoutTax, totalQuantity,
						 * 								 totalAmountWithTax, totalAmountOfDiscount,
						 * 								 totalPercentageOfDiscount, totalAmountWithTaxWithDiscNonRoundOff, totalAmountWithTaxWithDisc, totalAmountWithTaxRoundOff, totalAmountWithTaxInWord, 
						 * 								 categoryWiseAmountForBills, totalAmount, taxAmountInWord, 
						 * 								 totalCGSTAmount,
						 * 							     totalIGSTAmount, 
						 * 								 totalSGSTAmount,
						 * 								 transporterName,
						 * 								 vehicalNumber,
						 * 								 transporterGstNumber,
						 * 								 docketNo, comment);*/
						return new BillPrintDataModel(proformaOrderId, invoiceNumber, "", "", orderDate, null, businessName, null,
								deliveryDate, productListForBillList, MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "", roundAmt,
								MathUtils.roundFromFloat(totalAmountWithoutTax, 2) + "", String.valueOf(totalQuantity),
								MathUtils.roundFromFloat(totalAmountWithTax, 2) + "", String.valueOf(discountAmtCut),
								String.valueOf(discountPerCut), String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
								String.valueOf(totalAmountWithTaxWithDisc), totalAmountWithTaxRoundOff, totalAmountWithTaxInWord,
								categoryWiseAmountForBills, MathUtils.roundFromFloat(totalAmount, 2) + "", taxAmountInWord,
								MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "",
								MathUtils.roundFromFloat((float)totalAmountDisc,2)+"",
								MathUtils.roundFromFloat((float)totalCGSTAmountDisc,3)+"",
								MathUtils.roundFromFloat((float)totalIGSTAmountDisc,2)+"",
								MathUtils.roundFromFloat((float)totalSGSTAmountDisc,3)+"",
								MathUtils.roundFromFloat((float)netTotalAmount,2)+"",
								MathUtils.roundFromFloat((float)netTotalCGSTAmount,3)+"",
								MathUtils.roundFromFloat((float)netTotalIGSTAmount,2)+"",
								MathUtils.roundFromFloat((float)netTotalSGSTAmount,3)+"",
								(proformaOrder.getTransportation() == null) ? "NA"
										: proformaOrder.getTransportation().getTransportName(),
								(proformaOrder.getVehicleNo() == null) ? "NA" : proformaOrder.getVehicleNo(),
								(proformaOrder.getTransportation() == null) ? "NA" : proformaOrder.getTransportation().getGstNo(),
								(proformaOrder.getDocketNo() == null) ? "NA" : proformaOrder.getDocketNo(), "",0);
/*
						return new BillPrintDataModel(proformaOrderId, invoiceNumber, "", "", orderDate, null, businessName, null,
								deliveryDate, productListForBillList, MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "", roundAmt,
								MathUtils.roundFromFloat(totalAmountWithoutTax, 2) + "", String.valueOf(totalQuantity),
								MathUtils.roundFromFloat(totalAmountWithTax, 2) + "", String.valueOf(discountAmtCut),
								String.valueOf(discountPerCut), String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
								String.valueOf(totalAmountWithTaxWithDisc), totalAmountWithTaxRoundOff, totalAmountWithTaxInWord,
								categoryWiseAmountForBills, MathUtils.roundFromFloat(totalAmount, 2) + "", taxAmountInWord,
								MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "",
								(proformaOrder.getTransportation() == null) ? "NA"
										: proformaOrder.getTransportation().getTransportName(),
								(proformaOrder.getVehicleNo() == null) ? "NA" : proformaOrder.getVehicleNo(),
								(proformaOrder.getTransportation() == null) ? "NA" : proformaOrder.getTransportation().getGstNo(),
								(proformaOrder.getDocketNo() == null) ? "NA" : proformaOrder.getDocketNo(), "",
								[],//bankDetailsForInvoiceList,
								proformaOrder.getTransportationCharges());*/
					} else {
						
						return null;
						/*
						return new BillPrintDataModel(proformaOrderId, invoiceNumber, "", "", orderDate, null,
								proformaOrder.getCustomerName(), proformaOrder.getCustomerMobileNumber(),
								proformaOrder.getCustomerGstNumber(), proformaOrder.getEmployeeGk(), deliveryDate,
								productListForBillList, MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "", roundAmt,
								MathUtils.roundFromFloat(totalAmountWithoutTax, 2) + "", String.valueOf(discountAmtCut),
								String.valueOf(discountPerCut), String.valueOf(totalQuantity),
								MathUtils.roundFromFloat(totalAmountWithTax, 2) + "", totalAmountWithTaxRoundOff,
								totalAmountWithTaxInWord, String.valueOf(totalAmountWithTaxWithDiscNonRoundOff),
								String.valueOf(totalAmountWithTaxWithDisc), categoryWiseAmountForBills,
								MathUtils.roundFromFloat(totalAmount, 2) + "", taxAmountInWord,
								MathUtils.roundFromFloat(totalCGSTAmount, 3) + "",
								MathUtils.roundFromFloat(totalIGSTAmount, 2) + "",
								MathUtils.roundFromFloat(totalSGSTAmount, 3) + "",
								(proformaOrder.getTransportation() == null) ? "NA"
										: proformaOrder.getTransportation().getTransportName(),
								(proformaOrder.getVehicleNo() == null) ? "NA" : proformaOrder.getVehicleNo(),
								(proformaOrder.getTransportation() == null) ? "NA" : proformaOrder.getTransportation().getGstNo(),
								(proformaOrder.getDocketNo() == null) ? "NA" : proformaOrder.getDocketNo(), "",
								bankDetailsForInvoiceList,
								proformaOrder.getTransportationCharges());
					*/}
				}
				
				
				//fetch Proforma Order By range
				/**
				 * <pre>
				 * fetch counter order by businessNameId, range, startDate, endDate
				 * &#64;param businessNameId
				 * &#64;param range
				 * &#64;param startDate
				 * &#64;param endDate 
				 * &#64;return  CounterOrder list
				 * </pre>
				 */
				@Transactional
				public List<ProformaOrder> fetchProformaOrderByRange(String businessNameId, String range, String startDate,
						String endDate) {

					// if gatekeeper logged then show his area counter order only
					// according added by user areas
					String areaListArray = "";
					if (getAppLoggedEmployeeId() != 0) {
						List<EmployeeAreaList> employeeAreaLists = employeeDetailsDAO
								.fetchEmployeeAreaListByEmployeeId(getAppLoggedEmployeeId());

						// List<Area> areaList=new ArrayList<>();
						for (EmployeeAreaList employeeAreaList : employeeAreaLists) {
							// areaList.add(employeeAreaList.getArea());
							areaListArray += employeeAreaList.getArea().getAreaId() + ",";
						}
						areaListArray = areaListArray.substring(0, areaListArray.length() - 1);
					}

					String hql = "";
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

					if (range.equals("today")) {
						hql = "from ProformaOrder where date(dateOfOrderTaken) = date(CURRENT_DATE()) ";
					} else if (range.equals("yesterday")) {
						cal.add(Calendar.DAY_OF_MONTH, -1);
						hql = "from ProformaOrder where date(dateOfOrderTaken) = '" + dateFormat.format(cal.getTime()) + "' ";
					} else if (range.equals("last7days")) {
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql = "from ProformaOrder where date(dateOfOrderTaken) > '" + dateFormat.format(cal.getTime()) + "'";
					} else if (range.equals("current3Months")) {
						cal.add(Calendar.MONTH, -3);
						hql = "from ProformaOrder where date(dateOfOrderTaken) > '" + dateFormat.format(cal.getTime()) + "'";
					} else if (range.equals("currentMonth")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) >= '" + DatePicker.getCurrentMonthStartDate()
								+ "' and date(dateOfOrderTaken) <= '" + DatePicker.getCurrentMonthLastDate() + "') ";
					} else if (range.equals("lastMonth")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) >= '" + DatePicker.getLastMonthFirstDate()
								+ "' and date(dateOfOrderTaken) <= '" + DatePicker.getLastMonthLastDate() + "') ";
					} else if (range.equals("last3Months")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) >= '" + DatePicker.getLast3MonthFirstDate()
								+ "' and date(dateOfOrderTaken) <= '" + DatePicker.getLast3MonthLastDate() + "') ";
					} else if (range.equals("last6Months")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) >= '" + DatePicker.getLast6MonthFirstDate()
								+ "' and date(dateOfOrderTaken) <= '" + DatePicker.getLast6MonthLastDate() + "') ";
					} else if (range.equals("range")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) >= '" + startDate
								+ "' and date(dateOfOrderTaken) <= '" + endDate + "') ";
					} else if (range.equals("pickDate")) {
						hql = "from ProformaOrder where (date(dateOfOrderTaken) = '" + startDate + "') ";
					} else if (range.equals("viewAll")) {
						hql = "from ProformaOrder where 1=1 ";
					} /*
						 * else if (range.equals("TopProducts")) {
						 * hql="from CounterOrder where 1=1 "; }
						 */
					hql = modifyQueryAccordingSessionSelectedBranchIds(hql);
					// if gatekeeper logged
					if (getAppLoggedEmployeeId() != 0) {
						if (businessNameId != null) {
							hql += " and employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList where area.areaId in ("
									+ areaListArray + ")) " + " and businessName.businessNameId='" + businessNameId + "'"
									+ "  order by dateOfOrderTaken desc";
						} else {
							hql += " and employeeGk.employeeId in (select DISTINCT employeeDetails.employee.employeeId from EmployeeAreaList where area.areaId in ("
									+ areaListArray + ")) " + "  order by dateOfOrderTaken desc";
						}
					} else { // if company/admin logged
						if (businessNameId != null) {
							hql += " and employeeGk.company.companyId in (" + getSessionSelectedCompaniesIds() + ") "
									+ " and businessName.businessNameId='" + businessNameId + "'"
									+ "  order by dateOfOrderTaken desc";
						} else {
							hql += " and employeeGk.company.companyId in (" + getSessionSelectedCompaniesIds() + ") "
									+ "  order by dateOfOrderTaken desc";
						}
					}

					Query query = sessionFactory.getCurrentSession().createQuery(hql);
					List<ProformaOrder> proformaOrderList = (List<ProformaOrder>) query.list();
					if (proformaOrderList.isEmpty()) {
						return null;
					}
					return proformaOrderList;
				}

				
				/**
				 * <pre>
				 * fetch Proforma order report for See Proforma Order Details,Edit,Delete
				 * @param range
				 * @param startDate
				 * @param endDate
				 * @ return CounterOrderReport list
				 * </pre>
				 */
				@Transactional
				public List<ProformaOrderReport> fetchProformaOrderReport(String range, String startDate, String endDate) {

					boolean showEdit = false;

					List<ProformaOrder> proformaOrderList = fetchProformaOrderByRange(null, range, startDate, endDate);
					if (proformaOrderList == null) {
						return null;
					}

					List<ProformaOrderReport> proformaOrderReportList = new ArrayList<>();
					int srno = 01;
					for (ProformaOrder proformaOrder : proformaOrderList) {

						SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
						SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

						String shopName;

					
						shopName = proformaOrder.getBusinessName().getShopName();
						
						/*if (counterOrder.getBusinessName() == null) {
							customerName = counterOrder.getCustomerName();
					
						} else {
							customerName = counterOrder.getBusinessName().getShopName();
						}*/

						
						String orderTakenDate = dateFormat.format(proformaOrder.getDateOfOrderTaken());
						String orderTakenTime = timeFormat.format(proformaOrder.getDateOfOrderTaken());


						String orderComment = "Sachin";
						/*
						if (proformaOrder.getOrderComment() != null) {
							orderComment = proformaOrder.getOrderComment();
						}*/
						
						
						
						proformaOrderReportList.add(new ProformaOrderReport(
								srno,
								proformaOrder.getProformaOrderPKId(), 
								proformaOrder.getProformaOrderId(), 
								shopName, 
								proformaOrder.getTotalAmount(), 
								proformaOrder.getTotalAmountWithTax(), 
								proformaOrder.getTotalQuantity(),
								orderTakenDate, 
								orderTakenTime, 
								orderComment));
						
						/*counterOrderReportList.add(new CounterOrderReport(srno, counterOrder.getCounterOrderId(),
								employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId())
										.getName(),
								customerName, counterOrder.getTotalAmount(), counterOrder.getTotalAmountWithTax(),
								counterOrder.getTotalQuantity(), counterOrder.getOrderStatus().getStatus(), grAmount, paymentDate,
								paymentStatus, orderTakenDate, orderTakenTime, (counterOrder.getBusinessName() == null), editable,
								agentName, agentId, isBusinessHaveAgent, isCommissionAssign, commissionType, commissionPercentage,
								commissionAmt, isCommissionPaid, orderComment, counterOrder.getInvoiceNumber()));*/
						srno++;
					}

					return proformaOrderReportList;
				}

				

				/*
				 * delete Proforma Order By id
				 * @see com.candylove.dao.CounterOrderDAO#deleteProformaOrderById(long)
				 */
				@Transactional
				public void deleteProformaOrderById(long id){
					
					deleteProformaProductDetailsById(id);
					
					String hql="delete ProformaOrder where proformaOrderPKId="+id;
					Query query=sessionFactory.getCurrentSession().createQuery(hql);
					query.executeUpdate();
				}
				
			

}
