package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ReturnOrderDAO;
import com.bluesquare.rc.dao.SnapShotDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.rest.models.DBSnapshotResponse;
import com.bluesquare.rc.rest.models.SnapShotReportResponse;
import com.bluesquare.rc.utils.Constants;


/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of SnapShotDAO
 * 1.fetchRecordForSnapShot(long employeeId, String fromDate, String toDate,String range)
 * </pre>
 */
@Repository("snapShotDAO")

@Component
public class SnapShotDAOImpl extends TokenHandler implements SnapShotDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	ReturnOrderDAO returnOrderDAO;
	
	/**
	 * <pre>
	 * fetch snapshot details for salesman
	 * 
	 * noOfOrderDetails : count of order taken
	 * noOfOrderDetailsCanceled : count of order cancelled 
	 * totalAmtSale : total amount with tax sum from OrderDetails
	 * totalRtnAmt : total amount with tax sum from ReturnOrderDetails
	 * retplaceOrderAmt : total amount with tax sum from ReissueOrderDetails
	 * totalCollectAmt  : total paid amount from Payment
	 * totalIssueAmt : issued amount with tax sum from OrderDetails
	 * 
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * </pre>
	 */
	@SuppressWarnings("unchecked")
	@Transactional

	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		
		SnapShotReportResponse snapShotReportResponse=new SnapShotReportResponse();
		String hql="";
		Query query;
		long noOfOrderDetails=0;
		long noOfOrderDetailsCanceled=0;
		double totalAmtSale=0;
		double totalRtnAmt=0;
		double retplaceOrderAmt=0;;
		double totalCollectAmt=0;
		double totalIssueAmt=0;
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		
		if(range.equals("range"))
						{
							hql="from OrderDetails where employeeSM.employeeId=" +employeeId + "  And (date(orderDetailsAddedDatetime)>='"+fromDate+"'  And date(orderDetailsAddedDatetime)<='"+toDate+"')"
									+" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by orderDetailsAddedDatetime desc";
							
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
							
							for(OrderDetails orderDetailsList1:orderDetailsList)
							{
								if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
								{
									noOfOrderDetailsCanceled+=1;
								}else{
									totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
									totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
								}									
								noOfOrderDetails+=1;
							}
							//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
							
							//commented on 29-05-2018
							/*for(OrderDetails orderDetailsList1:orderDetailsList)
							{
								if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
								{
									totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
									totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
								}								
							}*/
							
							hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And (date(returnOrderProductDatetime)>='"+fromDate+"'  And date(returnOrderProductDatetime)<='"+toDate+"')"+
									" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by returnOrderProductDatetime desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
							for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
							{
								totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
							}
							
							hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And (date(reIssueDate)>='"+fromDate+"'  And date(reIssueDate)<='"+toDate+"')"+
									" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
							for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
							{
								retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
							}
							
							hql="from Payment where status=false and orderDetails.employeeSM.employeeId=" +employeeId + " And (date(paidDate)>='"+fromDate+"'  And date(paidDate)<='"+toDate+"')"+
									" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by paidDate desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<Payment> paymentList=(List<Payment>)query.list();
							for(Payment paymentList1:paymentList)
							{
								totalCollectAmt+=paymentList1.getPaidAmount();
							}
							
							/*// in below 3 lines we are fetching  returnOrderProductList By orderId
								hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
								
								// here we calculate the totalRtn amt
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									// in below 3 line we calculate the payment list by orderId 
									hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> PaymentList=(List<Payment>)query.list();
									
									// here we calculate the total paid amount
									for(Payment paymentList1:PaymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
								
								
							}*/
							
							}
			else if(range.equals("last7days"))
								{
									cal.add(Calendar.DAY_OF_MONTH, -7);
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
											
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
									
									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}else{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}									
										noOfOrderDetails+=1;
									}
									//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
									
									//commented on 29-05-2018
									/*for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}*/
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
									
									
									/*
										// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
								}
			else if(range.equals("last1month"))
								{
									cal.add(Calendar.MONTH, -1);
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}else{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}									
										noOfOrderDetails+=1;
									}
									//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
									
									//commented on 29-05-2018
									/*for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}*/
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
										
										
										
										/*
										// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
							
								}
			else if(range.equals("last3months"))
									{
										cal.add(Calendar.MONTH, -3);
										hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
												" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												noOfOrderDetailsCanceled+=1;
											}else{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}									
											noOfOrderDetails+=1;
										}
										//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
										
										//commented on 29-05-2018
										/*for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}								
										}*/
										
										
										hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
										
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
												" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
										for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
										{
											retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
										}
										
										hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> paymentList=(List<Payment>)query.list();
										for(Payment paymentList1:paymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
											/*
											// in below 3 lines we are fetching  returnOrderProductList By orderId
											hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
											
											// here we calculate the totalRtn amt
												for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
												{
													totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
												}
												
												// in below 3 line we calculate the payment list by orderId 
												hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
												query=sessionFactory.getCurrentSession().createQuery(hql);
												List<Payment> PaymentList=(List<Payment>)query.list();
												
												// here we calculate the total paid amount
												for(Payment paymentList1:PaymentList)
												{
													totalCollectAmt+=paymentList1.getPaidAmount();
												}
											
											
										}*/
										}
			else if(range.equals("pickDate"))
									{
										hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)='"+fromDate +"'"+
												" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												noOfOrderDetailsCanceled+=1;
											}else{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}									
											noOfOrderDetails+=1;
										}
										//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
										
										//commented on 29-05-2018
										/*for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}								
										}*/
										
										
										hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)='"+dateFormat.format(cal.getTime())+"'"+
												" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
										for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
										{
											retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
										}
										
										hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> paymentList=(List<Payment>)query.list();
										for(Payment paymentList1:paymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
											
											/*// in below 3 lines we are fetching  returnOrderProductList By orderId
											hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
											
											// here we calculate the totalRtn amt
												for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
												{
													totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
												}
												
												// in below 3 line we calculate the payment list by orderId 
												hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
												query=sessionFactory.getCurrentSession().createQuery(hql);
												List<Payment> PaymentList=(List<Payment>)query.list();
												
												// here we calculate the total paid amount
												for(Payment paymentList1:PaymentList)
												{
													totalCollectAmt+=paymentList1.getPaidAmount();
												}
											
											
										}*/
									}
			else if(range.equals("viewAll"))
								{
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}else{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}									
										noOfOrderDetails+=1;
									}
									//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
									
									//commented on 29-05-2018
									/*for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}*/
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId +
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
									
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId +
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId +
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
										
										
										/*// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
								}
			else if(range.equals("currentDate"))
							{
								hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)=date(CURRENT_DATE()) "+
										" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by orderDetailsAddedDatetime desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

								for(OrderDetails orderDetailsList1:orderDetailsList)
								{
									if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
									{
										noOfOrderDetailsCanceled+=1;
									}else{
										totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
										totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
									}									
									noOfOrderDetails+=1;
								}
								//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
								
								//commented on 29-05-2018
								/*for(OrderDetails orderDetailsList1:orderDetailsList)
								{
									if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
									{
										totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
										totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
									}								
								}*/
								
								
								hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"'"+
										" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
											+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
											+" order by returnOrderProductDatetime desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
								for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
								{
									totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
								}
								
								hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)='"+dateFormat.format(cal.getTime())+"'"+
										" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
									+" and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
									+" order by reIssueDate desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
								for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
								{
									retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
								}
								
								hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)='"+dateFormat.format(cal.getTime())+"'"+
										" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "
										+" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "
										+" order by paidDate desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<Payment> paymentList=(List<Payment>)query.list();
								for(Payment paymentList1:paymentList)
								{
									totalCollectAmt+=paymentList1.getPaidAmount();
								}
/*									// in below 3 lines we are fetching  returnOrderProductList By orderId
									hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									
									// here we calculate the totalRtn amt
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										// in below 3 line we calculate the payment list by orderId 
										hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> PaymentList=(List<Payment>)query.list();
										
										// here we calculate the total paid amount
										for(Payment paymentList1:PaymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
									
									
								}*/
							}
			
		
		
		
		
		
		//hql="from OrderDetails where employeeSM.employeeId="+employeeId;
		snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
		snapShotReportResponse.setTotalNoOrderCanceled(noOfOrderDetailsCanceled);
		snapShotReportResponse.setTotalAmtSales(totalAmtSale);
		snapShotReportResponse.setTotalIssueAmt(totalIssueAmt);
		snapShotReportResponse.setRtnOrderAmt(totalRtnAmt);
		snapShotReportResponse.setReplaceOrderAmt(retplaceOrderAmt);
		snapShotReportResponse.setTotalCollectionAmt(totalCollectAmt);
		
		if(snapShotReportResponse.equals(null))
		{
			return null;
		}
		
		return snapShotReportResponse;
	}

	@Transactional
	public DBSnapshotResponse fetchDBSnapshot(long employeeId, String pickDate) {
		// TODO Auto-generated method stub
		DBSnapshotResponse dbSnapshotResponse=new DBSnapshotResponse();
		
		String hql,hql1,hql2,hql3,hql4,hql5="";
		Query query,query1,query2,query3,query4,query5;
		
		
		/*Query to find the total Number of order received By Delivery Boy*/
		 hql1="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.packedDate)='"+pickDate+"'"+
				" and e.employeeDB.employeeId="+employeeId;
		 query1=sessionFactory.getCurrentSession().createQuery(hql1);
		List<OrderDetails> orderDetailList=(List<OrderDetails>)query1.list();
		if(orderDetailList.isEmpty()){
			dbSnapshotResponse.setTotalNoOfOrderReceived(0);
		}else{
			dbSnapshotResponse.setTotalNoOfOrderReceived(orderDetailList.size());
		}
		
		/*Query to fetch Payment List by id Date Range*/
		 hql2="from Payment where employee.employeeId="+employeeId + " and date(paidDate)= '"+pickDate+"'";
		 query2=sessionFactory.getCurrentSession().createQuery(hql2);
		List<Payment> paymentList=(List<Payment>)query2.list();
		if(paymentList.isEmpty()){
			dbSnapshotResponse.setTotalCollectionAmount(0);
		}else {
			double totalCollectionAmount=0;
			for(Payment payment:paymentList){
				totalCollectionAmount+=payment.getPaidAmount();
			}
			dbSnapshotResponse.setTotalCollectionAmount(totalCollectionAmount);
		}


		
		/*Query to find out the total order Delivered*/
		 hql3="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.deliveryDate)='"+pickDate+"'"+
				" and e.employeeDB.employeeId="+employeeId + " and e.orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		 query3=sessionFactory.getCurrentSession().createQuery(hql3);
		List<OrderDetails> deliveredOrderDetailList=(List<OrderDetails>)query3.list();
		if(deliveredOrderDetailList.isEmpty()){
			dbSnapshotResponse.setTotalNoOfOrderDelivered(0);
		}else{
			dbSnapshotResponse.setTotalNoOfOrderDelivered(deliveredOrderDetailList.size());
		}
		
		
		/*Query to find out the total Return order Delivered*/
		 hql4="from ReturnOrderProduct where employee.employeeId="+employeeId + " and date(returnOrderProductDatetime)='"+pickDate+"'";
		 query4= sessionFactory.getCurrentSession().createQuery(hql4);
		List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query4.list();
		if(returnOrderProductList.isEmpty()){
			dbSnapshotResponse.setTotalReturnOrder(0);
		}else {
			dbSnapshotResponse.setTotalReturnOrder(returnOrderProductList.size());
		}
		
		/*Query to find out the total replacemnet delivery order*/
		 hql5="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)='"+pickDate +"' And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
		 query5=sessionFactory.getCurrentSession().createQuery(hql5);
		List<ReIssueOrderDetails> reIssueOrderDetailList=(List<ReIssueOrderDetails>)query5.list();
		
		//List<ReIssueOrderDetails> reIssueOrderDetailList=returnOrderDAO.fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(employeeId,pickDate,"","pickDate");
		if(reIssueOrderDetailList==null){
			dbSnapshotResponse.setTotalNoOfReplacmentOrderDelivered(0);
		}else{
			dbSnapshotResponse.setTotalNoOfReplacmentOrderDelivered(reIssueOrderDetailList.size());
		}

		return dbSnapshotResponse;
	}

}

