package com.bluesquare.rc.dao.impl;

import java.sql.Blob;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.DailyStockDetails;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.PermanentDamageDetails;
import com.bluesquare.rc.entities.PermanentDamageMonthWise;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.DailyStockDetailsModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.ProductNameAndId;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.rest.models.ProductListInnerResponse;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DamageRecoveryIdGenerator;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.ImageConvertor;
import com.bluesquare.rc.utils.MathUtils;
/**
 * <pre>
 * @author Sachin Pawar 28-05-2018 Code Documentation
 * provides Implementation for following methods of ProductDAO
 * 1.Update(Product product)
 * 2.saveProductForWebApp(MultipartFile file,Product product)
 * 3.saveProduct(Product product)
 * 4.updateProductForWebApp(MultipartFile file,Product product)
 * 5.updateProductForWebApp(Product product)
 * 6.fetchProductForWebApp(long productId)
 * 7.fetchProductForImage(long productId,long companyId)
 * 8.setNoImageToProductForImage(Product product)
 * 9.fetchProductByCategoryIdForWebApp(long categoryId)
 * 10.fetchProductListForWebApp() 
 * 11.findProductNameExist(String productName,long productId)
 * 12.fetchProductListByCompanyId(long companyId)
 * 13.fetchProductViewListForWebApp()
 * 14.calculateProperAmountModel(double mrp1,double igstPer1)
 * 15.calculateProperAmountModelOld(double mrp1,double igstPer1)
 * 16.fetchProductViewListForWebApp(long productId)
 * 17.fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId)
 * 18.fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId)
 * 19.fetchProductListBySupplierId(String supplierId)
 * 20.fetchProductList()
 * 21.fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId)
 * 22.fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest )
 * 23.fetchProductForAddInventory()
 * 24.fetchProductListForReport(String range,String startDate,String endDate,String topProductNo)
 * 25.SortByQuantity - comparator
 * 26.fetchDamageRecoveryMonthWiseByDamageRecoveryId(long damageRecoveryId)
 * 27.fetchDamageRecoveryMonthWiseByProductId(long productId)
 * 28.saveDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails,long damageRecoveryId)
 * 29.updateDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails)
 * 30.fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(String damageRecoveryDetailsId)
 * 31.fetchDamageRecoveryDetailsByDamageRecoveryId(long damageRecoveryId)
 * 32.saveUpdateDamageRecoveryMonthWise(long productId,long damageQuantity)
 * 33.fetchDamageRecoveryDayWise(String startMonth,String startYear,String endMonth,String endYear)
 * 34.creatFirstRecordOfDailyStockReport()
 * 35.fetchTodaysDailyStockDetails()
 * 36.fetchDailyStockDetails(String pickDate,long companyId)
 * 37.fetchDailyStockReportByProductIdAndCurrentDate(long productId)
 * 38.updateDailyStockExchange(long productId,long qty,boolean isAdd )
 * 39.saveDamageDefine(DamageDefine damageDefine)
 * 40.fetchDamageDefineList(String range,String startDate,String endDate)
 * </pre>
 */
@Repository("productDAO")

@Component
public class ProductDAOImpl extends TokenHandler implements ProductDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	Company company;
	
	@Autowired
	Product product;

	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	DamageRecoveryIdGenerator damageRecoveryIdGenerator;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;

	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Autowired
	ImageConvertor imageConvertor;
	
	@Autowired
	SupplierDAO supplierDAO;
	
	@Autowired
	BranchDAO branchDAO;

	@Transactional
	public void Update(Product product) {
		product=(Product)sessionFactory.getCurrentSession().merge(product);
		sessionFactory.getCurrentSession().update(product);
	}
	/**
	 * <pre>
	 * set logged user company
	 * save product info with product image
	 * if file is not empty then convert it to blob and save product
	 * other wise set null on product image and save image 
	 * @param file
	 * @param product
	 * </pre>
	 */
	@Transactional
	public void saveProductForWebApp(MultipartFile file,Product product) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		product.setCompany(company);
		
		try {
			
			// As Image is Hide
			/*if(!file.isEmpty())
			{
				Blob blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(file.getInputStream(),
						file.getSize());
				//String image=ImageConvertor.convertBlobToString(blob);
				product.setProductImage(blob);
			}*/
			sessionFactory.getCurrentSession().save(product);
		} catch (Exception e) {
			System.out.println("saveProductForWebApp Error : "+e.toString());
		}
	}
	/**
	 * <pre>
	 * set logged user company
	 * save product info 
	 * @param product
	 * </pre>
	 */
	@Transactional
	public void saveProduct(Product product) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		product.setCompany(company);
		sessionFactory.getCurrentSession().save(product);
	}
	/**
	 * <pre>
	 * update product info with product image
	 * if file is not empty then convert it to blob and update product
	 * other wise set null on product image and update image 
	 * @param file
	 * @param product
	 * </pre>
	 */
	@Transactional
	public void updateProductForWebApp(MultipartFile file,Product product) {
		// TODO Auto-generated method stub
		
		try {
			//As Image is hide
			/*if(!file.isEmpty())
			{
				Blob blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(file.getInputStream(),
						file.getSize());
				product.setProductImage(blob);
			}*/
			
			Update(product);
		} catch (Exception e) {
			System.out.println("updateProductForWebApp Error : "+e.toString());
		}
	}
	/**
	 * <pre>
	 * update product info  
	 * @param product
	 * </pre>
	 */
	@Transactional
	public void updateProductForWebApp(Product product) {
		// TODO Auto-generated method stub
		
		try {
			Update(product);
		} catch (Exception e) {
			System.out.println("updateProductForWebApp Error : "+e.toString());
		}
	}
	/**
	 * <pre>
	 * fetch product by id which is belongs to logged user company
	 * @param productId
	 * @return Product
	 * </pre>
	 */
	@Transactional
	public Product fetchProductForWebApp(long productId) {
		String hql = "from Product where productId=" + productId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
			   
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		return productList.get(0);
	}
	/**
	 * <pre>
	 * fetch product by id which is belongs to given company id
	 * @param productId
	 * @return Product
	 * </pre>
	 */
	@Transactional
	public Product fetchProductForImage(long productId,long companyId,long brandId) {
		String hql = "from Product where productId=" + productId
				    +" and company.companyId="+companyId
				    +" and branch.branchId="+brandId;
			   
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		
		return productList.get(0);
	}
	/**
	 * <pre>
	 * set no image in null image product
	 * @param product
	 * @return Product
	 * </pre>
	 */
	@Transactional
	public Product setNoImageToProductForImage(Product product) {
		//ImageConvertor imageConvertor=new ImageConvertor(sessionFactory);
		/*product.setProductContentType("image/png");
		product.setProductImage(imageConvertor.convertStringToBlob(imageConvertor.noImage()));*/
		return product;
	}
	/**
	 * <pre>
	 * fetch product list by category id
	 * @param categoryId
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductByCategoryIdForWebApp(long categoryId) {
		String hql = "from Product where categories.categoryId=" + categoryId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		return productList;
	}
	/**
	 * <pre>
	 * fetch product list which is belongs to logged user company
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductListForWebApp() {
		String hql = "from Product "
		+" where company.companyId in ("+getSessionSelectedCompaniesIds()+")"
		+" and branch.branchId="+getSessionSelectedBranchIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		return productList;
	}
	/**
	 * <pre>
	 * fetch product name and id list
	 * </pre>
	 * @return {@link ProductNameAndId}
	 */
	@Transactional
	public List<ProductNameAndId> fetchProductNameAndIdList() {
		String hql = "from Product "
		+" where company.companyId in ("+getSessionSelectedCompaniesIds()+")"
		+" and branch.branchId="+getSessionSelectedBranchIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		List<ProductNameAndId> productNameAndIdList=new ArrayList<>();
		for(Product product: productList){
			productNameAndIdList.add(new ProductNameAndId(product.getProductId(), product.getProductName()));
		}
		return productNameAndIdList;
	}
	/**
	 * <pre>
	 * check product name already exist or not
	 * @param productName
	 * @param productId
	 * @return Product
	 * </pre>
	 */
	@Transactional
	public Product findProductNameExist(String productName,long productId) {
		String hql = "from Product "+
		" where company.companyId in ("+getSessionSelectedCompaniesIds()+") and UPPER(productName)='"+productName.trim().toUpperCase()+"'"
		+" and branch.branchId="+getSessionSelectedBranchIds();
		if(productId!=0){
			hql+=" and productId<>"+productId;
		}
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		return productList.get(0);
	}
	/**
	 * <pre>
	 * fetch product list which is belongs to given company id
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductListByCompanyId(long companyId,long brandId) {
		String hql = "from Product "+
		" where company.companyId="+companyId
		+" and branch.branchId="+brandId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}
		return productList;
	}
	/**
	 * <pre>
	 * fetch product list which is belongs to logged user company
	 * calculate below using {@link CalculateProperTaxModel} 
	 * taxableTotal,igstamoun,cgstamount,sgstamount,total
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<ProductViewList> fetchProductViewListForWebApp() {
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		String hql = "from Product "+
		" where company.companyId in ("+getSessionSelectedCompaniesIds()+")"
		+" and branch.branchId="+getSessionSelectedBranchIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		if (productList.isEmpty()) {
			return null;
		}	
		
		List<ProductViewList> productViewList=new ArrayList<ProductViewList>();
		Iterator<Product> itr=productList.iterator();
		int srno=1;
		while(itr.hasNext())
		{
			Product product=itr.next();
			/*double taxableTotal=product.getCurrentQuantity()*product.getRate();
			double cgstamount=(taxableTotal*product.getCategories().getCgst())/100;
			double igstamount=(taxableTotal*product.getCategories().getIgst())/100;
			double sgstamount=(taxableTotal*product.getCategories().getSgst())/100;
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			double total=productRateWithTax*product.getCurrentQuantity();*/
			
			//sachin sharma given this logic
			/*double taxableTotal = product.getCurrentQuantity() * Double.parseDouble(decimalFormat.format(product.getRate()));
			double igstUnitTax=Double.parseDouble(decimalFormat.format((product.getRate()*product.getCategories().getIgst())/100));
			double igstTotalAmt=product.getCurrentQuantity()*igstUnitTax;
			double cgstamount =igstTotalAmt/2;
			double sgstamount=igstTotalAmt/2;
			double igstamount=igstTotalAmt;	//end		
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			double total=product.getCurrentQuantity()*productRateWithTax;*/
			
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			double taxableTotal = product.getCurrentQuantity() * calculateProperTaxModel.getUnitprice();
			double igstamount=product.getCurrentQuantity()*calculateProperTaxModel.getIgst();
			double cgstamount =product.getCurrentQuantity()*calculateProperTaxModel.getCgst();
			double sgstamount=product.getCurrentQuantity()*calculateProperTaxModel.getSgst();		
			double total=product.getCurrentQuantity()*calculateProperTaxModel.getMrp();
			
			productViewList.add(new ProductViewList(srno, 
					product.getProductId(), 
					product.getProductCode(),
					product.getProductName(), 
					product.getCategories().getCategoryName(), 
					product.getBrand().getName(), 
					product.getCategories().getHsnCode(), 
					product.getThreshold(), 
					product.getRate(), 
					productRateWithTax,
					product.getCurrentQuantity(), 
					taxableTotal, 
					product.getCategories().getCgst(),
					cgstamount, 
					product.getCategories().getIgst(),
					igstamount, 
					product.getCategories().getSgst(), 
					sgstamount, 
					total, 
					product.getProductAddedDatetime(), 
					product.getProductQuantityUpdatedDatetime(),
					product.getProductBarcode()));
			srno++;
		}
		
		return productViewList;
	}

	public static void main(String[] args) {
				
		float mrp=70;
	/*	System.out.println(new ProductDAOImpl().calculateProperAmountModelOld(mrp, 0));
		System.out.println(new ProductDAOImpl().calculateProperAmountModelOld(mrp, 5));
		System.out.println(new ProductDAOImpl().calculateProperAmountModelOld(mrp, 12));
		System.out.println(new ProductDAOImpl().calculateProperAmountModelOld(mrp, 18));
		System.out.println(new ProductDAOImpl().calculateProperAmountModelOld(mrp, 28));
		
		System.out.println();
*/
		System.out.println(new ProductDAOImpl().calculateProperAmountModel(mrp, 0));
		System.out.println(new ProductDAOImpl().calculateProperAmountModel(mrp, 5));
		System.out.println(new ProductDAOImpl().calculateProperAmountModel(mrp, 12));
		System.out.println(new ProductDAOImpl().calculateProperAmountModel(mrp, 18));
		System.out.println(new ProductDAOImpl().calculateProperAmountModel(mrp, 28));
	}
	/**
	 * calculate using {@link MathUtils.roundFromFloat}
	 */
	public CalculateProperTaxModel calculateProperAmountModel(double mrp1,double igstPer1) {
		
		DecimalFormat decimalFormatTwoDigit=new DecimalFormat("#.##");
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		float mrp=(float)mrp1;
		float igstPer=(float)igstPer1;
		
		float unitPrice=(
				mrp/
					(
						(igstPer+100)/100
					)
			  );
		unitPrice = MathUtils.roundFromFloat(unitPrice, 2);
		//unitPrice=Float.parseFloat(decimalFormatTwoDigit.format(unitPrice));
				
		float cgst=(
					unitPrice*
					(
						(igstPer/2)/100
					)
				  );
		cgst= MathUtils.roundFromFloat(cgst,2);
		
		float sgst=(
					unitPrice*
					(
						(igstPer/2)/100
					)
				  );
		sgst= MathUtils.roundFromFloat(sgst,2);
		
		float igst=cgst+sgst;
		igst= MathUtils.roundFromFloat(igst,2);
		
		float newMrp=unitPrice+igst;
		newMrp= MathUtils.roundFromFloat(newMrp,2);
		
		if(mrp!=newMrp)
		{
			igst=(unitPrice)*(igstPer/100);
			igst= MathUtils.roundFromFloat(igst,2);
			
			cgst=igst/2;
			sgst=igst/2;
			
			newMrp=unitPrice+igst;
			newMrp=MathUtils.roundFromFloat(newMrp,2);
			
			if(mrp!=newMrp)
			{
				float mrpDiff=mrp-newMrp;
				unitPrice=unitPrice+mrpDiff;
				unitPrice=MathUtils.roundFromFloat(unitPrice,2);
				
				return new CalculateProperTaxModel(mrp,  
						unitPrice, 
						igst, 
						MathUtils.roundFromFloat(sgst,3), 
						MathUtils.roundFromFloat(cgst,3));
				
			}else{
			
				return new CalculateProperTaxModel(newMrp,  
												unitPrice, 
												igst, 
												MathUtils.roundFromFloat(sgst,3), 
												MathUtils.roundFromFloat(cgst,3));
			}
		}
		else
		{
			return new CalculateProperTaxModel(newMrp,  
					unitPrice, 
					igst, 
					sgst, 
					cgst);
		}
	}
	/**
	 * calculate using {@link DecimalFormat}
	 * @param mrp1
	 * @param igstPer1
	 * @return
	 */
public CalculateProperTaxModel calculateProperAmountModelOld(double mrp1,double igstPer1) {
		
		DecimalFormat decimalFormatTwoDigit=new DecimalFormat("#.##");
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		
		float mrp=(float)mrp1;
		float igstPer=(float)igstPer1;
		
		float unitPrice=(
				mrp/
					(
						(igstPer+100)/100
					)
			  );
		unitPrice=Float.parseFloat(decimalFormatTwoDigit.format(unitPrice));
				
		float cgst=(
					unitPrice*
					(
						(igstPer/2)/100
					)
				  );
		cgst=Float.parseFloat(decimalFormatTwoDigit.format(cgst));
		
		float sgst=(
					unitPrice*
					(
						(igstPer/2)/100
					)
				  );
		sgst=Float.parseFloat(decimalFormatTwoDigit.format(sgst));
		
		float igst=cgst+sgst;
		igst=Float.parseFloat(decimalFormatTwoDigit.format(igst));
		
		float newMrp=unitPrice+igst;
		newMrp=Float.parseFloat(decimalFormatTwoDigit.format(newMrp));
		
		if(mrp!=newMrp)
		{
			igst=(unitPrice)*(igstPer/100);
			igst=Float.parseFloat(decimalFormatTwoDigit.format(igst));
			
			cgst=igst/2;
			sgst=igst/2;
			
			newMrp=unitPrice+igst;
			newMrp=Float.parseFloat(decimalFormatTwoDigit.format(newMrp));
			
			if(mrp!=newMrp)
			{
				float mrpDiff=mrp-newMrp;
				unitPrice=unitPrice+mrpDiff;
				unitPrice=Float.parseFloat(decimalFormatTwoDigit.format(unitPrice));
				
				return new CalculateProperTaxModel(mrp,  
						unitPrice, 
						igst, 
						Float.parseFloat(decimalFormatThreeDigit.format(sgst)), 
						Float.parseFloat(decimalFormatThreeDigit.format(cgst)));
				
			}else{
			
				return new CalculateProperTaxModel(newMrp,  
												unitPrice, 
												igst, 
												Float.parseFloat(decimalFormatThreeDigit.format(sgst)), 
												Float.parseFloat(decimalFormatThreeDigit.format(cgst)));
			}
		}
		else
		{
			return new CalculateProperTaxModel(newMrp,  
					unitPrice, 
					igst, 
					sgst, 
					cgst);
		}
	}
/**
 * <pre>
 * fetch product list which is belongs to logged user company by productId
 * calculate below using {@link CalculateProperTaxModel} 
 * taxableTotal,igstamoun,cgstamount,sgstamount,total
 * @return Product list
 * </pre>
 */
	@Transactional
	public ProductViewList fetchProductViewListForWebApp(long productId) {
		// TODO Auto-generated method stub
		String hql = "from Product where productId=" + productId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) query.list();
		
		if (productList.isEmpty()) {
			return null;
		}	
		
		List<ProductViewList> productViewList=new ArrayList<ProductViewList>();
		Iterator<Product> itr=productList.iterator();
		int srno=1;
		while(itr.hasNext())
		{
			Product product=itr.next();
			/*double taxableTotal=product.getCurrentQuantity()*product.getRate();
			double cgstamount=(taxableTotal*product.getCategories().getCgst())/100;
			double igstamount=(taxableTotal*product.getCategories().getIgst())/100;
			double sgstamount=(taxableTotal*product.getCategories().getSgst())/100;
			double total=taxableTotal+cgstamount+igstamount+sgstamount;
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);*/
			
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			double taxableTotal = product.getCurrentQuantity() * calculateProperTaxModel.getUnitprice();
			double igstamount=product.getCurrentQuantity()*calculateProperTaxModel.getIgst();
			double cgstamount =product.getCurrentQuantity()*calculateProperTaxModel.getCgst();
			double sgstamount=product.getCurrentQuantity()*calculateProperTaxModel.getSgst();		
			double total=product.getCurrentQuantity()*calculateProperTaxModel.getMrp();
			
			productViewList.add(new ProductViewList(srno, 
					product.getProductId(), 
					product.getProductCode(),
					product.getProductName(), 
					product.getCategories().getCategoryName(), 
					product.getBrand().getName(), 
					product.getCategories().getHsnCode(), 
					product.getThreshold(), 
					product.getRate(),
					productRateWithTax,
					product.getCurrentQuantity(),
					taxableTotal,
					product.getCategories().getCgst(), 
					cgstamount, 
					product.getCategories().getIgst(),
					igstamount, 
					product.getCategories().getSgst(), 
					sgstamount,
					total,
					product.getProductAddedDatetime(), 
					product.getProductQuantityUpdatedDatetime(),
					product.getProductBarcode()));
					
					srno++;
		}
		
		return productViewList.get(0);
	}
	/**
	 * <pre>
	 * fetch product by brand and category id
	 * @param categoryId
	 * @param brandId
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId) {
			String hql="";//="from Product where brand.brandId="+brandId+ " and categories.categoryId="+categoryId;
			
			if(categoryId==0 && brandId==0){
				hql="from Product where 1=1 ";
			}
			else if(categoryId==0){
				hql="from Product where brand.brandId="+brandId;
			}
			else if(brandId==0)
			{
				hql="from Product where categories.categoryId="+categoryId;
			}
			else
			{
				hql="from Product where brand.brandId="+brandId+ " and categories.categoryId="+categoryId;
			}
			
			hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
			hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
			
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Product> list=(List<Product>)query.list();
			
			if(list.isEmpty())
			{
				return null;				
			}
			
		return list;
	}
	/**
	 * <pre>
	 * fetch product by brand and category id and supplier Id
	 * product give which is assinged at time supplier adding
	 * @param categoryId
	 * @param brandId
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId) {
						
			List<Product> productsList=fetchProductListBySupplierId(supplierId);
			if(productsList==null)
			{
				return null;				
			}
			List<Product> productsList2=new ArrayList<>();
			
			
			if(categoryId==0 && brandId==0){
				return productsList;
			}
			
			if(categoryId==0){

				for(Product product : productsList)
				{
					if(product.getBrand().getBrandId()==brandId)
					{
						productsList2.add(product);
					}
				}
	        
	        }
	        else if(brandId==0){
	        	for(Product product : productsList)
				{
					if(product.getCategories().getCategoryId()==categoryId)
					{
						productsList2.add(product);
					}
				}    
	        }		
	        else
	        {
	        	for(Product product : productsList)
				{
					if(product.getCategories().getCategoryId()==categoryId && product.getBrand().getBrandId()==brandId)
					{
						productsList2.add(product);
					}
				}  
	        }
			
		return productsList2;
	}
	
	
	/*@Transactional
	public List<Product> makeProductImageNull(List<Product> productList) {
		// TODO Auto-generated method stub
		
		List<Product> productList2=new ArrayList<>();
		for(Product product : productList)
		{
			product.setProductImage(null);
			productList2.add(product);
		}
			
		return productList2;
	}*/
	
	/*@Transactional
	public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists) {
		// TODO Auto-generated method stub
		
		List<SupplierProductList> supplierProductLists2=new ArrayList<>();
		for(SupplierProductList supplierProductList : supplierProductLists)
		{
			supplierProductList.getProduct().setProductImage(null);
			supplierProductLists2.add(supplierProductList);
		}
			
		return supplierProductLists2;
	}*/
	/**
	 * <pre>
	 * fetch product list of supplier which assigned at time of adding supplier
	 * @param supplierId
	 * @return Product list
	 * </pre>
	 */
	@Transactional
	public List<Product> fetchProductListBySupplierId(String supplierId) {

		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'"
				+ " and supplier.company.companyId="+getSessionSelectedCompaniesIds()
				+ " and supplier.branch.branchId="+getSessionSelectedBranchIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		
		if(list.isEmpty())
		{
			return null;
		}
		
		List<Product> productList=new ArrayList<>();
		
		for(SupplierProductList supplierProductList : list )
		{
			productList.add(supplierProductList.getProduct());
		}
		
		//productList=makeProductImageNull(productList);
		
	return productList;
	}
	/**
	 * <pre>
	 * fetch product list by logged user company
	 * @return ProductAddInventory list
	 * </pre>
	 */
	@Transactional
	public List<ProductAddInventory> fetchProductList(){
		
		ProductListInnerResponse innerResponse=new ProductListInnerResponse();
		List<ProductAddInventory> innerResponselist=new ArrayList<ProductAddInventory>();
		
		String hql="from Product where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Product> list=(List<Product>) query.list();
		if(list.isEmpty()){
			return null;
		}
		
		Iterator<Product> iterator=list.iterator();
		while(iterator.hasNext()){
		Product product=iterator.next();
		
		innerResponselist.add(new ProductAddInventory(product.getProductId(), 
														product.getCurrentQuantity(), 
														product.getProductCode(), 
														product.getProductName(), 
														product.getRate(), 
														product.getBrand().getBrandId(), 
														product.getCategories().getCategoryId(), 
														product.getThreshold(), 
														0,
														new CategoriesModel(
																product.getCategories().getCategoryId(), 
																product.getCategories().getCategoryName(), 
																product.getCategories().getCategoryDescription(), 
																product.getCategories().getHsnCode(), 
																product.getCategories().getCgst(), 
																product.getCategories().getSgst(), 
																product.getCategories().getIgst(), 
																product.getCategories().getCategoryDate(), 
																product.getCategories().getCategoryUpdateDate()),
														new BrandModel(
																product.getBrand().getBrandId(), 
																product.getBrand().getName(), 
																product.getBrand().getBrandAddedDatetime(), 
																product.getBrand().getBrandUpdateDatetime()),
														product.getCompany(),
														product.getBranch()));
		}
		return innerResponselist;
	}
	/**
	 * <pre>
	 * fetch product list of supplier by supplierId,brandId,categoryId
	 * @return ProductAddInventory list
	 * </pre>
	 */
	@Transactional
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId) {
						
		//SupplierDAOimpl supplierDAOimpl=new SupplierDAOimpl(sessionFactory);
		List<ProductAddInventory> productAddInventoryList=supplierDAO.fetchProductBySupplierId(supplierId);
			
			if(productAddInventoryList==null)
			{
				return null;				
			}			
			
			if(categoryId==0 && brandId==0){
				return productAddInventoryList;
			}
			List<ProductAddInventory> productAddInventoryList2=new ArrayList<>();

			if(categoryId==0){

				for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductBrandId()==brandId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}
	        
	        }
	        else if(brandId==0){
	        	for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductCategoryId()==categoryId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}    
	        }		
	        else
	        {
	        	for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductBrandId()==brandId && productAddInventory.getProductCategoryId()==categoryId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}  
	        }
			
		return productAddInventoryList2;
	}
	/**
	 * <pre>
	 * fetch product list by brandId,categoryId ,logged user company
	 * @param brandAndCategoryRequest
	 * @return ProductAddInventory list
	 * </pre>
	 */
	@Transactional
	public List<ProductAddInventory> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest ){
		
		String hql;
		
		List<ProductAddInventory> innerResponselist=new ArrayList<ProductAddInventory>();
		
		if(brandAndCategoryRequest.categoryId==0 && brandAndCategoryRequest.getBrandId()==0){
			hql="from Product where 1=1 ";
		}
		else if(brandAndCategoryRequest.categoryId==0){
		 hql="from Product where brand.brandId="+brandAndCategoryRequest.getBrandId();		
		}
		else if(brandAndCategoryRequest.getBrandId()==0){
			hql="from Product where categories.categoryId="+brandAndCategoryRequest.getCategoryId();		
		}
		else{
			hql="from Product where brand.brandId="+brandAndCategoryRequest.getBrandId()+" and categories.categoryId="+brandAndCategoryRequest.getCategoryId();
			}
		
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Product> list=(List<Product>) query.list();
		if(list.isEmpty()){
			return null;
		}
		
		Iterator<Product> iterator=list.iterator();
		while(iterator.hasNext()){
		Product product=iterator.next();
		
		innerResponselist.add(new ProductAddInventory(product.getProductId(), 
				product.getCurrentQuantity(), 
				product.getProductCode(), 
				product.getProductName(), 
				product.getRate(), 
				product.getBrand().getBrandId(), 
				product.getCategories().getCategoryId(), 
				product.getThreshold(), 
				0,
				new CategoriesModel(
						product.getCategories().getCategoryId(), 
						product.getCategories().getCategoryName(), 
						product.getCategories().getCategoryDescription(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(), 
						product.getCategories().getCategoryDate(), 
						product.getCategories().getCategoryUpdateDate()),
				new BrandModel(
						product.getBrand().getBrandId(), 
						product.getBrand().getName(), 
						product.getBrand().getBrandAddedDatetime(), 
						product.getBrand().getBrandUpdateDatetime()),
				product.getCompany(),
				product.getBranch()));
		}
		return innerResponselist;
	}
	/**
	 * <pre>
	 * fetch product list which belongs to logged user company
	 * @return ProductAddInventory list
	 * </pre>
	 */
	@Transactional
	public List<ProductAddInventory> fetchProductForAddInventory(){
		
		List<ProductAddInventory> productAddInventories=new ArrayList<ProductAddInventory>();
				
		List<Product> list=fetchProductListForWebApp();
		if(list.isEmpty()){
			return null;
		}
		Iterator<Product> iterator=list.iterator();
		while(iterator.hasNext()){
		ProductAddInventory addInventory=new ProductAddInventory();	
		Product product=iterator.next();
		addInventory.setCurrentQuantity(product.getCurrentQuantity());
		addInventory.setProductBrandId(product.getBrand().getBrandId());
		addInventory.setProductCategoryId(product.getCategories().getCategoryId());
		addInventory.setProductCode(product.getProductCode());
		addInventory.setProductId(product.getProductId());
		addInventory.setProductName(product.getProductName());
		addInventory.setRate(product.getRate());
		addInventory.setProductThreshold(product.getThreshold());
		productAddInventories.add(addInventory);
		}
		return productAddInventories;
	}
	/**
	 * <pre>
	 * fetch product list with sold quantity ,damage quantity, free quantity
	 * Sold quantity : salesman order , counter order
	 * damage quantity : damage define
	 * free quantity : salesman order , counter order
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @param topProductNo
	 * @return ProductReportView list
	 * </pre>
	 */
	@Transactional
	public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo)
	{
		List<ProductReportView> productReportViewList=new ArrayList<>();
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<Product> productList=fetchProductListForWebApp();
		long srno=1;
		Calendar cal = Calendar.getInstance();		
		
		if (range.equals("today")) {
			hql="from OrderDetails where date(packedDate) = date(CURRENT_DATE()) and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(packedDate) = '"+dateFormat.format(cal.getTime())+"' and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(packedDate) > '"+dateFormat.format(cal.getTime())+"' and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("currentMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(packedDate) <= '"+DatePicker.getCurrentMonthLastDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("lastMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLastMonthLastDate()+"') and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("last3Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast3MonthLastDate()+"') and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("last6Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast6MonthLastDate()+"') and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("range")) {
			hql="from OrderDetails where (date(packedDate) >= '"+startDate+"' and date(packedDate) <= '"+endDate+"') and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("pickDate")) {
			hql="from OrderDetails where (date(packedDate) = '"+startDate+"') and   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("viewAll")) {
			hql="from OrderDetails where   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		/*else if (range.equals("TopProducts")) {
			hql="from OrderDetails where   orderStatus.status in ('"+Constants.ORDER_STATUS_PACKED+"','"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"') ";
		}*/
		
		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list=(List<OrderDetails>) query.list();
		
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null,range, startDate, endDate);
		
		
		if(productList!=null)
		{
			for(Product product : productList)
			{
				long issuedQuantity=0;
				long damageQuantity=0;
				long freeQuantity=0;
				double amountWithTax=0;
				double rateWithTax=0;
				
				for(OrderDetails orderDetails:list)
				{
					List<OrderProductDetails> orderProductDetailsList=orderDetailsDAO.fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
					for(OrderProductDetails orderProductDetails:orderProductDetailsList)
					{
						if(product.getProductId()==orderProductDetails.getProduct().getProduct().getProductId())
						{
							if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED) || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
							{
								if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
								{
									issuedQuantity+=orderProductDetails.getIssuedQuantity();
								}
								else
								{
									freeQuantity+=orderProductDetails.getIssuedQuantity();
								}							
								amountWithTax+=orderProductDetails.getIssueAmount();
							}
							else
							{
								if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
								{
									issuedQuantity+=orderProductDetails.getConfirmQuantity();
								}
								else
								{
									freeQuantity+=orderProductDetails.getConfirmQuantity();
								}							
								amountWithTax+=orderProductDetails.getConfirmAmount();
							}
						}
					}
				}
				
				List<DamageDefine> damageDefineList=fetchDamageDefineList(range, startDate, endDate);
				if(damageDefineList!=null)
				{
					for(DamageDefine damageDefine: damageDefineList)
					{
						if(product.getProductId()==damageDefine.getProduct().getProductId())
						{
							damageQuantity+=damageDefine.getQty();
						}
					}
				}
				
				//counter order
				if(counterOrderList!=null){
					for(CounterOrder counterOrder : counterOrderList){
						List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
						if(counterOrderProductDetailsList!=null){
							for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList)
							{
								if(product.getProductId()==counterOrderProductDetails.getProduct().getProduct().getProductId())
								{
									if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
									{
										issuedQuantity+=counterOrderProductDetails.getPurchaseQuantity();
									}
									else
									{
										freeQuantity+=counterOrderProductDetails.getPurchaseQuantity();
									}							
									amountWithTax+=counterOrderProductDetails.getPurchaseAmount();
								}
							}
						}
					}
				}	
				
				if(issuedQuantity!=0)
				{
					rateWithTax=amountWithTax/issuedQuantity;
					productReportViewList.add(new ProductReportView(
							srno, 
							new ProductModel(
									product.getProductId(), 
									product.getProductName(), 
									null, 
									new CategoriesModel(
											product.getCategories().getCategoryId(), 
											product.getCategories().getCategoryName(), 
											null, 
											null, 
											0, 
											0, 
											0, 
											null, 
											null), 
									null, 
									0, 
									null, 
									0, 
									0, 
									0, 
									0, 
									null, 
									null, 
									null, 
									null),
							issuedQuantity,
							damageQuantity,
							freeQuantity, 
							amountWithTax, 
							rateWithTax));
					srno++;
				}
				/*else
				{
					rateWithTax=0;
				}
				productReportViewList.add(new ProductReportView(srno, product,quantity, amountWithTax, rateWithTax));*/
				
			}
		}
		List<ProductReportView> productReportViewList2=new ArrayList<>();

		Collections.sort(productReportViewList,new SortByQuantity());
		if (Long.parseLong(topProductNo)>0)//range.equals("TopProducts") 
		{
			if(productReportViewList.size()>=Long.parseLong(topProductNo))
			{
				long i=1;
				for(ProductReportView productReportView: productReportViewList)
				{
					productReportView.setSrno(i);
					productReportViewList2.add(productReportView);
					if(i==Long.parseLong(topProductNo))
					{
						break;
					}
					i++;
				}
				return productReportViewList2;
			}
			else
			{
				long i=1;
				for(ProductReportView productReportView: productReportViewList)
				{
					productReportView.setSrno(i);
					productReportViewList2.add(productReportView);
					i++;
				}
				return productReportViewList2;
			}
		}
		return productReportViewList;
	}
	/**
	 * product report sort by issued quantity
	 */
	public class SortByQuantity implements Comparator<ProductReportView> 
	{
	    public int compare(ProductReportView p1, ProductReportView p2) 
	    {
	        if (p1.getIssuedQuantity() < p2.getIssuedQuantity()) return 1;
	        if (p1.getIssuedQuantity() > p2.getIssuedQuantity()) return -1;
	        return 0;
	    }    
	}
	
	/**
	 * <pre>
	 * fetch  damage recovery month wise details by damageRecoveryId
	 * @param damageRecoveryId
	 * @return DamageRecoveryMonthWise
	 * </pre>
	 */
	@Transactional
	public DamageRecoveryMonthWise fetchDamageRecoveryMonthWiseByDamageRecoveryId(long damageRecoveryId)
	{
		String hql="from DamageRecoveryMonthWise where damageRecoveryId="+damageRecoveryId+
				" and product.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and product.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageRecoveryMonthWise> damageRecoveryMonthWiseList=(List<DamageRecoveryMonthWise>)query.list();
		if(damageRecoveryMonthWiseList.isEmpty()){
			return null;
		}
		return damageRecoveryMonthWiseList.get(0);
	}
	
	/**
	 * <pre>
	 * fetch DamageRecoveryMonthWise details By ProductId in current month
	 * @param productId
	 * @return DamageRecoveryMonthWise
	 * </pre>
	 */
	@Transactional
	public DamageRecoveryMonthWise fetchDamageRecoveryMonthWiseByProductId(long productId)
	{
		String hql="from DamageRecoveryMonthWise where product.productId="+productId+" and (date(datetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(datetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')"+
				   " and product.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and product.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageRecoveryMonthWise> damageRecoveryMonthWiseList=(List<DamageRecoveryMonthWise>)query.list();
		if(damageRecoveryMonthWiseList.isEmpty())
		{
			return null;
		}
		return damageRecoveryMonthWiseList.get(0);
	}
	/**
	 * <pre>
	 * damage recovery details which against supplier save 
	 * @param damageRecoveryDetails
	 * @param damageRecoveryId
	 * </pre>
	 */
	@Transactional
	public void saveDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails,long damageRecoveryId)
	{
		DamageRecoveryMonthWise damageRecoveryMonthWise=fetchDamageRecoveryMonthWiseByDamageRecoveryId(damageRecoveryId);
		
		damageRecoveryDetails.setDamageRecoveryMonthWise(damageRecoveryMonthWise);
		damageRecoveryDetails.setDamageRecoveryDetailsId(damageRecoveryIdGenerator.generateDamageRecoveryDetailsId());
		sessionFactory.getCurrentSession().save(damageRecoveryDetails);
		
		damageRecoveryMonthWise.setQuantityGiven(damageRecoveryMonthWise.getQuantityGiven()+damageRecoveryDetails.getQuantityGiven());
		
		damageRecoveryMonthWise=(DamageRecoveryMonthWise)sessionFactory.getCurrentSession().merge(damageRecoveryMonthWise);
		sessionFactory.getCurrentSession().update(damageRecoveryMonthWise);
	}
	/**
	 * <pre>
	 * update damage recovery details which against supplier
	 * if any product recovered then increase product current quantity according recovered quantity
	 * @param damageRecoveryDetails
	 * </pre>
	 */
	@Transactional
	public void updateDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails)
	{
		damageRecoveryDetails=(DamageRecoveryDetails)sessionFactory.getCurrentSession().merge(damageRecoveryDetails);
		sessionFactory.getCurrentSession().update(damageRecoveryDetails);
		
		DamageRecoveryMonthWise damageRecoveryMonthWise=damageRecoveryDetails.getDamageRecoveryMonthWise();
		damageRecoveryMonthWise.setQuantityNotClaimed(damageRecoveryDetails.getQuantityNotClaimed()+damageRecoveryMonthWise.getQuantityNotClaimed());
		damageRecoveryMonthWise.setQuantityReceived(damageRecoveryDetails.getQuantityReceived()+damageRecoveryMonthWise.getQuantityReceived());
		
		damageRecoveryMonthWise=(DamageRecoveryMonthWise)sessionFactory.getCurrentSession().merge(damageRecoveryMonthWise);
		sessionFactory.getCurrentSession().update(damageRecoveryMonthWise);
		
		Product product=damageRecoveryMonthWise.getProduct();
		
		//update product current quantity 
		product.setCurrentQuantity(product.getCurrentQuantity()+damageRecoveryDetails.getQuantityReceived());
		Update(product);
		
		//update orderusedproduct depend on its corresponding product
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * fetch damage recovery details which against supplier by  damageRecoveryDetailsId
	 * @param damageRecoveryDetailsId
	 * </pre>
	 */
	@Transactional
	public DamageRecoveryDetails fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(String damageRecoveryDetailsId)
	{
		String hql="from DamageRecoveryDetails where damageRecoveryDetailsId='"+damageRecoveryDetailsId+"' "
				+ " and damageRecoveryMonthWise.product.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and damageRecoveryMonthWise.product.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageRecoveryDetails> damageRecoveryDetailsList=(List<DamageRecoveryDetails>)query.list();
		
		return damageRecoveryDetailsList.get(0);
	}
	/**
	 * <pre>
	 * fetch damage recovery details against supplier by damageRecoveryId(main)
	 * @param damageRecoveryId
	 * @return DamageRecoveryDetails list
	 * </pre>
	 */
	@Transactional
	public List<DamageRecoveryDetails> fetchDamageRecoveryDetailsByDamageRecoveryId(long damageRecoveryId){
		String hql="from DamageRecoveryDetails where damageRecoveryMonthWise.damageRecoveryId="+damageRecoveryId+
				 " and damageRecoveryMonthWise.product.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and damageRecoveryMonthWise.product.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageRecoveryDetails> damageRecoveryDetailsList=(List<DamageRecoveryDetails>)query.list();
		
		return damageRecoveryDetailsList;
	}
	/**
	 * <pre>
	 * save update DamageRecoveryMonthWise
	 * if DamageRecoveryMonthWise
	 * @param productId
	 * @param damageQuantity
	 * @return DamageRecoveryMonthWise
	 * </pre>
	 */
	//List<DamageRecoveryDetails> damageRecoveryDetailsList=fetchDamageRecoveryDayWiseByDamageRecoveryId(damageRecoveryId);
	@Transactional
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryMonthWise(long productId,long damageQuantity)
	{				
		Product product=fetchProductForWebApp(productId);
		DamageRecoveryMonthWise damageRecoveryMonthWise=new DamageRecoveryMonthWise();
		
		damageRecoveryMonthWise=fetchDamageRecoveryMonthWiseByProductId(productId);
		if(damageRecoveryMonthWise==null)
		{
			damageRecoveryMonthWise=new DamageRecoveryMonthWise();
			product.setProductId(productId);
			damageRecoveryMonthWise.setProduct(product);
			damageRecoveryMonthWise.setQuantityDamage(damageQuantity);
			damageRecoveryMonthWise.setDatetime(new Date());
			sessionFactory.getCurrentSession().save(damageRecoveryMonthWise);
		}
		else
		{
			damageRecoveryMonthWise.setQuantityDamage(damageRecoveryMonthWise.getQuantityDamage()+damageQuantity);
			damageRecoveryMonthWise.setDatetime(new Date());
			
			damageRecoveryMonthWise=(DamageRecoveryMonthWise)sessionFactory.getCurrentSession().merge(damageRecoveryMonthWise);
			sessionFactory.getCurrentSession().update(damageRecoveryMonthWise);
		}
		
		return damageRecoveryMonthWise;
	}
	/**
	 * <pre>
	 * fetch DamageRecovery month Wise by 
	 * @param startMonth
	 * @param startYear
	 * @param endMonth
	 * @param endYear
	 * @return DamageRecoveryMonthWise list
	 * </pre>
	 */
	@Transactional
	public List<DamageRecoveryMonthWise> fetchDamageRecoveryDayWise(String startMonth,String startYear,String endMonth,String endYear)
	{		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar calStart=Calendar.getInstance();
		calStart.set(Calendar.YEAR, Integer.parseInt(startYear));
		calStart.set(Calendar.MONTH, Integer.parseInt(startMonth)-1);	
		calStart.set(Calendar.DATE, 1);
		calStart.set(Calendar.DAY_OF_MONTH, 1);
		
		String startDate=dateFormat.format(calStart.getTime());
		
		Calendar calEnd=Calendar.getInstance();
		calEnd.set(Calendar.YEAR, Integer.parseInt(endYear));
		calEnd.set(Calendar.MONTH, Integer.parseInt(endMonth)-1);
		calEnd.set(Calendar.DATE, 1);
		calEnd.set(Calendar.DAY_OF_MONTH, calEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		String endDate=dateFormat.format(calEnd.getTime());
		
		/*String hql="from DamageRecoveryMonthWise where "+
		"( Year(datetime) = :startYear And Month(datetime) >= :startMonth )"+
	    "Or ( Year(datetime) > :startYear And Year(datetime) < :endYear )"+
	    "Or ( Year(datetime) = :endYear And Month(datetime) <= :endMonth )";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("startYear", Integer.parseInt(startYear));
		query.setParameter("startMonth", Integer.parseInt(startMonth));
		query.setParameter("endYear", Integer.parseInt(endYear));
		query.setParameter("endMonth", Integer.parseInt(endMonth));*/
		
		
		
		String hql="from DamageRecoveryMonthWise where date(datetime)>='"+
				    startDate+"' and date(datetime)<='"+endDate+"'"+
					" and product.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		hql+=" and product.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageRecoveryMonthWise> damageRecoveryDayWiseList=(List<DamageRecoveryMonthWise>)query.list();
		
		if(damageRecoveryDayWiseList.isEmpty())
		{
			return null;
		}	
		
		return damageRecoveryDayWiseList;
	}
	/**
	 * <pre>
	 * call this method using scheduler 
	 * create/update DailyStockDetails openingStock,closingStock,dispatchedStock,AddedStock product wise
	 * </pre>
	 */
	@Transactional
	public void creatFirstRecordOfDailyStockReport(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");		
			List<Company> companyList=companyDAO.fetchAllCompany();		
			if(companyList!=null){
				for(Company company : companyList){
					List<BranchModel> branchList=branchDAO.fetchBranchListByCompanyId(company.getCompanyId()); 
					for(BranchModel branch: branchList){
						String hql="from DailyStockDetails where date(date)=date(CURRENT_DATE()) "
					              +" and product.company.companyId="+company.getCompanyId()
					              +" and product.branch.branchId="+branch.getBranchId();
						
						Query query=sessionFactory.getCurrentSession().createQuery(hql);
						
						List<DailyStockDetails> dailyStockDetailsList=(List<DailyStockDetails>)query.list();
						if(dailyStockDetailsList.isEmpty()){
							
							//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
							List<Product> productList=fetchProductListByCompanyId(company.getCompanyId(),branch.getBranchId());
							
							if(productList!=null){
							// creating daily stock report product wise
								for(Product product:productList){
									
								     Calendar cal = Calendar.getInstance();
								     cal.add(Calendar.DATE, -1);
								    
								     String hql1="from DailyStockDetails where date(date)='"+ dateFormat.format(cal.getTime())+"'"+
								    		 	 " and product.productId="+product.getProductId();
								     
										Query query1=sessionFactory.getCurrentSession().createQuery(hql1);
										List<DailyStockDetails> dailyStockDetails=(List<DailyStockDetails>)query1.list();
										DailyStockDetails dailyStockDetails2=new DailyStockDetails();
										if(dailyStockDetails.isEmpty()){
											dailyStockDetails2.setDate(new Date());
											dailyStockDetails2.setOpeningStock(product.getCurrentQuantity());
											dailyStockDetails2.setClosingStock(product.getCurrentQuantity());
											dailyStockDetails2.setDispatchedStock(0);
											dailyStockDetails2.setAddedStock(0);
											dailyStockDetails2.setProduct(product);
										}else{
											dailyStockDetails2.setDate(new Date());
											dailyStockDetails2.setOpeningStock(dailyStockDetails.get(0).getClosingStock());
											dailyStockDetails2.setClosingStock(dailyStockDetails.get(0).getClosingStock());
											dailyStockDetails2.setDispatchedStock(0);
											dailyStockDetails2.setAddedStock(0);
											dailyStockDetails2.setProduct(product);
										}
										
										sessionFactory.getCurrentSession().save(dailyStockDetails2);
										System.out.println(dailyStockDetails2);
								}
							}
						}					
					}					
				}
			}
	}
	/**
	 * <pre>
	 * fetch todays daily stock details
	 * @return DailyStockDetails list
	 * </pre>
	 */
	@Transactional
	public List<DailyStockDetails> fetchTodaysDailyStockDetails(){
	
		String hqlPre="from DailyStockDetails where date(date)='date(CURRENT_DATE())'";    
		Query queryPre=sessionFactory.getCurrentSession().createQuery(hqlPre);
		List<DailyStockDetails> dailyStockDetailsPre=(List<DailyStockDetails>)queryPre.list();
		if(dailyStockDetailsPre.isEmpty()){
			return null;
		}
		return dailyStockDetailsPre;
	}
	/**
	 * <pre>
	 * fetch todays daily stock details by pickDate,companyId
	 * @return DailyStockDetails list
	 * </pre>
	 */
	@Transactional
	public List<DailyStockDetailsModel> fetchDailyStockDetails(String pickDate,long companyId,long brandId) {
		// TODO Auto-generated method stub
		String hql="from DailyStockDetails where date(date)='"+pickDate+"' and product.company.companyId="+companyId+" and product.brand.brandId="+brandId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DailyStockDetails> dailyStockDetailList=(List<DailyStockDetails>)query.list();
		if(dailyStockDetailList.isEmpty()){
			return null;
		}
		
		List<DailyStockDetailsModel> dailyStockDetailsModelList=new ArrayList<>();
		for(DailyStockDetails dailyStockDetails: dailyStockDetailList){
			dailyStockDetailsModelList.add(new DailyStockDetailsModel(
					dailyStockDetails.getProduct().getProductName(), 
					dailyStockDetails.getOpeningStock(),
					dailyStockDetails.getAddedStock(), 
					dailyStockDetails.getDispatchedStock(), 
					dailyStockDetails.getClosingStock()));
		}
		return dailyStockDetailsModelList;
	}
	/**
	 * <pre>
	 * fetch todays daily stock details by product id
	 * @return DailyStockDetails list
	 * </pre>
	 */
	@Transactional
	public DailyStockDetails fetchDailyStockReportByProductIdAndCurrentDate(long productId){
		
		String hql="from DailyStockDetails where date(date)=date(CURRENT_DATE()) and product.productId="+productId;//+" and product.company.companyId="+company.getCompanyId();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DailyStockDetails> dailyStockDetails=(List<DailyStockDetails>)query.list();
		if(dailyStockDetails.isEmpty()){
			return null;
		}
		return dailyStockDetails.get(0);
	}
	/**
	 * <pre>
	 * update daily stock details dispatched and added quantity
	 * @param productId
	 * @param qty
	 * @param isAdd
	 * </pre>
	 */
	@Transactional
	public void updateDailyStockExchange(long productId,long qty,boolean isAdd ){
		DailyStockDetails dailyStockDetails=fetchDailyStockReportByProductIdAndCurrentDate(productId);
		if(dailyStockDetails!=null){
			if(isAdd==false){
				dailyStockDetails.setDispatchedStock(dailyStockDetails.getDispatchedStock()+qty);
				long closingStock=dailyStockDetails.getOpeningStock()+dailyStockDetails.getAddedStock()-dailyStockDetails.getDispatchedStock();
				dailyStockDetails.setClosingStock(closingStock);
				
				dailyStockDetails=(DailyStockDetails)sessionFactory.getCurrentSession().merge(dailyStockDetails);
				sessionFactory.getCurrentSession().update(dailyStockDetails);	
			}else{
				dailyStockDetails.setAddedStock(dailyStockDetails.getAddedStock()+qty);
				dailyStockDetails.setClosingStock(dailyStockDetails.getClosingStock()+qty);
				
				dailyStockDetails=(DailyStockDetails)sessionFactory.getCurrentSession().merge(dailyStockDetails);
				sessionFactory.getCurrentSession().update(dailyStockDetails);
			}
		}
		
	}
	
	@Transactional
	public void saveDamageDefine(DamageDefine damageDefine){
		sessionFactory.getCurrentSession().save(damageDefine);
		
	}
	/**
	 * <pre>
	 * fetch damage define list by range,startDate,endDate
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return DamageDefine list
	 * </pre>
	 */
	@Transactional
	public List<DamageDefine> fetchDamageDefineList(String range,String startDate,String endDate){
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();		
		
		if (range.equals("today")) {
			hql="from DamageDefine where date(date) = date(CURRENT_DATE())";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from DamageDefine where date(date) = '"+dateFormat.format(cal.getTime())+"' ";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from DamageDefine where date(date) > '"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("currentMonth")) {
			hql="from DamageDefine where (date(date) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(date) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("lastMonth")) {
			hql="from DamageDefine where (date(date) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(date) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from DamageDefine where (date(date) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(date) <= '"+DatePicker.getLast3MonthLastDate()+"') ";
		}
		else if (range.equals("last6Months")) {
			hql="from DamageDefine where (date(date) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(date) <= '"+DatePicker.getLast6MonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from DamageDefine where (date(date) >= '"+startDate+"' and date(date) <= '"+endDate+"') ";
		}
		else if (range.equals("pickDate")) {
			hql="from DamageDefine where (date(date) = '"+startDate+"') ";
		}
		else if (range.equals("viewAll")) {
			hql="from DamageDefine where 1=1 ";
		}
		hql+=" and product.company.companyId="+getSessionSelectedCompaniesIds()
		   +" and product.branch.branchId="+getSessionSelectedBranchIds()
		   +" order by date desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DamageDefine> damageDefineList=(List<DamageDefine>)query.list();
		if(damageDefineList.isEmpty()){
			return null;
		}
		return damageDefineList;
	}
	
	/**
	 * <pre>
	 * fetch PermanentDamageDetails list by permanentDamageMonthWiseId
	 * </pre>
	 * @param permanentDamageMonthWiseId
	 * @return PermanentDamageDetails list
	 */
	@Transactional
	public List<PermanentDamageDetails> fetchPermanentDamageDetailsListByPermanentDamageMonthWiseId(long permanentDamageMonthWiseId){
		String hql="from PermanentDamageDetails where permanentDamageMonthWise.permanentDamageMonthWiseId="+permanentDamageMonthWiseId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PermanentDamageDetails> permanentDamageDetailsList=(List<PermanentDamageDetails>)query.list();
		if(permanentDamageDetailsList.isEmpty()){
			return null;
		}
		return permanentDamageDetailsList;
	}
	/**
	 * <pre>
	 * fetch PermanentDamageMonthWise by productId
	 * </pre>
	 * @param productId
	 * @return PermanentDamageMonthWise
	 */
	@Transactional
	public PermanentDamageMonthWise fetchPermanentDamageMonthWiseByPermanentDamageMonthWiseId(long productId){
		String hql="from PermanentDamageMonthWise where product.productId="+productId+" and month(damageDate)=month(CURRENT_DATE())";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PermanentDamageMonthWise> permanentDamageMonthWiseList=(List<PermanentDamageMonthWise>)query.list();
		if(permanentDamageMonthWiseList.isEmpty()){
			return null;
		}		
		return permanentDamageMonthWiseList.get(0);
	}
	/**
	 * <pre>
	 * fetch PermanentDamageMonthWise list by monthNumber
	 * </pre>
	 * @param monthNumber
	 * @return PermanentDamageMonthWise list
	 */
	@Transactional
	public List<PermanentDamageMonthWise> fetchPermanentDamageMonthWiseByMonth(long year,long monthNumber){
		String hql="from PermanentDamageMonthWise where month(damageDate)="+monthNumber+" and year(damageDate)="+year;
		hql+=" and product.company.companyId="+getSessionSelectedCompaniesIds()
		   +" and product.branch.branchId="+getSessionSelectedBranchIds()
		   +" order by damageDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PermanentDamageMonthWise> permanentDamageMonthWiseList=(List<PermanentDamageMonthWise>)query.list();
		if(permanentDamageMonthWiseList.isEmpty()){
			return null;
		}		
		return permanentDamageMonthWiseList;
	}
	/**
	 * <pre>
	 * check PermanentDamageMonthWise have or not by productId
	 * if have then update its damage quatity
	 * else create new 
	 * save PermanenetDamageDetails 
	 * </pre>
	 * @param permanentDamageDetails
	 * @param productId
	 */
	@Transactional
	public void savePermanenetDamageDetails(PermanentDamageDetails permanentDamageDetails,long productId){
		PermanentDamageMonthWise permanentDamageMonthWise=fetchPermanentDamageMonthWiseByPermanentDamageMonthWiseId(productId);
		if(permanentDamageMonthWise==null){
			permanentDamageMonthWise=new PermanentDamageMonthWise();
			permanentDamageMonthWise.setDamageDate(new Date());
			permanentDamageMonthWise.setProduct(fetchProductForWebApp(productId));
			permanentDamageMonthWise.setTotalQuantityDamage(permanentDamageDetails.getQuantityDamage());
			sessionFactory.getCurrentSession().save(permanentDamageMonthWise);
		}else{
			permanentDamageMonthWise.setTotalQuantityDamage(permanentDamageMonthWise.getTotalQuantityDamage()+permanentDamageDetails.getQuantityDamage());
			permanentDamageMonthWise=(PermanentDamageMonthWise)sessionFactory.getCurrentSession().merge(permanentDamageMonthWise);
			sessionFactory.getCurrentSession().update(permanentDamageMonthWise);
		}
		permanentDamageDetails.setPermanentDamageMonthWise(permanentDamageMonthWise);
		sessionFactory.getCurrentSession().save(permanentDamageDetails);
	}
	
	/**
	 * <pre>
	 * check unique or not at time adding / updating Barcode in 
	 * @param checkText
	 * @param type
	 * @param productId
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String checkProductBarcodeDuplication(String checkText,String type,long productId){
		
	String hql="";
		if(type.equals("productBarcode")){
			hql="from Product where productBarcode='"+checkText+"'";
		}
		
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		//hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		if(productId!=0){
			hql+=" and productId<>"+productId;
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Product> productList=(List<Product>)query.list();
		if(productList.isEmpty()){
			return "Success";
		}
		return "Failed";
	}

	
	
	/**
	 * <pre>
	 * fetch Product By Barcode Number 
	 * @param barcodeNo
	 * @return product
	 * </pre>
	 */
	@Transactional
	public Product fetchProductByBarcodeNumber(String barcodeNo){
		
		String hql="from Product where productBarcode='"+barcodeNo+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Product>  product=(List<Product>)query.list();
		if(product.isEmpty()){
			return null;
		}
		return product.get(0);
		
		
	}
}
