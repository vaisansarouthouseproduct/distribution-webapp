package com.bluesquare.rc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.StateDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.utils.AllAccess;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;
/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of StateDAO
 * 1.fetchAllStateForWebApp()
 * 2.saveForWebApp(State state)
 * 3.updateForWebApp(State state)
 * 4.fetchState(long stateId)
 * 5.fetchStateByCountryIdForWebapp(long countryId)
 * 6.fetchStateByStateName(String stateName)
 * </pre>
 */
@Repository("stateDAO")

@Component
public class StateDAOImpl extends TokenHandler implements StateDAO {

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	/**
	 * <pre>
	 * fetch state list
	 * @return State list
	 * </pre>
	 */
	@Transactional
	public List<State> fetchAllStateForWebApp() {
		String hql = "from State where 1=1 ";
		//hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : "+query);
		@SuppressWarnings("unchecked")
		List<State> states = (List<State>) query.list();
		if (states.isEmpty()) {
			return null;
		}
			return states;
		
	}

	/**
	 * <pre>
	 * save state
	 * @param state
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(State state) {
		sessionFactory.getCurrentSession().save(state);
		
	}
	/**
	 * <pre>
	 * update state
	 * @param state
	 * </pre>
	 */
	@Transactional
	public void updateForWebApp(State state) {
		state=(State)sessionFactory.getCurrentSession().merge(state);
		sessionFactory.getCurrentSession().update(state);
		
	}
	/**
	 * <pre>
	 * fetch state by stateId
	 * @param stateId
	 * @return state
	 * </pre>
	 */
	@Transactional
	public State fetchState(long stateId) {
		String hql = "from State where stateId=" + stateId;
		//hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<State> states= (List<State>) query.list();
		if (states.isEmpty()) {
			return null;
		}
		return states.get(0);
	}
	/**
	 * <pre>
	 * fetch state list by country id
	 * @param countryId
	 * @return State list
	 * </pre>
	 */
	@Transactional
	public List<State> fetchStateByCountryIdForWebapp(long countryId) {
		String hql = "from State where country.countryId=" + countryId;
		//hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<State> states= (List<State>) query.list();
		if (states.isEmpty()) {
			return null;
		}
		return states;
	}
	
	/**
	 * <pre>
	 * fetch state with state name
	 * @param stateName
	 * @return State
	 * </pre>
	 */
	@Transactional
	public State fetchStateByStateName(String stateName) {
		Query query=sessionFactory.getCurrentSession().createQuery("from State where name LIKE '"+stateName+"%'");
		List<State> stateList=(List<State>)query.list();
		
		if(stateList.isEmpty())
		{
			return null;
		}
		
		return stateList.get(0);
	}
	
	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long stateId : allAccess.getStateIdList())
		{
			ids+=stateId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and stateId in ("+ids+")";
		
		return hql;
	}*/

	/*@Override
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] stateIdList=decodedJWT.getClaim("stateIdList").asArray(longClass);
				
				String ids="";
				for(Long stateId : stateIdList)
				{
					ids+=stateId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and stateId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}*/

}
