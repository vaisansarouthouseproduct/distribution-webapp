package com.bluesquare.rc.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.MeetingDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.MeetingScheduleModel;
import com.bluesquare.rc.rest.models.MeetingRequest;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 07-06-2018 Method Create with Code Documentation
 * 1. saveMeeting(Meeting meeting);
 * 2. updateMeeting(Meeting meeting);
 * 3. deleteMeeting(long meetingId);
 * 4. updateCancelStatus(long meetingId);
 * 5. fetchMeetingByMeetingId(long meetingId);
 * 6. fetchMeetingListbyEmployeeIdAndDate(long employeeDetailsId,String pickDate);
 * 7. fetchMeetingsbyEmployeeIdAndDate(long employeeId, String pickDate)
 * 8. fetchEmployeeWiseScheduledMeeting(long departmentId,String pickDate);
 * 9. checkMeetingSchedule(long employeeId,String date,String fromTime,String toTime)
 * 10. fetchCompletedMeetingList(String range,String startDate,String endDate)
 * 11. meetingCompleteWithReview(MeetingRequest meetingRequest)
 * 12. fetchCompletedMeetingList(String range,String startDate,String endDate)
 * 13. fetchCancelledMeetingList(String range,String startDate,String endDate)
 * 14. fetchPendingMeetingList(String range,String startDate,String endDate)
 * 15.fetchMeetingByShopVistedId(long shopVistedId)
 * </pre>
 */
/**
 * @author mahesh
 *
 */
/**
 * @author mahesh
 *
 */
@Repository("meetingDAO")
@Component
public class MeetingDAOImpl extends TokenHandler implements MeetingDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	/**
	 * <pre>
	 * save meeting against specified employee
	 * @param meeting
	 * </pre>
	 */
	@Transactional
	public void saveMeeting(Meeting meeting) {
		sessionFactory.getCurrentSession().save(meeting);
	}
	/**
	 * <pre>
	 * update meeting against specified employee
	 * or transfer meeting to another employee 
	 * @param meeting
	 * </pre>
	 */
	@Transactional
	public void updateMeeting(Meeting meeting) {
		meeting=(Meeting)sessionFactory.getCurrentSession().merge(meeting);
		sessionFactory.getCurrentSession().update(meeting);
	}
	/**
	 * <pre>
	 * delete meeting details permantaly
	 * @param meetingId
	 * </pre>
	 */
	@Transactional
	public void deleteMeeting(long meetingId) {
		Meeting meeting=fetchMeetingByMeetingId(meetingId);
		sessionFactory.getCurrentSession().delete(meeting);
	}
	/**
	 * <pre>
	 * set cancel status true for cancel meeting 
	 * but not delete record from db
	 * @param meetingId
	 * </pre>
	 */
	@Transactional
	public void updateCancelStatus(long meetingId, String cancelReason) {
		Meeting meeting=fetchMeetingByMeetingId(meetingId);
		meeting.setMeetingStatus(Constants.MEETING_CANCEL);
		meeting.setCancelReason(cancelReason);
		sessionFactory.getCurrentSession().update(meeting);
	}
	/**
	 * <pre>
	 * fetch meeting details by meeting id
	 * @param meetingId
	 * @return Meeting
	 * </pre>
	 */
	@Transactional
	public Meeting fetchMeetingByMeetingId(long meetingId) {
		return (Meeting)sessionFactory.getCurrentSession().get(Meeting.class, meetingId);
	}
	/**
	 * <pre>
	 * fetch meeting list by employee details id and picked date
	 * @param employeeDetailsId
	 * @param pickDate
	 * @return Meeting list
	 * </pre>
	 */
	@Transactional
	public List<Meeting> fetchMeetingListbyEmployeeIdAndDate(long employeeDetailsId, String pickDate) {
		String hql="from Meeting where 1=1 ";
		hql+=" and employeeDetails.employeeDetailsId="+employeeDetailsId;
		hql+=" and date(meetingFromDateTime)='"+pickDate+"' and date(meetingToDateTime)='"+pickDate+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	
	/**
	 * <pre>
	 * fetch meeting list by employee details id and picked date
	 * @param employeeDetailsId
	 * @param pickDate
	 * @return Meeting list
	 * </pre>
	 */
	@Transactional
	public List<Meeting> fetchMeetingListbyEmployeeIdAndDateExceptCancel(long employeeDetailsId, String pickDate) {
		String hql="from Meeting where 1=1 ";
		hql+=" and meetingStatus!='"+Constants.MEETING_CANCEL+"'";
		hql+=" and employeeDetails.employeeDetailsId="+employeeDetailsId;
		hql+=" and date(meetingFromDateTime)='"+pickDate+"' and date(meetingToDateTime)='"+pickDate+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	
	/**
	 * <pre>
	 * fetch pending meetings list by employee id and pick date
	 * condition with company
	 * @param employeeId
	 * @param pickDate
	 * @return Meeting list
	 * </pre>
	 */
	@Transactional
	public List<Meeting> fetchMeetingsbyEmployeeIdAndDate(long employeeId, String pickDate) {
		String hql="from Meeting where 1=1 ";
		hql+=" and employeeDetails.employee.employeeId="+employeeId;
		hql+=" and date(meetingFromDateTime)='"+pickDate+"' and date(meetingToDateTime)='"+pickDate+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and meetingStatus ='"+Constants.MEETING_PENDING +"'";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	/**
	 * <pre>
	 * fetch employee wise meeting data
	 * @param departmentId
	 * @param pickDate
	 * @return MeetingScheduleModel list
	 * </pre>
	 */
	@Transactional
	public List<MeetingScheduleModel> fetchEmployeeWiseScheduledMeeting(long departmentId, String pickDate) {
		List<MeetingScheduleModel> meetingScheduleList=new ArrayList<>();
		List<EmployeeDetails> employeeDetailsList=null;
		if(departmentId==0){
			employeeDetailsList=employeeDetailsDAO.fetchEmployeeDetailsList();
		}else{
			employeeDetailsList=employeeDetailsDAO.fetchEmployeeDetailsByDepartmentId(departmentId);
		}
		if(employeeDetailsList!=null){
			long srno=1;
			for(EmployeeDetails employeeDetails: employeeDetailsList){
				boolean isMeetingScheduled=false;
				String hql="from Meeting where employeeDetails.employeeDetailsId="+employeeDetails.getEmployeeDetailsId();
				hql+=" and date(meetingFromDateTime)='"+pickDate+"' and date(meetingToDateTime)='"+pickDate+"'";
				hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
				hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
				Query query=sessionFactory.getCurrentSession().createQuery(hql);
				List<Meeting> meetingList=(List<Meeting>)query.list();
				if(meetingList.isEmpty()){
					isMeetingScheduled=false;
				}else{
					isMeetingScheduled=true;
				}
				
				meetingScheduleList.add(new MeetingScheduleModel(
						srno, 
						employeeDetails.getEmployeeDetailsId(),
						employeeDetails.getName(),
						employeeDetails.getEmployee().getDepartment().getName(),
						isMeetingScheduled));
				srno++;
			}
		}
		if(meetingScheduleList.isEmpty()){
			return null;
		}
		return meetingScheduleList;
	}
	/**
	 * <pre>
	 * check meeting already set or not in given time
	 * @param employeeId
	 * @param date
	 * @param fromTime
	 * @param toTime
	 * @return msg if already meeting set
	 * </pre>
	 */
	@Transactional
	public String checkMeetingSchedule(long employeeId, String date, String fromTime, String toTime) {
		
		
		Calendar calendarStart=Calendar.getInstance();
		Calendar calendarEnd=Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		try {
			calendarStart.setTime(sdf.parse(date+" "+fromTime));
			calendarEnd.setTime(sdf.parse(date+" "+toTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm:ss", Locale.US);//HH for 24 hour clock
		
		String hql="from Meeting where 1=1 ";
		hql+=" and meetingStatus!='"+Constants.MEETING_CANCEL+"'";
		hql+=" and employeeDetails.employee.employeeId="+employeeId;
		hql+=" and date(meetingFromDateTime)='"+date+"' and date(meetingToDateTime)='"+date+"'";
		hql+=" and (((time(meetingFromDateTime)<='"+sdf_time.format(calendarStart.getTime())+"' and time(meetingToDateTime)>='"+sdf_time.format(calendarStart.getTime())+"')";
		hql+=" or (time(meetingFromDateTime)<='"+sdf_time.format(calendarEnd.getTime())+"' and time(meetingToDateTime)>='"+sdf_time.format(calendarEnd.getTime())+"'))";
		hql+=" or (time(meetingFromDateTime)>'"+sdf_time.format(calendarStart.getTime())+"' and time(meetingToDateTime)<'"+sdf_time.format(calendarEnd.getTime())+"'))";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		String msg="";
		if(meetingList.isEmpty()){
			return "";
		}else{
			SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
    		msg="Meeting already set from "+date+" "+dateFormat.format(meetingList.get(0).getMeetingFromDateTime())+
    			" to "+dateFormat.format(meetingList.get(0).getMeetingToDateTime());
    		
    		return msg;
		}
		
		/*EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(employeeId);
		List<Meeting> meetingList=fetchMeetingListbyEmployeeIdAndDateExceptCancel(employeeDetails.getEmployeeDetailsId(), date);
		
		String msg="";
		
		if(meetingList!=null){
			for(Meeting meeting : meetingList){
				
				Calendar cStart = Calendar.getInstance();
				Calendar cEnd = Calendar.getInstance();				
				cStart.setTime(meeting.getMeetingFromDateTime());
				cEnd.setTime(meeting.getMeetingToDateTime());
					
				    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
				    	while (calendarStart.before(calendarEnd) || calendarStart.equals(calendarEnd)) 
						{ 
				    		if(cStart.getTimeInMillis()==calendarStart.getTimeInMillis())
							{
					    		SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
					    		msg="Meeting already set from "+date+" "+dateFormat.format(meeting.getMeetingFromDateTime())+" to "+dateFormat.format(meeting.getMeetingToDateTime());
					    		
					    		return msg;
							}	
					    	calendarStart.add(Calendar.MINUTE, 1);
						}
				    	
				    	try {
							calendarStart.setTime(sdf.parse(date+" "+fromTime));
						} catch (Exception e) {
							e.printStackTrace();
						}
				    	
						cStart.add(Calendar.MINUTE, 1);
					}
				}		
			}
		
		return msg;*/
	}
	
	/**
	 * <pre>
	 * check meeting already set or not in given time for update meeting
	 * @param employeeId
	 * @param date
	 * @param fromTime
	 * @param toTime
	 * @return msg if already meeting set
	 * </pre>
	 */
	@Transactional
	public String checkMeetingScheduleForUpdate(long employeeId, String date, String fromTime, String toTime,long meetingId) {
		Calendar calendarStart=Calendar.getInstance();
		Calendar calendarEnd=Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		try {
			calendarStart.setTime(sdf.parse(date+" "+fromTime));
			calendarEnd.setTime(sdf.parse(date+" "+toTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm:ss", Locale.US);//HH for 24 hour clock
		
		String hql="from Meeting where 1=1 ";
		hql+=" and meetingId!="+meetingId;
		hql+=" and meetingStatus!='"+Constants.MEETING_CANCEL+"'";
		hql+=" and employeeDetails.employee.employeeId="+employeeId;
		hql+=" and date(meetingFromDateTime)='"+date+"' and date(meetingToDateTime)='"+date+"'";
		hql+=" and (((time(meetingFromDateTime)<='"+sdf_time.format(calendarStart.getTime())+"' and time(meetingToDateTime)>='"+sdf_time.format(calendarStart.getTime())+"')";
		hql+=" or (time(meetingFromDateTime)<='"+sdf_time.format(calendarEnd.getTime())+"' and time(meetingToDateTime)>='"+sdf_time.format(calendarEnd.getTime())+"'))";
		hql+=" or (time(meetingFromDateTime)>'"+sdf_time.format(calendarStart.getTime())+"' and time(meetingToDateTime)<'"+sdf_time.format(calendarEnd.getTime())+"'))";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		String msg="";
		if(meetingList.isEmpty()){
			return "";
		}else{
			SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
    		msg="Meeting already set from "+date+" "+dateFormat.format(meetingList.get(0).getMeetingFromDateTime())+
    			" to "+dateFormat.format(meetingList.get(0).getMeetingToDateTime());
    		
    		return msg;
		}
		/*EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(employeeId);
		List<Meeting> meetingList=fetchMeetingListbyEmployeeIdAndDateExceptCancel(employeeDetails.getEmployeeDetailsId(), date);
		
		String msg="";
		
		if(meetingList!=null){
			for(Meeting meeting : meetingList){
				
				//skip current updating meeting checking 
				if(meeting.getMeetingId()==meetingId){
					continue;
				}
				

				
				Calendar cStart = Calendar.getInstance();
				Calendar cEnd = Calendar.getInstance();				
				cStart.setTime(meeting.getMeetingFromDateTime());
				cEnd.setTime(meeting.getMeetingToDateTime());
					
				    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
				    	while (calendarStart.before(calendarEnd) || calendarStart.equals(calendarEnd)) 
						{ 
				    		if(cStart.getTimeInMillis()==calendarStart.getTimeInMillis())
							{
					    		SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
					    		msg="Meeting already set from "+date+" "+dateFormat.format(meeting.getMeetingFromDateTime())+" to "+dateFormat.format(meeting.getMeetingToDateTime());
					    		
					    		return msg;
							}	
					    	calendarStart.add(Calendar.MINUTE, 1);
						}
				    	
				    	try {
							calendarStart.setTime(sdf.parse(date+" "+fromTime));
						} catch (Exception e) {
							e.printStackTrace();
						}
				    	
						cStart.add(Calendar.MINUTE, 1);
					}
				
			}
		}		
			
		
		return msg;*/
	}
	
	/**
	 * <pre>
	 * change meeting status changed as meeting is completed
	 * @param MeetingRequest
	 * </pre>
	 */
	@Transactional
	public void meetingCompleteWithReview(MeetingRequest meetingRequest) {
		// TODO Auto-generated method stub
		Meeting meeting=fetchMeetingByMeetingId(meetingRequest.getMeetingId());
		meeting.setMeetingStatus(Constants.MEETING_COMPLETE);
		meeting.setMeetingReview(meetingRequest.getMeetingReview());
		meeting.setUpdatedDateTime(new Date());
		
		sessionFactory.getCurrentSession().update(meeting);
		
	}
	/**
	 * <pre>
	 * fetch completed meeting list by employee id and given range
	 * </pre>
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return Meeting list
	 */
	@Transactional
	
	public List<Meeting> fetchCompletedMeetingList(String range,String startDate,String endDate){
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String hql="from Meeting where 1=1 ";
		hql+=" and employeeDetails.employee.employeeId="+getAppLoggedEmployeeId();
		
		if(range.equals("range")){
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("lastMonth")){
			cal.add(Calendar.MONTH, -1);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -3);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("pickDate")){
			hql+=" and date(meetingFromDateTime)='"+startDate+"'";
		}
		
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and meetingStatus ='"+Constants.MEETING_COMPLETE+"'";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	
	/**
	 * <pre>
	 * fetch cancel meeting list by employee id and given range
	 * </pre>
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return Meeting list
	 */
	@Transactional
	public List<Meeting> fetchCancelledMeetingList(String range,String startDate,String endDate){
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String hql="from Meeting where 1=1 ";
		hql+=" and employeeDetails.employee.employeeId="+getAppLoggedEmployeeId();
		
		if(range.equals("range")){
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("lastMonth")){
			cal.add(Calendar.MONTH, -1);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -3);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("pickDate")){
			hql+=" and date(meetingFromDateTime)='"+startDate+"'";
		}
		
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and meetingStatus ='"+Constants.MEETING_CANCEL+"'";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	
	/**
	 * <pre>
	 * fetch pending meeting list by employee id and given range
	 * </pre>
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return Meeting list
	 */
	@Transactional
	public List<Meeting> fetchPendingMeetingList(String range,String startDate,String endDate){
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String hql="from Meeting where 1=1 ";
		hql+=" and employeeDetails.employee.employeeId="+getAppLoggedEmployeeId();
		
		if(range.equals("range")){
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("lastMonth")){
			cal.add(Calendar.MONTH, -1);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -3);
			startDate=dateFormat.format(cal.getTime());
			endDate=dateFormat.format(new Date());
			hql+=" and date(meetingFromDateTime)>='"+startDate+"' and date(meetingToDateTime)<='"+endDate+"'";
		}else if(range.equals("pickDate")){
			hql+=" and date(meetingFromDateTime)='"+startDate+"'";
		}
		
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and meetingStatus ='"+Constants.MEETING_PENDING+"'";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meetingList=(List<Meeting>)query.list();
		if(meetingList.isEmpty()){
			return null;			
		}
		return meetingList;
	}
	
	/**
	 * <pre>
	 * rescheduleTheMeeting
	 * </pre>
	 * @param MeetingModel 
	 */
	@Transactional
	public void rescheduleTheMeeting(Meeting meeting) {
		// TODO Auto-generated method stub
		
	}
	@Transactional
	public Meeting fetchMeetingByShopVistedId(long shopVistedId) {
		// TODO Auto-generated method stub
		String hql="from Meeting where shopVisit.id="+shopVistedId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Meeting> meeting=(List<Meeting>)query.list();
		if (meeting.isEmpty()) {
			return null;
		}
		return meeting.get(0);
	}
}
