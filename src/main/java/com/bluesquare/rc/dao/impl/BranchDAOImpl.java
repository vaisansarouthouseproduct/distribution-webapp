package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.models.BranchModel;

@Repository("branchDAO")
@Component
public class BranchDAOImpl implements BranchDAO {

	@Autowired
	HttpSession session;

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<BranchModel> fetchBranchListByCompanyId(long companyId) {
		List<BranchModel> branchModelList =new ArrayList<>();
		String hql = "from Branch where company.companyId=" + companyId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Branch> branchList = (List<Branch>) query.list();
		if (branchList.isEmpty()) {
			return null;
		}
		
		for(Branch branch: branchList){
			branchModelList.add(new BranchModel(branch.getBranchId(), branch.getName(), branch.getCompany(), branch.getBalance()));
		}
		
		return branchModelList;
	}

	@Transactional
	public List<Branch> fetchBranchListByBrachIdList(long[] branchIds) {

		String branchIdStr = "";
		for (long branchId : branchIds) {
			branchIdStr += branchId + ",";
		}
		branchIdStr = branchIdStr.substring(0, branchIdStr.length() - 1);

		String hql = "from Branch where branchId in (" + branchIdStr + ")";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Branch> branchList = (List<Branch>) query.list();
		
		if (branchList.isEmpty()) {
			return null;
		}
		
		return branchList;
	}
	
	

	@Transactional
	public Branch fetchBranchByBranchId(long branchId) {
		String hql = "from Branch where branchId=" + branchId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Branch> branchList = (List<Branch>) query.list();
		if (branchList.isEmpty()) {
			return null;
		}
		return branchList.get(0);
	}

	
	@Transactional
	public void updateBranch(Branch branch){
		sessionFactory.getCurrentSession().update(branch);
	}
	

	@Transactional
	public List<BranchModel> fetchBranchByEmployeeId(long employeeId) {
		List<BranchModel> branchModelList=new ArrayList<>();
		String hql = "select eb.branch from EmployeeBranches eb where eb.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Branch> branchList = (List<Branch>) query.list();
		if (branchList.isEmpty()) {
			return null;
		}
		for(Branch branch: branchList){
			branchModelList.add(new BranchModel(branch.getBranchId(), branch.getName(), branch.getCompany(), branch.getBalance()));
		}
		return branchModelList;
	}
}
