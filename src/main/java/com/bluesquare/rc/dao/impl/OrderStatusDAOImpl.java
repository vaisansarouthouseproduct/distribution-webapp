package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.OrderStatusDAO;
import com.bluesquare.rc.entities.OrderStatus;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 28-05-2018 Code Documentation
 * provides Implementation for following methods of OrderStatusDAO
 * 1.fetchOrderStatus(String name)
 * </pre>
 */
@Repository("orderStatusDAO")
public class OrderStatusDAOImpl extends TokenHandler implements OrderStatusDAO{
	
	@Autowired
	SessionFactory sessionFactory;
	
	/**
	 * <pre>
	 * fetch order status details by name
	 * @param name
	 * @return OrderStatus
	 * </pre>
	 */
	@Transactional
	public OrderStatus fetchOrderStatus(String name) {
		String hql = "from OrderStatus where lower(name) like '%" + name+"%'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<OrderStatus> list = (List<OrderStatus>) query.list();
		if (list != null && !list.isEmpty()) {
			System.out.println(Constants.RETURN_DATA);
			return list.get(0);
		} else {
			System.out.println(Constants.RETURN_EMPTY_DATA);
			return list.get(0);
			}
		
	}


	

	
}
