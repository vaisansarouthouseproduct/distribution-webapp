package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.utils.DateUtils;

/**
 * <pre>
 * @author Sachin Pawar 29-05-2018 Code Documentation
 * provides Implementation for following methods of TransportationDAO
 * 1.fetchById(String id)
 * 2.save(Transportation transportation)
 * 3.update(Transportation transportation)
 * 4.delete(Transportation transportation)
 * 5.List<Transportation> fetchAll()
 * </pre>
 */
@Repository("transportationDAO")
@Component
public class TransportationDAOImpl extends TokenHandler implements TransportationDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;

	/**
	 * fetch transportation details by id
	 * @param id
	 * @return Transportation
	 */
	@Transactional
	public Transportation fetchById(String id) {
		String hql=" from Transportation where id='"+id+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Transportation> transportations = (List<Transportation>)query.list();
		return transportations.get(0);
	}
	/**
	 * <pre>
	 * save transportation details
	 * @param transportation
	 * </pre>
	 */
	@Transactional
	public void save(Transportation transportation) {
		// TODO Auto-generated method stub
		String currentTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		transportation.setInsertedDate(currentTime);
		transportation.setStatus(true);
		transportation = (Transportation)sessionFactory.getCurrentSession().merge(transportation);
		sessionFactory.getCurrentSession().save(transportation);
		
	}
	/**
	 * <pre>
	 * update transportation details
	 * @param transportation
	 * </pre>
	 */
	@Transactional
	public void update(Transportation transportation) {
		// TODO Auto-generated method stub
		String currentTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		transportation.setUpdatedDate(currentTime);
		transportation = (Transportation)sessionFactory.getCurrentSession().merge(transportation);
		sessionFactory.getCurrentSession().update(transportation);
		
	}
	/**
	 * <pre>
	 * disable transportation details
	 * @param transportation
	 * </pre>
	 */
	@Transactional
	public void delete(Transportation transportation) {
		// TODO Auto-generated method stub
		String currentTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		transportation.setUpdatedDate(currentTime);
		transportation.setStatus(false);
		transportation = (Transportation)sessionFactory.getCurrentSession().merge(transportation);
		sessionFactory.getCurrentSession().update(transportation);
		
	}
	/**
	 * <pre>
	 * fetch transportation details list by logged user company
	 * @param transportation
	 * </pre>
	 */
	@Transactional
	public List<Transportation> fetchAll() {
		String hql=" from Transportation where status=true and company.companyId="+getSessionSelectedCompaniesIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Transportation> transportations = (List<Transportation>)query.list();
		return transportations;
	}
	/**
	 * <pre>
	 * fetch transportation details list by logged user company
	 * @param transportation
	 * </pre>
	 */
	@Transactional
	public List<TransportationModel> fetchTransportationModelAll() {
		
		List<Transportation> transportations = fetchAll();
		if(transportations==null){
			return null;
		}
		List<TransportationModel> transportationModelList=new ArrayList<>();
		for(Transportation transportation: transportations){
			transportationModelList.add(new TransportationModel(
					transportation.getId(),
					transportation.getTransportName(), 
					transportation.getMobNo(), 
					transportation.getGstNo(), 
					transportation.getVehicleNo()));
		}
		return transportationModelList;
	}

	

}
