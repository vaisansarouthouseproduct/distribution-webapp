package com.bluesquare.rc.dao.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlProcessor.MatchStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeBasicSalaryStatus;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.entities.EmployeeLocation;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.entities.EmployeeSalary;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.models.EmployeeHolidayList;
import com.bluesquare.rc.models.EmployeeHolidayModel;
import com.bluesquare.rc.models.EmployeeHrmDetailsResponse;
import com.bluesquare.rc.models.EmployeeLastLocation;
import com.bluesquare.rc.models.EmployeePaymentModel;
import com.bluesquare.rc.models.EmployeeRouteList;
import com.bluesquare.rc.models.EmployeeRouteListModel;
import com.bluesquare.rc.models.EmployeeSalaryModel;
import com.bluesquare.rc.models.EmployeeSalaryStatus;
import com.bluesquare.rc.models.EmployeeViewModel;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.EmployeeDetailsModel;
import com.bluesquare.rc.rest.models.EmployeeHolidayDetailsResponse;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.EmployeePaymentDetailsResponse;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.rest.models.SalesManReport;
import com.bluesquare.rc.rest.models.SalesManReportSub;
import com.bluesquare.rc.utils.ChatRegistration;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.EmployeeDetailsIdGenerator;
import com.bluesquare.rc.utils.EmployeeUserCodeGenerator;
import com.bluesquare.rc.utils.MathUtils;
import com.bluesquare.rc.utils.SendSMS;

/**
 * <pre>
 * @author Sachin Pawar 24-05-2018 Code Documentation
 * provides Implementation for following methods of EmployeeDetailsDAO
 * 1.saveForWebApp(EmployeeDetails employeeDetails);
* 2.updateForWebApp(EmployeeDetails employeeDetails, boolean updateValid);
* 3.employeeDetailsListForWebApp();
* 4.fetchEmployeeDetailsForWebApp(long employeeDetailsId);
* 5.fetchEmployeeAreaListByEmployeeId(long employeeId);
* 6.fetchEmployeeDetailsForView();
* 7.fetchEmployeeDetailsForGkEmployeeView();
* 8.fetchEmployeeDetailsList();
* 9.fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId);
* 10.tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,String range, long employeeDetailsId);
* 11.fetchEmployeeHolidayModelForWebApp(long employeeDetailsId, String filter,String startDate, String endDate);
* 12.fetchEmployeeAreaDetails(long employeeDetailsId,long companyId);
* 13.bookHolidayForWebApp(EmployeeHolidays employeeHolidays);
* 14.checkHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId);
* 15.checkUpdatingHolidayGivenOrNot(String startDate, String endDate, long employeeDetailsId,long employeeHolidayId);
* 16.giveIncentives(EmployeeIncentives employeeIncentives);
* 17.fetchIncentives(long employeeIncentiveId);
* 18.givePayment(EmployeeSalary employeeSalary);
* 19.fetchEmployeeSalary(long employeeSalaryId);
* 20.updatePayment(EmployeeSalary employeeSalary);
* 21.openPaymentModel(long employeeDetailsId);
* 22.sendSMSTOEmployee(String employeeDetailsIdList, String smsText, String mobileNumber);
* 23.getEmployeeDetailsByemployeeId(long employeeId);
* 24.clearToken(String token,long employeeId);
* 25.getGateKeeperEmployeeDetailsByareaId(long areaId);
* 26.fetchAreaByEmployeeId(long employeeId);
* 27.fetchDBEmployeeDetailByAreaId(long areaId);
* 28.fetchSMEmployeeDetailByAreaId(long areaId);
* 29.fetchEmployeeDetail(long employeeDetailId);
* 30.fetchDeliveryBoyListByBusinessNameAreaId(long businessNameAreaId);
* 31.fetchSalesManReport(String range, String startDate, String endDate);
* 32.fetchSMandDBByGateKeeperId(long gateKeeperId);
* 33.fetchDBByGateKeeperId(long employeeId);
* 34.fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId);
* 35.saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);
* 36.updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);
* 37.fetchEmployeeIncentiveListByFilter(long employeeDetailsId, String filter,String startDate, String endDate);
* 38.updateIncentive(EmployeeIncentives employeeIncentives);
* 39.updateEmployeeHoliday(EmployeeHolidays employeeHolidays);
* 40.fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId);
* 41.fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId);
* 42.fetchEmployeeLastLocationByDepartmentId(long deparmentId);
* 43.fetchEmployeeLocationByEmployeeDetailsId(long employeeDetailsId);
* 44.fetchEmployeeLastLocationByEmployeeDetailsId(long employeeDetailsId);
* 45.fetchEmployeeDetailsByDepartmentId(long deparmentId);
* 46.saveEmployeeLocation(EmployeeLocation employeeLocation);
* 47.fetchEmployeeListForChat();
* 48.totalEmployeeeSalaryAmountForProfitAndLoss(String startDate,String endDate);
* 49.fetchEmployeeLeaveAndBalanceAmountForAPP(long employeeId)
* 50.fetchAreaListByCompanyId(long companyId);
* 51. getAdminEmployee();
* 52.List<Object[]> fetchEmployeeLocationIdsDetails(long employeeDetailsId,long companyId)
 * </pre>
 */
@Repository("employeeDetailsDAO")

@Component
public class EmployeeDetailsDAOImpl extends TokenHandler implements EmployeeDetailsDAO {

	@Autowired 
	SessionFactory sessionFactory;

	@Autowired
	EmployeeDetails employeeDetails;
	
	@Autowired
	EmployeeDetails employeeDetail;
	
	@Autowired
	HttpSession session; 
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	EmployeeDetailsIdGenerator employeeDetailsIdGenerator;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	EmployeeDAO employeeDAO;
	
	@Autowired
	EmployeeUserCodeGenerator employeeUserCodeGenerator;
	
	public EmployeeDetailsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public EmployeeDetailsDAOImpl() {
	}
	/**
	 * <pre>
	 * contact details save
	 * generate employeeDetailsId
	 * save employee details 
	 * @param employeeDetails
	 * </pre>
	 */
	@Transactional
	public void saveForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.save(employeeDetails.getContact());
		
		employeeDetails.setUserCode(employeeUserCodeGenerator.generateEmployeeUserCode());
		employeeDetails.setEmployeeDetailsGenId(employeeDetailsIdGenerator.generateEmployeeDetailsId(employeeDetails.getEmployee().getDepartment()));
		sessionFactory.getCurrentSession().save(employeeDetails);
		
		ChatRegistration.chatRegistration(employeeDetails.getUserCode(), employeeDetails.getUserCode());
	}
	/**
	 * <pre>
	 * update contact details 
	 * generate employeeDetailsId if department change
	 * update employee details 
	 * @param employeeDetails
	 * @param updateValid
	 * </pre>
	 */
	@Transactional
	public void updateForWebApp(EmployeeDetails employeeDetails,boolean updateValid) {
		// TODO Auto-generated method stub
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.update(employeeDetails.getContact());
		
		if(updateValid)
		{
			employeeDetails.setEmployeeDetailsGenId(employeeDetailsIdGenerator.generateEmployeeDetailsId(employeeDetails.getEmployee().getDepartment()));
		}
		employeeDetails=(EmployeeDetails)sessionFactory.getCurrentSession().merge(employeeDetails);
		sessionFactory.getCurrentSession().update(employeeDetails);
		//ChatRegistration.chatRegistration(employeeDetails.getEmployee().getUserId(), employeeDetails.getEmployee().getPassword());
	}
	/**
	 * save EmployeeProfilePicture
	 * @param employeeProfilePicture
	 */
	@Transactional
	public void saveEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture){
		sessionFactory.getCurrentSession().save(employeeProfilePicture);
	}
	/**
	 * update EmployeeProfilePicture
	 * @param employeeProfilePicture
	 */
	@Transactional
	public void updateEmployeeProfilePicture(EmployeeProfilePicture employeeProfilePicture){
		employeeProfilePicture=(EmployeeProfilePicture)sessionFactory.getCurrentSession().merge(employeeProfilePicture);
		sessionFactory.getCurrentSession().update(employeeProfilePicture);
	}
	
	/**
	 * fetch employee image by employee id
	 * @return emp image base64 string
	 */
	@Transactional
	public EmployeeProfilePicture fetchEmployeeImage(){
		String hql="from EmployeeProfilePicture where employee.employeeId="+getAppLoggedEmployeeId();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeProfilePicture> employeeProfilePictureList = (List<EmployeeProfilePicture>) query.list();
		return employeeProfilePictureList.get(0);
	}
	/**
	 * <pre>
	 * fetch employeeDetailsList which is from logged user company
	 * @return EmployeeDetails list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeDetails> employeeDetailsListForWebApp() {
		String hql = "from EmployeeDetails "+
		"WHERE employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
		if (employeeList.isEmpty()) {
			return null;
		}
		return employeeList;
	}
	/**
	 * <pre>
	 * fetch employee details by employeeDetailsId
	 * @param employeeDetailsId
	 * @return EmployeeDetails
	 * </pre>
	 */
	@Transactional
	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId) {
		String hql = "from EmployeeDetails "+
		"where employeeDetailsId='"+employeeDetailsId+"'";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}
	/**
	 * <pre>
	 * fetch Employee Area List by employeeId
	 * @param employeeId
	 * @return EmployeeAreaList 
	 * </pre>
	 */
	@Transactional
	public List<EmployeeAreaList> fetchEmployeeAreaListByEmployeeId(long employeeId) {
		String hql="from EmployeeAreaList where employeeDetails.employee.employeeId="+employeeId;
		hql+=" and employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeAreaList> employeeAreaList = (List<EmployeeAreaList>) query.list();
		if (employeeAreaList.isEmpty()) {
			return null;
		}
		return employeeAreaList;
	}
	/**
	 * <pre>
	 * fetch employeeDetailsList which is from logged user company
	 * @return EmployeeDetails list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeDetails> fetchEmployeeDetailsList() {
		// TODO Auto-generated method stub
		String hql = "from EmployeeDetails "+
				"WHERE employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		Iterator<EmployeeDetails> itr=employeeList.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
		if (employeeList.isEmpty()) {
			return null;
		}
		
		return employeeList;
	}
	
	public boolean findBranchSameOrNot(long employeeId){
		List<Branch> branchList=employeeDAO.fetchBranchListByEmployeeId(employeeId);	
		for(Branch branch : branchList){
			if(branch.getBranchId()==getSessionSelectedBranchIds()){
				return true;
			}
		}			
		return false;
	}
	
	/**
	 * <pre>
	 * fetch employeeDetailsList which is from logged user company
	 * if startDate before employee registered date then use register date
	 * ->get paid salary in current month
	 * ->get no of holiday in current month 
	 * ->get incentives given in current month
	 * ->get deduction amount in current month
	 * ->get total salary in current month 
	 * @return EmployeeViewModel list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetailsForView() {
		// TODO Auto-generated method stub
		String hql = "from EmployeeDetails "+
				"WHERE employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeViewModel> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		int srno=0;
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				continue;
			}
			
			double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
			double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
			srno++;
			list.add(new EmployeeViewModel(
					srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(), 
					employeeDetails.getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getContact().getEmailId(), 
					totalAmount,
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					incentives,
					(int)noOfHolidays, 
					employeeDetails.getEmployee().getDepartment().getName()
					,employeeDetails.isStatus()));
			
		}
		
		return list;
	}
	/**
	 * <pre>
	 * gatekeeper showing employee details which come under his areas
	 * @return EmployeeViewModel list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetailsForGkEmployeeView() {
		// TODO Auto-generated method stub
		String logName=(String)session.getAttribute("loginName");
		// TODO Auto-generated method stub
		String hql = "from EmployeeDetails where 1=1 "
					+" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		if(!logName.equals("Admin")){
				   hql+="  and status=false and employeeDetailsId in "
					+ "(select  DISTINCT employeeDetails.employeeDetailsId from EmployeeAreaList where area.areaId in "
					+ "(select area.areaId from EmployeeAreaList where employeeDetails.employee.employeeId="+getAppLoggedEmployeeId()+"))"					
				    + " and employee.employeeId!="+getAppLoggedEmployeeId();
		}
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeViewModel> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		int srno=0;
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();

			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				continue;
			}
			
			srno++;
			list.add(new EmployeeViewModel(
					srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(), 
					employeeDetails.getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getContact().getEmailId(), 
					0,
					0, 
					0, 
					0,
					0, 
					employeeDetails.getEmployee().getDepartment().getName()
					,employeeDetails.isStatus()));
			
		}
		
		return list;
	}
	/**
	 * <pre>
	 * get count of no of holidays taken find by range,startDate,endDate,employeeDetails 
	 * if startDate before employee registered date then use register date
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return no of holidays count
	 * </pre>
	 */
	private long getNoOfHolidays(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		
			long noOfHolidays=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}				
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}				
			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeHolidays where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else if (range.equals("CurrentMonth")) {
				try {cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}

				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Range")) {
				try {cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeHolidays> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();			
				Date to;

				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				long diff;
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					cStart.setTime(from);
					 cEnd.setTime(to);
					 diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
				}
				else
				{				
					noOfHolidays=noOfHolidays+1;
				}    
			}
			return noOfHolidays;
	}

	
	/**
	 * <pre>
	 * find paid salary amount by range,startDate,endDate,employeeDetails
	 * if startDate before employee registered date then use register date
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return paid salary
	 * </pre>
	 */
	private double getPaidSalary(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {

			double paidCurrentMonthSalary=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeSalary where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

				
			}
			else if (range.equals("CurrentMonth")) {
				try {cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Range")) {
				try {cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where status=false and (date(payingDate) >= '"+startDate+"' and date(payingDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where status=false and (date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				

			}
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeSalary> list=(List<EmployeeSalary>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeSalary> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeSalary employeeSalary=itr.next();
				paidCurrentMonthSalary=paidCurrentMonthSalary+employeeSalary.getPayingAmount();
			}
			return paidCurrentMonthSalary;
	}
	/**
	 * <pre>
	 * fetch single employee details 
	 * 	 if startDate before employee registered date then use register date
	 * 
	 * ->get paid salary in current month
	 * ->get no of holiday in current month 
	 * ->get incentives given in current month
	 * ->get deduction amount in current month
	 * ->get total salary in current month 
	 * ->get total given salary details list in current month 
	 * 
	 * @param employeeDetailsId
	 * @return EmployeeSalaryStatus List
	 * </pre>
	 */
	@Transactional
	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId) {

		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"' "+
					" and employee.company.companyId="+getSessionSelectedCompaniesIds();
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeSalaryStatus> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		int srno=0;
		while(itr.hasNext()){
			EmployeeDetails employeeDetails=itr.next();
			String areaList=getAreaList(employeeDetails.getEmployeeDetailsId());
			long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double basicMothlySalary=employeeDetails.getBasicSalary();
			double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );			
			double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
			/*double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;*/
			double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
			
			List<EmployeeSalaryModel> employeeSalaryList=getEmployeeSalaryList(employeeDetails);
			list.add(new EmployeeSalaryStatus(
					(long)srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(),
					employeeDetails.getName(), 
					employeeDetails.getEmployee().getDepartment().getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getAddress(), 
					areaList, 
					(int)noOfHolidays, 
					findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails), 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					basicMothlySalary,
					incentives,
					deduction,
					totalAmount,
					employeeDetails.getEmployeeDetailsAddedDatetime(),
					employeeDetails.isStatus(),
					employeeSalaryList));
		}
		
		
		return list;
	}
	/**
	 * <pre>
	 * current month salary given list
	 * if startDate before employee registered date then use register date
	 * @param employeeDetails
	 * @return EmployeeSalaryModel list
	 * </pre>
	 */
	private List<EmployeeSalaryModel> getEmployeeSalaryList(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
		Calendar cal = Calendar.getInstance();
		Calendar regDate = Calendar.getInstance();
		regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String hql;
		if(regDate.compareTo(cal)<0)
		{
			hql="from EmployeeSalary where status=false and date(payingDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		}
		else
		{
			hql="from EmployeeSalary where status=false and date(payingDate) >= '"+sdf.format(regDate)+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		}
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
		if(employeeSalaryList.isEmpty())
		{
			return null;
		}
		List<EmployeeSalaryModel> employeeSalaryModelList=new ArrayList<>();
		Iterator<EmployeeSalary> itr=employeeSalaryList.iterator();
		int srno=0;
		while(itr.hasNext())
		{
			srno++;
			EmployeeSalary employeeSalary=itr.next();
			
			String modeOfPayment;
			String bankDetail;
			String checkDate;
			
			if(employeeSalary.getChequeNumber()==null)
			{
				modeOfPayment="Cash";
				bankDetail="NA";
				checkDate="NA";
			}
			else
			{
				bankDetail=employeeSalary.getBankName()+"-"+employeeSalary.getChequeNumber();
				checkDate=sdf.format(employeeSalary.getChequeDate());
				modeOfPayment="Cheque";
			}
			employeeSalaryModelList.add(new EmployeeSalaryModel(srno, 
																employeeSalary.getEmployeeSalaryId(),
																employeeSalary.getPayingAmount(),
																/*employeeSalary.getIncentive(),*/
																employeeSalary.getPayingDate(), 
																modeOfPayment, 
																bankDetail,
																checkDate,
																employeeSalary.getComment()));
		}
		
		return employeeSalaryModelList;
	}

		
	/**
	 * <pre>
	 * get area list in string of given employeeDetailsId
	 * </pre>
	 * @param employeeDetailsId
	 * @return areas in string
	 */
	private String getAreaList(long employeeDetailsId) {
		
		String hql = "from EmployeeAreaList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeAreaList> employeeList = (List<EmployeeAreaList>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		String areaList="";
		Iterator<EmployeeAreaList> itr=employeeList.iterator();
		while(itr.hasNext()){
			EmployeeAreaList employeeAreaList=itr.next();
			areaList=areaList+employeeAreaList.getArea().getName()+",";
		}
		areaList=areaList.substring(0, areaList.length() - 1);
		
		return areaList;
	}
	/**
	 * <pre>
	 * fetch single employee details in range,startDate,endDate,employeeDetailsId
	 * 	 if startDate before employee registered date then use register date
	 * 
	 * ->get paid salary in range,startDate,endDate,employeeDetailsId
	 * ->get no of holiday in range,startDate,endDate,employeeDetailsId
	 * ->get incentives given in range,startDate,endDate,employeeDetailsId
	 * ->get deduction amount in range,startDate,endDate,employeeDetailsId
	 * ->get total salary in range,startDate,endDate,employeeDetailsId
	 * ->get total given salary details list in range,startDate,endDate,employeeDetailsId
	 * 
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetailsId
	 * @return EmployeeSalaryStatus List
	 * </pre>
	 */
	@Transactional
	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,
			String range, long employeeDetailsId){
		
		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";
		hql+=" and employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeSalaryStatus> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		long srno=0;
		while(itr.hasNext()){
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				continue;
			}
			
			String areaList=getAreaList(employeeDetails.getEmployeeDetailsId());
			long noOfHolidays=getNoOfHolidays(startDate, endDate, range, employeeDetails);
			double basicMothlySalary=employeeDetails.getBasicSalary();
			double deduction=getDeductionAmount(startDate, endDate, range, employeeDetails);
			double incentives=getIncentivesForWebApp(startDate, endDate, range, employeeDetails);
			double paidCurrentMonthSalary=getPaidSalary(startDate, endDate, range, employeeDetails);
			double totalAmount=findTotalSalaryTwoDates(startDate, endDate, range, employeeDetails)+incentives-deduction;//paidCurrentMonthSalary-deduction;
			/*double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;*/
			double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
			List<EmployeeSalaryModel> employeeSalaryList=tofilterRangeEmployeeSalaryModelStatusForWebApp(startDate, endDate, range, employeeDetails);
			list.add(new EmployeeSalaryStatus(
					(long)srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(),
					employeeDetails.getName(), 
					employeeDetails.getEmployee().getDepartment().getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getAddress(), 
					areaList, 
					(int)noOfHolidays, 
					findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails), 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					basicMothlySalary,
					incentives,
					deduction,
					totalAmount,
					employeeDetails.getEmployeeDetailsAddedDatetime(),
					employeeDetails.isStatus(),
					employeeSalaryList));
		}
		
		
		return list;
	}
	/**
	 * <pre>
	 * get total given salary details list in range,startDate,endDate,employeeDetailsId
	 * if startDate before employee registered date then use register date
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetailsId
	 * @return EmployeeSalaryModel list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeSalaryModel> tofilterRangeEmployeeSalaryModelStatusForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {

		
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				

			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeSalary where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

				
			}
			else if (range.equals("CurrentMonth")) {
				try {cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where status=false and date(payingDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where status=false and date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Range")) {
				try {cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where status=false and (date(payingDate) >= '"+startDate+"' and date(payingDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where status=false and (date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
			hql+=" order by payingDate desc";
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
			if(employeeSalaryList.isEmpty())
			{
				return null;
			}
			List<EmployeeSalaryModel> employeeSalaryModelList=new ArrayList<>();
			Iterator<EmployeeSalary> itr=employeeSalaryList.iterator();
			int srno=0;
			while(itr.hasNext())
			{
				srno++;
				EmployeeSalary employeeSalary=itr.next();
				
				String modeOfPayment;
				String bankDetail;
				String checkDate;
				
				if(employeeSalary.getPayType().equals(Constants.CASH_PAY_STATUS))
				{
					modeOfPayment="Cash";
					bankDetail="NA";
					checkDate="NA";
				}
				else if(employeeSalary.getPayType().equals(Constants.OTHER_PAY_STATUS)){
					bankDetail=employeeSalary.getPaymentMethod().getPaymentMethodName()+"-"+employeeSalary.getTransactionReferenceNumber();
					checkDate="NA";
					modeOfPayment=Constants.OTHER_PAY_STATUS;
				}else
				{
					bankDetail=employeeSalary.getBankName()+"-"+employeeSalary.getChequeNumber();
					checkDate=sdf.format(employeeSalary.getChequeDate());
					modeOfPayment="Cheque";
				}
				employeeSalaryModelList.add(new EmployeeSalaryModel(srno, 
																	employeeSalary.getEmployeeSalaryId(),
																	employeeSalary.getPayingAmount(),
																	/*employeeSalary.getIncentive(),*/
																	employeeSalary.getPayingDate(), 
																	modeOfPayment, 
																	bankDetail,
																	checkDate,
																	employeeSalary.getComment()));
			}
			
			return employeeSalaryModelList;
	}

	/*private double getUnPaidSalary(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		//double unpaidCurrentMonthSalary=0;
		String employeeDetailsId=employeeDetails.getEmployeeDetailsId();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "";
		Query query = null;
		Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
		Calendar regDate = Calendar.getInstance();
		regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
		long diff,days=0;
		if (range.equals("Last6Months")) {
			cal.add(Calendar.MONTH, -6);
			hql = "from EmployeeSalary where payingDate > '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);
			
			diff = cal.getTime().getTime() - new Date().getTime();
			days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;

		}
		else if (range.equals("Last1Year")) {
			cal.add(Calendar.MONTH, -12);
			hql = "from EmployeeSalary where payingDate > '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);
			diff = cal.getTime().getTime() - new Date().getTime();
			days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;

		}
		
		else if (range.equals("ViewAll")) {

			hql = "from EmployeeSalary where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);
			
			diff = employeeDetails.getEmployeeDetailsAddedDatetime().getTime() - new Date().getTime();
			days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
		}
		else if (range.equals("CurrentMonth")) {

			hql="from EmployeeSalary where payingDate >= '"+DatePicker.getCurrentMonthStartDate()+"' and payingDate <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);
			Calendar c = Calendar.getInstance();
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			
			Date enddate=c.getTime();
			c.set(Calendar.DAY_OF_MONTH, 1);
			Date startdate=c.getTime();
			diff = startdate.getTime() - enddate.getTime();
			days=30;//TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		}
		else if (range.equals("Range")) {

			hql="from EmployeeSalary where payingDate >= '"+startDate+"' and payingDate <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);
			
			try {
				diff = dateFormat.parse(endDate).getTime()-dateFormat.parse(startDate).getTime();
				days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
		
		List<EmployeeSalary> list=(List<EmployeeSalary>)query.list();
		if(list.isEmpty())
		{
			return 0;
		}
		Iterator<EmployeeSalary> itr=list.iterator();
		double totalamt=0;
		while(itr.hasNext())
		{
			EmployeeSalary employeeSalary=itr.next();
			totalamt=totalamt+employeeSalary.getPayingAmount();
		}
		double daySalary=employeeDetails.getBasicSalary()/30;
		
		return (daySalary*days);
	}*/
	/**
	 * <pre>
	 * find total salary given list between start date and end date 
	 * if startDate before employee registered date then use register date
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return total salary amount
	 */
	@Transactional
	public double findTotalSalaryTwoDates(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) 
	{
	
			double salary=0;
			Calendar cal = Calendar.getInstance();
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			Calendar endDateCheck=Calendar.getInstance();
			try {endDateCheck.setTime(sdf.parse(endDate));} catch (ParseException e) {e.printStackTrace();}					
			Calendar currentDateCheck=Calendar.getInstance();
			
			if (range.equals("Last6Months")) {
				
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
				else
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
				else
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
			}			
			else if (range.equals("ViewAll")) {
				salary=getTotalSalary(sdf.format(employeeDetails.getEmployeeDetailsAddedDatetime()), sdf.format(new Date()), range, employeeDetails);
			}
			else if (range.equals("CurrentMonth")) {
				//salary=getTotalSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				try{cal.setTime(sdf.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
				else
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
			}
			else if (range.equals("Range")) {				
				//salary=getTotalSalary(startDate, sdf.format(new Date()), range, employeeDetails);
				try{cal.setTime(sdf.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
				else
				{
					if(endDateCheck.compareTo(currentDateCheck)<0)
					{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(endDateCheck.getTime()), range, employeeDetails);
					}else{
						salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(currentDateCheck.getTime()), range, employeeDetails);
					}
				}
				
			}
			
			return salary;	
	}
	
	/*public static void main(String[] args) {
		Calendar last=Calendar.getInstance();
		//last.setTime(cStart.getTime());
		Calendar cStart = Calendar.getInstance();
		 last.set(Calendar.DAY_OF_MONTH, last.getActualMaximum(Calendar.DAY_OF_MONTH));
		 System.out.println(cStart.equals(last));
	}*/
	
	/*public long findNumberOfDaysInMonthByDate(String date){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date caldate=new Date();
		
		try{caldate = sdf.parse(date);} catch (ParseException e) {e.printStackTrace();}
		
		
		Calendar mycal = Calendar.getInstance();
		mycal.setTime(caldate);
		
		// Get the number of days in that month
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		return daysInMonth;
	}*/
	/**
	 * <pre>
	 * find total salary given list between start date and end date 
	 * if startDate before employee registered date then use register date
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return total salary amount
	 */
	public double getTotalSalary(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		
		
			double basicSalary=0;
			Calendar disableDate = Calendar.getInstance(); disableDate.setTime(employeeDetails.getEmployeeDetailsDisableDatetime());
			
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start=new Date();
			Date end=new Date();
		try{
			start = sdf.parse(startDate);
			end = sdf.parse(endDate);
			} catch (ParseException e) {e.printStackTrace();}
			Calendar cStart = Calendar.getInstance(); cStart.setTime(start);
			Calendar cEnd = Calendar.getInstance(); cEnd.setTime(end);
			Calendar last=Calendar.getInstance();
			long diff,days,maxDay;
			double perday,salary=0;
					while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
							last.setTime(cStart.getTime());
							 last.set(Calendar.DAY_OF_MONTH, last.getActualMaximum(Calendar.DAY_OF_MONTH));
							// maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 if(last.compareTo(cEnd)<0)
							 {	
								// diff = last.getTime().getTime()-cStart.getTime().getTime();
								 
								 Calendar tempDate=Calendar.getInstance();
								 tempDate.setTime(cStart.getTime());
								 
								 while (tempDate.before(last) || tempDate.equals(last))  
								 {
									 /*if employee disable then
									  * compare end date with disable date 
									  * if disable date before end date then use disable as end date  
									  */
									if(employeeDetails.isStatus())
									{
										if(tempDate.before(disableDate)  || tempDate.equals(disableDate))
										{
											String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
											//find monthly salary from given date 
											basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
											
											perday=basicSalary/30;
											salary=salary+perday;
										}
									}
									else
									{
										String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
										//find monthly salary from given date 
										basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
										perday=basicSalary/30;
										salary=salary+perday;
									}
									tempDate.add(Calendar.DAY_OF_MONTH, 1);
								}
								 
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								// diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 
								 Calendar tempDate=Calendar.getInstance();
								 tempDate.setTime(cStart.getTime());
								 
								 while (tempDate.before(cEnd) || tempDate.equals(cEnd))  
								 {
									 /*if employee disable then
									  * compare end date with disable date 
									  * if disable date before end date then use disable as end date  
									  */
									if(employeeDetails.isStatus())
									{
										if( tempDate.before(disableDate)  || tempDate.equals(disableDate) )
										{
											String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
											
											//find monthly salary from given date 
											basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
											
											perday=basicSalary/30;
											salary=salary+perday;
										}
									}
									else
									{
										String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
										//find monthly salary from given date 
										basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
										
										perday=basicSalary/30;
										salary=salary+perday;
									}
									tempDate.add(Calendar.DAY_OF_MONTH, 1);
								}
								 
								 cStart.setTime(cEnd.getTime());
							 }
							 //days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 
							 /*//perday=basicSalary/maxDay;
							 perday=basicSalary/30;
							 salary=salary+(perday*days);*/
							 
						
						
						
					    //add one day to date
					    cStart.add(Calendar.DAY_OF_MONTH, 1);
					   // System.out.println(cStart.getTime() +"-"+ cStart.get(Calendar.MONTH));
					    //do something...
					
				}
					
					return salary;
	}
	/**
	 * <pre>
	 * find incentive total from start date,end date, range, employeeDetailsId
	 * if startDate before registered date then take registered date as startDate 
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return incentive total
	 */
	@Transactional
	public double getIncentivesForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails){
		
		
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				 {	
					hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				 {	
					hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}		
			else if (range.equals("ViewAll")) {
				hql = "from EmployeeIncentives where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				
			}
			else if (range.equals("CurrentMonth")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				 {
					hql="from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql="from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			else if (range.equals("Range")) {
				try{cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				 {
					hql="from EmployeeIncentives where status=false and (date(incentiveGivenDate) >= '"+startDate+"' and date(incentiveGivenDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql="from EmployeeIncentives where status=false and (date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(incentiveGivenDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeIncentives> list=(List<EmployeeIncentives>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeIncentives> itr=list.iterator();
			double incentives=0;
			while(itr.hasNext())
			{
				EmployeeIncentives employeeIncentives=itr.next();
				incentives=incentives+employeeIncentives.getIncentiveAmount();
			}
			return incentives;
	}
	/**
	 * <pre>
	 * get deduction amount by startDate,endDate,range,employeeDetailsId
	 * if startDate before registered date then take registered date as startDate 
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return
	 */
	public double  getDeductionAmount(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		
			double deduction=0;
			Calendar cal = Calendar.getInstance();
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				 {
					deduction=getDeductionCriteriaAmount(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}			
			else if (range.equals("ViewAll")) {
				deduction=getDeductionCriteriaAmount(sdf.format(employeeDetails.getEmployeeDetailsAddedDatetime()), sdf.format(new Date()), range, employeeDetails);
			}
			else if (range.equals("CurrentMonth")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				}
			}
			else if (range.equals("Range")) {
				try{cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(startDate, endDate, range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), endDate, range, employeeDetails);
				}
			}
			
			return deduction;	
	}
	/**
	 * <pre>
	 * get deduction amount by unpaid holiday given
	 * if startDate before registered date then take registered date as startDate  
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return amount deducted for holiday given
	 */
	public double getDeductionCriteriaAmount(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		
			//double basicSalary=employeeDetails.getBasicSalary();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start=new Date();
			Date end=new Date();
			try {
				start = sdf.parse(startDate);
				end = sdf.parse(endDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Calendar cStart = Calendar.getInstance(); cStart.setTime(start);
			Calendar cEnd = Calendar.getInstance(); cEnd.setTime(end);
			Calendar last=Calendar.getInstance();
			long diff,days,maxDay;
			double perday,deductions=0;
					while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
							 List<String> holidayDateList=new ArrayList<>();
							 
							 last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 //no of unpaid holiday given list
								 holidayDateList=getNoOfUnPaidHolidays(sdf.format(cStart.getTime()), sdf.format(last.getTime()), range, employeeDetails);
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								//no of unpaid holiday given list
								 holidayDateList=getNoOfUnPaidHolidays(sdf.format(cStart.getTime()), sdf.format(cEnd.getTime()), range, employeeDetails);
								 cStart.setTime(cEnd.getTime());
							 }							 
							 
							//update todays salary status
							EmployeeBasicSalaryStatus employeeBasicSalary=fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(employeeDetails.getEmployeeDetailsId());
							employeeBasicSalary.setEndDate(new Date());
							updateEmployeeOldBasicSalary(employeeBasicSalary);
							 
							if(holidayDateList!=null)
							{
							  for (int i=0; i<holidayDateList.size(); i++) 
							  {
								//String hql="from EmployeeBasicSalaryStatus where date(startDate) <= '"+holidayDateList.get(i)+"' and date(endDate) >= '"+holidayDateList.get(i)+"' and employeeDetails.employeeDetailsId='"+employeeDetails.getEmployeeDetailsId()+"' order by employeeBasicSalaryStatusId desc";
																
								//find employee salary on given date
								double basicSalary=fetchEmployeeBasicSalaryStatusByDate(holidayDateList.get(i),employeeDetails.getEmployeeDetailsId());
								
								perday=basicSalary/30;
								deductions=deductions+perday;
							 }
							} 
							 
							// perday=basicSalary/30;
							 //deductions=deductions+(perday*days);
						
					    //add one day to date
					    cStart.add(Calendar.DAY_OF_MONTH, 1);
					   // System.out.println(cStart.getTime() +"-"+ cStart.get(Calendar.MONTH));
					    //do something...
					
					}
					
					return deductions;
		
		
	}
	/**
	 * <pre>
	 * find unpaid holiday date list by startDate,endDate,range,employeeDetails
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return
	 */
	private List<String> getNoOfUnPaidHolidays(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		
			long noOfHolidays=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null; 
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeHolidays where status=false and  paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";


			}
			else if (range.equals("CurrentMonth")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and paidHoliday=false and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}

			}
			else if (range.equals("Range")) {
				try{cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"') and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+endDate+"') and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds()+" order by employeeHolidaysId desc";
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
			if(list.isEmpty())
			{
				return null;
			}
			
			List<String> holidayDateList=new ArrayList<>();
			
			Iterator<EmployeeHolidays> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();
				Date to;
				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				long diff;
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					
					cStart.setTime(from);
					cEnd.setTime(to);
					/*diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;*/
					
					while (cStart.before(cEnd) || cStart.equals(cEnd)) {
						holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
						cStart.add(Calendar.DAY_OF_MONTH, 1);
					}
					
				}
				else
				{				
					//noOfHolidays=noOfHolidays+1;
					holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(from));
				}    
			}
			return holidayDateList;
	}
	/*@Transactional
	public void delete(long id) {
		EmployeeDetails EmployeeToDelete = new EmployeeDetails();
		EmployeeToDelete.setEmployeeDetailsId(id);
		sessionFactory.getCurrentSession().delete(EmployeeToDelete);
	}*/
	/**
	 * <pre>
	 * fetch employee holiday list by startDate,endDate,range,employeeDetailsId
	 * @param employeeDetailsId
	 * @param filter
	 * @param startDate
	 * @param endDate
	 * @return EmployeeHolidayModel
	 * </pre>
	 */
	@Transactional
	public EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(long employeeDetailsId,String filter,String startDate,String endDate) {
		//TODO Auto-generated method stub
		
		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'"+
					  " and employee.company.companyId="+getSessionSelectedCompaniesIds();
				
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeDetails> employeeHolidayslist=(List<EmployeeDetails>)query.list();
		
		if(employeeHolidayslist.isEmpty())
		{
			return null;
		}
		
		
		EmployeeDetails employeeDetails=employeeHolidayslist.get(0);
			
			long employeeDetailId=employeeDetails.getEmployeeDetailsId();
			String name=employeeDetails.getName();
			String departmentname=employeeDetails.getEmployee().getDepartment().getName();
			String mobileNumber=employeeDetails.getContact().getMobileNumber();
			long noOfHolidays=getNoOfHolidays(startDate, endDate, filter, employeeDetails);
			//fetch employee holiday details by startDate,endDate,range,employeeDetailsId
			//if startDate is before registered date then take registered date as start date
			List<EmployeeHolidayList> employeeHolidayList=tofilterRangeEmployeeHolidayListModelStatusForWebApp(startDate, endDate, filter, employeeDetails);;
	
		return new EmployeeHolidayModel(employeeDetailId,employeeDetails.getEmployeeDetailsGenId(), name, departmentname, mobileNumber,employeeDetails.isStatus(), noOfHolidays, employeeHolidayList);
	}
	/**
	 * <pre>
	 * fetch employee holiday details by startDate,endDate,range,employeeDetailsId
	 * if startDate is before registered date then take registered date as start date
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @param employeeDetails
	 * @return EmployeeHolidayList list
	 */
	@Transactional
	public List<EmployeeHolidayList> tofilterRangeEmployeeHolidayListModelStatusForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
	
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			
			else if (range.equals("ViewAll")) {
	
				hql = "from EmployeeHolidays where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
	
				
			}
			else if (range.equals("CurrentMonth")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+DatePicker.getCurrentMonthStartDate()+"'  and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}
			else if (range.equals("Range")) {
				try{cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"')  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and (date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}else if (range.equals("pickDate")) {
				hql="from EmployeeHolidays where status=false and date(givenHolidayDate) = '"+startDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}else if (range.equals("last7days")) {
				cal.add(Calendar.DAY_OF_MONTH, -7);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				
			}else if (range.equals("last1month")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getLastMonthFirstDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+DatePicker.getLastMonthFirstDate()+"'  and date(givenHolidayDate) <='"+DatePicker.getLastMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+DatePicker.getLastMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}else if (range.equals("last3months")) {
				try{cal.setTime(dateFormat.parse(DatePicker.getLast3MonthFirstDate()));} catch (ParseException e) {e.printStackTrace();}
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+DatePicker.getLast3MonthFirstDate()+"'  and date(givenHolidayDate) <='"+DatePicker.getLast3MonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where status=false and date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+DatePicker.getLast3MonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
			}
			
			hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
			query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeHolidays> employeeHolidayList=(List<EmployeeHolidays>)query.list();
			if(employeeHolidayList.isEmpty())
			{
				return null;
			}
			List<EmployeeHolidayList> employeeHolidayListList=new ArrayList<>();
			Iterator<EmployeeHolidays> itr=employeeHolidayList.iterator();
			long srno=0;
			while(itr.hasNext())
			{
				srno++;
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();
				Date to;
				long diff;
				long noOfHolidays=0;
				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					
					cStart.setTime(from);
					 cEnd.setTime(to);
					 diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
				}
				else
				{				
					noOfHolidays=noOfHolidays+1;
				}    
				double amountDeduct=getAmountDeductOnHoliday(employeeHolidays.getFromDate(),employeeHolidays.getToDate(),employeeDetailsId);
				String typeOfLeave="";
				if(employeeHolidays.isPaidHoliday())
				{
					typeOfLeave="Paid";
					
					//if paid holiday then amount not deduct
					amountDeduct=0;
				}
				else
				{
					typeOfLeave="UnPaid";					
				}
				
				employeeHolidayListList.add(new EmployeeHolidayList(srno, 
																	employeeHolidays.getEmployeeHolidaysId(),
																	noOfHolidays, 
																	typeOfLeave, 
																	amountDeduct, 
																	employeeHolidays.getGivenHolidayDate(),
																	employeeHolidays.getFromDate(),
																	employeeHolidays.getToDate(),
																	employeeHolidays.getReason()));
			}
			
			return employeeHolidayListList;
	
	}
	/**
	 * <pre>
	 * get amount deducted between two dates by fromDate,toDate,employeeDetailsId
	 * </pre>
	 * @param fromDate
	 * @param toDate
	 * @param employeeDtailsId
	 * @return amount deducted
	 */
	private double getAmountDeductOnHoliday(Date fromDate, Date toDate,long employeeDtailsId) {

		
		long diff;
		long noOfHolidays=0;
		double amountDeducted=0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cEnd = Calendar.getInstance();
		Calendar cStart = Calendar.getInstance();
		List<String> holidayDateList=new ArrayList<>();
		
		if(toDate!=null)
		{			
			 cStart.setTime(fromDate);
			 cEnd.setTime(toDate);
			diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
			//noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
			//amountDeducted=getAmountDeductByDates(sdf.format(fromDate), sdf.format(toDate),basicSalary);
			
			while (cStart.before(cEnd) || cStart.equals(cEnd)) {
				holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
				cStart.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		else
		{		
			cStart.setTime(fromDate);
			//noOfHolidays=noOfHolidays+1;
			//amountDeducted=getAmountDeductByDates(sdf.format(fromDate), sdf.format(fromDate),basicSalary);
			
				holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
		}    
		 
		double deductions=0;
		 for (int i=0; i<holidayDateList.size(); i++) 
		 {
			
			double basicSalary=fetchEmployeeBasicSalaryStatusByDate(holidayDateList.get(i),employeeDtailsId);
			
			double perday=basicSalary/30;
			deductions=deductions+perday;
		}
		
		return deductions;
	}

	/*
	//calculate no of days in month from given date 
	public static void main(String[] args) {
		int iYear = 2000;
		int iMonth = Calendar.JUNE; // 1 (months begin with 0)
		int iDay = 1;

		// Create a calendar object and set year and month
		Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);

		// Get the number of days in that month
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		System.out.println(daysInMonth);
	}*/
	/*private double getAmountDeductByDates(String fromDate, String toDate,double basicSalary) {


			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start=new Date();
			Date end=new Date();
			try {
				start = sdf.parse(fromDate);
				end = sdf.parse(toDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Calendar cStart = Calendar.getInstance(); cStart.setTime(start);
			Calendar cEnd = Calendar.getInstance(); cEnd.setTime(end);
			Calendar last=Calendar.getInstance();
			long diff,days,maxDay;
			double perday,deductions=0;
					while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
						
						if(cStart.get(Calendar.MONTH)==0)
						{
							 last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
							 
						}
						else if(cStart.get(Calendar.MONTH)==1)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==2)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==3)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==4)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==5)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==6)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==7)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==8)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==9)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==10)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						else if(cStart.get(Calendar.MONTH)==11)
						{
							last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 diff = last.getTime().getTime()-cStart.getTime().getTime();
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 cStart.setTime(cEnd.getTime());
							 }							 
							 days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 perday=basicSalary/30;
							 deductions=deductions+(perday*days);
						}
						
						
					    //add one day to date
					    cStart.add(Calendar.DAY_OF_MONTH, 1);
					   // System.out.println(cStart.getTime() +"-"+ cStart.get(Calendar.MONTH));
					    //do something...
					
				}
			
			
			return deductions;
	}*/
	/**
	 * <pre>
	 * fetch employee area details by employeeDetails and companyId
	 * @param employeeDetailsId
	 * @param companyId
	 * @return EmployeeAreaDetails
	 * </pre>
	 */
	@Transactional
	public EmployeeAreaDetails fetchEmployeeAreaDetails(long employeeDetailsId,long companyId) {

		String hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'"+
					" and employee.company.companyId="+companyId;
					
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		
		
		
		hql="from EmployeeAreaList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'"+
				" and employeeDetails.employee.company.companyId="+companyId;
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeAreaList> employeeAreaList=(List<EmployeeAreaList>)query.list();

		List<AreaModel> areaModelList=new ArrayList<>();
		for(EmployeeAreaList area : employeeAreaList){
				areaModelList.add(new AreaModel(area.getArea().getAreaId(), area.getArea().getName(), area.getArea().getPincode()));
		}	
		EmployeeAreaDetails employeeAreaDetails=new EmployeeAreaDetails(list.get(0).getName(),
				list.get(0).getEmployee().getDepartment().getName(),
				list.get(0).getContact().getMobileNumber(),
				list.get(0).getEmployeeDetailsGenId(),
				areaModelList);
		return employeeAreaDetails;
	}
	/**
	 * <pre>
	 * EmployeeHoliday save
	 * @param employeeHolidays
	 * </pre>
	 */
	@Transactional
	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(employeeHolidays);
	}
	/**
	 * <pre>
	 * check holiday given or not on between startDate and endDate
	 * @param startDate
	 * @param endDate
	 * @param employeeDetailsId
	 * @return if holiday taken then msg other wise empty msg
	 * </pre>
	 */
	@Transactional
	public String checkHolidayGivenOrNot(String startDate, String endDate,long employeeDetailsId) {
		String msg="";
		String hql="from EmployeeHolidays where  status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'"+
					" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		
		//date condition
		hql+=" and (((DATE(fromDate)<='"+startDate+"' and DATE(toDate)>='"+startDate+"') "
			 + " or (DATE(fromDate)<='"+endDate+"' and DATE(toDate)>='"+endDate+"' ))"+
				" or (DATE(fromDate)>'"+startDate+"' and DATE(toDate)<'"+endDate+"' ))";
				
		
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
		if(list.isEmpty()){
			return msg;
		}else{
			if(list.get(0).getFromDate().compareTo(list.get(0).getToDate()) == 0)
    		{
    			msg="Holiday already taken in "+dateFormat.format(list.get(0).getFromDate());
    		}
    		else
    		{
    			msg="Holiday already taken from "+dateFormat.format(list.get(0).getFromDate())+" to "+dateFormat.format(list.get(0).getToDate());
    		}
			return msg;
		}
		/*Iterator<EmployeeHolidays> itr=list.iterator();
		
		
		
		while(itr.hasNext())
		{
			EmployeeHolidays employeeHolidays=itr.next();
			
			Calendar cStart = Calendar.getInstance();
			Calendar cEnd = Calendar.getInstance();
			Calendar start = Calendar.getInstance();
			Calendar end = Calendar.getInstance();
			
			try {
				 start.setTime(dateFormat.parse(startDate));
				 end.setTime(dateFormat.parse(endDate));
			} catch (Exception e) {}
			
			
			if(employeeHolidays.getFromDate()!=null && employeeHolidays.getToDate()!=null)
			{
				
				cStart.setTime(employeeHolidays.getFromDate());
				cEnd.setTime(employeeHolidays.getToDate());
				
			    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
				{
			    	//System.out.println("cStart : "+cStart.getTime() +" - cEnd"+cEnd.getTime());
			    	
			    	while (start.before(end) || start.equals(end)) 
					{ 
			    		 //System.out.println("start : "+start.getTime() +" - end"+end.getTime());
				    	if(cStart.compareTo(start)==0)
						{
				    		if(employeeHolidays.getFromDate().compareTo(employeeHolidays.getToDate()) == 0)
				    		{
				    			msg="Holiday already taken in "+dateFormat.format(employeeHolidays.getFromDate());
				    		}
				    		else
				    		{
				    			msg="Holiday already taken from "+dateFormat.format(employeeHolidays.getFromDate())+" to "+dateFormat.format(employeeHolidays.getToDate());
				    		}
							
				    		return msg;
							 //break;
						}	
				    	start.add(Calendar.DAY_OF_MONTH, 1);
					}
			    	
			    	try {
						start.setTime(dateFormat.parse(startDate));
					} catch (Exception e) {}
			    	
					cStart.add(Calendar.DAY_OF_MONTH, 1);
				}
			}		
			
		}
		return msg;*/
	}
	/**
	 * <pre>
	 * check holiday given or not on between startDate and endDate except editing holiday details
	 * @param startDate
	 * @param endDate
	 * @param employeeDetailsId
	 * @param employeeHolidayId
	 * @return if holiday taken then msg other wise empty msg
	 * </pre>
	 */
	@Transactional
	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate,long employeeDetailsId,long employeeHolidayId) {
		String msg="";
		String hql="from EmployeeHolidays where  status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'"+
				" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds()+" and employeeHolidaysId<>"+employeeHolidayId;
		//date condition
		hql+=" and (((DATE(fromDate)<='"+startDate+"' and DATE(toDate)>='"+startDate+"') "
		   + " or (DATE(fromDate)<='"+endDate+"' and DATE(toDate)>='"+endDate+"' ))"+
					" or (DATE(fromDate)>'"+startDate+"' and DATE(toDate)<'"+endDate+"' ))";
		
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
		if(list.isEmpty()){
			return msg;
		}else{
			if(list.get(0).getFromDate().compareTo(list.get(0).getToDate()) == 0)
    		{
    			msg="Holiday already taken in "+dateFormat.format(list.get(0).getFromDate());
    		}
    		else
    		{
    			msg="Holiday already taken from "+dateFormat.format(list.get(0).getFromDate())+" to "+dateFormat.format(list.get(0).getToDate());
    		}
			return msg;
		}
				/*Iterator<EmployeeHolidays> itr=list.iterator();
				
				
				
				while(itr.hasNext())
				{
					EmployeeHolidays employeeHolidays=itr.next();
					
					Calendar cStart = Calendar.getInstance();
					Calendar cEnd = Calendar.getInstance();
					Calendar start = Calendar.getInstance();
					Calendar end = Calendar.getInstance();
					
					try {
						 start.setTime(dateFormat.parse(startDate));
						 end.setTime(dateFormat.parse(endDate));
					} catch (Exception e) {}
					
					
					if(employeeHolidays.getFromDate()!=null && employeeHolidays.getToDate()!=null)
					{
						
						cStart.setTime(employeeHolidays.getFromDate());
						cEnd.setTime(employeeHolidays.getToDate());
						
					    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
						{
					    	//System.out.println("cStart : "+cStart.getTime() +" - cEnd"+cEnd.getTime());
					    	
					    	while (start.before(end) || start.equals(end)) 
							{ 
					    		 //System.out.println("start : "+start.getTime() +" - end"+end.getTime());
						    	if(cStart.compareTo(start)==0)
								{
						    		if(employeeHolidays.getFromDate().compareTo(employeeHolidays.getToDate()) == 0)
						    		{
						    			msg="Holiday already taken in "+dateFormat.format(employeeHolidays.getFromDate());
						    		}
						    		else
						    		{
						    			msg="Holiday already taken from "+dateFormat.format(employeeHolidays.getFromDate())+" to "+dateFormat.format(employeeHolidays.getToDate());
						    		}
									
						    		return msg;
									 //break;
								}	
						    	start.add(Calendar.DAY_OF_MONTH, 1);
							}
					    	
					    	try {
								start.setTime(dateFormat.parse(startDate));
							} catch (Exception e) {}
					    	
							cStart.add(Calendar.DAY_OF_MONTH, 1);
						}
					}		
					
				}
				return msg;*/
	}

	
	@Transactional
	public void giveIncentives(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(employeeIncentives);
	}
	
	@Transactional
	public EmployeeIncentives fetchIncentives(long employeeIncentiveId) {
		String hql="from EmployeeIncentives where employeeIncentiveId="+employeeIncentiveId+
				" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeIncentives> employeeIncentiveList=(List<EmployeeIncentives>)query.list();
		return employeeIncentiveList.get(0);
	}
	/**
	 * <pre>
	 * save employee salary and 
	 * make ledger entry debit side and deduct same balance from company balance 
	 * @param employeeSalary
	 * </pre>
	 */
	@Transactional
	public void givePayment(EmployeeSalary employeeSalary)
	{
		sessionFactory.getCurrentSession().save(employeeSalary);
		
		EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeSalary.getEmployeeDetails().getEmployeeDetailsId());
		
		//ledger entry create
		String payMode="";
		if(employeeSalary.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(employeeSalary.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=employeeSalary.getBankName()+"-"+employeeSalary.getChequeNumber();
		}else{
			payMode=employeeSalary.getPaymentMethod().getPaymentMethodName()+"-"+employeeSalary.getTransactionReferenceNumber();
		}
		
		double debit=employeeSalary.getPayingAmount();
		Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
		
		String  hql="from EmployeeDetails where employeeDetailsId='"+employeeSalary.getEmployeeDetails().getEmployeeDetailsId()+"'"+
				" and employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		
		List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		Ledger ledger=new Ledger(
				    payMode, 
					employeeSalary.getPayingDate(), 
					null, 
					list.get(0).getEmployeeDetailsGenId()+" Salary",//employeeSalary.getComment(), 					
					employeeDetails.getName(), 
					debit, 
					0, 
					0, 
					employeeSalary,
					branch
				);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
	}
	/**
	 * <pre>
	 * update employee salary and 
	 * update ledger entry debit side and deduct same balance from company balance 
	 * @param employeeSalary
	 * </pre> 
	 */
	@Transactional
	public void updatePayment(EmployeeSalary employeeSalary)
	{
		employeeSalary=(EmployeeSalary)sessionFactory.getCurrentSession().merge(employeeSalary);
		sessionFactory.getCurrentSession().update(employeeSalary);
		
		String payMode="";
		if(employeeSalary.getPayType().equals(Constants.CASH_PAY_STATUS)){
			payMode="Cash";
		}else if(employeeSalary.getPayType().equals(Constants.CHEQUE_PAY_STATUS)){
			payMode=employeeSalary.getBankName()+"-"+employeeSalary.getChequeNumber();
		}else{
			payMode=employeeSalary.getPaymentMethod().getPaymentMethodName()+"-"+employeeSalary.getTransactionReferenceNumber();
		}
		
		double balance=0,debit=0,debitOld=0,credit=0;		
		Ledger ledger=null;
		if(employeeSalary.isStatus()==false){
			//ledger update
			ledger=ledgerDAO.fetchLedger("employee", String.valueOf(employeeSalary.getEmployeeSalaryId()));
			List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
			if(ledger!=null){
	
				if(ledgerListBefore==null){
					balance=0;
				}else{
					balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
				}
				debitOld=ledger.getDebit();
				debit=employeeSalary.getPayingAmount();			
				balance=balance-debit;
				
				ledger.setBalance(balance);
				ledger.setDebit(debit);
				ledger.setPayMode(payMode);
				ledgerDAO.updateLedger(ledger);
				
				ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
			}
			//ledger update done 
		}else{
			//delete ledger
			ledger=ledgerDAO.fetchLedger("employee", String.valueOf(employeeSalary.getEmployeeSalaryId()));
			ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
			sessionFactory.getCurrentSession().delete(ledger);	
			if(ledger!=null){
				balance=ledger.getBalance();
				debitOld=ledger.getDebit();
				balance=balance+debitOld;
				
				ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
			}
			//delete ledger end
		}
		
		
	}
	
	@Transactional
	public EmployeeSalary fetchEmployeeSalary(long employeeSalaryId){
		String hql="from EmployeeSalary where employeeSalaryId="+employeeSalaryId+
				" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
		return employeeSalaryList.get(0);				
	}
	/**
	 * --not used 
	 * <pre>
	 * open payment model by employeeDerailsId
	 * show following information by current month
	 * -> get deduction amount 
	 * -> get Incentives amount
	 * -> get paid salary 
	 * -> total salary to be paid
	 * 
	 * @param employeeDetailsId
	 * </pre>
	 */
	@Transactional
	public EmployeePaymentModel openPaymentModel(long employeeDetailsId){
		String  hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'"+
				" and employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		
		List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		EmployeePaymentModel employeePaymentModel=null;
		Iterator<EmployeeDetails> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			double deduction=getDeductionAmount(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );
			double incentives=getIncentivesForWebApp(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );			
			double paidCurrentMonthSalary=getPaidSalary(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );
			double totalAmount=findTotalSalaryTwoDates(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails )+incentives-deduction;
			//double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
			double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
			
			employeePaymentModel=new EmployeePaymentModel(employeeDetails.getName(),
					totalAmount, 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary);
			
		}
			return employeePaymentModel;	
	}
	/**
	 * <pre>
	 * send sms to given employeeIds
	 * if  employeeIds have one then send sms to given mobile number other wise send on registered mobile number
	 * @param employeeDetailsIdList
	 * @param smsText
	 * @param mobileNumber
	 * @return success/failed 
	 * </pre>
	 */
	@Transactional
	public String sendSMSTOEmployee(String employeeDetailsIdList,String smsText,String mobileNumber) {

		try {
			String endText="";
			Company company=(Company)session.getAttribute("companyDetails");
			if (company != null) {
				endText=" \n --"+company.getCompanyName();
			}
			
			EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
			if (employeeDetails != null) {
				endText=" \n --"+employeeDetails.getName()+"(GK)";
			}
			
			String[] employeeDetailsIdList2 = employeeDetailsIdList.split(",");
			
			//for changed mobile number
			if(employeeDetailsIdList2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+employeeDetailsIdList2[0]);
				return "Success";
			}
			
			for(int i=0; i<employeeDetailsIdList2.length; i++){
				employeeDetails=fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsIdList2[i]));
				SendSMS.sendSMS(Long.parseLong(employeeDetails.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+employeeDetailsIdList2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";			
		}
		
	}
	
	@Transactional
	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId) {
		String hql = "from EmployeeDetails where employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}
	/**
	 * <pre>
	 * clear token from where employee not match in EmployeeDetails table
	 * @param token
	 * @param employeeId
	 * </pre>
	 */
	@Transactional
	public void clearToken(String token,long employeeId) {
		String hql = "update EmployeeDetails b set b.token=null where employee.employeeId<>"+employeeId+" and token='"+token+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
	}
	/**
	 * <pre>
	 * fetch gatekeeper list by areaId
	 * @param areaId
	 * </pre>
	 */
	@Transactional
	public List<EmployeeAreaList> getGateKeeperEmployeeDetailsByareaId(long areaId) {
		
		String hql="from EmployeeAreaList where area.areaId="+areaId+" and employeeDetails.employee.department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
		hql+=" and employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeAreaList> employeeAreaList = (List<EmployeeAreaList>) query.list();
		if (employeeAreaList.isEmpty()) {
			return null;
		}
		return employeeAreaList;
	}
	/**
	 * <pre>
	 * fetch delivery boy list by areaId
	 * @param areaId
	 * </pre>
	 */
	@Transactional
    public List<EmployeeDetails> fetchDBEmployeeDetailByAreaId(long areaId) {
        String hql="";
        Query query;
        hql="from EmployeeDetails where employee.department.name='"+ Constants.DELIVERYBOY_DEPT_NAME +"'"+
        " and employeeDetailsId in (select emparea.employeeDetails.employeeDetailsId from EmployeeAreaList as emparea "+
		"where emparea.area.areaId in ("+areaId+")) "+
		" and employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
        /*" and employeeDetailsId in (select emparea.employeeDetails.employeeDetailsId from EmployeeAreaList as emparea "+
		"where emparea.area.areaId in ("+getSessionSelectedIds()+"))"+*/
        
        query=sessionFactory.getCurrentSession().createQuery(hql);
        
        List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
        Iterator<EmployeeDetails> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
        if(list.isEmpty())
        {
        	return null;
        }
                
        return list;
    }
	/**
	 * <pre>
	 * fetch salesman list by areaId
	 * @param areaId
	 * </pre>
	 */
	@Transactional
    public List<EmployeeDetails> fetchSMEmployeeDetailByAreaId(long areaId) {
        String hql="";
        Query query;
        
        hql="from EmployeeDetails where employee.department.name='"+ Constants.SALESMAN_DEPT_NAME +"'"+
		" and employeeDetailsId in (select emparea.employeeDetails.employeeDetailsId from EmployeeAreaList as emparea "+
		"where emparea.area.areaId in ("+areaId+"))"+
		" and employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
        /*" and employeeDetailsId in (select emparea.employeeDetails.employeeDetailsId from EmployeeAreaList as emparea "+
		"where emparea.area.areaId in ("+getSessionSelectedIds()+"))"+*/
        
        query=sessionFactory.getCurrentSession().createQuery(hql);
        
        List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
        Iterator<EmployeeDetails> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
        if(list.isEmpty())
        {
        	return null;
        }
        
        return list;
    }
	/**
	 * <pre>
	 * fetch area list by employee id
	 * @param employeeId
	 * </pre>
	 */
	@Transactional
    public List<Area> fetchAreaByEmployeeId(long employeeId) {

        String hql="";
        Query query;
        hql="from EmployeeAreaList where employeeDetails.employee.employeeId="+employeeId;
        query=sessionFactory.getCurrentSession().createQuery(hql);
        List<EmployeeAreaList> list1=(List<EmployeeAreaList>)query.list();
                
        List<Area> areasList=new ArrayList<>();
        for(EmployeeAreaList employeeAreaList : list1)
        {
            areasList.add(employeeAreaList.getArea());
        }        
        
        return areasList;
    
        
    }
	
	/**
	 * <pre>
	 * fetch area list by employee id
	 * @param employeeId
	 * @return AreaModel list
	 * </pre>
	 */
	@Transactional
    public List<AreaModel> fetchAreaModelListByEmployeeId(long employeeId) {
		
        List<Area> areaList=fetchAreaByEmployeeId(employeeId);
        if(areaList==null){
        	return null;
        }
        List<AreaModel> areaModelList=new ArrayList<>();
        for(Area area : areaList)
        {
            areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
        }        
        
        return areaModelList;    
    }
	/**
	 * <pre>
	 * fetch single employee details 
	 * 	 if startDate before employee registered date then use register date
	 * 
	 * ->get paid salary in current month
	 * ->get no of holiday in current month 
	 * ->get incentives given in current month
	 * ->get deduction amount in current month
	 * ->get total salary in current month 
	 * 
	 * @param employeeDetailsId
	 * @return EmployeeViewModel List
	 * </pre>
	 */
	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetail(long employeeDetailId) {
	
	
		String hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailId+"'"+
				" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();
		
		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeViewModel> employeeViewModelList=new ArrayList<>();
		
		EmployeeDetails employeeDetails=employeeList.get(0);
		
		double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		//double unpaidCurrentMonthSalary=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)-paidCurrentMonthSalary;
		long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
		/*double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;*/
		double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
		
		int srno=1;
		employeeViewModelList.add(new EmployeeViewModel(srno, 
				employeeDetails.getEmployeeDetailsId(), 
				employeeDetails.getEmployeeDetailsGenId(), 
				employeeDetails.getName(),
				employeeDetails.getContact().getMobileNumber(), 
				employeeDetails.getContact().getEmailId(), 
				totalAmount,
				paidCurrentMonthSalary, 
				unpaidCurrentMonthSalary, 
				incentives,
				(int)noOfHolidays, 
				employeeDetails.getEmployee().getDepartment().getName()
				,employeeDetails.isStatus()));
		
		return employeeViewModelList;
	
	}
	/**
	 * <pre>
	 * fetch delivery boy list by business name areaId
	 * @param areaId
	 * @return EmployeeDetails list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchDeliveryBoyListByBusinessNameAreaId(long businessNameAreaId) {
	
		String hql="";
		Query query;
		
		hql="select employeeDetails from EmployeeAreaList e where e.employeeDetails.employee.department.name='"+ Constants.DELIVERYBOY_DEPT_NAME +"'"+
		" and e.area.areaId="+businessNameAreaId+
		" and e.employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		 query=sessionFactory.getCurrentSession().createQuery(hql);
		 List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		 if(list.isEmpty())
		 {
			 return null;
		 }
		 List<EmployeeNameAndId> employeeNameAndIdList=new ArrayList<>();
		 for(EmployeeDetails employeeDetails : list){
			 employeeNameAndIdList.add(new EmployeeNameAndId(employeeDetails.getEmployee().getEmployeeId(), employeeDetails.getName()));
		 }
		return employeeNameAndIdList;
	}
	/**
	 * <pre>
	 * fetch salesman report according salesman order amount 
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return SalesManReport
	 * </pre>
	 */
	@Transactional
	public SalesManReport fetchSalesManReport(String range,String startDate,String endDate)
	{
		List<SalesManReportSub> salesManReportSubList=new ArrayList<>();
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		double totalAmountCollection=0;
		
		String hql="from EmployeeDetails where employee.department.name='"+Constants.SALESMAN_DEPT_NAME+"'"+
		" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+"))";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList=(List<EmployeeDetails>)query.list();
		Iterator<EmployeeDetails> itr=employeeDetailsList.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
		List<EmployeeNameAndId> employeeNameAndIdList=new ArrayList<>();
		long srno=1;
		for(EmployeeDetails employeeDetails:employeeDetailsList)
		{
			double smCollection=0,smCollectionWithTax=0;
			
			//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
			
			List<OrderReportList> orderReportList=orderDetailsDAO.showOrderReportByEmployeeSMId(String.valueOf(employeeDetails.getEmployee().getEmployeeId()), range, startDate, endDate);
			if(orderReportList!=null)
			{
				for(OrderReportList orderReport:orderReportList)
				{
					smCollection+=orderReport.getAmountWithoutTax();
					smCollectionWithTax+=orderReport.getAmountWithTax();
				}
			}	
				salesManReportSubList.add(new SalesManReportSub(
					srno, 
					employeeDetails.getEmployee().getEmployeeId(), 
					employeeDetails.getName(),
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(),
					Double.parseDouble(decimalFormat.format(smCollection)),
					Double.parseDouble(decimalFormat.format(smCollectionWithTax)),
					(orderReportList==null)? 0 : orderReportList.size()));
			srno++;
			totalAmountCollection+=smCollectionWithTax;
			employeeNameAndIdList.add(new EmployeeNameAndId(employeeDetails.getEmployeeDetailsId(), employeeDetails.getName()));
		}
	
		return new SalesManReport(salesManReportSubList, employeeNameAndIdList, Double.parseDouble(decimalFormat.format(totalAmountCollection)));
	}
	/**
	 * <pre>
	 * fetch salesman and delivery boy list by gateKeeperId
	 * @param gateKeeperId
	 * @return EmployeeNameAndId list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchSMandDBByGateKeeperId(long gateKeeperId)
	{
		/*String hql="from EmployeeDetails where employee.department.name in ('"+Constants.SALESMAN_DEPT_NAME+"','"+Constants.DELIVERYBOY_DEPT_NAME+"')";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList=(List<EmployeeDetails>)query.list();
		if(employeeDetailsList.isEmpty())
		{
			return null;
		}
		
		String employeeDetailsIds="";
		for(EmployeeDetails employeeDetails: employeeDetailsList)
		{
			employeeDetailsIds=employeeDetailsIds+"'"+employeeDetails.getEmployeeDetailsId()+"',";
		}
		employeeDetailsIds.substring(0, employeeDetailsIds.length() - 1);*/
		
		List<EmployeeNameAndId> employeeNameAndIds=new ArrayList<>();
		String hql="";
		Query query;
		
		List<Area> areaList=fetchAreaByEmployeeId(gateKeeperId);
		List<Long> areaListIdArray = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        areaListIdArray.add(area.getAreaId());
	    }
	    
		hql="Select a.employeeDetails from EmployeeAreaList a where a.area.areaId in (:ids) "+
		" and a.employeeDetails.employee.department.name in ('"+Constants.SALESMAN_DEPT_NAME+"','"+Constants.DELIVERYBOY_DEPT_NAME+"')"+
		" and a.employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", areaListIdArray);
		List<EmployeeDetails> employeeDetailsList2=(List<EmployeeDetails>)query.list();		
				
		Set<EmployeeDetails> set = new TreeSet<EmployeeDetails>(new Comparator<EmployeeDetails>() {
			@Transactional
			public int compare(EmployeeDetails o1, EmployeeDetails o2) {
				if(o1.getEmployeeDetailsId()==o2.getEmployeeDetailsId()){
	        		return 0;
	        	}
	        	return 1;
			}
		});
		set.addAll(employeeDetailsList2);
		
		employeeDetailsList2= new ArrayList(set);
		
		for(EmployeeDetails employeeDetails : employeeDetailsList2)
		{
			employeeNameAndIds.add(new EmployeeNameAndId(employeeDetails.getEmployee().getEmployeeId(), employeeDetails.getName()));
		}
		
		return employeeNameAndIds;
	}
	
	
	/**
	 * <pre>
	 * fetch salesman and delivery boy list by employeeId
	 * @param employeeId
	 * @return EmployeeNameAndId list
	 * </pre>
	 */
	@Transactional
    public List<EmployeeNameAndId> fetchDBByGateKeeperId(long employeeId)
    {
		List<EmployeeNameAndId> employeeNameAndIds=new ArrayList<>();
		String hql="";
		Query query;
		
		List<Area> areaList=fetchAreaByEmployeeId(employeeId);
		List<Long> pnjId = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
	    
		hql="Select a.employeeDetails from EmployeeAreaList a where a.area.areaId in (:ids) "+
		" and a.employeeDetails.employee.department.name in ('"+Constants.DELIVERYBOY_DEPT_NAME+"')"+
		" and a.employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", pnjId);
		List<EmployeeDetails> employeeDetailsList2=(List<EmployeeDetails>)query.list();		
		
		Set set = new TreeSet(new Comparator<EmployeeDetails>() {
			@Transactional
			public int compare(EmployeeDetails o1, EmployeeDetails o2) {
				if(o1.getEmployeeDetailsId()==o2.getEmployeeDetailsId()){
	        		return 0;
	        	}
	        	return 1;
			}
		});
		set.addAll(employeeDetailsList2);
		
		employeeDetailsList2= new ArrayList(set);
		
		for(EmployeeDetails employeeDetails : employeeDetailsList2)
		{
			employeeNameAndIds.add(new EmployeeNameAndId(employeeDetails.getEmployee().getEmployeeId(), employeeDetails.getName()));
		}
		
		return employeeNameAndIds;
	    
    	
    }
	/**
	 * <pre>
	 * fetch last employee salary status by employee Details Id
	 * @param employeeDetailsId
	 * @return EmployeeBasicSalaryStatus
	 * </pre>
	 */
	@Transactional
	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId)
	{
		String hql="from EmployeeBasicSalaryStatus where employeeDetails.employeeDetailsId='"+employeeDetailsId+"' "+
				" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds()+
				" order by employeeBasicSalaryStatusId desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeBasicSalaryStatus> employeeOldBasicSalaryList=(List<EmployeeBasicSalaryStatus>)query.list();
		if(employeeOldBasicSalaryList.isEmpty())
		{
			return null;
		}
		return employeeOldBasicSalaryList.get(0);
	}
	
	@Transactional
	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary)
	{
		sessionFactory.getCurrentSession().save(employeeOldBasicSalary);
	}
	
	@Transactional
	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary)
	{
		employeeOldBasicSalary=(EmployeeBasicSalaryStatus)sessionFactory.getCurrentSession().merge(employeeOldBasicSalary);
		sessionFactory.getCurrentSession().update(employeeOldBasicSalary);
	}
	/**
	 * <pre>
	 * fetch employee salary status by date and employeeDetailsId 
	 * @param date
	 * @param employeeDetailsId
	 * @return salary amount
	 * 
	 * </pre>
	 */
	@Transactional
	public double fetchEmployeeBasicSalaryStatusByDate(String date,long employeeDetailsId)
	{
		String hql="from EmployeeBasicSalaryStatus where (date(startDate) <= '"+date+"' and date(endDate) >= '"+date+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'"+
					" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds()+
					" order by employeeBasicSalaryStatusId desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeBasicSalaryStatus> employeeOldBasicSalaryList=(List<EmployeeBasicSalaryStatus>)query.list();
		
		if(employeeOldBasicSalaryList.isEmpty())
		{
			return fetchEmployeeDetailsForWebApp(employeeDetailsId).getBasicSalary();
		}
		
		return employeeOldBasicSalaryList.get(0).getBasicSalary();
	}
	/**
	 * <pre>
	 * fetch employee incentive list by range,startDate,endDate,endDate 
	 * @param employeeDetailsId
	 * @param filter
	 * @param startDate
	 * @param endDate
	 * @return EmployeeIncentives list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId,String filter,String startDate,String endDate)
	{
		
		
		employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "";
		Query query = null;
		Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
		Calendar regDate = Calendar.getInstance();
		regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
		if (filter.equals("Last6Months")) {
			cal.add(Calendar.MONTH, -6);
			if(regDate.compareTo(cal)<0)
			{
				hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			
		}
		else if (filter.equals("Last1Year")) {
			cal.add(Calendar.MONTH, -12);
			if(regDate.compareTo(cal)<0)
			{
				hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql = "from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			
		}
		
		else if (filter.equals("ViewAll")) {

			hql = "from EmployeeIncentives where status=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

		}
		else if (filter.equals("CurrentMonth")) {
			try{cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));} catch (ParseException e) {e.printStackTrace();}
			if(regDate.compareTo(cal)<0)
			{
				hql="from EmployeeIncentives where status=false and date(incentiveGivenDate) >= '"+DatePicker.getCurrentMonthStartDate()+"'  and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			
		}
		else if (filter.equals("Range")) {
			try{cal.setTime(dateFormat.parse(startDate));} catch (ParseException e) {e.printStackTrace();}
			if(regDate.compareTo(cal)<0)
			{
				hql="from EmployeeIncentives where status=false and (date(incentiveGivenDate) >= '"+startDate+"' and date(incentiveGivenDate) <='"+endDate+"')  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql="from EmployeeIncentives where status=false and (date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(incentiveGivenDate) <='"+endDate+"') and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
		}
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeIncentives> employeeIncentiveList=(List<EmployeeIncentives>)query.list();
 		return employeeIncentiveList;
		
	}
	
	@Transactional
	public String updateIncentive(EmployeeIncentives employeeIncentives)
	{
			employeeIncentives=(EmployeeIncentives)sessionFactory.getCurrentSession().merge(employeeIncentives);
			sessionFactory.getCurrentSession().update(employeeIncentives);
			return "Success";
		
	}
	
	@Transactional
	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId)
	{
		
		return (EmployeeHolidays)sessionFactory.getCurrentSession().get(EmployeeHolidays.class,employeeHolidayId);
		
	}
	
	@Transactional
	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId)
	{		
		return (EmployeeIncentives)sessionFactory.getCurrentSession().get(EmployeeIncentives.class,employeeIncentivesId);		
	}
	
	@Transactional
	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays)
	{		
			employeeHolidays=(EmployeeHolidays)sessionFactory.getCurrentSession().merge(employeeHolidays);
			sessionFactory.getCurrentSession().update(employeeHolidays);
			return "Success";	
	}
	/**
	 * <pre>
	 * fetch EmployeeDetails list by departmentId
	 * @param deparmentId
	 * </pre>
	 */
	@Transactional
	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId) {
		String hql = "from EmployeeDetails where employee.department.departmentId='" + deparmentId+"'"+
		" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+"))";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		Iterator<EmployeeDetails> itr=employeeDetailsList.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			//check branch 
			if(findBranchSameOrNot(employeeDetails.getEmployee().getEmployeeId())==false){
				itr.remove();
			}
		}
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList;
	}
	/**
	 * <pre>
	 * fetch employee last location in today by employeeDetailsId
	 * @param employeeDetailsId
	 * @return EmployeeLocation
	 * </pre>
	 */
	@Transactional
	public EmployeeLocation fetchEmployeeLastLocationByEmployeeDetailsId(long employeeDetailsId,String date)
	{
		String hql="from EmployeeLocation where employeeDetails.employeeDetailsId="+employeeDetailsId+" AND date(datetime)='"+date+"'"+
				" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds()+	
				" order by datetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeLocation> employeeLocationDetails=(List<EmployeeLocation>)query.list();
		if(employeeLocationDetails.isEmpty())
		{
			return null;
		}
		return employeeLocationDetails.get(0);
	}
	/**
	 * <pre>
	 * fetch employee last location list in today by employeeDetailsId
	 * @param employeeDetailsId
	 * @return EmployeeLocation list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeLocation> fetchEmployeeLastLocationListByEmployeeDetailsId(long employeeDetailsId,String date)
	{
		String hql="from EmployeeLocation where employeeDetails.employeeDetailsId="+employeeDetailsId+" AND date(datetime)='"+date+"'"+
					" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeLocation> employeeLocationDetails=(List<EmployeeLocation>)query.list();
		if(employeeLocationDetails.isEmpty())
		{
			return null;
		}
		return employeeLocationDetails;
	}
	
	/***
	 * <pre>
	 * fetch todays employee location list separated by route change by department Id 
	 * @param deparmentId
	 * @return EmployeeLastLocation list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeLastLocation> fetchEmployeeLastLocationByDepartmentId(long deparmentId,String date)
	{
		List<EmployeeLastLocation> employeeLastLocationList=new ArrayList<>();
		
		List<EmployeeDetails> empDetailsList=fetchEmployeeDetailsByDepartmentId(deparmentId);		
		if(empDetailsList!=null)
		{
			for(EmployeeDetails employeeDetails: empDetailsList)
			{
				List<EmployeeRouteListModel> employeeRouteListModelList=new ArrayList<>();		
				List<EmployeeRouteList> employeeRouteList=new ArrayList<>();
				List<EmployeeLocation> employeeLocationList=fetchEmployeeLastLocationListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId(),date);
				
				if(employeeLocationList!=null)
				{
					boolean inner=false;
					employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(0).getLatitude(),
																employeeLocationList.get(0).getLongitude(), 
																employeeLocationList.get(0).getAddress(),
																employeeLocationList.get(0).getDatetime(),
																employeeLocationList.get(0).getLocationInfo()));
					
					for(int i=0; i<employeeLocationList.size()-1; i++)
					{	
						if(employeeLocationList.get(i+1).isNewRoute())
						{
							//employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(i).getLatitude(), employeeLocationList.get(i).getLongitude(), employeeLocationList.get(i).getDatetime(),employeeLocationList.get(i).getLocationInfo()));
							employeeRouteListModelList.add(new EmployeeRouteListModel(employeeRouteList));
							employeeRouteList=new ArrayList<>();
							inner=true;
						}
						
						inner=false;
						employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(i+1).getLatitude(), 
																	employeeLocationList.get(i+1).getLongitude(),
																	employeeLocationList.get(i+1).getAddress(), 
																	employeeLocationList.get(i+1).getDatetime(),
																	employeeLocationList.get(i+1).getLocationInfo()));
					}					
					if(inner==false)
					{
						employeeRouteListModelList.add(new EmployeeRouteListModel(employeeRouteList));
					}
				}
				
				
				EmployeeLocation employeeLocation=fetchEmployeeLastLocationByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId(),date);
				if(employeeLocation!=null)
				{
					employeeLastLocationList.add(new EmployeeLastLocation(	new EmployeeDetailsModel(
																					employeeDetails.getEmployeeDetailsId(),
																					employeeDetails.getEmployeeDetailsGenId(), 
																					employeeDetails.getName(), 
																					employeeDetails.getAddress(),
																					employeeDetails.getBasicSalary(), 
																					employeeDetails.getContact(), 
																					employeeDetails.getEmployeeDetailsAddedDatetime(), 
																					employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
																					employeeDetails.isStatus(),
																					employeeDetails.getEmployeeDetailsDisableDatetime()), 
																			employeeLocation.getLatitude(), 
																			employeeLocation.getLongitude(),
																			employeeLocation.getAddress(),
																			employeeRouteListModelList	));
				}
				else
				{
					employeeLastLocationList.add(new EmployeeLastLocation(	new EmployeeDetailsModel(
																				employeeDetails.getEmployeeDetailsId(),
																				employeeDetails.getEmployeeDetailsGenId(), 
																				employeeDetails.getName(), 
																				employeeDetails.getAddress(),
																				employeeDetails.getBasicSalary(), 
																				employeeDetails.getContact(), 
																				employeeDetails.getEmployeeDetailsAddedDatetime(), 
																				employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
																				employeeDetails.isStatus(),
																				employeeDetails.getEmployeeDetailsDisableDatetime()),
																			"", "","",employeeRouteListModelList));
				}				
			}			
		}
		return employeeLastLocationList;
	}
	/**
	 * <pre>
	 * fetch employee last location details by employee details id
	 * 
	 * @param employeeDetailsId
	 * @return EmployeeLastLocation list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeLastLocation> fetchEmployeeLocationByEmployeeDetailsId(long employeeDetailsId,String date)
	{
		List<EmployeeLastLocation> employeeLastLocationList=new ArrayList<>();
		List<EmployeeRouteListModel> employeeRouteListModelList=new ArrayList<>();		
		List<EmployeeRouteList> employeeRouteList=new ArrayList<>();
		List<EmployeeLocation> employeeLocationList=fetchEmployeeLastLocationListByEmployeeDetailsId(employeeDetailsId,date);
		employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		if(employeeLocationList!=null)
		{
			boolean inner=false;
			employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(0).getLatitude(), 
														employeeLocationList.get(0).getLongitude(), 
														employeeLocationList.get(0).getAddress(), 
														employeeLocationList.get(0).getDatetime(),
														employeeLocationList.get(0).getLocationInfo()));
			
			for(int i=0; i<employeeLocationList.size()-1; i++)
			{	
				if(employeeLocationList.get(i+1).isNewRoute())
				{
					//employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(i).getLatitude(), employeeLocationList.get(i).getLongitude(), employeeLocationList.get(i).getDatetime(),employeeLocationList.get(i).getLocationInfo()));
					employeeRouteListModelList.add(new EmployeeRouteListModel(employeeRouteList));
					employeeRouteList=new ArrayList<>();
					inner=true;
				}
				
				inner=false;
				employeeRouteList.add(new EmployeeRouteList(employeeLocationList.get(i+1).getLatitude(), 
															employeeLocationList.get(i+1).getLongitude(), 
															employeeLocationList.get(i+1).getAddress(), 
															employeeLocationList.get(i+1).getDatetime(),
															employeeLocationList.get(i+1).getLocationInfo()));
			}
			
			if(inner==false)
			{
				employeeRouteListModelList.add(new EmployeeRouteListModel(employeeRouteList));
			}
		}
				
		EmployeeLocation employeeLocation=fetchEmployeeLastLocationByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId(),date);
		if(employeeLocation!=null)
		{
			employeeLastLocationList.add(new EmployeeLastLocation(new EmployeeDetailsModel(
																		employeeDetails.getEmployeeDetailsId(),
																		employeeDetails.getEmployeeDetailsGenId(), 
																		employeeDetails.getName(), 
																		employeeDetails.getAddress(),
																		employeeDetails.getBasicSalary(), 
																		employeeDetails.getContact(), 
																		employeeDetails.getEmployeeDetailsAddedDatetime(), 
																		employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
																		employeeDetails.isStatus(),
																		employeeDetails.getEmployeeDetailsDisableDatetime()), 
																employeeLocation.getLatitude(),
																employeeLocation.getLongitude(), 
																employeeLocation.getAddress(), 
																employeeRouteListModelList));
		}
		else
		{
			employeeLastLocationList.add(new EmployeeLastLocation(
																	new EmployeeDetailsModel(
																			employeeDetails.getEmployeeDetailsId(),
																			employeeDetails.getEmployeeDetailsGenId(), 
																			employeeDetails.getName(), 
																			employeeDetails.getAddress(),
																			employeeDetails.getBasicSalary(), 
																			employeeDetails.getContact(), 
																			employeeDetails.getEmployeeDetailsAddedDatetime(), 
																			employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
																			employeeDetails.isStatus(),
																			employeeDetails.getEmployeeDetailsDisableDatetime())
																	, "", "","",employeeRouteListModelList));
		}	
		return employeeLastLocationList;
	}



	
	@Transactional
	public void saveEmployeeLocation(EmployeeLocation employeeLocation)
	{
		sessionFactory.getCurrentSession().save(employeeLocation);
	}
	
	
	/*public com.bluesquare.rc.models.EmployeeLocation fetchEmployeeRouteListByEmployeeId(long employeeDetailsId)
	{
		employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		List<EmployeeRouteList> employeeRouteList=new ArrayList<>();
		List<EmployeeLocation> employeeLocationList=fetchEmployeeLastLocationListByEmployeeDetailsId(employeeDetailsId);
		
		if(employeeLocationList!=null)
		{
			for(EmployeeLocation  employeeLocation:employeeLocationList)
			{	
					employeeRouteList.add(new EmployeeRouteList(employeeLocation.getLatitude(), 
																employeeLocation.getLongitude(), 
																employeeLocation.getAddress(), 
																employeeLocation.getDatetime(),
																employeeLocation.getLocationInfo()));
			}
		}
		
		return new com.bluesquare.rc.models.EmployeeLocation(employeeDetails, employeeRouteList);
	}	*/
	/**
	 * <pre>
	 * fetch employee details list by areas which logged employee belongs
	 * @return EmployeeNameAndId list
	 * </pre>
	 */
	@Transactional
	public List<EmployeeNameAndId> fetchEmployeeListForChat() {

		
		List<Area> areaList=fetchAreaByEmployeeId(getAppLoggedEmployeeId());
		List<Long> areaListIdArray = new ArrayList<>();
	    for (Area area : areaList) {
	        areaListIdArray.add(area.getAreaId());
	    }
		
		String hql="Select a.employeeDetails from EmployeeAreaList a where a.area.areaId in (:ids) "+
				" and a.employeeDetails.employee.employeeId<>"+getAppLoggedEmployeeId()+
				" and a.employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", areaListIdArray);
		List<EmployeeDetails> employeeDetailsList=(List<EmployeeDetails>)query.list();
		if(employeeDetailsList.isEmpty()){
			return null;	
		}		
		
		Set<EmployeeDetails> set = new TreeSet<EmployeeDetails>(new Comparator<EmployeeDetails>() {
			@Transactional
			public int compare(EmployeeDetails o1, EmployeeDetails o2) {
				if(o1.getEmployeeDetailsId()==o2.getEmployeeDetailsId()){
	        		return 0;
	        	}
	        	return 1;
			}
		});
		set.addAll(employeeDetailsList);
		
		employeeDetailsList = new ArrayList<EmployeeDetails>(set);
		
		List<EmployeeNameAndId> employeeNameAndIds=new ArrayList<>();
		
		for(EmployeeDetails employeeDetails : employeeDetailsList){
			
			employeeNameAndIds.add(new EmployeeNameAndId(
														employeeDetails.getEmployee().getEmployeeId(), 
														employeeDetails.getName(),
														employeeDetails.getEmployee().getDepartment().getName(),
														employeeDetails.getEmployee().getDepartment().getShortNameForEmployeeId(),
														employeeDetails.getEmployee().getUserId(),
														employeeDetails.getUserCode()
														));
			
		}
		
		return employeeNameAndIds;
	}
	/**
	 * <pre>
	 * total employee salary given between startDate and EndDate
	 * @param startDate
	 * @param endDate
	 * @return salary amount
	 * </pre>
	 */
	@Transactional
	public double totalEmployeeeSalaryAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="from EmployeeSalary where (date(payingDate)>='"+startDate+"' and date(payingDate)<='"+endDate+"') and status=false and employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
		double totalAmountPaid=0;
		for(EmployeeSalary employeeSalary : employeeSalaryList){
			totalAmountPaid+=employeeSalary.getPayingAmount();
		}
		return totalAmountPaid;
	}
	/**
	 * <pre>
	 * fetch employee details with no of holidays and balance pay in current month for App HRM
	 * @param employeeId
	 * @return EmployeeHrmDetailsResponse
	 * </pre>
	 */
	@Transactional
	public EmployeeHrmDetailsResponse fetchEmployeeLeaveAndBalanceAmountForAPP(long employeeId) {

		EmployeeDetails employeeDetails=getEmployeeDetailsByemployeeId(employeeId);
		
		double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
		/*double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;*/
		double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);

		return new EmployeeHrmDetailsResponse(employeeDetails, noOfHolidays, unpaidCurrentMonthSalary);
	}
	/**
	 * <pre>
	 * get employee deductions,incentives,total amount,paid amount ,unpaid amount between given start and end date
	 * </pre> 
	 * @param fromDate
	 * @param endDate
	 * @return EmployeePaymentDetailsResponse
	 */
	@Transactional
	public EmployeePaymentDetailsResponse fetchEmployeeSalaryDetails(String fromDate,String endDate){
		
		String range="Range";
		EmployeeDetails employeeDetails=getEmployeeDetailsByemployeeId(getAppLoggedEmployeeId());
		double deduction=getDeductionAmount(fromDate, endDate, range, employeeDetails);
		double incentives=getIncentivesForWebApp(fromDate, endDate, range, employeeDetails);
		double paidCurrentMonthSalary=getPaidSalary(fromDate, endDate, range, employeeDetails);
		double totalAmount=findTotalSalaryTwoDates(fromDate, endDate, range, employeeDetails)+incentives-deduction;
		/*double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;*/
		double unpaidCurrentMonthSalary=MathUtils.round(totalAmount, 2)-MathUtils.round(paidCurrentMonthSalary, 2);
		
		return new EmployeePaymentDetailsResponse(  "NA", 
													"NA", 
													"NA", 
													employeeDetails.getBasicSalary(), 
													incentives, 
													deduction, 
													totalAmount, 
													paidCurrentMonthSalary, 
													unpaidCurrentMonthSalary);
	}
	/**
	 * <pre>
	 * fetch employee holiday details no of paid/unpaid/total holidays and list of holidays
	 * </pre>
	 * @param filter
	 * @param startDate
	 * @param endDate
	 * @return EmployeeHolidayDetailsResponse
	 */
	@Transactional
	public EmployeeHolidayDetailsResponse fetchEmployeeHolidayDetailsForApp(String filter,String startDate,String endDate) {

		long noOfHolidays=0;
		long noOfPaidHolidays=0;
		long noOfUnpaidHolidays=0;
		double deductions=0;
		
		EmployeeDetails employeeDetails=getEmployeeDetailsByemployeeId(getAppLoggedEmployeeId());
		List<EmployeeHolidayList> employeeHolidayList=tofilterRangeEmployeeHolidayListModelStatusForWebApp(startDate, endDate, filter, employeeDetails);;
		if(employeeHolidayList!=null){
			for(EmployeeHolidayList employeeHoliday: employeeHolidayList){
				noOfHolidays+=employeeHoliday.getNoOfHolidays();
				if(employeeHoliday.getTypeOfLeave().equals("Paid")){
					noOfPaidHolidays+=employeeHoliday.getNoOfHolidays();
				}else{
					noOfUnpaidHolidays+=employeeHoliday.getNoOfHolidays();
					deductions+=employeeHoliday.getAmountDeduct();
				}
			}
		}
		
		return new EmployeeHolidayDetailsResponse(noOfHolidays, noOfPaidHolidays, noOfUnpaidHolidays, deductions, employeeHolidayList);
	}

	
	/**
	 * <pre>
	 * fetch AreaList By CompanyId
	 * </pre>
	 * @param companyId
	 * @return AreaList
	 */
	@Transactional
	public List<Area> fetchAreaListByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		String hql="from Area where company.companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList=(List<Area>)query.list();
		if(areaList.isEmpty()){
		return null;	
		}
		
		return areaList;
	}
	
	
	/**
	 * <pre>
	 * get Admin Employee
	 * </pre>
	 * @return Employee
	 */
	@Transactional
	public Employee getAdminEmployee() {
		String hql="from Employee where department.name='"+Constants.ADMIN+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Employee> employee=(List<Employee>)query.list();
		if(employee.isEmpty()){
		return null;	
		}
		
		return employee.get(0);
	}
	
	/**
	 * <pre>
	 * fetch location ids by employeeDetails and companyId
	 * @param employeeDetailsId
	 * @param companyId
	 * @return EmployeeAreaDetails
	 * </pre>
	 */
	@Transactional
	public List<Object[]> fetchEmployeeLocationIdsDetails(long employeeDetailsId,long companyId) {

		String hql="select area.areaId,area.region.regionId,area.region.city.cityId,area.region.city.state.stateId,area.region.city.state.country.countryId from EmployeeAreaList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'"+
				" and employeeDetails.employee.company.companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Object[]> locationIdsList=(List<Object[]>)query.list();
		
		return locationIdsList;
	}
}
