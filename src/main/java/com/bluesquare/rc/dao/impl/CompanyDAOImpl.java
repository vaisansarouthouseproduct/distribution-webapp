package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.EmailSender;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.OTPGenerator;
import com.bluesquare.rc.utils.SendSMS;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
/**
 * <pre>
 * @author Sachin Pawar 23-05-2018 Code Documentation
 * provides Implementation for following methods of CompanyDAO
 * 1.saveCompany(Company company,String cityIdList)
 * 2.fetchCompnyCities(long companyId)
 * 3.updateCompany(Company company)
 * 4.updateCompany(Company company,String cityIdList)
 * 5.fetchCompanyByCompanyId(long companyId)
 * 6.fetchAllCompany()
 * 7.checkDuplication(String checkText,String type,long companyId)
 * 8.validateCompany(String username, String password)
 * 9.sendSMSTOCompanies(String companiesId,String smsText,String mobileNumber)
 * 10.sendOTPToCompanyUsingMailAndSMS(String emailIdAndMobileNumber,boolean isMobileNumber)
 * 11.fetchCompanyListByStateId(long stateId )
 * </pre>
 */
@Repository("companyDAO")

@Component
public class CompanyDAOImpl extends TokenHandler implements CompanyDAO{

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	EmailSender emailSender;
	/**
	 * <pre>
	 * save company and companyCities which cities assigned by admin 
	 * @param company
	 * @param cityIdList
	 * </pre>
	 */
	@Transactional
	public void saveCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub.
		contactDAO.save(company.getContact());
		sessionFactory.getCurrentSession().save(company);
		
		Branch branch=new Branch();
		branch.setName("Branch1");
		branch.setCompany(company);
		sessionFactory.getCurrentSession().save(branch);
		
		
		String[] cityIds=cityIdList.split(",");
		for(String cityId : cityIds){
			CompanyCities companyCities=new CompanyCities();
			City city=new City();
			city.setCityId(Long.parseLong(cityId));
			companyCities.setCity(city);
			companyCities.setCompany(company);
			sessionFactory.getCurrentSession().save(companyCities);
		}
		
	}

	@Transactional
	public List<CompanyCities> fetchCompnyCities(long companyId){
		String hql="from CompanyCities where company.companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CompanyCities> CompanyCities=(List<CompanyCities>)query.list();
		return CompanyCities;
	}
	
	@Transactional
	public void updateCompany(Company company){
		contactDAO.update(company.getContact());
		company=(Company)sessionFactory.getCurrentSession().merge(company);
		sessionFactory.getCurrentSession().update(company);
	}
	/**
	 * <pre>
	 * update company 
	 * delete previous assigned cities from companyCities
	 * save companyCities which cities assigned by admin 
	 * @param company
	 * @param cityIdList
	 * </pre>
	 */
	@Transactional
	public void updateCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub
		contactDAO.update(company.getContact());
		company=(Company)sessionFactory.getCurrentSession().merge(company);
		sessionFactory.getCurrentSession().update(company);
		
		List<CompanyCities> companyCityList=fetchCompnyCities(company.getCompanyId());
		for(CompanyCities companyCity: companyCityList){
			companyCity=(CompanyCities)sessionFactory.getCurrentSession().merge(companyCity);
			sessionFactory.getCurrentSession().delete(companyCity);
		}
		
		String[] cityIds=cityIdList.split(",");
		for(String cityId : cityIds){
			CompanyCities companyCities=new CompanyCities();
			City city=new City();
			city.setCityId(Long.parseLong(cityId));
			companyCities.setCity(city);
			companyCities.setCompany(company);
			sessionFactory.getCurrentSession().save(companyCities);
		}
	}
	@Transactional
	public Company fetchCompanyByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		String hql="from Company where companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);			
	}
	
	@Transactional
	public List<Company> fetchAllCompany() {
		String hql="from Company where companyId<>0";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList;		
	}
	/**
	 * <pre>
	 * check duplication at time of save/update company
	 * companyName,panNumber,gstNo,mobileNumber,userId,telephoneNumber,emailId
	 * @param checkText
	 * @param type
	 * @param companyId
	 * </pre>
	 */
	@Transactional
	public String checkDuplication(String checkText,String type,long companyId){
		
		String hql="";
		if(type.equals("companyName")){
			hql="from Company where companyName='"+checkText+"'";
		}else if(type.equals("panNumber")){
			hql="from Company where panNumber='"+checkText+"'";
		}else if(type.equals("gstNo")){
			hql="from Company where gstinno='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from Company where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("userId")){
			hql="from Company where userId='"+checkText+"'";
		}else if(type.equals("telephoneNumber")){
			hql="from Company where contact.telephoneNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from Company where contact.emailId='"+checkText+"'";
		}		
		if(companyId!=0){
			hql+=" and companyId<>"+companyId;
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();		
		
		if(type.equals("userId")){			
			hql="from EmployeeDetails where employee.userId='"+checkText+"' and employee.department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
			 query=sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeDetails> employeeList=(List<EmployeeDetails>)query.list();
			if(companyList.isEmpty() && employeeList.isEmpty()){
				return "Success";
			}else{
				return "Failed";
			}
		}
		
		if(companyList.isEmpty()){
			return "Success";
		}
		
		return "Failed";
		
	}
	
	/*@Transactional
	public Company checkCompanyName(String companyName) {
		String hql="from Company where companyName='"+companyName.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyNameForUpdate(String companyName,long companyId) {
		String hql="from Company where companyName='"+companyName.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}

	@Transactional
	public Company checkCompanyUserName(String userName) {
		String hql="from Company where userId='"+userName.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyUserNameForUpdate(String userName,long companyId) {
		String hql="from Company where userId='"+userName.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyGstIn(String gstinno) {
		String hql="from Company where gstinno='"+gstinno.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyGstInForUpdate(String gstinno,long companyId) {
		String hql="from Company where gstinno='"+gstinno.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}*/
	/**
	 * <pre>
	 * validate company login by username ,password using named query
	 * @param username
	 * @param password
	 * @return company
	 * </pre>
	 */
	@Transactional
	public Company validateCompany(String username, String password) {
		String hql = "from Company where userId= :user and password=:pass";// and department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("user", username);
		query.setParameter("pass", password);
		
		@SuppressWarnings("unchecked")
		List<Company> list = (List<Company>) query.list();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	/**
	 * <pre>
	 * send sms to given company ids
	 * if given company ids length is one then send sms on given number 
	 * otherwise send on register number  
	 * @param companiesId
	 * @param smsText
	 * @param mobileNumber
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String sendSMSTOCompanies(String companiesId,String smsText,String mobileNumber) {

		try {
			 Company companySession=(Company)session.getAttribute("companyDetails");
			 String endText=" \n --"+companySession.getCompanyName();
			
			String[] companiesId2 = companiesId.split(",");
			
			if(companiesId2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+companiesId2[0]);
				return "success";
			}
			
			for(int i=0; i<companiesId2.length; i++){
				Company company=fetchCompanyByCompanyId(Long.parseLong(companiesId2[i]));
				SendSMS.sendSMS(Long.parseLong(company.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+companiesId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	/**
	 * <pre>
	 * send OTP to register mobile number or mail
	 * @param emailIdAndMobileNumber
	 * @param isMobileNumber
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String sendOTPToCompanyUsingMailAndSMS(String emailIdAndMobileNumber,boolean isMobileNumber) {
			
			String otpNumber=OTPGenerator.generateOtp();
			if(isMobileNumber){
				SendSMS.sendSMS(Long.parseLong(emailIdAndMobileNumber), "Your Verification OTP is "+otpNumber);
			}else{
				String subject="OTP for Forget Password";
				String body="Your Verification OTP is "+otpNumber;
				String to=emailIdAndMobileNumber;
				boolean isHtmlMail=true;
				emailSender.sendEmail(subject, body, to, isHtmlMail);
			}
			System.out.println("Your Verification OTP is "+otpNumber);	
			
			return otpNumber;
	}

	@Transactional
	public List<Company> fetchCompanyListByStateId(long stateId) {
		// TODO Auto-generated method stub
		List<Company> companyList=new ArrayList<>();
		
		if(stateId!=0){
			String  hql="from CompanyCities where city.state.stateId="+stateId +" and company.companyId!=0";
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<CompanyCities> companyCityList=(List<CompanyCities>)query.list();
			if(companyCityList.isEmpty()){
				return null;
			}	
			for(CompanyCities c:companyCityList){
				companyList.add(c.getCompany());
			}
		}else{
			
			String hql="from Company where companyId!=0";
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			companyList=(List<Company>)query.list();
			
			if(companyList.isEmpty()){
				return null;
			}
		}
	
		return companyList;
	}
	
	
}
