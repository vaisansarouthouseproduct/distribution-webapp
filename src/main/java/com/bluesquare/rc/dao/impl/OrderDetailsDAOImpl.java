package com.bluesquare.rc.dao.impl;


import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.InventoryDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.OrderStatusDAO;
import com.bluesquare.rc.dao.PaymentDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.ReturnOrderDAO;
import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.CancelReason;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DeliveredProduct;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.OrderStatus;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.CancelOrderRequest;
import com.bluesquare.rc.models.CategoryWiseAmountForBill;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.GstBillReport;
import com.bluesquare.rc.models.OrderDetailsListOfBusiness;
import com.bluesquare.rc.models.OrderDetailsListOfBusinessSub;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.ProductListForBill;
import com.bluesquare.rc.models.PurchaseTable;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.models.SaleAndPurchaseTableMerge;
import com.bluesquare.rc.models.SaleTable;
import com.bluesquare.rc.models.SalesReportModel;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.ReIssueOrderDetailsModel;
import com.bluesquare.rc.responseEntities.ReIssueOrderProductDetailsReport;
import com.bluesquare.rc.responseEntities.ReturnFromDeliveryBoyModel;
import com.bluesquare.rc.rest.models.BookOrderFreeProductRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.FetchOrderDetailsModel;
import com.bluesquare.rc.rest.models.GkSnapProductResponse;
import com.bluesquare.rc.rest.models.OrderDetailByBusinessNameIdEmployeeIdRequest;
import com.bluesquare.rc.rest.models.OrderDetailsForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentList;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.rest.models.OrderRequest;
import com.bluesquare.rc.rest.models.PReturnOrderDetailsModel;
import com.bluesquare.rc.rest.models.PaymentListRequest;
import com.bluesquare.rc.rest.models.ReIssueDelivered;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.rest.models.ReturnPermanentModelRequest;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.CreditNoteInvoiceNumberGenerate;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.DateUtils;
import com.bluesquare.rc.utils.EmailSender;
import com.bluesquare.rc.utils.ImageConvertor;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.bluesquare.rc.utils.InvoiceNumberGenerate;
import com.bluesquare.rc.utils.Notification;
import com.bluesquare.rc.utils.NumberToWordsConverter;
import com.bluesquare.rc.utils.OrderIdGenerator;
import com.bluesquare.rc.utils.PermanentReturnOrderIdGenerator;
import com.bluesquare.rc.utils.ReturnFromDeliveryBoyGenerator;
import com.bluesquare.rc.utils.ReturnOrderIdGenerator;
import com.bluesquare.rc.utils.SendSMS;
/**
 * <pre>
 * @author Sachin Pawar 26-05-2018 Code Documentation
 * provides Implementation for following methods of OrderDetailsDAO
 * 1.fetchOrderStatus(String orderStatusName)
 * 2.bookOrder(OrderRequest orderRequest)
 * 3.freeProductAddInOrderProductDetails(BookOrderFreeProductRequest bookOrderFreeProductRequest)
 * 4.packedOrderToDeliverBoy(OrderIssueRequest orderIssueRequest)
 * 5.fetchOrderProductIssueDetailsByOrderId(String orderId)
 * 6.confirmPackedOrderFromDB(String orderId)
 * 7.cancelOrder(CancelOrderRequest cancelOrderRequest)
 * 8.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(long employeeId, String fromDate, String toDate,String range)
 * 9.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(long employeeId,long areaId, String fromDate, String toDate,String range)
 * 10.orderDeliveredAndReturn(ReturnOrderRequest returnOrderRequest,String appPath)
 * 11.OrderProductDetailsIdComparator -- comparator class
 * 12.updateBookOrder(OrderRequest orderRequest)
 * 13.findModeOfOrderProduct(String editOrderModeList,long productId,String type)
 * 14.fetchOrderDetailsByOrderId(String orderDetailsId)
 * 15.fetchOrderDetailsByOrderIdForApp(String orderDetailsId)
 * 16.updateOrderDetailsPaymentDays(OrderDetails orderDetails)
 * 17.fetchOrderListByAreaId(long employeeId)
 * 18.fetchOrderListByAreaId(long areaId,long employeeId)
 * 19.fetchPendingOrderListByAreaId(long areaId,long employeeId)
 * 20.fetchOrderDetailsTodaysListByAreaId(long areaId)
 * 21.fetchOrderDetailsPendingListByAreaId(long areaId)
 * 22.fetchPendingOrderListByAreaId(long employeeId)
 * 23.showOrderReport(String range,String startDate,String endDate)
 * 24.showOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate)
 * 25.showOrderReportByEmployeeSMId(String employeeSMId,String range,String startDate,String endDate)
 * 26.fetchOrderProductDetailByOrderId(String orderId)
 * 27.fetchOrderProductDetailByOrderIdForApp(String orderId)
 * 28.orderProductDetailsListForWebApp(String orderDetailsId)
 * 29.fetchSupplierOrders24hour(String range,String startDate,String endDate)
 * 30.fetchSupplierOrders24hourBySupplierId(String supplierId)
 * 31.fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId)
 * 32.fetchOrderListForPayment(PaymentListRequest paymentListRequest)
 * 33.fetchOrderDetailBybusinessNameIdAndDateRange(String businessNameId,String fromDate, String toDate)
 * 34.fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(long employeeId, String fromDate,String toDate, String range)
 * 35.fetchOrderDetailForCustomerReportByBusinessNameId(String businessNameId)
 * 36.fetchTotalCollectionByDateRangeAndEmployeeId(long employeeId,String fromDate, String toDate,  String range)
 * 37.fetchTodaysPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
 * 38.fetchPendingPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
 * 39.fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
 * 40.fetchPendingIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
 * 41.reIssueOrderDetails(OrderReIssueRequest orderReIssueRequest)
 * 42.fetchReIssueOrderProductDetailsList(String orderId)
 * 43.fetchOrderProductIssueListForIssueReportByOrderId(String orderId)
 * 44.fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(long employeeId,String fromDate, String toDate, String range, String orderStatus)
 * 45.fetchOrderDetailsByDateRangeAndEmpId(long employeeId, String fromDate, String toDate,String range)
 * 46.fetchReplacementIssuedOrdersByEmployeeId(long employeeId,String status)
 * 47.doneReIssueAndOrderStatusDelivered(ReIssueDelivered reIssueDelivered)
 * 48.fetchReIssueOrderDetailsByReIssueOrderDetailsId(long reIssueOrderId)
 * 49.fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(long reIssueOrderId)
 * 50.fetchOrderDetailsForReIssueDelivedByOrderId(long reIssueOrderId)
 * 51.fetchTopFiveSalesManByIssuedSale(String range,String startDate,String endDate)
 * 52.fetchOrderDetailForTotalCollectionReportByBusinessNameId(OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest)
 * 53.fetchTotalSaleForIndexPage(String range,String startDate,String endDate)
 * 54.fetchTotalAmountInvestInMarketForIndexPage(String range,String startDate,String endDate)
 * 55.fetchReIssueOrderDetails(String returnOrderProductId)
 * 56.fetchBillPrintData(String orderId,long companyId)
 * 57.fetchOrderDetailsListOfBusinessByBusinessNameId(String businessNameId,String fromDate, String toDate,  String range)
 * 58.sendSMSTOShopsUsingOrderId(String orderIds,String smsText,String mobileNumber)
 * 59.fetchOrderUsedProductForWebApp(long orderUsedproductId)
 * 60.fetchOrderUsedProductByProductId(long productId)
 * 61.deleteOrderUsedProduct(OrderUsedProduct orderUsedProduct)
 * 62.updateOrderUsedProductCurrentQuantity()
 * 63.fetchSalesReportModel(String startDate, String endDate,String range)
 * 64.cancelOrderReport(String range,String startDate,String endDate)
 * 65.fetchCancelOrderReportByEmployeeId(long employeeId, String fromDate, String toDate,String range)
 * 66.updateEditOrder(String updateOrderProductListId,String orderId)
 * 67.fetchReturnOrderFromDeliveryBoyReport(String range,String startDate,String endDate)
 * 68.fetchReturnFromDeliveryBoyMain(String returnFromDeliveryBoyMainId)
 * 69.fetchReturnFromDeliveryBoyList(String returnFromDeliveryBoyMainId)
 * 70.updateReturnFromDeliveryBoy(String orderId,String returnFromDeliveryBoyList)
 * 71.updateReturnFromDeliveryBoyForApp(List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList)
 * 72.fetchGstBillReport(String startDate,String endDate,String type)
 * 73.fetchSnapProductDetailsForGk()
 * 74.totalSaleAmountForProfitAndLoss(String startDate,String endDate)
 * 75.fetchLast3MonthOrderListByAreaIdForOfflineMode(long employeeId)
 * 76.getLast90DaysFirstDate()
 * 77.fetchCancelReason()
 * 78.fetchofflineApiHandlingByDbIdAndIMEINo(OfflineAPIRequest apiRequest)
 * 79.saveOfflineAPIrequest(OfflineAPIRequest apiRequest)
 * 80.updateTheStatusOfOfflineAPIRequest(long dbId,String imeiNo,String status)
 * 81.checkTransaportationUsed(long transportationId)
 * 82.fetchReturnOrderProductPByReturnOrderProductPkId(long returnOrderProductPkId)
 * 83.fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(long returnOrderProductPkId)
 * 84.savePermanentReturnOrder(ReturnPermanentModelRequest returnPermanentModelRequest)
 * </pre>
 */
@Repository("orderDetailsDAO")
@Component
public class OrderDetailsDAOImpl extends TokenHandler implements OrderDetailsDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	OrderStatusDAO orderStatusDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	OrderDetails orderDetails;
	@Autowired
	OrderProductDetails orderProductDetails;
	@Autowired
	BusinessName businessName;
	@Autowired
	Employee employee;
	@Autowired
	OrderStatus orderStatus;
	@Autowired
	Area area;
	@Autowired
	Product product;
	@Autowired
	OrderProductIssueDetails orderProductIssueDetails;
	@Autowired
	DeliveredProduct  deliveredProduct;
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	@Autowired
	BusinessNameDAO businessNameDAO;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	PaymentDAO paymentDAO;
	
	@Autowired
	EmployeeDAO employeeDAO;
	
	@Autowired
	ReturnOrderDAO returnOrderDAO;
	
	@Autowired
	InventoryDAO inventoryDAO;
	
	@Autowired
	InvoiceNumberGenerate invoiceNumberGenerate;
	
	@Autowired
	OrderIdGenerator orderIdGenerator;
	
	@Autowired
	ReturnOrderIdGenerator returnOrderIdGenerator;
	
	@Autowired
	AreaDAO areaDAO;
	
	//SimpleDateFormat simpleDavteFormat=new SimpleDateFormat("yyyy/MM/dd");
	
	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Autowired
	ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator;
	
	@Autowired
	PermanentReturnOrderIdGenerator permanentReturnOrderIdGenerator;
	
	@Autowired
	ImageConvertor imageConvertor;
	
	@Autowired
	TransportationDAO transportationDAO;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	CreditNoteInvoiceNumberGenerate creditNoteInvoiceNumberGenerate;
	/**
	 * <pre>
	 * fetch orderStatus details by orderStatusName
	 * @param orderStatusName
	 * </pre>
	 */
	@Transactional
	public OrderStatus fetchOrderStatus(String orderStatusName)
	{
		String hql="from OrderStatus where status LIKE '"+orderStatusName+"%'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderStatus> list=(List<OrderStatus>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}
	/**
	 * <pre>
	 * fetch business details by businessNameId for set in order
	 * set order status 'Booked'
	 * save order details
	 * save order product details
	 * send notification to gatekeeper
	 * @param orderRequest
	 * @return orderId
	 * </pre>
	 */
	@Transactional
	public String bookOrder(OrderRequest orderRequest) {
			
			orderDetails=orderRequest.getOrderDetails();
			
			businessName=businessNameDAO.fetchBusinessForWebApp(orderDetails.getBusinessName().getBusinessNameId());
			orderDetails.setBusinessName(businessName);
			
			orderDetails.setOrderId(orderIdGenerator.generate());
			
			orderDetails.setOrderDetailsAddedDatetime(new Date());
			orderDetails.setPaymentPeriodDays(0);
			
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_BOOKED);
			
			orderDetails.setOrderStatus(orderStatus);
			orderDetails.setPayStatus(false);
						
			sessionFactory.getCurrentSession().save(orderDetails);
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
						
			for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()){
				orderProductDetails.setOrderDetails(orderDetails);	
				Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProductId());
				
				Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
				Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
				
				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				orderUsedBrand.setCompany(company);
				orderUsedBrand.setBranch(branch);
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(),
						company,
						branch);
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						/*product.getProductImage(),
						product.getProductContentType(),*/
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity(),
						company,
						branch);
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				orderProductDetails.setProduct(orderUsedProduct);
				orderProductDetails.setType(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE);

				
				
				sessionFactory.getCurrentSession().save(orderProductDetails);
			}
						
			//BusinessNameDAOImpl businessNameDAO=new BusinessNameDAOImpl(sessionFactory);
			businessName=businessNameDAO.fetchBusinessForWebApp(orderDetails.getBusinessName().getBusinessNameId());
			
			////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId());	
			String salesPersonId=employeeDetails.getEmployeeDetailsGenId();
			//fetch employee details list by businessName area Id
			List<EmployeeAreaList> employeeAreaList=employeeDetailsDAO.getGateKeeperEmployeeDetailsByareaId(businessName.getArea().getAreaId());
			
			try {
				if(employeeAreaList!=null){
					for(EmployeeAreaList employeeArea: employeeAreaList){
						Notification.sendNotificationToGateKeeperForOrder(employeeArea.getEmployeeDetails().getToken(), 
																		  salesPersonId,
																		  businessName.getShopName(), 
																		  orderDetails.getOrderId());	
					}						
				}
				
			} catch (Exception e) {
				System.err.println("sendNotificationToGateKeeperForOrder error : "+e.toString());
			}
			
			return orderDetails.getOrderId();
	}
	/**
	 * <pre>
	 * save free order product against given orderId 
	 * @param bookOrderFreeProductRequest
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String freeProductAddInOrderProductDetails(BookOrderFreeProductRequest bookOrderFreeProductRequest)
	{
		orderDetails=fetchOrderDetailsByOrderId(bookOrderFreeProductRequest.getOrderId());
		long freeQuantity=0;
		//ProductDAOImpl  productDAO=new ProductDAOImpl(sessionFactory);
		for(OrderProductDetails orderProductDetails : bookOrderFreeProductRequest.getOrderProductDetailList()){
			orderProductDetails.setOrderDetails(orderDetails);	
			Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProductId());
			
			Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			orderUsedBrand.setCompany(company);
			orderUsedBrand.setBranch(branch);
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst(),
					company,
					branch);
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					/*product.getProductImage(),
					product.getProductContentType(),*/
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity(),
					company,
					branch);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			orderProductDetails.setProduct(orderUsedProduct);
			orderProductDetails.setType(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE);
			
			freeQuantity+=orderProductDetails.getPurchaseQuantity();
			
			//update free Quantity product and orderUsedProduct 
			product.setFreeQuantity(product.getFreeQuantity()+orderProductDetails.getPurchaseQuantity());
			
			productDAO.Update(product);
			updateOrderUsedProductCurrentQuantity();
			
			sessionFactory.getCurrentSession().save(orderProductDetails);
		}
		orderDetails.setTotalQuantity(orderDetails.getTotalQuantity()+freeQuantity);
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
		
		return "Success";
	}
	/**
	 * <pre>
	 * set transportation details if given
	 * update issued total amount,amountwithtax,quantity in orderDetails
	 * set delivery,packed date
	 * update issued amount,amountwithtax,quantity in order products
	 * cut order product quantity from current quantity of product
	 * send notification to delivery regarding order packed 
	 * @param orderIssueRequest
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
    public String packedOrderToDeliverBoy(OrderIssueRequest orderIssueRequest) {
		
		try {
			
			orderDetails=fetchOrderDetailsByOrderId(orderIssueRequest.getOrderDetails().getOrderId());
			
			if(orderIssueRequest.getOrderDetails().getTransportation()!=null){
				Transportation transportation=transportationDAO.fetchById(orderIssueRequest.getOrderDetails().getTransportation().getId()+"");
				orderDetails.setTransportation(transportation);
				orderDetails.setDocketNo(orderIssueRequest.getOrderDetails().getDocketNo());
				orderDetails.setVehicleNo(orderIssueRequest.getOrderDetails().getVehicleNo());
			}
			//orderDetails=orderIssueRequest.getOrderDetails();
			orderDetails.setIssuedTotalAmount(orderIssueRequest.getOrderDetails().getIssuedTotalAmount());
			orderDetails.setIssuedTotalAmountWithTax(orderIssueRequest.getOrderDetails().getIssuedTotalAmountWithTax());
			orderDetails.setIssuedTotalQuantity(orderIssueRequest.getOrderDetails().getIssuedTotalQuantity());
			
			/*if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PACKED))
			{
				orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED_PACKED);
			}
			else
			{*/
				orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_PACKED);
			//}
			orderDetails.setOrderStatus(orderStatus);
			orderDetails.setDeliveryDate(new SimpleDateFormat("yyyy/MM/dd").parse(orderIssueRequest.getDeliveryDate()));
			orderDetails.setPackedDate(new Date());
			orderDetails.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());
			
			orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
			sessionFactory.getCurrentSession().update(orderDetails);
			
			List<OrderProductDetails>  orderProductDetailList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			for(OrderProductDetails orderProductDetails:orderProductDetailList)
			{
				for(OrderProductDetails orderProductDetails2:orderIssueRequest.getOrderProductDetailsList())
				{
					if(orderProductDetails.getProduct().getProduct().getProductId()==orderProductDetails2.getProduct().getProductId() 
						&& orderProductDetails2.getType().equals(orderProductDetails.getType())	)
					{
						Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						
						orderProductDetails.setIssuedQuantity(orderProductDetails2.getIssuedQuantity());
						orderProductDetails.setIssueAmount(orderProductDetails2.getIssueAmount());
						
						orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
						sessionFactory.getCurrentSession().update(orderProductDetails);
						
						product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetails.getIssuedQuantity());
						
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), false);
						//here update order product current quantity
						updateOrderUsedProductCurrentQuantity();
					}
				}
			}
			
			orderProductIssueDetails.setOrderDetails(orderDetails);
			Employee employeeDB=new Employee();
			employeeDB.setEmployeeId(orderIssueRequest.getEmployeeIdDB());
			orderProductIssueDetails.setEmployeeDB(employeeDB);
			Employee employeeGK=new Employee();
			employeeGK.setEmployeeId(orderIssueRequest.getEmployeeIdGk());
			orderProductIssueDetails.setEmployeeGK(employeeGK);
			sessionFactory.getCurrentSession().save(orderProductIssueDetails);
			
			////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			EmployeeDetails employeeDetailsDB=employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getEmployeeDB().getEmployeeId());
			EmployeeDetails employeeDetailsGK=employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getEmployeeGK().getEmployeeId());

			Notification.sendNotificationToDeliveryBoyForOrder(employeeDetailsDB.getToken(),employeeDetailsGK.getEmployeeDetailsGenId() , orderDetails.getBusinessName().getShopName(), orderDetails.getOrderId(),"OPEN_ORDER_DETAILS_DB");
			
			//here update order product current quantity
			
			
			
			return "Success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}
	}
	/**
	 * <pre>
	 * fetch order product issue details by orderId
	 * @param orderId
	 * @return OrderProductIssueDetails
	 * </pre>
	 */
	@Transactional
	public OrderProductIssueDetails fetchOrderProductIssueDetailsByOrderId(String orderId)
	{
		String hql="from OrderProductIssueDetails where orderDetails.orderId='"+orderId+"'"+
					" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductIssueDetails> list=(List<OrderProductIssueDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}
	/**
	 * <pre>
	 * delivery boy receive order from gatekeeper 
	 * order status change to Issued 
	 * @param orderId
	 * @return success/failed
	 * </pre>
	 */
	@Transactional
	public String confirmPackedOrderFromDB(String orderId){
		
		orderDetails=fetchOrderDetailsByOrderId(orderId);
		/*if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_ISSUED))
		{
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED_ISSUED);
		}
		else
		{*/
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_ISSUED);
		//}		
		orderDetails.setOrderStatus(orderStatus);
		orderDetails.setIssueDate(new Date());
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
				
	  /*orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(orderId);
		sessionFactory.getCurrentSession().update(orderProductIssueDetails);*/		
		
		return "";
	}
	/**
	 * <pre>
	 * cancel order by order id from Gatekeeper, Deliver boy, SalesMan
	 * @param cancelOrderRequest
	 * @return success/msg
	 * </pre>
	 */
	@Transactional
	public String cancelOrder(CancelOrderRequest cancelOrderRequest) {
		// TODO Auto-generated method stub
				
		String hql="";
		Query query;
	
		// fetch order product details by orderId
		List<OrderProductDetails>  orderProductDetailList=fetchOrderProductDetailByOrderId(cancelOrderRequest.getOrderId());
		
		//get cancel order status details
		orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_CANCELED);
		
		//fetch order details by order id
		OrderDetails orderDetails=fetchOrderDetailsByOrderId(cancelOrderRequest.getOrderId());
		
		//if order is delivered or delivery pending then not allow to cancel
		if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED) || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
		{
			System.out.println("Order Can not be cancelled");
			return "This Order Is Delivered";
		}
		else 
		{
			//if salesman request to cancel order 
			//only allowed on booked status
			
			//EmployeeDAOImpl employeeDAO=new EmployeeDAOImpl(sessionFactory);
			EmployeeDetails employeeDetails=employeeDAO.getEmployeeDetails(cancelOrderRequest.getEmployeeId());
			Employee employee=employeeDetails.getEmployee();
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			if(employee.getDepartment().getName().equals(Constants.SALESMAN_DEPT_NAME))
			{
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(employee);
					orderDetails.setCancelReason(cancelOrderRequest.getReason());
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetails.getIssuedQuantity());
						
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), true);
						//here update order product current quantity
						//updateOrderUsedProductCurrentQuantity();
						
						//deleteOrderUsedProduct(orderProductDetails.getProduct());
					}	
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
				{
					return "Order is already cancelled";
				}
				else
				{
					return "Order is "+orderDetails.getOrderStatus().getStatus();
				}
			}
			//if gatekeeper request to cancel order 
			//only allowed on packed status
			//cancel order and order product quantity return added to current quantity 
			else if(employee.getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
			{
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(employee);
					orderDetails.setCancelReason(cancelOrderRequest.getReason());
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetails.getIssuedQuantity());
						
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), true);						
						//deleteOrderUsedProduct(orderProductDetails.getProduct());
					}	
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
				{
					return "Order is already cancelled";
				}
				else
				{
					return "Order is "+orderDetails.getOrderStatus().getStatus()+ " , So You can not Cancel the Order.";
				}
			}
			//if delivery boy request to cancel order 
			//only allowed on issued status
			//cancel order and order product details added to returnFromDelivery for define Damage/non-damage
			else
			{
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
				{
					orderDetails.setOrderStatus(orderStatus);
					orderDetails.setCancelDate(new Date());
					orderDetails.setEmployeeIdCancel(employee);
					orderDetails.setCancelReason(cancelOrderRequest.getReason());
					
					orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
					sessionFactory.getCurrentSession().update(orderDetails);
					
					List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
					
					for(OrderProductDetails orderProductDetails:orderProductDetailList)
					{
						
						ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
								orderProductDetails.getProduct(), 
								orderProductDetails.getSellingRate(), 
								orderProductDetails.getIssuedQuantity(), 
								orderProductDetails.getIssuedQuantity(),
								0,
								0,
								0, 
								orderProductDetails.getType());
						returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
						
						/*Product product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProduct().getProductId());
						product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetails.getIssuedQuantity());
						productDAO.Update(product);
						
						//here update order product current quantity
						updateOrderUsedProductCurrentQuantity();*/
					}
					
					long totalReturnQuantity=0;
					long totalIssuedQuantity=0;
					long totalDeliveryQuantity=0;
					long totalDamageQuantity=0;
					long totalNonDamageQuantity=0;

					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
						
						totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
						totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
						totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
						totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
						totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
					}
					orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(cancelOrderRequest.getOrderId());
					
					Company company=new Company();
					company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					
					Branch branch=new Branch();
					branch.setBranchId(getSessionSelectedBranchIds());
					
					ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
							totalReturnQuantity, 
							totalIssuedQuantity, 
							totalDeliveryQuantity, 
							totalDamageQuantity, 
							totalNonDamageQuantity,  
							orderProductIssueDetails.getEmployeeGK(), 
							orderProductIssueDetails.getEmployeeDB(), 
							orderDetails, 
							new Date(), 
							false,company,branch);
					//ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator=new ReturnFromDeliveryBoyGenerator(sessionFactory);
					returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
					sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
					
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
						returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
						sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
					}					
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
				{
					return "Order is already cancelled";
				}
				else
				{
					return "Order is "+orderDetails.getOrderStatus().getStatus();
				}
			}
			
			//here update order product current quantity
			updateOrderUsedProductCurrentQuantity();
			
			return "Success";					
		}		
	}
	/**
	 * <pre>
	 * fetch issued orders details by employeeId,fromDate,toDate,range 
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * @return OrderProductIssueReportResponse
	 * </pre>
	 */
	@Transactional
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(long employeeId, String fromDate, String toDate,String range) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query;
		OrderProductIssueReportResponse orderProductIssueReportResponse=new OrderProductIssueReportResponse();
		
		List<AreaModel> areaModelLists=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);		
		
		
		String areaListArray="";
		List<AreaModel> areaList=new ArrayList<>();
		for(AreaModel areaModel: areaModelLists)
		{
			areaList.add(areaModel);
			areaListArray+=areaModel.getAreaId()+",";
		}
		areaListArray=areaListArray.substring(0, areaListArray.length()-1);
		
		orderProductIssueReportResponse.setAreaList(areaList);
		//query.setParameterList("ids", longList);
		
		 
		Calendar cal = Calendar.getInstance(); 
		
		if(range.equals("range"))
		{
			hql = "from OrderProductIssueDetails where (date(orderDetails.packedDate)>='"+ fromDate + "' And date(orderDetails.packedDate)<='" + toDate + "') And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";	
		}
		if(range.equals("pickDate"))
		{
			hql = "from OrderProductIssueDetails where (date(orderDetails.packedDate)='"+ fromDate + "') And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";	
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderProductIssueDetails where date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
		}
		else if(range.equals("currentMonth")){
			hql="from OrderProductIssueDetails where (date(orderDetails.packedDate)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetails.packedDate)<='"+DatePicker.getCurrentMonthLastDate()+"')  And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
	
		}					
		else if(range.equals("lastMonth")){
			hql="from OrderProductIssueDetails where (date(orderDetails.packedDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetails.packedDate)<='"+DatePicker.getLastMonthLastDate()+"')  And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
	
		}else if(range.equals("last3Months")){
			hql="from OrderProductIssueDetails where (date(orderDetails.packedDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetails.packedDate)<='"+DatePicker.getLast3MonthLastDate()+"') And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderProductIssueDetails where orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
		}
		else if(range.equals("today"))
		{
			hql="from OrderProductIssueDetails where date(orderDetails.packedDate)=date(CURRENT_DATE()) And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderProductIssueDetails where date(orderDetails.packedDate)='"+ dateFormat.format(cal.getTime()) + "' And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
		}
		
		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
			 " and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "+
			 "and orderDetails.businessName.area.areaId in ("+areaListArray+")   order by orderDetails.packedDate desc";
		
		//hql+=" and orderDetails.businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetails.packedDate desc";
	
		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductIssueDetails> orderProductIssueDetailsList = (List<OrderProductIssueDetails>) query.list();

		if (orderProductIssueDetailsList.isEmpty()) {
			return orderProductIssueReportResponse;
		}
		orderProductIssueReportResponse.setOrderProductIssueDetailsList(orderProductIssueDetailsList);
		return orderProductIssueReportResponse;
	}
	/**
	 * <pre>
	 * fetch issued orders details by employeeId,fromDate,toDate,range ,areaId
	 * if areaId not zero then fetch order by business areaId otherwise all 
	 * @param employeeId
	 * @param areaId
	 * @param fromDate  
	 * @param toDate
	 * @param range
	 * @return OrderProductIssueReportResponse
	 * </pre>
	 */
	@Transactional
	public OrderProductIssueReportResponse fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(long employeeId,
			long areaId, String fromDate, String toDate,String range) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query;
		OrderProductIssueReportResponse orderProductIssueReportResponse=new OrderProductIssueReportResponse();
		
		List<AreaModel> areaModelLists=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);		
		
		
		String areaListArray="";
		List<AreaModel> areaList=new ArrayList<>();
		for(AreaModel areaModel: areaModelLists)
		{
			areaList.add(areaModel);
			areaListArray+=areaModel.getAreaId()+",";
		}
		areaListArray=areaListArray.substring(0, areaListArray.length()-1);
		
		
		orderProductIssueReportResponse.setAreaList(areaList);
		//query.setParameterList("ids", longList);
		
		 
		Calendar cal = Calendar.getInstance(); 
		//OrderDetails orderDetails=new OrderDetails();
		if(areaId==0)
		{
					if(range.equals("range"))
					{
						hql = "from OrderProductIssueDetails where (date(orderDetails.packedDate)>='"+ fromDate + "' And date(orderDetails.packedDate)<='" + toDate + "') And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";	
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderProductIssueDetails where date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderProductIssueDetails where date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"'  And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
				
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderProductIssueDetails where date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}else if(range.equals("pickDate")){
						hql="from OrderProductIssueDetails where date(orderDetails.packedDate)='"+fromDate +"'  And orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderProductIssueDetails where orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}
					else if(range.equals("currentDate"))
					{
						hql="from OrderProductIssueDetails where date(orderDetails.packedDate)=date(CURRENT_DATE()) And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}
					
		}
		else{
					if(range.equals("range"))
					{
						hql = "from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +" and (date(orderDetails.packedDate)>='"+ fromDate + "' And date(orderDetails.packedDate)<='" + toDate + "') And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";	
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +" And date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"'And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +" And date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"'And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
				
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +" And date(orderDetails.packedDate)>='"+dateFormat.format(cal.getTime())+"'And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}else if(range.equals("pickDate")){
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +"  And date(orderDetails.packedDate)='"+fromDate +"'And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId+"And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}
					else if(range.equals("currentDate"))
					{
						hql="from OrderProductIssueDetails where orderDetails.businessName.area.areaId="+ areaId +" And date(orderDetails.packedDate)=date(CURRENT_DATE()) And  orderDetails.orderStatus.status!='"+Constants.ORDER_STATUS_BOOKED+"'";
					}					
		}
	
		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
			 " and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "+
			 " and orderDetails.businessName.area.areaId in ("+areaListArray+")  order by orderDetails.packedDate desc";
		//hql+=" and orderDetails.businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetails.packedDate desc";
		
		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductIssueDetails> orderProductIssueDetailsList = (List<OrderProductIssueDetails>) query.list();

		if (orderProductIssueDetailsList.isEmpty()) {
			return orderProductIssueReportResponse;
		}
		List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
		for(OrderProductIssueDetails orderProductIssueDetails : orderProductIssueDetailsList){
			FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
			fetchOrderDetailsModel.setOrderId(orderProductIssueDetails.getOrderDetails().getOrderId());
			fetchOrderDetailsModel.setAreaName(orderProductIssueDetails.getOrderDetails().getBusinessName().getArea().getName());
			fetchOrderDetailsModel.setOrderDate(orderProductIssueDetails.getOrderDetails().getOrderDetailsAddedDatetime());
			fetchOrderDetailsModel.setPurchasedQuantity(orderProductIssueDetails.getOrderDetails().getTotalQuantity());
			fetchOrderDetailsModel.setShopName(orderProductIssueDetails.getOrderDetails().getBusinessName().getShopName());
			fetchOrderDetailsModel.setOrderStatus(orderProductIssueDetails.getOrderDetails().getOrderStatus().getStatus());
			fetchOrderDetailsModel.setAreaId(orderProductIssueDetails.getOrderDetails().getBusinessName().getArea().getAreaId());
			fetchOrderDetailsModel.setBusinessNameId(orderProductIssueDetails.getOrderDetails().getBusinessName().getBusinessNameId());
			fetchOrderDetailsModel.setOwnerName(orderProductIssueDetails.getOrderDetails().getBusinessName().getOwnerName());
			fetchOrderDetailsModel.setIssuedQuantity(orderProductIssueDetails.getOrderDetails().getIssuedTotalQuantity());
			fetchOrderDetailsModel.setPackedDate(orderProductIssueDetails.getOrderDetails().getPackedDate());
			fetchOrderDetailsModel.setIssuedDate(orderProductIssueDetails.getOrderDetails().getIssueDate());
			fetchOrderDetailsModel.setTotalAmount(orderProductIssueDetails.getOrderDetails().getTotalAmount());
			fetchOrderDetailsModel.setTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getTotalAmountWithTax());
			fetchOrderDetailsModel.setIssuedTotalAmount(orderProductIssueDetails.getOrderDetails().getIssuedTotalAmount());
			fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getIssuedTotalAmountWithTax());
			fetchOrderDetailsModel.setConfirmTotalAmount(orderProductIssueDetails.getOrderDetails().getConfirmTotalAmount());
			fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderProductIssueDetails.getOrderDetails().getConfirmTotalAmountWithTax());
			fetchOrderDetailsModel.setCancelEmployeeDepartment((orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel()!=null)?orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel().getDepartment().getName():null);
			fetchOrderDetailsModel.setCancelDate((orderProductIssueDetails.getOrderDetails().getEmployeeIdCancel()!=null)?orderProductIssueDetails.getOrderDetails().getCancelDate():null);
			fetchOrderDetailsModel.setConfirmDate(orderProductIssueDetails.getOrderDetails().getConfirmDate());
			fetchOrderDetailsModel.setMobileNo(orderProductIssueDetails.getOrderDetails().getBusinessName().getContact().getMobileNumber());
			fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
		}
		orderProductIssueReportResponse.setFetchOrderDetailsModelList(fetchOrderDetailsModelList);
		return orderProductIssueReportResponse;
	}
	/**
	 * <pre>
	 * order delivered and return some product quantity
	 * set confirm details(amount,amountwithtax,quantity) order details as well as order product details  
	 * if return order product come then save returnOrder and returnOrderProduct list
	 * order status change to Delivered 
	 * delivered product details save
	 * bill send to business name emailId
	 * @param returnOrderRequest
	 * @param appPath
	 * @return nothing
	 * </pre>
	 */
	@Transactional
	public String orderDeliveredAndReturn(ReturnOrderRequest returnOrderRequest,String appPath){
		Calendar calendar=Calendar.getInstance();
		
		orderDetails=returnOrderRequest.getOrderDetails();
		
		OrderDetails orderDetailsOld=fetchOrderDetailsByOrderId(orderDetails.getOrderId());
		orderDetailsOld.setConfirmDate(new Date());
		orderDetailsOld.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
		orderDetailsOld.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
		orderDetailsOld.setConfirmTotalQuantity(orderDetails.getConfirmTotalQuantity());		
		calendar.add(Calendar.DATE, (int)orderDetailsOld.getPaymentPeriodDays());
		
		
		orderDetailsOld.setOrderDetailsPaymentTakeDatetime(calendar.getTime());	
		
		if(returnOrderRequest.getReturnOrderProductList()!=null)
		{
			returnOrderRequest.getReturnOrderProduct().setOrderDetails(orderDetailsOld);
			returnOrderRequest.getReturnOrderProduct().setReturnOrderProductId(returnOrderIdGenerator.generate());
			returnOrderRequest.getReturnOrderProduct().setReturnOrderProductDatetime(new Date());
			returnOrderRequest.getReturnOrderProduct().setReIssueStatus(Constants.RETURN_ORDER_PENDING);
			
			Employee employee=new Employee();
			employee.setEmployeeId(getAppLoggedEmployeeId());
			returnOrderRequest.getReturnOrderProduct().setEmployee(employee);
			
			sessionFactory.getCurrentSession().save(returnOrderRequest.getReturnOrderProduct());
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			for(ReturnOrderProductDetails returnOrderProductDetails :returnOrderRequest.getReturnOrderProductList())
			{				
				returnOrderProductDetails.setReturnOrderProduct(returnOrderRequest.getReturnOrderProduct());		
				/*OrderUsedProduct orderUsedProduct=fetchOrderUsedProductForWebApp(returnOrderProductDetails.getProduct().getProductId());
				Product product=productDAO.fetchProductForWebApp(orderUsedProduct.getProduct().getProductId());
				product.setCurrentQuantity(product.getCurrentQuantity()+returnOrderProductDetails.getReturnQuantity());		
				
				productDAO.Update(product);
				
				//here update order product current quantity
				updateOrderUsedProductCurrentQuantity();*/
				
				sessionFactory.getCurrentSession().save(returnOrderProductDetails);
			}				
		}
		
		
		List<OrderProductDetails>  orderProductDetailList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
		Collections.sort(orderProductDetailList, new OrderProductDetailsIdComparator());
		
		List<OrderProductDetails>  orderProductDetailList2=returnOrderRequest.getOrderProductDetailsList();
		Collections.sort(orderProductDetailList2, new OrderProductDetailsIdComparator());
		
		int j=0;
		for(int i=0; i<orderProductDetailList.size(); i++)
		{
			OrderProductDetails orderProductDetailsOld=orderProductDetailList.get(i);
			OrderProductDetails orderProductDetailsNew=orderProductDetailList2.get(i);
			
			orderProductDetailsOld.setConfirmAmount(orderProductDetailsNew.getConfirmAmount());
			orderProductDetailsOld.setConfirmQuantity(orderProductDetailsNew.getConfirmQuantity());
			
			orderProductDetailsOld=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetailsOld);
			sessionFactory.getCurrentSession().update(orderProductDetailsOld);
			
			if(orderProductDetailsOld.getIssuedQuantity()>orderProductDetailsNew.getConfirmQuantity())
			{
				j++;
			}
		}
		
		if(j>0)
		{
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED_PENDING);	
		}
		else
		{
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED);
		}
		
		orderDetailsOld.setOrderStatus(orderStatus);
		
		orderDetailsOld=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetailsOld);
		sessionFactory.getCurrentSession().update(orderDetailsOld);
		
		deliveredProduct.setOrderDetails(orderDetailsOld);
		//ImageConvertor imageConvertor=new ImageConvertor(sessionFactory);
		deliveredProduct.setOrderReceiverSignature(imageConvertor.convertStringToBlob(returnOrderRequest.getSignatureBase64()));
		deliveredProduct.setStatus(Constants.ORDER_STATUS_DELIVERED);
		sessionFactory.getCurrentSession().save(deliveredProduct);
		
		try {
			//send mail process
			BillPrintDataModel billPrintDataModel=fetchBillPrintData(orderDetailsOld.getOrderId(),Long.parseLong(getSessionSelectedCompaniesIds()),getSessionSelectedBranchIds());
			String filePath="/resources/pdfFiles/invoie.pdf";
			
			/*ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/");*/
			System.out.println("appPath = " + appPath);

			// construct the complete absolute path of the file
			String fullPath = appPath + filePath;      
			//File downloadFile = new File(fullPath);
			
			File dFile=null;
			try {
				dFile = InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String fileName=orderDetailsOld.getInvoiceNumber()+".pdf";
			
			EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
			emailSender.sendEmail("Order Invoice", " ", orderDetailsOld.getBusinessName().getContact().getEmailId(), true, dFile,fileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return "";
	}
	//make ascending order product details id
	class OrderProductDetailsIdComparator implements Comparator<OrderProductDetails>{  
		public int compare(OrderProductDetails s1,OrderProductDetails s2){  
		return Long.compare(s1.getOrderProductDetailsid(),s2.getOrderProductDetailsid());  
		}  
	}
	/**
	 * <pre>
	 * edit order on Booked,Packed,Issued order status
	 * @param orderRequest
	 * @return orderId
	 * </pre>
	 */
	@Transactional
	public String updateBookOrder(OrderRequest orderRequest) {
			
		OrderDetails orderDetails=fetchOrderDetailsByOrderId(orderRequest.getOrderDetails().getOrderId());
			List<OrderProductDetails> orderProductDetailsListOld=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
			List<OrderProductDetails> orderProductDetailsList=new ArrayList<>();
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			double totalAmount=0;
			double totalAmountWithTax=0;
			long totalQuantity=0;
			Product product;
			//only booked status order can edit in if section
			if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
			{
				//calculate main totals of OrderDetails 
				for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList())
				{
					product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProductId());
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), product.getCategories().getIgst());
					totalAmount+=calculateProperTaxModel.getUnitprice()*orderProductDetails.getPurchaseQuantity();
					totalAmountWithTax+=orderProductDetails.getPurchaseAmount();
					totalQuantity+=orderProductDetails.getPurchaseQuantity();
				}
				orderDetails.setTotalAmount(totalAmount);
				orderDetails.setTotalAmountWithTax(totalAmountWithTax);
				orderDetails.setTotalQuantity(totalQuantity);
				
				orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
				sessionFactory.getCurrentSession().update(orderDetails);
				
				//delete previous order product details list
				for(OrderProductDetails orderProductDetails:orderProductDetailsListOld)
				{				
					orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
					sessionFactory.getCurrentSession().delete(orderProductDetails);
					deleteOrderUsedProduct(orderProductDetails.getProduct());
					
				}
				//saving updated order product details with order used product,categories,brand
				for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList())
				{
					orderProductDetails.setOrderDetails(orderDetails);
					
					product=productDAO.fetchProductForWebApp(orderProductDetails.getProduct().getProductId());
					
					Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
					
					OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
					orderUsedBrand.setName(product.getBrand().getName());
					orderUsedBrand.setCompany(company);
					orderUsedBrand.setBranch(branch);
					sessionFactory.getCurrentSession().save(orderUsedBrand);
					
					OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
							product.getCategories().getCategoryName(), 
							product.getCategories().getHsnCode(), 
							product.getCategories().getCgst(), 
							product.getCategories().getSgst(), 
							product.getCategories().getIgst(),
							company,
							branch);
					sessionFactory.getCurrentSession().save(orderUsedCategories);
					
					OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
							product,
							product.getProductName(), 
							product.getProductCode(), 
							orderUsedCategories, 
							orderUsedBrand, 
							product.getRate(), 
							/*product.getProductImage(),
							product.getProductContentType(),*/
							product.getThreshold(), 
							product.getCurrentQuantity(),
							product.getDamageQuantity(),
							company,
							branch);
									
					sessionFactory.getCurrentSession().save(orderUsedProduct);
					
					orderProductDetails.setProduct(orderUsedProduct);
					
					sessionFactory.getCurrentSession().save(orderProductDetails);
				}
			}
			else
			{
				//only packed and issued status orders can edit in else part
				OrderProductIssueDetails  orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(orderDetails.getOrderId());
				
				//calculate totalIssued quantity
				//create order used product,category,brand  then order used product and store in {@link orderProductDetailsList}
				for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList())
				{					
					product=orderProductDetails.getProduct().getProduct();
					totalQuantity+=orderProductDetails.getIssuedQuantity();
					
					if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), product.getCategories().getIgst());
						double issueAmt=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
						totalAmount+=issueAmt;		
						
						totalAmountWithTax+=orderProductDetails.getIssueAmount();
						
						orderProductDetails.setIssueAmount(orderProductDetails.getIssueAmount());
					}
					else
					{
						orderProductDetails.setSellingRate(0);
						orderProductDetails.setIssueAmount(0);
					}
					orderProductDetails.setOrderDetails(orderDetails);

					Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
					
					OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
					orderUsedBrand.setName(product.getBrand().getName());
					orderUsedBrand.setCompany(company);
					orderUsedBrand.setBranch(branch);
					//sessionFactory.getCurrentSession().save(orderUsedBrand);
					
					OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
							product.getCategories().getCategoryName(), 
							product.getCategories().getHsnCode(), 
							product.getCategories().getCgst(), 
							product.getCategories().getSgst(), 
							product.getCategories().getIgst(),
							company,
							branch);
					//sessionFactory.getCurrentSession().save(orderUsedCategories);
					
					OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
							product,
							product.getProductName(), 
							product.getProductCode(), 
							orderUsedCategories, 
							orderUsedBrand, 
							product.getRate(), 
							/*product.getProductImage(),
							product.getProductContentType(),*/
							product.getThreshold(), 
							product.getCurrentQuantity(),
							product.getDamageQuantity(),
							company,
							branch);
									
					//sessionFactory.getCurrentSession().save(orderUsedProduct);
					
					orderProductDetails.setProduct(orderUsedProduct);
					orderProductDetailsList.add(orderProductDetails);
				}
				orderDetails.setIssuedTotalAmount(totalAmount);
				orderDetails.setIssuedTotalAmountWithTax(totalAmountWithTax);
				orderDetails.setIssuedTotalQuantity(totalQuantity);
				
				List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
				//delete old order product details
				for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
				{
					boolean isDeleted=true;
					// if product not delete then update purchase quantity and amount as old order products
					for(int i=0; i<orderProductDetailsList.size(); i++){
					
						if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
								&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
						{
							OrderProductDetails orderProductDetails=orderProductDetailsList.get(i);
							
							orderProductDetails.setPurchaseAmount(orderProductDetailsOld.getPurchaseAmount());
							orderProductDetails.setPurchaseQuantity(orderProductDetailsOld.getPurchaseQuantity());
							orderProductDetailsList.set(i, orderProductDetails);
							isDeleted=false;
						}
						
					}
					//if product found as delete and order status is issued then add in Return From delivery boy 
					if(isDeleted)
					{
						//if order products delete when issued order status then its make ReturnFromDeliveryBoy List 
						//for temporary store upto delivery boy return give order product to gatekeeper 
						//and then gatekeeper check and add to current quantity or define damange
						if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
						{
							//OrderProductDetails orderProductDetails=orderProductDetailsList.get(i);
							product=orderProductDetailsOld.getProduct().getProduct();
							
							Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
							Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
							
							OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
							orderUsedBrand.setName(product.getBrand().getName());
							orderUsedBrand.setCompany(company);
							orderUsedBrand.setBranch(branch);
							sessionFactory.getCurrentSession().save(orderUsedBrand);
							
							OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
									product.getCategories().getCategoryName(), 
									product.getCategories().getHsnCode(), 
									product.getCategories().getCgst(), 
									product.getCategories().getSgst(), 
									product.getCategories().getIgst(),
									company,
									branch);
							sessionFactory.getCurrentSession().save(orderUsedCategories);
							
							OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
									product,
									product.getProductName(), 
									product.getProductCode(), 
									orderUsedCategories, 
									orderUsedBrand, 
									product.getRate(), 
									/*product.getProductImage(),
									product.getProductContentType(),*/
									product.getThreshold(), 
									product.getCurrentQuantity(),
									product.getDamageQuantity(),
									company,
									branch);
											
							sessionFactory.getCurrentSession().save(orderUsedProduct);
								
								// return from delivery boy child create and add in {@link returnFromDeliveryBoyList}
								ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
										orderUsedProduct, 
										orderProductDetailsOld.getSellingRate(), 
										orderProductDetailsOld.getIssuedQuantity(), 
										orderProductDetailsOld.getIssuedQuantity(),
										0,
										0,
										0, 
										orderProductDetailsOld.getType());
								returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
						}
					}
					
					//permanent delete order product
					orderProductDetailsOld=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetailsOld);
					sessionFactory.getCurrentSession().delete(orderProductDetailsOld);
					
					//permanent delete order used product 
					deleteOrderUsedProduct(orderProductDetailsOld.getProduct());
				}
				
				for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
				{
					boolean productHave=false;
					//compare old order used product with new order used product and update product current quantity
					//here update current quantity which not deleted
					for(OrderProductDetails orderProductDetails: orderProductDetailsList)
					{
						if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetails.getProduct().getProduct().getProductId()
								&& orderProductDetailsOld.getType().equals(orderProductDetails.getType()))
						{
							productHave=true;
							product=orderProductDetails.getProduct().getProduct();
							
							//if packed status then 
							//update product current quantity directly
							////update daily stock details
							if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
							{		
									if(orderProductDetailsOld.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
									{
										product.setFreeQuantity(product.getFreeQuantity()-orderProductDetailsOld.getIssuedQuantity());
										product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
									}
									else
									{	
										product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
									}
									
									
									if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
									{
										product.setFreeQuantity(product.getFreeQuantity()+orderProductDetails.getIssuedQuantity());
										product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetails.getIssuedQuantity());
									}
									else
									{	
										product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetails.getIssuedQuantity());
									}
									
									
									product=(Product)sessionFactory.getCurrentSession().merge(product);
									productDAO.Update(product);
									productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), false);
									productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetailsOld.getIssuedQuantity(), true);
									//updateOrderUsedProductCurrentQuantity(product);
							}
							//if order status id issued then 
							//if old issued quantity is greater than new issued quantity
							//order product details add in return from delivery boy
							if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED) 
									&& orderProductDetailsOld.getIssuedQuantity()>orderProductDetails.getIssuedQuantity())
							{
								long returnQuantity=orderProductDetailsOld.getIssuedQuantity()-orderProductDetails.getIssuedQuantity();
								
								Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
								Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
								
								OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
								orderUsedBrand.setName(product.getBrand().getName());
								orderUsedBrand.setCompany(company);
								orderUsedBrand.setBranch(branch);
								sessionFactory.getCurrentSession().save(orderUsedBrand);
								
								OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
										product.getCategories().getCategoryName(), 
										product.getCategories().getHsnCode(), 
										product.getCategories().getCgst(), 
										product.getCategories().getSgst(), 
										product.getCategories().getIgst(),
										company,
										branch);
								sessionFactory.getCurrentSession().save(orderUsedCategories);
								
								OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
										product,
										product.getProductName(), 
										product.getProductCode(), 
										orderUsedCategories, 
										orderUsedBrand, 
										product.getRate(), 
										/*product.getProductImage(),
										product.getProductContentType(),*/
										product.getThreshold(), 
										product.getCurrentQuantity(),
										product.getDamageQuantity(),
										company,
										branch);
												
								sessionFactory.getCurrentSession().save(orderUsedProduct);
								// return from delivery boy child create and add in {@link returnFromDeliveryBoyList}
								ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
										orderUsedProduct, 
										orderProductDetails.getSellingRate(), 
										returnQuantity, 
										orderProductDetailsOld.getIssuedQuantity(),
										orderProductDetails.getIssuedQuantity(),
										0,
										0, 
										orderProductDetails.getType());
								returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
								//sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
							}							
						}					
					}
					//if product found as deleted from new order product list
					//and if order status is packed then packed quantity return add directly to product current quantity
					//update daily stock details
					if(productHave==false)
					{
						if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
						{
							product=productDAO.fetchProductForWebApp(orderProductDetailsOld.getProduct().getProduct().getProductId());
							
							if(orderProductDetailsOld.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
							{
								product.setFreeQuantity(product.getFreeQuantity()-orderProductDetailsOld.getIssuedQuantity());
								product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
							}
							else
							{	
								product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
							}
							
							product=(Product)sessionFactory.getCurrentSession().merge(product);
							productDAO.Update(product);
							productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetailsOld.getIssuedQuantity(), true);
							//updateOrderUsedProductCurrentQuantity(product);
						}
					}
				}
				
				
				/*if(returnFromDeliveryBoyListOld!=null)
				{
					List<ReturnFromDeliveryBoy> returnFromDeliveryBoyListNewAdd=new ArrayList<>();
					
					//new added changes merge with previous records
					for(int i=0; i<returnFromDeliveryBoyList.size(); i++)
					{
						//boolean inside=false;
						for(ReturnFromDeliveryBoy returnFromDeliveryBoyOld: returnFromDeliveryBoyListOld)
						{
							if(returnFromDeliveryBoyList.get(i).getProduct().getProduct().getProductId()==returnFromDeliveryBoyOld.getProduct().getProduct().getProductId()
									&& returnFromDeliveryBoyList.get(i).getType().equals(returnFromDeliveryBoyOld.getType()))
							{
								//inside=true;
								ReturnFromDeliveryBoy returnFromDeliveryBoy=returnFromDeliveryBoyList.get(i);
								returnFromDeliveryBoy.setIssuedQuantity(returnFromDeliveryBoyOld.getIssuedQuantity());
								returnFromDeliveryBoy.setReturnQuantity(returnFromDeliveryBoyOld.getIssuedQuantity()-returnFromDeliveryBoy.getDeliveryQuantity());
								returnFromDeliveryBoyList.set(i, returnFromDeliveryBoy);
							}
						}
						/*if(inside==false)
						{
							returnFromDeliveryBoyListNewAdd.add(returnFromDeliveryBoyList.get(i));
						}
					}
					//old but not change add
					for(ReturnFromDeliveryBoy returnFromDeliveryBoyOld: returnFromDeliveryBoyListOld)
					{
						boolean inside=false;
						for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
						{
							if(returnFromDeliveryBoy.getProduct().getProduct().getProductId()==returnFromDeliveryBoyOld.getProduct().getProduct().getProductId()
									&& returnFromDeliveryBoy.getType().equals(returnFromDeliveryBoyOld.getType()))
							{
								inside=true;
							}
						}
						if(inside==false)
						{
							returnFromDeliveryBoyListNewAdd.add(returnFromDeliveryBoyOld);
						}
						else
						{
							deleteOrderUsedProduct(returnFromDeliveryBoyOld.getProduct());
						}
						sessionFactory.getCurrentSession().delete(returnFromDeliveryBoyOld);
					}
					returnFromDeliveryBoyList.addAll(returnFromDeliveryBoyListNewAdd);					
				}*/
				
				
				// if returnFromDeliveryBoyList not found as empty then save return from delivery boy details 
				if(!returnFromDeliveryBoyList.isEmpty())
				{
					long totalReturnQuantity=0;
					long totalIssuedQuantity=0;
					long totalDeliveryQuantity=0;
					long totalDamageQuantity=0;
					long totalNonDamageQuantity=0;
					
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
					{
						totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
						totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
						totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
						totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
						totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
					}
					Company company=new Company();
					company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					
					Branch branch=new Branch();
					branch.setBranchId(getSessionSelectedBranchIds());
						ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
								totalReturnQuantity, 
								totalIssuedQuantity, 
								totalDeliveryQuantity, 
								totalDamageQuantity, 
								totalNonDamageQuantity, 
								orderProductIssueDetails.getEmployeeGK(), 
								orderProductIssueDetails.getEmployeeDB(), 
								orderDetails, 
								new Date(), 
								false,
								company,
								branch);
						
					//ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator=new ReturnFromDeliveryBoyGenerator(sessionFactory);
					returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
					sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
				
				
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
					{
						returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
						sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
					}
				}
				//add old product with issue qty zero and amt zero
				//make issued quantity and amount zero when product found as deleted
				List<OrderProductDetails> orderProductDetailsListTemp=new ArrayList<>();
				for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
				{
					boolean productFile=true;
					for(int i=0; i<orderProductDetailsList.size(); i++)
					{
						if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
								&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
						{
							productFile=false;
						}
					}
					
					if(productFile)
					{
						orderProductDetailsOld.setIssueAmount(0);
						orderProductDetailsOld.setIssuedQuantity(0);
						orderProductDetailsListTemp.add(orderProductDetailsOld);
						//deleteOrderUsedProduct(orderProductDetailsOld.getProduct());
					}
				}
				orderProductDetailsList.addAll(orderProductDetailsListTemp);
				
				//add new edited order product details
				boolean purchaseDataChange=false;
				for(int i=0; i<orderProductDetailsList.size(); i++)
				{
					boolean productHave=false;
					//find product is deleted or not
					for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
					{
						if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
								&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
						{
							productHave=true;
						}
					}
					//update current quantity which order product not deleted 
					if(productHave==false)
					{
						product=orderProductDetailsList.get(i).getProduct().getProduct();
						
						if(orderProductDetailsList.get(i).getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
						{
							product.setFreeQuantity(product.getFreeQuantity()+orderProductDetailsList.get(i).getIssuedQuantity());
							product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetailsList.get(i).getIssuedQuantity());
						}
						else
						{	
							product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetailsList.get(i).getIssuedQuantity());
						}
						
						product=(Product)sessionFactory.getCurrentSession().merge(product);
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetailsList.get(i).getIssuedQuantity(), false);
						//updateOrderUsedProductCurrentQuantity(product);
					}		
					//if order status is packed and editModeList(gives records which edited in packed status) is non empty
					//if order product found is edited then same issued quantiy and amount same reflect in purchase quantity and amount
					if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED)
							&& !orderRequest.getEditModeList().isEmpty()){
						
						if(findModeOfOrderProduct(orderRequest.getEditModeList(), 
											  orderProductDetailsList.get(i).getProduct().getProduct().getProductId(), 
											  orderProductDetailsList.get(i).getType()).equals(Constants.EDIT_MODE))
						{
							purchaseDataChange=true;
							orderProductDetails=orderProductDetailsList.get(i);
							orderProductDetails.setPurchaseAmount(orderProductDetailsList.get(i).getIssueAmount());
							orderProductDetails.setPurchaseQuantity(orderProductDetailsList.get(i).getIssuedQuantity());
							orderProductDetailsList.set(i,orderProductDetails);
						}				
					}
					
					// save order used category 
					sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct().getCategories());
					
					// save order used brand 
					sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct().getBrand());
					
					// save order used product 
					sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct());
					
					// save order product 
					sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i));
				}

				double totalPurchaseAmount=0,totalPurchaseAmountWithTax=0;
				long totalPurchaseQuantity=0;
				// if any order product found as edited in order status packed
				//then recalculate main orderdetails total amount,amountwithtax,quantity
				if(purchaseDataChange)
				{
					for(OrderProductDetails orderProductDetails : orderProductDetailsList)
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
						totalPurchaseAmount+=orderProductDetails.getPurchaseQuantity()*calculateProperTaxModel.getUnitprice();//orderProductDetails.getProduct().getRate();
						totalPurchaseAmountWithTax+=orderProductDetails.getPurchaseAmount();
						totalPurchaseQuantity+=orderProductDetails.getPurchaseQuantity();
					}
					orderDetails.setTotalQuantity(totalPurchaseQuantity);
					orderDetails.setTotalAmount(totalPurchaseAmount);			
					orderDetails.setTotalAmountWithTax(totalPurchaseAmountWithTax);
				}				
				
				//update order details 
				orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
				sessionFactory.getCurrentSession().update(orderDetails);			
			}
			//update order used product current quantity with product current quantity
			updateOrderUsedProductCurrentQuantity();
			
			//return order id
			return orderDetails.getOrderId(); 		
	}
	/**
	 * <pre>
	 * find order product is edited or not in packed status
	 * </pre>
	 * @param editOrderModeList
	 * @param productId
	 * @param type
	 * @return Edit / Non Edit
	 */
	public String findModeOfOrderProduct(String editOrderModeList,long productId,String type)
	{
		String orderModeList[]=editOrderModeList.split(",");
		for(int i=0; i<orderModeList.length; i++)
		{
			String orderMode[]=orderModeList[i].split("-");
			if(Integer.parseInt(orderMode[0])==productId && orderMode[1].equals(type))
			{
				return Constants.EDIT_MODE;//orderMode[2];
			}
		}
		return Constants.NON_EDIT_MODE;
	}
	/**
	 * <pre>
	 * fetch order details by orderId which is belongs to current logged user company
	 * @param orderDetailsId
	 * @return order details
	 *  </pre>
	 */
	@Transactional
	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId){
		
		String hql="from OrderDetails where orderId='"+orderDetailsId+"'"+
				   " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") ";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		return list.get(0);
 	}
	/**
	 * <pre>
	 * fetch order details by orderId which is belongs to current logged user company
	 * and order business area need to be in logged used area assigned list
	 * @param orderDetailsId
	 * @return order details
	 *  </pre>
	 */
	@Transactional
	public OrderDetails fetchOrderDetailsByOrderIdForApp(String orderDetailsId){
		
		String hql="from OrderDetails where orderId='"+orderDetailsId+"'"+
				   " and businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+")"+
				   " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				   " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") ";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		return list.get(0);
 	}
	/**
	 * <pre>
	 * update order details main object 
	 * 1. payment due date
	 * 2. payment period days number
	 * 3. payment status
	 * 4. etc 
	 * @param orderDetails
	 * </pre>
	 */
	@Transactional
	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails){
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
	}
	/**
	 * <pre>
	 * fetch order details and product list 
	 * order list get by which business area belongs logged user areas and company only   
	 * @param employeeId
	 * @return OrderDetailsList
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchOrderListByAreaId(long employeeId) {

		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		
		hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE())  and businessName.area.areaId in (:ids) and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
				" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
		//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", pnjId);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			orderDetailsList.setOrderDetailsForWebList(list);
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch order details and product list 
	 * order list get by which business area belongs logged user areas and company only 
	 * @param areaId  
	 * @param employeeId
	 * @return OrderDetailsList
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchOrderListByAreaId(long areaId,long employeeId) {

		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE()) and businessName.area.areaId="+areaId+" and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
					" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
					" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
					" order by orderDetailsAddedDatetime desc";
			//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
		}
		else
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE())  and businessName.area.areaId in (:ids) and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
					" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
					" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
					" order by orderDetailsAddedDatetime desc";
			//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch booked before current date order details and product list 
	 * order list get by which business area belongs logged user areas and company only 
	 * @param areaId  
	 * @param employeeId
	 * @return OrderDetailsList
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchPendingOrderListByAreaId(long areaId,long employeeId) {

		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		
		orderDetailsList.setAreaList(areaList);
		
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) < date(CURRENT_DATE()) and businessName.area.areaId="+areaId+" and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
					" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
					" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
					" order by orderDetailsAddedDatetime desc";
			//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);		
		}
		else
		{			
			hql="from OrderDetails where date(orderDetailsAddedDatetime) < date(CURRENT_DATE())  and businessName.area.areaId in (:ids) and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
					" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
					" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
					" order by orderDetailsAddedDatetime desc";
			//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch booked current date order details and product list by areaId 
	 * @param areaId  
	 * @return OrderDetailsList
	 * </pre>
	 */
	/*@Transactional
	public List<OrderDetails> fetchOrderDetailsTodaysListByAreaId(long areaId) {
		// TODO Auto-generated method stub
		
		String hql="from OrderDetails where  date(orderDetailsAddedDatetime) = date(CURRENT_DATE()) and businessName.area.areaId="+ areaId+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
			    //" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> todaysorderdDetailsList=(List<OrderDetails>)query.list();
		if(todaysorderdDetailsList.isEmpty())
		{
			return null;
		}
		return todaysorderdDetailsList;
	}*/
	/**
	 * <pre>
	 * fetch booked before current date order details and product list by areaId 
	 * @param areaId  
	 * @return OrderDetailsList
	 * </pre>
	 */
/*	@Transactional
	public List<OrderDetails> fetchOrderDetailsPendingListByAreaId(long areaId) {
		// TODO Auto-generated method stub

		String hql="from OrderDetails where  date(orderDetailsAddedDatetime) < date(CURRENT_DATE()) and businessName.area.areaId="+ areaId+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> todaysorderdDetailsList=(List<OrderDetails>)query.list();
		if(todaysorderdDetailsList.isEmpty())
		{
			return null;
		}
		return todaysorderdDetailsList;
	}*/
	/**
	 * <pre>
	 * fetch booked before current date order details and product list 
	 * order list get by which business area belongs logged user areas and company only 
	 * @param areaId  
	 * @param employeeId
	 * @return OrderDetailsList
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchPendingOrderListByAreaId(long employeeId) {

		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		
		orderDetailsList.setAreaList(areaList);
		
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		
		hql="from OrderDetails where date(orderDetailsAddedDatetime) < date(CURRENT_DATE())  and businessName.area.areaId in (:ids) and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'"+
				" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", pnjId);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			orderDetailsList.setOrderDetailsForWebList(list);
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch order details fetch by range,startDate,endDate
	 * get order details only in Booked,Packed,Issued,Delivery Pending
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showOrderReport(String range,String startDate,String endDate)
	{
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) >= '"+startDate+"' and date(orderDetailsAddedDatetime) <= '"+endDate+"' and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("today"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE()) and  orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+simpleDateFormat.format(cal.getTime())+"' and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) >= '"+simpleDateFormat.format(cal.getTime())+"'  and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLastMonthFirstDate()+"') and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("last3Months"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLast3MonthFirstDate()+"') and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("pickDate"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+startDate+"' and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderDetails where orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}

		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(OrderDetails orderDetails : list){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			 if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK = "Pending";
					orderStatusGKDate = "--";
					orderStatusGKTime= "--";
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getTotalAmount();
					amountWithTax=orderDetails.getTotalAmountWithTax();
					quantity=orderDetails.getTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_PACKED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(/*orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED) || */orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB=orderDetails.getOrderStatus().getStatus();
					orderStatusDBDate=simpleDateFormat.format(orderDetails.getConfirmDate());
					orderStatusDBTime= simpleTimeFormat.format(orderDetails.getConfirmDate());
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
			
			list2.add(new OrderReportList(srno,
					orderDetails.getOrderId(), 
					amount,
					amountWithTax,
					orderDetails.getBusinessName().getArea().getName(),
					quantity, 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
					orderDetails.getPaymentPeriodDays(), 
					orderDetails.getOrderDetailsAddedDatetime(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							orderDetails.getOrderStatus().getStatus()));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * fetch order details fetch by range,startDate,endDate,businessNameId 
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate)
	{
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) >= '"+startDate+"' and date(orderDetailsAddedDatetime) <= '"+endDate+"' and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("today"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE()) and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+simpleDateFormat.format(cal.getTime())+"' and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) >= '"+simpleDateFormat.format(cal.getTime())+"' and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')  and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("last3Months"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"') and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("pickDate"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+startDate+"' and businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderDetails where  businessName.businessNameId='"+businessNameId+"'";
		}
		
		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				//" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')"+
				" order by orderDetailsAddedDatetime desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy"); 
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(OrderDetails orderDetails : list){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			 if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK = "Pending";
					orderStatusGKDate = "--";
					orderStatusGKTime= "--";
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getTotalAmount();
					amountWithTax=orderDetails.getTotalAmountWithTax();
					quantity=orderDetails.getTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_PACKED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED) || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB=orderDetails.getOrderStatus().getStatus();
					orderStatusDBDate=simpleDateFormat.format(orderDetails.getConfirmDate());
					orderStatusDBTime= simpleTimeFormat.format(orderDetails.getConfirmDate());
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}else{
					if(orderDetails.getEmployeeIdCancel().getDepartment().equals(Constants.SALESMAN_DEPT_NAME)){
						
						orderStatusSM=Constants.ORDER_STATUS_BOOKED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK = "Pending";
						orderStatusGKDate = "--";
						orderStatusGKTime= "--";
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getTotalAmount();
						amountWithTax=orderDetails.getTotalAmountWithTax();
						quantity=orderDetails.getTotalQuantity();
						
					}else if(orderDetails.getEmployeeIdCancel().getDepartment().equals(Constants.GATE_KEEPER_DEPT_NAME)){
						
						orderStatusSM=Constants.ORDER_STATUS_BOOKED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK=Constants.ORDER_STATUS_PACKED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
						orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getIssuedTotalAmount();
						amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
						quantity=orderDetails.getIssuedTotalQuantity();
						
					}else{
						orderStatusSM=Constants.ORDER_STATUS_BOOKED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK=Constants.ORDER_STATUS_ISSUED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
						orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getIssuedTotalAmount();
						amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
						quantity=orderDetails.getIssuedTotalQuantity();
					}
				}
			
			list2.add(new OrderReportList(srno,
					orderDetails.getOrderId(), 
					amount,
					amountWithTax,
					orderDetails.getBusinessName().getArea().getName(),
					quantity, 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
					orderDetails.getPaymentPeriodDays(), 
					orderDetails.getOrderDetailsAddedDatetime(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							orderDetails.getOrderStatus().getStatus()));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * fetch order details fetch by range,startDate,endDate,employeeSMId 
	 * get order details only in Booked,Packed,Issued,Delivery Pending
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showOrderReportByEmployeeSMId(String employeeSMId,String range,String startDate,String endDate)
	{
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) >= '"+startDate+"' and date(orderDetailsAddedDatetime) <= '"+endDate+"' and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("today"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = date(CURRENT_DATE()) and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+simpleDateFormat.format(cal.getTime())+"' and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+simpleDateFormat.format(cal.getTime())+"' and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("last3Months"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLast3MonthFirstDate()+"') and employeeSM.employeeId="+employeeSMId+"  and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getLastMonthFirstDate()+"') and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("pickDate"))
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime) = '"+startDate+"' and employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderDetails where employeeSM.employeeId="+employeeSMId+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		
		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" order by orderDetailsAddedDatetime desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(OrderDetails orderDetails : list){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			 if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK = "Pending";
					orderStatusGKDate = "--";
					orderStatusGKTime= "--";
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getTotalAmount();
					amountWithTax=orderDetails.getTotalAmountWithTax();
					quantity=orderDetails.getTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_PACKED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED) || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB=orderDetails.getOrderStatus().getStatus();
					orderStatusDBDate=simpleDateFormat.format(orderDetails.getConfirmDate());
					orderStatusDBTime= simpleTimeFormat.format(orderDetails.getConfirmDate());
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
			
			list2.add(new OrderReportList(srno,
					orderDetails.getOrderId(), 
					amount,
					amountWithTax,
					orderDetails.getBusinessName().getArea().getName(),
					quantity, 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
					orderDetails.getPaymentPeriodDays(), 
					orderDetails.getOrderDetailsAddedDatetime(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							orderDetails.getOrderStatus().getStatus()));
			srno++;
		}
		
		return list2;
	}
	
	/**
	 * <pre>
	 * fetch order product details by order id which belong logged user company
	 * @param orderId
	 * @return OrderProductDetails list
	 * </pre>
	 */
	@Transactional
    public List<OrderProductDetails> fetchOrderProductDetailByOrderId(String orderId) {
        // TODO Auto-generated method stub
        String hql="from OrderProductDetails where orderDetails.orderId='"+orderId+"'"+
        	       " and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
        	       " and orderDetails.businessName.branch.branchId="+getSessionSelectedBranchIds();
        Query query=sessionFactory.getCurrentSession().createQuery(hql);
        List<OrderProductDetails> list=(List<OrderProductDetails>)query.list();
        if(list.isEmpty()){
            return null;
        }
        
        return list;
    
    }
	/**
	 * <pre>
	 * fetch order product details by order id which belong logged user company,areas(compare with business area)
	 * @param orderId
	 * @return OrderProductDetails list
	 * </pre>
	 */
	@Transactional
    public List<OrderProductDetails> fetchOrderProductDetailByOrderIdForApp(String orderId) {
        // TODO Auto-generated method stub
        String hql="from OrderProductDetails where orderDetails.orderId='"+orderId+"'"+
        		   " and orderDetails.businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+")"+
        	       " and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
        	       " and orderDetails.businessName.branch.branchId="+getSessionSelectedBranchIds();
        
        Query query=sessionFactory.getCurrentSession().createQuery(hql);
        List<OrderProductDetails> list=(List<OrderProductDetails>)query.list();
        if(list.isEmpty()){
            return null;
        }
        
        return list;
    
    }
	
	/*public List<OrderProductDetails> makeProductImageMNullorderProductDetailsList(List<OrderProductDetails> orderProductDetailsList)
	{
		List<OrderProductDetails> orderProductDetailsL=new ArrayList<>();
		
		for(OrderProductDetails orderProductDetails : orderProductDetailsList)
		{
			orderProductDetails.getProduct().setProductImage(null);
			orderProductDetails.getProduct().getProduct().setProductImage(null);
			orderProductDetailsL.add(orderProductDetails);
		}
		
		return orderProductDetailsL;
	}*/
	/**
	 * <pre>
	 * fetch order product details by orderId
	 * @param orderDetailsId
	 * @return OrderProductDetailListForWebApp list
	 * </pre>
	 */
	@Transactional
	public List<OrderProductDetailListForWebApp> orderProductDetailsListForWebApp(String orderDetailsId){
		
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebAppsList=new ArrayList<>();
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		
		List<OrderProductDetails> orderProductDetails=fetchOrderProductDetailByOrderId(orderDetailsId);
		long srno=1;
		
		OrderDetails orderDetails=fetchOrderDetailsByOrderId(orderDetailsId);
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		
		for(OrderProductDetails orderProductDetail : orderProductDetails)
		{
			double totalAmount=0;
			double totalAmountWithTax=0;
			long quantity=0;
			
			double  sellingRate=orderProductDetail.getSellingRate();
			//if order not cancel  still
			if(orderDetails.getEmployeeIdCancel()==null)
			{
				// if order status is booked then calculate amounts by purchase quantity else issued quantity
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					if(orderProductDetail.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						totalAmount=0;
						totalAmountWithTax=0;
					}
					else
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetail.getSellingRate(), 
																												   orderProductDetail.getProduct().getCategories().getIgst());
						totalAmount=(calculateProperTaxModel.getUnitprice()*orderProductDetail.getPurchaseQuantity());;
						totalAmountWithTax=(sellingRate*orderProductDetail.getPurchaseQuantity());
					}
					quantity=orderProductDetail.getPurchaseQuantity();
				}
				else
				{
					if(orderProductDetail.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						totalAmount=0;
						totalAmountWithTax=0;
					}
					else
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetail.getSellingRate(), 
								   																				   orderProductDetail.getProduct().getCategories().getIgst());
						totalAmount=(calculateProperTaxModel.getUnitprice()*orderProductDetail.getIssuedQuantity());;
						totalAmountWithTax=(sellingRate*orderProductDetail.getIssuedQuantity());
					}
					quantity=orderProductDetail.getIssuedQuantity();
				}
			}
			//if order is cancelled 
			else
			{
				// if order cancelled by salesman then calculate amounts by purchase quantity else issued quantity
				if(orderDetails.getEmployeeIdCancel().getDepartment().getName().equals(Constants.SALESMAN_DEPT_NAME))
				{
					if(orderProductDetail.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						totalAmount=0;
						totalAmountWithTax=0;
					}
					else
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetail.getSellingRate(), 
																								                   orderProductDetail.getProduct().getCategories().getIgst());
						totalAmount=(calculateProperTaxModel.getUnitprice()*orderProductDetail.getPurchaseQuantity());;
						totalAmountWithTax=(sellingRate*orderProductDetail.getPurchaseQuantity());
					}
					quantity=orderProductDetail.getPurchaseQuantity();
				}
				else
				{
					if(orderProductDetail.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						totalAmount=0;
						totalAmountWithTax=0;
					}
					else
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetail.getSellingRate(), 
				                   																			       orderProductDetail.getProduct().getCategories().getIgst());
						totalAmount=(calculateProperTaxModel.getUnitprice()*orderProductDetail.getIssuedQuantity());;
						totalAmountWithTax=(sellingRate*orderProductDetail.getIssuedQuantity());
					}
					quantity=orderProductDetail.getIssuedQuantity();
				}
			}
			
			orderProductDetailListForWebAppsList.add(new OrderProductDetailListForWebApp(srno,
																		orderProductDetail.getOrderDetails().getOrderId(),
																		orderProductDetail.getProduct().getCategories().getCategoryName(),
																		orderProductDetail.getProduct().getBrand().getName(),
																		orderProductDetail.getProduct().getProductName(),
																		employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderProductDetail.getOrderDetails().getEmployeeSM().getEmployeeId()).getName(),
																		employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderProductDetail.getOrderDetails().getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
																		employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderProductDetail.getOrderDetails().getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
																		Double.parseDouble(new DecimalFormat("###").format(sellingRate)),
																		quantity,
																		totalAmount, 
																		totalAmountWithTax, 
																		orderProductDetail.getOrderDetails().getOrderDetailsAddedDatetime(),
																		orderProductDetail.getType()));
			srno++;
		}
		
		return orderProductDetailListForWebAppsList;
	}
	/**
	 * <pre>
	 * fetch supplier order by range,startDate,endDate which comes under last 24 hour
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return SupplierOrder list
	 * </pre>
	 */
	@Transactional
	public List<SupplierOrder> fetchSupplierOrders24hour(String range,String startDate,String endDate){
		
			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			String hql;
			Query query = null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			//HH:mm:ss.SSSSSS
			SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
		/*	String orderDetailsId,
		 	
			orderDetails=orderDetailsDAOImpl.fetchOrderDetailsByOrderId(orderDetailsId);
			Calendar orderDate=Calendar.getInstance();
			orderDate.setTime(orderDetails.getOrderDetailsAddedDatetime());*/
			
			/*if (range.equals("last7days")) {
				cal.add(Calendar.DAY_OF_MONTH, -7);
				
				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:date1day) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("date1day", cal);

			}
			else if (range.equals("last1month")) {
				cal.add(Calendar.MONTH, -1);

				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:date1day) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("date1day", cal);

			}		
			else if (range.equals("all")) {

				hql="from SupplierOrder";
				query=sessionFactory.getCurrentSession().createQuery(hql);

			}
			else */if (range.equals("CurrentDay")) {
				
				cal.add(Calendar.HOUR_OF_DAY, -24);			
				hql="from SupplierOrder where supplierOrderDatetime >= '"+dateFormat2.format(cal.getTime())+"' "+
					" and status=true and company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and branch.branchId="+getSessionSelectedBranchIds()+
					" order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				//query.setCalendarDate("date1day", cal);
			}
			/*else if (range.equals("range")) {
				cal.setTime(dateFormat.parse(startDate));
				cal2.setTime(dateFormat.parse(endDate));
			
				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:startDate) and date(supplierOrderDatetime) <= date(:endDate) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("startDate", cal);
				query.setCalendarDate("endDate", cal);
				
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}*/
			List<SupplierOrder> list=(List<SupplierOrder>)query.list();
			if(list.isEmpty())
			{
				return null;
			}
					
			return list;
	}
	/**
	 * <pre>
	 * fetch supplier order details which belongs to given supplier id
	 * @param supplierId
	 * @return SupplierOrder list
	 * </pre>
	 */
	@Transactional
	public List<SupplierOrder> fetchSupplierOrders24hourBySupplierId(String supplierId){
		//List<SupplierOrder> supplierOrdersList=new ArrayList<>();
		String hql="SELECT distinct SupplierOrder FROM SupplierOrderDetails where supplierOrder.company.companyId="+getSessionSelectedCompaniesIds()+
				" and supplierOrder.supplierOrderId='"+supplierId+"'"+
				" and supplierOrder.branch.branchId="+getSessionSelectedBranchIds();
		
		//String hql="from SupplierOrder where company.companyId="+getSessionSelectedCompaniesIds()+" order by supplierOrderDatetime desc";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrder> supplierOrderList=(List<SupplierOrder>)query.list();
		return supplierOrderList;
		/*for(SupplierOrder supplierOrder : supplierOrderList)
		{
			String hql2="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrder.getSupplierOrderId()+"'"
					+ " and supplierOrder.company.companyId="+getSessionSelectedCompaniesIds();
			Query query2= sessionFactory.getCurrentSession().createQuery(hql2);
			List<SupplierOrderDetails> supplierOrderDetailsList=(List<SupplierOrderDetails>)query2.list();
			
			for(SupplierOrderDetails supplierOrderDetails : supplierOrderDetailsList)
			{
				if(supplierOrderDetails.getSupplier().getSupplierId().equals(supplierId))
				{
					supplierOrdersList.add(supplierOrder);
					break;
				}
			}
		}
		
		return supplierOrdersList;*/
	}
	
	/**
	 * <pre>
	 * fetch supplier order product details which belongs only given supplier Id and logged user company
	 * @param supplierOrderId
	 * @return SupplierOrderDetails list
	 * </pre>
	 */
	@Transactional 
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId)
	{
		String hql2="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrderId+"'"+
					" and supplierOrder.company.companyId="+getSessionSelectedCompaniesIds()+
					" and supplierOrder.branch.branchId="+getSessionSelectedBranchIds();;
		Query query2= sessionFactory.getCurrentSession().createQuery(hql2);
		List<SupplierOrderDetails> supplierOrderDetailsList=(List<SupplierOrderDetails>)query2.list();
		if(supplierOrderDetailsList.isEmpty())
		{
			return null;			
		}
		
		return supplierOrderDetailsList;
	}
	/**
	 * <pre>
	 * fetch order list by employeeId, payStatus, range
	 * 
	 * pay status :- 
	 * Current : todays date
	 * Pending : before todays date
	 * Future : after todays date
	 * 
	 * get order details which belongs to logged user areas and company
	 * and order status need Delivered and Delivery Pending
	 * 
	 * @param paymentListRequest
	 * @return OrderDetailsForPayment list
	 * </pre>
	 */
	@Transactional
	public List<OrderDetailsForPayment> fetchOrderListForPayment(PaymentListRequest paymentListRequest){
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(paymentListRequest.getEmployeeId());
		
		List<OrderDetailsForPayment> orderDetailsForPaymentsList=new ArrayList<>();
		
		List<Long> areaIds = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        areaIds.add(area.getAreaId());
	    }
	    SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	    Calendar cal=Calendar.getInstance();
		
	    String hql="";
	    if(paymentListRequest.getPayStatus().equals(Constants.PAY_STATUS_CURRENT_DATE_PAYMENT))
	    {
	    	hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) = date(CURRENT_DATE())";
	    }
	    else if(paymentListRequest.getPayStatus().equals(Constants.PAY_STATUS_PENDING_DATE_PAYMENT))
	    {
	    	hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) < date(CURRENT_DATE())";
	    }else{
	    	if(paymentListRequest.getRange().equals("future7days")){
	    		cal.add(Calendar.DAY_OF_MONTH, 7);
	    		hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) <= '"+dateFormat.format(cal.getTime())+"' and date(orderDetailsPaymentTakeDatetime) > date(CURRENT_DATE())";
	    	}else if(paymentListRequest.getRange().equals("future15days")){
	    		cal.add(Calendar.DAY_OF_MONTH, 15);
	    		hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) <= '"+dateFormat.format(cal.getTime())+"' and date(orderDetailsPaymentTakeDatetime) > date(CURRENT_DATE())";
	    	}else if(paymentListRequest.getRange().equals("futureMonth")){
	    		cal.add(Calendar.MONTH, 1);	    		
	    		hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) <= '"+dateFormat.format(cal.getTime())+"' and date(orderDetailsPaymentTakeDatetime) > date(CURRENT_DATE())";
	    	}else if(paymentListRequest.getRange().equals("future3Months")){
	    		cal.add(Calendar.MONTH, 3);	    		
	    		hql="from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) <= '"+dateFormat.format(cal.getTime())+"' and date(orderDetailsPaymentTakeDatetime) > date(CURRENT_DATE())";
	    	}
	    }
	    
	    hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
	    	 " and badDebtsStatus=false"+
	    	 " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
             " and businessName.area.areaId in (:ids) and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"+	
	    	 " order by orderDetailsPaymentTakeDatetime";
	    
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", areaIds);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;	
		}
		
		//PaymentDAOImpl paymentDAO=new PaymentDAOImpl(sessionFactory);
		
		for(OrderDetails orderDetails : list)
		{
			List<Payment> paymentList=paymentDAO.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
			double amountDue=0, issuedTotalAmountWithTax=0;
			
			if(paymentList==null)
			{
				amountDue=orderDetails.getIssuedTotalAmountWithTax();					
			}
			else
			{
				amountDue=paymentList.get(0).getDueAmount();
			}
			
			issuedTotalAmountWithTax=orderDetails.getIssuedTotalAmountWithTax();
			
			orderDetailsForPaymentsList.add(new OrderDetailsForPayment(orderDetails.getOrderId(), orderDetails.getBusinessName(), amountDue, issuedTotalAmountWithTax,orderDetails.getOrderDetailsPaymentTakeDatetime()));
		}
		
		return orderDetailsForPaymentsList;
	}
	
	
	/*@Override
	public List<ReturnOrderResponse> makeProductsImageNullOrderDetailsWithOrderProductDetailsList(List<ReturnOrderResponse> returnOrderResponsesList) {
		List<ReturnOrderResponse> returnOrderResponsesList2=new ArrayList<>();
		List<OrderProductDetails> orderProductDetailsList2=new ArrayList<>();
		
		for(ReturnOrderResponse returnOrderResponse : returnOrderResponsesList)
		{
			List<OrderProductDetails> orderProductDetailsList=returnOrderResponse.getOrderProductDetails();// getting list of orderProductDetails
			for(OrderProductDetails orderProductDetails:orderProductDetailsList)
			{
				orderProductDetails.getProduct().setProductImage(null); // using this forEach loop to iterate the productList and removing the Logo of each
				orderProductDetails.getProduct().getProduct().setProductImage(null);
				orderProductDetailsList2.add(orderProductDetails);
			}
			returnOrderResponse.setOrderProductDetails(orderProductDetailsList2);
			returnOrderResponsesList2.add(returnOrderResponse);
		}        
        
			
        	return returnOrderResponsesList2;
        	
        }*/
	/**
	 * <pre>
	 * fetch order details by business id and date range which order is delivered
	 * @param businessNameId
	 * @param fromDate
	 * @param toDate
	 * @return OrderDetails list
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails>  fetchOrderDetailBybusinessNameIdAndDateRange(String businessNameId,
			String fromDate, String toDate) {
		// TODO Auto-generated method stub
		String hql="from OrderDetails where businessName.businessNameId='"+businessNameId +"' and date(orderDetailsAddedDatetime) >= '"+fromDate+"' And date(orderDetailsAddedDatetime) <= '"+
				toDate+"'  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')"+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);  // Query to fetch orderDetails by DateRange
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty()){
			return null;
		}
		
		return list;
	}
	/**
	 * <pre>
	 * get order details report by business which come under logger user areas and company
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * </pre>
	 */
	@Transactional
	public List<CustomerReportResponse> fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(long employeeId, String fromDate,
			String toDate, String range) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<CustomerReportResponse>  customerReportResponseList=new ArrayList<>();
		
		List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(employeeId);
		
		String areaIds="";
		for(EmployeeAreaList employeeAreaList: employeeAreaLists)
		{
			areaIds+=employeeAreaList.getArea().getAreaId()+",";
		}
		areaIds=areaIds.substring(0, areaIds.length()-1);
		
		List<BusinessName> businessNameList=businessNameDAO.fetchBusinessNameByAreaIds(areaIds); 
		if(businessNameList!=null)
		{
			for(BusinessName businessName:businessNameList)
			{
					Calendar cal = Calendar.getInstance(); 
					
					// here we use filter for OrderDetail by fromDate,toDate And Range
					if(range.equals("range")){
					hql="from OrderDetails where (date(orderDetailsAddedDatetime)>='"+fromDate+"' And date(orderDetailsAddedDatetime)<='"+toDate+"') and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'  and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'  and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
				
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'  and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}else if(range.equals("pickDate")){
						hql="from OrderDetails where date(orderDetailsAddedDatetime)='"+fromDate +"'  and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderDetails where businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}
				
					long noOfOrder=0;
					double totalAmt=0;
					
					hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
							" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
							" order by orderDetailsAddedDatetime desc";
					//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
					
					query=sessionFactory.getCurrentSession().createQuery(hql);
					@SuppressWarnings("unchecked")
					List<OrderDetails> orderDetailList=(List<OrderDetails>)query.list();
					if(orderDetailList.isEmpty()){
						noOfOrder=0;
						customerReportResponseList.add(new CustomerReportResponse(businessName.getBusinessNameId(), businessName.getShopName(), businessName.getArea().getName(), noOfOrder, totalAmt));				
					}
					else
					{
						for(OrderDetails orderDetails:orderDetailList)
						{
							//totalAmt+=orderDetails.getIssuedTotalAmountWithTax();
							totalAmt+=orderDetails.getTotalAmountWithTax();
						}
						
						noOfOrder=orderDetailList.size();
						customerReportResponseList.add(new CustomerReportResponse(businessName.getBusinessNameId(), businessName.getShopName(), businessName.getArea().getName(), noOfOrder, totalAmt));
					}
			}
		}
			
			return customerReportResponseList;		
	}
	/**
	 * <pre>
	 * get order details list by business Id which come under logger user company
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails> fetchOrderDetailForCustomerReportByBusinessNameId(
			 String businessNameId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		hql="from OrderDetails where businessName.businessNameId= '"+businessNameId+"'"+
			" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			" order by orderDetailsAddedDatetime desc";
		    //" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> orderDetailList=(List<OrderDetails>)query.list();
		
		if(orderDetailList.isEmpty()){
			return null;
		}
		return orderDetailList;
	}
	/**
	 * <pre>
	 * get order details report by business which come under logger user areas and company
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * </pre>
	 */
	@Transactional
	public List<CustomerReportResponse> fetchTotalCollectionByDateRangeAndEmployeeId(long employeeId,
			String fromDate, String toDate,  String range) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query;
		
		List<CustomerReportResponse>  customerReportResponseList=new ArrayList<>();
		
		List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(employeeId);
		
		String areaIds="";
		for(EmployeeAreaList employeeAreaList: employeeAreaLists)
		{
			areaIds+=employeeAreaList.getArea().getAreaId()+",";
		}
		areaIds=areaIds.substring(0, areaIds.length()-1);
		
		List<BusinessName> businessNameList=businessNameDAO.fetchBusinessNameByAreaIds(areaIds); 
		if(businessNameList!=null){
			for(BusinessName businessName:businessNameList)
			{
					Calendar cal = Calendar.getInstance(); 
					
					// here we use filter for OrderDetail by fromDate,toDate And Range
					/*if(range.equals("range")){
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +"  and businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +"   and businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
				
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +"   and businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
					}else if(range.equals("pickDate")){
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +"   and businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderDetails where employeeSM.employeeId=" +employeeId +"  and businessName.businessNameId='"+businessName.getBusinessNameId()+"'";
					}
					else if(range.equals("currentDate"))
					{*/
						hql="from OrderDetails where 1=1 "+//employeeSM.employeeId=" +employeeId +
						    " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
						    " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
							" AND businessName.businessNameId='"+businessName.getBusinessNameId()+"'"+
							" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"+
							" order by orderDetailsAddedDatetime desc";
							//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
					//}
					
					double totalAmtWithTax=0;
					double paidAmount=0;
					double dueAmount=0;
					
					query=sessionFactory.getCurrentSession().createQuery(hql);
					@SuppressWarnings("unchecked")
					List<OrderDetails> orderDetailList=(List<OrderDetails>)query.list();
					if(orderDetailList.isEmpty()){
						
						customerReportResponseList.add(new CustomerReportResponse(businessName.getBusinessNameId(), businessName.getShopName(), businessName.getArea().getName(), 0, totalAmtWithTax));				
					}
					else
					{
						for(OrderDetails orderDetails:orderDetailList)
						{
							totalAmtWithTax+=orderDetails.getIssuedTotalAmountWithTax();
							
							List<Payment> paymentList=paymentDAO.fetchPaymentListbyRange(orderDetails.getOrderId(), fromDate, toDate, range);
							if(paymentList==null)
							{
								///dueAmount+=orderDetails.getIssuedTotalAmountWithTax();
								paidAmount+=0;
							}
							else
							{
								//dueAmount+=paymentList.get(0).getDueAmount();
								
								for(Payment payment: paymentList)
								{
									paidAmount+=payment.getPaidAmount();
								}
							}
							
							//paidAmount+=totalAmtWithTax-dueAmount;
						}
						if(paidAmount!=0){
							customerReportResponseList.add(new CustomerReportResponse(businessName.getBusinessNameId(), businessName.getShopName(), businessName.getArea().getName(), orderDetailList.size(), paidAmount));
						}
					}
			}
		}
		return customerReportResponseList;

	}
	/**
	 * <pre>
	 * fetch todays date booked order by logged user areas and company  
	 * @param areaId
	 * @param employeeId
	 * @return {@link OrderDetailsList} list
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchTodaysPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
	{
		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.packedDate) = date(CURRENT_DATE()) "+
					" and e.orderDetails.businessName.area.areaId="+areaId+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_PACKED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by e.orderDetails.orderDetailsAddedDatetime desc";
					//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
		}
		else
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.packedDate) = date(CURRENT_DATE()) "+
					//" and businessName.area.areaId in (:ids) "+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_PACKED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by e.orderDetails.orderDetailsAddedDatetime desc";
					//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			//query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch before todays dates booked order by logged user areas and company  
	 * @param areaId
	 * @param employeeId
	 * @return {@link OrderDetailsList} list
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchPendingPackedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
	{
		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.packedDate) < date(CURRENT_DATE()) "+
					" and e.orderDetails.businessName.area.areaId="+areaId+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_PACKED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by e.orderDetails.orderDetailsAddedDatetime desc";
					//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
		}
		else
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.packedDate) < date(CURRENT_DATE()) "+
					//" and businessName.area.areaId in (:ids) "+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_PACKED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by e.orderDetails.orderDetailsAddedDatetime desc";
					//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			//query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch todays date issued order by logged user areas and company  
	 * @param areaId
	 * @param employeeId
	 * @return {@link OrderDetailsList} list
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
	{
		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.issueDate) = date(CURRENT_DATE()) "+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.businessName.area.areaId="+areaId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'"+
					" and  e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by  e.orderDetails.orderDetailsAddedDatetime desc";
					//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
		}
		else
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.issueDate) = date(CURRENT_DATE()) "+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by  e.orderDetails.orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			//query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * fetch before todays date issued order by logged user areas and company  
	 * @param areaId
	 * @param employeeId
	 * @return {@link OrderDetailsList} list
	 * </pre>
	 */
	@Transactional
	public OrderDetailsList fetchPendingIssuedOrderListByAreaIdAndEmployeeId(long areaId,long employeeId)
	{
		String hql;
		Query query;
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<AreaModel> areaList=employeeDetailsDAO.fetchAreaModelListByEmployeeId(employeeId);
		orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<AreaModel> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	AreaModel area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
		if(areaId!=0)
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.issueDate) < date(CURRENT_DATE()) "+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.businessName.area.areaId="+areaId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by  e.orderDetails.orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
		}
		else
		{
			hql="select e.orderDetails from OrderProductIssueDetails e where date(e.orderDetails.issueDate) < date(CURRENT_DATE())"+
					" and e.employeeDB.employeeId="+employeeId+
					" and e.orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'"+
					" and e.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" and e.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" order by  e.orderDetails.orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			//query.setParameterList("ids", pnjId);
		}
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			orderDetailsList.setOrderDetailsList(null);
		}
		else
		{
			List<FetchOrderDetailsModel> fetchOrderDetailsModelList=new ArrayList<>();
			for(OrderDetails orderDetails: list){
				FetchOrderDetailsModel fetchOrderDetailsModel=new FetchOrderDetailsModel();
				fetchOrderDetailsModel.setOrderId(orderDetails.getOrderId());
				fetchOrderDetailsModel.setAreaName(orderDetails.getBusinessName().getArea().getName());
				fetchOrderDetailsModel.setOrderDate(orderDetails.getOrderDetailsAddedDatetime());
				fetchOrderDetailsModel.setPurchasedQuantity(orderDetails.getTotalQuantity());
				fetchOrderDetailsModel.setShopName(orderDetails.getBusinessName().getShopName());
				fetchOrderDetailsModel.setOrderStatus(orderDetails.getOrderStatus().getStatus());
				fetchOrderDetailsModel.setAreaId(orderDetails.getBusinessName().getArea().getAreaId());
				fetchOrderDetailsModel.setBusinessNameId(orderDetails.getBusinessName().getBusinessNameId());
				fetchOrderDetailsModel.setOwnerName(orderDetails.getBusinessName().getOwnerName());
				fetchOrderDetailsModel.setIssuedQuantity(orderDetails.getIssuedTotalQuantity());
				fetchOrderDetailsModel.setPackedDate(orderDetails.getPackedDate());
				fetchOrderDetailsModel.setIssuedDate(orderDetails.getIssueDate());
				fetchOrderDetailsModel.setTotalAmount(orderDetails.getTotalAmount());
				fetchOrderDetailsModel.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
				fetchOrderDetailsModel.setIssuedTotalAmount(orderDetails.getIssuedTotalAmount());
				fetchOrderDetailsModel.setIssuedTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
				fetchOrderDetailsModel.setConfirmTotalAmount(orderDetails.getConfirmTotalAmount());
				fetchOrderDetailsModel.setConfirmTotalAmountWithTax(orderDetails.getConfirmTotalAmountWithTax());
				fetchOrderDetailsModel.setCancelEmployeeDepartment((orderDetails.getEmployeeIdCancel()!=null)?orderDetails.getEmployeeIdCancel().getDepartment().getName():null);
				fetchOrderDetailsModel.setMobileNo(orderDetails.getBusinessName().getContact().getMobileNumber());
				fetchOrderDetailsModelList.add(fetchOrderDetailsModel);
			}
			orderDetailsList.setOrderDetailsList(fetchOrderDetailsModelList);
		}
		return orderDetailsList;
	}
	/**
	 * <pre>
	 * reissue define
	 * reduce product current quantity basis of which give reissue quantity
	 * reissue same quantity also define in damage and damage recovery
	 * send notification to assigned delivery boy / salesman 
	 * @param orderReIssueRequest
	 * @return success / failed
	 * </pre>
	 */
	@Transactional
	public String reIssueOrderDetails(OrderReIssueRequest orderReIssueRequest)
	{
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			ReIssueOrderDetails reIssueOrderDetails=orderReIssueRequest.getReIssueOrderDetails();
			
		/*	Transportation transportation=transportationDAO.fetchById(reIssueOrderDetails.getTransportation().getId()+"");
			reIssueOrderDetails.setTransportation(transportation);*/
			
			ReturnOrderProduct returnOrderProduct=returnOrderDAO.fetchReturnOrderForGKReportByReturnOrderProductId(orderReIssueRequest.getReIssueOrderDetails().getReturnOrderProduct().getReturnOrderProductId());
			reIssueOrderDetails.setReturnOrderProduct(returnOrderProduct);
			
			reIssueOrderDetails.setReIssueDate(new Date());
			try {
				reIssueOrderDetails.setReIssueDeliveryDate(simpleDateFormat.parse(orderReIssueRequest.getReIssueDeliveryDate()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			reIssueOrderDetails.setStatus(Constants.RETURN_ORDER_PENDING);
			sessionFactory.getCurrentSession().save(reIssueOrderDetails);
			
			List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderDAO.fetchReturnOrderProductDetailsByReturnOrderDetailsId(reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId()); 
			List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=orderReIssueRequest.getReIssueOrderProductDetailsList();				
			for(ReIssueOrderProductDetails reIssueOrderProductDetails : reIssueOrderProductDetailsList)
			{
				reIssueOrderProductDetails.setReIssueOrderDetails(reIssueOrderDetails);
				sessionFactory.getCurrentSession().save(reIssueOrderProductDetails);
				
				reIssueOrderProductDetails.getProduct().getProduct().setDamageQuantity
						(
						reIssueOrderProductDetails.getProduct().getProduct().getDamageQuantity()+
						reIssueOrderProductDetails.getReIssueQuantity()
						);
				product=productDAO.fetchProductForWebApp(reIssueOrderProductDetails.getProduct().getProduct().getProductId());
				product.setCurrentQuantity(product.getCurrentQuantity()-reIssueOrderProductDetails.getReIssueQuantity());
				
				productDAO.Update(product);
				productDAO.updateDailyStockExchange(reIssueOrderProductDetails.getProduct().getProduct().getProductId(), reIssueOrderProductDetails.getReIssueQuantity(), false);
				
				if(reIssueOrderProductDetails.getReIssueQuantity()>0){
					
					//here damage recovery save or update DamageRecoveryDayWise table using damage quantity
					productDAO.saveUpdateDamageRecoveryMonthWise(
							reIssueOrderProductDetails.getProduct().getProduct().getProductId(),
							reIssueOrderProductDetails.getReIssueQuantity());
					
					String reason="";
					for(ReturnOrderProductDetails returnOrderProductDetails:  returnOrderProductDetailsList ){
						if(returnOrderProductDetails.getProduct().getProduct().getProductId()==reIssueOrderProductDetails.getProduct().getProduct().getProductId()){
							reason=returnOrderProductDetails.getReason();
						}
					}
					//orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId());
					EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnOrderProduct.getEmployee().getEmployeeId());
					//damagedefine for report
					Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
					DamageDefine damageDefine=new DamageDefine( reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId(),
																reIssueOrderProductDetails.getProduct().getProduct(), 
																employeeDetails.getName(), 
																employeeDetails.getEmployee().getDepartment().getName(), 
																reIssueOrderProductDetails.getReIssueQuantity(),
																new Date(), 
																reason,
																branch);
					productDAO.saveDamageDefine(damageDefine);
				}
				//here update order product current quantity			
				updateOrderUsedProductCurrentQuantity();
			}
			
			////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			EmployeeDetails employeeDetailsDB=employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			EmployeeDetails employeeDetailsGK=employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeGK().getEmployeeId());

			//ReturnOrderDAOImpl returnOrderDAO=new ReturnOrderDAOImpl(sessionFactory);
			//ReturnOrderProduct returnOrderProduct=returnOrderDAO.fetchReturnOrderForGKReportByReturnOrderProductId(reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId());
			returnOrderProduct.setReIssueStatus(Constants.RETURN_ORDER_COMPLETE);
			
			returnOrderProduct=(ReturnOrderProduct)sessionFactory.getCurrentSession().merge(returnOrderProduct);
			sessionFactory.getCurrentSession().update(returnOrderProduct);
			
			Notification.sendNotificationToDeliveryBoyForOrder(employeeDetailsDB.getToken(),employeeDetailsGK.getEmployeeDetailsGenId() , returnOrderProduct.getOrderDetails().getBusinessName().getShopName(), returnOrderProduct.getOrderDetails().getOrderId(),"OPEN_REPLACEMENT_ORDER");
			
			
			
			return "Success";
		
		
	}
	
	/*@Transactional
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsList(String orderId)
	{
		String hql="from ReIssueOrderProductDetails where reIssueOrderDetails.returnOrderProduct.orderDetails.orderId='"+orderId+"'"+
				" and reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReIssueOrderProductDetails> list=(List<ReIssueOrderProductDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;		
	}*/
	/**
	 * <pre>
	 * fetch order product issued details by order id
	 * @param orderId
	 * @return OrderProductIssueDetails
	 * </pre>
	 */
	@Transactional
	public OrderProductIssueDetails fetchOrderProductIssueListForIssueReportByOrderId(
			String orderId) {
		String hql="";
		Query query;
		
		hql="from OrderProductIssueDetails where orderDetails.orderId='"+ orderId+"'"+
				" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductIssueDetails> orderProductIssueDetailsList=(List<OrderProductIssueDetails>)query.list(); 
		
		if(orderProductIssueDetailsList.isEmpty()){
			return null;
		}
		return orderProductIssueDetailsList.get(0);		
	}
	/**
	 * <pre>
	 * fetch order details by range,startDate,endDate,logged user areas(compare with business area) and company
	 * and order status (Delivered,Delivery Pending,Both)
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * @return OrderProductIssueDetails list
	 * </pre>
	 */
	@Transactional
	public List<OrderProductIssueDetails> fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus(long employeeId,
			String fromDate, String toDate, String range, String orderStatus) {
		// TODO Auto-generated method stub
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
		
		/*List<Long> pnjId = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	Area area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }*/
		
		if(orderStatus.equals("Status"))
		{
					if(range.equals("range"))
					{
						hql="from OrderProductIssueDetails where (date(orderDetails.issueDate)>='"+fromDate+"' And date(orderDetails.issueDate)<='"+toDate+"')  And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";						
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";						
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
						
					}else if(range.equals("pickDate")){
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)='"+fromDate +"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";	
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderProductIssueDetails where orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";	 
					}
					else if(range.equals("currentDate"))
					{
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)=date(CURRENT_DATE())  And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";		
					}
		}		
		else if(orderStatus.equals(Constants.ORDER_STATUS_DELIVERED))
		{
					if(range.equals("range"))
					{
						hql="from OrderProductIssueDetails where (date(orderDetails.issueDate)>='"+fromDate+"' And date(orderDetails.issueDate)<='"+toDate+"') And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"'  And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
						
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
						
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
						
					}else if(range.equals("pickDate")){
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)='"+fromDate +"'And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";	
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>=date(CURRENT_DATE()) And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";	 
					}
					else if(range.equals("currentDate"))
					{
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)=date(CURRENT_DATE()) And orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";		
					}
		}
		else if(orderStatus.equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
		{
					if(range.equals("range"))
					{
						hql="from OrderProductIssueDetails where (date(orderDetails.issueDate)>='"+fromDate+"' And date(orderDetails.issueDate)<='"+toDate+"') And orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
					}
					else if(range.equals("last7days")){
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"'And orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
						
					}else if(range.equals("last1month")){
						cal.add(Calendar.MONTH, -1);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
						
					}else if(range.equals("last3months")){
						cal.add(Calendar.MONTH, -3);
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)>='"+dateFormat.format(cal.getTime())+"' And orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
						
					}else if(range.equals("pickDate")){
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)='"+fromDate +"' And orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";	
					}
					else if(range.equals("viewAll"))
					{
						hql="from OrderProductIssueDetails where orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";	 
					}
					else if(range.equals("currentDate"))
					{
						hql="from OrderProductIssueDetails where date(orderDetails.issueDate)=date(CURRENT_DATE()) And  orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
					}
		}
	
		hql+=" and employeeDB.employeeId="+employeeId+
			 " and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			 " and orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			 " order by orderDetails.issueDate desc";
		//hql+=" and orderDetails.businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetails.issueDate desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		//query.setParameterList("ids", pnjId);
		List<OrderProductIssueDetails> orderDeliveryList=(List<OrderProductIssueDetails>)query.list();
		
		if(orderDeliveryList.isEmpty()){
			return null;
		}
		
		return orderDeliveryList;
	}

	/**
	 * <pre>
	 * fetch order details list by range,startDate,endDate and logged employee areas wise
	 * @param employeeId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails> fetchOrderDetailsByDateRangeAndEmpId(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
		
		List<Long> pnjId = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	Area area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
	
		if(range.equals("range"))
		{
			hql="from OrderDetails where (date(orderDetailsAddedDatetime)>='"+fromDate+"' And date(orderDetailsAddedDatetime)<='"+toDate+"')";
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			
		}else if(range.equals("last1month")){
			cal.add(Calendar.MONTH, -1);
			hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			
		}else if(range.equals("last3months")){
			cal.add(Calendar.MONTH, -3);
			hql="from OrderDetails where date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			
		}else if(range.equals("pickDate")){
			hql="from OrderDetails where date(orderDetailsAddedDatetime)='"+fromDate +"'";	
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderDetails where 1=1 " ;	 
		}
		else if(range.equals("currentDate"))
		{
			hql="from OrderDetails where  date(orderDetailsAddedDatetime)>=date(CURRENT_DATE()) ";		
		}
				
		hql+=" and businessName.area.areaId in (:ids) "+
				 " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				 " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				 " order by orderDetailsAddedDatetime desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", pnjId);
		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		if(orderDetailsList.isEmpty()){
			return null;	
		}
		return orderDetailsList;
	}

	/**
	 * <pre>
	 * fetch replacement issued orders by employee id and status(Complete,Pending)
	 * @return ReIssueOrderDetails list
	 * </pre>
	 */
	@Transactional
	public List<ReIssueOrderDetails> fetchReplacementIssuedOrdersByEmployeeId(long employeeId,String status)
	{	
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
		List<Long> areaListIdArray = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) {
	    	Area area = iterator.next();
	        areaListIdArray.add(area.getAreaId());
	    }
	   
		
		String hql="";
		if(status.equals(Constants.PAY_STATUS_CURRENT_DATE_PAYMENT))
		{
			hql="from ReIssueOrderDetails e where e.employeeSMDB.employeeId="+employeeId+" and e.status='"+Constants.RETURN_ORDER_PENDING+"' and date(e.reIssueDate)=date(CURRENT_DATE())";
		}
		else
		{
			hql="from ReIssueOrderDetails e where e.employeeSMDB.employeeId="+employeeId+" and e.status='"+Constants.RETURN_ORDER_PENDING+"' and date(e.reIssueDate)<date(CURRENT_DATE())";
		}
		hql+=" and e.returnOrderProduct.orderDetails.businessName.area.areaId in (:ids) "+
				 " and e.returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				 " and e.returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by e.reIssueDate desc";
		//hql+=" and returnOrderProduct.orderDetails.businessName.area.areaId in ("+getSessionSelectedIds()+") order by e.reIssueDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		 query.setParameterList("ids", areaListIdArray);
		List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
		if(reIssueOrderDetailsList.isEmpty())
		{
			return null;
		}		
		return reIssueOrderDetailsList;
	}
	/**
	 * <pre>
	 * order confirm quantity,amount same as issued quantity ,amount
	 * reissue status Completed
	 * Order status Delivered 
	 * @param reIssueDelivered
	 * @return success / failed
	 * </pre>
	 */
	@Transactional
	public String doneReIssueAndOrderStatusDelivered(ReIssueDelivered reIssueDelivered) 
	{
		try { 
			ReIssueOrderDetails reIssueOrderDetails=fetchReIssueOrderDetailsByReIssueOrderDetailsId(reIssueDelivered.getReIssueOrderId());
			reIssueOrderDetails.setStatus(Constants.RETURN_ORDER_COMPLETE);
			reIssueOrderDetails.setReIssueDeliveredDate(new Date());
			
			orderDetails=fetchOrderDetailsByOrderId(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId());
			orderStatus=fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED);
			orderDetails.setOrderStatus(orderStatus);
			orderDetails.setDeliveryDate(new Date());
			orderDetails.setConfirmTotalQuantity(orderDetails.getIssuedTotalQuantity());
			orderDetails.setConfirmTotalAmount(orderDetails.getIssuedTotalAmount());
			orderDetails.setConfirmTotalAmountWithTax(orderDetails.getIssuedTotalAmountWithTax());
			
			orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
			sessionFactory.getCurrentSession().update(orderDetails);
			
			List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId());
			for(OrderProductDetails orderProductDetails: orderProductDetailsList)
			{
				orderProductDetails.setConfirmAmount(orderProductDetails.getIssueAmount());
				orderProductDetails.setConfirmQuantity(orderProductDetails.getIssuedQuantity());
				
				orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
				sessionFactory.getCurrentSession().update(orderProductDetails);
			}
			
			deliveredProduct.setOrderDetails(orderDetails);
			//ImageConvertor imageConvertor=new ImageConvertor(sessionFactory);
			deliveredProduct.setOrderReceiverSignature(imageConvertor.convertStringToBlob(reIssueDelivered.getSignBase64()));
			deliveredProduct.setStatus(Constants.ORDER_STATUS_DELIVERED_PENDING);
			sessionFactory.getCurrentSession().save(deliveredProduct);
			
			return "Success";
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}		
		
		
	}
	
	@Transactional
	public ReIssueOrderDetails fetchReIssueOrderDetailsByReIssueOrderDetailsId(long reIssueOrderId)
	{
		String hql="from ReIssueOrderDetails where reIssueOrderId="+reIssueOrderId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
		if(reIssueOrderDetailsList.isEmpty())
		{
			return null;
		}
		return reIssueOrderDetailsList.get(0);
	}
	
	@Transactional
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(long reIssueOrderId)
	{
		String hql="from ReIssueOrderProductDetails where reIssueOrderDetails.reIssueOrderId="+reIssueOrderId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=(List<ReIssueOrderProductDetails>)query.list();
		if(reIssueOrderProductDetailsList.isEmpty())
		{
			return null;
		}
		return reIssueOrderProductDetailsList;
	}
	
	
	@Transactional
	public ReIssueOrderProductDetailsListModel fetchOrderDetailsForReIssueDelivedByOrderId(long reIssueOrderId)
	{
		ReIssueOrderProductDetailsListModel reIssueOrderProductDetailsListModel=new ReIssueOrderProductDetailsListModel();
		ReIssueOrderDetails reIssueOrderDetails=fetchReIssueOrderDetailsByReIssueOrderDetailsId(reIssueOrderId);
		//EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());
		reIssueOrderProductDetailsListModel.setReIssueOrderDetailsModel(new ReIssueOrderDetailsModel(
				reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getOrderId(), 
				reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductId(), 
				reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getShopName(), 
				reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getAddress(), 
				reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getBusinessName().getContact().getMobileNumber(), 
				reIssueOrderDetails.getReturnOrderProduct().getReturnOrderProductDatetime(), 
				reIssueOrderDetails.getReIssueOrderId(), 
				reIssueOrderDetails.getTotalQuantity(), 
				reIssueOrderDetails.getReturnOrderProduct().getTotalQuantity()));
		
		List<ReIssueOrderProductDetailsReport> reIssueOrderProductDetailsList=new ArrayList<>();

		List<ReIssueOrderProductDetails> reIssueOrderProductDetailList=fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(reIssueOrderId);
		for(ReIssueOrderProductDetails reIssueOrderProductDetails : reIssueOrderProductDetailList){
			reIssueOrderProductDetailsList.add(new ReIssueOrderProductDetailsReport(
					reIssueOrderProductDetails.getProduct().getProductName(), 
					reIssueOrderProductDetails.getReturnQuantity(), 
					reIssueOrderProductDetails.getReIssueQuantity(),
					reIssueOrderProductDetails.getType()));
		}
		reIssueOrderProductDetailsListModel.setReIssueOrderLists(reIssueOrderProductDetailsList);
		
		return reIssueOrderProductDetailsListModel;
	}
	/**
	 * <pre>
	 * find sales report by sales man in order details list except order status Booked and Cancelled
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return ChartDetailsResponse
	 * </pre>
	 */
	@Transactional 
	public ChartDetailsResponse fetchTopFiveSalesManByIssuedSale(String range,String startDate,String endDate)
	{		
		String hql="from Employee emp where emp.department.name='"+Constants.SALESMAN_DEPT_NAME+"' "+
					" and company.companyId="+getSessionSelectedCompaniesIds();
				
				/*" and emp.employeeId in (select empDet.employee.employeeId from EmployeeDetails empDet where empDet.employeeDetailsId in "+
				"(select empArea.employeeDetails.employeeDetailsId from EmployeeAreaList empArea where empArea.area.areaId in ("+getSessionSelectedIds()+"))"
				+ ")";*/
				
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Employee> employeeList=(List<Employee>)query.list();
		
		Map<Employee,Double> employeeSaleAmount=new HashMap<>();
		
		if (range.equals("currentMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(packedDate) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
		}
		else if (range.equals("lastMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast3MonthLastDate()+"')";
		}
		else if (range.equals("last6Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast6MonthLastDate()+"')";
		}
		else if (range.equals("range")) {
			hql="from OrderDetails where (date(packedDate) >= '"+startDate+"' and date(packedDate) <= '"+endDate+"')";
		}
		
		for(Employee employee: employeeList)
		{	
					
			hql+= " and employeeSM.employeeId="+employee.getEmployeeId()+" and orderStatus.status not in ('"+Constants.ORDER_STATUS_BOOKED+"','"+Constants.ORDER_STATUS_CANCELED+"')"+
					" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
					" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
			
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
			if(orderDetailsList.isEmpty())
			{
				break;
			}
			double totalSale=0;
			for(OrderDetails orderDetails: orderDetailsList)
			{
				totalSale+=orderDetails.getIssuedTotalAmountWithTax();
			}
			employeeSaleAmount.put(employee, totalSale);
		}
		
		Set<Entry<Employee, Double>> set = employeeSaleAmount.entrySet();
        List<Entry<Employee, Double>> list = new ArrayList<Entry<Employee, Double>>(set);
        Collections.sort( list, new Comparator<Map.Entry<Employee, Double>>()
        {
            public int compare( Map.Entry<Employee, Double> o1, Map.Entry<Employee, Double> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        });
		
        double totalEmployeesSale=0;
        int count=0;
        for(Map.Entry<Employee, Double> entry:list)
        {
        	count++;
        	totalEmployeesSale+=entry.getValue();
        	if(count==5)
        	{
        		break;
        	}
        }        
        
        String salesManName1="";
		float salesManPercentage1=0;
		String salesManName2="";
		float salesManPercentage2=0;
		String salesManName3="";
		float salesManPercentage3=0;
		String salesManName4="";
		float salesManPercentage4=0;
		String salesManName5="";
		float salesManPercentage5=0;
		
		//EmployeeDAOImpl employeeDAO=new EmployeeDAOImpl(sessionFactory);
		
		count=0;
        for(Map.Entry<Employee, Double> entry : list){

        	count++;
			if(count==1)
			{
				salesManName1=employeeDAO.getEmployeeDetails(entry.getKey().getEmployeeId()).getName();
				salesManPercentage1=(float) ((entry.getValue()/totalEmployeesSale)*100);
			}
			else if(count==2)
			{
				salesManName2=employeeDAO.getEmployeeDetails(entry.getKey().getEmployeeId()).getName();
				salesManPercentage2=(float) ((entry.getValue()/totalEmployeesSale)*100);
			}
			else if(count==3)
			{
				salesManName3=employeeDAO.getEmployeeDetails(entry.getKey().getEmployeeId()).getName();
				salesManPercentage3=(float) ((entry.getValue()/totalEmployeesSale)*100);
			}
			else if(count==4)
			{
				salesManName4=employeeDAO.getEmployeeDetails(entry.getKey().getEmployeeId()).getName();
				salesManPercentage4=(float) ((entry.getValue()/totalEmployeesSale)*100);
			}
			else if(count==5)
			{
				salesManName5=employeeDAO.getEmployeeDetails(entry.getKey().getEmployeeId()).getName();
				salesManPercentage5=(float) ((entry.getValue()/totalEmployeesSale)*100);
			}
			else
			{
				break;
			}
        }
        ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse();
		 chartDetailsResponse.setSalesManChartDetailsResponse(salesManName1, salesManPercentage1, salesManName2, salesManPercentage2, salesManName3, salesManPercentage3, salesManName4, salesManPercentage4, salesManName5, salesManPercentage5);
		 return chartDetailsResponse;
	}
	
	/**
	 * <pre>
	 * filter order details by payment date
	 * iterate orders and find orderId,paid amount,balance amount,total amount and find status of payment(Paid,PartiallyPaid,UnPaid)
	 * @param orderDetailByBusinessNameIdEmployeeIdRequest
	 * </pre>
	 */
	@Transactional
	public List<OrderDetailsPaymentList> fetchOrderDetailForTotalCollectionReportByBusinessNameId(OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		
		String range=orderDetailByBusinessNameIdEmployeeIdRequest.getRange();
		String startDate=orderDetailByBusinessNameIdEmployeeIdRequest.getStartDate();
		String endDate=orderDetailByBusinessNameIdEmployeeIdRequest.getEndDate();
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		/*hql="from OrderDetails where employeeSM.employeeId=" +orderDetailByBusinessNameIdEmployeeIdRequest.getEmployeeId() +
			    " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" AND businessName.businessNameId='"+orderDetailByBusinessNameIdEmployeeIdRequest.getBusinessNameId()+"'"+
				" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"') ";
				
		
		if(range.equals("range"))
		{
			hql+=" and (date(orderDetailsAddedDatetime)>='"+startDate+"' And date(orderDetailsAddedDatetime)<='"+endDate+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql+=" and date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("last1Month"))
		{
			cal.add(Calendar.MONTH, -1);
			hql+=" and date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";					
		}
		else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -3);
			hql+=" and date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("pickDate")){
			hql+=" and date(orderDetailsAddedDatetime)='"+startDate+"'";
		}
		
		hql+=" order by orderDetailsAddedDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);*/

		//filter order details by payment date
		List<OrderDetails> orderDetailList=paymentDAO.fetchOrderDetailsListPaymentDateByRange( startDate, endDate, range,orderDetailByBusinessNameIdEmployeeIdRequest.getBusinessNameId(),orderDetailByBusinessNameIdEmployeeIdRequest.getEmployeeId());
				
		List<OrderDetailsPaymentList> orderDetailsPaymentList=new ArrayList<>();
		
		
		if(orderDetailList==null){
			return null;

		}else{
			for(OrderDetails orderDetails:orderDetailList){

				double paidAmount=0;
				double balanceAmount=0;
				double totalAmount=orderDetails.getIssuedTotalAmountWithTax();
				List<Payment> paymentList=paymentDAO.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				
				if(paymentList!=null){
					
					balanceAmount=paymentList.get(0).getDueAmount();
					paidAmount=totalAmount-balanceAmount;
					
				}else{
					balanceAmount=totalAmount;
					paidAmount=0;
				}
				String status;
				
				if(paidAmount==totalAmount){
					status="Paid";
				}else if(paidAmount<totalAmount){
					status="PartialPaid";
				}else{
					status="UnPaid";
				}
					if(paidAmount!=0){
						orderDetailsPaymentList.add(new OrderDetailsPaymentList(orderDetails.getOrderId(), 
																				orderDetails.getIssuedTotalAmountWithTax(), 
																				paidAmount, 
																				balanceAmount,
																			status));
					}
				}
		}
			
		
		
		return orderDetailsPaymentList;
	}
	/**
	 * total sale find basis of Salesman Order, counter order by range,startDate,endDate 
	 * where order status is Packed,Issued,Delivered,Delivery Pending
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return total amount of sale
	 */
	@Transactional
	public double fetchTotalSaleForIndexPage(String range,String startDate,String endDate)
	{
		//'"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'
		String hql="";
				
			if (range.equals("currentMonth")) {
				hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(packedDate) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
			}
			else if (range.equals("lastMonth")) {
				hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLastMonthLastDate()+"') ";
			}
			else if (range.equals("last3Months")) {
				hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast3MonthLastDate()+"')";
			}
			else if (range.equals("last6Months")) {
				hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast6MonthLastDate()+"')";
			}
			else if (range.equals("range")) {
				hql="from OrderDetails where (date(packedDate) >= '"+startDate+"' and date(packedDate) <= '"+endDate+"')";
			}	
				
		 hql+= " and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"','"+Constants.ORDER_STATUS_BOOKED+"')"+
			   " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			   " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			   " order by packedDate desc";
			//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		double totalSaleAmount=0;
		for(OrderDetails orderDetails: orderDetailsList)
		{
			//totalSaleAmount+=orderDetails.getIssuedTotalAmountWithTax();
			totalSaleAmount+=orderDetails.getConfirmTotalAmountWithTax();
		}
		
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null, range, startDate, endDate);
		if(counterOrderList!=null){
			for(CounterOrder counterOrder: counterOrderList)
			{
				//totalSaleAmount+=orderDetails.getIssuedTotalAmountWithTax();
				totalSaleAmount+=counterOrder.getTotalAmountWithTax();
			}
		}		
		return totalSaleAmount;
	}
	/**
	 * <pre>
	 * find total amount invested in market
	 * Sales Man Order : Delivered ,Delivery Pending Status
	 * Counter Order : Delivered
	 * Calculate Amount which Pending to coming from Business/Customer  
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return total amount invested
	 * </pre>
	 */
	@Transactional
	public double fetchTotalAmountInvestInMarketForIndexPage(String range,String startDate,String endDate)
	{				
		String hql="";
		
		if (range.equals("currentMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(packedDate) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
		}
		else if (range.equals("lastMonth")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast3MonthLastDate()+"')";
		}
		else if (range.equals("last6Months")) {
			hql="from OrderDetails where (date(packedDate) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(packedDate) <= '"+DatePicker.getLast6MonthLastDate()+"')";
		}
		else if (range.equals("range")) {
			hql="from OrderDetails where (date(packedDate) >= '"+startDate+"' and date(packedDate) <= '"+endDate+"')";
		}		
		
	   hql+=" and payStatus=false and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"+
			" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
			" order by packedDate desc";
				   //" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		double totalAmountInvestInMarket=0;

		//PaymentDAOImpl paymentDAO=new PaymentDAOImpl(sessionFactory);
		
		for(OrderDetails orderDetails: orderDetailsList)
		{
			List<Payment> paymentList= paymentDAO.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
			if(paymentList==null)
			{
				totalAmountInvestInMarket+=orderDetails.getIssuedTotalAmountWithTax();
			}
			else
			{
				totalAmountInvestInMarket+=paymentList.get(0).getDueAmount();
			}	
		}
		
		//counter order
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null, range,startDate,endDate);
		if(counterOrderList!=null){
			for(CounterOrder counterOrder: counterOrderList)
			{
				if(counterOrder.isPayStatus()==false && counterOrder.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED)){
					List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
					if(paymentCounterList==null){
						totalAmountInvestInMarket+=counterOrder.getTotalAmountWithTax();
					}else{
						double amountPaid=0;
						for(PaymentCounter paymentCounter2: paymentCounterList){
							amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
						}
						totalAmountInvestInMarket+=counterOrder.getTotalAmountWithTax()-amountPaid;//paymentCounterList.get(0).getBalanceAmount();
					}
				}
			}
		}
			
		return totalAmountInvestInMarket;
	}
	/**
	 * <pre>
	 * fetch reissue order details by returnOrderId
	 * @param returnOrderProductId
	 * @return ReIssueOrderDetails
	 * </pre>
	 */
	@Transactional
	public ReIssueOrderDetails fetchReIssueOrderDetails(String returnOrderProductId)
	{
		String hql="from ReIssueOrderDetails where returnOrderProduct.returnOrderProductId='"+returnOrderProductId+"'"+
				   " and returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				   " and returnOrderProduct.orderDetails.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReIssueOrderDetails> list=(List<ReIssueOrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;			
		}
		return list.get(0);
	}
	/***
	 * <pre>
	 * make salesman order bill details for generate invoice pdf
	 * @param orderId
	 * @param companyId
	 * @return BillPrintDataModel
	 * </pre>
	 */
@Transactional
public BillPrintDataModel fetchBillPrintData(String orderId,long companyId,long branchId)
{
	
	//get order Details
	String hql="from OrderDetails where orderId='"+orderId+"'"+
			" and businessName.company.companyId="+companyId+
			" and businessName.branch.branchId="+branchId;
	
	Query query=sessionFactory.getCurrentSession().createQuery(hql);	
	List<OrderDetails> list=(List<OrderDetails>)query.list();
	OrderDetails orderDetails=list.get(0);
		
	BusinessName businessName=orderDetails.getBusinessName();
	
	//get orderProduct Details
	hql="from OrderProductDetails where orderDetails.orderId='"+orderDetails.getOrderId()+"'"+
			" and orderDetails.businessName.company.companyId="+companyId+
			" and orderDetails.businessName.branch.branchId="+branchId;
	query=sessionFactory.getCurrentSession().createQuery(hql);
	List<OrderProductDetails> orderProductDetailsList=(List<OrderProductDetails>)query.list();
		
	//OrderProductIssueDetails orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(orderId);
	
	//remove zero issued quantity products
	Iterator<OrderProductDetails> iterator=orderProductDetailsList.iterator();
	while(iterator.hasNext())
	{
		OrderProductDetails orderProductDetails=iterator.next();
		if(orderProductDetails.getIssuedQuantity()==0)
		{
			iterator.remove();
		}
	}
	//invoice number take from order details
	String invoiceNumber=orderDetails.getInvoiceNumber();
	
	//booked order date from order details
	String orderDate=new SimpleDateFormat("dd-MM-yyyy").format(orderDetails.getOrderDetailsAddedDatetime());
	
	//business address set for multiline
	List<String> addressLineList=new ArrayList<>();
	String[] arr = businessName.getAddress().split(" ");   	
	int cout=0;
	String line="";
	 for ( String ss : arr) {
		 cout++;			 	 
		 line+=ss+" ";		  
		 
		 if(cout==5)
		 {
			 cout=0;
			 addressLineList.add(line);
			 line="";
			 continue;
		 }		
	  }
	 if(addressLineList.size()>0)
	 {
		 addressLineList.set((addressLineList.size()-1), addressLineList.get(addressLineList.size()-1).substring(0, addressLineList.get(addressLineList.size()-1).length() - 1));
	 }
	 else
	 {
		 addressLineList.add(line);
	 }
	 System.out.println(addressLineList);
	
	//delivery date from order date
	String deliveryDate=new SimpleDateFormat("dd-MM-yyyy").format(orderDetails.getDeliveryDate());
	
	List<ProductListForBill> productListForBillList=new ArrayList<>();
	
	float totalAmountWithoutTax=0;
	
	float cGSTAmount=0;
	float iGSTAmount=0;
	float sGSTAmount=0;
	
	long totalQuantity=0;
	float totalAmountWithTax=0;
	String totalAmountWithTaxInWord="";
	
	List<CategoryWiseAmountForBill> categoryWiseAmountForBills=new ArrayList<>();
	
	float totalAmount=0;
	float taxAmount=0;
	String taxAmountInWord="";
	
	float totalCGSTAmount=0;
	float totalIGSTAmount=0;
	float totalSGSTAmount=0;
	
	DecimalFormat decimalFormat=new DecimalFormat("#0.00");
	DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
	long srno=1;
	//create product wise details
	for(OrderProductDetails orderProductDetails: orderProductDetailsList)
	{
		float amountWithoutTax=0;
		CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), 
				   orderProductDetails.getProduct().getCategories().getIgst());
		
		if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
		{	
			amountWithoutTax=orderProductDetails.getIssuedQuantity()*calculateProperTaxModel.getUnitprice();
		
			totalAmountWithoutTax+=amountWithoutTax;
			
			float igst=orderProductDetails.getProduct().getCategories().getIgst();
			float cgst=orderProductDetails.getProduct().getCategories().getCgst();
			float sgst=orderProductDetails.getProduct().getCategories().getSgst();
			
			float rate=calculateProperTaxModel.getUnitprice();
			long issuedQuantity=orderProductDetails.getIssuedQuantity();
			
			if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
			{
				cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
				iGSTAmount+=0;
				sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
			}
			else
			{
				cGSTAmount+=0;
				iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
				sGSTAmount+=0;						
			}
		}
		totalQuantity+=orderProductDetails.getIssuedQuantity();
		productListForBillList.add(new ProductListForBill(
				String.valueOf(srno),
				orderProductDetails.getProduct().getProductName()+(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "-(Free)" :""), 
				orderProductDetails.getProduct().getCategories().getHsnCode(),
				String.valueOf(((long)orderProductDetails.getProduct().getCategories().getIgst())),
				String.valueOf(orderProductDetails.getIssuedQuantity()), 
				(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0" :decimalFormat.format(calculateProperTaxModel.getUnitprice())), 
				(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0.0" :decimalFormat.format(amountWithoutTax) )));
		srno++;
		//double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
		//totalAmountWithTax+=amountWithoutTax+tax;	
		
	}
	//total amount with tax basis of product details
	for(ProductListForBill productListForBill2 :productListForBillList)
	{
		totalAmountWithTax+=Float.parseFloat(productListForBill2.getAmountWithoutTax());
	}
	totalAmountWithTax=Float.parseFloat(decimalFormat.format(totalAmountWithTax));
	cGSTAmount=Float.parseFloat(decimalFormatThreeDigit.format(cGSTAmount));
	iGSTAmount=Float.parseFloat(decimalFormat.format(iGSTAmount));
	sGSTAmount=Float.parseFloat(decimalFormatThreeDigit.format(sGSTAmount));
	
	totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;
	
	Set<OrderUsedCategories> categoriesList=new HashSet<>();
	//get unique category 
	for(OrderProductDetails orderProductDetails: orderProductDetailsList)
	{
		if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
		{
			categoriesList.add(orderProductDetails.getProduct().getCategories());
		}
	}
	
	//category wise taxslab and amount find  
	Set<String> categoriesHsnCodeList=new HashSet<>();
	for(OrderUsedCategories categories: categoriesList)
	{
		String hsncode="";			
		float taxableValue=0;
		float cgstPercentage=0; 
		float igstPercentage=0;			
		float sgstPercentage=0;
		float cgstRate=0; 
		float igstRate=0;
		float sgstRate=0;
		
		cGSTAmount=0;
		iGSTAmount=0;
		sGSTAmount=0;
		
		boolean gotDuplicateHsnCode=false;
		
		for(String hsnCode : categoriesHsnCodeList)
		{
			if(categories.getHsnCode().equals(hsnCode))
			{
				gotDuplicateHsnCode=true;
			}
		}
		if(gotDuplicateHsnCode)
		{
			continue;
		}
		
		categoriesHsnCodeList.add(categories.getHsnCode());
		
		for(OrderProductDetails orderProductDetails: orderProductDetailsList)
		{
			if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{
				if(categories.getHsnCode().equals(orderProductDetails.getProduct().getCategories().getHsnCode()))
				{
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), 
							   orderProductDetails.getProduct().getCategories().getIgst());
					taxableValue+=orderProductDetails.getIssuedQuantity()*calculateProperTaxModel.getUnitprice();
					
					float igst=orderProductDetails.getProduct().getCategories().getIgst();
					float cgst=orderProductDetails.getProduct().getCategories().getCgst();
					float sgst=orderProductDetails.getProduct().getCategories().getSgst();
					float rate=calculateProperTaxModel.getUnitprice();
					long issuedQuantity=orderProductDetails.getIssuedQuantity();
					
					if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
					{
						cGSTAmount+=calculateProperTaxModel.getCgst()*issuedQuantity;
						iGSTAmount+=0;
						sGSTAmount+=calculateProperTaxModel.getSgst()*issuedQuantity;
					}
					else
					{
						cGSTAmount+=0;
						iGSTAmount+=calculateProperTaxModel.getIgst()*issuedQuantity;
						sGSTAmount+=0;						
					}
					cgstPercentage=orderProductDetails.getProduct().getCategories().getCgst();
					igstPercentage=orderProductDetails.getProduct().getCategories().getIgst();	
					sgstPercentage=orderProductDetails.getProduct().getCategories().getSgst();
					hsncode=orderProductDetails.getProduct().getCategories().getHsnCode();
				}
			}
		}
		
		categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(
				hsncode, 
				decimalFormat.format(taxableValue), 
				decimalFormat.format(cgstPercentage), 
				decimalFormatThreeDigit.format(cGSTAmount), 
				decimalFormat.format(igstPercentage), 
				decimalFormat.format(iGSTAmount), 
				decimalFormat.format(sgstPercentage), 
				decimalFormatThreeDigit.format(sGSTAmount)));
		
		totalAmount+=taxableValue;
		totalCGSTAmount+=cGSTAmount;
		totalSGSTAmount+=sGSTAmount;
		totalIGSTAmount+=iGSTAmount;			
	}
	taxAmount=(
			Float.parseFloat(decimalFormatThreeDigit.format(totalSGSTAmount))+
			Float.parseFloat(decimalFormatThreeDigit.format(totalCGSTAmount))+
			Float.parseFloat(decimalFormat.format(totalIGSTAmount)));
	//total amount without tax in word
	taxAmountInWord=NumberToWordsConverter.convertWithPaisa(Float.parseFloat(decimalFormat.format(taxAmount)));
	
	//find round of amount
	totalAmountWithTax=Float.parseFloat(decimalFormat.format(totalAmountWithTax));
	float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax;
	float roundOf;
	String roundOfAmount="";
	if(decimalAmount==0)
	{
		roundOf=0.0f;
		roundOfAmount=""+decimalFormat.format(roundOf);
		totalAmountWithTax=totalAmountWithTax+roundOf;
	}
	else if(decimalAmount>=0.5)
	{
		roundOf=1-decimalAmount;
		roundOfAmount="+"+decimalFormat.format(roundOf);
		totalAmountWithTax=totalAmountWithTax+roundOf;
	}
	else
	{
		roundOf=decimalAmount;
		roundOfAmount="-"+decimalFormat.format(roundOf);
		totalAmountWithTax=totalAmountWithTax-roundOf;
	}	
	
	/*if(decimalAmount>0.0f)
	{
		roundOf=1-decimalAmount;
		roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
		totalAmountWithTax=totalAmountWithTax-roundOf;
	}*/
	
	totalAmountWithTaxInWord=NumberToWordsConverter.convertWithPaisa(Float.parseFloat(decimalFormat.format(totalAmountWithTax)));

	/**
	 * calculate category wise block under discount and net amount 
	 */
//	double discountAmtCut = 0, discountPerCut = 0;
//	if (orderDetails.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
//		discountAmtCut = proformaOrder.getDiscount();
//		discountPerCut = (proformaOrder.getDiscount() / totalAmountWithTax) * 100;
//	} else {
//		discountPerCut = proformaOrder.getDiscount();
//		discountAmtCut = (proformaOrder.getDiscount() * totalAmountWithTax) / 100;
//	}
	double totalAmountDisc=0;//(discountPerCut*totalAmount)/100;
	double totalCGSTAmountDisc=0;//(totalCGSTAmount*totalAmount)/100;
	double totalIGSTAmountDisc=0;//(totalIGSTAmount*totalAmount)/100;
	double totalSGSTAmountDisc=0;//(totalSGSTAmount*totalAmount)/100;
	
	double netTotalAmount=totalAmount-totalAmountDisc;
	double netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
	double netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
	double netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
	
	return new BillPrintDataModel(
			orderId,
			invoiceNumber, 
			"",
			"",
			orderDate, 
			addressLineList,
			businessName, 
			null,
			deliveryDate, 
			productListForBillList, 
			decimalFormatThreeDigit.format(totalCGSTAmount), 
			decimalFormat.format(totalIGSTAmount), 
			decimalFormatThreeDigit.format(totalSGSTAmount), 
			roundOfAmount,
			decimalFormat.format(totalAmountWithoutTax),
			String.valueOf(totalQuantity), 
			decimalFormat.format(totalAmountWithTax),
			"",
			"",
			"",
			"",
			"",//totalAmountWithTaxRoundOff,
			totalAmountWithTaxInWord, 
			categoryWiseAmountForBills, 
			decimalFormat.format(totalAmount), 
			taxAmountInWord, 
			decimalFormatThreeDigit.format(totalCGSTAmount), 
			decimalFormat.format(totalIGSTAmount), 
			decimalFormatThreeDigit.format(totalSGSTAmount),
			decimalFormat.format(totalAmountDisc),
			decimalFormatThreeDigit.format(totalCGSTAmountDisc),
			decimalFormatThreeDigit.format(totalIGSTAmountDisc),
			decimalFormatThreeDigit.format(totalSGSTAmountDisc),
			decimalFormat.format(netTotalAmount),
			decimalFormatThreeDigit.format(netTotalCGSTAmount),
			decimalFormatThreeDigit.format(netTotalIGSTAmount),
			decimalFormatThreeDigit.format(netTotalSGSTAmount),
			(orderDetails.getTransportation()==null)?"NA":orderDetails.getTransportation().getTransportName(),
			(orderDetails.getVehicleNo()==null)?"NA":orderDetails.getVehicleNo(),
			(orderDetails.getTransportation()==null)?"NA":orderDetails.getTransportation().getGstNo(),
			(orderDetails.getDocketNo()==null)?"NA":orderDetails.getDocketNo(),
					"",
					0);
	}
	/**
	 * <pre>
	 * fetch order details by business , range, startDate, endDate
	 * @param businessNameId
	 * @param fromDate
	 * @param toDate
	 * @param range
	 * @return OrderDetailsListOfBusiness
	 * </pre>
	 */
	@Transactional 
	public OrderDetailsListOfBusiness fetchOrderDetailsListOfBusinessByBusinessNameId(String businessNameId,String fromDate, String toDate,  String range)
	{
		List<OrderDetailsListOfBusinessSub> orderDetailsListOfBusinessSubList=new ArrayList<>();
		
		//BusinessNameDAOImpl businessNameDAO=new BusinessNameDAOImpl(sessionFactory);
		BusinessName businessName=businessNameDAO.fetchBusinessForWebApp(businessNameId);
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String hql = "";
		Query query=null;		
		
		if(range.equals("range"))
		{
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And (date(orderDetailsAddedDatetime)>='"+fromDate+"' And date(orderDetailsAddedDatetime)<='"+toDate+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("today"))
		{
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And date(orderDetailsAddedDatetime)=date(CURRENT_DATE())";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And date(orderDetailsAddedDatetime)='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And (date(orderDetailsAddedDatetime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetailsAddedDatetime)<='"+DatePicker.getLastMonthLastDate()+"')";					
		}else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -6);
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And (date(orderDetailsAddedDatetime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetailsAddedDatetime)<='"+DatePicker.getLast3MonthLastDate()+"')";
		}
		else if(range.equals("pickDate")){
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And date(orderDetailsAddedDatetime)='"+fromDate+"'";
		}
		else if(range.equals("viewAll")){
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"'";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from OrderDetails where businessName.businessNameId='"+businessNameId+"' And (date(orderDetailsAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetailsAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
		}
		
		hql+=" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				" and businessName.branch.branchId="+getSessionSelectedBranchIds()+
			    " order by orderDetailsAddedDatetime desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
		double totalBusinessAmount=0, amountPaid=0, amountUnPaid=0;
		long srno=1;
		for(OrderDetails orderDetails: orderDetailsList)
		{
			if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
			{
				orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
													srno, 
													orderDetails.getOrderId(), 
													"NA", 
													"NA", 
													"NA", 
													orderDetails.getTotalAmountWithTax(),
													0, 
													orderDetails.getTotalAmountWithTax(), 
													"UnPaid"));
				totalBusinessAmount+=orderDetails.getTotalAmountWithTax();
				amountPaid+=0;
				amountUnPaid+=orderDetails.getTotalAmountWithTax();
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
			{
				orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
						srno, 
						orderDetails.getOrderId(), 
						"NA", 
						"NA", 
						"NA", 
						orderDetails.getTotalAmountWithTax(), 
						0, 
						orderDetails.getTotalAmountWithTax(), 
						"UnPaid"));
				totalBusinessAmount+=orderDetails.getTotalAmountWithTax();
				amountPaid+=0;
				amountUnPaid+=orderDetails.getTotalAmountWithTax();
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
			{
				orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
						srno, 
						orderDetails.getOrderId(), 
						"NA", 
						"NA", 
						"NA",  
						orderDetails.getIssuedTotalAmountWithTax(),
						0, 
						orderDetails.getIssuedTotalAmountWithTax(), 
						"UnPaid"));
				totalBusinessAmount+=orderDetails.getIssuedTotalAmountWithTax();
				amountPaid+=0;
				amountUnPaid+=orderDetails.getIssuedTotalAmountWithTax();
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED) || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
			{
				//PaymentDAOImpl paymentDAO=new PaymentDAOImpl(sessionFactory);
				List<Payment> paymentList=paymentDAO.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				
				if(paymentList==null)
				{
				        orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
						srno, 
						orderDetails.getOrderId(), 
						"NA", 
						"NA", 
						"NA", 
						orderDetails.getIssuedTotalAmountWithTax(), 
						0,
						orderDetails.getIssuedTotalAmountWithTax(), 
					    "UnPaid"));
				        
				        totalBusinessAmount+=orderDetails.getIssuedTotalAmountWithTax();
						amountPaid+=0;
						amountUnPaid+=orderDetails.getIssuedTotalAmountWithTax();
				}
				else
				{
					double totalAmount=orderDetails.getIssuedTotalAmountWithTax();
					double totalDueAmount=paymentList.get(0).getDueAmount();
					double totalPaidAmount=totalAmount-totalDueAmount;
					if(totalAmount<=totalPaidAmount)
					{
						orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
								srno, 
								orderDetails.getOrderId(), 
								simpleDateFormat.format(orderDetails.getOrderDetailsPaymentTakeDatetime()), 
								(paymentList.get(0).getPayType().equals(Constants.CASH_PAY_STATUS))?"NA":paymentList.get(0).getBankName()+"-"+paymentList.get(0).getChequeNumber(),
								paymentList.get(0).getPayType(),
								orderDetails.getIssuedTotalAmountWithTax(),
								orderDetails.getIssuedTotalAmountWithTax(), 
								0,
							    "Paid"));
						
						totalBusinessAmount+=orderDetails.getIssuedTotalAmountWithTax();
						amountPaid+=orderDetails.getIssuedTotalAmountWithTax();
						amountUnPaid+=0;
					}
					else
					{
						orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
								srno, 
								orderDetails.getOrderId(), 
								simpleDateFormat.format(orderDetails.getOrderDetailsPaymentTakeDatetime()), 
								(paymentList.get(0).getPayType().equals(Constants.CASH_PAY_STATUS))?"NA":paymentList.get(0).getBankName()+"-"+paymentList.get(0).getChequeNumber(),
								paymentList.get(0).getPayType(),
								orderDetails.getIssuedTotalAmountWithTax(),
								totalPaidAmount, 
								totalDueAmount,
							    "Partial Paid"));
						
						totalBusinessAmount+=orderDetails.getIssuedTotalAmountWithTax();
						amountPaid+=totalPaidAmount;
						amountUnPaid+=totalDueAmount;
					}
				}
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
			{
				if(orderDetails.getIssuedTotalAmountWithTax()==0)
				{				
						orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
						srno, 
						orderDetails.getOrderId(), 
						"NA", 
						"NA", 
						"NA", 
						orderDetails.getTotalAmountWithTax(),
						0, 
						orderDetails.getTotalAmountWithTax(), 
						"UnPaid"));
						
						totalBusinessAmount+=orderDetails.getTotalAmountWithTax();
						amountPaid+=0;
						amountUnPaid+=orderDetails.getTotalAmountWithTax();
				}
				else
				{
						orderDetailsListOfBusinessSubList.add(new OrderDetailsListOfBusinessSub(
						srno, 
						orderDetails.getOrderId(), 
						"NA", 
						"NA", 
						"NA", 
						orderDetails.getIssuedTotalAmountWithTax(),
						0, 
						orderDetails.getIssuedTotalAmountWithTax(), 
						"UnPaid"));
						
						totalBusinessAmount+=orderDetails.getIssuedTotalAmountWithTax();
						amountPaid+=0;
						amountUnPaid+=orderDetails.getIssuedTotalAmountWithTax();
				}
			}
			srno++;
		}
		
		
		return new OrderDetailsListOfBusiness(
				businessName.getShopName(),
				businessName.getBusinessNameId(),
				businessName.getContact(),
				businessName.getArea().getName(),
				totalBusinessAmount, amountPaid, amountUnPaid, orderDetailsListOfBusinessSubList);
	}
	/**
	 * <pre>
	 * send sms to selected order businesses 
	 * if one select then send sms on given mobile number 
	 * other wise registered number
	 * @param orderIds
	 * @param smsText
	 * @return success / failed
	 * </pre>
	 */
	@Transactional
	public String sendSMSTOShopsUsingOrderId(String orderIds,String smsText,String mobileNumber) {

		try {
			String endText=getEndTextForSMS();		
			String[] orderId2 = orderIds.split(",");
			
			if(orderId2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+orderId2[0]);
				return "Success";
			}
			
			for(int i=0; i<orderId2.length; i++){
				String mobNumber;
				CounterOrder counterOrder;
				if(orderId2[i].contains("CORD")){
					counterOrder=counterOrderDAO.fetchCounterOrder(orderId2[i]);
					if(counterOrder.getBusinessName()==null){
						mobNumber=counterOrder.getCustomerMobileNumber();
					}else{
						mobNumber=counterOrder.getBusinessName().getContact().getMobileNumber();
					}
				}else{
					orderDetails=fetchOrderDetailsByOrderId(orderId2[i]);
					mobNumber=orderDetails.getBusinessName().getContact().getMobileNumber();
				}
				
				SendSMS.sendSMS(Long.parseLong(mobNumber), smsText+endText);
				System.out.println("SMS send to : "+orderId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}/*
	@Transactional
	public OrderUsedProduct fetchOrderUsedProductForWebApp(long orderUsedproductId)
	{
		String hql="from OrderUsedProduct where productId="+orderUsedproductId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderUsedProduct> orderUsedProductList=(List<OrderUsedProduct>)query.list();
		if(orderUsedProductList.isEmpty())
		{
			return null;
		}
		return orderUsedProductList.get(0);
	}
	
	@Transactional
	public List<OrderUsedProduct> fetchOrderUsedPffroductByProductId(long productId)
	{
		String hql="from OrderUsedProduct where product.productId="+productId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderUsedProduct> orderUsedProductList=(List<OrderUsedProduct>)query.list();
		if(orderUsedProductList.isEmpty())
		{
			return null;
		}
		return orderUsedProductList;
	}*/
	
	/**
	 * <pre>
	 * delete order used category
	 * delete order used brand
	 * delete order used product
	 * @param orderUsedProduct
	 * </pre>
	 */
	@Transactional
	public void deleteOrderUsedProduct(OrderUsedProduct orderUsedProduct)
	{
		OrderUsedCategories orderUsedCategories=orderUsedProduct.getCategories();
		OrderUsedBrand orderUsedBrand=orderUsedProduct.getBrand();
		
		orderUsedCategories=(OrderUsedCategories)sessionFactory.getCurrentSession().merge(orderUsedCategories);
		sessionFactory.getCurrentSession().delete(orderUsedCategories);
		
		orderUsedBrand=(OrderUsedBrand)sessionFactory.getCurrentSession().merge(orderUsedBrand);
		sessionFactory.getCurrentSession().delete(orderUsedBrand);
		
		orderUsedProduct=(OrderUsedProduct)sessionFactory.getCurrentSession().merge(orderUsedProduct);
		sessionFactory.getCurrentSession().delete(orderUsedProduct);
	}
	/**
	 * <pre>
	 * update order used product current quantity with product current quantity
	 * @return success
	 * </pre>
	 */
	@Transactional
	public String updateOrderUsedProductCurrentQuantity()
	{	
	  /*update order_used_product  as opr  set 
		current_quantity=(select current_quantity from product as pr where pr.product_id=opr.product_entity_id),
		damage_quantity=(select damage_quantity from product as pr where pr.product_id=opr.product_entity_id)
		where 1=1*/
		
		Query query = sessionFactory.getCurrentSession().createQuery("update OrderUsedProduct opr  set "+
				" currentQuantity=(select pr.currentQuantity from Product pr where pr.productId=opr.product.productId)"
				 /*+" where product.company.companyId="+getSessionSelectedCompaniesIds()*/);
		
		int result = query.executeUpdate();
		
		System.out.println("Order User Products update : "+result);
		
		return "Success";
	}
	/**
	 * <pre>
	 * fetch order details list for sales report which order status is Delivered
	 * @param startDate
	 * @param endDate
	 * @param range
	 * @return SalesReportModel list
	 * </pre>
	 */
	@Transactional
	public List<SalesReportModel> fetchSalesReportModel(String startDate, String endDate,String range)
	{
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		List<SalesReportModel> salesManReportModelList=new ArrayList<>();
		
		if (range.equals("range")) {
			hql="from OrderDetails where (date(confirmDate)>='"+startDate+"' and date(confirmDate)<='"+endDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where (date(confirmDate)='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where (date(confirmDate)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
		}
		else if (range.equals("last1month")) {
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getLastMonthFirstDate()+"' and date(confirmDate)>='"+DateUtils.getLastMonthLastDate()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("lastMonth")) {//index need
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getLastMonthFirstDate()+"' and date(confirmDate)>='"+DateUtils.getLastMonthLastDate()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("last3months")) { 
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getLast3MonthFirstDate()+"' and date(confirmDate)>='"+DateUtils.getLast3MonthLastDate()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("last3Months")) { 
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getLast3MonthFirstDate()+"' and date(confirmDate)>='"+DateUtils.getLast3MonthLastDate()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("last6Months")) { 
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getLast6MonthFirstDate()+"' and date(confirmDate)>='"+DateUtils.getLast6MonthLastDate()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("currentMonth")) { 
			hql="from OrderDetails where (date(confirmDate)>='"+DateUtils.getCurrentMonthStartDateInString()+"' and date(confirmDate)<='"+DateUtils.getCurrentMonthLastDateInString()+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("today")) {
			hql="from OrderDetails where (date(confirmDate)='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("pickdate")) {
			hql="from OrderDetails where (date(confirmDate)='"+startDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		else if (range.equals("viewAll")) {
			hql="from OrderDetails  where orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
		}
		
		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "+
				" order by deliveryDate desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();		
		
		if(orderDetailsList.isEmpty())
		{
			return null;			
		}	
		dateFormat=new SimpleDateFormat("dd-MM-yyyy");
		long srno=1;
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		for(OrderDetails orderDetails: orderDetailsList)
		{			
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			orderStatusSM=Constants.ORDER_STATUS_BOOKED;
			orderStatusSMDate=dateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
			orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
			orderStatusGK=Constants.ORDER_STATUS_PACKED;
			orderStatusGKDate=dateFormat.format(orderDetails.getPackedDate());
			orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
			orderStatusDB=orderDetails.getOrderStatus().getStatus();
			orderStatusDBDate=dateFormat.format(orderDetails.getConfirmDate());
			orderStatusDBTime= simpleTimeFormat.format(orderDetails.getConfirmDate());
			amount=orderDetails.getIssuedTotalAmount();
			amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
			quantity=orderDetails.getIssuedTotalQuantity();
				
			
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId());
			salesManReportModelList.add(new SalesReportModel(
					srno, 
					orderDetails.getOrderId(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getArea().getName(), 
					employeeDetails.getName(), 
					employeeDetails.getEmployeeDetailsId(), 
					Float.parseFloat(decimalFormat.format(orderDetails.getIssuedTotalAmount())),
					Float.parseFloat(decimalFormat.format(orderDetails.getIssuedTotalAmountWithTax())),
					dateFormat.format(orderDetails.getOrderDetailsPaymentTakeDatetime()),
					orderStatusSM,
					orderStatusSMDate,
					orderStatusSMTime,
					orderStatusGK,
					orderStatusGKDate,
					orderStatusGKTime,
					orderStatusDB,
					orderStatusDBDate,
					orderStatusDBTime));
			srno++;
		}
		return salesManReportModelList;
	}
	/**
	 * <pre>
	 * fetch order details list by range,startdate,enddate which order status is Cancelled
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> cancelOrderReport(String range,String startDate,String endDate)
	{
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from OrderDetails where date(cancelDate) >= '"+startDate+"' and date(cancelDate) <= '"+endDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("today"))
		{
			hql="from OrderDetails where date(cancelDate) = date(CURRENT_DATE()) and  orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(cancelDate) = '"+simpleDateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(cancelDate) >= '"+simpleDateFormat.format(cal.getTime())+"'  and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from OrderDetails where (date(cancelDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(cancelDate) <= '"+DatePicker.getCurrentMonthLastDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from OrderDetails where (date(cancelDate) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(cancelDate) <= '"+DatePicker.getLastMonthFirstDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("last3Months"))
		{
			hql="from OrderDetails where (date(cancelDate) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(cancelDate) <= '"+DatePicker.getLast3MonthFirstDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("pickDate"))
		{
			hql="from OrderDetails where date(cancelDate) = '"+startDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}
		else if(range.equals("viewAll"))
		{
			hql="from OrderDetails where orderStatus.status in ('"+Constants.ORDER_STATUS_CANCELED+"')";
		}

		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "+
				" order by cancelDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(OrderDetails orderDetails : list){
			EmployeeDetails employeeDetailsCancel=employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeIdCancel().getEmployeeId());
			
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			 if(employeeDetailsCancel.getEmployee().getDepartment().getName().equals(Constants.SALESMAN_DEPT_NAME))
				{
					orderStatusSM=Constants.ORDER_STATUS_CANCELED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getCancelDate());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getCancelDate());
					orderStatusGK = "--";
					orderStatusGKDate = "--";
					orderStatusGKTime= "--";
					orderStatusDB = "--";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getTotalAmount();
					amountWithTax=orderDetails.getTotalAmountWithTax();
					quantity=orderDetails.getTotalQuantity();
				}
				else if(employeeDetailsCancel.getEmployee().getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_CANCELED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getCancelDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getCancelDate());
					orderStatusDB = "--";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(employeeDetailsCancel.getEmployee().getDepartment().getName().equals(Constants.DELIVERYBOY_DEPT_NAME))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_PACKED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
					orderStatusDB = Constants.ORDER_STATUS_CANCELED;
					orderStatusDBDate = simpleDateFormat.format(orderDetails.getCancelDate());
					orderStatusDBTime= simpleTimeFormat.format(orderDetails.getCancelDate());
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
			
			list2.add(new OrderReportList(srno,
					orderDetails.getOrderId(), 
					amount,
					amountWithTax,
					orderDetails.getBusinessName().getArea().getName(),
					quantity, 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
					orderDetails.getPaymentPeriodDays(), 
					orderDetails.getOrderDetailsAddedDatetime(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							orderDetails.getOrderStatus().getStatus(),
							orderDetails.getCancelReason()));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * fetch order details list by range,startDate,endDate which order status is Cancelled
	 * and also its required in area which given employeeId belongs 
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails> fetchCancelOrderReportByEmployeeId(long employeeId, String fromDate, String toDate,
			String range) {

		String hql = "";
		Query query = null;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
		
		List<Long> pnjId = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	Area area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }

		if (range.equals("range")) {
			hql = "from OrderDetails ord where ord.orderStatus.status='" + Constants.ORDER_STATUS_CANCELED
					+ "' and date(ord.cancelDate) >='" + fromDate
					+ "' and date(ord.cancelDate) <='" + toDate + "'";
		} else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql = "from OrderDetails ord where ord.orderStatus.status='" + Constants.ORDER_STATUS_CANCELED
					+ "' and date(ord.cancelDate) >='"
					+ simpleDateFormat.format(cal.getTime()) + "'";
		} else if (range.equals("last1month")) {
			cal.add(Calendar.MONTH, -1);
			hql = "from OrderDetails ord where ord.orderStatus.status='" + Constants.ORDER_STATUS_CANCELED
					+ "' and date(ord.cancelDate) >='"
					+ simpleDateFormat.format(cal.getTime()) + "'";
		} else if (range.equals("last3months")) {
			cal.add(Calendar.MONTH, -3);
			hql = "from OrderDetails ord where ord.orderStatus.status='" + Constants.ORDER_STATUS_CANCELED
					+ "' and date(ord.cancelDate) >='"
					+ simpleDateFormat.format(cal.getTime()) + "'";
		} else if (range.equals("pickDate")) {
			hql = "from OrderDetails ord where ord.orderStatus.status='" + Constants.ORDER_STATUS_CANCELED
					+ "' and date(ord.cancelDate)='" + fromDate+"'";
		}

		//" and businessName.area.areaId in (:ids) "+
		hql+=" and ord.employeeIdCancel="+employeeId+
				" and ord.businessName.branch.branchId in ("+getSessionSelectedBranchIds()+") "+
				" and ord.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" order by ord.cancelDate desc";
		//hql+=" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by cancelDate desc";
		query = sessionFactory.getCurrentSession().createQuery(hql);
		//query.setParameterList("ids", pnjId);
		@SuppressWarnings("unchecked")
		List<OrderDetails> cancelOrderList = (List<OrderDetails>) query.list();
		if (cancelOrderList.isEmpty()) {
			return null;
		}

		return cancelOrderList;
	}
	/**
	 * <pre>
	 * edit order on Booked,Packed,Issued order status
	 * @param updateOrderProductListId
	 * @param orderId
	 * </pre>
	 */
	@Transactional
	public void updateEditOrder(String updateOrderProductListId,String orderId)
	{	
		double totalAmountWithTax=0;
		long totalQuantity=0;
		double totalAmount=0;
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		//DecimalFormat decimalFormat=new DecimalFormat("###");
		List<OrderProductDetails> orderProductDetailsList=new ArrayList<>();
		List<OrderProductDetails> orderProductDetailsListOld=fetchOrderProductDetailByOrderId(orderId);
		
		orderDetails=fetchOrderDetailsByOrderId(orderId);		
		orderDetails.setPayStatus(false);
		
		//[productId,purchaseQuantity,issuedQuantity,rate,sesslingRate,purchaseAmount,issuedAmount,type]
		String orderProductList[]=updateOrderProductListId.split("-");
		
		//only booked status order can edit in if section
		if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
		{
			//calculate main totals of OrderDetails 
			//saving updated order product details with order used product,categories,brand
			for(String orderProduct : orderProductList)
			{
				/*
				 						orderProductValues[0]=orderProduct.product.product.productId
										orderProductValues[1]=orderProduct.purchaseQuantity
										orderProductValues[2]=orderProduct.issuedQuantity
										orderProductValues[3]=orderProduct.rate
										orderProductValues[4]=orderProduct.sellingRate
										orderProductValues[5]=orderProduct.purchaseAmount
										orderProductValues[6]=orderProduct.issueAmount
										orderProductValues[7]=orderProduct.type										
										orderProductValues[8]=orderProduct.igst
										orderProductValues[9]=editMode
				 */
				String orderProductValues[]=orderProduct.split(",");
				
				product=productDAO.fetchProductForWebApp(Long.parseLong(orderProductValues[0]));
				
				totalQuantity+=Long.parseLong(orderProductValues[1]);
				
				OrderProductDetails orderProductDetails=new OrderProductDetails();
				
				if(orderProductValues[7].equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Double.parseDouble(orderProductValues[4]), Double.parseDouble(orderProductValues[8]));
					double purchaseAmt=calculateProperTaxModel.getUnitprice()*Long.parseLong(orderProductValues[1]);
					totalAmount+=purchaseAmt;
					
					double purchaseAmtWithTax=Double.parseDouble(orderProductValues[5]);				
					totalAmountWithTax+=purchaseAmtWithTax;
					
					orderProductDetails.setSellingRate(Double.parseDouble(orderProductValues[4]));
					orderProductDetails.setPurchaseAmount(purchaseAmtWithTax);
				}
				else
				{
					orderProductDetails.setSellingRate(0);
					orderProductDetails.setPurchaseAmount(0);
				}
				orderProductDetails.setPurchaseQuantity(Long.parseLong(orderProductValues[1]));
				orderProductDetails.setType(orderProductValues[7]);
				orderProductDetails.setOrderDetails(orderDetails);

				Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
				Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
				
				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				orderUsedBrand.setCompany(company);
				orderUsedBrand.setBranch(branch);
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(),
						company,
						branch);
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						/*product.getProductImage(),
						product.getProductContentType(),*/
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity(),
						company,
						branch);
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				orderProductDetails.setProduct(orderUsedProduct);
				orderProductDetailsList.add(orderProductDetails);
			}
			orderDetails.setTotalAmount(totalAmount);
			orderDetails.setTotalAmountWithTax(totalAmountWithTax);
			orderDetails.setTotalQuantity(totalQuantity);
			sessionFactory.getCurrentSession().save(orderDetails);
			
			//delete old order product details
			for(OrderProductDetails orderProductDetails: orderProductDetailsListOld)
			{
				deleteOrderUsedProduct(orderProductDetails.getProduct());
				orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
				sessionFactory.getCurrentSession().delete(orderProductDetails);
			}
			
			//add new edited order product details
			for(OrderProductDetails orderProductDetails: orderProductDetailsList)
			{
				sessionFactory.getCurrentSession().save(orderProductDetails);
			}
		}
		else
		{
			boolean purchaseDataChange=false;
			//only packed and issued status orders can edit in else part
			OrderProductIssueDetails  orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(orderId);
			
			//calculate totalIssued quantity
			//create order used product,category,brand  then order used product and store in {@link orderProductDetailsList}
			//if packed order product found as edited
			for(String orderProduct : orderProductList)
			{
				String orderProductValues[]=orderProduct.split(",");	
				
				product=productDAO.fetchProductForWebApp(Long.parseLong(orderProductValues[0]));
				
				totalQuantity+=Long.parseLong(orderProductValues[2]);
				
				OrderProductDetails orderProductDetails=new OrderProductDetails();
				if(orderProductValues[7].equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Double.parseDouble(orderProductValues[4]), Double.parseDouble(orderProductValues[8]));
					double issueAmt=calculateProperTaxModel.getUnitprice()*Long.parseLong(orderProductValues[2]);
					totalAmount+=issueAmt;
					
					double issueAmtWithTax=Double.parseDouble(orderProductValues[6]);				
					totalAmountWithTax+=issueAmtWithTax;
					
					orderProductDetails.setSellingRate(Double.parseDouble(orderProductValues[4]));
					orderProductDetails.setIssueAmount(issueAmtWithTax);
				}
				else
				{
					orderProductDetails.setSellingRate(0);
					orderProductDetails.setIssueAmount(0);
				}
				orderProductDetails.setIssuedQuantity(Long.parseLong(orderProductValues[2]));
				orderProductDetails.setType(orderProductValues[7]);
				orderProductDetails.setOrderDetails(orderDetails);

				Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
				Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
				
				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				orderUsedBrand.setCompany(company);
				orderUsedBrand.setBranch(branch);
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(),
						company,
						branch);
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						/*product.getProductImage(),
						product.getProductContentType(),*/
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity(),
						company,
						branch);
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				orderProductDetails.setProduct(orderUsedProduct);
				
				//if order status is packed and editModeList(gives records which edited in packed status) is non empty
				//if order product found is edited then same issued quantiy and amount same reflect in purchase quantity and amount
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					if(orderProductValues[9].equals(Constants.EDIT_MODE))
					{
						purchaseDataChange=true;
						orderProductDetails.setPurchaseAmount(orderProductDetails.getIssueAmount());
						orderProductDetails.setPurchaseQuantity(orderProductDetails.getIssuedQuantity());
					}
				}
				orderProductDetailsList.add(orderProductDetails);
			}
			orderDetails.setIssuedTotalAmount(totalAmount);
			orderDetails.setIssuedTotalAmountWithTax(totalAmountWithTax);
			orderDetails.setIssuedTotalQuantity(totalQuantity);
			
			List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
			//delete old order product details
			for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
			{
				boolean isDeleted=true;
				// if product not delete then update purchase quantity and amount as old order products
				for(int i=0; i<orderProductDetailsList.size(); i++)
				{
					if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
							&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
					{
						OrderProductDetails orderProductDetails=orderProductDetailsList.get(i);
						
						orderProductDetails.setPurchaseAmount(orderProductDetailsOld.getPurchaseAmount());
						orderProductDetails.setPurchaseQuantity(orderProductDetailsOld.getPurchaseQuantity());
						orderProductDetailsList.set(i, orderProductDetails);
						isDeleted=false;
					}
				}
				//if product found as delete and order status is issued then add in Return From delivery boy
				if(isDeleted)
				{
					//if order products delete when issued order status then its make ReturnFromDeliveryBoy List 
					//for temporary store upto delivery boy return give order product to gatekeeper 
					//and then gatekeeper check and add to current quantity or define damange
					if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
					{
					//OrderProductDetails orderProductDetails=orderProductDetailsList.get(i);
					product=orderProductDetailsOld.getProduct().getProduct();
					Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
					Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
					
					OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
					orderUsedBrand.setName(product.getBrand().getName());
					orderUsedBrand.setCompany(company);
					orderUsedBrand.setBranch(branch);
					sessionFactory.getCurrentSession().save(orderUsedBrand);
					
					OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
							product.getCategories().getCategoryName(), 
							product.getCategories().getHsnCode(), 
							product.getCategories().getCgst(), 
							product.getCategories().getSgst(), 
							product.getCategories().getIgst(),
							company,
							branch);
					sessionFactory.getCurrentSession().save(orderUsedCategories);
					
					OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
							product,
							product.getProductName(), 
							product.getProductCode(), 
							orderUsedCategories, 
							orderUsedBrand, 
							product.getRate(), 
							/*product.getProductImage(),
							product.getProductContentType(),*/
							product.getThreshold(), 
							product.getCurrentQuantity(),
							product.getDamageQuantity(),
							company,
							branch);
									
					sessionFactory.getCurrentSession().save(orderUsedProduct);
							// return from delivery boy child create and add in {@link returnFromDeliveryBoyList}
							ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
									orderUsedProduct, 
									orderProductDetailsOld.getSellingRate(), 
									orderProductDetailsOld.getIssuedQuantity(), 
									orderProductDetailsOld.getIssuedQuantity(),
									0,
									0,
									0, 
									orderProductDetailsOld.getType());
							returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
					}
				}
				//permanent delete order used product 
				deleteOrderUsedProduct(orderProductDetailsOld.getProduct());
				
				//permanent delete order product
				orderProductDetailsOld=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetailsOld);
				sessionFactory.getCurrentSession().delete(orderProductDetailsOld);
			}
			
			
			for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
			{
				boolean productHave=false;
				//compare old order used product with new order used product and update product current quantity
				//here update current quantity which not deleted
				for(OrderProductDetails orderProductDetails: orderProductDetailsList)
				{
					if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetails.getProduct().getProduct().getProductId()
							&& orderProductDetailsOld.getType().equals(orderProductDetails.getType()))
					{
						productHave=true;
						product=orderProductDetails.getProduct().getProduct();
						
						//if packed status then 
						//update product current quantity directly
						////update daily stock details
						if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
						{							
								if(orderProductDetailsOld.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
								{
									product.setFreeQuantity(product.getFreeQuantity()-orderProductDetailsOld.getIssuedQuantity());
									product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
								}
								else
								{	
									product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
								}
								
								if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
								{
									product.setFreeQuantity(product.getFreeQuantity()+orderProductDetails.getIssuedQuantity());
									product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetails.getIssuedQuantity());
								}
								else
								{	
									product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetails.getIssuedQuantity());
								}
								
								product=(Product)sessionFactory.getCurrentSession().merge(product);
								productDAO.Update(product);
								productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetails.getIssuedQuantity(), false);
								//updateOrderUsedProductCurrentQuantity(product);
						}
						//if order status id issued then 
						//if old issued quantity is greater than new issued quantity
						//order product details add in return from delivery boy
						if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED) 
								&& orderProductDetailsOld.getIssuedQuantity()>orderProductDetails.getIssuedQuantity())
						{
							long returnQuantity=orderProductDetailsOld.getIssuedQuantity()-orderProductDetails.getIssuedQuantity();
							
							Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
							Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
							
							OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
							orderUsedBrand.setName(product.getBrand().getName());
							orderUsedBrand.setCompany(company);
							orderUsedBrand.setBranch(branch);
							sessionFactory.getCurrentSession().save(orderUsedBrand);
							
							OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
									product.getCategories().getCategoryName(), 
									product.getCategories().getHsnCode(), 
									product.getCategories().getCgst(), 
									product.getCategories().getSgst(), 
									product.getCategories().getIgst(),
									company,
									branch);
							sessionFactory.getCurrentSession().save(orderUsedCategories);
							
							OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
									product,
									product.getProductName(), 
									product.getProductCode(), 
									orderUsedCategories, 
									orderUsedBrand, 
									product.getRate(), 
									/*product.getProductImage(),
									product.getProductContentType(),*/
									product.getThreshold(), 
									product.getCurrentQuantity(),
									product.getDamageQuantity(),
									company,
									branch);
											
							sessionFactory.getCurrentSession().save(orderUsedProduct);
							// return from delivery boy child create and add in {@link returnFromDeliveryBoyList}
							ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
									orderUsedProduct, 
									orderProductDetails.getSellingRate(), 
									returnQuantity, 
									orderProductDetailsOld.getIssuedQuantity(),
									orderProductDetails.getIssuedQuantity(),
									0,
									0, 
									orderProductDetails.getType());
							returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
							//sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
						}							
					}					
				}
				//if product found as deleted from new order product list
				//and if order status is packed then packed quantity return add directly to product current quantity
				//update daily stock details
				if(productHave==false)
				{
					if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
					{
						product=productDAO.fetchProductForWebApp(orderProductDetailsOld.getProduct().getProduct().getProductId());
						
						if(orderProductDetailsOld.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
						{
							product.setFreeQuantity(product.getFreeQuantity()-orderProductDetailsOld.getIssuedQuantity());
							product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
						}
						else
						{	
							product.setCurrentQuantity(product.getCurrentQuantity()+orderProductDetailsOld.getIssuedQuantity());
						}
						
						product=(Product)sessionFactory.getCurrentSession().merge(product);
						productDAO.Update(product);
						productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetailsOld.getIssuedQuantity(), true);
						//updateOrderUsedProductCurrentQuantity(product);
					}
				}
			}
			
			
			/*if(returnFromDeliveryBoyListOld!=null)
			{
				List<ReturnFromDeliveryBoy> returnFromDeliveryBoyListNewAdd=new ArrayList<>();
				
				//new added changes merge with previous records
				for(int i=0; i<returnFromDeliveryBoyList.size(); i++)
				{
					//boolean inside=false;
					for(ReturnFromDeliveryBoy returnFromDeliveryBoyOld: returnFromDeliveryBoyListOld)
					{
						if(returnFromDeliveryBoyList.get(i).getProduct().getProduct().getProductId()==returnFromDeliveryBoyOld.getProduct().getProduct().getProductId()
								&& returnFromDeliveryBoyList.get(i).getType().equals(returnFromDeliveryBoyOld.getType()))
						{
							//inside=true;
							ReturnFromDeliveryBoy returnFromDeliveryBoy=returnFromDeliveryBoyList.get(i);
							returnFromDeliveryBoy.setIssuedQuantity(returnFromDeliveryBoyOld.getIssuedQuantity());
							returnFromDeliveryBoy.setReturnQuantity(returnFromDeliveryBoyOld.getIssuedQuantity()-returnFromDeliveryBoy.getDeliveryQuantity());
							returnFromDeliveryBoyList.set(i, returnFromDeliveryBoy);
						}
					}
					/*if(inside==false)
					{
						returnFromDeliveryBoyListNewAdd.add(returnFromDeliveryBoyList.get(i));
					}
				}
				//old but not change add
				for(ReturnFromDeliveryBoy returnFromDeliveryBoyOld: returnFromDeliveryBoyListOld)
				{
					boolean inside=false;
					for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
					{
						if(returnFromDeliveryBoy.getProduct().getProduct().getProductId()==returnFromDeliveryBoyOld.getProduct().getProduct().getProductId()
								&& returnFromDeliveryBoy.getType().equals(returnFromDeliveryBoyOld.getType()))
						{
							inside=true;
						}
					}
					if(inside==false)
					{
						returnFromDeliveryBoyListNewAdd.add(returnFromDeliveryBoyOld);
					}
					else
					{
						deleteOrderUsedProduct(returnFromDeliveryBoyOld.getProduct());
					}
					sessionFactory.getCurrentSession().delete(returnFromDeliveryBoyOld);
				}
				returnFromDeliveryBoyList.addAll(returnFromDeliveryBoyListNewAdd);					
			}*/
			
			
			// if returnFromDeliveryBoyList not found as empty then save return from delivery boy details
			if(!returnFromDeliveryBoyList.isEmpty())
			{
				long totalReturnQuantity=0;
				long totalIssuedQuantity=0;
				long totalDeliveryQuantity=0;
				long totalDamageQuantity=0;
				long totalNonDamageQuantity=0;
				
				for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
				{
					totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
					totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
					totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
					totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
					totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
				}
				
				Company company=new Company();
				company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
				
				Branch branch=new Branch();
				branch.setBranchId(getSessionSelectedBranchIds());
				
				ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
						totalReturnQuantity, 
						totalIssuedQuantity, 
						totalDeliveryQuantity, 
						totalDamageQuantity, 
						totalNonDamageQuantity, 
						orderProductIssueDetails.getEmployeeGK(), 
						orderProductIssueDetails.getEmployeeDB(), 
						orderDetails, 
						new Date(), 
						false,
						company,
						branch);
				
				//ReturnFromDeliveryBoyGenerator returnFromDeliveryBoyGenerator=new ReturnFromDeliveryBoyGenerator(sessionFactory);
				returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId());
				sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
		
				for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
				{
					returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
					sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
				}
			}
			//add old product with issue qty zero and amt zero
			//make issued quantity and amount zero when product found as deleted
			List<OrderProductDetails> orderProductDetailsListTemp=new ArrayList<>();
			for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
			{
				boolean productFile=true;
				for(int i=0; i<orderProductDetailsList.size(); i++)
				{
					if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
							&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
					{
						productFile=false;
					}
				}
				
				if(productFile)
				{
					orderProductDetailsOld.setIssueAmount(0);
					orderProductDetailsOld.setIssuedQuantity(0);
					orderProductDetailsListTemp.add(orderProductDetailsOld);
					//deleteOrderUsedProduct(orderProductDetailsOld.getProduct());
				}
			}
			orderProductDetailsList.addAll(orderProductDetailsListTemp);
			
			//add new edited order product details
			
			for(int i=0; i<orderProductDetailsList.size(); i++)
			{
				boolean productHave=false;
				//find product is deleted or not
				for(OrderProductDetails orderProductDetailsOld: orderProductDetailsListOld)
				{
					if(orderProductDetailsOld.getProduct().getProduct().getProductId() == orderProductDetailsList.get(i).getProduct().getProduct().getProductId()
							&& orderProductDetailsOld.getType().equals(orderProductDetailsList.get(i).getType()))
					{
						productHave=true;
					}
				}
				//update current quantity which order product not deleted 
				if(productHave==false)
				{
					product=orderProductDetailsList.get(i).getProduct().getProduct();
					
					if(orderProductDetailsList.get(i).getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						product.setFreeQuantity(product.getFreeQuantity()+orderProductDetailsList.get(i).getIssuedQuantity());
						product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetailsList.get(i).getIssuedQuantity());
					}
					else
					{	
						product.setCurrentQuantity(product.getCurrentQuantity()-orderProductDetailsList.get(i).getIssuedQuantity());
					}
					
					product=(Product)sessionFactory.getCurrentSession().merge(product);
					productDAO.Update(product);
					productDAO.updateDailyStockExchange(product.getProductId(), orderProductDetailsList.get(i).getIssuedQuantity(), false);
					//updateOrderUsedProductCurrentQuantity(product);
				}		
				/*if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					if(findModeOfOrderProduct(orderRequest.getEditModeList(), 
										  orderProductDetailsList.get(i).getProduct().getProduct().getProductId(), 
										  orderProductDetailsList.get(i).getType()).equals(Constants.EDIT_MODE))
					{
						purchaseDataChange=true;
						orderProductDetails=orderProductDetailsList.get(i);
						orderProductDetails.setPurchaseAmount(orderProductDetailsList.get(i).getIssueAmount());
						orderProductDetails.setPurchaseQuantity(orderProductDetailsList.get(i).getIssuedQuantity());
						orderProductDetailsList.set(i,orderProductDetails);
					}				
				}*/
				// save order used category 
				sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct().getCategories());

				// save order used brand
				sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct().getBrand());
				
				// save order used product 
				sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i).getProduct());
				
				// save order product
				sessionFactory.getCurrentSession().save(orderProductDetailsList.get(i));
			}

			double totalPurchaseAmount=0,totalPurchaseAmountWithTax=0;
			long totalPurchaseQuantity=0;
			// if any order product found as edited in order status packed
			//then recalculate main orderdetails total amount,amountwithtax,quantity
			if(purchaseDataChange)
			{
				for(OrderProductDetails orderProductDetails : orderProductDetailsList)
				{
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
					totalPurchaseAmount+=orderProductDetails.getPurchaseQuantity()*calculateProperTaxModel.getUnitprice();//orderProductDetails.getProduct().getRate();
					totalPurchaseAmountWithTax+=orderProductDetails.getPurchaseAmount();
					totalPurchaseQuantity+=orderProductDetails.getPurchaseQuantity();
				}
				orderDetails.setTotalQuantity(totalPurchaseQuantity);
				orderDetails.setTotalAmount(totalPurchaseAmount);			
				orderDetails.setTotalAmountWithTax(totalPurchaseAmountWithTax);
			}			
			//update order details 
			sessionFactory.getCurrentSession().save(orderDetails);			
		}
		//update order used product current quantity with product current quantity
		updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * fetch return from return from delivery boy list by range, startDate, endDate
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return ReturnOrderFromDeliveryBoyReport list
	 * </pre>
	 */
	@Transactional
	public List<ReturnOrderFromDeliveryBoyReport> fetchReturnOrderFromDeliveryBoyReport(String range,String startDate,String endDate){
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from ReturnFromDeliveryBoyMain where (date(date)>='"+startDate+"' And date(date)<='"+endDate+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReturnFromDeliveryBoyMain where date(date)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("today"))
		{
			hql="from ReturnFromDeliveryBoyMain where date(date)=date(CURRENT_DATE())";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from ReturnFromDeliveryBoyMain where date(date)='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from ReturnFromDeliveryBoyMain where  (date(date)>='"+DatePicker.getLastMonthFirstDate()+"' and date(date)<='"+DatePicker.getLastMonthLastDate()+"')";					
		}
		else if(range.equals("last3Months")){
			hql="from ReturnFromDeliveryBoyMain where (date(date)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(date)<='"+DatePicker.getLast3MonthLastDate()+"')";
		}
		else if(range.equals("pickDate")){
			hql="from ReturnFromDeliveryBoyMain where date(date)='"+startDate+"'";
		}
		else if(range.equals("viewAll")){
			hql="from ReturnFromDeliveryBoyMain where 1=1 ";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from ReturnFromDeliveryBoyMain where (date(date) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(date) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
		}
		//App
		else if(range.equals("last1MonthApp")){
			cal.add(Calendar.MONTH, -1);
			hql="from ReturnFromDeliveryBoyMain where date(date)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("last3MonthApp")){
			cal.add(Calendar.MONTH, -3);
			hql="from ReturnFromDeliveryBoyMain where date(date)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("last6MonthApp")){
			cal.add(Calendar.MONTH, -6);
			hql="from ReturnFromDeliveryBoyMain where date(date)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("last1YearApp")){
			cal.add(Calendar.MONTH, -12);
			hql="from ReturnFromDeliveryBoyMain where date(date)>='"+dateFormat.format(cal.getTime())+"'";
		}
		
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+")"
			 +" and branch.branchId in ("+getSessionSelectedBranchIds()+")"
			 +"  order by date desc";
		//hql+=" and orderDetails.businessName.area.areaId in ("+getSessionSelectedIds()+")  order by date desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnFromDeliveryBoyMain> list=(List<ReturnFromDeliveryBoyMain>)query.list();
		
		List<ReturnOrderFromDeliveryBoyReport> returnOrderFromDeliveryBoyReportList=new ArrayList<>();
		long srno=1;
		for(ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain : list)
		{			
			////EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			/*returnOrderFromDeliveryBoyReportList.add(new ReturnOrderFromDeliveryBoyReport(srno,
					,
					returnFromDeliveryBoyMain));*/
			if(returnFromDeliveryBoyMain.getOrderDetails()!=null){
				returnOrderFromDeliveryBoyReportList.add(new ReturnOrderFromDeliveryBoyReport(
						srno,
						returnFromDeliveryBoyMain.getReturnFromDeliveryBoyMainId(), 
						returnFromDeliveryBoyMain.getOrderDetails().getBusinessName().getShopName(), 
						returnFromDeliveryBoyMain.getTotalIssuedQuantity(), 
						returnFromDeliveryBoyMain.getTotalReturnQuantity(), 
						returnFromDeliveryBoyMain.isReceivedStatus(),
						employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyMain.getOrderDetails().getEmployeeSM().getEmployeeId()).getName(),
						returnFromDeliveryBoyMain.getOrderDetails().getBusinessName().getArea().getName(),
						returnFromDeliveryBoyMain.getTotalDeliveryQuantity(),
						returnFromDeliveryBoyMain.getTotalDamageQuantity()
						));
			}else{
				returnOrderFromDeliveryBoyReportList.add(new ReturnOrderFromDeliveryBoyReport(
						srno,
						returnFromDeliveryBoyMain.getReturnFromDeliveryBoyMainId(), 
						(returnFromDeliveryBoyMain.getCounterOrder().getBusinessName()==null)?returnFromDeliveryBoyMain.getCounterOrder().getCustomerName():returnFromDeliveryBoyMain.getCounterOrder().getBusinessName().getShopName(), 
						returnFromDeliveryBoyMain.getTotalIssuedQuantity(), 
						returnFromDeliveryBoyMain.getTotalReturnQuantity(), 
						returnFromDeliveryBoyMain.isReceivedStatus(),
						employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyMain.getCounterOrder().getEmployeeGk().getEmployeeId()).getName(),
						(returnFromDeliveryBoyMain.getCounterOrder().getBusinessName()==null)?"NA":returnFromDeliveryBoyMain.getCounterOrder().getBusinessName().getArea().getName(),
						returnFromDeliveryBoyMain.getTotalDeliveryQuantity(),
						returnFromDeliveryBoyMain.getTotalDamageQuantity()
						));
			}
			
			srno++;
		}
		return returnOrderFromDeliveryBoyReportList;
	}
	
	@Transactional
	public ReturnFromDeliveryBoyMain fetchReturnFromDeliveryBoyMain(String returnFromDeliveryBoyMainId)
	{
		String hql="from ReturnFromDeliveryBoyMain where returnFromDeliveryBoyMainId='"+returnFromDeliveryBoyMainId+"'"+
			       " and company.companyId="+getSessionSelectedCompaniesIds()+
			       " and branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnFromDeliveryBoyMain> list=(List<ReturnFromDeliveryBoyMain>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}
	
	@Transactional
	public List<ReturnFromDeliveryBoy> fetchReturnFromDeliveryBoyList(String returnFromDeliveryBoyMainId)
	{
		String hql="from ReturnFromDeliveryBoy where returnFromDeliveryBoyMain.returnFromDeliveryBoyMainId='"+returnFromDeliveryBoyMainId+"'"+
				" and returnFromDeliveryBoyMain.company.companyId="+getSessionSelectedCompaniesIds()
			   +" and returnFromDeliveryBoyMain.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnFromDeliveryBoy> list=(List<ReturnFromDeliveryBoy>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}
	/**
	 * <pre>
	 * update damage and non damage quantity in ReturnFromDeliveryBoy List and total in ReturnFromDeliveryBoyMain 
	 * If Any Quantity found in damage then its define in damage define and damageRecovery
	 * and non damage quantity added to current quantity product wise
	 * @param orderId
	 * @param returnFromDeliveryBoyList
	 * </pre>
	 */
	@Transactional
	public void updateReturnFromDeliveryBoy(String orderId,String returnFromDeliveryBoyList)
	{
		ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain= fetchReturnFromDeliveryBoyMain(orderId);
		returnFromDeliveryBoyMain.setReceivedStatus(true);
		
		long totalReturnQuantity=0;
		long totalIssuedQuantity=0;
		long totalDeliveryQuantity=0;
		long totalDamageQuantity=0;
		long totalNonDamageQuantity=0;
		
		List<ReturnFromDeliveryBoy> returnFromDeliveryBoyListDB=fetchReturnFromDeliveryBoyList(orderId);
		String returnFromDeliveryBoy[]=returnFromDeliveryBoyList.split("-");
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		for(int i=0; i<returnFromDeliveryBoy.length; i++)
		{
			String returnFromDeliveryBoySingle[]=returnFromDeliveryBoy[i].split(",");
			ReturnFromDeliveryBoy returnProduct=new ReturnFromDeliveryBoy();
			for(ReturnFromDeliveryBoy returnProducts: returnFromDeliveryBoyListDB)
			{
				if(returnProducts.getReturnFromDeliveryBoyId()==Integer.parseInt(returnFromDeliveryBoySingle[0]))
				{
					returnProduct=returnProducts;
				}
			}
			
			returnProduct.setDamageQuantity(Integer.parseInt(returnFromDeliveryBoySingle[1]));
			returnProduct.setNonDamageQuantity(Integer.parseInt(returnFromDeliveryBoySingle[2]));
			
			product=productDAO.fetchProductForWebApp(returnProduct.getProduct().getProduct().getProductId());
			
			if(returnProduct.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
			{
				product.setFreeQuantity(product.getFreeQuantity()-returnProduct.getNonDamageQuantity());
				product.setCurrentQuantity(product.getCurrentQuantity()+returnProduct.getNonDamageQuantity());
			}
			else
			{	
				product.setCurrentQuantity(product.getCurrentQuantity()+returnProduct.getNonDamageQuantity());
			}
			
			product.setDamageQuantity(product.getDamageQuantity()+returnProduct.getDamageQuantity());
			productDAO.updateDailyStockExchange(product.getProductId(), returnProduct.getDamageQuantity(), true);
			productDAO.Update(product);
			productDAO.updateDailyStockExchange(product.getProductId(), returnProduct.getNonDamageQuantity(), true);
			
			
			totalReturnQuantity+=returnProduct.getReturnQuantity();
			totalIssuedQuantity+=returnProduct.getIssuedQuantity();
			totalDeliveryQuantity+=returnProduct.getDeliveryQuantity();
			totalDamageQuantity+=returnProduct.getDamageQuantity();
			totalNonDamageQuantity+=returnProduct.getNonDamageQuantity();
			
			
			if(returnProduct.getDamageQuantity()>0){
				
				//here damage recovery save or update DamageRecoveryDayWise table using damage quantity
				productDAO.saveUpdateDamageRecoveryMonthWise(
										product.getProductId(),
										returnProduct.getDamageQuantity());
				
				EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnProduct.getReturnFromDeliveryBoyMain().getEmployeeDB().getEmployeeId());
				//damagedefine for report
				Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
				DamageDefine damageDefine=new DamageDefine( orderId, 
						product, 
						employeeDetails.getName(),
						employeeDetails.getEmployee().getDepartment().getName(), 
						returnProduct.getDamageQuantity(), 
						new Date(), 
						returnFromDeliveryBoySingle[3],
						branch);
				productDAO.saveDamageDefine(damageDefine);
				

				returnProduct.setDamageQuantityReason(returnFromDeliveryBoySingle[3]);
			}else{
				returnProduct.setDamageQuantityReason(null);
			}
			
			returnProduct=(ReturnFromDeliveryBoy)sessionFactory.getCurrentSession().merge(returnProduct);
			sessionFactory.getCurrentSession().update(returnProduct);
		}
		
		returnFromDeliveryBoyMain.setTotalDamageQuantity(totalDamageQuantity);
		returnFromDeliveryBoyMain.setTotalDeliveryQuantity(totalDeliveryQuantity);
		returnFromDeliveryBoyMain.setTotalIssuedQuantity(totalIssuedQuantity);
		returnFromDeliveryBoyMain.setTotalNonDamageQuantity(totalNonDamageQuantity);
		returnFromDeliveryBoyMain.setTotalReturnQuantity(totalReturnQuantity);
		returnFromDeliveryBoyMain.setReceivedStatus(true);
		
		returnFromDeliveryBoyMain=(ReturnFromDeliveryBoyMain)sessionFactory.getCurrentSession().merge(returnFromDeliveryBoyMain);
		sessionFactory.getCurrentSession().update(returnFromDeliveryBoyMain);
		
		updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * update damage and non damage quantity in ReturnFromDeliveryBoy List and total in ReturnFromDeliveryBoyMain 
	 * If Any Quantity found in damage then its define in damage define and damageRecovery
	 * and non damage quantity added to current quantity product wise
	 * @param orderId
	 * @param returnFromDeliveryBoyList
	 * </pre>
	 */
	@Transactional
	public void updateReturnFromDeliveryBoyForApp(List<ReturnFromDeliveryBoyModel>  returnFromDeliveryBoyList,String returnFromDeliveryBoyMainId)
	{
			
		ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=fetchReturnFromDeliveryBoyMain(returnFromDeliveryBoyMainId);
		//List<ReturnFromDeliveryBoy> returnFromDeliveryBoyListOld=fetchReturnFromDeliveryBoy(returnFromDeliveryBoyMainId);
		
		long totalReturnQuantity=0;
		long totalIssuedQuantity=0;
		long totalDeliveryQuantity=0;
		long totalDamageQuantity=0;
		long totalNonDamageQuantity=0;
		
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		for(ReturnFromDeliveryBoyModel returnFromDeliveryBoyModel: returnFromDeliveryBoyList)
		{			
			ReturnFromDeliveryBoy returnFromDeliveryBoy=(ReturnFromDeliveryBoy)sessionFactory.getCurrentSession().get(ReturnFromDeliveryBoy.class, returnFromDeliveryBoyModel.getReturnFromDeliveryBoyId());
			product=productDAO.fetchProductForWebApp(returnFromDeliveryBoyModel.getProductId());
			
			if(returnFromDeliveryBoyModel.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
			{
				product.setFreeQuantity(product.getFreeQuantity()-returnFromDeliveryBoyModel.getNonDamageQuantity());
				product.setCurrentQuantity(product.getCurrentQuantity()+returnFromDeliveryBoyModel.getNonDamageQuantity());
			}
			else
			{	
				product.setCurrentQuantity(product.getCurrentQuantity()+returnFromDeliveryBoyModel.getNonDamageQuantity());
			}
			
			product.setDamageQuantity(product.getDamageQuantity()+returnFromDeliveryBoyModel.getDamageQuantity());
			
			productDAO.updateDailyStockExchange(product.getProductId(), returnFromDeliveryBoyModel.getDamageQuantity(), true);
			
			productDAO.Update(product);			
			productDAO.updateDailyStockExchange(product.getProductId(), returnFromDeliveryBoyModel.getNonDamageQuantity(), true);
			
			totalReturnQuantity+=returnFromDeliveryBoyModel.getReturnQuantity();
			totalIssuedQuantity+=returnFromDeliveryBoyModel.getIssuedQuantity();
			totalDeliveryQuantity+=returnFromDeliveryBoyModel.getDeliveryQuantity();
			totalDamageQuantity+=returnFromDeliveryBoyModel.getDamageQuantity();
			totalNonDamageQuantity+=returnFromDeliveryBoyModel.getNonDamageQuantity();
			
			returnFromDeliveryBoy.setDamageQuantity(returnFromDeliveryBoyModel.getDamageQuantity());
			returnFromDeliveryBoy.setNonDamageQuantity(returnFromDeliveryBoyModel.getNonDamageQuantity());
			returnFromDeliveryBoy.setDamageQuantityReason(returnFromDeliveryBoyModel.getReason());
			
			returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
			
			returnFromDeliveryBoy=(ReturnFromDeliveryBoy)sessionFactory.getCurrentSession().merge(returnFromDeliveryBoy);
			sessionFactory.getCurrentSession().update(returnFromDeliveryBoy);
			
			//returnFromDeliveryBoyMain = (ReturnFromDeliveryBoyMain) sessionFactory.getCurrentSession().get(ReturnFromDeliveryBoyMain.class, new Long(returnFromDeliveryBoyMain.getReturnFromDeliveryBoyMainPkId()));
						
			if(returnFromDeliveryBoyModel.getDamageQuantity()>0){
				
				//here damage recovery save or update DamageRecoveryDayWise table using damage quantity
				productDAO.saveUpdateDamageRecoveryMonthWise(
										product.getProductId(),
										returnFromDeliveryBoyModel.getDamageQuantity());
				
				EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyMain.getEmployeeDB().getEmployeeId());
				//damagedefine for report
				Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
				DamageDefine damageDefine=new DamageDefine( returnFromDeliveryBoyMain.getOrderDetails().getOrderId(), 
						product, 
						employeeDetails.getName(),
						employeeDetails.getEmployee().getDepartment().getName(),  
						returnFromDeliveryBoyModel.getDamageQuantity(), 
						new Date(), 
						returnFromDeliveryBoyModel.getReason(),
						branch);
				productDAO.saveDamageDefine(damageDefine);
			}
		}
		
		returnFromDeliveryBoyMain.setTotalDamageQuantity(totalDamageQuantity);
		returnFromDeliveryBoyMain.setTotalDeliveryQuantity(totalDeliveryQuantity);
		returnFromDeliveryBoyMain.setTotalIssuedQuantity(totalIssuedQuantity);
		returnFromDeliveryBoyMain.setTotalNonDamageQuantity(totalNonDamageQuantity);
		returnFromDeliveryBoyMain.setTotalReturnQuantity(totalReturnQuantity);
		returnFromDeliveryBoyMain.setReceivedStatus(true);
		
		returnFromDeliveryBoyMain=(ReturnFromDeliveryBoyMain)sessionFactory.getCurrentSession().merge(returnFromDeliveryBoyMain);
		sessionFactory.getCurrentSession().update(returnFromDeliveryBoyMain);
				
		//update order used product damange quantity and current quantity from product entity
		updateOrderUsedProductCurrentQuantity();
	}
	/**
	 * <pre>
	 * fetch gst report from sales man order and counter order
	 * gst report showing can be in split or merge mode
	 * split : product wise
	 * merge : invoice wise
	 * @param startDate
	 * @param endDate
	 * @param type
	 * @return GstBillReport
	 * </pre>
	 */
	@Transactional
	public GstBillReport fetchGstBillReport(String startDate,String endDate,String type)
	{
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
		
		String salqHql,purchaseHql;
		Query saleQuery,purchaseQuery;
		
		List<SaleTable> saleTableList=new ArrayList<>();
		List<PurchaseTable> purchaseTableList=new ArrayList<>();
		
		List<SaleAndPurchaseTableMerge> saleMergeTableList=new ArrayList<>();
		List<SaleAndPurchaseTableMerge> purchaseMergeTableList=new ArrayList<>();
		
		double saleTotalTaxableAmt=0;
		double saleCgstAmt=0;
		double saleSgstAmt=0;
		double saleIgstAmt=0;
		double saleFinalAmt=0;
		
		double purchaseTotalTaxableAmt=0;
		double purchaseCgstAmt=0;
		double purchaseSgstAmt=0;
		double purchaseIgstAmt=0;
		double purchaseFinalAmt=0;
		
		salqHql="from OrderDetails where date(confirmDate)>='"+startDate+"' and date(confirmDate)<='"+endDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by confirmDate";
		//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by confirmDate";
		purchaseHql="from Inventory where date(inventoryAddedDatetime)>='"+startDate+"' and date(inventoryAddedDatetime)<='"+endDate+"'"+
				" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and supplier.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by inventoryAddedDatetime";
		///*" and supplier.area.areaId in ("+getSessionSelectedIds()+") "+*/" order by inventoryAddedDatetime";
		
		saleQuery=sessionFactory.getCurrentSession().createQuery(salqHql);
		purchaseQuery=sessionFactory.getCurrentSession().createQuery(purchaseHql);
		
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)saleQuery.list();
		List<Inventory> inventoryList=(List<Inventory>)purchaseQuery.list();
		
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null,"range", startDate, endDate);
		
		DecimalFormat decimalFormatTwoDigit=new DecimalFormat("#0.00");
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#0.000");
		
		double taxableAmount=0;
		double cgstAmt=0;
		double sgstAmt=0;
		double igstAmt=0;
		double totalAmount=0;
		
		double totalTaxableAmount=0;
		double totalCgstAmt=0;
		double totalSgstAmt=0;
		double totalIgstAmt=0;
		double totalTotalAmount=0;
		
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		if(type.trim().equals("split"))
		{
			for(OrderDetails orderDetails: orderDetailsList)
			{
				List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				
				for(OrderProductDetails orderProductDetails: orderProductDetailsList)
				{
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
					
					if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						taxableAmount=0;
						igstAmt=0;
						cgstAmt=0;
						sgstAmt=0;
						totalAmount=0;
					}
					else
					{
						taxableAmount=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
						igstAmt=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
						cgstAmt=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
						sgstAmt=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
						totalAmount=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
					}
					
					saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));				
					saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
					
					if(orderDetails.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
						
						saleCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt));
						saleSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt));
						saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
						
						saleTableList.add(new SaleTable(0, 
														simpleDateFormat.format(orderDetails.getConfirmDate()), 
														orderProductDetails, 
														Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
														Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
														Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
														0, 
														Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}else{
						
						saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
						saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
						saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
						
						saleTableList.add(new SaleTable(0, 
								simpleDateFormat.format(orderDetails.getConfirmDate()), 
								orderProductDetails, 
								Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
								0, 
								0, 
								Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}
				}
			}
			
			//Counter order
			if(counterOrderList!=null){
				for(CounterOrder counterOrder : counterOrderList){
										
					List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
					
					/* for new implement logic diff*/
					double totalOrderAmount=0;
					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){
						totalOrderAmount+=counterOrderProductDetails.getSellingRate()*(counterOrderProductDetails.getPurchaseQuantity()+counterOrderProductDetails.getReturnQuantity());
					}
					
					if(counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
						totalOrderAmount-=counterOrder.getDiscount();
					}else{
						totalOrderAmount=
								(totalOrderAmount)-
								(
										totalOrderAmount * (counterOrder.getDiscount()/100)
								);
					}
					
					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){

						/* when quantity zero(for return) then not show in gst report*/
						if(counterOrderProductDetails.getPurchaseQuantity()==0){
							continue;
						}
						
//						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
//								counterOrderProductDetails.getSellingRate(), 
//								counterOrderProductDetails.getProduct().getCategories().getIgst());
						
						if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
						{
							taxableAmount=0;
							igstAmt=0;
							cgstAmt=0;
							sgstAmt=0;
							totalAmount=0;
						}
						else
						{
							/* discount basis calculate amount */
							totalAmount=counterOrderProductDetails.getPurchaseAmount();
							//double discountPerCut = counterOrder.getDiscount();
							double totalOrderAmountNet = totalOrderAmount;
							
							if(counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
								totalOrderAmountNet=(totalOrderAmount+counterOrder.getDiscount());
							}else{
								totalOrderAmountNet = (
										totalOrderAmount /
											(
													1-(counterOrder.getDiscount()/100)
											)
										);
							}
							
							totalAmount=(
									totalOrderAmount/
									totalOrderAmountNet
									)*totalAmount;
														
							CalculateProperTaxModel taxModel= productDAO.calculateProperAmountModel(totalAmount, counterOrderProductDetails.getProduct().getCategories().getIgst());
							taxableAmount=taxModel.getUnitprice();
							igstAmt=taxModel.getIgst();
							cgstAmt=taxModel.getCgst();
							sgstAmt=taxModel.getSgst();
							totalAmount=taxModel.getMrp();
						}
						
						
						
						saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));				
						saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
						
						if(counterOrder.getBusinessName()!=null){
							if(counterOrder.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
			 					
								saleCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt));
								saleSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt));
								saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
								
								saleTableList.add(new SaleTable(0, 
																simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
																counterOrderProductDetails, 
																Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
																Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
																Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
																0, 
																Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
							}else{
								
								saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
								saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
								saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
								
								saleTableList.add(new SaleTable(0, 
										simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
										counterOrderProductDetails, 
										Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
										0, 
										0, 
										Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
										Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
							}
						}else{//customer assumed as Intra Tax type
							saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
							saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
							saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
							
							saleTableList.add(new SaleTable(0, 
															simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
															counterOrderProductDetails, 
															Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
															Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
															Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
															0, 
															Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
						}
					}						
				}
			}
			
			//InventoryDAOImpl inventoryDAO=new InventoryDAOImpl(sessionFactory);
			for(Inventory inventory: inventoryList)
			{
				List<InventoryDetails> inventoryDetailsList=inventoryDAO.fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
				
				/* for new implement logic diff*/
				double totalInventoryAmount=inventory.getTotalAmountTax();
				
				for(InventoryDetails inventoryDetails: inventoryDetailsList)
				{
					
					/* discount basis calculate amount */
					totalAmount=inventoryDetails.getAmount();
					//double discountPerCut = counterOrder.getDiscount();
					double totalInventoryAmountNet = totalInventoryAmount;
					
					if(inventory.getDiscountGiven()){
						if(inventory.getDiscountType().equals("AMOUNT")){
							totalInventoryAmountNet=(totalInventoryAmount+inventory.getDiscountAmount());
						}else{
							totalInventoryAmountNet = (
									totalInventoryAmount /
										(
												1-(inventory.getDiscountPercentage()/100)
										)
									);
						}
					}
					
					totalAmount=(
							totalInventoryAmount/
							totalInventoryAmountNet
							)*totalAmount;
					
					double rate=0;
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
							totalAmount, 
							inventoryDetails.getProduct().getCategories().getIgst());
					/*for(SupplierProductList supplierProduct: supplierProductList)
					{
						if(inventoryDetails.getProduct().getProduct().getProductId()==supplierProduct.getProduct().getProductId())
						{
							rate=supplierProduct.getSupplierRate();
						}
					}*/
					taxableAmount=calculateProperTaxModel.getUnitprice();
					igstAmt=calculateProperTaxModel.getIgst();
					cgstAmt=calculateProperTaxModel.getCgst();
					sgstAmt=calculateProperTaxModel.getSgst();
					totalAmount=calculateProperTaxModel.getMrp();
					
					purchaseTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));
					purchaseCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt));
					purchaseSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt));
					purchaseIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
					purchaseFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
										
					if(inventory.getSupplier().getTaxType().equals(Constants.INTRA_STATUS)){
						purchaseTableList.add(new PurchaseTable(0, 
								simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
								inventoryDetails, 
								Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
								Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
								Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
								0, //Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}else{
						purchaseTableList.add(new PurchaseTable(0, 
								simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
								inventoryDetails, 
								Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
								0,//Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt)), 
								0,//Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}
				}				
			}
			
			return new GstBillReport(saleTableList, 
									purchaseTableList, 
									saleMergeTableList,
									purchaseMergeTableList,
									Double.parseDouble(decimalFormatTwoDigit.format(saleTotalTaxableAmt)),
									Double.parseDouble(decimalFormatThreeDigit.format(saleCgstAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(saleSgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleFinalAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseTotalTaxableAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(purchaseCgstAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(purchaseSgstAmt)), 
									0,//Double.parseDouble(decimalFormatTwoDigit.format(purchaseIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseFinalAmt)));
		}
		else
		{
			/*Merge Report*/
			
			for(OrderDetails orderDetails: orderDetailsList)
			{
				List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				
				totalTaxableAmount=0;
				totalCgstAmt=0;
				totalSgstAmt=0;
				totalIgstAmt=0;
				totalTotalAmount=0;
			
				totalTaxableAmount=orderDetails.getTotalAmount();
				totalIgstAmt=orderDetails.getTotalAmountWithTax()-orderDetails.getTotalAmount();
				totalCgstAmt=totalIgstAmt/2;
				totalSgstAmt=totalIgstAmt/2;
				totalTotalAmount=orderDetails.getTotalAmountWithTax();
				
				/*for(OrderProductDetails orderProductDetails: orderProductDetailsList)
				{			
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
					if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						taxableAmount=0;
						igstAmt=0;
						cgstAmt=0;
						sgstAmt=0;
						totalAmount=0;
					}
					else
					{
						taxableAmount=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
						igstAmt=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
						cgstAmt=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
						sgstAmt=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
						totalAmount=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
					}
					
					totalTaxableAmount+=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
					totalIgstAmt+=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
					totalCgstAmt+=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
					totalSgstAmt+=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
					totalTotalAmount+=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
				}*/				

				saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
				
				saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
				
				if(orderDetails.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
					
					saleCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt));
					saleSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt));
					saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
					
					saleMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(orderDetails.getConfirmDate()), 
							orderDetails.getInvoiceNumber(), 
							orderDetails.getBusinessName().getShopName(), 
							orderDetails.getBusinessName().getGstinNumber(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt)), 
							Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt)), 
							0, 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}else{
					
					saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
					saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
					saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
					
					saleMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(orderDetails.getConfirmDate()), 
							orderDetails.getInvoiceNumber(), 
							orderDetails.getBusinessName().getShopName(), 
							orderDetails.getBusinessName().getGstinNumber(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							0, 
							0, 
							Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}
			}
			
			totalTaxableAmount=0;
			totalCgstAmt=0;
			totalSgstAmt=0;
			totalIgstAmt=0;
			totalTotalAmount=0;
			
			//Counter order
			if(counterOrderList!=null){
				for(CounterOrder counterOrder : counterOrderList){
										
					totalTaxableAmount=0;
					totalIgstAmt=0;
					totalCgstAmt=0;
					totalSgstAmt=0;
					totalTotalAmount=0;
				
					totalTaxableAmount=counterOrder.getTotalAmount();
					totalIgstAmt=counterOrder.getTotalAmountWithTax()-counterOrder.getTotalAmount();
					totalCgstAmt=totalIgstAmt/2;
					totalSgstAmt=totalIgstAmt/2;
					totalTotalAmount=counterOrder.getTotalAmountWithTax();
					
//					List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
//					
//					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){
//						double totalAmountTemp=counterOrderProductDetails.getPurchaseAmount();
//						CalculateProperTaxModel taxModel= productDAO.calculateProperAmountModel(totalAmountTemp, counterOrderProductDetails.getProduct().getCategories().getIgst());
//						totalTaxableAmount+=taxModel.getUnitprice();
//						totalIgstAmt+=taxModel.getIgst();
//						totalCgstAmt+=taxModel.getCgst();
//						totalSgstAmt+=taxModel.getSgst();
//						totalTotalAmount+=taxModel.getMrp();
//					}
					
//					List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
//					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){			
//						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
//																		counterOrderProductDetails.getSellingRate(), 
//																		counterOrderProductDetails.getProduct().getCategories().getIgst());
//						if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
//						{
//							taxableAmount=0;
//							igstAmt=0;
//							cgstAmt=0;
//							sgstAmt=0;
//							totalAmount=0;
//						}
//						else
//						{
//							totalAmount=counterOrderProductDetails.getPurchaseAmount();
//							
//							taxableAmount=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
//							igstAmt=calculateProperTaxModel.getIgst()*counterOrderProductDetails.getPurchaseQuantity();
//							cgstAmt=calculateProperTaxModel.getCgst()*counterOrderProductDetails.getPurchaseQuantity();
//							sgstAmt=calculateProperTaxModel.getSgst()*counterOrderProductDetails.getPurchaseQuantity();
//							totalAmount=calculateProperTaxModel.getMrp()*counterOrderProductDetails.getPurchaseQuantity();
//						}
//						
//						totalTaxableAmount+=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
//						totalIgstAmt+=calculateProperTaxModel.getIgst()*counterOrderProductDetails.getPurchaseQuantity();
//						totalCgstAmt+=calculateProperTaxModel.getCgst()*counterOrderProductDetails.getPurchaseQuantity();
//						totalSgstAmt+=calculateProperTaxModel.getSgst()*counterOrderProductDetails.getPurchaseQuantity();
//						totalTotalAmount+=calculateProperTaxModel.getMrp()*counterOrderProductDetails.getPurchaseQuantity();
//					}	
					
					saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
					
					saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
					
					if(counterOrder.getBusinessName()!=null){
						if(counterOrder.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
							
							saleCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt));
							saleSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt));
							saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
							
							saleMergeTableList.add(new SaleAndPurchaseTableMerge(
									simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
									counterOrder.getInvoiceNumber(), 
									counterOrder.getBusinessName().getShopName(), 
									counterOrder.getBusinessName().getGstinNumber(),  
									Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
									Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt)), 
									0, 
									Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
						}else{
							
							saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
							saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
							saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
							
							saleMergeTableList.add(new SaleAndPurchaseTableMerge(
									simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
									counterOrder.getInvoiceNumber(), 
									counterOrder.getBusinessName().getShopName(), 
									counterOrder.getBusinessName().getGstinNumber(),  
									Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
									0, 
									0, 
									Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
						}
					}else{//external customer - assumed its Intra Taxtype
						saleCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt));
						saleSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt));
						saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
						
						saleMergeTableList.add(new SaleAndPurchaseTableMerge(
								simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
								counterOrder.getInvoiceNumber(), 
								counterOrder.getCustomerName(), 
								counterOrder.getCustomerGstNumber(),  
								Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
								Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt)), 
								Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt)), 
								0, 
								Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
					}
				}
			}
			
			totalTaxableAmount=0;
			totalCgstAmt=0;
			totalSgstAmt=0;
			totalIgstAmt=0;
			totalTotalAmount=0;
			
			//InventoryDAOImpl inventoryDAO=new InventoryDAOImpl(sessionFactory);
			for(Inventory inventory: inventoryList)
			{
				List<InventoryDetails> inventoryDetailsList=inventoryDAO.fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
				
				totalTaxableAmount=0;
				totalCgstAmt=0;
				totalSgstAmt=0;
				totalIgstAmt=0;
				totalTotalAmount=0;
				
				totalTaxableAmount=inventory.getTotalAmount();
				totalIgstAmt=inventory.getTotalAmountTax()-inventory.getTotalAmount();
				totalCgstAmt=totalIgstAmt/2;
				totalSgstAmt=totalIgstAmt/2;
				totalTotalAmount=inventory.getTotalAmountTax();
				
				totalIgstAmt=0;
				
				if(inventory.getSupplier().getTaxType().equals(Constants.INTRA_STATUS)){
					purchaseMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
							inventory.getInventoryTransactionId(),
							inventory.getSupplier().getName(), 
							inventory.getSupplier().getGstinNo(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt)), 
							Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt)), 
							0,//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}else{
					purchaseMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
							inventory.getInventoryTransactionId(),
							inventory.getSupplier().getName(), 
							inventory.getSupplier().getGstinNo(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							0,//Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt)), 
							0,//Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}
//				for(InventoryDetails inventoryDetails: inventoryDetailsList)
//				{
//					
//					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
//							inventoryDetails.getRate(), 
//							inventoryDetails.getProduct().getCategories().getIgst());
//					/*for(SupplierProductList supplierProduct: supplierProductList)
//					{
//						if(inventoryDetails.getProduct().getProduct().getProductId()==supplierProduct.getProduct().getProductId())
//						{
//							rate=supplierProduct.getSupplierRate();
//						}
//					}*/
//					taxableAmount=calculateProperTaxModel.getUnitprice()*inventoryDetails.getQuantity();
//					igstAmt=calculateProperTaxModel.getIgst()*inventoryDetails.getQuantity();
//					cgstAmt=calculateProperTaxModel.getCgst()*inventoryDetails.getQuantity();
//					sgstAmt=calculateProperTaxModel.getSgst()*inventoryDetails.getQuantity();
//					totalAmount=calculateProperTaxModel.getMrp()*inventoryDetails.getQuantity();
//					
//					totalTaxableAmount+=calculateProperTaxModel.getUnitprice()*inventoryDetails.getQuantity();
//					totalIgstAmt+=calculateProperTaxModel.getIgst()*inventoryDetails.getQuantity();
//					totalCgstAmt+=calculateProperTaxModel.getCgst()*inventoryDetails.getQuantity();
//					totalSgstAmt+=calculateProperTaxModel.getSgst()*inventoryDetails.getQuantity();
//					totalTotalAmount+=calculateProperTaxModel.getMrp()*inventoryDetails.getQuantity();
//				}			
				
				purchaseTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
				purchaseCgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalCgstAmt));
				purchaseSgstAmt+=Double.parseDouble(decimalFormatThreeDigit.format(totalSgstAmt));
				purchaseIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
				purchaseFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
				
//				purchaseMergeTableList.add(new SaleAndPurchaseTableMerge(
//						simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
//						inventory.getInventoryTransactionId(), 
//						inventory.getSupplier().getName(), 
//						inventory.getSupplier().getGstinNo(),  
//						Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
//						Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt)), 
//						Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt)), 
//						0,  //Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
//						Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
			}
			
			return new GstBillReport(saleTableList, 
									purchaseTableList, 
									saleMergeTableList,
									purchaseMergeTableList,
									Double.parseDouble(decimalFormatTwoDigit.format(saleTotalTaxableAmt)),
									Double.parseDouble(decimalFormatThreeDigit.format(saleCgstAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(saleSgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleFinalAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseTotalTaxableAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(purchaseCgstAmt)), 
									Double.parseDouble(decimalFormatThreeDigit.format(purchaseSgstAmt)), 
									0,//Double.parseDouble(decimalFormatTwoDigit.format(purchaseIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseFinalAmt)));
		
		}
	}
	
	@Transactional
	public GstBillReport fetchGstBillReport1(String startDate,String endDate,String type)
	{
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
		
		String salqHql,purchaseHql;
		Query saleQuery,purchaseQuery;
		
		List<SaleTable> saleTableList=new ArrayList<>();
		List<PurchaseTable> purchaseTableList=new ArrayList<>();
		
		List<SaleAndPurchaseTableMerge> saleMergeTableList=new ArrayList<>();
		List<SaleAndPurchaseTableMerge> purchaseMergeTableList=new ArrayList<>();
		
		double saleTotalTaxableAmt=0;
		double saleCgstAmt=0;
		double saleSgstAmt=0;
		double saleIgstAmt=0;
		double saleFinalAmt=0;
		
		double purchaseTotalTaxableAmt=0;
		double purchaseCgstAmt=0;
		double purchaseSgstAmt=0;
		double purchaseIgstAmt=0;
		double purchaseFinalAmt=0;
		
		salqHql="from OrderDetails where date(confirmDate)>='"+startDate+"' and date(confirmDate)<='"+endDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by confirmDate";
		//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by confirmDate";
		purchaseHql="from Inventory where date(inventoryAddedDatetime)>='"+startDate+"' and date(inventoryAddedDatetime)<='"+endDate+"'"+
				" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and supplier.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by inventoryAddedDatetime";
		///*" and supplier.area.areaId in ("+getSessionSelectedIds()+") "+*/" order by inventoryAddedDatetime";
		
		saleQuery=sessionFactory.getCurrentSession().createQuery(salqHql);
		purchaseQuery=sessionFactory.getCurrentSession().createQuery(purchaseHql);
		
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)saleQuery.list();
		List<Inventory> inventoryList=(List<Inventory>)purchaseQuery.list();
		
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null,"range", startDate, endDate);
		
		DecimalFormat decimalFormatTwoDigit=new DecimalFormat("#0.00");
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#0.000");
		
		double taxableAmount=0;
		double cgstAmt=0;
		double sgstAmt=0;
		double igstAmt=0;
		double totalAmount=0;
		
		double totalTaxableAmount=0;
		double totalCgstAmt=0;
		double totalSgstAmt=0;
		double totalIgstAmt=0;
		double totalTotalAmount=0;
		
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		if(type.trim().equals("split"))
		{
			for(OrderDetails orderDetails: orderDetailsList)
			{
				List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				
				for(OrderProductDetails orderProductDetails: orderProductDetailsList)
				{
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
					
					if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						taxableAmount=0;
						igstAmt=0;
						cgstAmt=0;
						sgstAmt=0;
						totalAmount=0;
					}
					else
					{
						taxableAmount=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
						igstAmt=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
						cgstAmt=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
						sgstAmt=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
						totalAmount=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
					}
					
					saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));				
					saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
					
					if(orderDetails.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
						
						saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
						saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
						saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
						
						saleTableList.add(new SaleTable(0, 
														simpleDateFormat.format(orderDetails.getConfirmDate()), 
														orderProductDetails, 
														Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
														Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
														Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
														0, 
														Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}else{
						
						saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
						saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
						saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
						
						saleTableList.add(new SaleTable(0, 
								simpleDateFormat.format(orderDetails.getConfirmDate()), 
								orderProductDetails, 
								Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
								0, 
								0, 
								Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
					}
				}
			}
			
			//Counter order
			if(counterOrderList!=null){
				for(CounterOrder counterOrder : counterOrderList){
					List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){

						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
								counterOrderProductDetails.getSellingRate(), 
								counterOrderProductDetails.getProduct().getCategories().getIgst());
						
						if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
						{
							taxableAmount=0;
							igstAmt=0;
							cgstAmt=0;
							sgstAmt=0;
							totalAmount=0;
						}
						else
						{
							taxableAmount=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
							igstAmt=calculateProperTaxModel.getIgst()*counterOrderProductDetails.getPurchaseQuantity();
							cgstAmt=calculateProperTaxModel.getCgst()*counterOrderProductDetails.getPurchaseQuantity();
							sgstAmt=calculateProperTaxModel.getSgst()*counterOrderProductDetails.getPurchaseQuantity();
							totalAmount=calculateProperTaxModel.getMrp()*counterOrderProductDetails.getPurchaseQuantity();
						}
						
						saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));				
						saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
						
						if(counterOrder.getBusinessName()!=null){
							if(counterOrder.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
								
								saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
								saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
								saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
								
								saleTableList.add(new SaleTable(0, 
																simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
																counterOrderProductDetails, 
																Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
																Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
																Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
																0, 
																Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
							}else{
								
								saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
								saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
								saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
								
								saleTableList.add(new SaleTable(0, 
										simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
										counterOrderProductDetails, 
										Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
										0, 
										0, 
										Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
										Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
							}
						}else{//customer assumed as Intra Tax type
							saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
							saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
							saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
							
							saleTableList.add(new SaleTable(0, 
															simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
															counterOrderProductDetails, 
															Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
															Double.parseDouble(decimalFormatThreeDigit.format(cgstAmt)), 
															Double.parseDouble(decimalFormatThreeDigit.format(sgstAmt)), 
															0, 
															Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
						}
					}						
				}
			}
			
			//InventoryDAOImpl inventoryDAO=new InventoryDAOImpl(sessionFactory);
			for(Inventory inventory: inventoryList)
			{
				List<InventoryDetails> inventoryDetailsList=inventoryDAO.fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
				
				for(InventoryDetails inventoryDetails: inventoryDetailsList)
				{
					double rate=0;
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
							inventoryDetails.getRate(), 
							inventoryDetails.getProduct().getCategories().getIgst());
					/*for(SupplierProductList supplierProduct: supplierProductList)
					{
						if(inventoryDetails.getProduct().getProduct().getProductId()==supplierProduct.getProduct().getProductId())
						{
							rate=supplierProduct.getSupplierRate();
						}
					}*/
					taxableAmount=calculateProperTaxModel.getUnitprice()*inventoryDetails.getQuantity();
					igstAmt=calculateProperTaxModel.getIgst()*inventoryDetails.getQuantity();
					cgstAmt=calculateProperTaxModel.getCgst()*inventoryDetails.getQuantity();
					sgstAmt=calculateProperTaxModel.getSgst()*inventoryDetails.getQuantity();
					totalAmount=calculateProperTaxModel.getMrp()*inventoryDetails.getQuantity();
					
					purchaseTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount));
					purchaseCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt));
					purchaseSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt));
					purchaseIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(igstAmt));
					purchaseFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalAmount));
										
					purchaseTableList.add(new PurchaseTable(0, 
							simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
							inventoryDetails, 
							Double.parseDouble(decimalFormatTwoDigit.format(taxableAmount)), 
							Double.parseDouble(decimalFormatTwoDigit.format(cgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(sgstAmt)), 
							0, //Double.parseDouble(decimalFormatTwoDigit.format(igstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalAmount))));
				}				
			}
			
			return new GstBillReport(saleTableList, 
									purchaseTableList, 
									saleMergeTableList,
									purchaseMergeTableList,
									Double.parseDouble(decimalFormatTwoDigit.format(saleTotalTaxableAmt)),
									Double.parseDouble(decimalFormatTwoDigit.format(saleCgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleSgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleFinalAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseTotalTaxableAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseCgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseSgstAmt)), 
									0,//Double.parseDouble(decimalFormatTwoDigit.format(purchaseIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseFinalAmt)));
		}
		else
		{
			for(OrderDetails orderDetails: orderDetailsList)
			{
				List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				
				totalTaxableAmount=0;
				totalCgstAmt=0;
				totalSgstAmt=0;
				totalIgstAmt=0;
				totalTotalAmount=0;
				
				for(OrderProductDetails orderProductDetails: orderProductDetailsList)
				{			
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
					if(orderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
					{
						taxableAmount=0;
						igstAmt=0;
						cgstAmt=0;
						sgstAmt=0;
						totalAmount=0;
					}
					else
					{
						taxableAmount=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
						igstAmt=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
						cgstAmt=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
						sgstAmt=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
						totalAmount=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
					}
					
					totalTaxableAmount+=calculateProperTaxModel.getUnitprice()*orderProductDetails.getIssuedQuantity();
					totalIgstAmt+=calculateProperTaxModel.getIgst()*orderProductDetails.getIssuedQuantity();
					totalCgstAmt+=calculateProperTaxModel.getCgst()*orderProductDetails.getIssuedQuantity();
					totalSgstAmt+=calculateProperTaxModel.getSgst()*orderProductDetails.getIssuedQuantity();
					totalTotalAmount+=calculateProperTaxModel.getMrp()*orderProductDetails.getIssuedQuantity();
				}				

				saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
				
				saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
				
				if(orderDetails.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
					
					saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
					saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
					saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
					
					saleMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(orderDetails.getConfirmDate()), 
							orderDetails.getInvoiceNumber(), 
							orderDetails.getBusinessName().getShopName(), 
							orderDetails.getBusinessName().getGstinNumber(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt)), 
							0, 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}else{
					
					saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
					saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
					saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
					
					saleMergeTableList.add(new SaleAndPurchaseTableMerge(
							simpleDateFormat.format(orderDetails.getConfirmDate()), 
							orderDetails.getInvoiceNumber(), 
							orderDetails.getBusinessName().getShopName(), 
							orderDetails.getBusinessName().getGstinNumber(),  
							Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
							0, 
							0, 
							Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
							Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
				}
			}
			
			totalTaxableAmount=0;
			totalCgstAmt=0;
			totalSgstAmt=0;
			totalIgstAmt=0;
			totalTotalAmount=0;
			
			//Counter order
			if(counterOrderList!=null){
				for(CounterOrder counterOrder : counterOrderList){
					
					totalTaxableAmount=0;
					totalCgstAmt=0;
					totalSgstAmt=0;
					totalIgstAmt=0;
					totalTotalAmount=0;
					
					List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderDAO.fetchCounterOrderProductDetails(counterOrder.getCounterOrderId());
					for(CounterOrderProductDetails counterOrderProductDetails:counterOrderProductDetailsList){			
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
																		counterOrderProductDetails.getSellingRate(), 
																		counterOrderProductDetails.getProduct().getCategories().getIgst());
						if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE))
						{
							taxableAmount=0;
							igstAmt=0;
							cgstAmt=0;
							sgstAmt=0;
							totalAmount=0;
						}
						else
						{
							taxableAmount=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
							igstAmt=calculateProperTaxModel.getIgst()*counterOrderProductDetails.getPurchaseQuantity();
							cgstAmt=calculateProperTaxModel.getCgst()*counterOrderProductDetails.getPurchaseQuantity();
							sgstAmt=calculateProperTaxModel.getSgst()*counterOrderProductDetails.getPurchaseQuantity();
							totalAmount=calculateProperTaxModel.getMrp()*counterOrderProductDetails.getPurchaseQuantity();
						}
						
						totalTaxableAmount+=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
						totalIgstAmt+=calculateProperTaxModel.getIgst()*counterOrderProductDetails.getPurchaseQuantity();
						totalCgstAmt+=calculateProperTaxModel.getCgst()*counterOrderProductDetails.getPurchaseQuantity();
						totalSgstAmt+=calculateProperTaxModel.getSgst()*counterOrderProductDetails.getPurchaseQuantity();
						totalTotalAmount+=calculateProperTaxModel.getMrp()*counterOrderProductDetails.getPurchaseQuantity();
					}	
					
					saleTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
					
					saleFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
					
					if(counterOrder.getBusinessName()!=null){
						if(counterOrder.getBusinessName().getTaxType().equals(Constants.INTRA_STATUS)){
							
							saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
							saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
							saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
							
							saleMergeTableList.add(new SaleAndPurchaseTableMerge(
									simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
									counterOrder.getInvoiceNumber(), 
									counterOrder.getBusinessName().getShopName(), 
									counterOrder.getBusinessName().getGstinNumber(),  
									Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
									Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt)), 
									0, 
									Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
						}else{
							
							saleCgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
							saleSgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
							saleIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
							
							saleMergeTableList.add(new SaleAndPurchaseTableMerge(
									simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
									counterOrder.getInvoiceNumber(), 
									counterOrder.getBusinessName().getShopName(), 
									counterOrder.getBusinessName().getGstinNumber(),  
									Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
									0, 
									0, 
									Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
						}
					}else{//external customer - assumed its Intra Taxtype
						saleCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
						saleSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
						saleIgstAmt+=0;//Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
						
						saleMergeTableList.add(new SaleAndPurchaseTableMerge(
								simpleDateFormat.format(counterOrder.getDateOfOrderTaken()), 
								counterOrder.getInvoiceNumber(), 
								counterOrder.getCustomerName(), 
								counterOrder.getCustomerGstNumber(),  
								Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt)), 
								Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt)), 
								0, 
								Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
					}
				}
			}
			
			totalTaxableAmount=0;
			totalCgstAmt=0;
			totalSgstAmt=0;
			totalIgstAmt=0;
			totalTotalAmount=0;
			
			//InventoryDAOImpl inventoryDAO=new InventoryDAOImpl(sessionFactory);
			for(Inventory inventory: inventoryList)
			{
				List<InventoryDetails> inventoryDetailsList=inventoryDAO.fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
				
				totalTaxableAmount=0;
				totalCgstAmt=0;
				totalSgstAmt=0;
				totalIgstAmt=0;
				totalTotalAmount=0;
				
				for(InventoryDetails inventoryDetails: inventoryDetailsList)
				{
					
					CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(
							inventoryDetails.getAmount(), 
							inventoryDetails.getProduct().getCategories().getIgst());
					/*for(SupplierProductList supplierProduct: supplierProductList)
					{
						if(inventoryDetails.getProduct().getProduct().getProductId()==supplierProduct.getProduct().getProductId())
						{
							rate=supplierProduct.getSupplierRate();
						}
					}*/
					taxableAmount=calculateProperTaxModel.getUnitprice();
					igstAmt=calculateProperTaxModel.getIgst();
					cgstAmt=calculateProperTaxModel.getCgst();
					sgstAmt=calculateProperTaxModel.getSgst();
					totalAmount=calculateProperTaxModel.getMrp();
					
					totalTaxableAmount+=calculateProperTaxModel.getUnitprice();
					totalIgstAmt+=calculateProperTaxModel.getIgst();
					totalCgstAmt+=calculateProperTaxModel.getCgst();
					totalSgstAmt+=calculateProperTaxModel.getSgst();
					totalTotalAmount+=calculateProperTaxModel.getMrp();
				}			
				
				purchaseTotalTaxableAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount));;
				purchaseCgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt));
				purchaseSgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt));
				purchaseIgstAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt));
				purchaseFinalAmt+=Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount));
				
				purchaseMergeTableList.add(new SaleAndPurchaseTableMerge(
						simpleDateFormat.format(inventory.getInventoryAddedDatetime()), 
						inventory.getInventoryTransactionId(), 
						inventory.getSupplier().getName(), 
						inventory.getSupplier().getGstinNo(),  
						Double.parseDouble(decimalFormatTwoDigit.format(totalTaxableAmount)), 
						Double.parseDouble(decimalFormatTwoDigit.format(totalCgstAmt)), 
						Double.parseDouble(decimalFormatTwoDigit.format(totalSgstAmt)), 
						0,  //Double.parseDouble(decimalFormatTwoDigit.format(totalIgstAmt)), 
						Double.parseDouble(decimalFormatTwoDigit.format(totalTotalAmount))));
			}
			
			return new GstBillReport(saleTableList, 
									purchaseTableList, 
									saleMergeTableList,
									purchaseMergeTableList,
									Double.parseDouble(decimalFormatTwoDigit.format(saleTotalTaxableAmt)),
									Double.parseDouble(decimalFormatTwoDigit.format(saleCgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleSgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(saleFinalAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseTotalTaxableAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseCgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseSgstAmt)), 
									0,//Double.parseDouble(decimalFormatTwoDigit.format(purchaseIgstAmt)), 
									Double.parseDouble(decimalFormatTwoDigit.format(purchaseFinalAmt)));
		
		}
	}
	/**
	 * <pre>
	 * fetch order details list which comes in logged user area and company 
	 * and current date with booked orders only
	 * calculate total ordered , available and required quantity product wise
	 * @return GkSnapProductResponse list
	 * </pre>
	 */
	@Transactional
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk() {
		List<GkSnapProductResponse> gkSnapProductResponseList=new ArrayList<>();
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		if(productList==null){
			return null;
		}

		String hql="from OrderDetails where 1=1 "+
				   " and businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+")"+
				   " and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				   " and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				   " and date(orderDetailsAddedDatetime)=date(CURRENT_DATE()) "+
				   " and orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"'";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);		
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		for(Product product: productList){
			
			String productName=product.getProductName();
			long totalOrderedQty=0; 
			long availableQty=product.getCurrentQuantity(); 
			long requiredQty;
			
			if(orderDetailsList.isEmpty()){
				totalOrderedQty=0;
			}else{
				for(OrderDetails orderDetails: orderDetailsList){
					
					List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
					for(OrderProductDetails orderProductDetails: orderProductDetailsList){
						if(product.getProductId()==orderProductDetails.getProduct().getProduct().getProductId()){
							totalOrderedQty+=orderProductDetails.getPurchaseQuantity();
						}
					}
					
				}
			}
			if(availableQty>totalOrderedQty){
				requiredQty=0;
			}else{
				requiredQty=totalOrderedQty-availableQty;
			}
			
			if(totalOrderedQty!=0) {
				gkSnapProductResponseList.add(new GkSnapProductResponse(productName, 
						totalOrderedQty, 
						availableQty, 
						requiredQty));
			}
			
		}
		return gkSnapProductResponseList;		
	}
	/**
	 * <pre>
	 * find total sale of sales man order (delivered and delivery pending)
	 * @param startDate
	 * @param endDate
	 * @return total sale amount
	 * </pre>
	 */
	@Transactional
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="from OrderDetails where (date(orderDetailsAddedDatetime)>= '"+startDate+"' and date(orderDetailsAddedDatetime)<='"+endDate+"')";
		hql+=" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		hql+=" and businessName.company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		double totalSale=0;
		for(OrderDetails orderDetails: orderDetailsList){
			totalSale+=orderDetails.getIssuedTotalAmountWithTax();
		}
		
		return totalSale;
	}
	
	/**
	 * <pre>
	 * find total sale of sales man order by range and employeeId
	 * @param startDate
	 * @param endDate
	 * @return total sale amount
	 * </pre>
	 */
	@Transactional
	public double totalSaleAmountForTarget(String startDate,String endDate,long employeeId,boolean includeTax){
		
		String hql="from OrderDetails where (date(orderDetailsAddedDatetime)>= '"+startDate+"' and date(orderDetailsAddedDatetime)<='"+endDate+"')";
		hql+=" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		hql+=" and employeeSM.employeeId="+employeeId;
		hql+=" and businessName.company.companyId="+getSessionSelectedCompaniesIds();
		//hql+=" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		double totalSale=0;
		for(OrderDetails orderDetails: orderDetailsList){
			if(includeTax){
				totalSale+=orderDetails.getTotalAmountWithTax();
			}else{
				totalSale+=orderDetails.getTotalAmount();
			}
		}
		
		return totalSale;
	}
	
	/**
	 * <pre>
	 * find total sale of sales man order by range and employeeId
	 * @param startDate
	 * @param endDate
	 * @return total sale amount
	 * </pre>
	 */
	@Transactional
	public double findNumberOfProductSalesByDateRangeAndEmployeeId(String startDate,String endDate,long employeeId,long productId,boolean includeTax){
		
		String hql="from OrderProductDetails where (date(orderDetails.orderDetailsAddedDatetime)>= '"+startDate+"' and date(orderDetails.orderDetailsAddedDatetime)<='"+endDate+"')";
		hql+=" and orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		hql+=" and orderDetails.employeeSM.employeeId="+employeeId;
		hql+=" and product.productId="+productId;
		hql+=" and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		//hql+=" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductDetails> orderProductDetailsList=(List<OrderProductDetails>)query.list();
		
		double totalSale=0;
		for(OrderProductDetails orderProductDetails: orderProductDetailsList){
			if(includeTax){
				totalSale+=orderProductDetails.getSellingRate()*orderProductDetails.getIssuedQuantity();
			}else{
				totalSale+=orderProductDetails.getSellingRate()*orderProductDetails.getProduct().getRate();
			}
		}
		
		return totalSale;
	}
	
	/**
	 * <pre>
	 * find total sale of sales man order by range and employeeId
	 * @param startDate
	 * @param endDate
	 * @return total sale amount
	 * </pre>
	 */
	@Transactional
	public long findNumberOfProductSalesByDateRangeAndEmployeeIdForQuantity(String startDate,String endDate,long employeeId,long productId){
		
		String hql="from OrderProductDetails where (date(orderDetails.orderDetailsAddedDatetime)>= '"+startDate+"' and date(orderDetails.orderDetailsAddedDatetime)<='"+endDate+"')";
		hql+=" and orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		hql+=" and orderDetails.employeeSM.employeeId="+employeeId;
		hql+=" and product.productId="+productId;
		hql+=" and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		//hql+=" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderProductDetails> orderProductDetailsList=(List<OrderProductDetails>)query.list();
		
		long totalQuantitySale=0;
		for(OrderProductDetails orderProductDetails: orderProductDetailsList){
			totalQuantitySale+=orderProductDetails.getIssuedQuantity();
		}
		
		return totalQuantitySale;
	}
	
	
	//fetching last 3 months Order List for taking return from them for Offline mode 
	@Transactional
	public List<OrderDetails> fetchLast3MonthOrderListByAreaIdForOfflineMode(long employeeId) {

		String hql;
		Query query;
	
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
		//orderDetailsList.setAreaList(areaList);
				
		List<Long> pnjId = new ArrayList<>();
	    Iterator<Area> iterator = areaList.iterator();
	    while (iterator.hasNext()) 
	    {
	    	Area area = iterator.next();
	        pnjId.add(area.getAreaId());
	    }
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");  
	    Date date = new Date();  
	    String currentDate=formatter.format(date);
	    System.out.println(formatter.format(date));  
	   String last3MonthFirstDate=getLast90DaysFirstDate();
	   //String last3MonthLastDate=DateUtils.getLast3MonthLastDate();
		hql="from OrderDetails where date(orderDetailsAddedDatetime) >='"+last3MonthFirstDate+"' and date(orderDetailsAddedDatetime)<='"+currentDate+"' and businessName.area.areaId in (:ids) and orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED+"'"+
				" and businessName.company.companyId="+getSessionSelectedCompaniesIds()+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by orderDetailsAddedDatetime desc";
		//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", pnjId);
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		
		if(list.isEmpty())
		{ 
			return null;
		}
		
		return list;
	}
	
	public static String getLast90DaysFirstDate()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Calendar gc =Calendar.getInstance();
		    gc.add(Calendar.DAY_OF_MONTH, -90);
		return dateFormat.format(gc.getTime());
	}
	
	/*public static void main(){
		System.out.println(getLast90DaysFirstDate());
	}*/
	
	
	//fetching cancel reason list for cancelling the order
	@Transactional
	public List<CancelReason> fetchCancelReason() {
		// TODO Auto-generated method stub
		String hql="from CancelReason";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CancelReason> cancelReasonsList=(List<CancelReason>)query.list();
		if(cancelReasonsList.isEmpty()){
			return null;
		}
		return cancelReasonsList;
	}

	
	// offlineAPIRequest is used to handle the duplicate entry of offline orders
	// here we are fetching offline api request by sqliteDb id and imei number
	@Transactional
	public OfflineAPIRequest fetchofflineApiHandlingByDbIdAndIMEINo(OfflineAPIRequest apiRequest){
		
		String hql="from OfflineAPIRequest where dbId ="+apiRequest.getDbId() +" and imeiNumber ='"+ apiRequest.getImeiNumber() +"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OfflineAPIRequest> offlineAPIRequestList=(List<OfflineAPIRequest>)query.list();
		if(offlineAPIRequestList.isEmpty()){
			return null;
		}
		return offlineAPIRequestList.get(0);
	}
	
	// here we save the Offline Api request and make their status as initiated.
	@Transactional
	public void saveOfflineAPIrequest(OfflineAPIRequest apiRequest){
		apiRequest.setStatus(Constants.OFFLINE_STATUS_INITIATED);
		apiRequest.setIsOfflineRequest(apiRequest.getIsOfflineRequest());
		apiRequest.setAddedDateTime(new Date());
		sessionFactory.getCurrentSession().save(apiRequest);
	}
	
	
	//here we update the status of OfflineAPI Request by dbId and imei number
	@Transactional
	public void updateTheStatusOfOfflineAPIRequest(long dbId,String imeiNo,String status){
		String hql="from OfflineAPIRequest where dbId ="+dbId +" and imeiNumber ='"+ imeiNo +"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OfflineAPIRequest> offlineAPIRequestList=(List<OfflineAPIRequest>)query.list();
		OfflineAPIRequest apiRequest=offlineAPIRequestList.get(0);
		
		apiRequest.setStatus(status);
		sessionFactory.getCurrentSession().save(apiRequest);
		
	}
	
	/**
	 * <pre>
	 * check transportation is used or not 
	 * @param transportationId
	 * @return true/false
	 * </pre>
	 */
	@Transactional 
	public boolean checkTransaportationUsed(long transportationId){
		String hql="select (SELECT count(*) FROM OrderDetails o where o.transportation.id="+transportationId+")+(SELECT count(*) FROM CounterOrder c where c.transportation.id="+transportationId+") from Area";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> countOfTransportaionUsed=(List<Long>)query.list();
		if(countOfTransportaionUsed.get(0)!=0){
			return true;
		}
		return false;
	}
	/**
	 * <pre>
	 * find number of order taken by range and employee
	 * </pre>
	 * @param startDate
	 * @param endDate
	 * @param employeeId
	 * @return
	 */
	@Transactional
	public int findNumberOfOrderTakenByDateRange(String startDate,String endDate,long employeeId){
		String hql="from OrderDetails where employeeSM.employeeId=" +employeeId + "  And (date(orderDetailsAddedDatetime)>='"+startDate+"'  "
				+ "And date(orderDetailsAddedDatetime)<='"+endDate+"')"
				+" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"
				//+" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"
				+" order by orderDetailsAddedDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		if(orderDetailsList.isEmpty()){
			return 0;
		}else{
			return orderDetailsList.size();
		}
	}
	/**
	 * <pre>
	 * fetch order details by business id for bad debts
	 * @param businessNameId
	 * @param fromDate
	 * @param toDate
	 * @return OrderDetails list
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails>  fetchOrderDetailsBybusinessNameId(String businessNameId) {
		// TODO Auto-generated method stub
		String hql="from OrderDetails where businessName.businessNameId='"+businessNameId 
				+"' and payStatus=false "+
				" and orderStatus.status not in ('"+Constants.ORDER_STATUS_CANCELED+"')"+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);  // Query to fetch orderDetails by DateRange
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty()){
			return null;
		}
		
		return list;
	}
	/**
	 * <pre>
	 * fetch order details by business id and for bad debts
	 * @param businessNameId
	 * @param fromDate
	 * @param toDate
	 * @return OrderDetails list
	 * </pre>
	 */
	@Transactional
	public List<OrderDetails>  fetchOrderDetailsBybusinessNameIdForView(String businessNameId) {
		// TODO Auto-generated method stub
		String hql="from OrderDetails where businessName.businessNameId='"+businessNameId +"' "+
				 " and badDebtsStatus=true "+
				" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and businessName.branch.branchId in ("+getSessionSelectedBranchIds()+")"+
				" order by orderDetailsAddedDatetime desc";
				//" and businessName.area.areaId in ("+getSessionSelectedIds()+") order by orderDetailsAddedDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);  // Query to fetch orderDetails by DateRange
		
		List<OrderDetails> list=(List<OrderDetails>)query.list();
		if(list.isEmpty()){
			return null;
		}
		
		return list;
	}
	/**
	 * <pre>
	 * fetch BadDebts order details fetch by businessNameId 
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return OrderReportList list
	 * </pre>
	 */
	@Transactional
	public List<OrderReportList> showOrderReportByBusinessNameIdOfBadDebts(String businessNameId)
	{
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy"); 
		
		List<OrderDetails> orderDetailsList= fetchOrderDetailsBybusinessNameIdForView(businessNameId);
		
		if(orderDetailsList==null)
		{
			return null;
		}
		
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(OrderDetails orderDetails : orderDetailsList){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss"); 
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			 if(orderDetails.getOldOrderStatus().equals(Constants.ORDER_STATUS_BOOKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK = "Pending";
					orderStatusGKDate = "--";
					orderStatusGKTime= "--";
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getTotalAmount();
					amountWithTax=orderDetails.getTotalAmountWithTax();
					quantity=orderDetails.getTotalQuantity();
				}
				else if(orderDetails.getOldOrderStatus().equals(Constants.ORDER_STATUS_PACKED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_PACKED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOldOrderStatus().equals(Constants.ORDER_STATUS_ISSUED))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB = "Pending";
					orderStatusDBDate = "--";
					orderStatusDBTime= "--";
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}
				else if(orderDetails.getOldOrderStatus().equals(Constants.ORDER_STATUS_DELIVERED) || orderDetails.getOldOrderStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING))
				{
					orderStatusSM=Constants.ORDER_STATUS_BOOKED;
					orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
					orderStatusGK=Constants.ORDER_STATUS_ISSUED;
					orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
					orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
					orderStatusDB=orderDetails.getOrderStatus().getStatus();
					orderStatusDBDate=simpleDateFormat.format(orderDetails.getConfirmDate());
					orderStatusDBTime= simpleTimeFormat.format(orderDetails.getConfirmDate());
					amount=orderDetails.getIssuedTotalAmount();
					amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
					quantity=orderDetails.getIssuedTotalQuantity();
				}else{
					if(orderDetails.getEmployeeIdCancel().getDepartment().getName().equals(Constants.SALESMAN_DEPT_NAME)){
						
						orderStatusSM=Constants.ORDER_STATUS_BOOKED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK = "Pending";
						orderStatusGKDate = "--";
						orderStatusGKTime= "--";
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getTotalAmount();
						amountWithTax=orderDetails.getTotalAmountWithTax();
						quantity=orderDetails.getTotalQuantity();
						
					}else if(orderDetails.getEmployeeIdCancel().getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME)){
						
						orderStatusSM=Constants.ORDER_STATUS_BOOKED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK=Constants.ORDER_STATUS_PACKED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusGKDate=simpleDateFormat.format(orderDetails.getPackedDate());
						orderStatusGKTime= simpleTimeFormat.format(orderDetails.getPackedDate());
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getIssuedTotalAmount();
						amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
						quantity=orderDetails.getIssuedTotalQuantity();
						
					}else{
						orderStatusSM=Constants.ORDER_STATUS_BOOKED;
						orderStatusSMDate=simpleDateFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusSMTime=simpleTimeFormat.format(orderDetails.getOrderDetailsAddedDatetime());
						orderStatusGK=Constants.ORDER_STATUS_ISSUED+" and "+Constants.ORDER_STATUS_CANCELED;
						orderStatusGKDate=simpleDateFormat.format(orderDetails.getIssueDate());
						orderStatusGKTime= simpleTimeFormat.format(orderDetails.getIssueDate());
						orderStatusDB = "Pending";
						orderStatusDBDate = "--";
						orderStatusDBTime= "--";
						amount=orderDetails.getIssuedTotalAmount();
						amountWithTax=orderDetails.getIssuedTotalAmountWithTax();
						quantity=orderDetails.getIssuedTotalQuantity();
					}
				}
			
			list2.add(new OrderReportList(srno,
					orderDetails.getOrderId(), 
					amount,
					amountWithTax,
					orderDetails.getBusinessName().getArea().getName(),
					quantity, 
					orderDetails.getBusinessName().getShopName(), 
					orderDetails.getBusinessName().getBusinessNameId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()).getEmployeeDetailsGenId(),
					orderDetails.getPaymentPeriodDays(), 
					orderDetails.getOrderDetailsAddedDatetime(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							orderDetails.getOrderStatus().getStatus()));
			srno++;
		}
		
		return list2;
	}
	/**
	 * <pre>
	 * fetch ReturnOrderProductP by returnOrderProductPkId
	 * </pre>
	 * @param returnOrderProductPkId
	 * @return ReturnOrderProductP
	 */
	@Transactional
	public ReturnOrderProductP fetchReturnOrderProductPByReturnOrderProductId(String returnOrderProductId){
		String hql="from ReturnOrderProductP where returnOrderProductId='"+returnOrderProductId+"'";
		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and orderDetails.businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" and orderDetails.businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+") order by returnOrderProductDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductP> returnOrderProductPList=(List<ReturnOrderProductP>)query.list();
		return returnOrderProductPList.get(0);
	}
	/**
	 * <pre>
	 * fetch ReturnOrderProductDetailsP list by returnOrderProductPkId
	 * </pre>
	 * @param returnOrderProductPkId
	 * @return ReturnOrderProductDetailsP list
	 */
	@Transactional
	public List<ReturnOrderProductDetailsP> fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(String returnOrderProductId){
		String hql="from ReturnOrderProductDetailsP where returnOrderProductP.returnOrderProductId='"+returnOrderProductId+"'";
		hql+=" and returnOrderProductP.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			" and returnOrderProductP.orderDetails.businessName.branch.branchId="+getSessionSelectedBranchIds()+
			" and returnOrderProductP.orderDetails.businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList=(List<ReturnOrderProductDetailsP>)query.list();
		return returnOrderProductDetailsPList;
	}
	
	/**
	 * <pre>
	 * fetch permanent return order by range,startDate,endDate
	 * @param range
	 * @param startDate
	 * @param endDate
	 * @return PReturnOrderDetailsModel list
	 * </pre>
	 */
	@Transactional
	public List<PReturnOrderDetailsModel> permanentReturnOrderReport(String range,String startDate,String endDate)
	{
		List<PReturnOrderDetailsModel> returnOrderDetailsModelList=new ArrayList<>();
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		
		String hql="";
		Calendar cal=Calendar.getInstance();
		
		if(range.equals("range"))
		{
			hql="from ReturnOrderProductP where date(returnOrderProductDatetime) >= '"+startDate+"' and date(returnOrderProductDatetime) <= '"+endDate+"' ";
		}
		else if(range.equals("today"))
		{
			hql="from ReturnOrderProductP where date(returnOrderProductDatetime) = date(CURRENT_DATE()) ";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from ReturnOrderProductP where date(returnOrderProductDatetime) = '"+simpleDateFormat.format(cal.getTime())+"' ";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReturnOrderProductP where date(returnOrderProductDatetime) >= '"+simpleDateFormat.format(cal.getTime())+"' ";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from ReturnOrderProductP where (date(returnOrderProductDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(returnOrderProductDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from ReturnOrderProductP where (date(returnOrderProductDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(returnOrderProductDatetime) <= '"+DatePicker.getLastMonthFirstDate()+"') ";
		}
		else if(range.equals("last3Months"))
		{
			hql="from ReturnOrderProductP where (date(returnOrderProductDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(returnOrderProductDatetime) <= '"+DatePicker.getLast3MonthFirstDate()+"') ";
		}
		else if(range.equals("pickDate"))
		{
			hql="from ReturnOrderProductP where date(returnOrderProductDatetime) = '"+startDate+"' ";
		}
		else if(range.equals("viewAll"))
		{
			hql="from ReturnOrderProductP where 1=1 ";
		}

		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
				" and orderDetails.businessName.branch.branchId="+getSessionSelectedBranchIds()+
				" and orderDetails.businessName.area.areaId in ("+areaDAO.getSessionAreaIdsForOtherEntities()+") order by returnOrderProductDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductP> list=(List<ReturnOrderProductP>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		long srno=1;
		for(ReturnOrderProductP  returnOrderProductP : list){
			EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnOrderProductP.getEmployee().getEmployeeId());
			returnOrderDetailsModelList.add(new PReturnOrderDetailsModel(
					srno,
					returnOrderProductP.getReturnOrderProductId(), 
					returnOrderProductP.getOrderDetails().getOrderId(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getShopName(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getAddress(),
					employeeDetails.getName(), 
					employeeDetails.getAddress(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getContact().getMobileNumber(),
					employeeDetails.getEmployeeDetailsId(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getName(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getName(), 
					returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getCity().getName(), 
					returnOrderProductP.getTotalQuantity(), 
					returnOrderProductP.getTotalAmount(), 
					returnOrderProductP.getTotalAmountWithTax()-returnOrderProductP.getTotalAmount(), 
					returnOrderProductP.getTotalAmountWithTax(), 
					returnOrderProductP.getOrderDetails().getOrderDetailsAddedDatetime(), 
					returnOrderProductP.getReturnOrderProductDatetime()));
			srno++;
		}
		return returnOrderDetailsModelList;
	}
	
	/**
	 * <pre>
	 * make and save permanent return order and return product transfer to 
	 * return from delivery boy section for define damage quantity  
	 * </pre>
	 * @param returnPermanentModelRequest
	 */
	@Transactional
	public void savePermanentReturnOrder(ReturnPermanentModelRequest returnPermanentModelRequest){
				
		//fetch return order details
		ReturnOrderProduct returnOrderProduct=returnOrderDAO.fetchReturnOrderForGKReportByReturnOrderProductId(returnPermanentModelRequest.getReturnOrderProductId());
		List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderDAO.fetchReturnOrderProductDetailsByReturnOrderDetailsId(returnPermanentModelRequest.getReturnOrderProductId());

		//fetch order details
		OrderDetails orderDetails=fetchOrderDetailsByOrderId(returnOrderProduct.getOrderDetails().getOrderId());
		List<OrderProductDetails> orderProductDetailsList=fetchOrderProductDetailByOrderIdForApp(returnOrderProduct.getOrderDetails().getOrderId());
		orderProductIssueDetails=fetchOrderProductIssueDetailsByOrderId(returnOrderProduct.getOrderDetails().getOrderId());
		
		//get permanent return order details from request  
		ReturnOrderProductP returnOrderProductP=returnPermanentModelRequest.getReturnOrderProductP();
		returnOrderProductP.setOrderDetails(orderDetails);
		returnOrderProductP.setInvoiceNumber(creditNoteInvoiceNumberGenerate.generateInvoiceNumber());
		List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList=returnPermanentModelRequest.getReturnOrderProductDetailsPList();
		
		String returnFromDeliveryBoyMainId=returnFromDeliveryBoyGenerator.generateReturnFromDeliveryBoyMainId();
		
		returnOrderProductP.setReturnOrderProductId(permanentReturnOrderIdGenerator.generate());
		returnOrderProductP.setReturnOrderProductDatetime(new Date());
		sessionFactory.getCurrentSession().save(returnOrderProductP);

		List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList=new ArrayList<>();
		
		double issuedTotalAmount=0;
		double issuedTotalAmountWithTax=0;
		long issuedTotalQuantity=0;
		
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		double amountPending=0;
		for(ReturnOrderProductDetailsP returnOrderProductDetailsP : returnOrderProductDetailsPList){
			
			issuedTotalAmount=0;
			issuedTotalAmountWithTax=0;
			issuedTotalQuantity=0;
			
			totalAmount=0;
			totalAmountWithTax=0;
			totalQuantity=0;
			
			//change issued quantity according permanent return order quantity
			for(int index=0; index<orderProductDetailsList.size(); index++){
					OrderProductDetails orderProductDetails = orderProductDetailsList.get(index);
					
				if(orderProductDetails.getProduct().getProduct().getProductId()==returnOrderProductDetailsP.getProduct().getProduct().getProductId() && 
						orderProductDetails.getType().equals(returnOrderProductDetailsP.getType())){
					orderProductDetails.setIssuedQuantity(orderProductDetails.getIssuedQuantity()-returnOrderProductDetailsP.getReturnQuantity());
					orderProductDetails.setIssueAmount(orderProductDetails.getIssuedQuantity()*orderProductDetails.getSellingRate());
					
					orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
					sessionFactory.getCurrentSession().update(orderProductDetails);
					
					orderProductDetailsList.set(index, orderProductDetails);
				}				
				CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(orderProductDetails.getSellingRate(), orderProductDetails.getProduct().getCategories().getIgst());
				issuedTotalAmount+=orderProductDetails.getIssuedQuantity()*calculateProperTaxModel.getUnitprice();
				issuedTotalAmountWithTax+=orderProductDetails.getIssueAmount();
				issuedTotalQuantity+=orderProductDetails.getIssuedQuantity();
			}
			
			//minus return order product quantity according or 
			//delete which return quantity is whole permanent returned
			for(int index=0; index<returnOrderProductDetailsList.size(); index++){
				ReturnOrderProductDetails returnOrderProductDetails = returnOrderProductDetailsList.get(index);
				if(returnOrderProductDetails.getProduct().getProduct().getProductId()==returnOrderProductDetailsP.getProduct().getProduct().getProductId() && 
						returnOrderProductDetails.getType().equals(returnOrderProductDetailsP.getType())){
					
					if(returnOrderProductDetailsP.getReturnQuantity()==returnOrderProductDetails.getReturnQuantity()){
						
						returnOrderProductDetails=(ReturnOrderProductDetails)sessionFactory.getCurrentSession().merge(returnOrderProductDetails);
						sessionFactory.getCurrentSession().delete(returnOrderProductDetails);
						returnOrderProductDetailsList.remove(index);
					}else{
						
						returnOrderProductDetails.setReturnQuantity(returnOrderProductDetails.getReturnQuantity()-returnOrderProductDetailsP.getReturnQuantity());
						returnOrderProductDetails.setIssuedQuantity(returnOrderProductDetails.getIssuedQuantity()-returnOrderProductDetailsP.getReturnQuantity());
						returnOrderProductDetails.setReturnTotalAmountWithTax(returnOrderProductDetails.getReturnQuantity()*returnOrderProductDetails.getSellingRate());
						
						returnOrderProductDetails=(ReturnOrderProductDetails)sessionFactory.getCurrentSession().merge(returnOrderProductDetails);
						sessionFactory.getCurrentSession().update(returnOrderProductDetails);
					}
					
					amountPending+=returnOrderProductDetails.getReturnQuantity()*returnOrderProductDetails.getSellingRate();
					break;
				}
			}
			returnOrderProductDetailsP.setReturnOrderProductP(returnOrderProductP);
			sessionFactory.getCurrentSession().save(returnOrderProductDetailsP);
			
			//make return from delivery boy 
			ReturnFromDeliveryBoy returnFromDeliveryBoy=new ReturnFromDeliveryBoy(
					returnOrderProductDetailsP.getProduct(), 
					returnOrderProductDetailsP.getSellingRate(), 
					returnOrderProductDetailsP.getReturnQuantity(), 
					returnOrderProductDetailsP.getIssuedQuantity(),
					returnOrderProductDetailsP.getIssuedQuantity()-returnOrderProductDetailsP.getReturnQuantity(),
					0,
					0, 
					returnOrderProductDetailsP.getType());
			returnFromDeliveryBoyList.add(returnFromDeliveryBoy);
		}		
		
		for(int index=0; index<returnOrderProductDetailsList.size(); index++){
			ReturnOrderProductDetails returnOrderProductDetails = returnOrderProductDetailsList.get(index);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(returnOrderProductDetails.getSellingRate(), returnOrderProductDetails.getProduct().getCategories().getIgst());

			totalAmount+=returnOrderProductDetails.getReturnQuantity()*calculateProperTaxModel.getUnitprice();
			totalAmountWithTax+=returnOrderProductDetails.getSellingRate()*returnOrderProductDetails.getReturnQuantity();
			totalQuantity+=returnOrderProductDetails.getReturnQuantity();
		}
		//return order update
		returnOrderProduct.setTotalAmount(totalAmountWithTax);
		returnOrderProduct.setTotalAmountWithTax(totalAmountWithTax);
		returnOrderProduct.setTotalQuantity(totalQuantity);
		
		returnOrderProduct=(ReturnOrderProduct)sessionFactory.getCurrentSession().merge(returnOrderProduct);
		sessionFactory.getCurrentSession().update(returnOrderProduct);
		
		//order update
		orderDetails.setIssuedTotalAmount(issuedTotalAmount);
		orderDetails.setIssuedTotalAmountWithTax(issuedTotalAmountWithTax);
		orderDetails.setIssuedTotalQuantity(issuedTotalQuantity);
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
		
		/* check if return product details list not found then delete main object also*/
		//returnOrderProductDetailsList=returnOrderDAO.fetchReturnOrderProductDetailsByReturnOrderDetailsId(returnPermanentModelRequest.getReturnOrderProductId());
		if(returnOrderProductDetailsList.isEmpty()){
			returnOrderProduct=(ReturnOrderProduct)sessionFactory.getCurrentSession().merge(returnOrderProduct);
			sessionFactory.getCurrentSession().delete(returnOrderProduct);			
		}
		
		/* check need to order cancel or not */
		//orderProductDetailsList=fetchOrderProductDetailByOrderIdForApp(returnOrderProduct.getOrderDetails().getOrderId());
		long totalissuedQuantity=0;
		for(OrderProductDetails orderProductDetails : orderProductDetailsList){
			totalissuedQuantity+=orderProductDetails.getIssuedQuantity();
		}
		
		
		if(totalissuedQuantity==0){
			orderDetails.setOrderStatus(orderStatusDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
		}
		/* need cancel or not checking done */
		

		
		//update amount pending
		BusinessName businessName=orderDetails.getBusinessName();
		businessName.setAmountPending(businessName.getAmountPending()+amountPending);
		
		businessName=(BusinessName)sessionFactory.getCurrentSession().merge(businessName);
		sessionFactory.getCurrentSession().update(businessName);
		
		long totalReturnQuantity=0;
		long totalIssuedQuantity=0;
		long totalDeliveryQuantity=0;
		long totalDamageQuantity=0;
		long totalNonDamageQuantity=0;
		
		//make main return from delivery boy object
		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			
			totalReturnQuantity+=returnFromDeliveryBoy.getReturnQuantity();
			totalIssuedQuantity+=returnFromDeliveryBoy.getIssuedQuantity();
			totalDeliveryQuantity+=returnFromDeliveryBoy.getDeliveryQuantity();
			totalDamageQuantity+=returnFromDeliveryBoy.getDamageQuantity();
			totalNonDamageQuantity+=returnFromDeliveryBoy.getNonDamageQuantity();
		}
		
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		
		Branch branch=new Branch();
		branch.setBranchId(getSessionSelectedBranchIds());
		
		ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain=new ReturnFromDeliveryBoyMain(
				totalReturnQuantity, 
				totalIssuedQuantity, 
				totalDeliveryQuantity, 
				totalDamageQuantity, 
				totalNonDamageQuantity,  
				orderProductIssueDetails.getEmployeeGK(), 
				orderProductIssueDetails.getEmployeeDB(), 
				orderDetails, 
				new Date(), 
				false,
				company,
				branch);
		//save main return from delivery boy object
		returnFromDeliveryBoyMain.setReturnFromDeliveryBoyMainId(returnFromDeliveryBoyMainId);
		sessionFactory.getCurrentSession().save(returnFromDeliveryBoyMain);
		
		//save return from delivery boy products object
		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList){
			returnFromDeliveryBoy.setReturnFromDeliveryBoyMain(returnFromDeliveryBoyMain);
			sessionFactory.getCurrentSession().save(returnFromDeliveryBoy);
		}
		
	}
	/**
	 * credit note pdf generate for sm order return
	 */
	@Transactional
	public BillPrintDataModel fetchReturnOrderCreditNoteBillPrintData(String returnOrderProductId,long companyId,long branchId)
	{
		/*ReturnOrderProductP returnOrderProductP=fetchReturnOrderProductPByReturnOrderProductId(returnOrderProductId);
		List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList=fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(returnOrderProductId);*/
		
		String hql="from ReturnOrderProductP where returnOrderProductId='"+returnOrderProductId+"'";
		hql+=" and orderDetails.businessName.company.companyId in ("+companyId+")"+
				" and orderDetails.businessName.branch.branchId="+branchId+
				"  order by returnOrderProductDatetime desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductP> returnOrderProductPList=(List<ReturnOrderProductP>)query.list();
		ReturnOrderProductP returnOrderProductP = returnOrderProductPList.get(0);
	
		hql="from ReturnOrderProductDetailsP where returnOrderProductP.returnOrderProductId='"+returnOrderProductId+"'";
		hql+=" and returnOrderProductP.orderDetails.businessName.company.companyId in ("+companyId+")"+
			" and returnOrderProductP.orderDetails.businessName.branch.branchId="+branchId;
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList=(List<ReturnOrderProductDetailsP>)query.list();
		
		OrderDetails orderDetails=returnOrderProductP.getOrderDetails();
			
		BusinessName businessName=orderDetails.getBusinessName();
		
		//invoice number take from order details
		String invoiceNumber=orderDetails.getInvoiceNumber();
		
		//booked order date from order details
		String orderDate=new SimpleDateFormat("dd-MM-yyyy").format(orderDetails.getOrderDetailsAddedDatetime());
		String returnDate=new SimpleDateFormat("dd-MM-yyyy").format(returnOrderProductP.getReturnOrderProductDatetime());
		
		
		//business address set for multiline
		List<String> addressLineList=new ArrayList<>();
		String[] arr = businessName.getAddress().split(" ");   	
		int cout=0;
		String line="";
		 for ( String ss : arr) {
			 cout++;			 	 
			 line+=ss+" ";		  
			 
			 if(cout==5)
			 {
				 cout=0;
				 addressLineList.add(line);
				 line="";
				 continue;
			 }		
		  }
		 if(addressLineList.size()>0)
		 {
			 addressLineList.set((addressLineList.size()-1), addressLineList.get(addressLineList.size()-1).substring(0, addressLineList.get(addressLineList.size()-1).length() - 1));
		 }
		 else
		 {
			 addressLineList.add(line);
		 }
		 System.out.println(addressLineList);
		
		//delivery date from order date
		String deliveryDate=new SimpleDateFormat("dd-MM-yyyy").format(orderDetails.getDeliveryDate());
		
		List<ProductListForBill> productListForBillList=new ArrayList<>();
		
		float totalAmountWithoutTax=0;
		
		float cGSTAmount=0;
		float iGSTAmount=0;
		float sGSTAmount=0;
		
		long totalQuantity=0;
		float totalAmountWithTax=0;
		String totalAmountWithTaxInWord="";
		
		List<CategoryWiseAmountForBill> categoryWiseAmountForBills=new ArrayList<>();
		
		float totalAmount=0;
		float taxAmount=0;
		String taxAmountInWord="";
		
		float totalCGSTAmount=0;
		float totalIGSTAmount=0;
		float totalSGSTAmount=0;
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		DecimalFormat decimalFormatThreeDigit=new DecimalFormat("#.###");
		long srno=1;
		//create product wise details
		for(ReturnOrderProductDetailsP returnOrderProductDetailsP: returnOrderProductDetailsPList)
		{
			float amountWithoutTax=0;
			CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(returnOrderProductDetailsP.getSellingRate(), 
					   returnOrderProductDetailsP.getProduct().getCategories().getIgst());
			
			if(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{	
				amountWithoutTax=returnOrderProductDetailsP.getReturnQuantity()*calculateProperTaxModel.getUnitprice();
			
				totalAmountWithoutTax+=amountWithoutTax;
				
				float igst=returnOrderProductDetailsP.getProduct().getCategories().getIgst();
				float cgst=returnOrderProductDetailsP.getProduct().getCategories().getCgst();
				float sgst=returnOrderProductDetailsP.getProduct().getCategories().getSgst();
				
				float rate=calculateProperTaxModel.getUnitprice();
				long returnQuantity=returnOrderProductDetailsP.getReturnQuantity();
				
				if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
				{
					cGSTAmount+=calculateProperTaxModel.getCgst()*returnQuantity;
					iGSTAmount+=0;
					sGSTAmount+=calculateProperTaxModel.getSgst()*returnQuantity;
				}
				else
				{
					cGSTAmount+=0;
					iGSTAmount+=calculateProperTaxModel.getIgst()*returnQuantity;
					sGSTAmount+=0;						
				}
			}
			totalQuantity+=returnOrderProductDetailsP.getReturnQuantity();
			productListForBillList.add(new ProductListForBill(
					String.valueOf(srno),
					returnOrderProductDetailsP.getProduct().getProductName()+(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "-(Free)" :""), 
					returnOrderProductDetailsP.getProduct().getCategories().getHsnCode(),
					String.valueOf(((long)returnOrderProductDetailsP.getProduct().getCategories().getIgst())),
					String.valueOf(returnOrderProductDetailsP.getReturnQuantity()), 
					(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0" :decimalFormat.format(calculateProperTaxModel.getUnitprice())), 
					(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0.0" :decimalFormat.format(amountWithoutTax) )));
			srno++;
			//double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
			//totalAmountWithTax+=amountWithoutTax+tax;	
			
		}
		//total amount with tax basis of product details
		for(ProductListForBill productListForBill2 :productListForBillList)
		{
			totalAmountWithTax+=Float.parseFloat(productListForBill2.getAmountWithoutTax());
		}
		totalAmountWithTax=Float.parseFloat(decimalFormat.format(totalAmountWithTax));
		cGSTAmount=Float.parseFloat(decimalFormatThreeDigit.format(cGSTAmount));
		iGSTAmount=Float.parseFloat(decimalFormat.format(iGSTAmount));
		sGSTAmount=Float.parseFloat(decimalFormatThreeDigit.format(sGSTAmount));
		
		totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;
		
		Set<OrderUsedCategories> categoriesList=new HashSet<>();
		//get unique category 
		for(ReturnOrderProductDetailsP returnOrderProductDetailsP: returnOrderProductDetailsPList)
		{
			if(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{
				categoriesList.add(returnOrderProductDetailsP.getProduct().getCategories());
			}
		}
		
		//category wise taxslab and amount find  
		Set<String> categoriesHsnCodeList=new HashSet<>();
		for(OrderUsedCategories categories: categoriesList)
		{
			String hsncode="";			
			float taxableValue=0;
			float cgstPercentage=0; 
			float igstPercentage=0;			
			float sgstPercentage=0;
			float cgstRate=0; 
			float igstRate=0;
			float sgstRate=0;
			
			cGSTAmount=0;
			iGSTAmount=0;
			sGSTAmount=0;
			
			boolean gotDuplicateHsnCode=false;
			
			for(String hsnCode : categoriesHsnCodeList)
			{
				if(categories.getHsnCode().equals(hsnCode))
				{
					gotDuplicateHsnCode=true;
				}
			}
			if(gotDuplicateHsnCode)
			{
				continue;
			}
			
			categoriesHsnCodeList.add(categories.getHsnCode());
			
			for(ReturnOrderProductDetailsP returnOrderProductDetailsP: returnOrderProductDetailsPList)
			{
				if(returnOrderProductDetailsP.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					if(categories.getHsnCode().equals(returnOrderProductDetailsP.getProduct().getCategories().getHsnCode()))
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(returnOrderProductDetailsP.getSellingRate(), 
								   returnOrderProductDetailsP.getProduct().getCategories().getIgst());
						taxableValue+=returnOrderProductDetailsP.getReturnQuantity()*calculateProperTaxModel.getUnitprice();
						
						float igst=returnOrderProductDetailsP.getProduct().getCategories().getIgst();
						float cgst=returnOrderProductDetailsP.getProduct().getCategories().getCgst();
						float sgst=returnOrderProductDetailsP.getProduct().getCategories().getSgst();
						float rate=calculateProperTaxModel.getUnitprice();
						long returnQuantity=returnOrderProductDetailsP.getReturnQuantity();
						
						if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
						{
							cGSTAmount+=calculateProperTaxModel.getCgst()*returnQuantity;
							iGSTAmount+=0;
							sGSTAmount+=calculateProperTaxModel.getSgst()*returnQuantity;
						}
						else
						{
							cGSTAmount+=0;
							iGSTAmount+=calculateProperTaxModel.getIgst()*returnQuantity;
							sGSTAmount+=0;						
						}
						cgstPercentage=returnOrderProductDetailsP.getProduct().getCategories().getCgst();
						igstPercentage=returnOrderProductDetailsP.getProduct().getCategories().getIgst();	
						sgstPercentage=returnOrderProductDetailsP.getProduct().getCategories().getSgst();
						hsncode=returnOrderProductDetailsP.getProduct().getCategories().getHsnCode();
					}
				}
			}
			
			categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(
					hsncode, 
					decimalFormat.format(taxableValue), 
					decimalFormat.format(cgstPercentage), 
					decimalFormatThreeDigit.format(cGSTAmount), 
					decimalFormat.format(igstPercentage), 
					decimalFormat.format(iGSTAmount), 
					decimalFormat.format(sgstPercentage), 
					decimalFormatThreeDigit.format(sGSTAmount)));
			
			totalAmount+=taxableValue;
			totalCGSTAmount+=cGSTAmount;
			totalSGSTAmount+=sGSTAmount;
			totalIGSTAmount+=iGSTAmount;			
		}
		taxAmount=(
				Float.parseFloat(decimalFormatThreeDigit.format(totalSGSTAmount))+
				Float.parseFloat(decimalFormatThreeDigit.format(totalCGSTAmount))+
				Float.parseFloat(decimalFormat.format(totalIGSTAmount)));
		//total amount without tax in word
		taxAmountInWord=NumberToWordsConverter.convertWithPaisa(Float.parseFloat(decimalFormat.format(taxAmount)));
		
		//find round of amount
		totalAmountWithTax=Float.parseFloat(decimalFormat.format(totalAmountWithTax));
		float decimalAmount=totalAmountWithTax-(int)totalAmountWithTax;
		float roundOf;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOf=0.0f;
			roundOfAmount=""+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else if(decimalAmount>=0.5)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="+"+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else
		{
			roundOf=decimalAmount;
			roundOfAmount="-"+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}	
		
		/*if(decimalAmount>0.0f)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/
		
		totalAmountWithTaxInWord=NumberToWordsConverter.convertWithPaisa(Float.parseFloat(decimalFormat.format(totalAmountWithTax)));
		
		/**
		 * calculate category wise block under discount and net amount 
		 */
//		double discountAmtCut = 0, discountPerCut = 0;
//		if (orderDetails.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
//			discountAmtCut = proformaOrder.getDiscount();
//			discountPerCut = (proformaOrder.getDiscount() / totalAmountWithTax) * 100;
//		} else {
//			discountPerCut = proformaOrder.getDiscount();
//			discountAmtCut = (proformaOrder.getDiscount() * totalAmountWithTax) / 100;
//		}
		double totalAmountDisc=0;//(discountPerCut*totalAmount)/100;
		double totalCGSTAmountDisc=0;//(totalCGSTAmount*totalAmount)/100;
		double totalIGSTAmountDisc=0;//(totalIGSTAmount*totalAmount)/100;
		double totalSGSTAmountDisc=0;//(totalSGSTAmount*totalAmount)/100;
		
		double netTotalAmount=totalAmount-totalAmountDisc;
		double netTotalCGSTAmount=totalCGSTAmount-totalCGSTAmountDisc;
		double netTotalIGSTAmount=totalIGSTAmount-totalIGSTAmountDisc;
		double netTotalSGSTAmount=totalSGSTAmount-totalSGSTAmountDisc;
		
		return new BillPrintDataModel(
				orderDetails.getOrderId(),
				invoiceNumber, 
				returnOrderProductP.getInvoiceNumber(),
				returnDate,
				orderDate, 
				addressLineList,
				businessName, 
				null,
				deliveryDate, 
				productListForBillList, 
				decimalFormatThreeDigit.format(totalCGSTAmount), 
				decimalFormat.format(totalIGSTAmount), 
				decimalFormatThreeDigit.format(totalSGSTAmount), 
				roundOfAmount,
				decimalFormat.format(totalAmountWithoutTax),
				String.valueOf(totalQuantity), 
				decimalFormat.format(totalAmountWithTax),
				"",
				"",
				"",
				"",
				"",//totalAmountWithTaxRoundOff,
				totalAmountWithTaxInWord, 
				categoryWiseAmountForBills, 
				decimalFormat.format(totalAmount), 
				taxAmountInWord, 
				decimalFormatThreeDigit.format(totalCGSTAmount), 
				decimalFormat.format(totalIGSTAmount), 
				decimalFormatThreeDigit.format(totalSGSTAmount),
				decimalFormat.format(totalAmountDisc),
				decimalFormatThreeDigit.format(totalCGSTAmountDisc),
				decimalFormatThreeDigit.format(totalIGSTAmountDisc),
				decimalFormatThreeDigit.format(totalSGSTAmountDisc),
				decimalFormat.format(netTotalAmount),
				decimalFormatThreeDigit.format(netTotalCGSTAmount),
				decimalFormatThreeDigit.format(netTotalIGSTAmount),
				decimalFormatThreeDigit.format(netTotalSGSTAmount),
				(orderDetails.getTransportation()==null)?"NA":orderDetails.getTransportation().getTransportName(),
				(orderDetails.getVehicleNo()==null)?"NA":orderDetails.getVehicleNo(),
				(orderDetails.getTransportation()==null)?"NA":orderDetails.getTransportation().getGstNo(),
				(orderDetails.getDocketNo()==null)?"NA":orderDetails.getDocketNo(),
				returnOrderProductP.getComment(),
				0);
		}
}
