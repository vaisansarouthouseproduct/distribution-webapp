package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BankingDAO;
import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.entities.Bank;
import com.bluesquare.rc.entities.BankAccount;
import com.bluesquare.rc.entities.BankAccountTempTransaction;
import com.bluesquare.rc.entities.BankAccountTransaction;
import com.bluesquare.rc.entities.BankCBTemplateDesign;
import com.bluesquare.rc.entities.BankChequeBook;
import com.bluesquare.rc.entities.BankChequeEntry;
import com.bluesquare.rc.entities.BankPayeeDetails;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.utils.Constants;

@Repository("bankingDAO")
@Component
public class BankingDAOImpl extends TokenHandler implements BankingDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	Company company;
	

	@Transactional
	public void insertAccount(BankAccount bankAccount) {
		Branch branch=branchDAO.fetchBranchByBranchId(getSessionSelectedBranchIds());
		bankAccount.setBranch(branch);
		if(bankAccount.getPrimary()){
			changePrimaryStatusForOtherAccounts(bankAccount.getId()+"");
		}
		sessionFactory.getCurrentSession().save(bankAccount);
		
	}

	@Transactional
	public void insertTempTransaction(BankAccountTempTransaction bankAccountTempTransaction) {
		if(bankAccountTempTransaction.getPayMode().equalsIgnoreCase(Constants.PAY_MODE_CHEQUE)){
			BankAccount bankAccount = bankAccountTempTransaction.getBankAccount();
			double chequeDepositedAmount = bankAccount.getChequeDepositedAmount();
			double chequeIssuedAmount = bankAccount.getChequeIssuedAmount();
			double debit = bankAccountTempTransaction.getDebit();
			double credit = bankAccountTempTransaction.getCredit();
			if(debit!=0){
				bankAccount.setChequeIssuedAmount(chequeIssuedAmount+debit);
			}
			if(credit!=0){
				bankAccount.setChequeDepositedAmount(chequeDepositedAmount+credit);
			}
			updateAccount(bankAccount);
		}
		
		sessionFactory.getCurrentSession().save(bankAccountTempTransaction);
		
	}

	@Transactional
	public void insertTransaction(BankAccountTransaction bankAccountTransaction) {
		BankAccount bankAccount = bankAccountTransaction.getBankAccount();
		double openingBalance = bankAccount.getActualBalance();
		double debit = bankAccountTransaction.getDebit();
		double credit = bankAccountTransaction.getCredit();
		double closingBalance = openingBalance - debit +credit;
		bankAccountTransaction.setOpening(openingBalance);
		bankAccountTransaction.setClosing(closingBalance);
		bankAccount.setActualBalance(closingBalance);
		double drawableBalance =  closingBalance - bankAccount.getMinimumBalance(); 
		bankAccount.setDrawableBalance(drawableBalance<0?0:drawableBalance);
		if(bankAccountTransaction.getPayMode().equalsIgnoreCase(Constants.PAY_MODE_CHEQUE)){
			double chequeDepositedAmount = bankAccount.getChequeDepositedAmount();
			double chequeIssuedAmount = bankAccount.getChequeIssuedAmount();
			if(debit!=0){
				bankAccount.setChequeIssuedAmount(chequeIssuedAmount-debit);
			}
			if(credit!=0){
				bankAccount.setChequeDepositedAmount(chequeDepositedAmount-credit);
			}
		}
		updateAccount(bankAccount);
		sessionFactory.getCurrentSession().save(bankAccountTransaction);
	}

	@Transactional
	public List<BankAccount> fetchBankAccounts() {
		String hql="from BankAccount where company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and primary=true";
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccount> bankAccounts = (List<BankAccount>)query.list();
		hql="from BankAccount where company.companyId="+getSessionSelectedCompaniesIds();
		hql+=" and primary=false";
		query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccount> bankAccounts1 = (List<BankAccount>)query.list();
		bankAccounts.addAll(bankAccounts1);
		return bankAccounts;
	}

	@Transactional
	public List<BankAccountTempTransaction> fetchTempTransactions(long bankAccountId,String fromDate,String toDate,String transactionStatus,int transactionCount) {
		String hql="from BankAccountTempTransaction where bankAccount="+bankAccountId;
		hql+=" and bankAccount.branch.branchId in ("+getSessionSelectedBranchIdsInQuery()+")";
		if(!transactionStatus.equalsIgnoreCase("NA")){
			if(transactionStatus.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_ISSUED)){
				hql+=" and debit<>0 and transactionStatus='"+Constants.TRANSACTION_PROCESSING+"'";
			}else if(transactionStatus.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_DEPOSITED)){
				hql+=" and credit<>0 and transactionStatus='"+Constants.TRANSACTION_PROCESSING+"'";
			}else if(transactionStatus.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_BOUNCED)){
				hql+=" and UPPER(TRIM(transactionStatus))='"+Constants.TRANSACTION_BOUNCED.toUpperCase().trim()+"'";
			}	
		}else{
			hql+=" and insertedDate>='"+fromDate+"' and insertedDate<='"+toDate+"'";
		}
		hql+=" ORDER BY id DESC";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		if(transactionCount!=-1){
			query.setMaxResults(transactionCount);
		}
		@SuppressWarnings("unchecked")
		List<BankAccountTempTransaction> bankAccounts = (List<BankAccountTempTransaction>)query.list();
		return bankAccounts;
	}

	@Transactional
	public List<BankAccountTransaction> fetchTransactions(long bankAccountId,String fromDate,String toDate,int transactionCount) {
		String hql="from BankAccountTransaction where bankAccount="+bankAccountId;
		hql+=" and bankAccount.branch.branchId in ("+getSessionSelectedBranchIdsInQuery()+")";
		if(transactionCount==-1){
			hql+=" and insertedDate>='"+fromDate+"' and insertedDate<='"+toDate+"'";
		}
		hql+=" ORDER BY id DESC";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		if(transactionCount!=-1){
			query.setMaxResults(transactionCount);
		}
		@SuppressWarnings("unchecked")
		List<BankAccountTransaction> bankAccounts = (List<BankAccountTransaction>)query.list();
		return bankAccounts;
	}

	@Transactional
	public BankAccount fetchBankAccountById(String id) {
		String hql="from BankAccount where id='"+id+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccount> bankAccounts = (List<BankAccount>)query.list();
		return bankAccounts.get(0);
	}

	@Transactional
	public BankAccountTempTransaction fetchTempTransactionById(String id) {
		String hql="from BankAccountTempTransaction where id='"+id+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccountTempTransaction> bankAccountTempTransactions = (List<BankAccountTempTransaction>)query.list();
		return bankAccountTempTransactions.get(0);
	}

	@Transactional
	public BankAccountTransaction fetchTransactionById(String id) {
		String hql="from BankAccountTransaction where id='"+id+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccountTransaction> bankAccountTransactions = (List<BankAccountTransaction>)query.list();
		return bankAccountTransactions.get(0);
	}

	@Transactional
	public void updateAccount(BankAccount bankAccount) {
		if(bankAccount.getPrimary()){
			changePrimaryStatusForOtherAccounts(bankAccount.getId()+"");
		}
		bankAccount=(BankAccount)sessionFactory.getCurrentSession().merge(bankAccount);
		sessionFactory.getCurrentSession().update(bankAccount);
	}

	@Transactional
	public void updateTempTransaction(BankAccountTempTransaction bankAccountTempTransaction) {
		bankAccountTempTransaction=(BankAccountTempTransaction)sessionFactory.getCurrentSession().merge(bankAccountTempTransaction);
		sessionFactory.getCurrentSession().update(bankAccountTempTransaction);
		
	}

	@Transactional
	public void changePrimaryStatusForOtherAccounts(String id) {
		String hql="Update BankAccount set primary=false where id<>'"+id+"'";
		hql+=" and company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
		
	}

	@Override
	public boolean accountNumberUsed(String bankAccountNumber) {
		String hql="from BankAccount where accountNumber='"+bankAccountNumber+"' ";
		hql+=" and company.companyId="+getSessionSelectedCompaniesIds();
		hql=modifyQueryAccordingSessionSelectedBranchIds(hql);
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankAccount> bankAccounts = (List<BankAccount>)query.list();
		return bankAccounts.size()==0?false:true;
	}

	@Transactional
	public void saveBank(Bank bank) {
		// TODO Auto-generated method stub
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		bank.setCompany(company);
		sessionFactory.getCurrentSession().save(bank);
		
	}
	
	@Transactional
	public void updateBank(Bank bank) {
		// TODO Auto-generated method stub
		bank=(Bank)sessionFactory.getCurrentSession().merge(bank);
		sessionFactory.getCurrentSession().update(bank);
		
	}

	@Transactional
	public List<Bank> fetchBankList() {
		// TODO Auto-generated method stub
		String hql = "from Bank where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Bank> bankList = (List<Bank>) query.list();

		if (bankList.isEmpty()) {
			return null;
		}
		return bankList;
	}

	@Transactional
	public Bank fetchBankByBankId(long bankId) {
		// TODO Auto-generated method stub
		String hql = "from Bank where bankId= "+bankId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Bank> bank=(List<Bank>)query.list();
		
		if(bank.isEmpty()){
			return null;
		}
		return bank.get(0);
	}

	@Transactional
	public void savePayeeDetails(BankPayeeDetails bankPayeeDetails) {
		// TODO Auto-generated method stub
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		bankPayeeDetails.setCompany(company);
		sessionFactory.getCurrentSession().save(bankPayeeDetails);
	}

	@Transactional
	public void updatePayeeDetails(BankPayeeDetails bankPayeeDetails) {
		// TODO Auto-generated method stub
		bankPayeeDetails=(BankPayeeDetails)sessionFactory.getCurrentSession().merge(bankPayeeDetails);
		sessionFactory.getCurrentSession().update(bankPayeeDetails);
	}

	@Transactional
	public List<BankPayeeDetails> fetchPayeeList() {
		String hql = "from BankPayeeDetails where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BankPayeeDetails> bankPayeeList = (List<BankPayeeDetails>) query.list();

		if (bankPayeeList.isEmpty()) {
			return null;
		}
		return bankPayeeList;
	}

	@Transactional
	public BankPayeeDetails fetchPayeeById(long payeeId) {
		// TODO Auto-generated method stub
		String hql = "from BankPayeeDetails where payeeId= "+payeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankPayeeDetails> bankPayee=(List<BankPayeeDetails>)query.list();
		
		if(bankPayee.isEmpty()){
			return null;
		}
		return bankPayee.get(0);
	}

	//cheque book method implementation
	
	@Transactional
	public void saveChequeBook(BankChequeBook bankChequeBook) {
		sessionFactory.getCurrentSession().save(bankChequeBook);
		
	}

	@Transactional
	public void updateChequeBook(BankChequeBook bankChequeBook) {
		// TODO Auto-generated method stub
		bankChequeBook=(BankChequeBook)sessionFactory.getCurrentSession().merge(bankChequeBook);
		sessionFactory.getCurrentSession().update(bankChequeBook);
	}


	@Transactional
	public List<BankChequeBook> fetchChequeBookList() {
		// TODO Auto-generated method stub
		String hql = "from BankChequeBook where bankAccount.company.companyId="+Long.parseLong(getSessionSelectedCompaniesIds());
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeBook> bankChequeBookList=(List<BankChequeBook>)query.list();
		if(bankChequeBookList.isEmpty()){
			return null;
		}
		for(BankChequeBook b:bankChequeBookList){
			List<String> unUsedChqNoList=fetchUnusedChequeNoListByCBId(b.getChequeBookId());
			b.setNumberOfUnusedLeaves(unUsedChqNoList.size());
			sessionFactory.getCurrentSession().update(b);
		}
		return bankChequeBookList;

	}

	@Transactional
	public BankAccount fetchBankAccountByLongId(long bankAccountId) {
		// TODO Auto-generated method stub
		String hql = "from BankAccount where id="+bankAccountId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankAccount> bankAccountList=(List<BankAccount>)query.list();
		if(bankAccountList.isEmpty()){
			return null;
		}
		return bankAccountList.get(0);
	}

	@Transactional
	public BankChequeBook fetchBankChequeBookById(long id) {
		// TODO Auto-generated method stub
		String hql="from BankChequeBook where chequeBookId="+id;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeBook> bankChequeBook=(List<BankChequeBook>)query.list();
		if (bankChequeBook.isEmpty()) {
			return null;
		}
		return bankChequeBook.get(0);
	}

	@Transactional
	public void saveChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(bankCBTemplateDesign);
	}

	@Transactional
	public void updateChequeTemplateDesign(BankCBTemplateDesign bankCBTemplateDesign) {
		// TODO Auto-generated method stub
		bankCBTemplateDesign=(BankCBTemplateDesign)sessionFactory.getCurrentSession().merge(bankCBTemplateDesign);
		sessionFactory.getCurrentSession().update(bankCBTemplateDesign);
	}

	@Transactional
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignList() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				String hql = "from BankCBTemplateDesign where bank.company.companyId="+Long.parseLong(getSessionSelectedCompaniesIds());
				Query query=sessionFactory.getCurrentSession().createQuery(hql);
				List<BankCBTemplateDesign> bankCBTemplateDesignList=(List<BankCBTemplateDesign>)query.list();
				if(bankCBTemplateDesignList.isEmpty()){
					return null;
				}
				return bankCBTemplateDesignList;

	}

	@Transactional
	public BankCBTemplateDesign fetchChequeTemplateDesignById(long id) {
		// TODO Auto-generated method stub
		String hql = "from BankCBTemplateDesign where id="+id;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankCBTemplateDesign> bankCBTemplateDesign=(List<BankCBTemplateDesign>)query.list();
		if(bankCBTemplateDesign.isEmpty()){
			return null;
		}
		return bankCBTemplateDesign.get(0);
	}

	@Transactional
	public List<BankCBTemplateDesign> fetchChequeTemplateDesignByBankId(long bankId) {
		// TODO Auto-generated method stub
		String hql = "from BankCBTemplateDesign where bank.bankId="+bankId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankCBTemplateDesign> bankCBTemplateDesign=(List<BankCBTemplateDesign>)query.list();
		if(bankCBTemplateDesign.isEmpty()){
			return null;
		}
		return bankCBTemplateDesign;
	}

	@Transactional
	public List<String> fetchUnusedChequeNoListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		List<String> chqNoListUnused=new ArrayList<>();
		List<String> chqNoListUsed=new ArrayList<>();
		List<String> allChequeNoList=new ArrayList<>();
		List<BankChequeEntry> bankChequeEntryListForRemove=new ArrayList<>();
		List<BankChequeEntry> bankChequeEntryList=new ArrayList<>();
		
		String hql1="from BankChequeBook where chequeBookId="+chequeBookId;
		Query query1=sessionFactory.getCurrentSession().createQuery(hql1);
		List<BankChequeBook> bankChequeBook=(List<BankChequeBook>)query1.list();
		
		
		// adding all cheque no to a String list
		for(int c=Integer.parseInt(bankChequeBook.get(0).getSrtChqNo());c<=Integer.parseInt(bankChequeBook.get(0).getEndChqNo());c++){
			String chqNo1=String.valueOf(c);
			while(chqNo1.length()!=6){
				chqNo1="0"+chqNo1;
			}
			allChequeNoList.add(chqNo1);
		}
		
		String hql="from BankChequeEntry where bankChequeBook.chequeBookId="+chequeBookId +" and printType='"+Constants.CHEQUE_PRINT_FROM_CHEQUE_BOOK+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeEntry> bankChequeEntryList1=(List<BankChequeEntry>)query.list();
		bankChequeEntryList.addAll(bankChequeEntryList1);
		
		String hql2="from BankChequeEntry where printType='"+Constants.CHEQUE_PRINT_MANUAL+"'";
		Query query2=sessionFactory.getCurrentSession().createQuery(hql2);
		List<BankChequeEntry> bankChequeEntryList2=(List<BankChequeEntry>)query2.list();
		
		bankChequeEntryList.addAll(bankChequeEntryList2);
		
		//adding all used cheque no to a String list 
		for(int l=0;l<bankChequeEntryList.size();l++){
			chqNoListUsed.add(bankChequeEntryList.get(l).getChequeNo());
		}
		
		
		// finding unused Chqno List
		for(String item:allChequeNoList){
			 if (chqNoListUsed.contains(item)) {
			       /* duplicateList.add(item);*/
			    } else {
			    	chqNoListUnused.add(item);
			    }
		}
		
		
		/*if(bankChequeEntryList.isEmpty()){
			
			for(int i=Integer.parseInt(bankChequeBook.get(0).getSrtChqNo());i<=Integer.parseInt(bankChequeBook.get(0).getEndChqNo());i++){
				String chqNo=String.valueOf(i);
				while(chqNo.length()!=6){
					chqNo="0"+chqNo;
				}
				
				
				chqNoListUnused.add(chqNo);
			}
			
		}else{
			String chqNo="";
		
			for(int j=Integer.parseInt(bankChequeBook.get(0).getSrtChqNo());j<=Integer.parseInt(bankChequeBook.get(0).getEndChqNo());j++){
				for(BankChequeEntry bankChequeEntry:bankChequeEntryList){
					
					if(j!=Integer.parseInt(bankChequeEntry.getChequeNo())){
						 chqNo=String.valueOf(j);
						while(chqNo.length()!=6){
							chqNo="0"+chqNo;
						}	
					}else{
						bankChequeEntryListForRemove.add(bankChequeEntry);
					}
				}
				bankChequeEntryList.removeAll(bankChequeEntryListForRemove);
				if(!chqNo.equals("")){
					chqNoListUnused.add(chqNo);
				}
				
			}
			
			
		}*/
		
		return chqNoListUnused;
	}

	@Transactional
	public List<BankChequeBook> fetchBankChequeBookListByAcoountId(long accountId) {
		// TODO Auto-generated method stub
		
		String hql="from BankChequeBook where bankAccount.id="+accountId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeBook> bankChequeBookList=(List<BankChequeBook>)query.list();
		if(bankChequeBookList.isEmpty()){
			return null;
		}
		return bankChequeBookList;
	}

	@Transactional
	public void saveManualPrintCheque(BankChequeEntry bankChequeEntry) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(bankChequeEntry);
	}
	
	@Transactional
	public void updateBankChequeEntry(BankChequeEntry bankChequeEntry){
		bankChequeEntry=(BankChequeEntry)sessionFactory.getCurrentSession().merge(bankChequeEntry);
		sessionFactory.getCurrentSession().save(bankChequeEntry);
	}
	
	@Transactional
	public BankChequeBook fetchChqBookByChqNoAndBankAccountId(String chequeNo, long bankAccountId){
		
		BankChequeBook bankChequeBook=new BankChequeBook();
		List<BankChequeBook> bankChequeBookList=fetchBankChequeBookListByAcoountId(bankAccountId);
		
		for(BankChequeBook bankChequeBook2:bankChequeBookList){
			if(Long.parseLong(chequeNo)>=Long.parseLong(bankChequeBook2.getSrtChqNo()) && Long.parseLong(chequeNo)<=Long.parseLong(bankChequeBook2.getEndChqNo())){
				bankChequeBook=bankChequeBook2;
			}				
		}
		return bankChequeBook;
	}
	
	
	/*@Transactional
	public long toFindUsedChequeFromCB(String strChqNo,String endChqNo){
		
		String hql="from BankChequeEntry where chequeNo>='"+strChqNo +"' and chequeNo<='"+endChqNo+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeEntry> bankChequeEntry=(List<BankChequeEntry>)query.list();
		if(bankChequeEntry.isEmpty()){
			return 0;
		}
		
		return bankChequeEntry.size();
	}
	*/
	
	@Transactional
	public BankChequeEntry fetchChequeEntryByChequeNo(String chqNo){
		String hql="from BankChequeEntry where chequeNo='"+chqNo+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BankChequeEntry> bankChequeEntryList=(List<BankChequeEntry>)query.list();
		if(bankChequeEntryList.isEmpty()){
			return null;
		}
		return bankChequeEntryList.get(0);
	}

	
	//fetching Cancel Cheque entry list
	@Transactional
	public List<BankChequeEntry> fetchCancelChequeEntryListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		
		List<BankChequeEntry> bankChequeEntryList=new ArrayList<>();
		List<String> allChequeNoList=new ArrayList<>();
		long cancelChequeCount=0;
		
		String hql1="from BankChequeBook where chequeBookId="+chequeBookId;
		Query query1=sessionFactory.getCurrentSession().createQuery(hql1);
		List<BankChequeBook> bankChequeBook=(List<BankChequeBook>)query1.list();
		
		
		// adding all cheque no to a String list
		for(int c=Integer.parseInt(bankChequeBook.get(0).getSrtChqNo());c<=Integer.parseInt(bankChequeBook.get(0).getEndChqNo());c++){
			String chqNo1=String.valueOf(c);
			while(chqNo1.length()!=6){
				chqNo1="0"+chqNo1;
			}
			allChequeNoList.add(chqNo1);
		}
		
		for(String s:allChequeNoList){
			BankChequeEntry bankChequeEntry=fetchChequeEntryByChequeNo(s);
			if(bankChequeEntry!=null){
				if(bankChequeEntry.getChequeStatus().equals(Constants.CHEQUE_PRINT_CANCEL_STATUS)){
					bankChequeEntryList.add(bankChequeEntry);
				}
			}
			
		}
	
		
		
		return bankChequeEntryList;
	}

	
	@Transactional
	public List<BankChequeEntry> fetchPrintedChequeEntryListByCBId(long chequeBookId) {
		// TODO Auto-generated method stub
		
		List<BankChequeEntry> bankChequeEntryList=new ArrayList<>();
		List<String> allChequeNoList=new ArrayList<>();
		long cancelChequeCount=0;
		
		String hql1="from BankChequeBook where chequeBookId="+chequeBookId;
		Query query1=sessionFactory.getCurrentSession().createQuery(hql1);
		List<BankChequeBook> bankChequeBook=(List<BankChequeBook>)query1.list();
		
		
		// adding all cheque no to a String list
		for(int c=Integer.parseInt(bankChequeBook.get(0).getSrtChqNo());c<=Integer.parseInt(bankChequeBook.get(0).getEndChqNo());c++){
			String chqNo1=String.valueOf(c);
			while(chqNo1.length()!=6){
				chqNo1="0"+chqNo1;
			}
			allChequeNoList.add(chqNo1);
		}
		
		for(String s:allChequeNoList){
			BankChequeEntry bankChequeEntry=fetchChequeEntryByChequeNo(s);
			if(bankChequeEntry!=null){
				if(bankChequeEntry.getChequeStatus().equals(Constants.CHEQUE_PRINT_PROCEED_STATUS)){
					bankChequeEntryList.add(bankChequeEntry);
				}
			}
			
		}
	
		
		
		return bankChequeEntryList;
	}

	
}
