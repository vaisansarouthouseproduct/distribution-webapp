package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.RouteDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.RoutePoints;
import com.bluesquare.rc.utils.RouteIdGenerator;

/**
 * <pre>
 * @author <b>Sachin Pawar 05-06-2018 Code Documentation</b>
 * Provides Implementation for following methods of RouteDAO
 * saveRoute(Route route)
 * updateRoute(Route route)
 * saveRoutePointsList(List<RoutePoints> routeList)	
 * deleteRoutePointsList(List<RoutePoints> routeList)
 * fetchRouteList()
 * fetchRoutePointsListByRouteId(long routeId)
 * fetchRouteByRouteId(long routeId)
 * fetchRoutePointsByRoutePoints╠d(long routePointsId)
 * </pre>
 */

@Repository("routeDAO")
@Component
public class RouteDAOImpl extends TokenHandler implements RouteDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	RouteIdGenerator routeIdGenerator;
	
	@Autowired
	CompanyDAO companyDAO;
	/**
	 * <pre>
	 * save Route by company
	 * @param route
	 * </pre>
	 */
	@Transactional
	public void saveRoute(Route route) {
		// TODO Auto-generated method stub
		Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		route.setRouteGenId(routeIdGenerator.generate());
		route.setCompany(company);
		sessionFactory.getCurrentSession().save(route);
	}
	/**
	 * <pre>
	 * update route
	 * @param route
	 * </pre>
	 */
	@Transactional
	public void updateRoute(Route route) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(route);
	}
	/**
	 * <pre>
	 * save route points according route
	 * @param routePointsList
	 * </pre>
	 */
	@Transactional
	public void saveRoutePointsList(List<RoutePoints> routePointsList) {
		// TODO Auto-generated method stub
		for(RoutePoints routePoints : routePointsList){
			sessionFactory.getCurrentSession().save(routePoints);
		}
	}
	/**
	 * <pre>
	 * delete route point before update new route points according any route
	 * @param routePointsList
	 * </pre>
	 */
	@Transactional
	public void deleteRoutePointsList(long routeId) {
		// TODO Auto-generated method stub
		String hql="delete from RoutePoints where route.routeId="+routeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
		
	}
	/**
	 * <pre>
	 * fetch route list 
	 * @return Route list
	 * </pre>
	 */
	@Transactional
	public List<Route> fetchRouteList() {
		// TODO Auto-generated method stub
		String hql="from Route where status=false";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty()){
			return null;
		}
		return routeList;
	}
	/**
	 * <pre>
	 * fetch route points by route id
	 * @param routeId
	 * @return RoutePoints list
	 * </pre>
	 */
	@Transactional
	public List<RoutePoints> fetchRoutePointsListByRouteId(long routeId) {
		String hql="from RoutePoints where route.routeId="+routeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<RoutePoints> routePointsList=(List<RoutePoints>)query.list();
		if(routePointsList.isEmpty()){
			return null;
		}
		return routePointsList;
	}
	/**
	 * <pre>
	 * fetch route by route id
	 * @param routeId
	 * @return Route
	 * </pre>
	 */
	@Transactional
	public Route fetchRouteByRouteId(long routeId) {
		String hql="from Route where routeId="+routeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty()){
			return null;
		}
		return routeList.get(0);
	}
	/**
	 * <pre>
	 * fetch route point details by routePointId
	 * @param routePointsId
	 * @return RoutePoints
	 * </pre>
	 */
	@Transactional
	public RoutePoints fetchRoutePointsByRoutePointsId(long routePointsId) {
		String hql="from RoutePoints where routePointsId="+routePointsId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<RoutePoints> routePointsList=(List<RoutePoints>)query.list();
		if(routePointsList.isEmpty()){
			return null;
		}
		return routePointsList.get(0);
	}

	/**
	 * <pre>
	 * check route name already exist or not
	 * @param routeName
	 * @return Route
	 * </pre>
	 */
	@Transactional
	public Route fetchRouteByRouteName(String routeName) {
		String hql="from Route where routeName='"+routeName+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty()){
			return null;
		}
		return routeList.get(0);
	}
	
	/*Check Route Name Duplication*/
	@Transactional
	public Route checkRouteNameDuplicate(String routeName,long routeId) {
		String hql="from Route where routeName='"+routeName+"' and routeId !="+routeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty()){
			return null;
		}
		return routeList.get(0);
	}
	/**
	 * fetch route list by route ids
	 * @param routeIds
	 * @return
	 */
	@Transactional
	public List<Route> fetchRouteListByRouteIdList(long[] routeIds) {

		String routeIdStr = "";
		for (long routeId : routeIds) {
			routeIdStr += routeId + ",";
		}
		routeIdStr = routeIdStr.substring(0, routeIdStr.length() - 1);

		String hql = "from Route where routeId in (" + routeIdStr + ")";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList = (List<Route>) query.list();
		
		if (routeList.isEmpty()) {
			return null;
		}
		
		return routeList;
	}

}
