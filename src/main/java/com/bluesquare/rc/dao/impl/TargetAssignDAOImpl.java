package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.CommissionAssignDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.ShopVisitDAO;
import com.bluesquare.rc.dao.TargetAssignDAO;
import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.EmployeeCommission;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.entities.TargetType;
import com.bluesquare.rc.models.EmployeeCommissionDetails;
import com.bluesquare.rc.models.TargetAssignDetails;
import com.bluesquare.rc.models.TargetsAndEmployeeByDeptResponse;
import com.bluesquare.rc.rest.models.TargetTypeModel;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DateUtils;
import com.bluesquare.rc.utils.MathUtils;
/**
 * <pre>
 * @author Sachin Pawar 02-06-2018 Code Documentation
 * Provides Implementation for following methods of TargetAssignDAO
 * 1.saveTargetAssign(TargetAssign targetAssign)
 * 2.updateTargetAssign(TargetAssign targetAssign)
 * 3.saveTargetAssignedTargets(TargetAssignTargets targetAssignTargets)
 * 4.fetchTargetAssignByTargetAssignId(long targetAssignId)
 * 5.deleteTargetAssignedTargets(long targetAssignId)
 * 6.fetchTargetAssignTargetsByTargetAssignId(long targetAssignId)
 * 7.fetchTargetListByTargetPeriod(String targetPeriod,String currentFuture,long departmentId)
 * 8.fetchTargetTypesByDepartmentId(long departmentId)
 * 9.deleteTargetAssigned(long targetAssignId)
 * 10.saveTargetAssignRegular(TargetAssignRegular targetAssignRegular)
 * 11.updateTargetAssignRegular(TargetAssignRegular targetAssignRegular)
 * 12.saveTargetAssignedTargetsRegular(TargetAssignRegularTargets targetAssignRegularTargets)
 * 13.fetchTargetAssignRegularByTargetAssignRegularId(long targetAssignRegularId)
 * 14.deleteTargetAssignedRegularTargets(long targetAssignRegularId)
 * 15.fetchTargetAssignRegularTargetsByTargetAssignRegularId(long targetAssignRegularId)
 * 16.checkEmployeeAlreadyHaveTargetOrNot(long employeeDetailsId)
 * 17.fetchTargetsAndEmployeeListByDepartmentId(long departmentId)
 * 18.fetchTargetAssignByTargetPeriods(TargetRequestModel requestModel)
 * 19.createNextDayTarget(String targetPeriod)
 * </pre>
 */
@Repository("targetAssignDAO")
@Component
public class TargetAssignDAOImpl extends TokenHandler implements TargetAssignDAO {

	@Autowired
	SessionFactory sessionFactory;
	 
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;

	@Autowired
	ShopVisitDAO shopVisitDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	BusinessNameDAO businessNameDAO;
	
	@Autowired
	CommissionAssignDAO commissionAssignDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	/* Period Wise */
	/**
	 * <pre>
	 * save target assigned
	 * @param targetAssign
	 * </pre>
	 */
	@Transactional
	public void saveTargetAssign(TargetAssign targetAssign) {
		sessionFactory.getCurrentSession().save(targetAssign);
	}
	/**
	 * <pre>
	 * update target assigned
	 * @param targetAssign
	 * </pre>
	 */
	@Transactional
	public void updateTargetAssign(TargetAssign targetAssign) {
		sessionFactory.getCurrentSession().update(targetAssign);
	}
	/**
	 * <pre>
	 * save target assign targets assigned
	 * @param targetAssignTargets
	 * </pre>
	 */
	@Transactional
	public void saveTargetAssignedTargets(TargetAssignTargets targetAssignTargets) {
		sessionFactory.getCurrentSession().save(targetAssignTargets);
	}
	/**
	 * <pre>
	 * fetch target assigned by target assigned id
	 * @param targetAssignId
	 * @return TargetAssign
	 * </pre>
	 */
	@Transactional
	public TargetAssign fetchTargetAssignByTargetAssignId(long targetAssignId){
		return (TargetAssign)sessionFactory.getCurrentSession().get(TargetAssign.class, targetAssignId);
	}
	/**
	 * <pre>
	 * delete target assigned targets by targetAssignedId
	 * @param targetAssignId
	 * </pre>
	 */
	@Transactional
	public void deleteTargetAssignedTargets(long targetAssignId){
		String hql="delete from TargetAssignTargets where targetAssign.targetAssignId="+targetAssignId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		query.executeUpdate();
	}
	/**
	 * <pre>
	 * fetch target assign target list by target assign id
	 * @param targetAssignId
	 * @return TargetAssignTargets list
	 * </pre>
	 */
	@Transactional
	public List<TargetAssignTargets> fetchTargetAssignTargetsByTargetAssignId(long targetAssignId){
		String hql="from TargetAssignTargets where targetAssign.targetAssignId="+targetAssignId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssignTargets> targetAssignTargetsList=(List<TargetAssignTargets>)query.list();
		return targetAssignTargetsList;
	}
	/**
	 * <pre>
	 * fetch target assign target list by target assign id
	 * @param targetAssignId
	 * @return TargetAssignTargets list
	 * </pre>
	 */
	@Transactional
	public List<TargetAssignTargets> fetchTargetAssignTargetsByTargetAssignIdAndTargetPeriod(long targetAssignId,String targetPeriod){
		String hql="from TargetAssignTargets where targetAssign.targetAssignId="+targetAssignId;
		hql+=" and commissionAssign.targetPeriod='"+targetPeriod+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssignTargets> targetAssignTargetsList=(List<TargetAssignTargets>)query.list();
		return targetAssignTargetsList;
	}
	
	/**
	 * <pre>
	 * fetch targetAssign list by employeeDetailsId
	 * </pre>
	 * @param employeeDetailsId
	 * @return
	 */
	public TargetAssign fetchTargetAssignByEmployeeDetailsId(long employeeDetailsId){
		String hql="from TargetAssign where 1=1 "
				+ " and status=false "
			    + " and employeeDetails.employeeDetailsId="+employeeDetailsId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssign> targetAssignList=(List<TargetAssign>)query.list();
		
		if(targetAssignList.isEmpty()){
			return null;
		}
		return targetAssignList.get(0);
	}
	/**
	 * <pre>
	 * fetch target list by targetPeriod,currentFuture,departmentId
	 * @param targetPeriod
	 * @param currentFuture
	 * @param departmentId
	 * @return TargetLists list
	 * </pre>
	 */
	@Transactional
	public List<TargetAssignDetails> fetchTargetListByTargetPeriod(long departmentId){
		List<TargetAssignDetails> targetAssignDetails=new ArrayList<>();
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar=Calendar.getInstance();
	
		
		String additionalCondition="";
		if(departmentId!=0){
			additionalCondition+=" and employeeDetails.employee.department.departmentId="+departmentId;
		}
		
		String hql="from TargetAssign where 1=1 "+
					additionalCondition
					+ " and status=false "
				    + " and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssign> targetAssignList=(List<TargetAssign>)query.list();
		
		if(targetAssignList.isEmpty()){
			return null;
		}else{
			long srno=1;
			for(TargetAssign targetAssign: targetAssignList){
				List<TargetAssignTargets> targetAssignTargetList=fetchTargetAssignTargetsByTargetAssignId(targetAssign.getTargetAssignId());
				
				List<EmployeeCommissionDetails> employeeCommissionDetails=new ArrayList<>();
				for(TargetAssignTargets targetAssignTargets : targetAssignTargetList){
					employeeCommissionDetails.add(new EmployeeCommissionDetails(
							targetAssignTargets.getCommissionAssign().getCommissionAssignId(),
							targetAssignTargets.getCommissionAssign().getCommissionAssignName(), 
							targetAssignTargets.getTargetValue()));
				}
				
				targetAssignDetails.add(new TargetAssignDetails(
						srno, 
						targetAssign.getTargetAssignId(),
						targetAssign.getEmployeeDetails().getName(), 
						targetAssign.getEmployeeDetails().getEmployee().getDepartment().getName(), 
						employeeCommissionDetails));
				
				srno++;
			}
			return targetAssignDetails;
		}
	}
	
	/**
	 * <pre>
	 * fetch target type list by department id
	 * @param departmentId
	 * @return TargetType list
	 * </pre>
	 */
	@Transactional 
	public List<TargetType> fetchTargetTypesByDepartmentId(long departmentId){
		String hql="from TargetType where department.departmentId="+departmentId+
				   " and company.companyId="+getSessionSelectedCompaniesIds();
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetType> targetTypeList=(List<TargetType>)query.list();
		
		if(targetTypeList.isEmpty()){
			return null;
		}
		
		return targetTypeList;
	}
	
	/**
	 * <pre>
	 * disable target assign
	 * @param targetAssignId
	 * </pre>
	 */
	@Transactional
	public void deleteTargetAssigned(long targetAssignId){
		TargetAssign targetAssign=fetchTargetAssignByTargetAssignId(targetAssignId);
		targetAssign.setStatus(true);
		updateTargetAssign(targetAssign);
	}
	
	@Transactional
	public TargetsAndEmployeeByDeptResponse fetchTargetsAndEmployeeListByDepartmentId(long departmentId){
		List<EmployeeDetails> employeeDetailsList=new ArrayList<>();//employeeDetailsDAO.fetchEmployeeDetailsByDepartmentId(departmentId);
		List<TargetType> targetTypes=fetchTargetTypesByDepartmentId(departmentId);
		List<TargetTypeModel> targetTypesNew=new ArrayList<>();
		for(int i=0; i<targetTypes.size(); i++){
			TargetType targetType=targetTypes.get(i);
			targetTypesNew.add(new TargetTypeModel(
					targetType.getTargetTypeId(), 
					targetType.getType(), 
					targetType.getValueType()));
		}
		
		return new TargetsAndEmployeeByDeptResponse(targetTypesNew, employeeDetailsList);
	} 
	
	/**
	 * fetch already assigned commission employee ids
	 * @param employeeId
	 * @return
	 */
	@Transactional
	public List<Long> commissionAssignedEmployeeIds(){
		
		String hql="SELECT employeeDetails.employee.employeeId from TargetAssign";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> employeeIds=(List<Long>)query.list();
		if(employeeIds.isEmpty()){
			return null;
		}
		
		return employeeIds;
	}
	
	/**
	 * <pre>
	 * check employee already have target assigned or not
	 * @param employeeDetailsId
	 * @return msg if found
	 * </pre>
	 *//*
	@Transactional
	public String checkEmployeeAlreadyHaveTargetOrNot(long employeeId){
		
		String hql="from TargetAssign where status=false and employeeDetails.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssign> targetAssignRegularList=(List<TargetAssign>)query.list();
		if(targetAssignRegularList.isEmpty()){
			return "";
		}
		
		return targetAssignRegularList.get(0).getEmployeeDetails().getName()+" already have "+targetAssignRegularList.get(0).getTargetPeriod();
	}*/

	/**
	 * <pre>
	 * fetch target assign details list by target period with dates
	 * @param requestModel
	 * </pre>
	 */
	/*@Transactional
	public TargetMainResponse fetchTargetAssignByTargetPeriods(TargetRequestModel requestModel){
		String hql="from TargetAssignTargets where 1=1 ";
		hql += " and targetAssign.targetPeriod='"+requestModel.getTargetPeriod()+"' "
			+ " and targetAssign.employeeDetails.employee.employeeId="+requestModel.getEmployeeId()
			+ " and targetAssign.status=false ";
		
		if(requestModel.getTargetPeriod().equals(Constants.TARGET_PERIOD_DAILY)){
			hql +=" and date(targetAssign.fromDatetime)='"+requestModel.getPickDate()+"' "
				    + " and date(targetAssign.toDatetime)='"+requestModel.getPickDate()+"'";
		}else if(requestModel.getTargetPeriod().equals(Constants.TARGET_PERIOD_WEEKLY)){
			hql +=" and date(targetAssign.fromDatetime)='"+requestModel.getWeekStartDate()+"' "
				    + " and date(targetAssign.toDatetime)='"+requestModel.getWeekEndDate()+"'";
		}else if(requestModel.getTargetPeriod().equals(Constants.TARGET_PERIOD_MONTHLY)){
			hql +=" and MONTH(targetAssign.fromDatetime)='"+requestModel.getMonthNo()+"' and Year(targetAssign.fromDatetime)='"+requestModel.getMonthYear()+"' ";
		}else if(requestModel.getTargetPeriod().equals(Constants.TARGET_PERIOD_YEARLY)){
			hql +=" and year(targetAssign.fromDatetime)='"+requestModel.getYear()+"' ";
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssignTargets> targetAssignTargetsList=(List<TargetAssignTargets>)query.list();
		
		int i=1;
		List<TargetResponse> targetResponseList=new ArrayList<>();
		for(TargetAssignTargets targetAssignTargets: targetAssignTargetsList){
			
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			double completeValue=0;
			if(targetAssignTargets.getCommissionAssign().getTargetType().getType().equals("no of shop visited")){
				int noOfShopVisited=shopVisitDAO.findNoOfShopVisitBetweenTwoDates(dateFormat.format(targetAssignTargets.getTargetAssign().getFromDatetime()), dateFormat.format(targetAssignTargets.getTargetAssign().getToDatetime()), targetAssignTargets.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
				completeValue=noOfShopVisited;
			}else if(targetAssignTargets.getCommissionAssign().getTargetType().getType().equals("total no of order taken")){
				int noOfOrderTaken=orderDetailsDAO.findNumberOfOrderTakenByDateRange(dateFormat.format(targetAssignTargets.getTargetAssign().getFromDatetime()), dateFormat.format(targetAssignTargets.getTargetAssign().getToDatetime()), targetAssignTargets.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
				completeValue=noOfOrderTaken;
			}else if(targetAssignTargets.getCommissionAssign().getTargetType().getType().equals("total sales amount")){
				double noOfOrderTaken=orderDetailsDAO.totalSaleAmountForTarget(dateFormat.format(targetAssignTargets.getTargetAssign().getFromDatetime()), dateFormat.format(targetAssignTargets.getTargetAssign().getToDatetime()), targetAssignTargets.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
				completeValue=noOfOrderTaken;
			}else if(targetAssignTargets.getCommissionAssign().getTargetType().getType().equals("new business added")){
				int numberOfBusinessAdded=businessNameDAO.findNumberOfBusinessAddedByDateRange(dateFormat.format(targetAssignTargets.getTargetAssign().getFromDatetime()), dateFormat.format(targetAssignTargets.getTargetAssign().getToDatetime()), targetAssignTargets.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
				completeValue=numberOfBusinessAdded;
			}
			List<CommissionAssignTargetSlabs> commissionAssignTargetSlabList=commissionAssignDAO.fetchCommissionAssignTargetSlabs(targetAssignTargets.getCommissionAssign().getCommissionAssignId());
			List<CommissionAssignTargetSlabsModel> commissionAssignTargetSlabsModelList=new ArrayList<>();
			for(CommissionAssignTargetSlabs commissionAssignTargetSlabs : commissionAssignTargetSlabList){
				commissionAssignTargetSlabsModelList.add(new CommissionAssignTargetSlabsModel(
						commissionAssignTargetSlabs.getCommissionAssignTargetSlabId(), 
						commissionAssignTargetSlabs.getFrom(), 
						commissionAssignTargetSlabs.getTo(), 
						commissionAssignTargetSlabs.getCommissionType(), 
						commissionAssignTargetSlabs.getTargetValue(), 
						commissionAssignTargetSlabs.getCommissionAssign().getTargetType().getType()));
			}
			targetResponseList.add(new TargetResponse(i,targetAssignTargets.getCommissionAssign().getTargetType().getType(), targetAssignTargets.getTargetValue(), completeValue,targetAssignTargets.getCommissionAssign().getCommissionAssignId(),commissionAssignTargetSlabList));
			i++;
		}
		
		TargetMainResponse targetMainResponse=new TargetMainResponse();
		
		if(targetResponseList.isEmpty()){
			targetMainResponse.setTargetResponseList(null);
		}else{
			targetMainResponse.setTargetResponseList(targetResponseList);
		}
		
		return targetMainResponse;
	}*/

	public static List<Date> splitDates(Date startDate,Date endDate){
		List<Date> dates=new ArrayList<>();
		
		Calendar calStart=Calendar.getInstance();
		calStart.setTime(startDate);
		System.out.println(calStart.getTime());
		
		dates.add(startDate);
		int monthNum=calStart.get(Calendar.MONTH);
		
		Calendar calEnd=Calendar.getInstance();
		calEnd.setTime(endDate);
		System.out.println(calEnd.getTime());
		
		while(calStart.before(calEnd) || calStart.equals(calEnd)){
			if(monthNum==calStart.get(Calendar.MONTH)){
				
			}else{
				Calendar cal=Calendar.getInstance();
				cal.setTime(calStart.getTime());
				cal.add(Calendar.DAY_OF_MONTH, -1);
				dates.add(cal.getTime());
				dates.add(calStart.getTime());
				monthNum=calStart.get(Calendar.MONTH);
			}
			calStart.add(Calendar.DAY_OF_MONTH, 1);
		}
		dates.add(endDate);
		return dates;
	}
	public static void main(String[] args) {
		/*try{
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(new SimpleDateFormat("yyyy-MM-dd").parse("2018-06-28"));
			
			Calendar calEnd=Calendar.getInstance();
			calEnd.setTime(new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-03"));
			
			List<Date> dates= splitDates(calStart.getTime(), calEnd.getTime());
			System.out.println(dates.toString());
		}catch(Exception e){
			System.out.println(e.toString());
		}*/
	}
	/**
	 * <pre>
	 * create next target scheduler by target period
	 * @param targetPeriod
	 * </pre>
	 */
	@Transactional
	public void createNextDayTarget(String targetPeriod){
		
		String hql="from TargetAssign where 1=1 ";
			   hql +=" and status=false ";
	   
	   Query query=sessionFactory.getCurrentSession().createQuery(hql);
	   List<TargetAssign> targetAssignList=(List<TargetAssign>)query.list();
	   
	   Calendar cal=Calendar.getInstance();	   	   
	   
	   Date startDate=new Date(),endDate=new Date();
	   
	   if(targetPeriod.equals(Constants.TARGET_PERIOD_DAILY)){
		   cal.add(Calendar.DAY_OF_MONTH, -1);
		   startDate=cal.getTime();
		   endDate=cal.getTime();	
		   assignIncentive(startDate, endDate, targetAssignList, targetPeriod);
	   }else if(targetPeriod.equals(Constants.TARGET_PERIOD_WEEKLY)){
		   startDate=DateUtils.getPreviousWeekStartDate();
		   endDate=DateUtils.getPreviousWeekEndDate();
		   
		   Calendar calStart=Calendar.getInstance();
			calStart.setTime(startDate);
			
			Calendar calEnd=Calendar.getInstance();
			calEnd.setTime(endDate);
		   
		   if(calStart.get(Calendar.MONTH)==calEnd.get(Calendar.MONTH)){
			   assignIncentive(startDate, endDate, targetAssignList, targetPeriod);
		   }else{
			   List<Date> dates=splitDates(startDate, endDate);
			   assignIncentive(dates.get(0), dates.get(1), targetAssignList, targetPeriod);
			   assignIncentive(dates.get(2), dates.get(3), targetAssignList, targetPeriod);
		   }
		   
	   }else if(targetPeriod.equals(Constants.TARGET_PERIOD_MONTHLY)){
		   startDate=DateUtils.getPreviousMonthStartDate();
		   endDate=DateUtils.getPreviousMonthLastDate();
		   
	   }/*else if(targetPeriod.equals(Constants.TARGET_PERIOD_YEARLY)){
		   startDate=DateUtils.getNextYearStartDate();
		   endDate=DateUtils.getNextYearLastDate();
	   }*/
	   
	   
	}	
	
	public void assignIncentive(Date startDate,Date endDate , List<TargetAssign> targetAssignList,String targetPeriod){
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(startDate);
		
		   for(TargetAssign targetAssign: targetAssignList){
			    
			    double totalAmount=0;
			    EmployeeCommission employeeCommission=commissionAssignDAO.fetchEmployeeCommissionByMonthAndYear(calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR),targetAssign.getEmployeeDetails().getEmployeeDetailsId());
			    List<com.bluesquare.rc.entities.EmployeeCommissionDetails> employeeCommissionDetailsList=new ArrayList<>();
			    List<com.bluesquare.rc.entities.EmployeeCommissionDetails> employeeCommissionDetailsNewList=new ArrayList<>();
			    if(employeeCommission==null){
			    	employeeCommission=new EmployeeCommission();
			    	employeeCommission.setDatetime(new Date());
					employeeCommission.setEmployeeDetails(targetAssign.getEmployeeDetails());
			    }else{
			    	employeeCommissionDetailsList=commissionAssignDAO.fetchEmployeeCommissionDetails(employeeCommission.getEmployeeCommissionId());
			    }
				
				
				List<TargetAssignTargets> targetAssignTargets=fetchTargetAssignTargetsByTargetAssignIdAndTargetPeriod(targetAssign.getTargetAssignId(),targetPeriod);
			    for(TargetAssignTargets targetAssignTarget: targetAssignTargets){
			    	double incentiveAmount=0;
			    	
			    	 //Incentive Calculating Start
			    	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			    	double completeValue=0;
			    	if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("no of shop visited")){
						int noOfShopVisited=shopVisitDAO.findNoOfShopVisitBetweenTwoDates(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
						completeValue=noOfShopVisited;
					}else if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("total no of order taken")){
						int noOfOrderTaken=orderDetailsDAO.findNumberOfOrderTakenByDateRange(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
						completeValue=noOfOrderTaken;
					}else if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("total sales amount")){
						double totalSaleAmount=orderDetailsDAO.totalSaleAmountForTarget(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId(),targetAssignTarget.getCommissionAssign().isIncludeTax());
						completeValue=totalSaleAmount; 
					}else if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("new business added")){
						int numberOfBusinessAdded=businessNameDAO.findNumberOfBusinessAddedByDateRange(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId());
						completeValue=numberOfBusinessAdded;
					}else if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("Product Wise Target")){
						List<CommissionAssignProducts> commissionAssignProductsList=commissionAssignDAO.fetchCommissionAssignProducts(targetAssignTarget.getCommissionAssign().getCommissionAssignId());
						for(CommissionAssignProducts commissionAssignProducts : commissionAssignProductsList){
							if(targetAssignTarget.getCommissionAssign().getAmountOrQtyTypeSlab().equals("amount")){
								completeValue=orderDetailsDAO.findNumberOfProductSalesByDateRangeAndEmployeeId(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId(),commissionAssignProducts.getProduct().getProductId(),targetAssignTarget.getCommissionAssign().isIncludeTax());
							}else{
								long totalQuantitySale=orderDetailsDAO.findNumberOfProductSalesByDateRangeAndEmployeeIdForQuantity(dateFormat.format(startDate), dateFormat.format(endDate), targetAssignTarget.getTargetAssign().getEmployeeDetails().getEmployee().getEmployeeId(),commissionAssignProducts.getProduct().getProductId());
								completeValue=totalQuantitySale;
							}
						}
					}
			    	
			    	if(completeValue!=0){			
			    		/*if(targetAssignTarget.getCommissionAssign().getTargetType().getType().equals("Product List Wise")){
			    			EmployeeIncentives employeeIncentives=new EmployeeIncentives();
					    	employeeIncentives.setEmployeeDetails(targetAssign.getEmployeeDetails());
							employeeIncentives.setIncentiveAmount(completeValue);
							employeeIncentives.setReason(targetAssignTarget.getCommissionAssign().getTargetPeriod()+" Target Incentive Added ");
							employeeIncentives.setIncentiveGivenDate(new Date());
							employeeIncentives.setStatus(false);
							employeeDetailsDAO.giveIncentives(employeeIncentives);
			    		}else{*/
			    			CommissionAssignTargetSlabs commissionAssignTargetSlabs=commissionAssignDAO.findIncentiveAmountByCompleteTargetValue(completeValue,targetAssignTarget.getCommissionAssign().getCommissionAssignId());
					    	if(commissionAssignTargetSlabs!=null){
					    		if(targetAssignTarget.getCommissionAssign().isCascadingCommission()){
					    			double diff=commissionAssignTargetSlabs.getToValue()-commissionAssignTargetSlabs.getFromValue();
					    			incentiveAmount=0;
						    		if(commissionAssignTargetSlabs.getCommissionType().equals("value")){
						    			incentiveAmount=(commissionAssignTargetSlabs.getTargetValue()/diff)*completeValue;
						    		}else if(commissionAssignTargetSlabs.getCommissionType().equals("percentage")){
						    			incentiveAmount=(completeValue*commissionAssignTargetSlabs.getTargetValue())/100;
						    			incentiveAmount=MathUtils.round(incentiveAmount, 2);
						    		}
						    		
							    	EmployeeIncentives employeeIncentives=new EmployeeIncentives();
							    	employeeIncentives.setEmployeeDetails(targetAssign.getEmployeeDetails());
									employeeIncentives.setIncentiveAmount(incentiveAmount);
									employeeIncentives.setReason(targetAssignTarget.getCommissionAssign().getTargetPeriod()+" Target Incentive Added ");
									employeeIncentives.setIncentiveGivenDate(new Date());
									employeeIncentives.setStatus(false);
									employeeDetailsDAO.giveIncentives(employeeIncentives);
									
					    		}else{
					    			incentiveAmount=0;
					    			List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList=commissionAssignDAO.fetchCommissionAssignTargetSlabs(targetAssignTarget.getCommissionAssign().getCommissionAssignId());
					    			for(CommissionAssignTargetSlabs slab : commissionAssignTargetSlabsList){
					    				double diff=(commissionAssignTargetSlabs.getToValue()+1)-commissionAssignTargetSlabs.getFromValue();
					    				if(completeValue>=slab.getFromValue()){
					    					if(slab.getCommissionType().equals("value")){
					    						if(completeValue>=slab.getFromValue() && completeValue<slab.getToValue()){
					    							incentiveAmount=+(slab.getTargetValue()/diff)*completeValue;
					    						}else{
					    							incentiveAmount=+slab.getTargetValue();
					    						}						    			
								    		}else if(slab.getCommissionType().equals("percentage")){
								    			incentiveAmount=+(completeValue*slab.getTargetValue())/100;
								    			incentiveAmount=MathUtils.round(incentiveAmount, 2);
								    		}	
					    				}else{
					    					break;
					    				}			
					    			}
					    			
					    			EmployeeIncentives employeeIncentives=new EmployeeIncentives();
							    	employeeIncentives.setEmployeeDetails(targetAssign.getEmployeeDetails());
									employeeIncentives.setIncentiveAmount(incentiveAmount);
									employeeIncentives.setReason(targetAssignTarget.getCommissionAssign().getTargetPeriod()+" Target Incentive Added ");
									employeeIncentives.setIncentiveGivenDate(new Date());
									employeeIncentives.setStatus(false);
									employeeDetailsDAO.giveIncentives(employeeIncentives);
									
					    		}			    		
					    	}
			    		//}
			    		
					    	com.bluesquare.rc.entities.EmployeeCommissionDetails employeeCommissionDetails=getEmployeeCommissionDetailsList(employeeCommissionDetailsList, targetAssignTarget.getCommissionAssign().getCommissionAssignId());
					    	if(employeeCommissionDetails==null){
					    		employeeCommissionDetails=new com.bluesquare.rc.entities.EmployeeCommissionDetails();
					    		employeeCommissionDetails.setCommissionAssign(targetAssignTarget.getCommissionAssign());
					    		employeeCommissionDetails.setCommissionValue(incentiveAmount);
					    		employeeCommissionDetails.setCompletedValue(completeValue);
					    		employeeCommissionDetailsNewList.add(employeeCommissionDetails);
					    	}else{
					    		employeeCommissionDetails.setCommissionValue(employeeCommissionDetails.getCommissionValue()+incentiveAmount);
					    		employeeCommissionDetails.setCompletedValue(employeeCommissionDetails.getCompletedValue()+completeValue);
					    		employeeCommissionDetails=(com.bluesquare.rc.entities.EmployeeCommissionDetails)sessionFactory.getCurrentSession().merge(employeeCommissionDetails);
					    		sessionFactory.getCurrentSession().update(employeeCommissionDetails);
					    	}
					    	totalAmount+=incentiveAmount;
			    	}
			    	
			    	// Incentive Adding Done
			    }
			    
			    employeeCommission.setTotalAmount(employeeCommission.getTotalAmount()+totalAmount);
			    if(employeeCommission.getEmployeeCommissionId()==0){
			    	sessionFactory.getCurrentSession().save(employeeCommission);
			    }
			    
			    for(com.bluesquare.rc.entities.EmployeeCommissionDetails employeeCommissionDetails : employeeCommissionDetailsNewList){
			    	employeeCommissionDetails.setEmployeeCommission(employeeCommission);
			    	sessionFactory.getCurrentSession().save(employeeCommissionDetails);
			    }
		   }
	}
	
	public com.bluesquare.rc.entities.EmployeeCommissionDetails getEmployeeCommissionDetailsList(List<com.bluesquare.rc.entities.EmployeeCommissionDetails> employeeCommissionDetailsList,long commissionAssignId) {
		for(com.bluesquare.rc.entities.EmployeeCommissionDetails  employeeCommissionDetails : employeeCommissionDetailsList){
			if(commissionAssignId == employeeCommissionDetails.getCommissionAssign().getCommissionAssignId()){
				return employeeCommissionDetails;
			}
		}
		return null;
	}
	
	/**
	 * <pre>
	 * get target period by employeeId
	 * @param employeeDetailsId
	 * @return msg if found
	 * </pre>
	 */
	@Transactional
	public String getTargetPeriodByEmployee(long employeeId){
		
		/*String hql="from TargetAssignRegular where status=false and employeeDetails.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TargetAssignRegular> targetAssignRegularList=(List<TargetAssignRegular>)query.list();
		if(targetAssignRegularList.isEmpty()){
			return "";
		}*/
		
		return "";//targetAssignRegularList.get(0).getTargetPeriod();
	}
	
	/*@Transactional
	public List<TargetAssignTargets> fetchCommissionAssignListByEmpId(long employeeId){
		

		List<CommissionAssign> commissionAssignList=new ArrayList<>();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	
		String targetPeriod="";//getTargetPeriodByEmployee(employeeId);
		
		String hql="from TargetAssignTargets where targetAssign.employeeDetails.employee.employeeId="+employeeId;
		
		if(targetPeriod.equals(Constants.TARGET_PERIOD_DAILY)){
			hql+=" and date(targetAssign.fromDatetime)='"+dateFormat.format(new Date())+"' and date(targetAssign.toDatetime)='"+dateFormat.format(new Date())+"'";
		}else if(targetPeriod.equals(Constants.TARGET_PERIOD_WEEKLY)){
			hql+=" and date(targetAssign.fromDatetime)='"+DateUtils.getCurrentWeekStartDate()+"' and date(targetAssign.toDatetime)='"+DateUtils.getCurrentWeekEndDate()+"'";
		}else if(targetPeriod.equals(Constants.TARGET_PERIOD_MONTHLY)){
			hql+=" and date(targetAssign.fromDatetime)='"+DateUtils.getCurrentMonthStartDate()+"' and date(targetAssign.toDatetime)='"+DateUtils.getCurrentMonthLastDate()+"'";
		}else if(targetPeriod.equals(Constants.TARGET_PERIOD_YEARLY)){
			hql+=" and date(targetAssign.fromDatetime)='"+DateUtils.getCurrentYearStartDate()+"' and date(targetAssign.toDatetime)='"+DateUtils.getCurrentYearLastDate()+"'";
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<TargetAssignTargets> targetAssignTargetList=(List<TargetAssignTargets>)query.list();
		
		if (targetAssignTargetList.isEmpty()) {
			return null;
			
		}
		
		
		return targetAssignTargetList;
	}*/
	
	
	@Transactional
	public List<CommissionAssignTargetSlabs> fetchCommissionAssignTargetSlabsList(long commissionId){
		List<CommissionAssign> commissionAssignList=new ArrayList<>();
		
		
		String hql="from CommissionAssignTargetSlabs where commissionAssign.commissionAssignId="+commissionId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabList=(List<CommissionAssignTargetSlabs>)query.list();
		
		if(commissionAssignTargetSlabList.isEmpty()){
			return null;
			
		}
		return commissionAssignTargetSlabList;

	}
}
