package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.models.BusinessBadDebts;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReportView;
import com.bluesquare.rc.responseEntities.BusinessNameAndId;

public interface BusinessNameDAO {

	public void saveBusinessName(BusinessName businessName);

	public List<BusinessName> businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId);

	// webapp
	public void saveForWebApp(BusinessName businessName);

	public void updateForWebApp(BusinessName businessName);

	public List<BusinessName> getBusinessNameListForWebApp();

	public List<BusinessNameAndId> getBusinessNameAndIdList();

	public BusinessName fetchBusinessForWebApp(String businessNameId);

	public List<BusinessNameList> fetchBusinessNameList();

	public String sendSMSTOShops(String shopsId, String smsText, String mobileNumber);

	public List<BusinessName> fetchBusinessNameByAreaId(long areaId);

	public List<BusinessName> fetchBusinessNameByAreaIds(String areaIds);

	public CustomerReportView fetchBusinessNameForReport(String range, String startDate, String endDate);

	public String checkBusinessDuplication(String checkText, String type, String businessNameId);

	public List<BusinessName> fetchBusinessNameListByEmployeeId(long employeeId);

	public int findNumberOfBusinessAddedByDateRange(String startDate, String endDate, long employeeId);

	public List<BusinessName> getBusinessNameListForApp();

	public List<BusinessBadDebts> fetchBusinessBadDebtsList(String year);

	public void setBusinessAsBadDebts(String businessNameId);
}
