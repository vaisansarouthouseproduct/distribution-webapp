package com.bluesquare.rc.dao;

import com.bluesquare.rc.rest.models.DBSnapshotResponse;
import com.bluesquare.rc.rest.models.SnapShotReportResponse;

public interface SnapShotDAO {

	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId,String fromDate,String toDate,String range);
	public DBSnapshotResponse fetchDBSnapshot(long employeeId,String pickDate);
	
	
}
