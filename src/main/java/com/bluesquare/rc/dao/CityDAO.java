package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.State;

public interface CityDAO extends DAO {

	
	//webApp
		public List<City> fetchAllCityForWebApp();
		
		public void saveForWebApp(City city);

		public void updateForWebApp(City city);
		
		public City fetchCityForWebApp(long cityId);
		
		public List<City> fetchCityByStateIdForWebApp(long stateId);
		
}
