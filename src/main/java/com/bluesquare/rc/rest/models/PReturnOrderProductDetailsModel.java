package com.bluesquare.rc.rest.models;

public class PReturnOrderProductDetailsModel {

	private long srno;
	private String productName;
	private long issuedQty;
	private long returnQty;
	private double totalAmtWithTax;
	private String type;
	private String reason;

	public PReturnOrderProductDetailsModel(long srno, String productName, long issuedQty, long returnQty,
			double totalAmtWithTax, String type, String reason) {
		super();
		this.srno = srno;
		this.productName = productName;
		this.issuedQty = issuedQty;
		this.returnQty = returnQty;
		this.totalAmtWithTax = totalAmtWithTax;
		this.type = type;
		this.reason = reason;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getIssuedQty() {
		return issuedQty;
	}

	public void setIssuedQty(long issuedQty) {
		this.issuedQty = issuedQty;
	}

	public long getReturnQty() {
		return returnQty;
	}

	public void setReturnQty(long returnQty) {
		this.returnQty = returnQty;
	}

	public double getTotalAmtWithTax() {
		return totalAmtWithTax;
	}

	public void setTotalAmtWithTax(double totalAmtWithTax) {
		this.totalAmtWithTax = totalAmtWithTax;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "PReturnOrderProductDetailsModel [srno=" + srno + ", productName=" + productName + ", issuedQty="
				+ issuedQty + ", returnQty=" + returnQty + ", totalAmtWithTax=" + totalAmtWithTax + ", type=" + type
				+ ", reason=" + reason + "]";
	}

}
