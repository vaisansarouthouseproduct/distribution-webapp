package com.bluesquare.rc.rest.models;

public class SupplierOrderProductDetails {

	private String productName;
	private String brandName;
	private String categoryName;
	private String hsnCode;
	private float igst;
	private float igstRate;
	private float cgst;
	private float cgstRate;
	private float sgst;
	private float sgstRate;
	private double totalAomount;
	private double totalAomountWithTax;
	
	public SupplierOrderProductDetails(String productName, String brandName, String categoryName, String hsnCode,
			float igst, float igstRate, float cgst, float cgstRate, float sgst, float sgstRate, double totalAomount,
			double totalAomountWithTax) {
		super();
		this.productName = productName;
		this.brandName = brandName;
		this.categoryName = categoryName;
		this.hsnCode = hsnCode;
		this.igst = igst;
		this.igstRate = igstRate;
		this.cgst = cgst;
		this.cgstRate = cgstRate;
		this.sgst = sgst;
		this.sgstRate = sgstRate;
		this.totalAomount = totalAomount;
		this.totalAomountWithTax = totalAomountWithTax;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public float getIgst() {
		return igst;
	}
	public void setIgst(float igst) {
		this.igst = igst;
	}
	public float getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(float igstRate) {
		this.igstRate = igstRate;
	}
	public float getCgst() {
		return cgst;
	}
	public void setCgst(float cgst) {
		this.cgst = cgst;
	}
	public float getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(float cgstRate) {
		this.cgstRate = cgstRate;
	}
	public float getSgst() {
		return sgst;
	}
	public void setSgst(float sgst) {
		this.sgst = sgst;
	}
	public float getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(float sgstRate) {
		this.sgstRate = sgstRate;
	}
	public double getTotalAomount() {
		return totalAomount;
	}
	public void setTotalAomount(double totalAomount) {
		this.totalAomount = totalAomount;
	}
	public double getTotalAomountWithTax() {
		return totalAomountWithTax;
	}
	public void setTotalAomountWithTax(double totalAomountWithTax) {
		this.totalAomountWithTax = totalAomountWithTax;
	}
	@Override
	public String toString() {
		return "SupplierOrderProductDetails [productName=" + productName + ", brandName=" + brandName
				+ ", categoryName=" + categoryName + ", hsnCode=" + hsnCode + ", igst=" + igst + ", igstRate="
				+ igstRate + ", cgst=" + cgst + ", cgstRate=" + cgstRate + ", sgst=" + sgst + ", sgstRate=" + sgstRate
				+ ", totalAomount=" + totalAomount + ", totalAomountWithTax=" + totalAomountWithTax + "]";
	}
	
	
	
	
	
}
