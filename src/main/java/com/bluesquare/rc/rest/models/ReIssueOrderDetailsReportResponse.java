package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;


public class ReIssueOrderDetailsReportResponse extends BaseDomain{
	
	private List<ReIssueOrderDetailsReport> reIssueOrderDetailsList;//reIssueOrderDetailsReportsList;

	public List<ReIssueOrderDetailsReport> getReIssueOrderDetailsList() {
		return reIssueOrderDetailsList;
	}

	public void setReIssueOrderDetailsList(List<ReIssueOrderDetailsReport> reIssueOrderDetailsList) {
		this.reIssueOrderDetailsList = reIssueOrderDetailsList;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsReportResponse [reIssueOrderDetailsList=" + reIssueOrderDetailsList + "]";
	}
	
	//private List<ReIssueOrderDetails> reIssueOrderDetailsList;

	

	/*public List<ReIssueOrderDetails> getReIssueOrderDetailsList() {
		return reIssueOrderDetailsList;
	}

	public void setReIssueOrderDetailsList(List<ReIssueOrderDetails> reIssueOrderDetailsList) {
		this.reIssueOrderDetailsList = reIssueOrderDetailsList;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsReportResponse [reIssueOrderDetailsReportsList=" + reIssueOrderDetailsReportsList
				+ ", reIssueOrderDetailsList=" + reIssueOrderDetailsList + "]";
	}*/


}
