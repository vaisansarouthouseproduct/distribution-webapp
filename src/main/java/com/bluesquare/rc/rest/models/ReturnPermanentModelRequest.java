package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;


public class ReturnPermanentModelRequest {

	public ReturnOrderProductP returnOrderProductP;
	public List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList;
	public String returnOrderProductId;

	public ReturnOrderProductP getReturnOrderProductP() {
		return returnOrderProductP;
	}

	public void setReturnOrderProductP(ReturnOrderProductP returnOrderProductP) {
		this.returnOrderProductP = returnOrderProductP;
	}

	public List<ReturnOrderProductDetailsP> getReturnOrderProductDetailsPList() {
		return returnOrderProductDetailsPList;
	}

	public void setReturnOrderProductDetailsPList(List<ReturnOrderProductDetailsP> returnOrderProductDetailsPList) {
		this.returnOrderProductDetailsPList = returnOrderProductDetailsPList;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	@Override
	public String toString() {
		return "ReturnPermanentModelRequest [returnOrderProductP=" + returnOrderProductP
				+ ", returnOrderProductDetailsPList=" + returnOrderProductDetailsPList + ", returnOrderProductId="
				+ returnOrderProductId + "]";
	}

}
