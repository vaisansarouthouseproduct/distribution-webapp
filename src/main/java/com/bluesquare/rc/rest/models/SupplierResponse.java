package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Supplier;

public class SupplierResponse extends BaseDomain{
	
	List<Supplier> supplier;

	

	public List<Supplier> getSupplier() {
		return supplier;
	}



	public void setSupplier(List<Supplier> supplier) {
		this.supplier = supplier;
	}



	@Override
	public String toString() {
		return "SupplierResponse [supplier=" + supplier + "]";
	}
	
	

}
