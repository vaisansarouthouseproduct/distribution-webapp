package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.ReturnOrderProductModel;

public class ReturnOrderDateRangeResponse extends BaseDomain {

	public List<AreaModel> areaList;

	public List<ReturnOrderProductModel> returnOrderProductModelList; // for
																		// panel

	public List<AreaModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}

	public List<ReturnOrderProductModel> getReturnOrderProductModelList() {
		return returnOrderProductModelList;
	}

	public void setReturnOrderProductModelList(List<ReturnOrderProductModel> returnOrderProductModelList) {
		this.returnOrderProductModelList = returnOrderProductModelList;
	}

	@Override
	public String toString() {
		return "ReturnOrderDateRangeResponse [areaList=" + areaList + ", returnOrderProductModelList="
				+ returnOrderProductModelList + "]";
	}

	


}
