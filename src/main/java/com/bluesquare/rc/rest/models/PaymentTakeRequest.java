package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.OfflineAPIRequest;
import com.bluesquare.rc.entities.PaymentMethod;

public class PaymentTakeRequest extends OfflineAPIRequest {

	private long paymentId;
	private String orderId;
	private String cashCheckStatus;
	private String fullPartialPayment;
	private long dueDate;
	private String bankName;
	private String checkNo;
	private long checkDate;
	private double paidAmount;

	private PaymentMethod paymentMethod;
	private String comment;
	private String transactionReferenceNumber;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCashCheckStatus() {
		return cashCheckStatus;
	}

	public void setCashCheckStatus(String cashCheckStatus) {
		this.cashCheckStatus = cashCheckStatus;
	}

	public String getFullPartialPayment() {
		return fullPartialPayment;
	}

	public void setFullPartialPayment(String fullPartialPayment) {
		this.fullPartialPayment = fullPartialPayment;
	}

	public long getDueDate() {
		return dueDate;
	}

	public void setDueDate(long dueDate) {
		this.dueDate = dueDate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}

	public long getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(long checkDate) {
		this.checkDate = checkDate;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	@Override
	public String toString() {
		return "PaymentTakeRequest [paymentId=" + paymentId + ", orderId=" + orderId + ", cashCheckStatus="
				+ cashCheckStatus + ", fullPartialPayment=" + fullPartialPayment + ", dueDate=" + dueDate
				+ ", bankName=" + bankName + ", checkNo=" + checkNo + ", checkDate=" + checkDate + ", paidAmount="
				+ paidAmount + ", paymentMethod=" + paymentMethod + ", comment=" + comment
				+ ", transactionReferenceNumber=" + transactionReferenceNumber + "]";
	}

}
