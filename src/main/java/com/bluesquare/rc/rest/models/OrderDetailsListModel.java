package com.bluesquare.rc.rest.models;

import java.util.List;

public class OrderDetailsListModel extends BaseDomain {
	private List<OrderDetailsModel> orderDetailsList;
	private String salesManName;
	private String shopName;
	private String mobileNumber;
	private String areaName;

	public List<OrderDetailsModel> getOrderDetailsList() {
		return orderDetailsList;
	}

	public void setOrderDetailsList(List<OrderDetailsModel> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}

	public String getSalesManName() {
		return salesManName;
	}

	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Override
	public String toString() {
		return "CustomerReportDetails [orderDetailsList=" + orderDetailsList + ", salesManName=" + salesManName
				+ ", shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", areaName=" + areaName + "]";
	}

}
