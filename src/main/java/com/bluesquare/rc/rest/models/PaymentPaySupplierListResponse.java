package com.bluesquare.rc.rest.models;

import java.util.List;

public class PaymentPaySupplierListResponse {

	private String inventoryTransactionId;
	private String supplierName;
	private double totalAmountTax;
	private List<PaymentPaySupplierModule> paymentPaySupplierModuleList;

	public PaymentPaySupplierListResponse(String inventoryTransactionId, String supplierName, double totalAmountTax,
			List<PaymentPaySupplierModule> paymentPaySupplierModuleList) {
		super();
		this.inventoryTransactionId = inventoryTransactionId;
		this.supplierName = supplierName;
		this.totalAmountTax = totalAmountTax;
		this.paymentPaySupplierModuleList = paymentPaySupplierModuleList;
	}

	public String getInventoryTransactionId() {
		return inventoryTransactionId;
	}

	public void setInventoryTransactionId(String inventoryTransactionId) {
		this.inventoryTransactionId = inventoryTransactionId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public double getTotalAmountTax() {
		return totalAmountTax;
	}

	public void setTotalAmountTax(double totalAmountTax) {
		this.totalAmountTax = totalAmountTax;
	}

	public List<PaymentPaySupplierModule> getPaymentPaySupplierModuleList() {
		return paymentPaySupplierModuleList;
	}

	public void setPaymentPaySupplierModuleList(List<PaymentPaySupplierModule> paymentPaySupplierModuleList) {
		this.paymentPaySupplierModuleList = paymentPaySupplierModuleList;
	}

	@Override
	public String toString() {
		return "PaymentPaySupplierListResponse [inventoryTransactionId=" + inventoryTransactionId + ", supplierName="
				+ supplierName + ", totalAmoutTax=" + totalAmountTax + ", paymentPaySupplierModuleList="
				+ paymentPaySupplierModuleList + "]";
	}

}
