/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.Date;
import java.util.List;

import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.responseEntities.ReturnFromDeliveryBoyModel;

/**
 * @author aNKIT
 *
 */
public class ReturnOrderFromDeliveryBoyReportModel extends BaseDomain {

	private String returnOrderFromDeliveryBoyMainId;
	private String employeeName;
	private String orderId;
	private String shopName;
	private String mobileNumber;
	private Date orderDate;
	private String areaName;
	private long totalIssueQuantity;
	private long totalDamageQuantity;
	private long totalDeliveredQuantity;

	private List<ReturnOrderFromDeliveryBoyReport> returnOrderFromDeliveryBoyReportList;

	private List<ReturnFromDeliveryBoyModel> returnFromDeliveryBoyList;

	public String getReturnOrderFromDeliveryBoyMainId() {
		return returnOrderFromDeliveryBoyMainId;
	}

	public void setReturnOrderFromDeliveryBoyMainId(String returnOrderFromDeliveryBoyMainId) {
		this.returnOrderFromDeliveryBoyMainId = returnOrderFromDeliveryBoyMainId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getTotalIssueQuantity() {
		return totalIssueQuantity;
	}

	public void setTotalIssueQuantity(long totalIssueQuantity) {
		this.totalIssueQuantity = totalIssueQuantity;
	}

	public long getTotalDamageQuantity() {
		return totalDamageQuantity;
	}

	public void setTotalDamageQuantity(long totalDamageQuantity) {
		this.totalDamageQuantity = totalDamageQuantity;
	}

	public long getTotalDeliveredQuantity() {
		return totalDeliveredQuantity;
	}

	public void setTotalDeliveredQuantity(long totalDeliveredQuantity) {
		this.totalDeliveredQuantity = totalDeliveredQuantity;
	}

	public List<ReturnOrderFromDeliveryBoyReport> getReturnOrderFromDeliveryBoyReportList() {
		return returnOrderFromDeliveryBoyReportList;
	}

	public void setReturnOrderFromDeliveryBoyReportList(
			List<ReturnOrderFromDeliveryBoyReport> returnOrderFromDeliveryBoyReportList) {
		this.returnOrderFromDeliveryBoyReportList = returnOrderFromDeliveryBoyReportList;
	}

	public List<ReturnFromDeliveryBoyModel> getReturnFromDeliveryBoyList() {
		return returnFromDeliveryBoyList;
	}

	public void setReturnFromDeliveryBoyList(List<ReturnFromDeliveryBoyModel> returnFromDeliveryBoyList) {
		this.returnFromDeliveryBoyList = returnFromDeliveryBoyList;
	}

	@Override
	public String toString() {
		return "ReturnOrderFromDeliveryBoyReportModel [returnOrderFromDeliveryBoyMainId="
				+ returnOrderFromDeliveryBoyMainId + ", employeeName=" + employeeName + ", orderId=" + orderId
				+ ", shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", orderDate=" + orderDate
				+ ", areaName=" + areaName + ", totalIssueQuantity=" + totalIssueQuantity + ", totalDamageQuantity="
				+ totalDamageQuantity + ", totalDeliveredQuantity=" + totalDeliveredQuantity
				+ ", returnOrderFromDeliveryBoyReportList=" + returnOrderFromDeliveryBoyReportList
				+ ", returnFromDeliveryBoyList=" + returnFromDeliveryBoyList + "]";
	}

}
