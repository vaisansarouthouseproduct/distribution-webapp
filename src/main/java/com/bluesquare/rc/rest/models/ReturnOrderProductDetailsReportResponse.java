package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.responseEntities.ReturnOrderProductDetailsReport;

public class ReturnOrderProductDetailsReportResponse extends BaseDomain {

	private ReturnOrderProduct returnOrderProduct;
	private List<ReturnOrderProductDetails> returnOrderProductDetailsList;

	private FetchOrderDetailsModel fetchOrderDetailsModel;
	private List<ReturnOrderProductDetailsReport> returnOrderProductDetailsReportList;

	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}

	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}

	public List<ReturnOrderProductDetails> getReturnOrderProductDetailsList() {
		return returnOrderProductDetailsList;
	}

	public void setReturnOrderProductDetailsList(List<ReturnOrderProductDetails> returnOrderProductDetailsList) {
		this.returnOrderProductDetailsList = returnOrderProductDetailsList;
	}

	public FetchOrderDetailsModel getFetchOrderDetailsModel() {
		return fetchOrderDetailsModel;
	}

	public void setFetchOrderDetailsModel(FetchOrderDetailsModel fetchOrderDetailsModel) {
		this.fetchOrderDetailsModel = fetchOrderDetailsModel;
	}

	public List<ReturnOrderProductDetailsReport> getReturnOrderProductDetailsReportList() {
		return returnOrderProductDetailsReportList;
	}

	public void setReturnOrderProductDetailsReportList(
			List<ReturnOrderProductDetailsReport> returnOrderProductDetailsReportList) {
		this.returnOrderProductDetailsReportList = returnOrderProductDetailsReportList;
	}

	@Override
	public String toString() {
		return "ReturnOrderProductDetailsReportResponse [returnOrderProduct=" + returnOrderProduct
				+ ", returnOrderProductDetailsList=" + returnOrderProductDetailsList + ", fetchOrderDetailsModel="
				+ fetchOrderDetailsModel + ", returnOrderProductDetailsReportList="
				+ returnOrderProductDetailsReportList + "]";
	}

}
