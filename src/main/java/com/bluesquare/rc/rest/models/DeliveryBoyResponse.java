package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;

public class DeliveryBoyResponse extends BaseDomain {
    
    private List<EmployeeDetails> deliveryBoyList;

    public List<EmployeeDetails> getDeliveryBoyList() {
        return deliveryBoyList;
    }

    public void setDeliveryBoyList(List<EmployeeDetails> deliveryBoyList) {
        this.deliveryBoyList = deliveryBoyList;
    }

    @Override
    public String toString() {
        return "DeliveryBoyResponse [deliveryBoyList=" + deliveryBoyList + "]";
    }
    
    
    

}