/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.rest.models.BaseDomain;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderDetailsListModel extends BaseDomain
{

	private List<ReIssueOrderDetailsReport> reIssueOrderDetailsList;//reIssueOrderDetailsReportsList;

	public List<ReIssueOrderDetailsReport> getReIssueOrderDetailsList() {
		return reIssueOrderDetailsList;
	}

	public void setReIssueOrderDetailsList(List<ReIssueOrderDetailsReport> reIssueOrderDetailsList) {
		this.reIssueOrderDetailsList = reIssueOrderDetailsList;
	}
	
	
}
