package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.AreaModel;

public class AreaListResponse extends BaseDomain {

	private List<AreaModel> areaList;

	public List<AreaModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "AreaListResponse [areaList=" + areaList + "]";
	}
	
	
	
}
