package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.SupplierModel;

public class OpenInventoryResponseModel extends BaseDomain {

	private List<SupplierModel> supplier;
	private List<BrandModel> brand;
	private List<CategoriesModel> category;

	public List<SupplierModel> getSupplier() {
		return supplier;
	}

	public void setSupplier(List<SupplierModel> supplier) {
		this.supplier = supplier;
	}

	public List<BrandModel> getBrand() {
		return brand;
	}

	public void setBrand(List<BrandModel> brand) {
		this.brand = brand;
	}

	public List<CategoriesModel> getCategory() {
		return category;
	}

	public void setCategory(List<CategoriesModel> category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "OpenInventoryResponseModel [supplier=" + supplier + ", brand=" + brand + ", category=" + category + "]";
	}

}
