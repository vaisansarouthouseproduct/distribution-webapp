package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.responseEntities.MeetingModel;

public class MeetingListResponse extends BaseDomain {
	
	public List<MeetingModel> meetingList;
	public MeetingModel meeting;
	public List<MeetingModel> getMeetingList() {
		return meetingList;
	}
	public void setMeetingList(List<MeetingModel> meetingList) {
		this.meetingList = meetingList;
	}
	public MeetingModel getMeeting() {
		return meeting;
	}
	public void setMeeting(MeetingModel meeting) {
		this.meeting = meeting;
	}
	@Override
	public String toString() {
		return "MeetingListResponse [meetingList=" + meetingList + ", meeting=" + meeting + "]";
	}

	

}
