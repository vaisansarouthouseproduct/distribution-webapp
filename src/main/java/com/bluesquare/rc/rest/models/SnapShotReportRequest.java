package com.bluesquare.rc.rest.models;

public class SnapShotReportRequest {
	
	private long employeeId;
	private String fromDate;
	private String toDate;
	private String range;
	
	
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	@Override
	public String toString() {
		return "SnapShotReportRequest [employeeId=" + employeeId + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", range=" + range + "]";
	}
	
	

}
