package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.responseEntities.CityModel;

public class CityListResponse extends BaseDomain{

	private List<CityModel> cityList;

	public List<CityModel> getCityList() {
		return cityList;
	}

	public void setCityList(List<CityModel> cityList) {
		this.cityList = cityList;
	}

	@Override
	public String toString() {
		return "CityListResponse [cityList=" + cityList + "]";
	}
	
	
	
}
