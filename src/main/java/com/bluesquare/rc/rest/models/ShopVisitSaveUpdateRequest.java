package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.ShopVisit;

public class ShopVisitSaveUpdateRequest {

	public ShopVisit shopVisit;
	public String shopVisitImage;

	public ShopVisit getShopVisit() {
		return shopVisit;
	}

	public void setShopVisit(ShopVisit shopVisit) {
		this.shopVisit = shopVisit;
	}

	public String getShopVisitImage() {
		return shopVisitImage;
	}

	public void setShopVisitImage(String shopVisitImage) {
		this.shopVisitImage = shopVisitImage;
	}

	@Override
	public String toString() {
		return "ShopVisitSaveUpdateRequest [shopVisit=" + shopVisit + ", shopVisitImage=" + shopVisitImage + "]";
	}

}
