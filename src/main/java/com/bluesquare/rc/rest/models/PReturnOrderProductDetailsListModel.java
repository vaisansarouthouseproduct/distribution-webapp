package com.bluesquare.rc.rest.models;

import java.util.List;

public class PReturnOrderProductDetailsListModel extends BaseDomain{
	private List<PReturnOrderDetailsModel> pReturnOrderDetailsModelList;
	private PReturnOrderDetailsModel returnOrderDetailsModel;
	private List<PReturnOrderProductDetailsModel> returnOrderProductDetailsModelList;

	public List<PReturnOrderDetailsModel> getpReturnOrderDetailsModelList() {
		return pReturnOrderDetailsModelList;
	}

	public void setpReturnOrderDetailsModelList(List<PReturnOrderDetailsModel> pReturnOrderDetailsModelList) {
		this.pReturnOrderDetailsModelList = pReturnOrderDetailsModelList;
	}

	public PReturnOrderDetailsModel getReturnOrderDetailsModel() {
		return returnOrderDetailsModel;
	}

	public void setReturnOrderDetailsModel(PReturnOrderDetailsModel returnOrderDetailsModel) {
		this.returnOrderDetailsModel = returnOrderDetailsModel;
	}

	public List<PReturnOrderProductDetailsModel> getReturnOrderProductDetailsModelList() {
		return returnOrderProductDetailsModelList;
	}

	public void setReturnOrderProductDetailsModelList(
			List<PReturnOrderProductDetailsModel> returnOrderProductDetailsModelList) {
		this.returnOrderProductDetailsModelList = returnOrderProductDetailsModelList;
	}

	@Override
	public String toString() {
		return "PReturnOrderProductDetailsListModel [pReturnOrderDetailsModelList=" + pReturnOrderDetailsModelList
				+ ", returnOrderDetailsModel=" + returnOrderDetailsModel + ", returnOrderProductDetailsModelList="
				+ returnOrderProductDetailsModelList + "]";
	}

}
