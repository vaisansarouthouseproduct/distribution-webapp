package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.TransportationModel;

public class TransportationResponse extends BaseDomain {

	public List<TransportationModel> transportations;

	public List<TransportationModel> getTransportations() {
		return transportations;
	}

	public void setTransportations(List<TransportationModel> transportations) {
		this.transportations = transportations;
	}

	@Override
	public String toString() {
		return "TransportationResponse [transportations=" + transportations + "]";
	}
}
