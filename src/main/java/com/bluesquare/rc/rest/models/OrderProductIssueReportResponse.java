package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.responseEntities.AreaModel;

public class OrderProductIssueReportResponse extends BaseDomain {

	private List<OrderProductIssueDetails> orderProductIssueDetailsList;
	private List<FetchOrderDetailsModel> fetchOrderDetailsModelList;
	private List<AreaModel> areaList;
	private long deliveredCount;
	private long deliveredPendingCount;

	public List<OrderProductIssueDetails> getOrderProductIssueDetailsList() {
		return orderProductIssueDetailsList;
	}

	public void setOrderProductIssueDetailsList(List<OrderProductIssueDetails> orderProductIssueDetailsList) {
		this.orderProductIssueDetailsList = orderProductIssueDetailsList;
	}

	public List<AreaModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}

	public long getDeliveredCount() {
		return deliveredCount;
	}

	public void setDeliveredCount(long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}

	public long getDeliveredPendingCount() {
		return deliveredPendingCount;
	}

	public void setDeliveredPendingCount(long deliveredPendingCount) {
		this.deliveredPendingCount = deliveredPendingCount;
	}

	public List<FetchOrderDetailsModel> getFetchOrderDetailsModelList() {
		return fetchOrderDetailsModelList;
	}

	public void setFetchOrderDetailsModelList(List<FetchOrderDetailsModel> fetchOrderDetailsModelList) {
		this.fetchOrderDetailsModelList = fetchOrderDetailsModelList;
	}

	@Override
	public String toString() {
		return "OrderProductIssueReportResponse [orderProductIssueDetailsList=" + orderProductIssueDetailsList
				+ ", fetchOrderDetailsModelList=" + fetchOrderDetailsModelList + ", areaList=" + areaList
				+ ", deliveredCount=" + deliveredCount + ", deliveredPendingCount=" + deliveredPendingCount + "]";
	}

}
