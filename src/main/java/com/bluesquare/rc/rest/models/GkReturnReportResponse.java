package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.ReturnOrderProduct;

public class GkReturnReportResponse extends BaseDomain {
	
	private List<ReturnOrderProduct> returnOrderProductList;
	private List<Area> areaId;
	
	
	public List<ReturnOrderProduct> getReturnOrderProductList() {
		return returnOrderProductList;
	}
	public void setReturnOrderProductList(List<ReturnOrderProduct> returnOrderProductList) {
		this.returnOrderProductList = returnOrderProductList;
	}
	public List<Area> getAreaId() {
		return areaId;
	}
	public void setAreaId(List<Area> areaId) {
		this.areaId = areaId;
	}
	@Override
	public String toString() {
		return "GkReturnReportResponse [returnOrderProductList=" + returnOrderProductList + ", areaId=" + areaId + "]";
	}

	
	
	
	

}
