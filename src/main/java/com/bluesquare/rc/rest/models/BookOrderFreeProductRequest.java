package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderProductDetails;

/**
 * Created by Rohan on 01-01-2018.
 */

public class BookOrderFreeProductRequest {

    private String orderId;
    private List<OrderProductDetails> orderProductDetailList;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderProductDetails> getOrderProductDetailList() {
        return orderProductDetailList;
    }

    public void setOrderProductDetailList(List<OrderProductDetails> orderProductDetailList) {
        this.orderProductDetailList = orderProductDetailList;
    }

    @Override
    public String toString() {
        return "BookOrderFreeProductRequest{" +
                "orderId='" + orderId + '\'' +
                ", orderProductDetailList=" + orderProductDetailList +
                '}';
    }
}

