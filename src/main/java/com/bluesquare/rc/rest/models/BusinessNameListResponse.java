package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.BusinessNameModel;

public class BusinessNameListResponse extends BaseDomain {

	private List<BusinessNameModel> businessNamesList;

	public List<BusinessNameModel> getBusinessNamesList() {
		return businessNamesList;
	}

	public void setBusinessNamesList(List<BusinessNameModel> businessNamesList) {
		this.businessNamesList = businessNamesList;
	}

	@Override
	public String toString() {
		return "BusinessNameListResponse [businessNamesList=" + businessNamesList + "]";
	}
	
	
	
	
}
