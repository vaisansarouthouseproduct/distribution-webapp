package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Employee;

public class LoginResponse extends BaseDomain {

	public Employee employee;
	private String employeeName;
	private String token;
	private List<Branch> branchList;
	private String userCode;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<Branch> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<Branch> branchList) {
		this.branchList = branchList;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Override
	public String toString() {
		return "LoginResponse [employee=" + employee + ", employeeName=" + employeeName + ", token=" + token
				+ ", branchList=" + branchList + ", userCode=" + userCode + "]";
	}

}
