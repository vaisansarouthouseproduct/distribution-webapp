/**
 * 
 */
package com.bluesquare.rc.rest.models;

/**
 * @author aNKIT
 *
 */
public class SalesManReportSub {

	private long srno;
	private long employeeSMId;
	private String employeeSMName;
	private long employeeDetailsSMId;
	private String employeeDetailsSMGenId;
	private double amountOfBusinessDid;
	private double amountOfBusinessWithTaxDid;
	private long orderCount;
	public SalesManReportSub(long srno, long employeeSMId, String employeeSMName, long employeeDetailsSMId,
			String employeeDetailsSMGenId, double amountOfBusinessDid, double amountOfBusinessWithTaxDid,
			long orderCount) {
		super();
		this.srno = srno;
		this.employeeSMId = employeeSMId;
		this.employeeSMName = employeeSMName;
		this.employeeDetailsSMId = employeeDetailsSMId;
		this.employeeDetailsSMGenId = employeeDetailsSMGenId;
		this.amountOfBusinessDid = amountOfBusinessDid;
		this.amountOfBusinessWithTaxDid = amountOfBusinessWithTaxDid;
		this.orderCount = orderCount;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getEmployeeSMId() {
		return employeeSMId;
	}
	public void setEmployeeSMId(long employeeSMId) {
		this.employeeSMId = employeeSMId;
	}
	public String getEmployeeSMName() {
		return employeeSMName;
	}
	public void setEmployeeSMName(String employeeSMName) {
		this.employeeSMName = employeeSMName;
	}
	public long getEmployeeDetailsSMId() {
		return employeeDetailsSMId;
	}
	public void setEmployeeDetailsSMId(long employeeDetailsSMId) {
		this.employeeDetailsSMId = employeeDetailsSMId;
	}
	public String getEmployeeDetailsSMGenId() {
		return employeeDetailsSMGenId;
	}
	public void setEmployeeDetailsSMGenId(String employeeDetailsSMGenId) {
		this.employeeDetailsSMGenId = employeeDetailsSMGenId;
	}
	public double getAmountOfBusinessDid() {
		return amountOfBusinessDid;
	}
	public void setAmountOfBusinessDid(double amountOfBusinessDid) {
		this.amountOfBusinessDid = amountOfBusinessDid;
	}
	public double getAmountOfBusinessWithTaxDid() {
		return amountOfBusinessWithTaxDid;
	}
	public void setAmountOfBusinessWithTaxDid(double amountOfBusinessWithTaxDid) {
		this.amountOfBusinessWithTaxDid = amountOfBusinessWithTaxDid;
	}
	public long getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(long orderCount) {
		this.orderCount = orderCount;
	}
	@Override
	public String toString() {
		return "SalesManReportSub [srno=" + srno + ", employeeSMId=" + employeeSMId + ", employeeSMName="
				+ employeeSMName + ", employeeDetailsSMId=" + employeeDetailsSMId + ", employeeDetailsSMGenId="
				+ employeeDetailsSMGenId + ", amountOfBusinessDid=" + amountOfBusinessDid
				+ ", amountOfBusinessWithTaxDid=" + amountOfBusinessWithTaxDid + ", orderCount=" + orderCount + "]";
	}


	
	
	
	
}
