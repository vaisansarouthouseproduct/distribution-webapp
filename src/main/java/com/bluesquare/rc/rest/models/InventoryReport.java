package com.bluesquare.rc.rest.models;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class InventoryReport {
	private long inventoryTransactionPkId;
	private String inventoryTransactionId;
	private String supplierName;
	private String supplierId;
	private double totalAmountTax;
	private Date addedDate;
	private long totalQuantity;
	private Date billDate;
	private String billNumber;
	private Date paymentDateTime;
	private double totalAmount;
	private double discountAmount;
	private double discountPercentage;
	private String discountType;
	private double totalAmountBeforeDiscount;
	private double totalAmountTaxBeforeDiscount;
	
	@JsonProperty
//	@JsonSerialize(using = NumericBooleanSerializer.class)
//	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	private boolean isDiscountGiven;
	
	private String discountOnMRP;
	

	public InventoryReport(long inventoryTransactionPkId, String inventoryTransactionId, String supplierName,
			String supplierId, double totalAmountTax, Date addedDate, long totalQuantity, Date billDate,
			String billNumber, Date paymentDateTime, double totalAmount, double discountAmount,
			double discountPercentage, String discountType, double totalAmountBeforeDiscount,
			double totalAmountTaxBeforeDiscount, boolean isDiscountGiven,String discountOnMRP) {
		super();
		this.inventoryTransactionPkId = inventoryTransactionPkId;
		this.inventoryTransactionId = inventoryTransactionId;
		this.supplierName = supplierName;
		this.supplierId = supplierId;
		this.totalAmountTax = totalAmountTax;
		this.addedDate = addedDate;
		this.totalQuantity = totalQuantity;
		this.billDate = billDate;
		this.billNumber = billNumber;
		this.paymentDateTime = paymentDateTime;
		this.totalAmount = totalAmount;
		this.discountAmount = discountAmount;
		this.discountPercentage = discountPercentage;
		this.discountType = discountType;
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
		this.totalAmountTaxBeforeDiscount = totalAmountTaxBeforeDiscount;
		this.isDiscountGiven = isDiscountGiven;
		this.discountOnMRP = discountOnMRP;
	}

	public long getInventoryTransactionPkId() {
		return inventoryTransactionPkId;
	}

	public void setInventoryTransactionPkId(long inventoryTransactionPkId) {
		this.inventoryTransactionPkId = inventoryTransactionPkId;
	}

	public String getInventoryTransactionId() {
		return inventoryTransactionId;
	}

	public void setInventoryTransactionId(String inventoryTransactionId) {
		this.inventoryTransactionId = inventoryTransactionId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public double getTotalAmountTax() {
		return totalAmountTax;
	}

	public void setTotalAmountTax(double totalAmountTax) {
		this.totalAmountTax = totalAmountTax;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}


	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getTotalAmountBeforeDiscount() {
		return totalAmountBeforeDiscount;
	}

	public void setTotalAmountBeforeDiscount(double totalAmountBeforeDiscount) {
		this.totalAmountBeforeDiscount = totalAmountBeforeDiscount;
	}

	public double getTotalAmountTaxBeforeDiscount() {
		return totalAmountTaxBeforeDiscount;
	}

	public void setTotalAmountTaxBeforeDiscount(double totalAmountTaxBeforeDiscount) {
		this.totalAmountTaxBeforeDiscount = totalAmountTaxBeforeDiscount;
	}	

	public boolean isDiscountGiven() {
		return isDiscountGiven;
	}

	public void setDiscountGiven(boolean isDiscountGiven) {
		this.isDiscountGiven = isDiscountGiven;
	}

	public Date getPaymentDateTime() {
		return paymentDateTime;
	}

	public void setPaymentDateTime(Date paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}

	
	
	public String getDiscountOnMRP() {
		return discountOnMRP;
	}

	public void setDiscountOnMRP(String discountOnMRP) {
		this.discountOnMRP = discountOnMRP;
	}

	


	@Override
	public String toString() {
		return "InventoryReport [inventoryTransactionPkId=" + inventoryTransactionPkId + ", inventoryTransactionId="
				+ inventoryTransactionId + ", supplierName=" + supplierName + ", supplierId=" + supplierId
				+ ", totalAmountTax=" + totalAmountTax + ", addedDate=" + addedDate + ", totalQuantity=" + totalQuantity
				+ ", billDate=" + billDate + ", billNumber=" + billNumber + ", paymentDateTime=" + paymentDateTime
				+ ", totalAmount=" + totalAmount + ", discountAmount=" + discountAmount + ", discountPercentage="
				+ discountPercentage + ", discountType=" + discountType + ", totalAmountBeforeDiscount="
				+ totalAmountBeforeDiscount + ", totalAmountTaxBeforeDiscount=" + totalAmountTaxBeforeDiscount
				+ ", isDiscountGiven=" + isDiscountGiven + ", discountOnMRP=" + discountOnMRP + "]";
	}




	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
}
