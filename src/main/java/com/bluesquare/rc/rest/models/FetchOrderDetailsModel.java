package com.bluesquare.rc.rest.models;

import java.util.Date;

public class FetchOrderDetailsModel {
	private String orderId;
	private String shopName;
	private String ownerName;
	private String areaName;
	private long areaId;
	private long purchasedQuantity;
	private long issuedQuantity;
	private Date orderDate;
	private Date packedDate;
	private Date issuedDate;
	private Date cancelDate;
	private Date confirmDate;
	private String orderStatus;
	private double totalAmount;
	private double totalAmountWithTax;
	private double issuedTotalAmount;
	private double issuedTotalAmountWithTax;
	private double confirmTotalAmount;
	private double confirmTotalAmountWithTax;
	private String cancelEmployeeDepartment;
	private String mobileNo;
	private String businessNameId;
	private String businessAddress;
	private long totalReturnQuantity;
	private long totalReIssueQuantity;
	private Date returnDate;
	private String returnOrderId;
	private double returnTotalAmountWithTax;
	private double reIssueTotalAmountWithTax;
	private String deliveryPersonName;
	private Date reIssueDeliveredDate;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public long getPurchasedQuantity() {
		return purchasedQuantity;
	}

	public void setPurchasedQuantity(long purchasedQuantity) {
		this.purchasedQuantity = purchasedQuantity;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getPackedDate() {
		return packedDate;
	}

	public void setPackedDate(Date packedDate) {
		this.packedDate = packedDate;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public double getIssuedTotalAmount() {
		return issuedTotalAmount;
	}

	public void setIssuedTotalAmount(double issuedTotalAmount) {
		this.issuedTotalAmount = issuedTotalAmount;
	}

	public double getIssuedTotalAmountWithTax() {
		return issuedTotalAmountWithTax;
	}

	public void setIssuedTotalAmountWithTax(double issuedTotalAmountWithTax) {
		this.issuedTotalAmountWithTax = issuedTotalAmountWithTax;
	}

	public double getConfirmTotalAmount() {
		return confirmTotalAmount;
	}

	public void setConfirmTotalAmount(double confirmTotalAmount) {
		this.confirmTotalAmount = confirmTotalAmount;
	}

	public double getConfirmTotalAmountWithTax() {
		return confirmTotalAmountWithTax;
	}

	public void setConfirmTotalAmountWithTax(double confirmTotalAmountWithTax) {
		this.confirmTotalAmountWithTax = confirmTotalAmountWithTax;
	}

	public String getCancelEmployeeDepartment() {
		return cancelEmployeeDepartment;
	}

	public void setCancelEmployeeDepartment(String cancelEmployeeDepartment) {
		this.cancelEmployeeDepartment = cancelEmployeeDepartment;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public long getTotalReturnQuantity() {
		return totalReturnQuantity;
	}

	public void setTotalReturnQuantity(long totalReturnQuantity) {
		this.totalReturnQuantity = totalReturnQuantity;
	}

	public String getReturnOrderId() {
		return returnOrderId;
	}

	public void setReturnOrderId(String returnOrderId) {
		this.returnOrderId = returnOrderId;
	}

	public double getReturnTotalAmountWithTax() {
		return returnTotalAmountWithTax;
	}

	public void setReturnTotalAmountWithTax(double returnTotalAmountWithTax) {
		this.returnTotalAmountWithTax = returnTotalAmountWithTax;
	}

	public String getDeliveryPersonName() {
		return deliveryPersonName;
	}

	public void setDeliveryPersonName(String deliveryPersonName) {
		this.deliveryPersonName = deliveryPersonName;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public long getTotalReIssueQuantity() {
		return totalReIssueQuantity;
	}

	public void setTotalReIssueQuantity(long totalReIssueQuantity) {
		this.totalReIssueQuantity = totalReIssueQuantity;
	}

	public double getReIssueTotalAmountWithTax() {
		return reIssueTotalAmountWithTax;
	}

	public void setReIssueTotalAmountWithTax(double reIssueTotalAmountWithTax) {
		this.reIssueTotalAmountWithTax = reIssueTotalAmountWithTax;
	}

	public Date getReIssueDeliveredDate() {
		return reIssueDeliveredDate;
	}

	public void setReIssueDeliveredDate(Date reIssueDeliveredDate) {
		this.reIssueDeliveredDate = reIssueDeliveredDate;
	}

	@Override
	public String toString() {
		return "FetchOrderDetailsModel [orderId=" + orderId + ", shopName=" + shopName + ", ownerName=" + ownerName
				+ ", areaName=" + areaName + ", areaId=" + areaId + ", purchasedQuantity=" + purchasedQuantity
				+ ", issuedQuantity=" + issuedQuantity + ", orderDate=" + orderDate + ", packedDate=" + packedDate
				+ ", issuedDate=" + issuedDate + ", cancelDate=" + cancelDate + ", confirmDate=" + confirmDate
				+ ", orderStatus=" + orderStatus + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", issuedTotalAmount=" + issuedTotalAmount + ", issuedTotalAmountWithTax="
				+ issuedTotalAmountWithTax + ", confirmTotalAmount=" + confirmTotalAmount
				+ ", confirmTotalAmountWithTax=" + confirmTotalAmountWithTax + ", cancelEmployeeDepartment="
				+ cancelEmployeeDepartment + ", mobileNo=" + mobileNo + ", businessNameId=" + businessNameId
				+ ", businessAddress=" + businessAddress + ", totalReturnQuantity=" + totalReturnQuantity
				+ ", totalReIssueQuantity=" + totalReIssueQuantity + ", returnDate=" + returnDate + ", returnOrderId="
				+ returnOrderId + ", returnTotalAmountWithTax=" + returnTotalAmountWithTax
				+ ", reIssueTotalAmountWithTax=" + reIssueTotalAmountWithTax + ", deliveryPersonName="
				+ deliveryPersonName + ", reIssueDeliveredDate=" + reIssueDeliveredDate + "]";
	}

}