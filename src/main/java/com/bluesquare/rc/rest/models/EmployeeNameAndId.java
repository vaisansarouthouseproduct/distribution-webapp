package com.bluesquare.rc.rest.models;

public class EmployeeNameAndId {
	private long employeeId;
	private String name;
	private String departmentName;
	private String departmentNameShort;
	private String userId;
	private String userCode;

	public EmployeeNameAndId(long employeeId, String name) {
		super();
		this.employeeId = employeeId;
		this.name = name;
	}

	public EmployeeNameAndId(long employeeId, String name, String departmentName, String departmentNameShort,
			String userId,String userCode) {
		super();
		this.employeeId = employeeId;
		this.name = name;
		this.departmentName = departmentName;
		this.departmentNameShort = departmentNameShort;
		this.userId = userId;
		this.userCode=userCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentNameShort() {
		return departmentNameShort;
	}

	public void setDepartmentNameShort(String departmentNameShort) {
		this.departmentNameShort = departmentNameShort;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Override
	public String toString() {
		return "EmployeeNameAndId [employeeId=" + employeeId + ", name=" + name + ", departmentName=" + departmentName
				+ ", departmentNameShort=" + departmentNameShort + ", userId=" + userId + ", userCode=" + userCode
				+ "]";
	}

}
