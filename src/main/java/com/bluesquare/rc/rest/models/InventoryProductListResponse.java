package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.Product;

public class InventoryProductListResponse extends BaseDomain {
	
	private List<Product> product;

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "InventoryProductListResponse [product=" + product + "]";
	}

	

}
