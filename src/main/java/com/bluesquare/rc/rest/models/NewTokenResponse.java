package com.bluesquare.rc.rest.models;

public class NewTokenResponse extends BaseDomain {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "NewTokenResponse [token=" + token + "]";
	}
	
	
}
