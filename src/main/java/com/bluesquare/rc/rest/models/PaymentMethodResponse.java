package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.PaymentMethod;


public class PaymentMethodResponse extends BaseDomain {

	private List<PaymentMethod> paymentMethodList;

	public List<PaymentMethod> getPaymentMethodList() {
		return paymentMethodList;
	}

	public void setPaymentMethodList(List<PaymentMethod> paymentMethodList) {
		this.paymentMethodList = paymentMethodList;
	}

	@Override
	public String toString() {
		return "PaymentMethodResponse [paymentMethodList=" + paymentMethodList + "]";
	}
	
	
}
