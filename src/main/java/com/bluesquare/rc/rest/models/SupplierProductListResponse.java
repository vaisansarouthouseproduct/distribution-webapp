package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.SupplierProductListModule;

public class SupplierProductListResponse {

	private String supplierName;
	private String mobileNumber;
	private String address;
	private String gstinNo;
	private List<SupplierProductListModule> supplierProductListModuleList;

	public SupplierProductListResponse(String supplierName, String mobileNumber, String address, String gstinNo,
			List<SupplierProductListModule> supplierProductListModuleList) {
		super();
		this.supplierName = supplierName;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.gstinNo = gstinNo;
		this.supplierProductListModuleList = supplierProductListModuleList;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGstinNo() {
		return gstinNo;
	}

	public void setGstinNo(String gstinNo) {
		this.gstinNo = gstinNo;
	}

	public List<SupplierProductListModule> getSupplierProductListModuleList() {
		return supplierProductListModuleList;
	}

	public void setSupplierProductListModuleList(List<SupplierProductListModule> supplierProductListModuleList) {
		this.supplierProductListModuleList = supplierProductListModuleList;
	}

	@Override
	public String toString() {
		return "SupplierProductListResponse [supplierName=" + supplierName + ", mobileNumber=" + mobileNumber
				+ ", address=" + address + ", gstinNo=" + gstinNo + ", supplierProductListModuleList="
				+ supplierProductListModuleList + "]";
	}

}
