package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;

public class CompanyModels {
	
	private Company company;
	private List<City> cityList;
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	@Override
	public String toString() {
		return "CompanyModels [company=" + company + ", cityList=" + cityList + "]";
	}
	
	

}
