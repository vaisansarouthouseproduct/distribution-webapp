package com.bluesquare.rc.rest.models;

import java.util.List;

public class ProductListResponse extends BaseDomain{
	
	List<ProductAddInventory> listInnerResponse;

	public List<ProductAddInventory> getListInnerResponse() {
		return listInnerResponse;
	}

	public void setListInnerResponse(List<ProductAddInventory> listInnerResponse) {
		this.listInnerResponse = listInnerResponse;
	}

	@Override
	public String toString() {
		return "ProductListResponse [listInnerResponse=" + listInnerResponse + "]";
	}

	
	

}
