package com.bluesquare.rc.rest.models;

public class EmployeeImageModal extends BaseDomain {

	public String employeeImage;

	public String getEmployeeImage() {
		return employeeImage;
	}

	public void setEmployeeImage(String employeeImage) {
		this.employeeImage = employeeImage;
	}

	@Override
	public String toString() {
		return "EmployeeImageUpdateRequest [employeeImage=" + employeeImage + "]";
	}
	
}
