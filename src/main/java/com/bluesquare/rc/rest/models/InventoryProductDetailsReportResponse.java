package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.InventoryDetailsModel;

public class InventoryProductDetailsReportResponse extends BaseDomain {
	
	private InventoryReport inventory;
	
	private String name;
	
	private List<InventoryDetailsModel> inventoryDetails;

	public InventoryReport getInventory() {
		return inventory;
	}

	public void setInventory(InventoryReport inventory) {
		this.inventory = inventory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<InventoryDetailsModel> getInventoryDetails() {
		return inventoryDetails;
	}

	public void setInventoryDetails(List<InventoryDetailsModel> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}

	@Override
	public String toString() {
		return "InventoryProductDetailsReportResponse [inventory=" + inventory + ", name=" + name
				+ ", inventoryDetails=" + inventoryDetails + "]";
	}

}
