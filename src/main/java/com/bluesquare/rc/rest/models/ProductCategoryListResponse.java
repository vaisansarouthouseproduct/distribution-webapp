package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.responseEntities.CategoriesModel;

public class ProductCategoryListResponse extends BaseDomain {
	
	List<CategoriesModel> categoryListInnerResponse;

	public List<CategoriesModel> getCategoryListInnerResponse() {
		return categoryListInnerResponse;
	}

	public void setCategoryListInnerResponse(List<CategoriesModel> categoryListInnerResponse) {
		this.categoryListInnerResponse = categoryListInnerResponse;
	}

	@Override
	public String toString() {
		return "ProductCategoryListResponse [categoryListInnerResponse=" + categoryListInnerResponse + "]";
	}

	
	

}
