package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.models.EmployeeHolidayList;

public class EmployeeHolidayDetailsResponse extends BaseDomain{

	private long noOfHolidays;
	private long noOfPaidHolidays;
	private long noOfUnpaidHolidays;
	private double deductions;
	private List<EmployeeHolidayList> employeeHolidayList;

	public EmployeeHolidayDetailsResponse(long noOfHolidays, long noOfPaidHolidays, long noOfUnpaidHolidays,
			double deductions, List<EmployeeHolidayList> employeeHolidayList) {
		super();
		this.noOfHolidays = noOfHolidays;
		this.noOfPaidHolidays = noOfPaidHolidays;
		this.noOfUnpaidHolidays = noOfUnpaidHolidays;
		this.deductions = deductions;
		this.employeeHolidayList = employeeHolidayList;
	}

	public long getNoOfHolidays() {
		return noOfHolidays;
	}

	public void setNoOfHolidays(long noOfHolidays) {
		this.noOfHolidays = noOfHolidays;
	}

	public long getNoOfPaidHolidays() {
		return noOfPaidHolidays;
	}

	public void setNoOfPaidHolidays(long noOfPaidHolidays) {
		this.noOfPaidHolidays = noOfPaidHolidays;
	}

	public long getNoOfUnpaidHolidays() {
		return noOfUnpaidHolidays;
	}

	public void setNoOfUnpaidHolidays(long noOfUnpaidHolidays) {
		this.noOfUnpaidHolidays = noOfUnpaidHolidays;
	}

	public double getDeductions() {
		return deductions;
	}

	public void setDeductions(double deductions) {
		this.deductions = deductions;
	}

	public List<EmployeeHolidayList> getEmployeeHolidayList() {
		return employeeHolidayList;
	}

	public void setEmployeeHolidayList(List<EmployeeHolidayList> employeeHolidayList) {
		this.employeeHolidayList = employeeHolidayList;
	}

	@Override
	public String toString() {
		return "EmployeeHolidayDetailsResponse [noOfHolidays=" + noOfHolidays + ", noOfPaidHolidays=" + noOfPaidHolidays
				+ ", noOfUnpaidHolidays=" + noOfUnpaidHolidays + ", deductions=" + deductions + ", employeeHolidayList="
				+ employeeHolidayList + "]";
	}

}
