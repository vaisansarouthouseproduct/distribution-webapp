package com.bluesquare.rc.rest.models;

import java.util.Date;

public class PReturnOrderDetailsModel {

	private long srno;
	private String returnOrderProductId;
	private String orderId;
	private String shopName;
	private String shopAddress;
	private String salesManName;
	private String salesManAddress;
	private String mobileNumber;
	private long employeeDetailsId;
	private String areaName;
	private String regionName;
	private String cityName;
	private long totalQty;
	private double taxableAmt;
	private double totalTax;
	private double totalAmt;
	private Date dateOfOrder;
	private Date dateOfReturn;

	public PReturnOrderDetailsModel(long srno, String returnOrderProductId, String orderId, String shopName,
			String shopAddress, String salesManName, String salesManAddress, String mobileNumber,
			long employeeDetailsId, String areaName, String regionName, String cityName, long totalQty,
			double taxableAmt, double totalTax, double totalAmt, Date dateOfOrder, Date dateOfReturn) {
		super();
		this.srno = srno;
		this.returnOrderProductId = returnOrderProductId;
		this.orderId = orderId;
		this.shopName = shopName;
		this.shopAddress = shopAddress;
		this.salesManName = salesManName;
		this.salesManAddress = salesManAddress;
		this.mobileNumber = mobileNumber;
		this.employeeDetailsId = employeeDetailsId;
		this.areaName = areaName;
		this.regionName = regionName;
		this.cityName = cityName;
		this.totalQty = totalQty;
		this.taxableAmt = taxableAmt;
		this.totalTax = totalTax;
		this.totalAmt = totalAmt;
		this.dateOfOrder = dateOfOrder;
		this.dateOfReturn = dateOfReturn;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public String getSalesManName() {
		return salesManName;
	}

	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}

	public String getSalesManAddress() {
		return salesManAddress;
	}

	public void setSalesManAddress(String salesManAddress) {
		this.salesManAddress = salesManAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public long getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}

	public double getTaxableAmt() {
		return taxableAmt;
	}

	public void setTaxableAmt(double taxableAmt) {
		this.taxableAmt = taxableAmt;
	}

	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public Date getDateOfOrder() {
		return dateOfOrder;
	}

	public void setDateOfOrder(Date dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}

	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}

	@Override
	public String toString() {
		return "PReturnOrderDetailsModel [srno=" + srno + ", returnOrderProductId=" + returnOrderProductId
				+ ", orderId=" + orderId + ", shopName=" + shopName + ", shopAddress=" + shopAddress + ", salesManName="
				+ salesManName + ", salesManAddress=" + salesManAddress + ", mobileNumber=" + mobileNumber
				+ ", employeeDetailsId=" + employeeDetailsId + ", areaName=" + areaName + ", regionName=" + regionName
				+ ", cityName=" + cityName + ", totalQty=" + totalQty + ", taxableAmt=" + taxableAmt + ", totalTax="
				+ totalTax + ", totalAmt=" + totalAmt + ", dateOfOrder=" + dateOfOrder + ", dateOfReturn="
				+ dateOfReturn + "]";
	}

}
