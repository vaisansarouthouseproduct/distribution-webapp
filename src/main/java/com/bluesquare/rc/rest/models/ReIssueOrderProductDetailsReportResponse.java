package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.ReturnOrderProductDetailsReport;

public class ReIssueOrderProductDetailsReportResponse extends BaseDomain {

	private FetchOrderDetailsModel reIssueOrderDetails;
	private List<ReturnOrderProductDetailsReport> reIssueOrderProductDetailsList;

	public FetchOrderDetailsModel getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}

	public void setReIssueOrderDetails(FetchOrderDetailsModel reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}

	public List<ReturnOrderProductDetailsReport> getReIssueOrderProductDetailsList() {
		return reIssueOrderProductDetailsList;
	}

	public void setReIssueOrderProductDetailsList(
			List<ReturnOrderProductDetailsReport> reIssueOrderProductDetailsList) {
		this.reIssueOrderProductDetailsList = reIssueOrderProductDetailsList;
	}

	@Override
	public String toString() {
		return "ReIssueOrderProductDetailsReportResponse [reIssueOrderDetails=" + reIssueOrderDetails
				+ ", reIssueOrderProductDetailsList=" + reIssueOrderProductDetailsList + "]";
	}

}
