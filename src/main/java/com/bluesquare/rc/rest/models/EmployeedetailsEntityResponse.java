package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;

public class EmployeedetailsEntityResponse extends BaseDomain{
	
	private EmployeeDetails employeeDetails;
	private String employeeAreaListString;
	
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public String getEmployeeAreaListString() {
		return employeeAreaListString;
	}
	public void setEmployeeAreaListString(String employeeAreaListString) {
		this.employeeAreaListString = employeeAreaListString;
	}
	@Override
	public String toString() {
		return "EmployeedetailsEntityResponse [employeeDetails=" + employeeDetails + ", employeeAreaListString="
				+ employeeAreaListString + "]";
	}
	
	
	
	

}
