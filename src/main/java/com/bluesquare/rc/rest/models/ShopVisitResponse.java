package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ShopVisit;
import com.bluesquare.rc.responseEntities.ShopVisitModel;

public class ShopVisitResponse extends BaseDomain {

	private List<ShopVisitModel> shopVisitList;
	private ShopVisitModel shopVisit;
	private String shopVisitImage;

	public List<ShopVisitModel> getShopVisitList() {
		return shopVisitList;
	}

	public void setShopVisitList(List<ShopVisitModel> shopVisitList) {
		this.shopVisitList = shopVisitList;
	}

	public ShopVisitModel getShopVisit() {
		return shopVisit;
	}

	public void setShopVisit(ShopVisitModel shopVisit) {
		this.shopVisit = shopVisit;
	}

	public String getShopVisitImage() {
		return shopVisitImage;
	}

	public void setShopVisitImage(String shopVisitImage) {
		this.shopVisitImage = shopVisitImage;
	}

	@Override
	public String toString() {
		return "ShopVisitResponse [shopVisitList=" + shopVisitList + ", shopVisit=" + shopVisit + ", shopVisitImage="
				+ shopVisitImage + "]";
	}

}
