package com.bluesquare.rc.rest.models;

public class LoginRequest {

		private String userId;
	    
	    private String password;
	    
	    private String token;
	    
	    //used for admin login
	    private long companyId;
	    private long branchId;
	    
	    public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public LoginRequest() {
	    }

	    public String getUserId() {
	        return userId;
	    }

	    public void setUserId(String userId) {
	        this.userId = userId;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

		public long getCompanyId() {
			return companyId;
		}

		public void setCompanyId(long companyId) {
			this.companyId = companyId;
		}

		public long getBranchId() {
			return branchId;
		}

		public void setBranchId(long branchId) {
			this.branchId = branchId;
		}

		@Override
		public String toString() {
			return "LoginRequest [userId=" + userId + ", password=" + password + ", token=" + token + ", companyId="
					+ companyId + ", branchId=" + branchId + "]";
		}

	   
	}

