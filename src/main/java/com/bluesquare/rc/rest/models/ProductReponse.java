package com.bluesquare.rc.rest.models;

import java.util.Date;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;


public class ProductReponse extends BaseDomain{

	private long productId;

	private String productName;

	private String productCode;

	private CategoriesModel categories;

	private BrandModel brand;

	private float rate;

	private String productContentType;
	
	private String productDescription;

	private long threshold;

	private long currentQuantity;
	
	private long damageQuantity;
	
	private long freeQuantity;

	private Date productAddedDatetime;

	private Date productQuantityUpdatedDatetime;
	
	public ProductReponse() {
		// TODO Auto-generated constructor stub
	}
	
	public ProductReponse(Product product) {
		this.productId = product.getProductId();
		this.productName = product.getProductName();
		this.categories = new CategoriesModel(
				product.getCategories().getCategoryId(), 
				product.getCategories().getCategoryName(), 
				product.getCategories().getCategoryDescription(), 
				product.getCategories().getHsnCode(), 
				product.getCategories().getCgst(), 
				product.getCategories().getSgst(), 
				product.getCategories().getIgst(), 
				product.getCategories().getCategoryDate(), 
				product.getCategories().getCategoryUpdateDate());
		this.brand = new BrandModel(
				product.getBrand().getBrandId(), 
				product.getBrand().getName(), 
				product.getBrand().getBrandAddedDatetime(), 
				product.getBrand().getBrandUpdateDatetime());
		this.rate = product.getRate();
		this.threshold = product.getThreshold();
		this.currentQuantity = product.getCurrentQuantity();
		this.damageQuantity = product.getDamageQuantity();
		this.freeQuantity = product.getFreeQuantity();
		this.productAddedDatetime = product.getProductAddedDatetime();
		this.productQuantityUpdatedDatetime = product.getProductQuantityUpdatedDatetime();
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public CategoriesModel getCategories() {
		return categories;
	}

	public void setCategories(CategoriesModel categories) {
		this.categories = categories;
	}

	public BrandModel getBrand() {
		return brand;
	}

	public void setBrand(BrandModel brand) {
		this.brand = brand;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public String getProductContentType() {
		return productContentType;
	}

	public void setProductContentType(String productContentType) {
		this.productContentType = productContentType;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public long getThreshold() {
		return threshold;
	}

	public void setThreshold(long threshold) {
		this.threshold = threshold;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public long getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(long freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}

	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}

	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}

	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}

	@Override
	public String toString() {
		return "ProductReponse [productId=" + productId + ", productName=" + productName + ", productCode="
				+ productCode + ", categories=" + categories + ", brand=" + brand + ", rate=" + rate
				+ ", productContentType=" + productContentType + ", productDescription=" + productDescription
				+ ", threshold=" + threshold + ", currentQuantity=" + currentQuantity + ", damageQuantity="
				+ damageQuantity + ", freeQuantity=" + freeQuantity + ", productAddedDatetime=" + productAddedDatetime
				+ ", productQuantityUpdatedDatetime=" + productQuantityUpdatedDatetime + "]";
	}
	
	
}
