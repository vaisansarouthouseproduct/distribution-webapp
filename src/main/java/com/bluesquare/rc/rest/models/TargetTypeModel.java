package com.bluesquare.rc.rest.models;

import javax.persistence.Column;

public class TargetTypeModel {

	private long targetTypeId;
	private String type;

	private String valueType;

	public TargetTypeModel(long targetTypeId, String type, String valueType) {
		super();
		this.targetTypeId = targetTypeId;
		this.type = type;
		this.valueType = valueType;
	}

	public long getTargetTypeId() {
		return targetTypeId;
	}

	public void setTargetTypeId(long targetTypeId) {
		this.targetTypeId = targetTypeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	@Override
	public String toString() {
		return "TargetTypeModel [targetTypeId=" + targetTypeId + ", type=" + type + ", valueType=" + valueType + "]";
	}

}
