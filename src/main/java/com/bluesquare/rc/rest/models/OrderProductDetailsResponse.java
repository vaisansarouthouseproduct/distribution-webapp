package com.bluesquare.rc.rest.models;

import java.util.Date;
import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.responseEntities.OrderProductDetailsModel;

public class OrderProductDetailsResponse extends BaseDomain {

	private String orderId;
	private Date orderDetailsAddedDatetime;
	private String shopName;
	private String mobileNumber;
	private String areaName;
	private String orderStatus;
	private OrderDetails orderDetailList;
	private List<OrderProductDetailsModel> orderProductDetailList;
	private String salesPersonName;
	private List<EmployeeNameAndId> employeeNameAndIdSMAndDBList;
	private ReturnOrderProduct returnOrderProduct;
	private List<Transportation> transportations;

	private String businessNameId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDetailsAddedDatetime() {
		return orderDetailsAddedDatetime;
	}

	public void setOrderDetailsAddedDatetime(Date orderDetailsAddedDatetime) {
		this.orderDetailsAddedDatetime = orderDetailsAddedDatetime;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public OrderDetails getOrderDetailList() {
		return orderDetailList;
	}

	public void setOrderDetailList(OrderDetails orderDetailList) {
		this.orderDetailList = orderDetailList;
	}

	public List<OrderProductDetailsModel> getOrderProductDetailList() {
		return orderProductDetailList;
	}

	public void setOrderProductDetailList(List<OrderProductDetailsModel> orderProductDetailList) {
		this.orderProductDetailList = orderProductDetailList;
	}

	public String getSalesPersonName() {
		return salesPersonName;
	}

	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}

	public List<EmployeeNameAndId> getEmployeeNameAndIdSMAndDBList() {
		return employeeNameAndIdSMAndDBList;
	}

	public void setEmployeeNameAndIdSMAndDBList(List<EmployeeNameAndId> employeeNameAndIdSMAndDBList) {
		this.employeeNameAndIdSMAndDBList = employeeNameAndIdSMAndDBList;
	}

	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}

	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}

	public List<Transportation> getTransportations() {
		return transportations;
	}

	public void setTransportations(List<Transportation> transportations) {
		this.transportations = transportations;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	@Override
	public String toString() {
		return "OrderProductDetailsResponse [orderId=" + orderId + ", orderDetailsAddedDatetime="
				+ orderDetailsAddedDatetime + ", shopName=" + shopName + ", mobileNumber=" + mobileNumber
				+ ", areaName=" + areaName + ", orderStatus=" + orderStatus + ", orderDetailList=" + orderDetailList
				+ ", orderProductDetailList=" + orderProductDetailList + ", salesPersonName=" + salesPersonName
				+ ", employeeNameAndIdSMAndDBList=" + employeeNameAndIdSMAndDBList + ", returnOrderProduct="
				+ returnOrderProduct + ", transportations=" + transportations + ", businessNameId=" + businessNameId
				+ "]";
	}
	
	
	

}