package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.DailyStockDetailsModel;

public class DailyStockReportResponse extends BaseDomain {
    private List<DailyStockDetailsModel> dailyStockDetails;

    public List<DailyStockDetailsModel> getDailyStockDetails() {
        return dailyStockDetails;
    }

    public void setDailyStockDetails(List<DailyStockDetailsModel> dailyStockDetails) {
        this.dailyStockDetails = dailyStockDetails;
    }

    @Override
    public String toString() {
        return "DailyStockReportResponse{" +
                "dailyStockDetails=" + dailyStockDetails +
                '}';
    }
}