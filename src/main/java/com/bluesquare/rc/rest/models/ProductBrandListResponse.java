package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.responseEntities.BrandModel;

public class ProductBrandListResponse extends BaseDomain{
	
	List<BrandModel> brandList;

	public List<BrandModel> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<BrandModel> brandList) {
		this.brandList = brandList;
	}

	@Override
	public String toString() {
		return "ProductBrandListResponse [brandList=" + brandList + "]";
	}

	
	
	

}
