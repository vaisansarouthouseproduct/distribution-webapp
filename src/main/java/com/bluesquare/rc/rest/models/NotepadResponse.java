package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.NotePadModel;

public class NotepadResponse extends BaseDomain{
	
	private NotePadModel notePad;
	private List<NotePadModel> notePadList;
	
	public NotePadModel getNotePad() {
		return notePad;
	}
	public void setNotePad(NotePadModel notePad) {
		this.notePad = notePad;
	}
	public List<NotePadModel> getNotePadList() {
		return notePadList;
	}
	public void setNotePadList(List<NotePadModel> notePadList) {
		this.notePadList = notePadList;
	}
	@Override
	public String toString() {
		return "NotepadResponse [notePad=" + notePad + ", notePadList=" + notePadList + "]";
	}
	

}
