package com.bluesquare.rc.rest.models;

import java.util.Date;

import com.bluesquare.rc.entities.BusinessName;

public class OrderDetailsForPayment {

	private String orderId;
	private BusinessName businessName;
	private double amountDue;
	private double totalAmountWithTax;
	private Date dueDate;
	public OrderDetailsForPayment(String orderId, BusinessName businessName, double amountDue,
			double totalAmountWithTax, Date dueDate) {
		super();
		this.orderId = orderId;
		this.businessName = businessName;
		this.amountDue = amountDue;
		this.totalAmountWithTax = totalAmountWithTax;
		this.dueDate = dueDate;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public BusinessName getBusinessName() {
		return businessName;
	}
	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}
	public double getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	@Override
	public String toString() {
		return "OrderDetailsForPayment [orderId=" + orderId + ", businessName=" + businessName + ", amountDue="
				+ amountDue + ", totalAmountWithTax=" + totalAmountWithTax + ", dueDate=" + dueDate + "]";
	}	
}
