package com.bluesquare.rc.rest.models;
import java.util.List;

public class TargetMainResponse extends BaseDomain {

    private List<TargetResponse> targetResponseList;

	public List<TargetResponse> getTargetResponseList() {
		return targetResponseList;
	}

	public void setTargetResponseList(List<TargetResponse> targetResponseList) {
		this.targetResponseList = targetResponseList;
	}

	@Override
	public String toString() {
		return "TargetMainResponse [targetResponseList=" + targetResponseList + "]";
	}
    
    
}