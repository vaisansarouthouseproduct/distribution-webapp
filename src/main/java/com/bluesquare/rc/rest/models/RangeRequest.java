package com.bluesquare.rc.rest.models;

public class RangeRequest {
	
	private long employeeId;
	private String range;
	private String fromDate;
	private String toDate;
	
	
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "ShopVisitRequest [employeeId=" + employeeId + ", range=" + range + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + "]";
	}
	
	
	
}
