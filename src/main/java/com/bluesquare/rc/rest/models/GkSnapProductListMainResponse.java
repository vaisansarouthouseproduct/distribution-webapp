package com.bluesquare.rc.rest.models;

import java.util.List;

public class GkSnapProductListMainResponse extends BaseDomain {

	private List<GkSnapProductResponse> gkSnapProductResponses;

	public List<GkSnapProductResponse> getGkSnapProductResponses() {
		return gkSnapProductResponses;
	}

	public void setGkSnapProductResponses(List<GkSnapProductResponse> gkSnapProductResponses) {
		this.gkSnapProductResponses = gkSnapProductResponses;
	}

	@Override
	public String toString() {
		return "GkSnapProductListMainResponse [gkSnapProductResponses=" + gkSnapProductResponses + "]";
	}
	
	
}
