package com.bluesquare.rc.rest.models;

public class TargetRequestModel {

	public long employeeId;
	public String targetPeriod;

	public String pickDate;

	public String weekStartDate;
	public String weekEndDate;

	public int monthNo;
	public long monthYear;

	public long Year;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getTargetPeriod() {
		return targetPeriod;
	}

	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}

	public String getPickDate() {
		return pickDate;
	}

	public void setPickDate(String pickDate) {
		this.pickDate = pickDate;
	}

	public String getWeekStartDate() {
		return weekStartDate;
	}

	public void setWeekStartDate(String weekStartDate) {
		this.weekStartDate = weekStartDate;
	}

	public String getWeekEndDate() {
		return weekEndDate;
	}

	public void setWeekEndDate(String weekEndDate) {
		this.weekEndDate = weekEndDate;
	}

	public int getMonthNo() {
		return monthNo;
	}

	public void setMonthNo(int monthNo) {
		this.monthNo = monthNo;
	}

	public long getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(long monthYear) {
		this.monthYear = monthYear;
	}

	public long getYear() {
		return Year;
	}

	public void setYear(long year) {
		Year = year;
	}

	@Override
	public String toString() {
		return "TargetRequestModel [employeeId=" + employeeId + ", targetPeriod=" + targetPeriod + ", pickDate="
				+ pickDate + ", weekStartDate=" + weekStartDate + ", weekEndDate=" + weekEndDate + ", monthNo="
				+ monthNo + ", monthYear=" + monthYear + ", Year=" + Year + "]";
	}

}
