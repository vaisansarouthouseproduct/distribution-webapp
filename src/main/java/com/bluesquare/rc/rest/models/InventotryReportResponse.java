package com.bluesquare.rc.rest.models;

import java.util.List;

public class InventotryReportResponse extends BaseDomain{
	
	private List<InventoryReport> inventory;

	public List<InventoryReport> getInventory() {
		return inventory;
	}

	public void setInventory(List<InventoryReport> inventory) {
		this.inventory = inventory;
	}

	@Override
	public String toString() {
		return "InventotryReportResonse [inventory=" + inventory + "]";
	}

	

}
