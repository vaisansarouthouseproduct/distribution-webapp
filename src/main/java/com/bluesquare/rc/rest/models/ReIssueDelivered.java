/**
 * 
 */
package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.OfflineAPIRequest;

/**
 * @author aNKIT
 *
 */
public class ReIssueDelivered extends OfflineAPIRequest{
	
	private long reIssueOrderId;
	private String signBase64;
	public long getReIssueOrderId() {
		return reIssueOrderId;
	}
	public void setReIssueOrderId(long reIssueOrderId) {
		this.reIssueOrderId = reIssueOrderId;
	}
	public String getSignBase64() {
		return signBase64;
	}
	public void setSignBase64(String signBase64) {
		this.signBase64 = signBase64;
	}
	@Override
	public String toString() {
		return "ReIssueDelivered [reIssueOrderId=" + reIssueOrderId + ", signBase64=" + signBase64 + "]";
	}

}
