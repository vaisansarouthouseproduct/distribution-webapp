package com.bluesquare.rc.rest.models;

public class OrderProductIssueReportRequest {
	
	private long employeeId;
	private String fromDate;
	private String toDate;
	private String range;
	private long areaId;
	
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public long getAreaId() {
		return areaId;
	}
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}
	@Override
	public String toString() {
		return "OrderProductIssueReportRequest [employeeId=" + employeeId + ", fromDate=" + fromDate + ", toDate="
				+ toDate + ", range=" + range + ", areaId=" + areaId + "]";
	}
	
	

}
