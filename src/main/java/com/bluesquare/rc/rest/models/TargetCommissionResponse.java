package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.TargetAssignTargets;

public class TargetCommissionResponse extends BaseDomain {

	private List<TargetAssignTargets> targetAssignTargetsList;
	private List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList;

	public List<TargetAssignTargets> getTargetAssignTargetsList() {
		return targetAssignTargetsList;
	}

	public void setTargetAssignTargetsList(List<TargetAssignTargets> targetAssignTargetsList) {
		this.targetAssignTargetsList = targetAssignTargetsList;
	}

	public List<CommissionAssignTargetSlabs> getCommissionAssignTargetSlabsList() {
		return commissionAssignTargetSlabsList;
	}

	public void setCommissionAssignTargetSlabsList(List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList) {
		this.commissionAssignTargetSlabsList = commissionAssignTargetSlabsList;
	}

	@Override
	public String toString() {
		return "TargetCommissionResponse [targetAssignTargetsList=" + targetAssignTargetsList
				+ ", commissionAssignTargetSlabsList=" + commissionAssignTargetSlabsList + "]";
	}

}
