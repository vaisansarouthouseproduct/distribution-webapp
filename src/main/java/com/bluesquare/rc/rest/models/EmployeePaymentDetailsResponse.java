package com.bluesquare.rc.rest.models;

public class EmployeePaymentDetailsResponse extends BaseDomain {
	public String bankName;
	public String bankBranchName;
	public String bankAccountNumber;
	public double basicSalary;
	public double incentiveAmount;
	public double deductionAmount;
	public double totalAmountPayble;
	public double totalAmountPaid;
	public double totalAmountPending;

	public EmployeePaymentDetailsResponse(String bankName, String bankBranchName, String bankAccountNumber,
			double basicSalary, double incentiveAmount, double deductionAmount, double totalAmountPayble,
			double totalAmountPaid, double totalAmountPending) {
		super();
		this.bankName = bankName;
		this.bankBranchName = bankBranchName;
		this.bankAccountNumber = bankAccountNumber;
		this.basicSalary = basicSalary;
		this.incentiveAmount = incentiveAmount;
		this.deductionAmount = deductionAmount;
		this.totalAmountPayble = totalAmountPayble;
		this.totalAmountPaid = totalAmountPaid;
		this.totalAmountPending = totalAmountPending;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public double getIncentiveAmount() {
		return incentiveAmount;
	}

	public void setIncentiveAmount(double incentiveAmount) {
		this.incentiveAmount = incentiveAmount;
	}

	public double getDeductionAmount() {
		return deductionAmount;
	}

	public void setDeductionAmount(double deductionAmount) {
		this.deductionAmount = deductionAmount;
	}

	public double getTotalAmountPayble() {
		return totalAmountPayble;
	}

	public void setTotalAmountPayble(double totalAmountPayble) {
		this.totalAmountPayble = totalAmountPayble;
	}

	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public double getTotalAmountPending() {
		return totalAmountPending;
	}

	public void setTotalAmountPending(double totalAmountPending) {
		this.totalAmountPending = totalAmountPending;
	}

	@Override
	public String toString() {
		return "EmployeePaymentDetailsResponse [bankName=" + bankName + ", bankBranchName=" + bankBranchName
				+ ", bankAccountNumber=" + bankAccountNumber + ", basicSalary=" + basicSalary + ", incentiveAmount="
				+ incentiveAmount + ", deductionAmount=" + deductionAmount + ", totalAmountPayble=" + totalAmountPayble
				+ ", totalAmountPaid=" + totalAmountPaid + ", totalAmountPending=" + totalAmountPending + "]";
	}

}
