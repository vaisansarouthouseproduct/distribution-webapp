package com.bluesquare.rc.rest.models;

public class CustomerReportResponse {

	private String businessNameId;
	private String shopName;
	private String areaName;
	private long noOfOrders;
	private double totalAmount;

	public CustomerReportResponse(String businessNameId, String shopName, String areaName, long noOfOrders,
			double totalAmount) {
		super();
		this.businessNameId = businessNameId;
		this.shopName = shopName;
		this.areaName = areaName;
		this.noOfOrders = noOfOrders;
		this.totalAmount = totalAmount;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public long getNoOfOrders() {
		return noOfOrders;
	}

	public void setNoOfOrders(long noOfOrders) {
		this.noOfOrders = noOfOrders;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "CustomerReportResponse [businessNameId=" + businessNameId + ", shopName=" + shopName + ", areaName="
				+ areaName + ", noOfOrders=" + noOfOrders + ", totalAmount=" + totalAmount + "]";
	}

}
