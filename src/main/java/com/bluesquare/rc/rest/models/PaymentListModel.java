package com.bluesquare.rc.rest.models;

import java.util.Date;
import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;

public class PaymentListModel extends BaseDomain {

	private String orderId;
	private String shopName;
	private String mobileNumber;
	private Date dateOfOrder;
	private Date deliveryDate;
	private double issuedAmountWithTax;
	private Date dueDate;
	// private OrderDetails orderDetails;
	private Date lastPaidDate;
	private double paidAmt;
	private double unPaidAmt;
	private List<PaymentList> paymentList;

	public PaymentListModel() {
		// TODO Auto-generated constructor stub
	}

	public PaymentListModel(String orderId, String shopName, String mobileNumber, Date dateOfOrder, Date deliveryDate,
			double issuedAmountWithTax, Date dueDate, Date lastPaidDate, double paidAmt, double unPaidAmt,
			List<PaymentList> paymentList) {
		super();
		this.orderId = orderId;
		this.shopName = shopName;
		this.mobileNumber = mobileNumber;
		this.dateOfOrder = dateOfOrder;
		this.deliveryDate = deliveryDate;
		this.issuedAmountWithTax = issuedAmountWithTax;
		this.dueDate = dueDate;
		this.lastPaidDate = lastPaidDate;
		this.paidAmt = paidAmt;
		this.unPaidAmt = unPaidAmt;
		this.paymentList = paymentList;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Date getDateOfOrder() {
		return dateOfOrder;
	}

	public void setDateOfOrder(Date dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public double getIssuedAmountWithTax() {
		return issuedAmountWithTax;
	}

	public void setIssuedAmountWithTax(double issuedAmountWithTax) {
		this.issuedAmountWithTax = issuedAmountWithTax;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLastPaidDate() {
		return lastPaidDate;
	}

	public void setLastPaidDate(Date lastPaidDate) {
		this.lastPaidDate = lastPaidDate;
	}

	public double getPaidAmt() {
		return paidAmt;
	}

	public void setPaidAmt(double paidAmt) {
		this.paidAmt = paidAmt;
	}

	public double getUnPaidAmt() {
		return unPaidAmt;
	}

	public void setUnPaidAmt(double unPaidAmt) {
		this.unPaidAmt = unPaidAmt;
	}

	public List<PaymentList> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<PaymentList> paymentList) {
		this.paymentList = paymentList;
	}

	@Override
	public String toString() {
		return "PaymentListModel [orderId=" + orderId + ", shopName=" + shopName + ", mobileNumber=" + mobileNumber
				+ ", dateOfOrder=" + dateOfOrder + ", deliveryDate=" + deliveryDate + ", issuedAmountWithTax="
				+ issuedAmountWithTax + ", dueDate=" + dueDate + ", lastPaidDate=" + lastPaidDate + ", paidAmt="
				+ paidAmt + ", unPaidAmt=" + unPaidAmt + ", paymentList=" + paymentList + "]";
	}



}
