package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;

public class BusinessTypeListResponse extends BaseDomain{

	private List<BusinessTypeModel> businessTypesList;

	public List<BusinessTypeModel> getBusinessTypesList() {
		return businessTypesList;
	}

	public void setBusinessTypesList(List<BusinessTypeModel> businessTypesList) {
		this.businessTypesList = businessTypesList;
	}

	@Override
	public String toString() {
		return "BusinessTypeListResponse [businessTypesList=" + businessTypesList + "]";
	}
	
	
}
