package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.responseEntities.RegionModel;

public class RegionListResponse extends BaseDomain{
	private List<RegionModel> regionList;

	public List<RegionModel> getRegionList() {
		return regionList;
	}

	public void setRegionList(List<RegionModel> regionList) {
		this.regionList = regionList;
	}

	@Override
	public String toString() {
		return "RegionListResponse [regionList=" + regionList + "]";
	}

	

}
