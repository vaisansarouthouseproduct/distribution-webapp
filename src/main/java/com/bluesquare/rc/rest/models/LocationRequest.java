package com.bluesquare.rc.rest.models;

public class LocationRequest {
	private String longitude;
	private String latitude;
	private long employeeId;
	private String address;
	private long dateTime;
	private String info;
	private boolean newRoute;
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getDateTime() {
		return dateTime;
	}
	public void setDateTime(long dateTime) {
		this.dateTime = dateTime;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public boolean isNewRoute() {
		return newRoute;
	}
	public void setNewRoute(boolean newRoute) {
		this.newRoute = newRoute;
	}
	@Override
	public String toString() {
		return "LocationRequest [longitude=" + longitude + ", latitude=" + latitude + ", employeeId=" + employeeId
				+ ", address=" + address + ", dateTime=" + dateTime + ", info=" + info + ", newRoute=" + newRoute + "]";
	}
}