package com.bluesquare.rc.rest.models;

public class OrderDetailByBusinessNameIdEmployeeIdRequest {

	private String businessNameId;
	private long employeeId;
	private String range;
	private String startDate;
	private String endDate;
	
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "OrderDetailByBusinessNameIdEmployeeIdRequest [businessNameId=" + businessNameId + ", employeeId="
				+ employeeId + ", range=" + range + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
	
	
}
