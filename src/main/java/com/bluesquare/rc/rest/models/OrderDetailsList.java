package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.responseEntities.AreaModel;

public class OrderDetailsList extends BaseDomain {

	private List<OrderDetails> orderDetailsForWebList;
	private List<FetchOrderDetailsModel> orderDetailsList;
	private List<AreaModel> areaList;
	private String pageName;
	private String salesManName;
	
	public List<OrderDetails> getOrderDetailsForWebList() {
		return orderDetailsForWebList;
	}
	public void setOrderDetailsForWebList(List<OrderDetails> orderDetailsForWebList) {
		this.orderDetailsForWebList = orderDetailsForWebList;
	}
	public List<FetchOrderDetailsModel> getOrderDetailsList() {
		return orderDetailsList;
	}
	public void setOrderDetailsList(List<FetchOrderDetailsModel> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}
	public List<AreaModel> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<AreaModel> areaList) {
		this.areaList = areaList;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getSalesManName() {
		return salesManName;
	}
	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}
	@Override
	public String toString() {
		return "OrderDetailsList [orderDetailsForWebList=" + orderDetailsForWebList + ", orderDetailsList="
				+ orderDetailsList + ", areaList=" + areaList + ", pageName=" + pageName + ", salesManName="
				+ salesManName + "]";
	}
	
	
}
