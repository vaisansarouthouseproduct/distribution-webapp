/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.responseEntities.ReIssueOrderDetailsModel;
import com.bluesquare.rc.responseEntities.ReIssueOrderProductDetailsReport;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderProductDetailsListModel extends BaseDomain {

	private ReIssueOrderDetailsModel reIssueOrderDetailsModel;// For App
	private List<ReIssueOrderProductDetailsReport> reIssueOrderLists;

	public ReIssueOrderDetailsModel getReIssueOrderDetailsModel() {
		return reIssueOrderDetailsModel;
	}

	public void setReIssueOrderDetailsModel(ReIssueOrderDetailsModel reIssueOrderDetailsModel) {
		this.reIssueOrderDetailsModel = reIssueOrderDetailsModel;
	}

	public List<ReIssueOrderProductDetailsReport> getReIssueOrderLists() {
		return reIssueOrderLists;
	}

	public void setReIssueOrderLists(List<ReIssueOrderProductDetailsReport> reIssueOrderLists) {
		this.reIssueOrderLists = reIssueOrderLists;
	}

	@Override
	public String toString() {
		return "ReIssueOrderProductDetailsListModel [reIssueOrderDetailsModel=" + reIssueOrderDetailsModel
				+ ", reIssueOrderLists=" + reIssueOrderLists + "]";
	}

}
