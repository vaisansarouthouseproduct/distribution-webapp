package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.State;

public class LocationSectionResponse extends BaseDomain{
	
	private List<Country> countryList;
	private List<State> stateList;
	private List<City> cityList;
	
	
	public List<Country> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}
	public List<State> getStateList() {
		return stateList;
	}
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	@Override
	public String toString() {
		return "LocationResponse [countryList=" + countryList + ", stateList=" + stateList + ", cityList=" + cityList
				+ "]";
	}
	
	
	
	
	
	

}
