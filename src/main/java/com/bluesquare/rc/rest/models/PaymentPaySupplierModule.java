package com.bluesquare.rc.rest.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class PaymentPaySupplierModule {

	private long paymentPayId;

	private double dueAmount;

	private Date dueDate;

	private double paidAmount;

	private Date paidDate;

	private String payType;

	private String chequeNumber;

	private String bankName;

	private Date chequeDate;

	private boolean status;

	public PaymentPaySupplierModule(long paymentPayId, double dueAmount, Date dueDate, double paidAmount, Date paidDate,
			String payType, String chequeNumber, String bankName, Date chequeDate, boolean status) {
		super();
		this.paymentPayId = paymentPayId;
		this.dueAmount = dueAmount;
		this.dueDate = dueDate;
		this.paidAmount = paidAmount;
		this.paidDate = paidDate;
		this.payType = payType;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.chequeDate = chequeDate;
		this.status = status;
	}

	public long getPaymentPayId() {
		return paymentPayId;
	}

	public void setPaymentPayId(long paymentPayId) {
		this.paymentPayId = paymentPayId;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PaymentPaySupplier [paymentPayId=" + paymentPayId + ", dueAmount=" + dueAmount + ", dueDate=" + dueDate
				+ ", paidAmount=" + paidAmount + ", paidDate=" + paidDate + ", payType=" + payType + ", chequeNumber="
				+ chequeNumber + ", bankName=" + bankName + ", chequeDate=" + chequeDate + ", status=" + status + "]";
	}

}
