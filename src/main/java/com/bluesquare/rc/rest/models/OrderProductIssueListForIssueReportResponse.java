package com.bluesquare.rc.rest.models;

import java.util.Date;
import java.util.List;

import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.responseEntities.OrderProductDetailsModel;

public class OrderProductIssueListForIssueReportResponse extends BaseDomain{
	
	private String salesman;
	private String deliveryBoy;
	private String orderId;
	private Date orderDetailsAddedDatetime;
	private String shopName;
	private String mobileNumber;
	private String areaName;
	private String orderStatus;
	private OrderProductIssueDetails orderProductIssueDetails;
	private List<OrderProductDetailsModel> orderProductDetailsList;
	
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getDeliveryBoy() {
		return deliveryBoy;
	}
	public void setDeliveryBoy(String deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}
	public OrderProductIssueDetails getOrderProductIssueDetails() {
		return orderProductIssueDetails;
	}
	public void setOrderProductIssueDetails(OrderProductIssueDetails orderProductIssueDetails) {
		this.orderProductIssueDetails = orderProductIssueDetails;
	}
	public List<OrderProductDetailsModel> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}
	public void setOrderProductDetailsList(List<OrderProductDetailsModel> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getOrderDetailsAddedDatetime() {
		return orderDetailsAddedDatetime;
	}
	public void setOrderDetailsAddedDatetime(Date orderDetailsAddedDatetime) {
		this.orderDetailsAddedDatetime = orderDetailsAddedDatetime;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	@Override
	public String toString() {
		return "OrderProductIssueListForIssueReportResponse [salesman=" + salesman + ", deliveryBoy=" + deliveryBoy
				+ ", orderId=" + orderId + ", orderDetailsAddedDatetime=" + orderDetailsAddedDatetime
				+ ", shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", areaName=" + areaName
				+ ", orderStatus=" + orderStatus + ", orderProductDetailsList=" + orderProductDetailsList + "]";
	}
	
	
		

}
