package com.bluesquare.rc.rest.models;

public class BusinessNameListRequest {

    public long businessTypeId;
    
    public long areaId;

    public long getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(long businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public long getAreaId() {
        return areaId;
    }

    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }

    @Override
    public String toString() {
        return "BusinessnameListRequest [businessTypeId=" + businessTypeId + ", areaId=" + areaId + "]";
    }
    
    
}