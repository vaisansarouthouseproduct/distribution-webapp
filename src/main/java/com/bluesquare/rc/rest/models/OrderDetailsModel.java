package com.bluesquare.rc.rest.models;

import java.util.Date;

public class OrderDetailsModel {

	private String orderId;
	private String orderStatus;
	private double totalAmountWithTax;
	private double confirmAmountWithTax;
	private Date dateOfOrderTaken;

	public OrderDetailsModel() {
		// TODO Auto-generated constructor stub
	}
	
	public OrderDetailsModel(String orderId, String orderStatus, double totalAmountWithTax, double confirmAmountWithTax,
			Date dateOfOrderTaken) {
		super();
		this.orderId = orderId;
		this.orderStatus = orderStatus;
		this.totalAmountWithTax = totalAmountWithTax;
		this.confirmAmountWithTax = confirmAmountWithTax;
		this.dateOfOrderTaken = dateOfOrderTaken;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public double getConfirmAmountWithTax() {
		return confirmAmountWithTax;
	}

	public void setConfirmAmountWithTax(double confirmAmountWithTax) {
		this.confirmAmountWithTax = confirmAmountWithTax;
	}

	public Date getDateOfOrderTaken() {
		return dateOfOrderTaken;
	}

	public void setDateOfOrderTaken(Date dateOfOrderTaken) {
		this.dateOfOrderTaken = dateOfOrderTaken;
	}

	@Override
	public String toString() {
		return "OrderDetailsListForCustomerReport [orderId=" + orderId + ", orderStatus=" + orderStatus
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", confirmAmountWithTax=" + confirmAmountWithTax
				+ ", dateOfOrderTaken=" + dateOfOrderTaken + "]";
	}

}
