package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.Meeting;

public class MeetingResponse extends BaseDomain {
	public Meeting meeting;

	public Meeting getMeeting() {
		return meeting;
	}

	public void setMeeting(Meeting meeting) {
		this.meeting = meeting;
	}

	@Override
	public String toString() {
		return "MeetingResponse [meeting=" + meeting + "]";
	}
	
	
}
