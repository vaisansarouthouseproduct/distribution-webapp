package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;

public class TargetResponse{

	private long srno;
	private String targetName;
	private double targetAssignValue;
	private double targetAchieveValue;
	private long commissionId;
	private List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList;
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	public double getTargetAssignValue() {
		return targetAssignValue;
	}
	public void setTargetAssignValue(double targetAssignValue) {
		this.targetAssignValue = targetAssignValue;
	}
	public double getTargetAchieveValue() {
		return targetAchieveValue;
	}
	public void setTargetAchieveValue(double targetAchieveValue) {
		this.targetAchieveValue = targetAchieveValue;
	}
	public long getCommissionId() {
		return commissionId;
	}
	public void setCommissionId(long commissionId) {
		this.commissionId = commissionId;
	}
	public List<CommissionAssignTargetSlabs> getCommissionAssignTargetSlabsList() {
		return commissionAssignTargetSlabsList;
	}
	public void setCommissionAssignTargetSlabsList(List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList) {
		this.commissionAssignTargetSlabsList = commissionAssignTargetSlabsList;
	}
	public TargetResponse(long srno, String targetName, double targetAssignValue, double targetAchieveValue,
			long commissionId, List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList) {
		super();
		this.srno = srno;
		this.targetName = targetName;
		this.targetAssignValue = targetAssignValue;
		this.targetAchieveValue = targetAchieveValue;
		this.commissionId = commissionId;
		this.commissionAssignTargetSlabsList = commissionAssignTargetSlabsList;
	}
	@Override
	public String toString() {
		return "TargetResponse [srno=" + srno + ", targetName=" + targetName + ", targetAssignValue="
				+ targetAssignValue + ", targetAchieveValue=" + targetAchieveValue + ", commissionId=" + commissionId
				+ ", commissionAssignTargetSlabsList=" + commissionAssignTargetSlabsList + "]";
	}
	
}