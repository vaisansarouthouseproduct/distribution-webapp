package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.Meeting;

public class MeetingRequest {
	
	private long meetingId;
	private String cancelReason;
	private String meetingReview;
	private String isNextMeetScheduled;
	private Meeting newMeeting;
	public long getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(long meetingId) {
		this.meetingId = meetingId;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getMeetingReview() {
		return meetingReview;
	}
	public void setMeetingReview(String meetingReview) {
		this.meetingReview = meetingReview;
	}
	public String getIsNextMeetScheduled() {
		return isNextMeetScheduled;
	}
	public void setIsNextMeetScheduled(String isNextMeetScheduled) {
		this.isNextMeetScheduled = isNextMeetScheduled;
	}
	public Meeting getNewMeeting() {
		return newMeeting;
	}
	public void setNewMeeting(Meeting newMeeting) {
		this.newMeeting = newMeeting;
	}
	@Override
	public String toString() {
		return "MeetingRequest [meetingId=" + meetingId + ", cancelReason=" + cancelReason + ", meetingReview="
				+ meetingReview + ", isNextMeetScheduled=" + isNextMeetScheduled + ", newMeeting=" + newMeeting + "]";
	}


}
