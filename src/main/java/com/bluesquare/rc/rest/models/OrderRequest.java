package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;

public class OrderRequest {
	
	private OrderDetails orderDetails;
	
	private List<OrderProductDetails> orderProductDetailList;
	
	private String returnProductOrderId;
	
	private String editModeList;

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public List<OrderProductDetails> getOrderProductDetailList() {
		return orderProductDetailList;
	}

	public void setOrderProductDetailList(List<OrderProductDetails> orderProductDetailList) {
		this.orderProductDetailList = orderProductDetailList;
	}

	public String getReturnProductOrderId() {
		return returnProductOrderId;
	}

	public void setReturnProductOrderId(String returnProductOrderId) {
		this.returnProductOrderId = returnProductOrderId;
	}

	public String getEditModeList() {
		return editModeList;
	}

	public void setEditModeList(String editModeList) {
		this.editModeList = editModeList;
	}

	@Override
	public String toString() {
		return "OrderRequest [orderDetails=" + orderDetails + ", orderProductDetailList=" + orderProductDetailList
				+ ", returnProductOrderId=" + returnProductOrderId + ", editModeList=" + editModeList + "]";
	}

}
