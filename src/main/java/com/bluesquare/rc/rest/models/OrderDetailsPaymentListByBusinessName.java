package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;

public class OrderDetailsPaymentListByBusinessName extends BaseDomain {

	private List<OrderDetailsPaymentList> orderDetailsPaymentList;
	private String shopName;
	private String mobileNumber;
	private String areaName;
	private String salesManName;

	public List<OrderDetailsPaymentList> getOrderDetailsPaymentList() {
		return orderDetailsPaymentList;
	}

	public void setOrderDetailsPaymentList(List<OrderDetailsPaymentList> orderDetailsPaymentList) {
		this.orderDetailsPaymentList = orderDetailsPaymentList;
	}

	public String getSalesManName() {
		return salesManName;
	}

	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Override
	public String toString() {
		return "OrderDetailsPaymentListByBusinessName [orderDetailsPaymentList=" + orderDetailsPaymentList
				+ ", shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", areaName=" + areaName
				+ ", salesManName=" + salesManName + "]";
	}

}
