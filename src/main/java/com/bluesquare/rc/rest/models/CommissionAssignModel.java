package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.models.CommissionAssignRequest;

public class CommissionAssignModel extends BaseDomain {
	public List<CommissionAssignRequest> commissionAssignList;

	public List<CommissionAssignRequest> getCommissionAssignList() {
		return commissionAssignList;
	}

	public void setCommissionAssignList(List<CommissionAssignRequest> commissionAssignList) {
		this.commissionAssignList = commissionAssignList;
	}

	@Override
	public String toString() {
		return "CommissionAssignModel [commissionAssignList=" + commissionAssignList + "]";
	}
	
}
