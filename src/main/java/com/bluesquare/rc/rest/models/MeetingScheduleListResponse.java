package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.models.MeetingScheduleModel;

public class MeetingScheduleListResponse extends BaseDomain {
	public List<MeetingScheduleModel> meetingScheduleList;

	public List<MeetingScheduleModel> getMeetingScheduleList() {
		return meetingScheduleList;
	}

	public void setMeetingScheduleList(List<MeetingScheduleModel> meetingScheduleList) {
		this.meetingScheduleList = meetingScheduleList;
	}

	@Override
	public String toString() {
		return "MeetingScheduleListResponse [meetingScheduleList=" + meetingScheduleList + "]";
	}

}
