package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;

public class OrderDetailsListAndProductListResponse extends BaseDomain{

	private List<OrderDetails> orderDetailList;
	private List<OrderProductDetails> orderProductDetailList;
	public List<OrderDetails> getOrderDetailList() {
		return orderDetailList;
	}
	public void setOrderDetailList(List<OrderDetails> orderDetailList) {
		this.orderDetailList = orderDetailList;
	}
	public List<OrderProductDetails> getOrderProductDetailList() {
		return orderProductDetailList;
	}
	public void setOrderProductDetailList(List<OrderProductDetails> orderProductDetailList) {
		this.orderProductDetailList = orderProductDetailList;
	}
	@Override
	public String toString() {
		return "OrderDetailsListAndProductListResponse [orderDetailList=" + orderDetailList
				+ ", orderProductDetailList=" + orderProductDetailList + "]";
	}
	
	
}
