package com.bluesquare.rc.rest.models;

public class DBSnapshotResponse extends BaseDomain {
	
	
	private long totalNoOfOrderReceived; // Total number of order received from Gatekeeper to delivered
	private long totalNoOfOrderDelivered;
	private double totalCollectionAmount;
	private long totalNoOfReplacmentOrderDelivered;
	private long totalReturnOrder;
	
	public long getTotalNoOfOrderReceived() {
		return totalNoOfOrderReceived;
	}
	public void setTotalNoOfOrderReceived(long totalNoOfOrderReceived) {
		this.totalNoOfOrderReceived = totalNoOfOrderReceived;
	}
	public long getTotalNoOfOrderDelivered() {
		return totalNoOfOrderDelivered;
	}
	public void setTotalNoOfOrderDelivered(long totalNoOfOrderDelivered) {
		this.totalNoOfOrderDelivered = totalNoOfOrderDelivered;
	}
	public double getTotalCollectionAmount() {
		return totalCollectionAmount;
	}
	public void setTotalCollectionAmount(double totalCollectionAmount) {
		this.totalCollectionAmount = totalCollectionAmount;
	}
	public long getTotalNoOfReplacmentOrderDelivered() {
		return totalNoOfReplacmentOrderDelivered;
	}
	public void setTotalNoOfReplacmentOrderDelivered(long totalNoOfReplacmentOrderDelivered) {
		this.totalNoOfReplacmentOrderDelivered = totalNoOfReplacmentOrderDelivered;
	}
	public long getTotalReturnOrder() {
		return totalReturnOrder;
	}
	public void setTotalReturnOrder(long totalReturnOrder) {
		this.totalReturnOrder = totalReturnOrder;
	}
	@Override
	public String toString() {
		return "DBSnapshotResponse [totalNoOfOrderReceived=" + totalNoOfOrderReceived + ", totalNoOfOrderDelivered="
				+ totalNoOfOrderDelivered + ", totalCollectionAmount=" + totalCollectionAmount
				+ ", totalNoOfReplacmentOrderDelivered=" + totalNoOfReplacmentOrderDelivered + ", totalReturnOrder="
				+ totalReturnOrder + "]";
	}
	
	
	
	

}
