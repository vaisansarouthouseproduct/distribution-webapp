package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.responseEntities.BusinessNameModel;

public class BusinessNameResponse extends BaseDomain {

	private BusinessNameModel businessName;

	public BusinessNameModel getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessNameModel businessName) {
		this.businessName = businessName;
	}

	@Override
	public String toString() {
		return "BusinessNameResponse [businessName=" + businessName + "]";
	}
	
	
}
