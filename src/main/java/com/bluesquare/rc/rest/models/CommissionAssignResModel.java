package com.bluesquare.rc.rest.models;

public class CommissionAssignResModel {

	private long commissionAssignId;

	private TargetTypeModel targetType;

	private String targetPeriod;

	private String commissionAssignName;

	public CommissionAssignResModel(long commissionAssignId, TargetTypeModel targetType, String targetPeriod,
			String commissionAssignName) {
		super();
		this.commissionAssignId = commissionAssignId;
		this.targetType = targetType;
		this.targetPeriod = targetPeriod;
		this.commissionAssignName = commissionAssignName;
	}

	public long getCommissionAssignId() {
		return commissionAssignId;
	}

	public void setCommissionAssignId(long commissionAssignId) {
		this.commissionAssignId = commissionAssignId;
	}

	public TargetTypeModel getTargetType() {
		return targetType;
	}

	public void setTargetType(TargetTypeModel targetType) {
		this.targetType = targetType;
	}

	public String getTargetPeriod() {
		return targetPeriod;
	}

	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}

	public String getCommissionAssignName() {
		return commissionAssignName;
	}

	public void setCommissionAssignName(String commissionAssignName) {
		this.commissionAssignName = commissionAssignName;
	}

	@Override
	public String toString() {
		return "CommissionAssignResModel [commissionAssignId=" + commissionAssignId + ", targetType=" + targetType
				+ ", targetPeriod=" + targetPeriod + ", commissionAssignName=" + commissionAssignName + "]";
	}

}
