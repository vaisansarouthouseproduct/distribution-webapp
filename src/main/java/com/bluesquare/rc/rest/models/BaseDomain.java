package com.bluesquare.rc.rest.models;

public  class BaseDomain {

	public  String status;
	
	public String errorMsg;
	
	public boolean success;
	
	public String msg;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "BaseDomain [status=" + status + ", errorMsg=" + errorMsg + ", success=" + success + ", msg=" + msg
				+ "]";
	}

	
	
}
