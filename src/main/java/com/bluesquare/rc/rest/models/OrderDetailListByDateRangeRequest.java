package com.bluesquare.rc.rest.models;

import java.util.Date;

public class OrderDetailListByDateRangeRequest {
	
	private String businessNameId;
	
	private String fromDate;
	private String toDate;		// RequestModel to Send shopName And Date range
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "OrderDetailListByDateRangeRequest [businessNameId=" + businessNameId + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + "]";
	}
	
	
	
	
	
	
	
	

}
