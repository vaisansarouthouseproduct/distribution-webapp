package com.bluesquare.rc.rest.models;

public class PaymentListRequest {

	public long employeeId;
	public String payStatus;
	public String range;
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	@Override
	public String toString() {
		return "PaymentListRequest [employeeId=" + employeeId + ", payStatus=" + payStatus + ", range=" + range + "]";
	}
	
	
}
