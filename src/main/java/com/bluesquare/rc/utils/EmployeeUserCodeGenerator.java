package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.EmployeeDetails;

@Component
public class EmployeeUserCodeGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public EmployeeUserCodeGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public EmployeeUserCodeGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	


	public String generateEmployeeUserCode() {

        try {
        	
        	String prefix = "BS_";

        	String hql="select userCode from EmployeeDetails where  "
        			+ " employee.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by employeeDetailsId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedUserCode = prefix + new Long(1).toString();
                System.out.println("generate user code : " + generatedUserCode);
                
                return generatedUserCode;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(prefix.length()));
        		
        		id=max+1;
     	        String generatedUserCode = prefix + new Long(id).toString();
     	        System.out.println("generate user code : " + generatedUserCode);
     	        return generatedUserCode;	
        	}
        	
            
        } catch (Exception e) {
            System.out.println("generate user code Error : "+e.toString());        }

        return null;
	}

}
