package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;

@Component
public class BusinessNameIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public BusinessNameIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public BusinessNameIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
		
	public String generateBusinessNameId() {

        try {
        	String prefix="BUS";
        	String hql="select businessNameId from BusinessName where company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by businessNamePkId desc";
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(3));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("generateBusinessNameId : " + generatedId);
     	        return generatedId;	
        	}
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<BusinessName> list=(List<BusinessName>)query.list();
        	if(list.isEmpty())
        	{
        		String generatedId = prefix+new Long(1).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(BusinessName businessName : list)
        	{
        		ids[i]=Long.parseLong(businessName.getBusinessNameId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
            
            String generatedId = prefix+new Long(id).toString();
            System.out.println("generateBusinessNameId : " + generatedId);
            
            return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("generateBusinessNameId Error : "+e.toString());        }

        return null;
	}

}
