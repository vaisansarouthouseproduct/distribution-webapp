package com.bluesquare.rc.utils;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {
	
	@Autowired
	JavaMailSender mailSender;

	@Autowired
	SessionFactory sessionFactory;

	public EmailSender(JavaMailSender mailSender, SessionFactory sessionFactory) {
		super();
		this.mailSender = mailSender;
		this.sessionFactory = sessionFactory;
	}
	
	/*public boolean SendSimpleMailMessage(String mailBody,String to,String subject){
		
		MimeMessage msg = new MimeMessage(sessionFactory.getCurrentSession());
		//SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
		try {
			simpleMailMessage.setTo(to);
			simpleMailMessage.setSubject(subject);
			simpleMailMessage.setText(mailBody);
			msg.setRecipients(Message.RecipientType.TO, to);
			//this.mailSender.send(simpleMailMessage);
			System.out.println("Mail sent");
			return true;
		} catch (Exception e) {
			System.out.println("Error sending Mail: "+e.toString());
			return false;
		}	
		
	}*/
	public boolean sendEmail( String subject, String body,  String to,  boolean isHtmlMail) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
           
            helper.setTo(to);
            helper.setSubject(subject);
            if (isHtmlMail) {
                helper.setText("<html><body>" + body + "</html></body>", true);
            } else {
                helper.setText(body);
            }
            mailSender.send(mimeMessage);
            System.out.println("Email sending complete.");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
	
	public boolean sendEmail( String subject, String body,  String to,  boolean isHtmlMail, File dFile,String fileName)
    {
        
       try {
           MimeMessage mimeMessage = mailSender.createMimeMessage();
           MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
         
           helper.setTo(to);
           helper.setSubject(subject);
           FileSystemResource file = new FileSystemResource(dFile);
           helper.addAttachment(fileName, file);
       
           if (isHtmlMail) {
               helper.setText("<html><body>" + body + "</html></body>", true);
           } else {
               helper.setText(body);
           }
           mailSender.send(mimeMessage);
           System.out.println("Email sending complete.");
           return true;
       } catch (Exception e) {
           e.printStackTrace();
           return false;
       }
   }
 
}
