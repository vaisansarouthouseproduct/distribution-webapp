package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.SupplierOrder;

@Component
public class RouteIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public RouteIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "ROUTE";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	/*String hql="select routeGenId from Route where status=false and company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by routeId desc";*/
        	
        	String hql="select routeGenId from Route where company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by routeId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("routeIdGenerator : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(5));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("routeIdGenerator : " + generatedId);
     	        return generatedId;	
        	}
            
        } catch (Exception e) {
            System.out.println("routeIdGenerator Error : "+e.toString());        
        }

        return null;
	}

}
