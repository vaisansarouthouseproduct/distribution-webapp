package com.bluesquare.rc.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	public static Calendar getCalendarFromString(String strDate) {
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = (Date) formatter.parse(strDate);
			calendar.setTime(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			;
		}

		return calendar;
	}

	public static String getStringFromCalendar(Calendar calendar) {
		String strDate = "1970-01-01";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			strDate = formatter.format(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			;
		}
		return strDate;
	}

	public static String getDateTimeStringFromCalendar(Calendar calendar) {
		String strDate = "1970-01-01";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			strDate = formatter.format(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			;
		}
		return strDate;
	}

	public static String getStringDateTimeFromCalendar(Calendar calendar) {
		String strDate = "1970-01-01";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			strDate = formatter.format(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			;
		}
		return strDate;
	}

	public static String formatDateAsIST(String date) {
		String strDate = "";
		Date date1;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat istFormatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date1 = formatter.parse(date);
			strDate = istFormatter.format(date1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			;
		}
		return strDate;
	}

	public static String getCurrentMonthLastDateInString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance(); // this takes current date
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

		return dateFormat.format(c.getTime());
	}

	public static String getCurrentMonthStartDateInString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance(); // this takes current date
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

		return dateFormat.format(c.getTime());
	}
	
	public static Date getCurrentMonthLastDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}

	public static Date getCurrentMonthStartDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}
	
	public static Date getNextMonthLastDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}

	public static Date getNextMonthStartDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}
	
	public static Date getPreviousMonthLastDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}

	public static Date getPreviousMonthStartDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		return c.getTime();
	}

	public static Date getCurrentYearLastDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_YEAR, c.getActualMaximum(Calendar.DAY_OF_YEAR));

		return c.getTime();
	}

	public static Date getCurrentYearStartDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.set(Calendar.DAY_OF_YEAR, c.getActualMinimum(Calendar.DAY_OF_YEAR));

		return c.getTime();
	}
	
	public static Date getNextYearLastDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.YEAR, 1);
		c.set(Calendar.DAY_OF_YEAR, c.getActualMaximum(Calendar.DAY_OF_YEAR));

		return c.getTime();
	}

	public static Date getNextYearStartDate() {
		Calendar c = Calendar.getInstance(); // this takes current date
		c.add(Calendar.YEAR, 1);
		c.set(Calendar.DAY_OF_YEAR, c.getActualMinimum(Calendar.DAY_OF_YEAR));

		return c.getTime();
	}

	public static String getStringTimeFromDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		String strDate = dateFormat.format(date);
		return strDate;
	}

	public static int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	public static String fetchMonthStartDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar startDateCal = Calendar.getInstance();
		startDateCal.setTime(date);
		startDateCal.set(Calendar.DAY_OF_MONTH, startDateCal.getActualMinimum(Calendar.DAY_OF_MONTH));
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(date);
		endDateCal.set(Calendar.DAY_OF_MONTH, endDateCal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return sdf.format(startDateCal.getTime());
	}

	public static Date getMonthStartDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH);
		Calendar startDateCal = Calendar.getInstance();
		startDateCal.setTime(date);
		startDateCal.set(Calendar.DAY_OF_MONTH, startDateCal.getActualMinimum(Calendar.DAY_OF_MONTH));
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(date);
		endDateCal.set(Calendar.DAY_OF_MONTH, endDateCal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return startDateCal.getTime();
	}

	public static String fetchMonthEndDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(date);
		endDateCal.set(Calendar.DAY_OF_MONTH, endDateCal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return sdf.format(endDateCal.getTime());
	}

	public static Date getMonthEndDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH);
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(date);
		endDateCal.set(Calendar.DAY_OF_MONTH, endDateCal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return endDateCal.getTime();
	}

	public static String fetchYearStartDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar startDateCal = Calendar.getInstance();
		startDateCal.setTime(date);
		Integer year = startDateCal.get(Calendar.YEAR);
		try {
			date = new SimpleDateFormat("MM/dd/yyyy").parse("01/01/" + year);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			;
		}
		startDateCal.setTime(date);
		return sdf.format(startDateCal.getTime());
	}

	public static String fetchYearEndDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar calendar = Calendar.getInstance();
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(date);
		Integer year = endDateCal.get(Calendar.YEAR);
		try {
			if (calendar.get(Calendar.YEAR) > endDateCal.get(Calendar.YEAR)) {
				date = new SimpleDateFormat("dd/MM/yyyy").parse("31/12/" + year);
			} else {
				date = calendar.getTime();
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			;
		}
		endDateCal.setTime(date);
		return sdf.format(endDateCal.getTime());
	}

	public static Date fetchPaymentMonthStartDate(String month, int year) {
		int intMonth = -1;
		Calendar currentCal = Calendar.getInstance();
		switch (month) {
		case "JANUARY":
			intMonth = 1;
			break;
		case "FEBRUARY":
			intMonth = 2;
			break;
		case "MARCH":
			intMonth = 3;
			break;
		case "APRIL":
			intMonth = 4;
			break;
		case "MAY":
			intMonth = 5;
			break;
		case "JUNE":
			intMonth = 6;
			break;
		case "JULY":
			intMonth = 7;
			break;
		case "AUGUST":
			intMonth = 8;
			break;
		case "SEPTEMBER":
			intMonth = 9;
			break;
		case "OCTOBER":
			intMonth = 10;
			break;
		case "NOVEMBER":
			intMonth = 11;
			break;
		case "DECEMBER":
			intMonth = 0;
			break;
		}
		Date currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		currentCal.setTime(currentMonthStartDate);
		currentCal.set(Calendar.MONTH, intMonth);
		currentCal.set(Calendar.YEAR, year);
		if (intMonth == 0) {
			currentCal.add(Calendar.YEAR, 1);
		}
		currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		return currentMonthStartDate;
	}

	public static Date fetchPaymentMonthEndDate(String month, int year) {
		int intMonth = -1;
		Calendar currentCal = Calendar.getInstance();
		switch (month) {
		case "JANUARY":
			intMonth = 1;
			break;
		case "FEBRUARY":
			intMonth = 2;
			break;
		case "MARCH":
			intMonth = 3;
			break;
		case "APRIL":
			intMonth = 4;
			break;
		case "MAY":
			intMonth = 5;
			break;
		case "JUNE":
			intMonth = 6;
			break;
		case "JULY":
			intMonth = 7;
			break;
		case "AUGUST":
			intMonth = 8;
			break;
		case "SEPTEMBER":
			intMonth = 9;
			break;
		case "OCTOBER":
			intMonth = 10;
			break;
		case "NOVEMBER":
			intMonth = 11;
			break;
		case "DECEMBER":
			intMonth = 0;
			break;
		}
		Date currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		currentCal.setTime(currentMonthStartDate);
		currentCal.set(Calendar.MONTH, intMonth);
		currentCal.set(Calendar.YEAR, year);
		if (intMonth == 0) {
			currentCal.add(Calendar.YEAR, 1);
		}
		currentMonthStartDate = getMonthEndDate(currentCal.getTime());
		return currentMonthStartDate;
	}

	public static Date fetchPurchaseMonthStartDate(String month, int year) {
		int intMonth = -1;
		Calendar currentCal = Calendar.getInstance();
		switch (month) {
		case "JANUARY":
			intMonth = 0;
			break;
		case "FEBRUARY":
			intMonth = 1;
			break;
		case "MARCH":
			intMonth = 2;
			break;
		case "APRIL":
			intMonth = 3;
			break;
		case "MAY":
			intMonth = 4;
			break;
		case "JUNE":
			intMonth = 5;
			break;
		case "JULY":
			intMonth = 6;
			break;
		case "AUGUST":
			intMonth = 7;
			break;
		case "SEPTEMBER":
			intMonth = 8;
			break;
		case "OCTOBER":
			intMonth = 9;
			break;
		case "NOVEMBER":
			intMonth = 10;
			break;
		case "DECEMBER":
			intMonth = 11;
			break;
		}
		Date currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		currentCal.setTime(currentMonthStartDate);
		currentCal.set(Calendar.MONTH, intMonth);
		currentCal.set(Calendar.YEAR, year);
		if (intMonth == 0) {
			currentCal.add(Calendar.YEAR, 1);
		}
		currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		return currentMonthStartDate;
	}

	public static Date fetchPurchaseMonthEndDate(String month, int year) {
		int intMonth = -1;
		Calendar currentCal = Calendar.getInstance();
		switch (month) {
		case "JANUARY":
			intMonth = 0;
			break;
		case "FEBRUARY":
			intMonth = 1;
			break;
		case "MARCH":
			intMonth = 2;
			break;
		case "APRIL":
			intMonth = 3;
			break;
		case "MAY":
			intMonth = 4;
			break;
		case "JUNE":
			intMonth = 5;
			break;
		case "JULY":
			intMonth = 6;
			break;
		case "AUGUST":
			intMonth = 7;
			break;
		case "SEPTEMBER":
			intMonth = 8;
			break;
		case "OCTOBER":
			intMonth = 9;
			break;
		case "NOVEMBER":
			intMonth = 10;
			break;
		case "DECEMBER":
			intMonth = 11;
			break;
		}
		Date currentMonthStartDate = getMonthStartDate(currentCal.getTime());
		currentCal.setTime(currentMonthStartDate);
		currentCal.set(Calendar.MONTH, intMonth);
		currentCal.set(Calendar.YEAR, year);
		if (intMonth == 0) {
			currentCal.add(Calendar.YEAR, 1);
		}
		currentMonthStartDate = getMonthEndDate(currentCal.getTime());
		return currentMonthStartDate;
	}

	public static String fetchMonth(int month) {
		String stringMonth = "Invalid Month";
		switch (month) {
		case 0:
			stringMonth = "JANUARY";
			break;
		case 1:
			stringMonth = "FEBRUARY";
			break;
		case 2:
			stringMonth = "MARCH";
			break;
		case 3:
			stringMonth = "APRIL";
			break;
		case 4:
			stringMonth = "MAY";
			break;
		case 5:
			stringMonth = "JUNE";
			break;
		case 6:
			stringMonth = "JULY";
			break;
		case 7:
			stringMonth = "AUGUST";
			break;
		case 8:
			stringMonth = "SEPTEMBER";
			break;
		case 9:
			stringMonth = "OCTOBER";
			break;
		case 10:
			stringMonth = "NOVEMBER";
			break;
		case 11:
			stringMonth = "DECEMBER";
			break;
		}
		return stringMonth;
	}

	public static int fetchMonth(String month) {
		Integer intMonth = -1;
		switch (month) {
		case "JANUARY":
			intMonth = 0;
			break;
		case "FEBRUARY":
			intMonth = 1;
			break;
		case "MARCH":
			intMonth = 2;
			break;
		case "APRIL":
			intMonth = 3;
			break;
		case "MAY":
			intMonth = 4;
			break;
		case "JUNE":
			intMonth = 5;
			break;
		case "JULY":
			intMonth = 6;
			break;
		case "AUGUST":
			intMonth = 7;
			break;
		case "SEPTEMBER":
			intMonth = 8;
			break;
		case "OCTOBER":
			intMonth = 9;
			break;
		case "NOVEMBER":
			intMonth = 10;
			break;
		case "DECEMBER":
			intMonth = 11;
			break;
		}
		return intMonth;
	}

	public static String fetchMonthCycle(String startDate, String endDate) {

		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat outputFormat = new SimpleDateFormat("dd-MMMM-yyyy");

		Date date1 = null, date2 = null;
		try {
			date1 = inputFormat.parse(startDate);
			date2 = inputFormat.parse(endDate);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		startDate = outputFormat.format(date1);
		endDate = outputFormat.format(date2);

		String msg = startDate + " to " + endDate;
		return msg;

	}

	public static String fetchMonth() {

		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];

		return month;

	}

	public static String getLastMonthFirstDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		return dateFormat.format(aCalendar.getTime());
	}

	public static String getLastMonthLastDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return dateFormat.format(aCalendar.getTime());
	}

	public static String getLast3MonthFirstDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -3);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		return dateFormat.format(aCalendar.getTime());
	}

	public static String getLast3MonthLastDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return dateFormat.format(aCalendar.getTime());
	}

	public static String getLast6MonthFirstDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -6);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		return dateFormat.format(aCalendar.getTime());
	}

	public static String getLast6MonthLastDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return dateFormat.format(aCalendar.getTime());
	}

	public static Date getCurrentWeekStartDate() {
		Calendar mCalendar = Calendar.getInstance();
		Date date = new Date();
		mCalendar.setTime(date);

		// 1 = Sunday, 2 = Monday, etc.
		int day_of_week = mCalendar.get(Calendar.DAY_OF_WEEK);

		int monday_offset;
		if (day_of_week == 1) {
			monday_offset = -6;
		} else
			monday_offset = (2 - day_of_week); // need to minus back
		mCalendar.add(Calendar.DAY_OF_YEAR, monday_offset);

		Date mDateMonday = mCalendar.getTime();

		// return 6 the next days of current day (object cal save current day)
		/*
		 * mCalendar.add(Calendar.DAY_OF_YEAR, 6); Date mDateSunday =
		 * mCalendar.getTime()
		 */;

		return mDateMonday;
	}

	public static Date getCurrentWeekEndDate() {
		Calendar mCalendar = Calendar.getInstance();
		Date date = new Date();
		mCalendar.setTime(date);

		// 1 = Sunday, 2 = Monday, etc.
		int day_of_week = mCalendar.get(Calendar.DAY_OF_WEEK);

		int monday_offset;
		if (day_of_week == 1) {
			monday_offset = -6;
		} else
			monday_offset = (2 - day_of_week); // need to minus back
		mCalendar.add(Calendar.DAY_OF_YEAR, monday_offset);

		// Date mDateMonday = mCalendar.getTime();

		// return 6 the next days of current day (object cal save current day)
		mCalendar.add(Calendar.DAY_OF_YEAR, 6);
		Date mDateSunday = mCalendar.getTime();

		return mDateSunday;

	}
	
	public static Date getNextWeekStartDate() {
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.add(Calendar.DAY_OF_WEEK, 7);

		// 1 = Sunday, 2 = Monday, etc.
		int day_of_week = mCalendar.get(Calendar.DAY_OF_WEEK);

		int monday_offset;
		if (day_of_week == 1) {
			monday_offset = -6;
		} else
			monday_offset = (2 - day_of_week); // need to minus back
		mCalendar.add(Calendar.DAY_OF_YEAR, monday_offset);

		Date mDateMonday = mCalendar.getTime();

		// return 6 the next days of current day (object cal save current day)
		/*
		 * mCalendar.add(Calendar.DAY_OF_YEAR, 6); Date mDateSunday =
		 * mCalendar.getTime()
		 */;

		return mDateMonday;
	}

	public static Date getNextWeekEndDate() {
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.add(Calendar.DAY_OF_WEEK, 7);

		// 1 = Sunday, 2 = Monday, etc.
		int day_of_week = mCalendar.get(Calendar.DAY_OF_WEEK);

		int monday_offset;
		if (day_of_week == 1) {
			monday_offset = -6;
		} else
			monday_offset = (2 - day_of_week); // need to minus back
		mCalendar.add(Calendar.DAY_OF_YEAR, monday_offset);

		// Date mDateMonday = mCalendar.getTime();

		// return 6 the next days of current day (object cal save current day)
		mCalendar.add(Calendar.DAY_OF_YEAR, 6);
		Date mDateSunday = mCalendar.getTime();

		return mDateSunday;

	}
	
	public static Date getPreviousWeekStartDate() {
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.add(Calendar.DAY_OF_WEEK, -7);

		return mCalendar.getTime();
	}

	public static Date getPreviousWeekEndDate() {
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.add(Calendar.DAY_OF_WEEK, -1);

		return mCalendar.getTime();
	}
	
	public static void main(String[] args) {
		System.out.println(getPreviousMonthStartDate()+" - "+getPreviousMonthLastDate());
	}
}
