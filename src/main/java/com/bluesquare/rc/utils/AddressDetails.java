/**
 * 
 */
package com.bluesquare.rc.utils;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aNKIT
 *
 */
public class AddressDetails {

	public List<Results> results;
	public String status;

	static class Results {
		public List<AddressComponents> address_components;
		public String formatted_address;
		public Geometry geometry;
		public String place_id;
		public List<String> types;

		public List<AddressComponents> getAddress_components() {
			return address_components;
		}

		public void setAddress_components(List<AddressComponents> address_components) {
			this.address_components = address_components;
		}

		public String getFormatted_address() {
			return formatted_address;
		}

		public void setFormatted_address(String formatted_address) {
			this.formatted_address = formatted_address;
		}

		public Geometry getGeometry() {
			return geometry;
		}

		public void setGeometry(Geometry geometry) {
			this.geometry = geometry;
		}

		public String getPlace_id() {
			return place_id;
		}

		public void setPlace_id(String place_id) {
			this.place_id = place_id;
		}

		public List<String> getTypes() {
			return types;
		}

		public void setTypes(List<String> types) {
			this.types = types;
		}

		@Override
		public String toString() {
			return "Results [address_components=" + address_components + ", formatted_address=" + formatted_address
					+ ", geometry=" + geometry + ", place_id=" + place_id + ", types=" + types + "]";
		}

	}

	static class AddressComponents {
		public String long_name;
		public String short_name;
		public List<String> types;

		public String getLong_name() {
			return long_name;
		}

		public void setLong_name(String long_name) {
			this.long_name = long_name;
		}

		public String getShort_name() {
			return short_name;
		}

		public void setShort_name(String short_name) {
			this.short_name = short_name;
		}

		public List<String> getTypes() {
			return types;
		}

		public void setTypes(List<String> types) {
			this.types = types;
		}

		@Override
		public String toString() {
			return "AddressComponents [long_name=" + long_name + ", short_name=" + short_name + ", types=" + types
					+ "]";
		}

	}

	static class Geometry {
		public Bounds bounds;
		public Bounds viewport;
		public latlng location;
		public String location_type;

		public Bounds getBounds() {
			return bounds;
		}

		public void setBounds(Bounds bounds) {
			this.bounds = bounds;
		}

		public Bounds getViewport() {
			return viewport;
		}

		public void setViewport(Bounds viewport) {
			this.viewport = viewport;
		}

		public latlng getLocation() {
			return location;
		}

		public void setLocation(latlng location) {
			this.location = location;
		}

		public String getLocation_type() {
			return location_type;
		}

		public void setLocation_type(String location_type) {
			this.location_type = location_type;
		}

		@Override
		public String toString() {
			return "Geometry [bounds=" + bounds + ", viewport=" + viewport + ", location=" + location
					+ ", location_type=" + location_type + "]";
		}

	}

	static class latlng {
		public String lat;
		public String lng;

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public String getLng() {
			return lng;
		}

		public void setLng(String lng) {
			this.lng = lng;
		}

		@Override
		public String toString() {
			return "latlng [lat=" + lat + ", lng=" + lng + "]";
		}

	}

	static class Bounds {
		public latlng northeast;
		public latlng southwest;

		public latlng getNortheast() {
			return northeast;
		}

		public void setNortheast(latlng northeast) {
			this.northeast = northeast;
		}

		public latlng getSouthwest() {
			return southwest;
		}

		public void setSouthwest(latlng southwest) {
			this.southwest = southwest;
		}

		@Override
		public String toString() {
			return "Bounds [northeast=" + northeast + ", southwest=" + southwest + "]";
		}

	}

	public List<Results> getResults() {
		return results;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AddressDetails [results=" + results + ", status=" + status + "]";
	}
	
}
