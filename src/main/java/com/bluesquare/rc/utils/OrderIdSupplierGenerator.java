package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.SupplierOrder;

@Component
public class OrderIdSupplierGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public OrderIdSupplierGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "ORDS";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	
        	/*String hql="select supplierOrderId from SupplierOrder where status=true and company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by supplierOrderPkId desc";*/
        	String hql="select supplierOrderId from SupplierOrder where company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by supplierOrderPkId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("OrderIdSupplierGenerator : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(4));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("OrderIdSupplierGenerator : " + generatedId);
     	        return generatedId;	
        	}
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<SupplierOrder> list=(List<SupplierOrder>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(SupplierOrder supplierOrder : list)
        	{
        		ids[i]=Long.parseLong(supplierOrder.getSupplierOrderId().substring(4));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("OrderIdSupplierGenerator Id: " + generatedId);
                return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("OrderIdSupplierGenerator Error : "+e.toString());        
            }

        return null;
	}

}
