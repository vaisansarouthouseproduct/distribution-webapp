package com.bluesquare.rc.utils;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Inventory;

@Component
public class DamageRecoveryIdGenerator {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;

	public DamageRecoveryIdGenerator() {
		// TODO Auto-generated constructor stub
	}

	public DamageRecoveryIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public String generateDamageRecoveryDetailsId() {

		try {
			String prefix = "DR";
			
			String hql = "select damageRecoveryDetailsId from DamageRecoveryDetails where "
					+ "supplier.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
					" order by damageRecoveryDetailsPkId desc";

			Query query=sessionFactory.getCurrentSession().createQuery(hql); 
			query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("DamageRecoveryDetailsIdGenerator : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(2));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("DamageRecoveryDetailsIdGenerator : " + generatedId);
     	        return generatedId;	
        	}
			
			/*Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List<String> damageRecoveryDetailsIdList = (List<String>) query.list();

			long id;

			if (damageRecoveryDetailsIdList.isEmpty()) {
				id = 10001;
			} else {
				String lastNo = damageRecoveryDetailsIdList.get(damageRecoveryDetailsIdList.size() - 1);
				String[] part = lastNo.split("DR");
				id = Long.parseLong(part[1]);
				id++;
			}
			
			if(damageRecoveryDetailsIdList.isEmpty())
        	{
        		String generatedId =prefix + new Long(1).toString();
                System.out.println("DamageRecoveryDetailsIdGenerator : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[damageRecoveryDetailsIdList.size()]; 
        	int i=0;
        	for(String damageRecoveryDetailsId : damageRecoveryDetailsIdList)
        	{
				String[] part = damageRecoveryDetailsId.split("DR");
				long idtemp = Long.parseLong(part[1]);
        		
        		ids[i]=idtemp;
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;

			String generatedId = prefix + new Long(id).toString();
			System.out.println("DamageRecoveryDetailsIdGenerator Id: " + generatedId);
			return generatedId;*/

		} catch (Exception e) {
			System.out.println("DamageRecoveryDetailsIdGenerator Error : " + e.toString());
		}

		return null;
	}

}
