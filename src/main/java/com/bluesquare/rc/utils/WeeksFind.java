package com.bluesquare.rc.utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class WeeksFind {
	public static void main(String[] args) throws Exception {
		/*SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat dateFormat2=new SimpleDateFormat("dd,MMM");
		List<String[]> weekList = findWeekInYear(2016);
		for (String[] weeks : weekList) {
			System.out.println(dateFormat2.format(dateFormat.parse(weeks[0])) + "-" + dateFormat2.format(dateFormat.parse(weeks[1])));
		}*/
		//weekFinderByYear(1995);
		
		String fileName="E:\\file.xls";
		WritableWorkbook workbook=Workbook.createWorkbook(new File(fileName));
		WritableSheet sheet1=workbook.createSheet("Sheet1", 0);
		WritableSheet sheet2=workbook.createSheet("Sheet2", 1);
		
		//Adding A Label
		Label productName=new Label(0, 0, "productName");
		sheet1.addCell(productName);
		
		Label productCode=new Label(1,0, "productCode");
		sheet1.addCell(productCode);
		
		Label brandId=new Label(2, 0, "brandId");
		sheet1.addCell(brandId);
		
		Label categoryId=new Label(3, 0, "categoryId");
		sheet1.addCell(categoryId);
		
		Label mrp=new Label(4, 0, "mrp");
		sheet1.addCell(mrp);
		
		Label threshold=new Label(5, 0, "threshold");
		sheet1.addCell(threshold);
		
		Label BrandId=new Label(0, 0, "Brand Id");
		sheet2.addCell(BrandId);
		
		Label BrandName=new Label(1, 0, "Brand Name");
		sheet2.addCell(BrandName);
		
		for(int i=0; i<10; i++){
			Label BrandIdRunning=new Label(0, (i+1), "Brand Id "+i);
			sheet2.addCell(BrandIdRunning);
			
			Label BrandNameRunning=new Label(1, (i+1), "Brand Name "+i);
			sheet2.addCell(BrandNameRunning);
		}
		
		workbook.write();
		workbook.close();
	}
	 
	public static List<String[]> findWeekInYear(int year) throws Exception {
		List<String[]> weekList = new ArrayList<>();

		DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("d-M-uuuu");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
		LocalDate weekFirstDay = LocalDate.of(year, 1, 1);
		LocalDate ld = LocalDate.parse(
				dateFormat2.format(dateFormat.parse(weekFirstDay.with(DayOfWeek.MONDAY).toString())), DATE_FORMAT);

		// System.out.println(\weekFirstDay.with(DayOfWeek.MONDAY));

		LocalDate weekLastDay = LocalDate.of(year, 12, 31);
		LocalDate lastDayOfMonth = LocalDate.parse(
				dateFormat2.format(dateFormat.parse(weekLastDay.with(DayOfWeek.SUNDAY).toString())), DATE_FORMAT);

		// System.out.println(weekLastDay.with(DayOfWeek.SUNDAY));

		while (!ld.isAfter(lastDayOfMonth)) {
			// find last working day of week beginning on firstWorkingDay
			LocalDate lastWorkingDay = ld.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
			if (lastWorkingDay.isAfter(lastDayOfMonth)) {
				// end work week at end of month
				lastWorkingDay = lastDayOfMonth;
			}
			// System.out.println(ld.format(DATE_FORMAT) + " -> " +
			// lastWorkingDay.format(DATE_FORMAT));
			weekList.add(new String[] { ld.format(DATE_FORMAT), lastWorkingDay.format(DATE_FORMAT) });
			// find beginning of next work week
			ld = ld.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		}
		return weekList;
	}
	 
	public static void weekFinderByYear(int year) throws ParseException{
       
		//start date set by week
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendarStart=Calendar.getInstance();
        calendarStart.setTime(dateFormat.parse(year+"-01-01"));        
        if(calendarStart.get(Calendar.DAY_OF_WEEK)!=2){
        	 calendarStart.setTime(dateFormat.parse((year-1)+"-12-31"));
        	 calendarStart.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        }        
        
        System.out.println(calendarStart.getTime());
        
        //end date set by week
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(dateFormat.parse(year+"-12-31"));        
        if(calendarEnd.get(Calendar.DAY_OF_WEEK)!=1){
        	calendarEnd.setTime(dateFormat.parse((year+1)+"-01-01"));
        	if(calendarEnd.get(Calendar.DAY_OF_WEEK)!=1){
        	  calendarEnd.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
              calendarEnd.add(Calendar.DAY_OF_MONTH, 1);
        	}
        }                
        System.out.println(calendarEnd.getTime());
        
        while(calendarStart.before(calendarEnd) || calendarStart.equals(calendarEnd)){

        	
        	String startDate=makeDateToString(calendarStart.getTime(),"dd-MMM,yyyy");
        	
        	calendarStart.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        	calendarStart.add(Calendar.DAY_OF_MONTH, 1);
        	
        	String endDate=makeDateToString(calendarStart.getTime(),"dd-MMM,yyyy");
        	
        	System.out.println(startDate+" - "+endDate);
        	calendarStart.add(Calendar.DAY_OF_MONTH, 1);
        }
        // Now get the first day of week.

    }
	
public static List<String[]> weekFinderByYearForWebApp(long year) throws ParseException{
	    
		List<String[]> weekDates=new ArrayList<>();
		
		//start date set by week
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendarStart=Calendar.getInstance();
        calendarStart.setTime(dateFormat.parse(year+"-01-01"));        
        if(calendarStart.get(Calendar.DAY_OF_WEEK)!=2){
        	 calendarStart.setTime(dateFormat.parse((year-1)+"-12-31"));
        	 calendarStart.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        }        
        
        //System.out.println(calendarStart.getTime());
        
        //end date set by week
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(dateFormat.parse(year+"-12-31"));        
        if(calendarEnd.get(Calendar.DAY_OF_WEEK)!=1){
        	calendarEnd.setTime(dateFormat.parse((year+1)+"-01-01"));
        	if(calendarEnd.get(Calendar.DAY_OF_WEEK)!=1){
        	  calendarEnd.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
              calendarEnd.add(Calendar.DAY_OF_MONTH, 1);
        	}
        }                
        //System.out.println(calendarEnd.getTime());
        
        while(calendarStart.before(calendarEnd) || calendarStart.equals(calendarEnd)){

        	String startDateShow=makeDateToString(calendarStart.getTime(),"dd-MMM,yyyy");
        	String startDate=makeDateToString(calendarStart.getTime(),"yyyy-MM-dd");
        	
        	calendarStart.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        	calendarStart.add(Calendar.DAY_OF_MONTH, 1);
        	
        	String endDateShow=makeDateToString(calendarStart.getTime(),"dd-MMM,yyyy");
        	String endDate=makeDateToString(calendarStart.getTime(),"yyyy-MM-dd");
        	
        	//System.out.println(startDate+" - "+endDate);
        	weekDates.add(new String[]{startDateShow,startDate,endDateShow,endDate});
        	calendarStart.add(Calendar.DAY_OF_MONTH, 1);
        }
        // Now get the first day of week.

        return weekDates;
    }
	
	 // Making Date to String...
    public static String makeDateToString(Date dateTime, String dateFormatPattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatPattern, Locale.ENGLISH);
        String date = simpleDateFormat.format(dateTime);
        return date;
    }

}
