package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;


@Component
public class ReturnCounterOrderIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public ReturnCounterOrderIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "CRTN";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="select returnCounterOrderGenId from ReturnCounterOrder where employee.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by returnCounterOrderId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("ReturnCounterOrder : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(4));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("ReturnCounterOrder : " + generatedId);
     	        return generatedId;	
        	}
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<ReturnOrderProduct> list=(List<ReturnOrderProduct>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(ReturnOrderProduct returnOrderProduct : list)
        	{
        		ids[i]=Long.parseLong(returnOrderProduct.getReturnOrderProductId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("ReturnOrderProduct Id: " + generatedId);
                return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("ReturnCounterOrder Error : "+e.toString());        }

        return null;
	}

}
