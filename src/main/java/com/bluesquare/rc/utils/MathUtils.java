package com.bluesquare.rc.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MathUtils {
	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static float roundFloat(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.floatValue();
	}

	/*
	 * public static float roundFromFloat(float value, int places) { if (places
	 * < 0) throw new IllegalArgumentException();
	 * 
	 * BigDecimal bd = new BigDecimal(value); bd = bd.setScale(places,
	 * RoundingMode.HALF_UP); return bd.floatValue(); }
	 */

	public static float roundFromFloatMain(float value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		String[] info = (value + "").split("\\.");

		String integralPartStr = info[0];
		String fractionalPartStr = info[1];

		Integer valueAfterRoundingPlace = Integer.parseInt(fractionalPartStr.substring(places));
		if (valueAfterRoundingPlace >= 5) {
			String valueToBeAdded = "0.";
			String decimalFormat = "#.";
			for (int i = 0; i < places - 1; i++) {
				valueToBeAdded += "0";
				decimalFormat += "#";
			}
			valueToBeAdded += "1";
			decimalFormat += "#";
			fractionalPartStr = fractionalPartStr.substring(0, places);
			fractionalPartStr = fractionalPartStr.substring(0, places);
			Double oldValue = Double.parseDouble(integralPartStr + "." + fractionalPartStr);
			Double floatAddition = Double.parseDouble(valueToBeAdded);
			Double newValue = oldValue + floatAddition;
			newValue = Double.parseDouble(new DecimalFormat(decimalFormat).format(newValue));
			return newValue.floatValue();
		} else {
			String valueToBeAdded = "0.";
			String decimalFormat = "#.";
			for (int i = 0; i < places - 1; i++) {
				valueToBeAdded += "0";
				decimalFormat += "#";
			}
			valueToBeAdded += "0";
			decimalFormat += "#";
			fractionalPartStr = fractionalPartStr.substring(0, places);
			Double oldValue = Double.parseDouble(integralPartStr + "." + fractionalPartStr);
			Double floatAddition = Double.parseDouble(valueToBeAdded);
			Double newValue = oldValue + floatAddition;

			newValue = Double.parseDouble(new DecimalFormat(decimalFormat).format(newValue));
			return newValue.floatValue();
		}
	}

	public static float roundFromFloat(float value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		else if (places == 0) {
			return Float.parseFloat(new DecimalFormat("#").format(value));
		} else {
			String stringValue = value + "";
			String[] strInfo = stringValue.split("\\.");
			if (strInfo.length == 1)
				return value;
			int length = strInfo[1].length();

			for (int i = length; i > places; i--) {
				int roundOffPlaces = i - 1;
				value = roundFromFloatMain(value, roundOffPlaces);
				stringValue = value + "";
				strInfo = stringValue.split("\\.");
				int newLength = strInfo[1].length();
				if (newLength <= places) {
					return value;
				} else {
					int diff = roundOffPlaces - newLength;
					if (diff != 0)
						i = i - diff;
				}
			}
			return value;
		}
	}

	public static void main(String args[]) {
		System.out.println(roundFromFloat(100.024446f, 2));
		// double oldValue = 100.02444;
		// double newValue = oldValue+0.00001;
		// System.out.println(newValue);
	}
}
