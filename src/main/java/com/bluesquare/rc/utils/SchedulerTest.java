package com.bluesquare.rc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.service.TargetAssignService;



@Component
@Configuration
@EnableScheduling
public class SchedulerTest {

	

	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	TargetAssignService targetAssignService;

	// second, minute, hour, day of month, month, day(s) of week, year(optional)
	//http://www.baeldung.com/cron-expressions
	//https://dzone.com/articles/running-on-time-with-springs-scheduled-tasks
	
	/*
	
	0 0 6 6 9 ? 2010
	| | | | | |   |
	| | | | | |   +- 2010 only.
	| | | | | +----- any day of the week.
	| | | | +------- 9th month (September).
	| | | +--------- 6th day of the month.
	| | +----------- 6th hour of the day.
	| +------------- Top of the hour (minutes = 0).
	+--------------- Top of the minute (seconds = 0).
	
	*/
	
	//@Scheduled(cron = "05 00 00 * * *")
	
	//daily create daily stock report (12:20 AM every day)
	//@Scheduled(cron = "00 20 00 * * *")
	@Scheduled(cron = "00 52 12 * * *")
	public void dailyCronJob() {
		
		System.out.println("inside dailyCronJob");
		if(productDAO.fetchTodaysDailyStockDetails()==null){
			productDAO.creatFirstRecordOfDailyStockReport();
		}
	}
	// second, minute, hour, day of month, month, day(s) of week
	//daily target create for next targets from regular(12:23 AM every day)
	@Scheduled(cron = "00 23 00 * * *")
	public void dailyTargetCronJob() {		
		System.out.println("inside dailyTargetCronJob");
		targetAssignService.createNextDayTarget(Constants.TARGET_PERIOD_DAILY);
	}
	
	//weekly target create for next targets from regular (12:25 AM every Monday)
	@Scheduled(cron = "00 25 00 * * MON")
	public void weeklyTargetCronJob() {		
		System.out.println("inside weeklyTargetCronJob");
		targetAssignService.createNextDayTarget(Constants.TARGET_PERIOD_WEEKLY);
	}

	//monthly target create for next targets from regular (12:27 AM every 1st day of Month)
	@Scheduled(cron = "00 27 00 1 * *")
	public void monthTargetCronJob() {		
		System.out.println("inside monthlyTargetCronJob");
		targetAssignService.createNextDayTarget(Constants.TARGET_PERIOD_MONTHLY);
	}
						
	//monthly target create for next targets from regular (12:30 AM every 1st month-day of Year)
	/*@Scheduled(cron = "00 30 00 1 1 *")
	public void yearlyTargetCronJob() {		
		System.out.println("inside monthlyTargetCronJob");
		targetAssignService.createNextDayTarget(Constants.TARGET_PERIOD_YEARLY);
	}*/
}
