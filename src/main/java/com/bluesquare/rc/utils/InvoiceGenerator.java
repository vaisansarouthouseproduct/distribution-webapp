package com.bluesquare.rc.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CategoryWiseAmountForBill;
import com.bluesquare.rc.models.InventoryAddedInvoiceModel;
import com.bluesquare.rc.models.ProductListForBill;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



@Component
public class InvoiceGenerator 
{
	private static String FILE = "Invoice.pdf";
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
	private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

	private static Font mediumBold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	private static Font mediumNormal = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);

	private static Font largeBold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
	private static Font largeNormal = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);
	
	private static Font UNDERLINE = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.UNDERLINE);

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

//-----------------------PDF-----------------------------------
	
	public static File generateInvoicePdf(BillPrintDataModel billPrintDataModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			Company company=new Company();
	
			if(billPrintDataModel.getBusinessName()!=null){
				company=billPrintDataModel.getBusinessName().getCompany();
			}else{
				company=billPrintDataModel.getEmployeeGKCounterOrder().getCompany();
			}
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("TAX INVOICE", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(company.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(company.getAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getTelephoneNumber()==null)?"--":company.getContact().getTelephoneNumber()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getGstinno()==null)?"--":company.getGstinno()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getEmailId()==null)?"--":company.getContact().getEmailId()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Invoice No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getInvoiceNumber(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getOrderDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Transportation Name \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(billPrintDataModel.getTransporterName()+" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Vehicle Number \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(billPrintDataModel.getVehicalNumber()+" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(billPrintDataModel.getTransporterGstNumber()+" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk((billPrintDataModel.getDocketNo()==null)?"NA":billPrintDataModel.getDocketNo()+" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			if(billPrintDataModel.getBusinessName()!=null){
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getShopName()+" \n", mediumBold));
				
				/*for(String addressLine : billPrintDataModel.getAddressLineList())
				{
					buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
				}*/
				
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getAddress()+"\n",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getName()+" ,",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getName()+" - "+billPrintDataModel.getBusinessName().getArea().getPincode()+",\n",mediumNormal));
				
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
				buyerPhrase.add(new Chunk("Code : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
				
				if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{	
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"/"+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()==null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()==null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else 
				{
					buyerPhrase.add(new Chunk("\n",mediumNormal));
				}
			
				/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getGstinNumber(),mediumNormal));
			}else{
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerName()+" \n", mediumBold));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));				
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerMobileNumber()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				if(billPrintDataModel.getCustomerGstNumber()==null){
					buyerPhrase.add(new Chunk(" NA",mediumNormal));
				}else{
					buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerGstNumber(),mediumNormal));	
				}
				
			}
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Buyer Order No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getOrderNumber(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Delivery Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getDeliveryDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			//buyerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Despatch Document No. \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			PdfPCell buyerSideTableCell4=new TopLeftBorderPDFCell();
			//buyerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell4Phrase = new Phrase();
			buyerSideTableCell4Phrase.setLeading(12);
			buyerSideTableCell4Phrase.add(new Chunk("Delivery Note Date \n", mediumBold));
			buyerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell4.addElement(buyerSideTableCell4Phrase);
			buyerSideTable.addCell(buyerSideTableCell4);			
			
			PdfPCell buyerSideTableCell5=new TopRightBorderPDFCell();
			//buyerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell5Phrase = new Phrase();
			buyerSideTableCell5Phrase.setLeading(12);
			buyerSideTableCell5Phrase.add(new Chunk("Despatch through \n", mediumBold));
			buyerSideTableCell5Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell5.addElement(buyerSideTableCell5Phrase);
			buyerSideTable.addCell(buyerSideTableCell5);
			
			PdfPCell buyerSideTableCell6=new TopLeftBorderPDFCell();
			//buyerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell6Phrase = new Phrase();
			buyerSideTableCell6Phrase.setLeading(12);
			buyerSideTableCell6Phrase.add(new Chunk("Destination \n", mediumBold));
			buyerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell6.addElement(buyerSideTableCell6Phrase);
			buyerSideTable.addCell(buyerSideTableCell6);	
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			PdfPTable productDetailsTable = new PdfPTable(new float[] { 14,80, 20 ,18,18, 18,/*18,*/30});
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr.No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Quantity", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("Rate", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
		
/*			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);*/
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Amount", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
	
			
			for(ProductListForBill productListForBill: billPrintDataModel.getProductListForBill())
			{
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productListForBill.getSrno(), mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productListForBill.getProductName(), mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productListForBill.getHsnCode(), mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productListForBill.getTaxSlab()+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productListForBill.getQuantityIssued(), mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productListForBill.getRatePerProduct(), mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
	/*			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk((productListForBill.getDiscountAmt()==null)?"0":productListForBill.getDiscountAmt(), mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);*/
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				//productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk("     "+productListForBill.getAmountWithoutTax(), mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
			}
			
			//totalAmount
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Taxable Amount", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell34Phrase = new Phrase();
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			/*
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);*/
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Phrase productDetailsTableCell35Phrase = new Phrase();
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk("     "+billPrintDataModel.getTotalAmountWithoutTax(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			/*PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);*/
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
		/*	PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);*/
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
	/*		PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);*/
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);
			
			//roundOf
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

/*			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);*/
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Phrase productDetailsTableCell53Phrase = new Phrase();
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("     "+"("+billPrintDataModel.getRoundOffAmount()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			//totalamountWithtax
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Total Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("     "+billPrintDataModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);

/*			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);*/
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Phrase productDetailsTableCell42Phrase = new Phrase();
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk("     "+billPrintDataModel.getTotalAmountWithTax(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTotalAmountWithTaxInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(CategoryWiseAmountForBill categoryWiseAmountForBill:billPrintDataModel.getCategoryWiseAmountForBills())
			{
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.getHsnCode(), mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.getTaxableValue(), mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstRate(), mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstRate(), mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstRate(), mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(billPrintDataModel.getTotalAmount(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
						
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTaxAmountInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
			lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			lastRowTable.setWidthPercentage(100);
			
			PdfPCell declarationTableCell=new RightBorderPDFCell();
			Phrase declarationTableCellPhrase = new Phrase();
			declarationTableCellPhrase.setLeading(12);
			declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
			declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct", mediumNormal));			
			declarationTableCell.addElement(declarationTableCellPhrase);		
			lastRowTable.addCell(declarationTableCell);
			
			PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			lastRowTable.addCell(signTableCell);
			
			PdfPCell lastRowMainCell=new PdfPCell(lastRowTable);
			lastRowMainCell.setColspan(2);
			lastRowMainCell.setPadding(0);
			mainTable.addCell(lastRowMainCell);			
			
			document.add(mainTable);
			
			Paragraph paragraphFooter = new Paragraph("This is a Computer Generated Invoice", mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	
		
	}


	public static File generateSMOrderCreditNotePdf(BillPrintDataModel billPrintDataModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			Company company=new Company();
	
			if(billPrintDataModel.getBusinessName()!=null){
				company=billPrintDataModel.getBusinessName().getCompany();
			}else{
				company=billPrintDataModel.getEmployeeGKCounterOrder().getCompany();
			}
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("CREDIT NOTE", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(company.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(company.getAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getTelephoneNumber()==null)?"--":company.getContact().getTelephoneNumber()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getGstinno()==null)?"--":company.getGstinno()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getEmailId()==null)?"--":company.getContact().getEmailId()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Credit Note No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(""+""+billPrintDataModel.getCreditNoteNumber(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Credit Note Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(""+""+billPrintDataModel.getReturnDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Invoice Number \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(""+""+billPrintDataModel.getInvoiceNumber()+" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Invoice Date \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(""+""+billPrintDataModel.getOrderDate()+" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			/*PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(billPrintDataModel.getTransporterGstNumber()+" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk((billPrintDataModel.getDocketNo()==null)?"NA":billPrintDataModel.getDocketNo()+" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	*/
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			if(billPrintDataModel.getBusinessName()!=null){
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getShopName()+" \n", mediumBold));
				
				/*for(String addressLine : billPrintDataModel.getAddressLineList())
				{
					buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
				}*/
				
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getAddress()+"\n",mediumNormal));
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getArea().getName()+" ,",mediumNormal));
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getName()+" - "+""+billPrintDataModel.getBusinessName().getArea().getPincode()+",\n",mediumNormal));
				
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
				buyerPhrase.add(new Chunk("Code : ", mediumBold));
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
				
				if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{	
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"/"+""+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()==null)
				{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()==null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else 
				{
					buyerPhrase.add(new Chunk("\n",mediumNormal));
				}
			
				/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				buyerPhrase.add(new Chunk(""+""+billPrintDataModel.getBusinessName().getGstinNumber(),mediumNormal));
			}else{
				buyerPhrase.add(new Chunk(""+""+billPrintDataModel.getCustomerName()+" \n", mediumBold));
				buyerPhrase.add(new Chunk("Mobile No. : ", mediumBold));				
				buyerPhrase.add(new Chunk(""+""+billPrintDataModel.getCustomerMobileNumber()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				if(billPrintDataModel.getCustomerGstNumber()==null){
					buyerPhrase.add(new Chunk(" NA",mediumNormal));
				}else{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getCustomerGstNumber(),mediumNormal));	
				}
				
			}
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Buyer Order No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(""+billPrintDataModel.getOrderNumber(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Delivery Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(""+billPrintDataModel.getDeliveryDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			//comment
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			buyerSideTableCell3.setColspan(2);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Comment \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(""+billPrintDataModel.getComment(),mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);	
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			PdfPTable productDetailsTable = new PdfPTable(new float[] { 14,80, 20 ,18,18, 18,/*18,*/30});
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr.No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Quantity", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("Rate", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
		
/*			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);*/
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Amount", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
	
			
			for(ProductListForBill productListForBill: billPrintDataModel.getProductListForBill())
			{
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productListForBill.getSrno(), mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productListForBill.getProductName(), mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productListForBill.getHsnCode(), mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productListForBill.getTaxSlab()+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productListForBill.getQuantityIssued(), mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productListForBill.getRatePerProduct(), mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
	/*			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk((productListForBill.getDiscountAmt()==null)?"0":productListForBill.getDiscountAmt(), mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);*/
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				//productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk("     "+productListForBill.getAmountWithoutTax(), mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
			}
			
			//totalAmount
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Taxable Amount", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell34Phrase = new Phrase();
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			/*
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);*/
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Phrase productDetailsTableCell35Phrase = new Phrase();
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk("     "+billPrintDataModel.getTotalAmountWithoutTax(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			/*PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);*/
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
		/*	PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);*/
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
	/*		PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);*/
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);
			
			//roundOf
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

/*			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);*/
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Phrase productDetailsTableCell53Phrase = new Phrase();
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("     "+"("+billPrintDataModel.getRoundOffAmount()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			//totalamountWithtax
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Total Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("     "+billPrintDataModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);

/*			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);*/
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Phrase productDetailsTableCell42Phrase = new Phrase();
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk("     "+billPrintDataModel.getTotalAmountWithTax(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTotalAmountWithTaxInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(CategoryWiseAmountForBill categoryWiseAmountForBill:billPrintDataModel.getCategoryWiseAmountForBills())
			{
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.getHsnCode(), mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.getTaxableValue(), mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstRate(), mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstRate(), mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstRate(), mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmount(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(""+billPrintDataModel.getTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(""+billPrintDataModel.getTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(""+billPrintDataModel.getTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
						
			
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTaxAmountInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
			lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			lastRowTable.setWidthPercentage(100);
			
			PdfPCell declarationTableCell=new RightBorderPDFCell();
			Phrase declarationTableCellPhrase = new Phrase();
			declarationTableCellPhrase.setLeading(12);
			declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
			declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct", mediumNormal));			
			declarationTableCell.addElement(declarationTableCellPhrase);		
			lastRowTable.addCell(declarationTableCell);
			
			PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			lastRowTable.addCell(signTableCell);
			
			PdfPCell lastRowMainCell=new PdfPCell(lastRowTable);
			lastRowMainCell.setColspan(2);
			lastRowMainCell.setPadding(0);
			mainTable.addCell(lastRowMainCell);			
			
			document.add(mainTable);
			
			Paragraph paragraphFooter = new Paragraph("This is a Computer Generated Invoice", mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	
		
	}

	
	public static File generateInvoicePdfCounterOrder(BillPrintDataModel billPrintDataModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			Company company=new Company();
	
			if(billPrintDataModel.getBusinessName()!=null){
				company=billPrintDataModel.getBusinessName().getCompany();
			}else{
				company=billPrintDataModel.getEmployeeGKCounterOrder().getCompany();
			}
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("TAX INVOICE", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(company.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(company.getAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getTelephoneNumber()==null)?"--":company.getContact().getTelephoneNumber()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getGstinno()==null)?"--":company.getGstinno()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getEmailId()==null)?"--":company.getContact().getEmailId()+" \n",mediumNormal));
			/*ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));*/
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Invoice No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getInvoiceNumber(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getOrderDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Transportation Name \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(billPrintDataModel.getTransporterName()+" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Vehicle Number \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(billPrintDataModel.getVehicalNumber()+" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(billPrintDataModel.getTransporterGstNumber()+" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk((billPrintDataModel.getDocketNo()==null)?"NA":billPrintDataModel.getDocketNo()+" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			if(billPrintDataModel.getBusinessName()!=null){
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getShopName()+" \n", mediumBold));
				
				/*for(String addressLine : billPrintDataModel.getAddressLineList())
				{
					buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
				}*/
				
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getAddress()+"\n",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getName()+" ,",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getName()+" - "+billPrintDataModel.getBusinessName().getArea().getPincode()+",\n",mediumNormal));
				
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
				buyerPhrase.add(new Chunk("Code : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
				
				if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{	
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"/"+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()==null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()==null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else 
				{
					buyerPhrase.add(new Chunk("\n",mediumNormal));
				}
			
				/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getGstinNumber(),mediumNormal));
			}else{
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerName()+" \n", mediumBold));
				buyerPhrase.add(new Chunk("Mobile No. : ", mediumBold));				
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerMobileNumber()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				if(billPrintDataModel.getCustomerGstNumber()==null){
					buyerPhrase.add(new Chunk(" NA",mediumNormal));
				}else{
					buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerGstNumber(),mediumNormal));	
				}
				
			}
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Buyer Order No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getOrderNumber(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Delivery Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getDeliveryDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			//buyerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Despatch Document No. \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			PdfPCell buyerSideTableCell4=new TopLeftBorderPDFCell();
			//buyerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell4Phrase = new Phrase();
			buyerSideTableCell4Phrase.setLeading(12);
			buyerSideTableCell4Phrase.add(new Chunk("Delivery Note Date \n", mediumBold));
			buyerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell4.addElement(buyerSideTableCell4Phrase);
			buyerSideTable.addCell(buyerSideTableCell4);			
			
			PdfPCell buyerSideTableCell5=new TopRightBorderPDFCell();
			//buyerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell5Phrase = new Phrase();
			buyerSideTableCell5Phrase.setLeading(12);
			buyerSideTableCell5Phrase.add(new Chunk("Despatch through \n", mediumBold));
			buyerSideTableCell5Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell5.addElement(buyerSideTableCell5Phrase);
			buyerSideTable.addCell(buyerSideTableCell5);
			
			PdfPCell buyerSideTableCell6=new TopLeftBorderPDFCell();
			//buyerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell6Phrase = new Phrase();
			buyerSideTableCell6Phrase.setLeading(12);
			buyerSideTableCell6Phrase.add(new Chunk("Destination \n", mediumBold));
			buyerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell6.addElement(buyerSideTableCell6Phrase);
			buyerSideTable.addCell(buyerSideTableCell6);	
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			//table size array
			float[] tableColSize=new float[] { 14,80, 24 ,15,18,15, 18,18,18,30};
			PdfPTable productDetailsTable = new PdfPTable(tableColSize);
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr. No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("MRP", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Qty.", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCel56=new RightBorderPDFCell();
			Paragraph productDetailsTableCell56Phrase = new Paragraph();
			productDetailsTableCell56Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell56Phrase.setLeading(12);
			productDetailsTableCell56Phrase.add(new Chunk("Total\nAmt.", mediumBold));
			productDetailsTableCel56.addElement(productDetailsTableCell56Phrase);
			productDetailsTable.addCell(productDetailsTableCel56);
		
			PdfPCell productDetailsTableCellDiscPer=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPerPhrase = new Paragraph();
			productDetailsTableCellDiscPerPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPerPhrase.setLeading(12);
			productDetailsTableCellDiscPerPhrase.add(new Chunk("Disc. (%)", mediumBold));
			productDetailsTableCellDiscPer.addElement(productDetailsTableCellDiscPerPhrase);
			productDetailsTable.addCell(productDetailsTableCellDiscPer);
			
			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.\nAmt.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Net Payable", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			for(ProductListForBill productListForBill: billPrintDataModel.getProductListForBill())
			{
				productDetailsTable = new PdfPTable(tableColSize);
				
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productListForBill.getSrno(), mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productListForBill.getProductName(), mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productListForBill.getHsnCode(), mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productListForBill.getTaxSlab()+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productListForBill.getRatePerProduct(), mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productListForBill.getQuantityIssued(), mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell52Phrase = new Paragraph();
				productDetailsTableCell52Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell52Phrase.setLeading(12);
				productDetailsTableCell52Phrase.add(new Chunk(productListForBill.getAmountWithoutTax(), mediumNormal));
				productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
				productDetailsTable.addCell(productDetailsTableCell52);
				
				PdfPCell productDetailsTableCell59=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell59Phrase = new Paragraph();
				productDetailsTableCell59Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell59Phrase.setLeading(12);
				productDetailsTableCell59Phrase.add(new Chunk(productListForBill.getDiscountPer(), mediumNormal));
				productDetailsTableCell59.addElement(productDetailsTableCell59Phrase);
				productDetailsTable.addCell(productDetailsTableCell59);
				
				PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk(productListForBill.getDiscountAmt(), mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_RIGHT);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk(productListForBill.getNetPayable(), mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
				
				productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
				productDetailsTableCellTemp.setPadding(0);
				productDetailsTableCellTemp.setColspan(2);
				mainTable.addCell(productDetailsTableCellTemp);
			}
			
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Total", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCell34Phrase = new Paragraph();
			productDetailsTableCell34Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk(billPrintDataModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);
			
			PdfPCell productDetailsTableCell57=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell57Phrase = new Phrase();
			productDetailsTableCell57Phrase.setLeading(12);
			productDetailsTableCell57Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell57.addElement(productDetailsTableCell57Phrase);
			productDetailsTable.addCell(productDetailsTableCell57);
			
			PdfPCell productDetailsTableCell60=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell60Phrase = new Phrase();
			productDetailsTableCell60Phrase.setLeading(12);
			productDetailsTableCell60Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell60.addElement(productDetailsTableCell60Phrase);
			productDetailsTable.addCell(productDetailsTableCell60);
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Paragraph productDetailsTableCell35Phrase = new Paragraph();
			productDetailsTableCell35Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithoutTax(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//discount section
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCellMainDisc1=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc1Phrase = new Phrase();
			productDetailsTableCellMainDisc1Phrase.setLeading(12);
			productDetailsTableCellMainDisc1Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc1.addElement(productDetailsTableCellMainDisc1Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc1);
			
			PdfPCell productDetailsTableCellMainDisc2=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc2Phrase = new Phrase();
			productDetailsTableCellMainDisc2Phrase.setLeading(12);
			productDetailsTableCellMainDisc2Phrase.add(new Chunk("           Discount", mediumBold));
			productDetailsTableCellMainDisc2.addElement(productDetailsTableCellMainDisc2Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc2);
			
			PdfPCell productDetailsTableCellMainDisc3=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc3Phrase = new Phrase();
			productDetailsTableCellMainDisc3Phrase.setLeading(12);
			productDetailsTableCellMainDisc3Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc3.addElement(productDetailsTableCellMainDisc3Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc3);
			
			PdfPCell productDetailsTableCellMainDisc4=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc4Phrase = new Phrase();
			productDetailsTableCellMainDisc4Phrase.setLeading(12);
			productDetailsTableCellMainDisc4Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc4.addElement(productDetailsTableCellMainDisc4Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc4);
			
			PdfPCell productDetailsTableCellMainDisc5=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc5Phrase = new Phrase();
			productDetailsTableCellMainDisc5Phrase.setLeading(12);
			productDetailsTableCellMainDisc5Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc5.addElement(productDetailsTableCellMainDisc5Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc5);
			
			PdfPCell productDetailsTableCellMainDisc6=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc6Phrase = new Phrase();
			productDetailsTableCellMainDisc6Phrase.setLeading(12);
			productDetailsTableCellMainDisc6Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc6.addElement(productDetailsTableCellMainDisc6Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc6);
			
			PdfPCell productDetailsTableCellMainDisc7=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc7Phrase = new Phrase();
			productDetailsTableCellMainDisc7Phrase.setLeading(12);
			productDetailsTableCellMainDisc7Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc7.addElement(productDetailsTableCellMainDisc7Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc7);
			
			PdfPCell productDetailsTableCellMainDisc8=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc8Phrase = new Paragraph();
			productDetailsTableCellMainDisc8Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellMainDisc8Phrase.setLeading(12);
			productDetailsTableCellMainDisc8Phrase.add(new Chunk(billPrintDataModel.getTotalPercentageOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc8.addElement(productDetailsTableCellMainDisc8Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc8);
			
			PdfPCell productDetailsTableCellMainDisc9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc9Phrase = new Phrase();
			productDetailsTableCellMainDisc9Phrase.setLeading(12);
			productDetailsTableCellMainDisc9Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc9.addElement(productDetailsTableCellMainDisc9Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc9);
			
			PdfPCell productDetailsTableCellMainDisc10=new TopBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc10Phrase = new Paragraph();
			productDetailsTableCellMainDisc10Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCellMainDisc10Phrase.setLeading(12);
			productDetailsTableCellMainDisc10Phrase.add(new Chunk(billPrintDataModel.getTotalAmountOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc10.addElement(productDetailsTableCellMainDisc10Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc10);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell63=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell63Phrase = new Phrase();
			productDetailsTableCell63Phrase.setLeading(12);
			productDetailsTableCell63Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell63.addElement(productDetailsTableCell63Phrase);
			productDetailsTable.addCell(productDetailsTableCell63);
			
			PdfPCell productDetailsTableCell64=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell64Phrase = new Phrase();
			productDetailsTableCell64Phrase.setLeading(12);
			productDetailsTableCell64Phrase.add(new Chunk("           Net Total Amount", mediumBold));
			productDetailsTableCell64.addElement(productDetailsTableCell64Phrase);
			productDetailsTable.addCell(productDetailsTableCell64);
			
			PdfPCell productDetailsTableCell65=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell65Phrase = new Phrase();
			productDetailsTableCell65Phrase.setLeading(12);
			productDetailsTableCell65Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell65.addElement(productDetailsTableCell65Phrase);
			productDetailsTable.addCell(productDetailsTableCell65);
			
			PdfPCell productDetailsTableCell66=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell67Phrase = new Phrase();
			productDetailsTableCell67Phrase.setLeading(12);
			productDetailsTableCell67Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell66.addElement(productDetailsTableCell67Phrase);
			productDetailsTable.addCell(productDetailsTableCell66);
			
			PdfPCell productDetailsTableCell68=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell69Phrase = new Phrase();
			productDetailsTableCell69Phrase.setLeading(12);
			productDetailsTableCell69Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell68.addElement(productDetailsTableCell69Phrase);
			productDetailsTable.addCell(productDetailsTableCell68);
			
			PdfPCell productDetailsTableCell70=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell70Phrase = new Phrase();
			productDetailsTableCell70Phrase.setLeading(12);
			productDetailsTableCell70Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell70.addElement(productDetailsTableCell70Phrase);
			productDetailsTable.addCell(productDetailsTableCell70);
			
			PdfPCell productDetailsTableCell71=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell71Phrase = new Phrase();
			productDetailsTableCell71Phrase.setLeading(12);
			productDetailsTableCell71Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell71.addElement(productDetailsTableCell71Phrase);
			productDetailsTable.addCell(productDetailsTableCell71);
			
			PdfPCell productDetailsTableCell72=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell72Phrase = new Phrase();
			productDetailsTableCell72Phrase.setLeading(12);
			productDetailsTableCell72Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell72.addElement(productDetailsTableCell72Phrase);
			productDetailsTable.addCell(productDetailsTableCell72);
			
			PdfPCell productDetailsTableCell73=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell73Phrase = new Phrase();
			productDetailsTableCell73Phrase.setLeading(12);
			productDetailsTableCell73Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell73.addElement(productDetailsTableCell73Phrase);
			productDetailsTable.addCell(productDetailsTableCell73);
			
			PdfPCell productDetailsTableCell74=new TopBorderPDFCell();
			Paragraph productDetailsTableCell74Phrase = new Paragraph();
			productDetailsTableCell74Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell74Phrase.setLeading(12);
			productDetailsTableCell74Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithTaxWithDiscNonRoundOff(), mediumBold));
			productDetailsTableCell74.addElement(productDetailsTableCell74Phrase);
			productDetailsTable.addCell(productDetailsTableCell74);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			/*//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
			PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
			PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);*/
			
			//roundOf
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);
			
			PdfPCell productDetailsTableCell56=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell58Phrase = new Phrase();
			productDetailsTableCell58Phrase.setLeading(12);
			productDetailsTableCell58Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell56.addElement(productDetailsTableCell58Phrase);
			productDetailsTable.addCell(productDetailsTableCell56);
			
			PdfPCell productDetailsTableCell61=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell61Phrase = new Phrase();
			productDetailsTableCell61Phrase.setLeading(12);
			productDetailsTableCell61Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell61.addElement(productDetailsTableCell61Phrase);
			productDetailsTable.addCell(productDetailsTableCell61);
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Paragraph productDetailsTableCell53Phrase = new Paragraph();
			productDetailsTableCell53Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("("+billPrintDataModel.getRoundOffAmount()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalamountWithtax
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Net Payable Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);

			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);
			
			PdfPCell productDetailsTableCell58=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell59Phrase = new Phrase();
			productDetailsTableCell59Phrase.setLeading(12);
			productDetailsTableCell59Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell58.addElement(productDetailsTableCell59Phrase);
			productDetailsTable.addCell(productDetailsTableCell58);
			
			PdfPCell productDetailsTableCell62=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell62Phrase = new Phrase();
			productDetailsTableCell62Phrase.setLeading(12);
			productDetailsTableCell62Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell62.addElement(productDetailsTableCell62Phrase);
			productDetailsTable.addCell(productDetailsTableCell62);
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Paragraph productDetailsTableCell42Phrase = new Paragraph();
			productDetailsTableCell42Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithTaxRoundOff(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTotalAmountWithTaxInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(CategoryWiseAmountForBill categoryWiseAmountForBill:billPrintDataModel.getCategoryWiseAmountForBills())
			{
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.getHsnCode(), mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.getTaxableValue(), mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstRate(), mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstRate(), mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstRate(), mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(billPrintDataModel.getTotalAmount(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
			
			/* disc amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell14=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell14Phrase = new Paragraph();
			taxDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell14Phrase.setLeading(10);
			taxDetailsTableCell14Phrase.add(new Chunk("Discount", mediumBold));
			taxDetailsTableCell14.addElement(taxDetailsTableCell14Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell14);
			
			PdfPCell taxDetailsTableCell15=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell15Phrase = new Paragraph();
			taxDetailsTableCell15Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell15Phrase.setLeading(10);
			taxDetailsTableCell15Phrase.add(new Chunk(billPrintDataModel.getTotalAmountDisc(), mediumNormal));
			taxDetailsTableCell15.addElement(taxDetailsTableCell15Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell15);
			
			PdfPTable cgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell16=new RightBorderPDFCell();
			Phrase taxDetailsTableCell16Phrase = new Phrase();
			taxDetailsTableCell16Phrase.setLeading(10);
			taxDetailsTableCell16Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell16.addElement(taxDetailsTableCell16Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell16);
			
			PdfPCell taxDetailsTableCell17=new PdfPCell();
			taxDetailsTableCell17.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell17Phrase = new Paragraph();
			taxDetailsTableCell17Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell17Phrase.setLeading(10);
			taxDetailsTableCell17Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell17.addElement(taxDetailsTableCell17Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell17);
			
			PdfPCell cgstValueTableCellDisc=new PdfPCell(cgstValueTableDisc);
			cgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellDisc);
			
			PdfPTable sgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell18=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell18Phrase = new Paragraph();
			taxDetailsTableCell18Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell18Phrase.setLeading(10);
			taxDetailsTableCell18Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell18.addElement(taxDetailsTableCell18Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell18);
			
			PdfPCell taxDetailsTableCell19=new PdfPCell();
			taxDetailsTableCell19.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell19Phrase = new Paragraph();
			taxDetailsTableCell19Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell19Phrase.setLeading(10);
			taxDetailsTableCell19Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell19.addElement(taxDetailsTableCell19Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell19);
			
			PdfPCell sgstValueTableCellDisc=new PdfPCell(sgstValueTableDisc);
			sgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellDisc);
			
			PdfPTable igstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			igstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell20=new RightBorderPDFCell();
			Phrase taxDetailsTableCell20Phrase = new Phrase();
			taxDetailsTableCell20Phrase.setLeading(10);
			taxDetailsTableCell20Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell20.addElement(taxDetailsTableCell20Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell20);
			
			PdfPCell taxDetailsTableCell21=new PdfPCell();
			taxDetailsTableCell21.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell21Phrase = new Paragraph();
			taxDetailsTableCell21Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell21Phrase.setLeading(10);
			taxDetailsTableCell21Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell21.addElement(taxDetailsTableCell21Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell21);
			
			PdfPCell igstValueTableCellDisc=new PdfPCell(igstValueTableDisc);
			igstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellDisc);
			/* disc amount in hsn code wise part end*/
			
			/* net amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell22=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell22Phrase = new Paragraph();
			taxDetailsTableCell22Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell22Phrase.setLeading(10);
			taxDetailsTableCell22Phrase.add(new Chunk("Net Total", mediumBold));
			taxDetailsTableCell22.addElement(taxDetailsTableCell22Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell22);
			
			PdfPCell taxDetailsTableCell23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell23Phrase = new Paragraph();
			taxDetailsTableCell23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell23Phrase.setLeading(10);
			taxDetailsTableCell23Phrase.add(new Chunk(billPrintDataModel.getNetTotalAmount(), mediumNormal));
			taxDetailsTableCell23.addElement(taxDetailsTableCell23Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell23);
			
			PdfPTable cgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell24=new RightBorderPDFCell();
			Phrase taxDetailsTableCell24Phrase = new Phrase();
			taxDetailsTableCell24Phrase.setLeading(10);
			taxDetailsTableCell24Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell24.addElement(taxDetailsTableCell24Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell24);
			
			PdfPCell taxDetailsTableCell25=new PdfPCell();
			taxDetailsTableCell25.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell25Phrase = new Paragraph();
			taxDetailsTableCell25Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell25Phrase.setLeading(10);
			taxDetailsTableCell25Phrase.add(new Chunk(billPrintDataModel.getNetTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell25.addElement(taxDetailsTableCell25Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell25);
			
			PdfPCell cgstValueTableCellNet=new PdfPCell(cgstValueTableNet);
			cgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellNet);
			
			PdfPTable sgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell26=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell26Phrase = new Paragraph();
			taxDetailsTableCell26Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell26Phrase.setLeading(10);
			taxDetailsTableCell26Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell26.addElement(taxDetailsTableCell26Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell26);
			
			PdfPCell taxDetailsTableCell27=new PdfPCell();
			taxDetailsTableCell27.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell27Phrase = new Paragraph();
			taxDetailsTableCell27Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell27Phrase.setLeading(10);
			taxDetailsTableCell27Phrase.add(new Chunk(billPrintDataModel.getNetTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell27.addElement(taxDetailsTableCell27Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell27);
			
			PdfPCell sgstValueTableCellNet=new PdfPCell(sgstValueTableNet);
			sgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellNet);
			
			PdfPTable igstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			igstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell28=new RightBorderPDFCell();
			Phrase taxDetailsTableCell28Phrase = new Phrase();
			taxDetailsTableCell28Phrase.setLeading(10);
			taxDetailsTableCell28Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell28.addElement(taxDetailsTableCell28Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell28);
			
			PdfPCell taxDetailsTableCell29=new PdfPCell();
			taxDetailsTableCell29.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell29Phrase = new Paragraph();
			taxDetailsTableCell29Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell29Phrase.setLeading(10);
			taxDetailsTableCell29Phrase.add(new Chunk(billPrintDataModel.getNetTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell29.addElement(taxDetailsTableCell29Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell29);
			
			PdfPCell igstValueTableCellNet=new PdfPCell(igstValueTableNet);
			igstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellNet);
			/* net amount in hsn code wise part end*/
		
			
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTaxAmountInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			/*PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
			lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			lastRowTable.setWidthPercentage(100);
			
			PdfPCell declarationTableCell=new RightBorderPDFCell();
			Phrase declarationTableCellPhrase = new Phrase();
			declarationTableCellPhrase.setLeading(12);
			declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
			declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct", mediumNormal));			
			declarationTableCell.addElement(declarationTableCellPhrase);		
			lastRowTable.addCell(declarationTableCell);
			
			PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			lastRowTable.addCell(signTableCell);*/
			
			PdfPTable bankAndSignDetailsTable = new PdfPTable(new float[] { 60 , 40});
			bankAndSignDetailsTable.setWidthPercentage(100);
			
			
			PdfPTable bankDeailsTable = new PdfPTable(new float[] { 20,40 ,40});
			bankDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			bankDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellBank1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank1Phrase = new Paragraph();
			taxDetailsTableCellBank1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank1Phrase.setLeading(10);
			taxDetailsTableCellBank1Phrase.add(new Chunk("Bank Name", mediumBold));
			taxDetailsTableCellBank1.addElement(taxDetailsTableCellBank1Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank1);	
			
			PdfPCell taxDetailsTableCellBank2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank2Phrase = new Paragraph();
			taxDetailsTableCellBank2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank2Phrase.setLeading(10);
			taxDetailsTableCellBank2Phrase.add(new Chunk("Kotak Mahindra Bank", mediumNormal));
			taxDetailsTableCellBank2.addElement(taxDetailsTableCellBank2Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank2);
			
			PdfPCell taxDetailsTableCellBank3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank3Phrase = new Paragraph();
			taxDetailsTableCellBank3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank3Phrase.setLeading(10);
			taxDetailsTableCellBank3Phrase.add(new Chunk("HDFC", mediumNormal));
			taxDetailsTableCellBank3.addElement(taxDetailsTableCellBank3Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank3);
			
			PdfPCell taxDetailsTableCellBank4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank4Phrase = new Paragraph();
			taxDetailsTableCellBank4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank4Phrase.setLeading(10);
			taxDetailsTableCellBank4Phrase.add(new Chunk("A/C Name", mediumBold));
			taxDetailsTableCellBank4.addElement(taxDetailsTableCellBank4Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank4);	
			
			PdfPCell taxDetailsTableCellBank5=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank5Phrase = new Paragraph();
			taxDetailsTableCellBank5Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank5Phrase.setLeading(10);
			taxDetailsTableCellBank5Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank5.addElement(taxDetailsTableCellBank5Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank5);
			
			PdfPCell taxDetailsTableCellBank6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank6Phrase = new Paragraph();
			taxDetailsTableCellBank6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank6Phrase.setLeading(10);
			taxDetailsTableCellBank6Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank6.addElement(taxDetailsTableCellBank6Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank6);
			
			PdfPCell taxDetailsTableCellBank7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank7Phrase = new Paragraph();
			taxDetailsTableCellBank7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank7Phrase.setLeading(10);
			taxDetailsTableCellBank7Phrase.add(new Chunk("A/C No.", mediumBold));
			taxDetailsTableCellBank7.addElement(taxDetailsTableCellBank7Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank7);	
			
			PdfPCell taxDetailsTableCellBank8=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank8Phrase = new Paragraph();
			taxDetailsTableCellBank8Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank8Phrase.setLeading(10);
			taxDetailsTableCellBank8Phrase.add(new Chunk("39876326723", mediumNormal));
			taxDetailsTableCellBank8.addElement(taxDetailsTableCellBank8Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank8);
			
			PdfPCell taxDetailsTableCellBank9=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank9Phrase = new Paragraph();
			taxDetailsTableCellBank9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank9Phrase.setLeading(10);
			taxDetailsTableCellBank9Phrase.add(new Chunk("23783728782738", mediumNormal));
			taxDetailsTableCellBank9.addElement(taxDetailsTableCellBank9Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank9);
			
			PdfPCell taxDetailsTableCellBank10=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank10Phrase = new Paragraph();
			taxDetailsTableCellBank10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank10Phrase.setLeading(10);
			taxDetailsTableCellBank10Phrase.add(new Chunk("IFSC", mediumBold));
			taxDetailsTableCellBank10.addElement(taxDetailsTableCellBank10Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank10);	
			
			PdfPCell taxDetailsTableCellBank11=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank11Phrase = new Paragraph();
			taxDetailsTableCellBank11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank11Phrase.setLeading(10);
			taxDetailsTableCellBank11Phrase.add(new Chunk("KKBK0000667", mediumNormal));
			taxDetailsTableCellBank11.addElement(taxDetailsTableCellBank11Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank11);
			
			PdfPCell taxDetailsTableCellBank12=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank12Phrase = new Paragraph();
			taxDetailsTableCellBank12Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank12Phrase.setLeading(10);
			taxDetailsTableCellBank12Phrase.add(new Chunk("HDFC0000002", mediumNormal));
			taxDetailsTableCellBank12.addElement(taxDetailsTableCellBank12Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank12);
			
			PdfPCell bankDetailsMainCell=new PdfPCell(bankDeailsTable);
			bankDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(bankDetailsMainCell);			
			
			PdfPTable signDeailsTable = new PdfPTable(new float[] { 50 ,50});
			signDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			signDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellSign1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign1Phrase = new Paragraph();
			taxDetailsTableCellSign1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign1Phrase.setLeading(10);
			taxDetailsTableCellSign1Phrase.add(new Chunk("\n\n\n\n", mediumNormal));
			taxDetailsTableCellSign1.addElement(taxDetailsTableCellSign1Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign1);
			
			/*PdfPCell taxDetailsTableCellSign23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign23Phrase = new Paragraph();
			taxDetailsTableCellSign23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign23Phrase.setLeading(10);
			taxDetailsTableCellSign23Phrase.add(new Chunk("for"+company.getCompanyName(), mediumBold));
			taxDetailsTableCellSign23.addElement(taxDetailsTableCellSign23Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign23);*/
			
			PdfPCell taxDetailsTableCellSign2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign2Phrase = new Paragraph();
			taxDetailsTableCellSign2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign2Phrase.setLeading(10);
			taxDetailsTableCellSign2Phrase.add(new Chunk("For "+company.getCompanyName()+"\n\n\n\n", mediumBold));
			taxDetailsTableCellSign2.addElement(taxDetailsTableCellSign2Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign2);	
			
			PdfPCell taxDetailsTableCellSign3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign3Phrase = new Paragraph();
			taxDetailsTableCellSign3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign3Phrase.setLeading(10);
			taxDetailsTableCellSign3Phrase.add(new Chunk("Receiver's Signature", mediumNormal));
			taxDetailsTableCellSign3.addElement(taxDetailsTableCellSign3Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign3);
			
			PdfPCell taxDetailsTableCellSign4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign4Phrase = new Paragraph();
			taxDetailsTableCellSign4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign4Phrase.setLeading(10);
			taxDetailsTableCellSign4Phrase.add(new Chunk("Authorized Signatory", mediumNormal));
			taxDetailsTableCellSign4.addElement(taxDetailsTableCellSign4Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign4);
			
			PdfPCell signDetailsMainCell=new PdfPCell(signDeailsTable);
			signDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(signDetailsMainCell);
			
			/*PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			bankDeailsTable.addCell(signTableCell);*/
			
			PdfPCell lastRowMainCell=new PdfPCell(bankAndSignDetailsTable);
			lastRowMainCell.setColspan(2);
			lastRowMainCell.setPadding(0);
			mainTable.addCell(lastRowMainCell);			
			
			document.add(mainTable);

			double transportationCharges=billPrintDataModel.getTrasportationCharge();
			String footer="";
			if(transportationCharges>0){
				//footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n Note:"+transportationCharges+" Rupees as transportation Charges is payable by customer\n\nThis is a Computer Generated Invoice";	
				footer="Note:"+transportationCharges+" Rupees as transportation Charges is payable by customer\n\nThis is a Computer Generated Invoice";	
			}else{
				//footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n\n\nThis is a Computer Generated Invoice";
				 footer="This is a Computer Generated Invoice";
			}
			
			Paragraph paragraphFooter = new Paragraph(footer);
			//Paragraph paragraphFooter = new Paragraph("We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n\nThis is a Computer Generated Invoice", mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			// Paragraph paragraphFooter1 = new Paragraph("This is a Computer Generated Invoice", mediumNormal);
			// paragraphFooter1.setAlignment(Element.ALIGN_CENTER);
			// paragraphFooter1.add(new Paragraph(" "));
			// document.add(paragraphFooter1);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	
		
	}
	
	public static File generateInvoicePdfCreditNote(BillPrintDataModel billPrintDataModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			Company company=new Company();
	
			if(billPrintDataModel.getBusinessName()!=null){
				company=billPrintDataModel.getBusinessName().getCompany();
			}else{
				company=billPrintDataModel.getEmployeeGKCounterOrder().getCompany();
			}
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("Credit Note", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(company.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(company.getAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getTelephoneNumber()==null)?"--":company.getContact().getTelephoneNumber()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getGstinno()==null)?"--":company.getGstinno()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getEmailId()==null)?"--":company.getContact().getEmailId()+" \n",mediumNormal));
			/*ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));*/
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Credit Note No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(""+billPrintDataModel.getCreditNoteNumber(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Credit Note Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(""+billPrintDataModel.getReturnDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Invoice Number \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(""+billPrintDataModel.getInvoiceNumber()+" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Invoice Date \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(""+billPrintDataModel.getOrderDate()+" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			/*PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(billPrintDataModel.getTransporterGstNumber()+" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk((billPrintDataModel.getDocketNo()==null)?"NA":billPrintDataModel.getDocketNo()+" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	*/
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			if(billPrintDataModel.getBusinessName()!=null){
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getShopName()+" \n", mediumBold));
				
				/*for(String addressLine : billPrintDataModel.getAddressLineList())
				{
					buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
				}*/
				
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getAddress()+"\n",mediumNormal));
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getArea().getName()+" ,",mediumNormal));
				buyerPhrase.add(new Paragraph(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getName()+" - "+""+billPrintDataModel.getBusinessName().getArea().getPincode()+",\n",mediumNormal));
				
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
				buyerPhrase.add(new Chunk("Code : ", mediumBold));
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
				
				if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{	
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"/"+""+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()==null)
				{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()==null && ""+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else 
				{
					buyerPhrase.add(new Chunk("\n",mediumNormal));
				}
			
				/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getBusinessName().getGstinNumber(),mediumNormal));
			}else{
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getCustomerName()+" \n", mediumBold));
				buyerPhrase.add(new Chunk("Mobile No. : ", mediumBold));				
				buyerPhrase.add(new Chunk(""+billPrintDataModel.getCustomerMobileNumber()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				if(billPrintDataModel.getCustomerGstNumber()==null){
					buyerPhrase.add(new Chunk(" NA",mediumNormal));
				}else{
					buyerPhrase.add(new Chunk(""+billPrintDataModel.getCustomerGstNumber(),mediumNormal));	
				}
				
			}
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Buyer Order No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(""+billPrintDataModel.getOrderNumber(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Delivery Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(""+billPrintDataModel.getDeliveryDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			//comment
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			buyerSideTableCell3.setColspan(2);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Comment \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(""+billPrintDataModel.getComment(),mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			/*PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			//buyerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Despatch Document No. \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			PdfPCell buyerSideTableCell4=new TopLeftBorderPDFCell();
			//buyerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell4Phrase = new Phrase();
			buyerSideTableCell4Phrase.setLeading(12);
			buyerSideTableCell4Phrase.add(new Chunk("Delivery Note Date \n", mediumBold));
			buyerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell4.addElement(buyerSideTableCell4Phrase);
			buyerSideTable.addCell(buyerSideTableCell4);			
			
			PdfPCell buyerSideTableCell5=new TopRightBorderPDFCell();
			//buyerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell5Phrase = new Phrase();
			buyerSideTableCell5Phrase.setLeading(12);
			buyerSideTableCell5Phrase.add(new Chunk("Despatch through \n", mediumBold));
			buyerSideTableCell5Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell5.addElement(buyerSideTableCell5Phrase);
			buyerSideTable.addCell(buyerSideTableCell5);
			
			PdfPCell buyerSideTableCell6=new TopLeftBorderPDFCell();
			//buyerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell6Phrase = new Phrase();
			buyerSideTableCell6Phrase.setLeading(12);
			buyerSideTableCell6Phrase.add(new Chunk("Destination \n", mediumBold));
			buyerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell6.addElement(buyerSideTableCell6Phrase);
			buyerSideTable.addCell(buyerSideTableCell6);	*/
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			//table size array
			float[] tableColSize=new float[] { 14,80, 24 ,15,18,15, 18,18,18,30};
			PdfPTable productDetailsTable = new PdfPTable(tableColSize);
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr. No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("MRP", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Qty.", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCel56=new RightBorderPDFCell();
			Paragraph productDetailsTableCell56Phrase = new Paragraph();
			productDetailsTableCell56Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell56Phrase.setLeading(12);
			productDetailsTableCell56Phrase.add(new Chunk("Total\nAmt.", mediumBold));
			productDetailsTableCel56.addElement(productDetailsTableCell56Phrase);
			productDetailsTable.addCell(productDetailsTableCel56);
		
			PdfPCell productDetailsTableCellDiscPer=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPerPhrase = new Paragraph();
			productDetailsTableCellDiscPerPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPerPhrase.setLeading(12);
			productDetailsTableCellDiscPerPhrase.add(new Chunk("Disc. (%)", mediumBold));
			productDetailsTableCellDiscPer.addElement(productDetailsTableCellDiscPerPhrase);
			productDetailsTable.addCell(productDetailsTableCellDiscPer);
			
			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.\nAmt.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Net Payable", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			for(ProductListForBill productListForBill: billPrintDataModel.getProductListForBill())
			{
				productDetailsTable = new PdfPTable(tableColSize);
				
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productListForBill.getSrno(), mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productListForBill.getProductName(), mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productListForBill.getHsnCode(), mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productListForBill.getTaxSlab()+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productListForBill.getRatePerProduct(), mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productListForBill.getQuantityIssued(), mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell52Phrase = new Paragraph();
				productDetailsTableCell52Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell52Phrase.setLeading(12);
				productDetailsTableCell52Phrase.add(new Chunk(productListForBill.getAmountWithoutTax(), mediumNormal));
				productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
				productDetailsTable.addCell(productDetailsTableCell52);
				
				PdfPCell productDetailsTableCell59=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell59Phrase = new Paragraph();
				productDetailsTableCell59Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell59Phrase.setLeading(12);
				productDetailsTableCell59Phrase.add(new Chunk(productListForBill.getDiscountPer(), mediumNormal));
				productDetailsTableCell59.addElement(productDetailsTableCell59Phrase);
				productDetailsTable.addCell(productDetailsTableCell59);
				
				PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk(productListForBill.getDiscountAmt(), mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_RIGHT);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk(productListForBill.getNetPayable(), mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
				
				productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
				productDetailsTableCellTemp.setPadding(0);
				productDetailsTableCellTemp.setColspan(2);
				mainTable.addCell(productDetailsTableCellTemp);
			}
			
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Total", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCell34Phrase = new Paragraph();
			productDetailsTableCell34Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk(""+billPrintDataModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);
			
			PdfPCell productDetailsTableCell57=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell57Phrase = new Phrase();
			productDetailsTableCell57Phrase.setLeading(12);
			productDetailsTableCell57Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell57.addElement(productDetailsTableCell57Phrase);
			productDetailsTable.addCell(productDetailsTableCell57);
			
			PdfPCell productDetailsTableCell60=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell60Phrase = new Phrase();
			productDetailsTableCell60Phrase.setLeading(12);
			productDetailsTableCell60Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell60.addElement(productDetailsTableCell60Phrase);
			productDetailsTable.addCell(productDetailsTableCell60);
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Paragraph productDetailsTableCell35Phrase = new Paragraph();
			productDetailsTableCell35Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmountWithoutTax(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//discount section
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCellMainDisc1=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc1Phrase = new Phrase();
			productDetailsTableCellMainDisc1Phrase.setLeading(12);
			productDetailsTableCellMainDisc1Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc1.addElement(productDetailsTableCellMainDisc1Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc1);
			
			PdfPCell productDetailsTableCellMainDisc2=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc2Phrase = new Phrase();
			productDetailsTableCellMainDisc2Phrase.setLeading(12);
			productDetailsTableCellMainDisc2Phrase.add(new Chunk("           Discount", mediumBold));
			productDetailsTableCellMainDisc2.addElement(productDetailsTableCellMainDisc2Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc2);
			
			PdfPCell productDetailsTableCellMainDisc3=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc3Phrase = new Phrase();
			productDetailsTableCellMainDisc3Phrase.setLeading(12);
			productDetailsTableCellMainDisc3Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc3.addElement(productDetailsTableCellMainDisc3Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc3);
			
			PdfPCell productDetailsTableCellMainDisc4=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc4Phrase = new Phrase();
			productDetailsTableCellMainDisc4Phrase.setLeading(12);
			productDetailsTableCellMainDisc4Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc4.addElement(productDetailsTableCellMainDisc4Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc4);
			
			PdfPCell productDetailsTableCellMainDisc5=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc5Phrase = new Phrase();
			productDetailsTableCellMainDisc5Phrase.setLeading(12);
			productDetailsTableCellMainDisc5Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc5.addElement(productDetailsTableCellMainDisc5Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc5);
			
			PdfPCell productDetailsTableCellMainDisc6=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc6Phrase = new Phrase();
			productDetailsTableCellMainDisc6Phrase.setLeading(12);
			productDetailsTableCellMainDisc6Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc6.addElement(productDetailsTableCellMainDisc6Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc6);
			
			PdfPCell productDetailsTableCellMainDisc7=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc7Phrase = new Phrase();
			productDetailsTableCellMainDisc7Phrase.setLeading(12);
			productDetailsTableCellMainDisc7Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc7.addElement(productDetailsTableCellMainDisc7Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc7);
			
			PdfPCell productDetailsTableCellMainDisc8=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc8Phrase = new Paragraph();
			productDetailsTableCellMainDisc8Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellMainDisc8Phrase.setLeading(12);
			productDetailsTableCellMainDisc8Phrase.add(new Chunk(""+billPrintDataModel.getTotalPercentageOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc8.addElement(productDetailsTableCellMainDisc8Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc8);
			
			PdfPCell productDetailsTableCellMainDisc9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc9Phrase = new Phrase();
			productDetailsTableCellMainDisc9Phrase.setLeading(12);
			productDetailsTableCellMainDisc9Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc9.addElement(productDetailsTableCellMainDisc9Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc9);
			
			PdfPCell productDetailsTableCellMainDisc10=new TopBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc10Phrase = new Paragraph();
			productDetailsTableCellMainDisc10Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCellMainDisc10Phrase.setLeading(12);
			productDetailsTableCellMainDisc10Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmountOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc10.addElement(productDetailsTableCellMainDisc10Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc10);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell63=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell63Phrase = new Phrase();
			productDetailsTableCell63Phrase.setLeading(12);
			productDetailsTableCell63Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell63.addElement(productDetailsTableCell63Phrase);
			productDetailsTable.addCell(productDetailsTableCell63);
			
			PdfPCell productDetailsTableCell64=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell64Phrase = new Phrase();
			productDetailsTableCell64Phrase.setLeading(12);
			productDetailsTableCell64Phrase.add(new Chunk("           Net Total Amount", mediumBold));
			productDetailsTableCell64.addElement(productDetailsTableCell64Phrase);
			productDetailsTable.addCell(productDetailsTableCell64);
			
			PdfPCell productDetailsTableCell65=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell65Phrase = new Phrase();
			productDetailsTableCell65Phrase.setLeading(12);
			productDetailsTableCell65Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell65.addElement(productDetailsTableCell65Phrase);
			productDetailsTable.addCell(productDetailsTableCell65);
			
			PdfPCell productDetailsTableCell66=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell67Phrase = new Phrase();
			productDetailsTableCell67Phrase.setLeading(12);
			productDetailsTableCell67Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell66.addElement(productDetailsTableCell67Phrase);
			productDetailsTable.addCell(productDetailsTableCell66);
			
			PdfPCell productDetailsTableCell68=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell69Phrase = new Phrase();
			productDetailsTableCell69Phrase.setLeading(12);
			productDetailsTableCell69Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell68.addElement(productDetailsTableCell69Phrase);
			productDetailsTable.addCell(productDetailsTableCell68);
			
			PdfPCell productDetailsTableCell70=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell70Phrase = new Phrase();
			productDetailsTableCell70Phrase.setLeading(12);
			productDetailsTableCell70Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell70.addElement(productDetailsTableCell70Phrase);
			productDetailsTable.addCell(productDetailsTableCell70);
			
			PdfPCell productDetailsTableCell71=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell71Phrase = new Phrase();
			productDetailsTableCell71Phrase.setLeading(12);
			productDetailsTableCell71Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell71.addElement(productDetailsTableCell71Phrase);
			productDetailsTable.addCell(productDetailsTableCell71);
			
			PdfPCell productDetailsTableCell72=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell72Phrase = new Phrase();
			productDetailsTableCell72Phrase.setLeading(12);
			productDetailsTableCell72Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell72.addElement(productDetailsTableCell72Phrase);
			productDetailsTable.addCell(productDetailsTableCell72);
			
			PdfPCell productDetailsTableCell73=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell73Phrase = new Phrase();
			productDetailsTableCell73Phrase.setLeading(12);
			productDetailsTableCell73Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell73.addElement(productDetailsTableCell73Phrase);
			productDetailsTable.addCell(productDetailsTableCell73);
			
			PdfPCell productDetailsTableCell74=new TopBorderPDFCell();
			Paragraph productDetailsTableCell74Phrase = new Paragraph();
			productDetailsTableCell74Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell74Phrase.setLeading(12);
			productDetailsTableCell74Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmountWithTaxWithDiscNonRoundOff(), mediumBold));
			productDetailsTableCell74.addElement(productDetailsTableCell74Phrase);
			productDetailsTable.addCell(productDetailsTableCell74);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			/*//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
			PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
			PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);*/
			
			//roundOf
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);
			
			PdfPCell productDetailsTableCell56=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell58Phrase = new Phrase();
			productDetailsTableCell58Phrase.setLeading(12);
			productDetailsTableCell58Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell56.addElement(productDetailsTableCell58Phrase);
			productDetailsTable.addCell(productDetailsTableCell56);
			
			PdfPCell productDetailsTableCell61=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell61Phrase = new Phrase();
			productDetailsTableCell61Phrase.setLeading(12);
			productDetailsTableCell61Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell61.addElement(productDetailsTableCell61Phrase);
			productDetailsTable.addCell(productDetailsTableCell61);
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Paragraph productDetailsTableCell53Phrase = new Paragraph();
			productDetailsTableCell53Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("("+""+billPrintDataModel.getRoundOffAmount()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalamountWithtax
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Net Payable Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);

			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);
			
			PdfPCell productDetailsTableCell58=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell59Phrase = new Phrase();
			productDetailsTableCell59Phrase.setLeading(12);
			productDetailsTableCell59Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell58.addElement(productDetailsTableCell59Phrase);
			productDetailsTable.addCell(productDetailsTableCell58);
			
			PdfPCell productDetailsTableCell62=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell62Phrase = new Phrase();
			productDetailsTableCell62Phrase.setLeading(12);
			productDetailsTableCell62Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell62.addElement(productDetailsTableCell62Phrase);
			productDetailsTable.addCell(productDetailsTableCell62);
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Paragraph productDetailsTableCell42Phrase = new Paragraph();
			productDetailsTableCell42Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmountWithTaxRoundOff(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+""+billPrintDataModel.getTotalAmountWithTaxInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(CategoryWiseAmountForBill categoryWiseAmountForBill:billPrintDataModel.getCategoryWiseAmountForBills())
			{
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.getHsnCode(), mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.getTaxableValue(), mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstRate(), mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstRate(), mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstRate(), mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(""+billPrintDataModel.getTotalAmount(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(""+billPrintDataModel.getTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(""+billPrintDataModel.getTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(""+billPrintDataModel.getTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
			
			/* disc amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell14=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell14Phrase = new Paragraph();
			taxDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell14Phrase.setLeading(10);
			taxDetailsTableCell14Phrase.add(new Chunk("Discount", mediumBold));
			taxDetailsTableCell14.addElement(taxDetailsTableCell14Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell14);
			
			PdfPCell taxDetailsTableCell15=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell15Phrase = new Paragraph();
			taxDetailsTableCell15Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell15Phrase.setLeading(10);
			taxDetailsTableCell15Phrase.add(new Chunk(billPrintDataModel.getTotalAmountDisc(), mediumNormal));
			taxDetailsTableCell15.addElement(taxDetailsTableCell15Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell15);
			
			PdfPTable cgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell16=new RightBorderPDFCell();
			Phrase taxDetailsTableCell16Phrase = new Phrase();
			taxDetailsTableCell16Phrase.setLeading(10);
			taxDetailsTableCell16Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell16.addElement(taxDetailsTableCell16Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell16);
			
			PdfPCell taxDetailsTableCell17=new PdfPCell();
			taxDetailsTableCell17.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell17Phrase = new Paragraph();
			taxDetailsTableCell17Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell17Phrase.setLeading(10);
			taxDetailsTableCell17Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell17.addElement(taxDetailsTableCell17Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell17);
			
			PdfPCell cgstValueTableCellDisc=new PdfPCell(cgstValueTableDisc);
			cgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellDisc);
			
			PdfPTable sgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell18=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell18Phrase = new Paragraph();
			taxDetailsTableCell18Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell18Phrase.setLeading(10);
			taxDetailsTableCell18Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell18.addElement(taxDetailsTableCell18Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell18);
			
			PdfPCell taxDetailsTableCell19=new PdfPCell();
			taxDetailsTableCell19.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell19Phrase = new Paragraph();
			taxDetailsTableCell19Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell19Phrase.setLeading(10);
			taxDetailsTableCell19Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell19.addElement(taxDetailsTableCell19Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell19);
			
			PdfPCell sgstValueTableCellDisc=new PdfPCell(sgstValueTableDisc);
			sgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellDisc);
			
			PdfPTable igstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			igstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell20=new RightBorderPDFCell();
			Phrase taxDetailsTableCell20Phrase = new Phrase();
			taxDetailsTableCell20Phrase.setLeading(10);
			taxDetailsTableCell20Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell20.addElement(taxDetailsTableCell20Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell20);
			
			PdfPCell taxDetailsTableCell21=new PdfPCell();
			taxDetailsTableCell21.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell21Phrase = new Paragraph();
			taxDetailsTableCell21Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell21Phrase.setLeading(10);
			taxDetailsTableCell21Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell21.addElement(taxDetailsTableCell21Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell21);
			
			PdfPCell igstValueTableCellDisc=new PdfPCell(igstValueTableDisc);
			igstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellDisc);
			/* disc amount in hsn code wise part end*/
			
			/* net amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell22=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell22Phrase = new Paragraph();
			taxDetailsTableCell22Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell22Phrase.setLeading(10);
			taxDetailsTableCell22Phrase.add(new Chunk("Net Total", mediumBold));
			taxDetailsTableCell22.addElement(taxDetailsTableCell22Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell22);
			
			PdfPCell taxDetailsTableCell23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell23Phrase = new Paragraph();
			taxDetailsTableCell23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell23Phrase.setLeading(10);
			taxDetailsTableCell23Phrase.add(new Chunk(billPrintDataModel.getNetTotalAmount(), mediumNormal));
			taxDetailsTableCell23.addElement(taxDetailsTableCell23Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell23);
			
			PdfPTable cgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell24=new RightBorderPDFCell();
			Phrase taxDetailsTableCell24Phrase = new Phrase();
			taxDetailsTableCell24Phrase.setLeading(10);
			taxDetailsTableCell24Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell24.addElement(taxDetailsTableCell24Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell24);
			
			PdfPCell taxDetailsTableCell25=new PdfPCell();
			taxDetailsTableCell25.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell25Phrase = new Paragraph();
			taxDetailsTableCell25Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell25Phrase.setLeading(10);
			taxDetailsTableCell25Phrase.add(new Chunk(billPrintDataModel.getNetTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell25.addElement(taxDetailsTableCell25Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell25);
			
			PdfPCell cgstValueTableCellNet=new PdfPCell(cgstValueTableNet);
			cgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellNet);
			
			PdfPTable sgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell26=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell26Phrase = new Paragraph();
			taxDetailsTableCell26Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell26Phrase.setLeading(10);
			taxDetailsTableCell26Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell26.addElement(taxDetailsTableCell26Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell26);
			
			PdfPCell taxDetailsTableCell27=new PdfPCell();
			taxDetailsTableCell27.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell27Phrase = new Paragraph();
			taxDetailsTableCell27Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell27Phrase.setLeading(10);
			taxDetailsTableCell27Phrase.add(new Chunk(billPrintDataModel.getNetTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell27.addElement(taxDetailsTableCell27Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell27);
			
			PdfPCell sgstValueTableCellNet=new PdfPCell(sgstValueTableNet);
			sgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellNet);
			
			PdfPTable igstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			igstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell28=new RightBorderPDFCell();
			Phrase taxDetailsTableCell28Phrase = new Phrase();
			taxDetailsTableCell28Phrase.setLeading(10);
			taxDetailsTableCell28Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell28.addElement(taxDetailsTableCell28Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell28);
			
			PdfPCell taxDetailsTableCell29=new PdfPCell();
			taxDetailsTableCell29.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell29Phrase = new Paragraph();
			taxDetailsTableCell29Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell29Phrase.setLeading(10);
			taxDetailsTableCell29Phrase.add(new Chunk(billPrintDataModel.getNetTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell29.addElement(taxDetailsTableCell29Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell29);
			
			PdfPCell igstValueTableCellNet=new PdfPCell(igstValueTableNet);
			igstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellNet);
			/* net amount in hsn code wise part end*/		
			
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+""+billPrintDataModel.getTaxAmountInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			/*PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
			lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			lastRowTable.setWidthPercentage(100);
			
			PdfPCell declarationTableCell=new RightBorderPDFCell();
			Phrase declarationTableCellPhrase = new Phrase();
			declarationTableCellPhrase.setLeading(12);
			declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
			declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct", mediumNormal));			
			declarationTableCell.addElement(declarationTableCellPhrase);		
			lastRowTable.addCell(declarationTableCell);
			
			PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			lastRowTable.addCell(signTableCell);*/
			
			PdfPTable bankAndSignDetailsTable = new PdfPTable(new float[] { 60 , 40});
			bankAndSignDetailsTable.setWidthPercentage(100);
			
			
			PdfPTable bankDeailsTable = new PdfPTable(new float[] { 20,40 ,40});
			bankDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			bankDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellBank1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank1Phrase = new Paragraph();
			taxDetailsTableCellBank1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank1Phrase.setLeading(10);
			taxDetailsTableCellBank1Phrase.add(new Chunk("Bank Name", mediumBold));
			taxDetailsTableCellBank1.addElement(taxDetailsTableCellBank1Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank1);	
			
			PdfPCell taxDetailsTableCellBank2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank2Phrase = new Paragraph();
			taxDetailsTableCellBank2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank2Phrase.setLeading(10);
			taxDetailsTableCellBank2Phrase.add(new Chunk("Kotak Mahindra Bank", mediumNormal));
			taxDetailsTableCellBank2.addElement(taxDetailsTableCellBank2Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank2);
			
			PdfPCell taxDetailsTableCellBank3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank3Phrase = new Paragraph();
			taxDetailsTableCellBank3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank3Phrase.setLeading(10);
			taxDetailsTableCellBank3Phrase.add(new Chunk("HDFC", mediumNormal));
			taxDetailsTableCellBank3.addElement(taxDetailsTableCellBank3Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank3);
			
			PdfPCell taxDetailsTableCellBank4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank4Phrase = new Paragraph();
			taxDetailsTableCellBank4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank4Phrase.setLeading(10);
			taxDetailsTableCellBank4Phrase.add(new Chunk("A/C Name", mediumBold));
			taxDetailsTableCellBank4.addElement(taxDetailsTableCellBank4Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank4);	
			
			PdfPCell taxDetailsTableCellBank5=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank5Phrase = new Paragraph();
			taxDetailsTableCellBank5Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank5Phrase.setLeading(10);
			taxDetailsTableCellBank5Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank5.addElement(taxDetailsTableCellBank5Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank5);
			
			PdfPCell taxDetailsTableCellBank6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank6Phrase = new Paragraph();
			taxDetailsTableCellBank6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank6Phrase.setLeading(10);
			taxDetailsTableCellBank6Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank6.addElement(taxDetailsTableCellBank6Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank6);
			
			PdfPCell taxDetailsTableCellBank7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank7Phrase = new Paragraph();
			taxDetailsTableCellBank7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank7Phrase.setLeading(10);
			taxDetailsTableCellBank7Phrase.add(new Chunk("A/C No.", mediumBold));
			taxDetailsTableCellBank7.addElement(taxDetailsTableCellBank7Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank7);	
			
			PdfPCell taxDetailsTableCellBank8=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank8Phrase = new Paragraph();
			taxDetailsTableCellBank8Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank8Phrase.setLeading(10);
			taxDetailsTableCellBank8Phrase.add(new Chunk("39876326723", mediumNormal));
			taxDetailsTableCellBank8.addElement(taxDetailsTableCellBank8Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank8);
			
			PdfPCell taxDetailsTableCellBank9=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank9Phrase = new Paragraph();
			taxDetailsTableCellBank9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank9Phrase.setLeading(10);
			taxDetailsTableCellBank9Phrase.add(new Chunk("23783728782738", mediumNormal));
			taxDetailsTableCellBank9.addElement(taxDetailsTableCellBank9Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank9);
			
			PdfPCell taxDetailsTableCellBank10=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank10Phrase = new Paragraph();
			taxDetailsTableCellBank10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank10Phrase.setLeading(10);
			taxDetailsTableCellBank10Phrase.add(new Chunk("IFSC", mediumBold));
			taxDetailsTableCellBank10.addElement(taxDetailsTableCellBank10Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank10);	
			
			PdfPCell taxDetailsTableCellBank11=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank11Phrase = new Paragraph();
			taxDetailsTableCellBank11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank11Phrase.setLeading(10);
			taxDetailsTableCellBank11Phrase.add(new Chunk("KKBK0000667", mediumNormal));
			taxDetailsTableCellBank11.addElement(taxDetailsTableCellBank11Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank11);
			
			PdfPCell taxDetailsTableCellBank12=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank12Phrase = new Paragraph();
			taxDetailsTableCellBank12Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank12Phrase.setLeading(10);
			taxDetailsTableCellBank12Phrase.add(new Chunk("HDFC0000002", mediumNormal));
			taxDetailsTableCellBank12.addElement(taxDetailsTableCellBank12Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank12);
			
			PdfPCell bankDetailsMainCell=new PdfPCell(bankDeailsTable);
			bankDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(bankDetailsMainCell);			
			
			PdfPTable signDeailsTable = new PdfPTable(new float[] { 50 ,50});
			signDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			signDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellSign1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign1Phrase = new Paragraph();
			taxDetailsTableCellSign1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign1Phrase.setLeading(10);
			taxDetailsTableCellSign1Phrase.add(new Chunk("\n\n\n\n", mediumNormal));
			taxDetailsTableCellSign1.addElement(taxDetailsTableCellSign1Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign1);
			
			/*PdfPCell taxDetailsTableCellSign23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign23Phrase = new Paragraph();
			taxDetailsTableCellSign23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign23Phrase.setLeading(10);
			taxDetailsTableCellSign23Phrase.add(new Chunk("for"+company.getCompanyName(), mediumBold));
			taxDetailsTableCellSign23.addElement(taxDetailsTableCellSign23Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign23);*/
			
			PdfPCell taxDetailsTableCellSign2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign2Phrase = new Paragraph();
			taxDetailsTableCellSign2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign2Phrase.setLeading(10);
			taxDetailsTableCellSign2Phrase.add(new Chunk("For "+company.getCompanyName()+"\n\n\n\n", mediumBold));
			taxDetailsTableCellSign2.addElement(taxDetailsTableCellSign2Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign2);	
			
			PdfPCell taxDetailsTableCellSign3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign3Phrase = new Paragraph();
			taxDetailsTableCellSign3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign3Phrase.setLeading(10);
			taxDetailsTableCellSign3Phrase.add(new Chunk("Receiver's Signature", mediumNormal));
			taxDetailsTableCellSign3.addElement(taxDetailsTableCellSign3Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign3);
			
			PdfPCell taxDetailsTableCellSign4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign4Phrase = new Paragraph();
			taxDetailsTableCellSign4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign4Phrase.setLeading(10);
			taxDetailsTableCellSign4Phrase.add(new Chunk("Authorized Signatory", mediumNormal));
			taxDetailsTableCellSign4.addElement(taxDetailsTableCellSign4Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign4);
			
			PdfPCell signDetailsMainCell=new PdfPCell(signDeailsTable);
			signDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(signDetailsMainCell);
			
			/*PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			bankDeailsTable.addCell(signTableCell);*/
			
			PdfPCell lastRowMainCell=new PdfPCell(bankAndSignDetailsTable);
			lastRowMainCell.setColspan(2);
			lastRowMainCell.setPadding(0);
			mainTable.addCell(lastRowMainCell);			
			
			document.add(mainTable);
			
			Paragraph paragraphFooter = new Paragraph("This is a Computer Generated Invoice", mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	}
	
	static class BottomRightBorderPDFCell extends PdfPCell
	{
		public BottomRightBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT|BOTTOM);
		}
	}
	
	static class BottomRightLeftBorderPDFCell extends PdfPCell
	{
		public BottomRightLeftBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT|BOTTOM|LEFT);
		}
	}
	
	static class BottomLeftBorderPDFCell extends PdfPCell
	{
		public BottomLeftBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(BOTTOM|LEFT);
		}
	}
	
	static class BottomRightTopBorderPDFCell extends PdfPCell
	{
		public BottomRightTopBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT|TOP|BOTTOM);
		}
	}
	
	static class BottomLeftTopBorderPDFCell extends PdfPCell
	{
		public BottomLeftTopBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(TOP|BOTTOM|LEFT);
		}
	}
	
	static class TopRightBorderPDFCell extends PdfPCell
	{
		public TopRightBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT|TOP);
		}
	}
	
	static class LeftRightTopBorderPDFCell extends PdfPCell
	{
		public LeftRightTopBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT|TOP|LEFT);
		}
	}
	
	static class TopLeftBorderPDFCell extends PdfPCell
	{
		public TopLeftBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(TOP|LEFT);
		}
	}
	
	static class TopBorderPDFCell extends PdfPCell
	{
		public TopBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(TOP);
		}
	}
	
	static class LeftBorderPDFCell extends PdfPCell
	{
		public LeftBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(LEFT);
		}
	}
	
	static class RightBorderPDFCell extends PdfPCell
	{
		public RightBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(RIGHT);
		}
	}
	
	static class BottomBorderPDFCell extends PdfPCell
	{
		public BottomBorderPDFCell() {
			// TODO Auto-generated constructor stub
			this.setBorder(BOTTOM);
		}
	}
	
/*	public class MyFooter extends PdfPageEventHelper {
	    Font ffont = new Font(Font.FontFamily.UNDEFINED, 5, Font.ITALIC);
	 
	    public void onEndPage(PdfWriter writer, Document document) {
	        PdfContentByte cb = writer.getDirectContent();
	        //Phrase header = new Phrase();
	        
	        Phrase footer = new Phrase("this is a footer", ffont);
	        
PdfPTable header = new PdfPTable(2);
try {
	header.setWidths(new int[]{10, 15});
} catch (DocumentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
header.setTotalWidth(527);
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			header.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk("Ramdev Enterprises \n", mediumBold));
			ownerPhrase.add(new Paragraph("Shreenath Bhavan,Shop-3,\n"
									 +"37/43 Bora Bazar Street,Fort,Mumbai,\n"
									 +"Pin-400001.\n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk("022-66558189/66105235\n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk("27AGUPA0945J1Z6\n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk("ramdevent2009@gmail.com\n",mediumNormal));
			ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk("AGUPA0945J",mediumNormal));
			ownerInfo.addElement(ownerPhrase);
			
			header.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new PdfPCell();
			//ownerSideTableCell1.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Invoice No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk("dfdf",mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk("fdf",mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new PdfPCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Delivery Note \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new PdfPCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk(" ", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			PdfPCell ownerSideTableCell5=new PdfPCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Supplier's Ref. ", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(" ",smallNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new PdfPCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Other Reference(s) ", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	
			
			header.addCell(ownerSideTable);
			//header.add(mainTable);
			header.writeSelectedRows(0, -1, 
					52,
	                document.getPageSize().getHeight()-15, writer.getDirectContent());
	        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
	                header,
	                (document.right() - document.left()) / 2 + document.leftMargin(),
	                document.top() + 10, 0);
	        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
	                footer,
	                (document.right() - document.left()) / 2 + document.leftMargin(),
	                document.bottom() - 10, 0);
	    }
	    
	    
	}*/
	

	//Proforma Order Invoice
	public static File generateInvoicePdfProformaOrder(BillPrintDataModel billPrintDataModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			Company company=new Company();
	
			if(billPrintDataModel.getBusinessName()!=null){
				company=billPrintDataModel.getBusinessName().getCompany();
			}else{
				company=billPrintDataModel.getEmployeeGKCounterOrder().getCompany();
			}
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("PRO-FORMA INVOICE", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(company.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(company.getAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getTelephoneNumber()==null)?"--":company.getContact().getTelephoneNumber()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getGstinno()==null)?"--":company.getGstinno()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((company.getContact().getEmailId()==null)?"--":company.getContact().getEmailId()+" \n",mediumNormal));
			/*ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));*/
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Invoice No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getInvoiceNumber(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getOrderDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Transportation Name \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(billPrintDataModel.getTransporterName()+" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Vehicle Number \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(billPrintDataModel.getVehicalNumber()+" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(billPrintDataModel.getTransporterGstNumber()+" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk((billPrintDataModel.getDocketNo()==null)?"NA":billPrintDataModel.getDocketNo()+" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			if(billPrintDataModel.getBusinessName()!=null){
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getShopName()+" \n", mediumBold));
				
				/*for(String addressLine : billPrintDataModel.getAddressLineList())
				{
					buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
				}*/
				
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getAddress()+"\n",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getName()+" ,",mediumNormal));
				buyerPhrase.add(new Paragraph(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getName()+" - "+billPrintDataModel.getBusinessName().getArea().getPincode()+",\n",mediumNormal));
				
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
				buyerPhrase.add(new Chunk("Code : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
				
				if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{	
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"/"+billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()!=null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()==null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getMobileNumber()+"\n",mediumNormal));
				}
				else if(billPrintDataModel.getBusinessName().getContact().getMobileNumber()==null && billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()!=null)
				{
					buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getContact().getTelephoneNumber()+"\n",mediumNormal));
				}
				else 
				{
					buyerPhrase.add(new Chunk("\n",mediumNormal));
				}
			
				/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getGstinNumber(),mediumNormal));
			}else{
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerName()+" \n", mediumBold));
				buyerPhrase.add(new Chunk("Mobile No. : ", mediumBold));				
				buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerMobileNumber()+"\n",mediumNormal));
				buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
				if(billPrintDataModel.getCustomerGstNumber()==null){
					buyerPhrase.add(new Chunk(" NA",mediumNormal));
				}else{
					buyerPhrase.add(new Chunk(billPrintDataModel.getCustomerGstNumber(),mediumNormal));	
				}
				
			}
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Buyer Order No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(billPrintDataModel.getOrderNumber(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Delivery Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(billPrintDataModel.getDeliveryDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			//buyerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Despatch Document No. \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			PdfPCell buyerSideTableCell4=new TopLeftBorderPDFCell();
			//buyerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell4Phrase = new Phrase();
			buyerSideTableCell4Phrase.setLeading(12);
			buyerSideTableCell4Phrase.add(new Chunk("Delivery Note Date \n", mediumBold));
			buyerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell4.addElement(buyerSideTableCell4Phrase);
			buyerSideTable.addCell(buyerSideTableCell4);			
			
			PdfPCell buyerSideTableCell5=new TopRightBorderPDFCell();
			//buyerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell5Phrase = new Phrase();
			buyerSideTableCell5Phrase.setLeading(12);
			buyerSideTableCell5Phrase.add(new Chunk("Despatch through \n", mediumBold));
			buyerSideTableCell5Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell5.addElement(buyerSideTableCell5Phrase);
			buyerSideTable.addCell(buyerSideTableCell5);
			
			PdfPCell buyerSideTableCell6=new TopLeftBorderPDFCell();
			//buyerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell6Phrase = new Phrase();
			buyerSideTableCell6Phrase.setLeading(12);
			buyerSideTableCell6Phrase.add(new Chunk("Destination \n", mediumBold));
			buyerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell6.addElement(buyerSideTableCell6Phrase);
			buyerSideTable.addCell(buyerSideTableCell6);	
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			//table size array
			float[] tableColSize=new float[] { 14,80, 24 ,15,18,15, 18,18,18,30};
			PdfPTable productDetailsTable = new PdfPTable(tableColSize);
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr. No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("MRP", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Qty.", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCel56=new RightBorderPDFCell();
			Paragraph productDetailsTableCell56Phrase = new Paragraph();
			productDetailsTableCell56Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell56Phrase.setLeading(12);
			productDetailsTableCell56Phrase.add(new Chunk("Total\nAmt.", mediumBold));
			productDetailsTableCel56.addElement(productDetailsTableCell56Phrase);
			productDetailsTable.addCell(productDetailsTableCel56);
		
			PdfPCell productDetailsTableCellDiscPer=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPerPhrase = new Paragraph();
			productDetailsTableCellDiscPerPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPerPhrase.setLeading(12);
			productDetailsTableCellDiscPerPhrase.add(new Chunk("Disc. (%)", mediumBold));
			productDetailsTableCellDiscPer.addElement(productDetailsTableCellDiscPerPhrase);
			productDetailsTable.addCell(productDetailsTableCellDiscPer);
			
			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.\nAmt.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Net Payable", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			for(ProductListForBill productListForBill: billPrintDataModel.getProductListForBill())
			{
				productDetailsTable = new PdfPTable(tableColSize);
				
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productListForBill.getSrno(), mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productListForBill.getProductName(), mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productListForBill.getHsnCode(), mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productListForBill.getTaxSlab()+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productListForBill.getRatePerProduct(), mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productListForBill.getQuantityIssued(), mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell52Phrase = new Paragraph();
				productDetailsTableCell52Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell52Phrase.setLeading(12);
				productDetailsTableCell52Phrase.add(new Chunk(productListForBill.getAmountWithoutTax(), mediumNormal));
				productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
				productDetailsTable.addCell(productDetailsTableCell52);
				
				PdfPCell productDetailsTableCell59=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell59Phrase = new Paragraph();
				productDetailsTableCell59Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell59Phrase.setLeading(12);
				productDetailsTableCell59Phrase.add(new Chunk(productListForBill.getDiscountPer(), mediumNormal));
				productDetailsTableCell59.addElement(productDetailsTableCell59Phrase);
				productDetailsTable.addCell(productDetailsTableCell59);
				
				PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk(productListForBill.getDiscountAmt(), mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_RIGHT);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk(productListForBill.getNetPayable(), mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
				
				productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
				productDetailsTableCellTemp.setPadding(0);
				productDetailsTableCellTemp.setColspan(2);
				mainTable.addCell(productDetailsTableCellTemp);
			}
			
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Total", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCell34Phrase = new Paragraph();
			productDetailsTableCell34Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk(billPrintDataModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);
			
			PdfPCell productDetailsTableCell57=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell57Phrase = new Phrase();
			productDetailsTableCell57Phrase.setLeading(12);
			productDetailsTableCell57Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell57.addElement(productDetailsTableCell57Phrase);
			productDetailsTable.addCell(productDetailsTableCell57);
			
			PdfPCell productDetailsTableCell60=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell60Phrase = new Phrase();
			productDetailsTableCell60Phrase.setLeading(12);
			productDetailsTableCell60Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell60.addElement(productDetailsTableCell60Phrase);
			productDetailsTable.addCell(productDetailsTableCell60);
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Paragraph productDetailsTableCell35Phrase = new Paragraph();
			productDetailsTableCell35Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithoutTax(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//discount section
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCellMainDisc1=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc1Phrase = new Phrase();
			productDetailsTableCellMainDisc1Phrase.setLeading(12);
			productDetailsTableCellMainDisc1Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc1.addElement(productDetailsTableCellMainDisc1Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc1);
			
			PdfPCell productDetailsTableCellMainDisc2=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc2Phrase = new Phrase();
			productDetailsTableCellMainDisc2Phrase.setLeading(12);
			productDetailsTableCellMainDisc2Phrase.add(new Chunk("           Discount", mediumBold));
			productDetailsTableCellMainDisc2.addElement(productDetailsTableCellMainDisc2Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc2);
			
			PdfPCell productDetailsTableCellMainDisc3=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc3Phrase = new Phrase();
			productDetailsTableCellMainDisc3Phrase.setLeading(12);
			productDetailsTableCellMainDisc3Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc3.addElement(productDetailsTableCellMainDisc3Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc3);
			
			PdfPCell productDetailsTableCellMainDisc4=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc4Phrase = new Phrase();
			productDetailsTableCellMainDisc4Phrase.setLeading(12);
			productDetailsTableCellMainDisc4Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc4.addElement(productDetailsTableCellMainDisc4Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc4);
			
			PdfPCell productDetailsTableCellMainDisc5=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc5Phrase = new Phrase();
			productDetailsTableCellMainDisc5Phrase.setLeading(12);
			productDetailsTableCellMainDisc5Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc5.addElement(productDetailsTableCellMainDisc5Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc5);
			
			PdfPCell productDetailsTableCellMainDisc6=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc6Phrase = new Phrase();
			productDetailsTableCellMainDisc6Phrase.setLeading(12);
			productDetailsTableCellMainDisc6Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc6.addElement(productDetailsTableCellMainDisc6Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc6);
			
			PdfPCell productDetailsTableCellMainDisc7=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc7Phrase = new Phrase();
			productDetailsTableCellMainDisc7Phrase.setLeading(12);
			productDetailsTableCellMainDisc7Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc7.addElement(productDetailsTableCellMainDisc7Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc7);
			
			PdfPCell productDetailsTableCellMainDisc8=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc8Phrase = new Paragraph();
			productDetailsTableCellMainDisc8Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellMainDisc8Phrase.setLeading(12);
			productDetailsTableCellMainDisc8Phrase.add(new Chunk(billPrintDataModel.getTotalPercentageOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc8.addElement(productDetailsTableCellMainDisc8Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc8);
			
			PdfPCell productDetailsTableCellMainDisc9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc9Phrase = new Phrase();
			productDetailsTableCellMainDisc9Phrase.setLeading(12);
			productDetailsTableCellMainDisc9Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc9.addElement(productDetailsTableCellMainDisc9Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc9);
			
			PdfPCell productDetailsTableCellMainDisc10=new TopBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc10Phrase = new Paragraph();
			productDetailsTableCellMainDisc10Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCellMainDisc10Phrase.setLeading(12);
			productDetailsTableCellMainDisc10Phrase.add(new Chunk(billPrintDataModel.getTotalAmountOfDiscount(), mediumBold));
			productDetailsTableCellMainDisc10.addElement(productDetailsTableCellMainDisc10Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc10);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell63=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell63Phrase = new Phrase();
			productDetailsTableCell63Phrase.setLeading(12);
			productDetailsTableCell63Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell63.addElement(productDetailsTableCell63Phrase);
			productDetailsTable.addCell(productDetailsTableCell63);
			
			PdfPCell productDetailsTableCell64=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell64Phrase = new Phrase();
			productDetailsTableCell64Phrase.setLeading(12);
			productDetailsTableCell64Phrase.add(new Chunk("           Net Total Amount", mediumBold));
			productDetailsTableCell64.addElement(productDetailsTableCell64Phrase);
			productDetailsTable.addCell(productDetailsTableCell64);
			
			PdfPCell productDetailsTableCell65=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell65Phrase = new Phrase();
			productDetailsTableCell65Phrase.setLeading(12);
			productDetailsTableCell65Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell65.addElement(productDetailsTableCell65Phrase);
			productDetailsTable.addCell(productDetailsTableCell65);
			
			PdfPCell productDetailsTableCell66=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell67Phrase = new Phrase();
			productDetailsTableCell67Phrase.setLeading(12);
			productDetailsTableCell67Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell66.addElement(productDetailsTableCell67Phrase);
			productDetailsTable.addCell(productDetailsTableCell66);
			
			PdfPCell productDetailsTableCell68=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell69Phrase = new Phrase();
			productDetailsTableCell69Phrase.setLeading(12);
			productDetailsTableCell69Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell68.addElement(productDetailsTableCell69Phrase);
			productDetailsTable.addCell(productDetailsTableCell68);
			
			PdfPCell productDetailsTableCell70=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell70Phrase = new Phrase();
			productDetailsTableCell70Phrase.setLeading(12);
			productDetailsTableCell70Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell70.addElement(productDetailsTableCell70Phrase);
			productDetailsTable.addCell(productDetailsTableCell70);
			
			PdfPCell productDetailsTableCell71=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell71Phrase = new Phrase();
			productDetailsTableCell71Phrase.setLeading(12);
			productDetailsTableCell71Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell71.addElement(productDetailsTableCell71Phrase);
			productDetailsTable.addCell(productDetailsTableCell71);
			
			PdfPCell productDetailsTableCell72=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell72Phrase = new Phrase();
			productDetailsTableCell72Phrase.setLeading(12);
			productDetailsTableCell72Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell72.addElement(productDetailsTableCell72Phrase);
			productDetailsTable.addCell(productDetailsTableCell72);
			
			PdfPCell productDetailsTableCell73=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell73Phrase = new Phrase();
			productDetailsTableCell73Phrase.setLeading(12);
			productDetailsTableCell73Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell73.addElement(productDetailsTableCell73Phrase);
			productDetailsTable.addCell(productDetailsTableCell73);
			
			PdfPCell productDetailsTableCell74=new TopBorderPDFCell();
			Paragraph productDetailsTableCell74Phrase = new Paragraph();
			productDetailsTableCell74Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell74Phrase.setLeading(12);
			productDetailsTableCell74Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithTaxWithDiscNonRoundOff(), mediumBold));
			productDetailsTableCell74.addElement(productDetailsTableCell74Phrase);
			productDetailsTable.addCell(productDetailsTableCell74);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			/*//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
			PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
			PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);*/
			
			//roundOf
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);
			
			PdfPCell productDetailsTableCell56=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell58Phrase = new Phrase();
			productDetailsTableCell58Phrase.setLeading(12);
			productDetailsTableCell58Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell56.addElement(productDetailsTableCell58Phrase);
			productDetailsTable.addCell(productDetailsTableCell56);
			
			PdfPCell productDetailsTableCell61=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell61Phrase = new Phrase();
			productDetailsTableCell61Phrase.setLeading(12);
			productDetailsTableCell61Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell61.addElement(productDetailsTableCell61Phrase);
			productDetailsTable.addCell(productDetailsTableCell61);
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Paragraph productDetailsTableCell53Phrase = new Paragraph();
			productDetailsTableCell53Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("("+billPrintDataModel.getRoundOffAmount()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalamountWithtax
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Net Payable Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);

			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);
			
			PdfPCell productDetailsTableCell58=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell59Phrase = new Phrase();
			productDetailsTableCell59Phrase.setLeading(12);
			productDetailsTableCell59Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell58.addElement(productDetailsTableCell59Phrase);
			productDetailsTable.addCell(productDetailsTableCell58);
			
			PdfPCell productDetailsTableCell62=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell62Phrase = new Phrase();
			productDetailsTableCell62Phrase.setLeading(12);
			productDetailsTableCell62Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell62.addElement(productDetailsTableCell62Phrase);
			productDetailsTable.addCell(productDetailsTableCell62);
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Paragraph productDetailsTableCell42Phrase = new Paragraph();
			productDetailsTableCell42Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk(billPrintDataModel.getTotalAmountWithTaxRoundOff(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTotalAmountWithTaxInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(CategoryWiseAmountForBill categoryWiseAmountForBill:billPrintDataModel.getCategoryWiseAmountForBills())
			{
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.getHsnCode(), mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.getTaxableValue(), mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.getCgstRate(), mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.getSgstRate(), mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstPercentage()+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.getIgstRate(), mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(billPrintDataModel.getTotalAmount(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
			
			/* disc amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell14=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell14Phrase = new Paragraph();
			taxDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell14Phrase.setLeading(10);
			taxDetailsTableCell14Phrase.add(new Chunk("Discount", mediumBold));
			taxDetailsTableCell14.addElement(taxDetailsTableCell14Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell14);
			
			PdfPCell taxDetailsTableCell15=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell15Phrase = new Paragraph();
			taxDetailsTableCell15Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell15Phrase.setLeading(10);
			taxDetailsTableCell15Phrase.add(new Chunk(billPrintDataModel.getTotalAmountDisc(), mediumNormal));
			taxDetailsTableCell15.addElement(taxDetailsTableCell15Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell15);
			
			PdfPTable cgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell16=new RightBorderPDFCell();
			Phrase taxDetailsTableCell16Phrase = new Phrase();
			taxDetailsTableCell16Phrase.setLeading(10);
			taxDetailsTableCell16Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell16.addElement(taxDetailsTableCell16Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell16);
			
			PdfPCell taxDetailsTableCell17=new PdfPCell();
			taxDetailsTableCell17.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell17Phrase = new Paragraph();
			taxDetailsTableCell17Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell17Phrase.setLeading(10);
			taxDetailsTableCell17Phrase.add(new Chunk(billPrintDataModel.getTotalCGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell17.addElement(taxDetailsTableCell17Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell17);
			
			PdfPCell cgstValueTableCellDisc=new PdfPCell(cgstValueTableDisc);
			cgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellDisc);
			
			PdfPTable sgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell18=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell18Phrase = new Paragraph();
			taxDetailsTableCell18Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell18Phrase.setLeading(10);
			taxDetailsTableCell18Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell18.addElement(taxDetailsTableCell18Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell18);
			
			PdfPCell taxDetailsTableCell19=new PdfPCell();
			taxDetailsTableCell19.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell19Phrase = new Paragraph();
			taxDetailsTableCell19Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell19Phrase.setLeading(10);
			taxDetailsTableCell19Phrase.add(new Chunk(billPrintDataModel.getTotalSGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell19.addElement(taxDetailsTableCell19Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell19);
			
			PdfPCell sgstValueTableCellDisc=new PdfPCell(sgstValueTableDisc);
			sgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellDisc);
			
			PdfPTable igstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			igstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell20=new RightBorderPDFCell();
			Phrase taxDetailsTableCell20Phrase = new Phrase();
			taxDetailsTableCell20Phrase.setLeading(10);
			taxDetailsTableCell20Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell20.addElement(taxDetailsTableCell20Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell20);
			
			PdfPCell taxDetailsTableCell21=new PdfPCell();
			taxDetailsTableCell21.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell21Phrase = new Paragraph();
			taxDetailsTableCell21Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell21Phrase.setLeading(10);
			taxDetailsTableCell21Phrase.add(new Chunk(billPrintDataModel.getTotalIGSTAmountDisc(), mediumNormal));
			taxDetailsTableCell21.addElement(taxDetailsTableCell21Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell21);
			
			PdfPCell igstValueTableCellDisc=new PdfPCell(igstValueTableDisc);
			igstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellDisc);
			/* disc amount in hsn code wise part end*/
			
			/* net amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell22=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell22Phrase = new Paragraph();
			taxDetailsTableCell22Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell22Phrase.setLeading(10);
			taxDetailsTableCell22Phrase.add(new Chunk("Net Total", mediumBold));
			taxDetailsTableCell22.addElement(taxDetailsTableCell22Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell22);
			
			PdfPCell taxDetailsTableCell23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell23Phrase = new Paragraph();
			taxDetailsTableCell23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell23Phrase.setLeading(10);
			taxDetailsTableCell23Phrase.add(new Chunk(billPrintDataModel.getNetTotalAmount(), mediumNormal));
			taxDetailsTableCell23.addElement(taxDetailsTableCell23Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell23);
			
			PdfPTable cgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell24=new RightBorderPDFCell();
			Phrase taxDetailsTableCell24Phrase = new Phrase();
			taxDetailsTableCell24Phrase.setLeading(10);
			taxDetailsTableCell24Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell24.addElement(taxDetailsTableCell24Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell24);
			
			PdfPCell taxDetailsTableCell25=new PdfPCell();
			taxDetailsTableCell25.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell25Phrase = new Paragraph();
			taxDetailsTableCell25Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell25Phrase.setLeading(10);
			taxDetailsTableCell25Phrase.add(new Chunk(billPrintDataModel.getNetTotalCGSTAmount(), mediumNormal));
			taxDetailsTableCell25.addElement(taxDetailsTableCell25Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell25);
			
			PdfPCell cgstValueTableCellNet=new PdfPCell(cgstValueTableNet);
			cgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellNet);
			
			PdfPTable sgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell26=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell26Phrase = new Paragraph();
			taxDetailsTableCell26Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell26Phrase.setLeading(10);
			taxDetailsTableCell26Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell26.addElement(taxDetailsTableCell26Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell26);
			
			PdfPCell taxDetailsTableCell27=new PdfPCell();
			taxDetailsTableCell27.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell27Phrase = new Paragraph();
			taxDetailsTableCell27Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell27Phrase.setLeading(10);
			taxDetailsTableCell27Phrase.add(new Chunk(billPrintDataModel.getNetTotalSGSTAmount(), mediumNormal));
			taxDetailsTableCell27.addElement(taxDetailsTableCell27Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell27);
			
			PdfPCell sgstValueTableCellNet=new PdfPCell(sgstValueTableNet);
			sgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellNet);
			
			PdfPTable igstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			igstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell28=new RightBorderPDFCell();
			Phrase taxDetailsTableCell28Phrase = new Phrase();
			taxDetailsTableCell28Phrase.setLeading(10);
			taxDetailsTableCell28Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell28.addElement(taxDetailsTableCell28Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell28);
			
			PdfPCell taxDetailsTableCell29=new PdfPCell();
			taxDetailsTableCell29.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell29Phrase = new Paragraph();
			taxDetailsTableCell29Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell29Phrase.setLeading(10);
			taxDetailsTableCell29Phrase.add(new Chunk(billPrintDataModel.getNetTotalIGSTAmount(), mediumNormal));
			taxDetailsTableCell29.addElement(taxDetailsTableCell29Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell29);
			
			PdfPCell igstValueTableCellNet=new PdfPCell(igstValueTableNet);
			igstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellNet);
			/* net amount in hsn code wise part end*/
		
						
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+billPrintDataModel.getTaxAmountInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			/*PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
			lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			lastRowTable.setWidthPercentage(100);
			
			PdfPCell declarationTableCell=new RightBorderPDFCell();
			Phrase declarationTableCellPhrase = new Phrase();
			declarationTableCellPhrase.setLeading(12);
			declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
			declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct", mediumNormal));			
			declarationTableCell.addElement(declarationTableCellPhrase);		
			lastRowTable.addCell(declarationTableCell);
			
			PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			lastRowTable.addCell(signTableCell);*/
			
			PdfPTable bankAndSignDetailsTable = new PdfPTable(new float[] { 60 , 40});
			bankAndSignDetailsTable.setWidthPercentage(100);
			
			
			PdfPTable bankDeailsTable = new PdfPTable(new float[] { 20,40 ,40});
			bankDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			bankDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellBank1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank1Phrase = new Paragraph();
			taxDetailsTableCellBank1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank1Phrase.setLeading(10);
			taxDetailsTableCellBank1Phrase.add(new Chunk("Bank Name", mediumBold));
			taxDetailsTableCellBank1.addElement(taxDetailsTableCellBank1Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank1);	
			
			PdfPCell taxDetailsTableCellBank2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank2Phrase = new Paragraph();
			taxDetailsTableCellBank2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank2Phrase.setLeading(10);
			taxDetailsTableCellBank2Phrase.add(new Chunk("Kotak Mahindra Bank", mediumNormal));
			taxDetailsTableCellBank2.addElement(taxDetailsTableCellBank2Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank2);
			
			PdfPCell taxDetailsTableCellBank3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank3Phrase = new Paragraph();
			taxDetailsTableCellBank3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank3Phrase.setLeading(10);
			taxDetailsTableCellBank3Phrase.add(new Chunk("HDFC", mediumNormal));
			taxDetailsTableCellBank3.addElement(taxDetailsTableCellBank3Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank3);
			
			PdfPCell taxDetailsTableCellBank4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank4Phrase = new Paragraph();
			taxDetailsTableCellBank4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank4Phrase.setLeading(10);
			taxDetailsTableCellBank4Phrase.add(new Chunk("A/C Name", mediumBold));
			taxDetailsTableCellBank4.addElement(taxDetailsTableCellBank4Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank4);	
			
			PdfPCell taxDetailsTableCellBank5=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank5Phrase = new Paragraph();
			taxDetailsTableCellBank5Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank5Phrase.setLeading(10);
			taxDetailsTableCellBank5Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank5.addElement(taxDetailsTableCellBank5Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank5);
			
			PdfPCell taxDetailsTableCellBank6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank6Phrase = new Paragraph();
			taxDetailsTableCellBank6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank6Phrase.setLeading(10);
			taxDetailsTableCellBank6Phrase.add(new Chunk("VSS", mediumNormal));
			taxDetailsTableCellBank6.addElement(taxDetailsTableCellBank6Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank6);
			
			PdfPCell taxDetailsTableCellBank7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank7Phrase = new Paragraph();
			taxDetailsTableCellBank7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank7Phrase.setLeading(10);
			taxDetailsTableCellBank7Phrase.add(new Chunk("A/C No.", mediumBold));
			taxDetailsTableCellBank7.addElement(taxDetailsTableCellBank7Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank7);	
			
			PdfPCell taxDetailsTableCellBank8=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank8Phrase = new Paragraph();
			taxDetailsTableCellBank8Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank8Phrase.setLeading(10);
			taxDetailsTableCellBank8Phrase.add(new Chunk("133333374", mediumNormal));
			taxDetailsTableCellBank8.addElement(taxDetailsTableCellBank8Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank8);
			
			PdfPCell taxDetailsTableCellBank9=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank9Phrase = new Paragraph();
			taxDetailsTableCellBank9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank9Phrase.setLeading(10);
			taxDetailsTableCellBank9Phrase.add(new Chunk("50256762976370", mediumNormal));
			taxDetailsTableCellBank9.addElement(taxDetailsTableCellBank9Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank9);
			
			PdfPCell taxDetailsTableCellBank10=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank10Phrase = new Paragraph();
			taxDetailsTableCellBank10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank10Phrase.setLeading(10);
			taxDetailsTableCellBank10Phrase.add(new Chunk("IFSC", mediumBold));
			taxDetailsTableCellBank10.addElement(taxDetailsTableCellBank10Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank10);	
			
			PdfPCell taxDetailsTableCellBank11=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank11Phrase = new Paragraph();
			taxDetailsTableCellBank11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank11Phrase.setLeading(10);
			taxDetailsTableCellBank11Phrase.add(new Chunk("KKRK0000667", mediumNormal));
			taxDetailsTableCellBank11.addElement(taxDetailsTableCellBank11Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank11);
			
			PdfPCell taxDetailsTableCellBank12=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellBank12Phrase = new Paragraph();
			taxDetailsTableCellBank12Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellBank12Phrase.setLeading(10);
			taxDetailsTableCellBank12Phrase.add(new Chunk("HDFC1234002", mediumNormal));
			taxDetailsTableCellBank12.addElement(taxDetailsTableCellBank12Phrase);
			bankDeailsTable.addCell(taxDetailsTableCellBank12);
			
			PdfPCell bankDetailsMainCell=new PdfPCell(bankDeailsTable);
			bankDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(bankDetailsMainCell);			
			
			PdfPTable signDeailsTable = new PdfPTable(new float[] { 50 ,50});
			signDeailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			signDeailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCellSign1=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign1Phrase = new Paragraph();
			taxDetailsTableCellSign1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign1Phrase.setLeading(10);
			taxDetailsTableCellSign1Phrase.add(new Chunk("\n\n\n\n", mediumNormal));
			taxDetailsTableCellSign1.addElement(taxDetailsTableCellSign1Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign1);
			
			/*PdfPCell taxDetailsTableCellSign23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign23Phrase = new Paragraph();
			taxDetailsTableCellSign23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign23Phrase.setLeading(10);
			taxDetailsTableCellSign23Phrase.add(new Chunk("for"+company.getCompanyName(), mediumBold));
			taxDetailsTableCellSign23.addElement(taxDetailsTableCellSign23Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign23);*/
			
			PdfPCell taxDetailsTableCellSign2=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign2Phrase = new Paragraph();
			taxDetailsTableCellSign2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign2Phrase.setLeading(10);
			taxDetailsTableCellSign2Phrase.add(new Chunk("For "+company.getCompanyName()+"\n\n\n\n", mediumBold));
			taxDetailsTableCellSign2.addElement(taxDetailsTableCellSign2Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign2);	
			
			PdfPCell taxDetailsTableCellSign3=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign3Phrase = new Paragraph();
			taxDetailsTableCellSign3Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign3Phrase.setLeading(10);
			taxDetailsTableCellSign3Phrase.add(new Chunk("Receiver's Signature", mediumNormal));
			taxDetailsTableCellSign3.addElement(taxDetailsTableCellSign3Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign3);
			
			PdfPCell taxDetailsTableCellSign4=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCellSign4Phrase = new Paragraph();
			taxDetailsTableCellSign4Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCellSign4Phrase.setLeading(10);
			taxDetailsTableCellSign4Phrase.add(new Chunk("Authorized Signatory", mediumNormal));
			taxDetailsTableCellSign4.addElement(taxDetailsTableCellSign4Phrase);
			signDeailsTable.addCell(taxDetailsTableCellSign4);
			
			PdfPCell signDetailsMainCell=new PdfPCell(signDeailsTable);
			signDetailsMainCell.setPadding(0);
			bankAndSignDetailsTable.addCell(signDetailsMainCell);
			
			/*PdfPCell signTableCell=new PdfPCell();
			signTableCell.setBorder(Rectangle.NO_BORDER);
			Phrase signTableCellPhrase = new Phrase();
			signTableCellPhrase.setLeading(12);
			signTableCellPhrase.add(new Chunk("for "+company.getCompanyName()+"\n\n\n", mediumBold));
			signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));			
			signTableCell.addElement(signTableCellPhrase);		
			bankDeailsTable.addCell(signTableCell);*/
			
			PdfPCell lastRowMainCell=new PdfPCell(bankAndSignDetailsTable);
			lastRowMainCell.setColspan(2);
			lastRowMainCell.setPadding(0);
			mainTable.addCell(lastRowMainCell);			
			
			document.add(mainTable);

			double transportationCharges=billPrintDataModel.getTrasportationCharge();
			String footer="";
			// if(transportationCharges>0){
			// 	footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n Note:"+transportationCharges+" Rupees as transportation Charges is payable by customer\n\nThis is a Computer Generated Pro-Forma Invoice";	
			// }else{
			// 	footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n\n\nThis is a Computer Generated Pro-Forma Invoice";
			// }

			if(transportationCharges>0){
				//footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n Note:"+transportationCharges+" Rupees as transportation Charges is payable by customer\n\nThis is a Computer Generated Invoice";	
				footer="Note:"+transportationCharges+" Rupees as transportation Charges is payable by customer\n\nThis is a Computer Generated  Pro-Forma Invoice";	
			}else{
				//footer="We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n\n\nThis is a Computer Generated Invoice";
				footer="This is a Computer Generated Pro-Forma Invoice";
			}
			
			Paragraph paragraphFooter = new Paragraph(footer, mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	
		
	}
	
	
	public static File generateInvoicePdfInventory(InventoryAddedInvoiceModel inventoryAddedInvoiceModel,String fileName) throws FileNotFoundException, DocumentException 
	{
			
			
			Document document = new Document();
			document.setMargins(50, 10, 20, 20);
		
			File pdfFile = new File(fileName);
		 
			PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
			document.open();
			Paragraph paragraph = new Paragraph("PURCHASE INVOICE", mediumBold);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.add(new Paragraph(" "));
			document.add(paragraph);
			//document.add( Chunk.NEWLINE );
			
			
			PdfPTable mainTable = new PdfPTable(2);
			
			//mainTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			mainTable.setWidthPercentage(100);
			
			//mainTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//mainTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerInfo=new PdfPCell();
			
			Phrase ownerPhrase = new Phrase();
			ownerPhrase.setLeading(12);
			ownerPhrase.add(new Chunk(inventoryAddedInvoiceModel.getCompanyName()+" \n", mediumBold));
			ownerPhrase.add(new Paragraph(inventoryAddedInvoiceModel.getCompanyAddress()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("Tel No. : ", mediumBold));
			ownerPhrase.add(new Chunk((inventoryAddedInvoiceModel.getCoTelNo())+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("GSTIN/UIN : ", mediumBold));
			ownerPhrase.add(new Chunk(inventoryAddedInvoiceModel.getGstInNo()+" \n",mediumNormal));
			ownerPhrase.add(new Chunk("E-Mail : ", mediumBold));
			ownerPhrase.add(new Chunk((inventoryAddedInvoiceModel.getEmailId()==null)+" \n",mediumNormal));
			/*ownerPhrase.add(new Chunk("Company's PAN : ", mediumBold));
			ownerPhrase.add(new Chunk(company.getPanNumber()==null?"NA":company.getPanNumber()+" ",mediumNormal));*/
			ownerInfo.addElement(ownerPhrase);
			
			mainTable.addCell(ownerInfo);
			
			PdfPTable ownerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setBorder(0);
			//ownerSideTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			ownerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell ownerSideTableCell1=new RightBorderPDFCell();
			Phrase ownerSideTableCell1Phrase = new Phrase();
			ownerSideTableCell1Phrase.setLeading(12);
			ownerSideTableCell1Phrase.add(new Chunk("Invoice No. \n", mediumBold));
			ownerSideTableCell1Phrase.add(new Chunk(inventoryAddedInvoiceModel.getInventoryTsnNo(),mediumNormal));
			ownerSideTableCell1.addElement(ownerSideTableCell1Phrase);
			ownerSideTable.addCell(ownerSideTableCell1);
			
			PdfPCell ownerSideTableCell2=new PdfPCell();
			//ownerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			ownerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			Phrase ownerSideTableCell2Phrase = new Phrase();
			ownerSideTableCell2Phrase.setLeading(12);
			ownerSideTableCell2Phrase.add(new Chunk("Dated \n", mediumBold));
			ownerSideTableCell2Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDeliveryDate(),mediumNormal));
			ownerSideTableCell2.addElement(ownerSideTableCell2Phrase);
			ownerSideTable.addCell(ownerSideTableCell2);
			
			PdfPCell ownerSideTableCell3=new TopRightBorderPDFCell();
			//ownerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell3Phrase = new Phrase();
			ownerSideTableCell3Phrase.setLeading(12);
			ownerSideTableCell3Phrase.add(new Chunk("Transportation Name \n", mediumBold));
			ownerSideTableCell3Phrase.add(new Chunk(" \n",mediumNormal));
			ownerSideTableCell3.addElement(ownerSideTableCell3Phrase);
			ownerSideTable.addCell(ownerSideTableCell3);
			
			PdfPCell ownerSideTableCell4=new TopLeftBorderPDFCell();
			//ownerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell4Phrase = new Phrase();
			ownerSideTableCell4Phrase.setLeading(12);
			ownerSideTableCell4Phrase.add(new Chunk("Vehicle Number \n", mediumBold));
			ownerSideTableCell4Phrase.add(new Chunk(" \n",mediumNormal));
			ownerSideTableCell4.addElement(ownerSideTableCell4Phrase);
			ownerSideTable.addCell(ownerSideTableCell4);			
			
			PdfPCell ownerSideTableCell5=new TopRightBorderPDFCell();
			//ownerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell5Phrase = new Phrase();
			ownerSideTableCell5Phrase.setLeading(12);
			ownerSideTableCell5Phrase.add(new Chunk("Transportation GST No. \n", mediumBold));
			ownerSideTableCell5Phrase.add(new Chunk(" \n",mediumNormal));
			ownerSideTableCell5.addElement(ownerSideTableCell5Phrase);
			ownerSideTable.addCell(ownerSideTableCell5);
			
			PdfPCell ownerSideTableCell6=new TopLeftBorderPDFCell();
			//ownerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase ownerSideTableCell6Phrase = new Phrase();
			ownerSideTableCell6Phrase.setLeading(12);
			ownerSideTableCell6Phrase.add(new Chunk("Docket No. \n", mediumBold));
			ownerSideTableCell6Phrase.add(new Chunk(" \n",mediumNormal));
			ownerSideTableCell6.addElement(ownerSideTableCell6Phrase);
			ownerSideTable.addCell(ownerSideTableCell6);	
			
			PdfPCell ownerSideTableCellTemp = new PdfPCell(ownerSideTable);
			ownerSideTableCellTemp.setPadding(0);
			mainTable.addCell(ownerSideTableCellTemp);
						
			PdfPCell buyerInfo=new PdfPCell();
			
			Phrase buyerPhrase = new Phrase();
			buyerPhrase.setLeading(12);
			buyerPhrase.add(new Chunk(inventoryAddedInvoiceModel.getSupplierName()+" \n", mediumBold));
			
			/*for(String addressLine : billPrintDataModel.getAddressLineList())
			{
				buyerPhrase.add(new Paragraph(addressLine+"\n",mediumNormal));
			}*/
			
			buyerPhrase.add(new Paragraph(inventoryAddedInvoiceModel.getSupplierAddress()+"\n",mediumNormal));
			
//			buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getName()+" ", mediumBold));
//			buyerPhrase.add(new Chunk("Code : ", mediumBold));
//			buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getArea().getRegion().getCity().getState().getCode()+"\n",mediumNormal));
			buyerPhrase.add(new Chunk("Mobile No./Tele. No. : ", mediumBold));
			buyerPhrase.add(new Chunk(inventoryAddedInvoiceModel.getMobNoAndTelNo()+"\n",mediumNormal));
			
		
			/*buyerPhrase.add(new Chunk("Tax Type : ", mediumBold));
			buyerPhrase.add(new Chunk(billPrintDataModel.getBusinessName().getTaxType()+"\n",mediumNormal));*/
			buyerPhrase.add(new Chunk("GSTN / UIN : ", mediumBold));
			buyerPhrase.add(new Chunk(inventoryAddedInvoiceModel.getSupplierGstInNo(),mediumNormal));
			
			buyerInfo.addElement(buyerPhrase);
			
			mainTable.addCell(buyerInfo);
			
			PdfPTable buyerSideTable = new PdfPTable(2);
			//ownerSideTable.getDefaultCell().setFixedHeight(mainTable.getTotalHeight());
			buyerSideTable.setWidthPercentage(100);
			//ownerSideTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			//ownerSideTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell buyerSideTableCell1=new RightBorderPDFCell();
			//buyerSideTableCell1.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell1Phrase = new Phrase();
			buyerSideTableCell1Phrase.setLeading(12);
			buyerSideTableCell1Phrase.add(new Chunk("Bill No. \n", mediumBold));
			buyerSideTableCell1Phrase.add(new Chunk(inventoryAddedInvoiceModel.getBillNo(),mediumNormal));
			buyerSideTableCell1.addElement(buyerSideTableCell1Phrase);
			buyerSideTable.addCell(buyerSideTableCell1);
			
			PdfPCell buyerSideTableCell2=new PdfPCell();
			buyerSideTableCell2.setBorder(Rectangle.NO_BORDER);
			//buyerSideTableCell2.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell2Phrase = new Phrase();
			buyerSideTableCell2Phrase.setLeading(12);
			buyerSideTableCell2Phrase.add(new Chunk("Bill Date \n", mediumBold));
			buyerSideTableCell2Phrase.add(new Chunk(inventoryAddedInvoiceModel.getBillDate(),mediumNormal));
			buyerSideTableCell2.addElement(buyerSideTableCell2Phrase);
			buyerSideTable.addCell(buyerSideTableCell2);
			
			PdfPCell buyerSideTableCell3=new TopRightBorderPDFCell();
			//buyerSideTableCell3.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell3Phrase = new Phrase();
			buyerSideTableCell3Phrase.setLeading(12);
			buyerSideTableCell3Phrase.add(new Chunk("Despatch Document No. \n", mediumBold));
			buyerSideTableCell3Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell3.addElement(buyerSideTableCell3Phrase);
			buyerSideTable.addCell(buyerSideTableCell3);
			
			PdfPCell buyerSideTableCell4=new TopLeftBorderPDFCell();
			//buyerSideTableCell4.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell4Phrase = new Phrase();
			buyerSideTableCell4Phrase.setLeading(12);
			buyerSideTableCell4Phrase.add(new Chunk("Delivery Note Date \n", mediumBold));
			buyerSideTableCell4Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell4.addElement(buyerSideTableCell4Phrase);
			buyerSideTable.addCell(buyerSideTableCell4);			
			
			PdfPCell buyerSideTableCell5=new TopRightBorderPDFCell();
			//buyerSideTableCell5.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell5Phrase = new Phrase();
			buyerSideTableCell5Phrase.setLeading(12);
			buyerSideTableCell5Phrase.add(new Chunk("Despatch through \n", mediumBold));
			buyerSideTableCell5Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell5.addElement(buyerSideTableCell5Phrase);
			buyerSideTable.addCell(buyerSideTableCell5);
			
			PdfPCell buyerSideTableCell6=new TopLeftBorderPDFCell();
			//buyerSideTableCell6.setFixedHeight(mainTable.getTotalHeight()/3);
			Phrase buyerSideTableCell6Phrase = new Phrase();
			buyerSideTableCell6Phrase.setLeading(12);
			buyerSideTableCell6Phrase.add(new Chunk("Destination \n", mediumBold));
			buyerSideTableCell6Phrase.add(new Chunk(" ",mediumNormal));
			buyerSideTableCell6.addElement(buyerSideTableCell6Phrase);
			buyerSideTable.addCell(buyerSideTableCell6);	
			
			PdfPCell buyerSideTableCellTemp = new PdfPCell(buyerSideTable);
			buyerSideTableCellTemp.setPadding(0);
			mainTable.addCell(buyerSideTableCellTemp);
			
			//table size array
			float[] tableColSize=new float[] { 14,80, 24 ,15,18,15, 18,18,18,30};
			PdfPTable productDetailsTable = new PdfPTable(tableColSize);
			productDetailsTable.setWidthPercentage(100);			
						
			PdfPCell productDetailsTableCell1=new RightBorderPDFCell();
			Paragraph productDetailsTableCell1Phrase = new Paragraph();
			productDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell1Phrase.setLeading(12);
			productDetailsTableCell1Phrase.add(new Chunk("Sr. No.", mediumBold));
			productDetailsTableCell1.addElement(productDetailsTableCell1Phrase);
			productDetailsTable.addCell(productDetailsTableCell1);
			
			PdfPCell productDetailsTableCell2=new RightBorderPDFCell();
			Paragraph productDetailsTableCell2Phrase = new Paragraph();
			productDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell2Phrase.setLeading(12);
			productDetailsTableCell2Phrase.add(new Chunk("Description Of Goods", mediumBold));
			productDetailsTableCell2.addElement(productDetailsTableCell2Phrase);
			productDetailsTable.addCell(productDetailsTableCell2);
			
			PdfPCell productDetailsTableCell3=new RightBorderPDFCell();
			Paragraph productDetailsTableCell3Phrase = new Paragraph();
			productDetailsTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell3Phrase.setLeading(12);
			productDetailsTableCell3Phrase.add(new Chunk("HSN/SAC", mediumBold));
			productDetailsTableCell3.addElement(productDetailsTableCell3Phrase);
			productDetailsTable.addCell(productDetailsTableCell3);
			
			PdfPCell productDetailsTableCell4=new RightBorderPDFCell();
			Paragraph productDetailsTableCell4Phrase = new Paragraph();
			productDetailsTableCell4Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell4Phrase.setLeading(12);
			productDetailsTableCell4Phrase.add(new Chunk("Tax Slab", mediumBold));
			productDetailsTableCell4.addElement(productDetailsTableCell4Phrase);
			productDetailsTable.addCell(productDetailsTableCell4);
			
			PdfPCell productDetailsTableCell6=new RightBorderPDFCell();
			Paragraph productDetailsTableCell6Phrase = new Paragraph();
			productDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell6Phrase.setLeading(12);
			productDetailsTableCell6Phrase.add(new Chunk("MRP", mediumBold));
			productDetailsTableCell6.addElement(productDetailsTableCell6Phrase);
			productDetailsTable.addCell(productDetailsTableCell6);
			
			PdfPCell productDetailsTableCell5=new RightBorderPDFCell();
			Paragraph productDetailsTableCell5Phrase = new Paragraph();
			productDetailsTableCell5Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell5Phrase.setLeading(12);
			productDetailsTableCell5Phrase.add(new Chunk("Qty.", mediumBold));
			productDetailsTableCell5.addElement(productDetailsTableCell5Phrase);
			productDetailsTable.addCell(productDetailsTableCell5);
			
			PdfPCell productDetailsTableCel56=new RightBorderPDFCell();
			Paragraph productDetailsTableCell56Phrase = new Paragraph();
			productDetailsTableCell56Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell56Phrase.setLeading(12);
			productDetailsTableCell56Phrase.add(new Chunk("Total\nAmt.", mediumBold));
			productDetailsTableCel56.addElement(productDetailsTableCell56Phrase);
			productDetailsTable.addCell(productDetailsTableCel56);
		
			PdfPCell productDetailsTableCellDiscPer=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPerPhrase = new Paragraph();
			productDetailsTableCellDiscPerPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPerPhrase.setLeading(12);
			productDetailsTableCellDiscPerPhrase.add(new Chunk("Disc. (%)", mediumBold));
			productDetailsTableCellDiscPer.addElement(productDetailsTableCellDiscPerPhrase);
			productDetailsTable.addCell(productDetailsTableCellDiscPer);
			
			PdfPCell productDetailsTableCellDisc=new RightBorderPDFCell();
			Paragraph productDetailsTableCellDiscPhrase = new Paragraph();
			productDetailsTableCellDiscPhrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellDiscPhrase.setLeading(12);
			productDetailsTableCellDiscPhrase.add(new Chunk("Disc.\nAmt.", mediumBold));
			productDetailsTableCellDisc.addElement(productDetailsTableCellDiscPhrase);
			productDetailsTable.addCell(productDetailsTableCellDisc);
			
			PdfPCell productDetailsTableCell7=new PdfPCell();
			productDetailsTableCell7.setBorder(Rectangle.NO_BORDER);
			Paragraph productDetailsTableCell7Phrase = new Paragraph();
			productDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell7Phrase.setLeading(12);
			productDetailsTableCell7Phrase.add(new Chunk("Net Payable", mediumBold));
			productDetailsTableCell7.addElement(productDetailsTableCell7Phrase);
			productDetailsTable.addCell(productDetailsTableCell7);
			
			PdfPCell productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			for(Map<String, Object> productData: inventoryAddedInvoiceModel.getProductList())
			{
				// srNo, productName, hsnCode,
				// taxSlab, MRP, Qty, totalAmt,
				// discPer, discAmt, newTotalAmt
				
				productDetailsTable = new PdfPTable(tableColSize);
				
				PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell8Phrase = new Paragraph();
				productDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell8Phrase.setLeading(12);
				productDetailsTableCell8Phrase.add(new Chunk(productData.get("srNo")+"", mediumNormal));
				productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
				productDetailsTable.addCell(productDetailsTableCell8);
				
				PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell9Phrase = new Paragraph();
				//productDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell9Phrase.setLeading(12);
				productDetailsTableCell9Phrase.add(new Chunk(productData.get("productName")+"", mediumNormal));
				productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
				productDetailsTable.addCell(productDetailsTableCell9);
				
				PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell10Phrase = new Paragraph();
				productDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell10Phrase.setLeading(12);
				productDetailsTableCell10Phrase.add(new Chunk(productData.get("hsnCode")+"", mediumNormal));
				productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
				productDetailsTable.addCell(productDetailsTableCell10);
				
				PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell11Phrase = new Paragraph();
				productDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell11Phrase.setLeading(12);
				productDetailsTableCell11Phrase.add(new Chunk(productData.get("taxSlab")+""+" %", mediumNormal));
				productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);				
				productDetailsTable.addCell(productDetailsTableCell11);
				
				PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell13Phrase = new Paragraph();
				productDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell13Phrase.setLeading(12);
				productDetailsTableCell13Phrase.add(new Chunk(productData.get("MRP")+"", mediumNormal));
				productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
				productDetailsTable.addCell(productDetailsTableCell13);
				
				PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell12Phrase = new Paragraph();
				productDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell12Phrase.setLeading(12);
				productDetailsTableCell12Phrase.add(new Chunk(productData.get("Qty")+"", mediumNormal));
				productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
				productDetailsTable.addCell(productDetailsTableCell12);
				
				PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell52Phrase = new Paragraph();
				productDetailsTableCell52Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell52Phrase.setLeading(12);
				productDetailsTableCell52Phrase.add(new Chunk(productData.get("totalAmt")+"", mediumNormal));
				productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
				productDetailsTable.addCell(productDetailsTableCell52);
				
				PdfPCell productDetailsTableCell59=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell59Phrase = new Paragraph();
				productDetailsTableCell59Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell59Phrase.setLeading(12);
				productDetailsTableCell59Phrase.add(new Chunk(productData.get("discPer")+"", mediumNormal));
				productDetailsTableCell59.addElement(productDetailsTableCell59Phrase);
				productDetailsTable.addCell(productDetailsTableCell59);
				
				PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
				Paragraph productDetailsTableCell43Phrase = new Paragraph();
				productDetailsTableCell43Phrase.setAlignment(Element.ALIGN_CENTER);
				productDetailsTableCell43Phrase.setLeading(12);
				productDetailsTableCell43Phrase.add(new Chunk(productData.get("discAmt")+"", mediumNormal));
				productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
				productDetailsTable.addCell(productDetailsTableCell43);
				
				PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
				Paragraph productDetailsTableCell14Phrase = new Paragraph();
				productDetailsTableCell14Phrase.setAlignment(Element.ALIGN_RIGHT);
				productDetailsTableCell14Phrase.setLeading(12);
				productDetailsTableCell14Phrase.add(new Chunk(productData.get("newTotalAmt")+"", mediumNormal));
				productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
				productDetailsTable.addCell(productDetailsTableCell14);
				
				productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
				productDetailsTableCellTemp.setPadding(0);
				productDetailsTableCellTemp.setColspan(2);
				mainTable.addCell(productDetailsTableCellTemp);
			}
			
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell29=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell29Phrase = new Phrase();
			productDetailsTableCell29Phrase.setLeading(12);
			productDetailsTableCell29Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell29.addElement(productDetailsTableCell29Phrase);
			productDetailsTable.addCell(productDetailsTableCell29);
			
			PdfPCell productDetailsTableCell30=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell30Phrase = new Phrase();
			productDetailsTableCell30Phrase.setLeading(12);
			productDetailsTableCell30Phrase.add(new Chunk("           Total", mediumBold));
			productDetailsTableCell30.addElement(productDetailsTableCell30Phrase);
			productDetailsTable.addCell(productDetailsTableCell30);
			
			PdfPCell productDetailsTableCell31=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell31Phrase = new Phrase();
			productDetailsTableCell31Phrase.setLeading(12);
			productDetailsTableCell31Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell31.addElement(productDetailsTableCell31Phrase);
			productDetailsTable.addCell(productDetailsTableCell31);
			
			PdfPCell productDetailsTableCell32=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell32Phrase = new Phrase();
			productDetailsTableCell32Phrase.setLeading(12);
			productDetailsTableCell32Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell32.addElement(productDetailsTableCell32Phrase);
			productDetailsTable.addCell(productDetailsTableCell32);
			
			PdfPCell productDetailsTableCell33=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell33Phrase = new Phrase();
			productDetailsTableCell33Phrase.setLeading(12);
			productDetailsTableCell33Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell33.addElement(productDetailsTableCell33Phrase);
			productDetailsTable.addCell(productDetailsTableCell33);
			
			PdfPCell productDetailsTableCell34=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCell34Phrase = new Paragraph();
			productDetailsTableCell34Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCell34Phrase.setLeading(12);
			productDetailsTableCell34Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalQuantity(), mediumBold));
			productDetailsTableCell34.addElement(productDetailsTableCell34Phrase);
			productDetailsTable.addCell(productDetailsTableCell34);
			
			PdfPCell productDetailsTableCell44=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell44Phrase = new Phrase();
			productDetailsTableCell44Phrase.setLeading(12);
			productDetailsTableCell44Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell44.addElement(productDetailsTableCell44Phrase);
			productDetailsTable.addCell(productDetailsTableCell44);
			
			PdfPCell productDetailsTableCell57=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell57Phrase = new Phrase();
			productDetailsTableCell57Phrase.setLeading(12);
			productDetailsTableCell57Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell57.addElement(productDetailsTableCell57Phrase);
			productDetailsTable.addCell(productDetailsTableCell57);
			
			PdfPCell productDetailsTableCell60=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell60Phrase = new Phrase();
			productDetailsTableCell60Phrase.setLeading(12);
			productDetailsTableCell60Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell60.addElement(productDetailsTableCell60Phrase);
			productDetailsTable.addCell(productDetailsTableCell60);
			
			PdfPCell productDetailsTableCell35=new TopBorderPDFCell();
			Paragraph productDetailsTableCell35Phrase = new Paragraph();
			productDetailsTableCell35Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell35Phrase.setLeading(12);
			productDetailsTableCell35Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalAmountBeforeDiscount(), mediumBold));
			productDetailsTableCell35.addElement(productDetailsTableCell35Phrase);
			productDetailsTable.addCell(productDetailsTableCell35);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//discount section
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCellMainDisc1=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc1Phrase = new Phrase();
			productDetailsTableCellMainDisc1Phrase.setLeading(12);
			productDetailsTableCellMainDisc1Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc1.addElement(productDetailsTableCellMainDisc1Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc1);
			
			PdfPCell productDetailsTableCellMainDisc2=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc2Phrase = new Phrase();
			productDetailsTableCellMainDisc2Phrase.setLeading(12);
			productDetailsTableCellMainDisc2Phrase.add(new Chunk("           Discount", mediumBold));
			productDetailsTableCellMainDisc2.addElement(productDetailsTableCellMainDisc2Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc2);
			
			PdfPCell productDetailsTableCellMainDisc3=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc3Phrase = new Phrase();
			productDetailsTableCellMainDisc3Phrase.setLeading(12);
			productDetailsTableCellMainDisc3Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCellMainDisc3.addElement(productDetailsTableCellMainDisc3Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc3);
			
			PdfPCell productDetailsTableCellMainDisc4=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc4Phrase = new Phrase();
			productDetailsTableCellMainDisc4Phrase.setLeading(12);
			productDetailsTableCellMainDisc4Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc4.addElement(productDetailsTableCellMainDisc4Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc4);
			
			PdfPCell productDetailsTableCellMainDisc5=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc5Phrase = new Phrase();
			productDetailsTableCellMainDisc5Phrase.setLeading(12);
			productDetailsTableCellMainDisc5Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc5.addElement(productDetailsTableCellMainDisc5Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc5);
			
			PdfPCell productDetailsTableCellMainDisc6=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc6Phrase = new Phrase();
			productDetailsTableCellMainDisc6Phrase.setLeading(12);
			productDetailsTableCellMainDisc6Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc6.addElement(productDetailsTableCellMainDisc6Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc6);
			
			PdfPCell productDetailsTableCellMainDisc7=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc7Phrase = new Phrase();
			productDetailsTableCellMainDisc7Phrase.setLeading(12);
			productDetailsTableCellMainDisc7Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCellMainDisc7.addElement(productDetailsTableCellMainDisc7Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc7);
			
			String discPer="",discAmt="";
			if(inventoryAddedInvoiceModel.getDiscountType().equals("PERCENTAGE")){
				discPer=inventoryAddedInvoiceModel.getDiscountPer();				
			}else{
				discAmt=inventoryAddedInvoiceModel.getDiscountAmt();
			}
			
			PdfPCell productDetailsTableCellMainDisc8=new TopRightBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc8Phrase = new Paragraph();
			productDetailsTableCellMainDisc8Phrase.setAlignment(Element.ALIGN_CENTER);
			productDetailsTableCellMainDisc8Phrase.setLeading(12);
			productDetailsTableCellMainDisc8Phrase.add(new Chunk(discPer, mediumBold));
			productDetailsTableCellMainDisc8.addElement(productDetailsTableCellMainDisc8Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc8);
			
			PdfPCell productDetailsTableCellMainDisc9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCellMainDisc9Phrase = new Phrase();
			productDetailsTableCellMainDisc9Phrase.setLeading(12);
			productDetailsTableCellMainDisc9Phrase.add(new Chunk(discAmt, mediumBold));
			productDetailsTableCellMainDisc9.addElement(productDetailsTableCellMainDisc9Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc9);
			
			PdfPCell productDetailsTableCellMainDisc10=new TopBorderPDFCell();
			Paragraph productDetailsTableCellMainDisc10Phrase = new Paragraph();
			productDetailsTableCellMainDisc10Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCellMainDisc10Phrase.setLeading(12);
			productDetailsTableCellMainDisc10Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDiscountAmt(), mediumBold));
			productDetailsTableCellMainDisc10.addElement(productDetailsTableCellMainDisc10Phrase);
			productDetailsTable.addCell(productDetailsTableCellMainDisc10);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalAmount
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell63=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell63Phrase = new Phrase();
			productDetailsTableCell63Phrase.setLeading(12);
			productDetailsTableCell63Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell63.addElement(productDetailsTableCell63Phrase);
			productDetailsTable.addCell(productDetailsTableCell63);
			
			PdfPCell productDetailsTableCell64=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell64Phrase = new Phrase();
			productDetailsTableCell64Phrase.setLeading(12);
			productDetailsTableCell64Phrase.add(new Chunk("           Net Total Amount", mediumBold));
			productDetailsTableCell64.addElement(productDetailsTableCell64Phrase);
			productDetailsTable.addCell(productDetailsTableCell64);
			
			PdfPCell productDetailsTableCell65=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell65Phrase = new Phrase();
			productDetailsTableCell65Phrase.setLeading(12);
			productDetailsTableCell65Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell65.addElement(productDetailsTableCell65Phrase);
			productDetailsTable.addCell(productDetailsTableCell65);
			
			PdfPCell productDetailsTableCell66=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell67Phrase = new Phrase();
			productDetailsTableCell67Phrase.setLeading(12);
			productDetailsTableCell67Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell66.addElement(productDetailsTableCell67Phrase);
			productDetailsTable.addCell(productDetailsTableCell66);
			
			PdfPCell productDetailsTableCell68=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell69Phrase = new Phrase();
			productDetailsTableCell69Phrase.setLeading(12);
			productDetailsTableCell69Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell68.addElement(productDetailsTableCell69Phrase);
			productDetailsTable.addCell(productDetailsTableCell68);
			
			PdfPCell productDetailsTableCell70=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell70Phrase = new Phrase();
			productDetailsTableCell70Phrase.setLeading(12);
			productDetailsTableCell70Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell70.addElement(productDetailsTableCell70Phrase);
			productDetailsTable.addCell(productDetailsTableCell70);
			
			PdfPCell productDetailsTableCell71=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell71Phrase = new Phrase();
			productDetailsTableCell71Phrase.setLeading(12);
			productDetailsTableCell71Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell71.addElement(productDetailsTableCell71Phrase);
			productDetailsTable.addCell(productDetailsTableCell71);
			
			PdfPCell productDetailsTableCell72=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell72Phrase = new Phrase();
			productDetailsTableCell72Phrase.setLeading(12);
			productDetailsTableCell72Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell72.addElement(productDetailsTableCell72Phrase);
			productDetailsTable.addCell(productDetailsTableCell72);
			
			PdfPCell productDetailsTableCell73=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell73Phrase = new Phrase();
			productDetailsTableCell73Phrase.setLeading(12);
			productDetailsTableCell73Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell73.addElement(productDetailsTableCell73Phrase);
			productDetailsTable.addCell(productDetailsTableCell73);
			
			PdfPCell productDetailsTableCell74=new TopBorderPDFCell();
			Paragraph productDetailsTableCell74Phrase = new Paragraph();
			productDetailsTableCell74Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell74Phrase.setLeading(12);
			productDetailsTableCell74Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalAmountAfterDiscount(), mediumBold));
			productDetailsTableCell74.addElement(productDetailsTableCell74Phrase);
			productDetailsTable.addCell(productDetailsTableCell74);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			/*//less : cgst
			PdfPCell productDetailsTableCell8=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell8Phrase = new Phrase();
			productDetailsTableCell8Phrase.setLeading(12);
			productDetailsTableCell8Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell8.addElement(productDetailsTableCell8Phrase);
			productDetailsTable.addCell(productDetailsTableCell8);
			
			PdfPCell productDetailsTableCell9=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell9Phrase = new Phrase();
			productDetailsTableCell9Phrase.setLeading(12);
			productDetailsTableCell9Phrase.add(new Chunk("           CGST ", mediumBold));
			productDetailsTableCell9.addElement(productDetailsTableCell9Phrase);
			productDetailsTable.addCell(productDetailsTableCell9);
			
			PdfPCell productDetailsTableCell10=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell10Phrase = new Phrase();
			productDetailsTableCell10Phrase.setLeading(12);
			productDetailsTableCell10Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell10.addElement(productDetailsTableCell10Phrase);
			productDetailsTable.addCell(productDetailsTableCell10);
			
			PdfPCell productDetailsTableCell11=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell11Phrase = new Phrase();
			productDetailsTableCell11Phrase.setLeading(12);
			productDetailsTableCell11Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell11.addElement(productDetailsTableCell11Phrase);
			productDetailsTable.addCell(productDetailsTableCell11);
			
			PdfPCell productDetailsTableCell12=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell12Phrase = new Phrase();
			productDetailsTableCell12Phrase.setLeading(12);
			productDetailsTableCell12Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell12.addElement(productDetailsTableCell12Phrase);
			productDetailsTable.addCell(productDetailsTableCell12);
			
			PdfPCell productDetailsTableCell13=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell13Phrase = new Phrase();
			productDetailsTableCell13Phrase.setLeading(12);
			productDetailsTableCell13Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell13.addElement(productDetailsTableCell13Phrase);
			productDetailsTable.addCell(productDetailsTableCell13);
			
			PdfPCell productDetailsTableCell45=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell45Phrase = new Phrase();
			productDetailsTableCell45Phrase.setLeading(12);
			productDetailsTableCell45Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell45.addElement(productDetailsTableCell45Phrase);
			productDetailsTable.addCell(productDetailsTableCell45);
			
			PdfPCell productDetailsTableCell14=new TopBorderPDFCell();
			Phrase productDetailsTableCell14Phrase = new Phrase();
			productDetailsTableCell14Phrase.setLeading(12);
			productDetailsTableCell14Phrase.add(new Chunk("     "+billPrintDataModel.getcGSTAmount(), mediumBold));
			productDetailsTableCell14.addElement(productDetailsTableCell14Phrase);
			productDetailsTable.addCell(productDetailsTableCell14);
			
			//sgst
			PdfPCell productDetailsTableCell15=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell15Phrase = new Phrase();
			productDetailsTableCell15Phrase.setLeading(12);
			productDetailsTableCell15Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell15.addElement(productDetailsTableCell15Phrase);
			productDetailsTable.addCell(productDetailsTableCell15);
			
			PdfPCell productDetailsTableCell16=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell16Phrase = new Phrase();
			productDetailsTableCell16Phrase.setLeading(12);
			productDetailsTableCell16Phrase.add(new Chunk("           SGST ", mediumBold));
			productDetailsTableCell16.addElement(productDetailsTableCell16Phrase);
			productDetailsTable.addCell(productDetailsTableCell16);
			
			PdfPCell productDetailsTableCell17=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell17Phrase = new Phrase();
			productDetailsTableCell17Phrase.setLeading(12);
			productDetailsTableCell17Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell17.addElement(productDetailsTableCell17Phrase);
			productDetailsTable.addCell(productDetailsTableCell17);
			
			PdfPCell productDetailsTableCell18=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell18Phrase = new Phrase();
			productDetailsTableCell18Phrase.setLeading(12);
			productDetailsTableCell18Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell18.addElement(productDetailsTableCell18Phrase);
			productDetailsTable.addCell(productDetailsTableCell18);
			
			PdfPCell productDetailsTableCell19=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell19Phrase = new Phrase();
			productDetailsTableCell19Phrase.setLeading(12);
			productDetailsTableCell19Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell19.addElement(productDetailsTableCell19Phrase);
			productDetailsTable.addCell(productDetailsTableCell19);
			
			PdfPCell productDetailsTableCell20=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell20Phrase = new Phrase();
			productDetailsTableCell20Phrase.setLeading(12);
			productDetailsTableCell20Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell20.addElement(productDetailsTableCell20Phrase);
			productDetailsTable.addCell(productDetailsTableCell20);
			
			PdfPCell productDetailsTableCell46=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell46Phrase = new Phrase();
			productDetailsTableCell46Phrase.setLeading(12);
			productDetailsTableCell46Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell46.addElement(productDetailsTableCell46Phrase);
			productDetailsTable.addCell(productDetailsTableCell46);
			
			PdfPCell productDetailsTableCell21=new TopBorderPDFCell();
			Phrase productDetailsTableCell21Phrase = new Phrase();
			productDetailsTableCell21Phrase.setLeading(12);
			productDetailsTableCell21Phrase.add(new Chunk("     "+billPrintDataModel.getsGSTAmount(), mediumBold));
			productDetailsTableCell21.addElement(productDetailsTableCell21Phrase);
			productDetailsTable.addCell(productDetailsTableCell21);
			
			//igst
			PdfPCell productDetailsTableCell22=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell22Phrase = new Phrase();
			productDetailsTableCell22Phrase.setLeading(12);
			productDetailsTableCell22Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell22.addElement(productDetailsTableCell22Phrase);
			productDetailsTable.addCell(productDetailsTableCell22);
			
			PdfPCell productDetailsTableCell23=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell23Phrase = new Phrase();
			productDetailsTableCell23Phrase.setLeading(12);
			productDetailsTableCell23Phrase.add(new Chunk("           IGST ", mediumBold));
			productDetailsTableCell23.addElement(productDetailsTableCell23Phrase);
			productDetailsTable.addCell(productDetailsTableCell23);
			
			PdfPCell productDetailsTableCell24=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell24Phrase = new Phrase();
			productDetailsTableCell24Phrase.setLeading(12);
			productDetailsTableCell24Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell24.addElement(productDetailsTableCell24Phrase);
			productDetailsTable.addCell(productDetailsTableCell24);
			
			PdfPCell productDetailsTableCell25=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell25Phrase = new Phrase();
			productDetailsTableCell25Phrase.setLeading(12);
			productDetailsTableCell25Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell25.addElement(productDetailsTableCell25Phrase);
			productDetailsTable.addCell(productDetailsTableCell25);
			
			PdfPCell productDetailsTableCell26=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell26Phrase = new Phrase();
			productDetailsTableCell26Phrase.setLeading(12);
			productDetailsTableCell26Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell26.addElement(productDetailsTableCell26Phrase);
			productDetailsTable.addCell(productDetailsTableCell26);
			
			PdfPCell productDetailsTableCell27=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell27Phrase = new Phrase();
			productDetailsTableCell27Phrase.setLeading(12);
			productDetailsTableCell27Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell27.addElement(productDetailsTableCell27Phrase);
			productDetailsTable.addCell(productDetailsTableCell27);
			
			PdfPCell productDetailsTableCell47=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell47Phrase = new Phrase();
			productDetailsTableCell47Phrase.setLeading(12);
			productDetailsTableCell47Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell47.addElement(productDetailsTableCell47Phrase);
			productDetailsTable.addCell(productDetailsTableCell47);
			
			PdfPCell productDetailsTableCell28=new TopBorderPDFCell();
			Phrase productDetailsTableCell28Phrase = new Phrase();
			productDetailsTableCell28Phrase.setLeading(12);
			productDetailsTableCell28Phrase.add(new Chunk("     "+billPrintDataModel.getiGSTAmount(), mediumBold));
			productDetailsTableCell28.addElement(productDetailsTableCell28Phrase);
			productDetailsTable.addCell(productDetailsTableCell28);*/
			
			//roundOf
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell43=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell43Phrase = new Phrase();
			productDetailsTableCell43Phrase.setLeading(12);
			productDetailsTableCell43Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell43.addElement(productDetailsTableCell43Phrase);
			productDetailsTable.addCell(productDetailsTableCell43);
			
			PdfPCell productDetailsTableCell48=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell48Phrase = new Phrase();
			productDetailsTableCell48Phrase.setLeading(12);
			productDetailsTableCell48Phrase.add(new Chunk("Less : Round Off", mediumBold));
			productDetailsTableCell48.addElement(productDetailsTableCell48Phrase);
			productDetailsTable.addCell(productDetailsTableCell48);
			
			PdfPCell productDetailsTableCell49=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell49Phrase = new Phrase();
			productDetailsTableCell49Phrase.setLeading(12);
			productDetailsTableCell49Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell49.addElement(productDetailsTableCell49Phrase);
			productDetailsTable.addCell(productDetailsTableCell49);
			
			PdfPCell productDetailsTableCell50=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell50Phrase = new Phrase();
			productDetailsTableCell50Phrase.setLeading(12);
			productDetailsTableCell50Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell50.addElement(productDetailsTableCell50Phrase);
			productDetailsTable.addCell(productDetailsTableCell50);
			
			PdfPCell productDetailsTableCell51=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell51Phrase = new Phrase();
			productDetailsTableCell51Phrase.setLeading(12);
			productDetailsTableCell51Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell51.addElement(productDetailsTableCell51Phrase);
			productDetailsTable.addCell(productDetailsTableCell51);
			
			PdfPCell productDetailsTableCell52=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell52Phrase = new Phrase();
			productDetailsTableCell52Phrase.setLeading(12);
			productDetailsTableCell52Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell52.addElement(productDetailsTableCell52Phrase);
			productDetailsTable.addCell(productDetailsTableCell52);

			PdfPCell productDetailsTableCell54=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell54Phrase = new Phrase();
			productDetailsTableCell54Phrase.setLeading(12);
			productDetailsTableCell54Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell54.addElement(productDetailsTableCell54Phrase);
			productDetailsTable.addCell(productDetailsTableCell54);
			
			PdfPCell productDetailsTableCell56=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell58Phrase = new Phrase();
			productDetailsTableCell58Phrase.setLeading(12);
			productDetailsTableCell58Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell56.addElement(productDetailsTableCell58Phrase);
			productDetailsTable.addCell(productDetailsTableCell56);
			
			PdfPCell productDetailsTableCell61=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell61Phrase = new Phrase();
			productDetailsTableCell61Phrase.setLeading(12);
			productDetailsTableCell61Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell61.addElement(productDetailsTableCell61Phrase);
			productDetailsTable.addCell(productDetailsTableCell61);
			
			PdfPCell productDetailsTableCell53=new TopBorderPDFCell();
			Paragraph productDetailsTableCell53Phrase = new Paragraph();
			productDetailsTableCell53Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell53Phrase.setLeading(12);
			productDetailsTableCell53Phrase.add(new Chunk("("+inventoryAddedInvoiceModel.getRoundOff()+")", mediumBold));
			productDetailsTableCell53.addElement(productDetailsTableCell53Phrase);
			productDetailsTable.addCell(productDetailsTableCell53);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			//totalamountWithtax
			productDetailsTable = new PdfPTable(tableColSize);
			
			PdfPCell productDetailsTableCell36=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell36Phrase = new Phrase();
			productDetailsTableCell36Phrase.setLeading(12);
			productDetailsTableCell36Phrase.add(new Chunk(" ", mediumBold));
			productDetailsTableCell36.addElement(productDetailsTableCell36Phrase);
			productDetailsTable.addCell(productDetailsTableCell36);
			
			PdfPCell productDetailsTableCell37=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell37Phrase = new Phrase();
			productDetailsTableCell37Phrase.setLeading(12);
			productDetailsTableCell37Phrase.add(new Chunk("           Net Payable Amount", mediumBold));
			productDetailsTableCell37.addElement(productDetailsTableCell37Phrase);
			productDetailsTable.addCell(productDetailsTableCell37);
			
			PdfPCell productDetailsTableCell38=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell38Phrase = new Phrase();
			productDetailsTableCell38Phrase.setLeading(12);
			productDetailsTableCell38Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell38.addElement(productDetailsTableCell38Phrase);
			productDetailsTable.addCell(productDetailsTableCell38);
			
			PdfPCell productDetailsTableCell39=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell39Phrase = new Phrase();
			productDetailsTableCell39Phrase.setLeading(12);
			productDetailsTableCell39Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell39.addElement(productDetailsTableCell39Phrase);
			productDetailsTable.addCell(productDetailsTableCell39);
			
			PdfPCell productDetailsTableCell41=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell41Phrase = new Phrase();
			productDetailsTableCell41Phrase.setLeading(12);
			productDetailsTableCell41Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell41.addElement(productDetailsTableCell41Phrase);
			productDetailsTable.addCell(productDetailsTableCell41);
			
			PdfPCell productDetailsTableCell40=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell40Phrase = new Phrase();
			productDetailsTableCell40Phrase.setLeading(12);
			productDetailsTableCell40Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell40.addElement(productDetailsTableCell40Phrase);
			productDetailsTable.addCell(productDetailsTableCell40);

			PdfPCell productDetailsTableCell55=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell55Phrase = new Phrase();
			productDetailsTableCell55Phrase.setLeading(12);
			productDetailsTableCell55Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell55.addElement(productDetailsTableCell55Phrase);
			productDetailsTable.addCell(productDetailsTableCell55);
			
			PdfPCell productDetailsTableCell58=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell59Phrase = new Phrase();
			productDetailsTableCell59Phrase.setLeading(12);
			productDetailsTableCell59Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell58.addElement(productDetailsTableCell59Phrase);
			productDetailsTable.addCell(productDetailsTableCell58);
			
			PdfPCell productDetailsTableCell62=new TopRightBorderPDFCell();
			Phrase productDetailsTableCell62Phrase = new Phrase();
			productDetailsTableCell62Phrase.setLeading(12);
			productDetailsTableCell62Phrase.add(new Chunk("", mediumBold));
			productDetailsTableCell62.addElement(productDetailsTableCell62Phrase);
			productDetailsTable.addCell(productDetailsTableCell62);
			
			PdfPCell productDetailsTableCell42=new TopBorderPDFCell();
			Paragraph productDetailsTableCell42Phrase = new Paragraph();
			productDetailsTableCell42Phrase.setAlignment(Element.ALIGN_RIGHT);
			productDetailsTableCell42Phrase.setLeading(12);
			productDetailsTableCell42Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalAmountAfterRoundOff(), mediumBold));
			productDetailsTableCell42.addElement(productDetailsTableCell42Phrase);
			productDetailsTable.addCell(productDetailsTableCell42);
			
			productDetailsTableCellTemp=new PdfPCell(productDetailsTable);
			productDetailsTableCellTemp.setPadding(0);
			productDetailsTableCellTemp.setColspan(2);
			mainTable.addCell(productDetailsTableCellTemp);
			
			
			PdfPCell totalAmountTableCell=new PdfPCell();
			totalAmountTableCell.setColspan(2);
			Phrase totalAmountTableCellPhrase = new Phrase();
			totalAmountTableCellPhrase.setLeading(12);
			totalAmountTableCellPhrase.add(new Chunk("Amount Chargeable(in words)\n", mediumNormal));
			totalAmountTableCellPhrase.add(new Chunk("INR "+inventoryAddedInvoiceModel.getTotalAmountAfterRoundOffInWord(), mediumBold));			
			totalAmountTableCell.addElement(totalAmountTableCellPhrase);			
			mainTable.addCell(totalAmountTableCell);
			
			PdfPTable taxDetailsTable = new PdfPTable(new float[] {25,25,40,40,40});
			taxDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			taxDetailsTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell1=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell1Phrase = new Paragraph();
			taxDetailsTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell1Phrase.setLeading(10);
			taxDetailsTableCell1Phrase.add(new Chunk("\nHSN/SAC", mediumBold));
			taxDetailsTableCell1.addElement(taxDetailsTableCell1Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell1);
			
			PdfPCell taxDetailsTableCell2=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell2Phrase = new Paragraph();
			taxDetailsTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell2Phrase.setLeading(10);
			taxDetailsTableCell2Phrase.add(new Chunk("\nTaxable Value", mediumBold));
			taxDetailsTableCell2.addElement(taxDetailsTableCell2Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell2);
						
			PdfPTable cgstTable = new PdfPTable(new float[] { 20 ,20});
			cgstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstTable.setWidthPercentage(100);
			
			PdfPCell cgstTableCell1=new PdfPCell();
			cgstTableCell1.setBorder(Rectangle.NO_BORDER);
			cgstTableCell1.setColspan(2);				
			Paragraph cgstTableCell1Phrase = new Paragraph();
			cgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell1Phrase.setLeading(10);
			cgstTableCell1Phrase.add(new Chunk("CGST", mediumBold));
			cgstTableCell1.addElement(cgstTableCell1Phrase);
			cgstTable.addCell(cgstTableCell1);
			
			PdfPCell cgstTableCell2=new TopRightBorderPDFCell();
			Paragraph cgstTableCell2Phrase = new Paragraph();
			cgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell2Phrase.setLeading(10);
			cgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			cgstTableCell2.addElement(cgstTableCell2Phrase);
			cgstTable.addCell(cgstTableCell2);
			
			PdfPCell cgstTableCell3=new TopBorderPDFCell();
			Paragraph cgstTableCell3Phrase = new Paragraph();
			cgstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			cgstTableCell3Phrase.setLeading(10);
			cgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			cgstTableCell3.addElement(cgstTableCell3Phrase);
			cgstTable.addCell(cgstTableCell3);
			
			PdfPCell taxDetailsTableCell3=new PdfPCell(cgstTable);
			taxDetailsTableCell3.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell3);
			
			PdfPTable sgstTable = new PdfPTable(new float[] { 20 ,20});
			sgstTable.setWidthPercentage(100);
			
			PdfPCell sgstTableCell1=new PdfPCell();
			sgstTableCell1.setBorder(Rectangle.NO_BORDER);
			sgstTableCell1.setColspan(2);
			Paragraph sgstTableCell1Phrase = new Paragraph();
			sgstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell1Phrase.setLeading(10);
			sgstTableCell1Phrase.add(new Chunk("SGST", mediumBold));
			sgstTableCell1.addElement(sgstTableCell1Phrase);
			sgstTable.addCell(sgstTableCell1);
			
			PdfPCell sgstTableCell2=new TopRightBorderPDFCell();
			Paragraph sgstTableCell2Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell2Phrase.setLeading(10);
			sgstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			sgstTableCell2.addElement(sgstTableCell2Phrase);
			sgstTable.addCell(sgstTableCell2);
			
			PdfPCell sgstTableCell3=new TopBorderPDFCell();
			Paragraph sgstTableCell3Phrase = new Paragraph();
			sgstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			sgstTableCell3Phrase.setLeading(10);
			sgstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			sgstTableCell3.addElement(sgstTableCell3Phrase);
			sgstTable.addCell(sgstTableCell3);
			
			PdfPCell taxDetailsTableCell4=new PdfPCell(sgstTable);
			taxDetailsTableCell4.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell4);

			PdfPTable igstTable = new PdfPTable(new float[] { 20 ,20});
			igstTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstTable.setWidthPercentage(100);
			
			PdfPCell igstTableCell1=new PdfPCell();
			igstTableCell1.setBorder(Rectangle.NO_BORDER);
			igstTableCell1.setColspan(2);
			Paragraph igstTableCell1Phrase = new Paragraph();
			igstTableCell1Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell1Phrase.setLeading(10);
			igstTableCell1Phrase.add(new Chunk("IGST", mediumBold));
			igstTableCell1.addElement(igstTableCell1Phrase);
			igstTable.addCell(igstTableCell1);
			
			PdfPCell igstTableCell2=new TopRightBorderPDFCell();
			Paragraph igstTableCell2Phrase = new Paragraph();
			igstTableCell2Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell2Phrase.setLeading(10);
			igstTableCell2Phrase.add(new Chunk("Rate", mediumBold));
			igstTableCell2.addElement(igstTableCell2Phrase);
			igstTable.addCell(igstTableCell2);
			
			PdfPCell igstTableCell3=new TopBorderPDFCell();
			Paragraph igstTableCell3Phrase = new Paragraph();
			igstTableCell3Phrase.setAlignment(Element.ALIGN_CENTER);
			igstTableCell3Phrase.setLeading(10);
			igstTableCell3Phrase.add(new Chunk("Amount", mediumBold));
			igstTableCell3.addElement(igstTableCell3Phrase);
			igstTable.addCell(igstTableCell3);
			
			PdfPCell taxDetailsTableCell5=new PdfPCell(igstTable);
			taxDetailsTableCell5.setPadding(0);
			taxDetailsTable.addCell(taxDetailsTableCell5);
		
			for(Map<String, Object> categoryWiseAmountForBill:inventoryAddedInvoiceModel.getTaxList())
			{
				// hsnCode, taxableValue, cgstRate,
				// cgstAmt, sgstRate, sgstAmt, igstRate,
				// igstAmt
				
				PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell6Phrase = new Paragraph();
				taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell6Phrase.setLeading(10);
				taxDetailsTableCell6Phrase.add(new Chunk(categoryWiseAmountForBill.get("hsnCode")+"", mediumNormal));
				taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell6);
				
				PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
				Paragraph taxDetailsTableCell7Phrase = new Paragraph();
				taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell7Phrase.setLeading(10);
				taxDetailsTableCell7Phrase.add(new Chunk(categoryWiseAmountForBill.get("taxableValue")+"", mediumNormal));
				taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
				taxDetailsTable.addCell(taxDetailsTableCell7);
				
				PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
				cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				cgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell8Phrase = new Paragraph();
				taxDetailsTableCell8Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell8Phrase.setLeading(10);
				taxDetailsTableCell8Phrase.add(new Chunk(categoryWiseAmountForBill.get("cgstRate")+" %", mediumNormal));
				taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
				cgstValueTable.addCell(taxDetailsTableCell8);
				
				PdfPCell taxDetailsTableCell9=new PdfPCell();
				taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell9Phrase = new Paragraph();
				taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell9Phrase.setLeading(10);
				taxDetailsTableCell9Phrase.add(new Chunk(categoryWiseAmountForBill.get("cgstAmt")+"", mediumNormal));
				taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
				cgstValueTable.addCell(taxDetailsTableCell9);
				
				PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
				cgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(cgstValueTableCell);
				
				PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
				sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				sgstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell10Phrase = new Paragraph();
				taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell10Phrase.setLeading(10);
				taxDetailsTableCell10Phrase.add(new Chunk(categoryWiseAmountForBill.get("sgstRate")+" %", mediumNormal));
				taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
				sgstValueTable.addCell(taxDetailsTableCell10);
				
				PdfPCell taxDetailsTableCell11=new PdfPCell();
				taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell11Phrase = new Paragraph();
				taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell11Phrase.setLeading(10);
				taxDetailsTableCell11Phrase.add(new Chunk(categoryWiseAmountForBill.get("sgstAmt")+"", mediumNormal));
				taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
				sgstValueTable.addCell(taxDetailsTableCell11);
				
				PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
				sgstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(sgstValueTableCell);
				
				PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
				igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				igstValueTable.setWidthPercentage(100);
				
				PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
				Paragraph taxDetailsTableCell12Phrase = new Paragraph();
				taxDetailsTableCell12Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell12Phrase.setLeading(10);
				taxDetailsTableCell12Phrase.add(new Chunk(categoryWiseAmountForBill.get("igstRate")+" %", mediumNormal));
				taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
				igstValueTable.addCell(taxDetailsTableCell12);
				
				PdfPCell taxDetailsTableCell13=new PdfPCell();
				taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
				Paragraph taxDetailsTableCell13Phrase = new Paragraph();
				taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
				taxDetailsTableCell13Phrase.setLeading(10);
				taxDetailsTableCell13Phrase.add(new Chunk(categoryWiseAmountForBill.get("igstAmt")+"", mediumNormal));
				taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
				igstValueTable.addCell(taxDetailsTableCell13);
				
				PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
				igstValueTableCell.setPadding(0);
				taxDetailsTable.addCell(igstValueTableCell);				
						
			}	
	
			PdfPCell taxDetailsTableCell6=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell6Phrase = new Paragraph();
			taxDetailsTableCell6Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell6Phrase.setLeading(10);
			taxDetailsTableCell6Phrase.add(new Chunk("Total", mediumBold));
			taxDetailsTableCell6.addElement(taxDetailsTableCell6Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell6);
			
			PdfPCell taxDetailsTableCell7=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell7Phrase = new Paragraph();
			taxDetailsTableCell7Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell7Phrase.setLeading(10);
			taxDetailsTableCell7Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalTaxableAmt(), mediumNormal));
			taxDetailsTableCell7.addElement(taxDetailsTableCell7Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell7);
			
			PdfPTable cgstValueTable = new PdfPTable(new float[] { 20 ,20});
			cgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell8=new RightBorderPDFCell();
			Phrase taxDetailsTableCell8Phrase = new Phrase();
			taxDetailsTableCell8Phrase.setLeading(10);
			taxDetailsTableCell8Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell8.addElement(taxDetailsTableCell8Phrase);
			cgstValueTable.addCell(taxDetailsTableCell8);
			
			PdfPCell taxDetailsTableCell9=new PdfPCell();
			taxDetailsTableCell9.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell9Phrase = new Paragraph();
			taxDetailsTableCell9Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell9Phrase.setLeading(10);
			taxDetailsTableCell9Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalCgstAmt(), mediumNormal));
			taxDetailsTableCell9.addElement(taxDetailsTableCell9Phrase);
			cgstValueTable.addCell(taxDetailsTableCell9);
			
			PdfPCell cgstValueTableCell=new PdfPCell(cgstValueTable);
			cgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCell);
			
			PdfPTable sgstValueTable = new PdfPTable(new float[] { 20 ,20});
			sgstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell10=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell10Phrase = new Paragraph();
			taxDetailsTableCell10Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell10Phrase.setLeading(10);
			taxDetailsTableCell10Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell10.addElement(taxDetailsTableCell10Phrase);
			sgstValueTable.addCell(taxDetailsTableCell10);
			
			PdfPCell taxDetailsTableCell11=new PdfPCell();
			taxDetailsTableCell11.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell11Phrase = new Paragraph();
			taxDetailsTableCell11Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell11Phrase.setLeading(10);
			taxDetailsTableCell11Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalSgstAmt(), mediumNormal));
			taxDetailsTableCell11.addElement(taxDetailsTableCell11Phrase);
			sgstValueTable.addCell(taxDetailsTableCell11);
			
			PdfPCell sgstValueTableCell=new PdfPCell(sgstValueTable);
			sgstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCell);
			
			PdfPTable igstValueTable = new PdfPTable(new float[] { 20 ,20});
			igstValueTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTable.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell12=new RightBorderPDFCell();
			Phrase taxDetailsTableCell12Phrase = new Phrase();
			taxDetailsTableCell12Phrase.setLeading(10);
			taxDetailsTableCell12Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell12.addElement(taxDetailsTableCell12Phrase);
			igstValueTable.addCell(taxDetailsTableCell12);
			
			PdfPCell taxDetailsTableCell13=new PdfPCell();
			taxDetailsTableCell13.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell13Phrase = new Paragraph();
			taxDetailsTableCell13Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell13Phrase.setLeading(10);
			taxDetailsTableCell13Phrase.add(new Chunk(inventoryAddedInvoiceModel.getTotalIgstAmt(), mediumNormal));
			taxDetailsTableCell13.addElement(taxDetailsTableCell13Phrase);
			igstValueTable.addCell(taxDetailsTableCell13);
			
			PdfPCell igstValueTableCell=new PdfPCell(igstValueTable);
			igstValueTableCell.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCell);
			
			/* disc amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell14=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell14Phrase = new Paragraph();
			taxDetailsTableCell14Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell14Phrase.setLeading(10);
			taxDetailsTableCell14Phrase.add(new Chunk("Discount", mediumBold));
			taxDetailsTableCell14.addElement(taxDetailsTableCell14Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell14);
			
			PdfPCell taxDetailsTableCell15=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell15Phrase = new Paragraph();
			taxDetailsTableCell15Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell15Phrase.setLeading(10);
			taxDetailsTableCell15Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDiscTaxableAmt(), mediumNormal));
			taxDetailsTableCell15.addElement(taxDetailsTableCell15Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell15);
			
			PdfPTable cgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell16=new RightBorderPDFCell();
			Phrase taxDetailsTableCell16Phrase = new Phrase();
			taxDetailsTableCell16Phrase.setLeading(10);
			taxDetailsTableCell16Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell16.addElement(taxDetailsTableCell16Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell16);
			
			PdfPCell taxDetailsTableCell17=new PdfPCell();
			taxDetailsTableCell17.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell17Phrase = new Paragraph();
			taxDetailsTableCell17Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell17Phrase.setLeading(10);
			taxDetailsTableCell17Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDiscCgstAmt(), mediumNormal));
			taxDetailsTableCell17.addElement(taxDetailsTableCell17Phrase);
			cgstValueTableDisc.addCell(taxDetailsTableCell17);
			
			PdfPCell cgstValueTableCellDisc=new PdfPCell(cgstValueTableDisc);
			cgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellDisc);
			
			PdfPTable sgstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell18=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell18Phrase = new Paragraph();
			taxDetailsTableCell18Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell18Phrase.setLeading(10);
			taxDetailsTableCell18Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell18.addElement(taxDetailsTableCell18Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell18);
			
			PdfPCell taxDetailsTableCell19=new PdfPCell();
			taxDetailsTableCell19.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell19Phrase = new Paragraph();
			taxDetailsTableCell19Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell19Phrase.setLeading(10);
			taxDetailsTableCell19Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDiscSgstAmt(), mediumNormal));
			taxDetailsTableCell19.addElement(taxDetailsTableCell19Phrase);
			sgstValueTableDisc.addCell(taxDetailsTableCell19);
			
			PdfPCell sgstValueTableCellDisc=new PdfPCell(sgstValueTableDisc);
			sgstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellDisc);
			
			PdfPTable igstValueTableDisc = new PdfPTable(new float[] { 20 ,20});
			igstValueTableDisc.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableDisc.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell20=new RightBorderPDFCell();
			Phrase taxDetailsTableCell20Phrase = new Phrase();
			taxDetailsTableCell20Phrase.setLeading(10);
			taxDetailsTableCell20Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell20.addElement(taxDetailsTableCell20Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell20);
			
			PdfPCell taxDetailsTableCell21=new PdfPCell();
			taxDetailsTableCell21.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell21Phrase = new Paragraph();
			taxDetailsTableCell21Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell21Phrase.setLeading(10);
			taxDetailsTableCell21Phrase.add(new Chunk(inventoryAddedInvoiceModel.getDiscIgstAmt(), mediumNormal));
			taxDetailsTableCell21.addElement(taxDetailsTableCell21Phrase);
			igstValueTableDisc.addCell(taxDetailsTableCell21);
			
			PdfPCell igstValueTableCellDisc=new PdfPCell(igstValueTableDisc);
			igstValueTableCellDisc.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellDisc);
			/* disc amount in hsn code wise part end*/
			
			/* net amount in hsn code wise part begin*/
			PdfPCell taxDetailsTableCell22=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell22Phrase = new Paragraph();
			taxDetailsTableCell22Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell22Phrase.setLeading(10);
			taxDetailsTableCell22Phrase.add(new Chunk("Net Total", mediumBold));
			taxDetailsTableCell22.addElement(taxDetailsTableCell22Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell22);
			
			PdfPCell taxDetailsTableCell23=new TopRightBorderPDFCell();
			Paragraph taxDetailsTableCell23Phrase = new Paragraph();
			taxDetailsTableCell23Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell23Phrase.setLeading(10);
			taxDetailsTableCell23Phrase.add(new Chunk(inventoryAddedInvoiceModel.getNetTaxableAmt(), mediumNormal));
			taxDetailsTableCell23.addElement(taxDetailsTableCell23Phrase);
			taxDetailsTable.addCell(taxDetailsTableCell23);
			
			PdfPTable cgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			cgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell24=new RightBorderPDFCell();
			Phrase taxDetailsTableCell24Phrase = new Phrase();
			taxDetailsTableCell24Phrase.setLeading(10);
			taxDetailsTableCell24Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell24.addElement(taxDetailsTableCell24Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell24);
			
			PdfPCell taxDetailsTableCell25=new PdfPCell();
			taxDetailsTableCell25.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell25Phrase = new Paragraph();
			taxDetailsTableCell25Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell25Phrase.setLeading(10);
			taxDetailsTableCell25Phrase.add(new Chunk(inventoryAddedInvoiceModel.getNetCgstAmt(), mediumNormal));
			taxDetailsTableCell25.addElement(taxDetailsTableCell25Phrase);
			cgstValueTableNet.addCell(taxDetailsTableCell25);
			
			PdfPCell cgstValueTableCellNet=new PdfPCell(cgstValueTableNet);
			cgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(cgstValueTableCellNet);
			
			PdfPTable sgstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			sgstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			sgstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell26=new RightBorderPDFCell();
			Paragraph taxDetailsTableCell26Phrase = new Paragraph();
			taxDetailsTableCell26Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell26Phrase.setLeading(10);
			taxDetailsTableCell26Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell26.addElement(taxDetailsTableCell26Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell26);
			
			PdfPCell taxDetailsTableCell27=new PdfPCell();
			taxDetailsTableCell27.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell27Phrase = new Paragraph();
			taxDetailsTableCell27Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell27Phrase.setLeading(10);
			taxDetailsTableCell27Phrase.add(new Chunk(inventoryAddedInvoiceModel.getNetSgstAmt(), mediumNormal));
			taxDetailsTableCell27.addElement(taxDetailsTableCell27Phrase);
			sgstValueTableNet.addCell(taxDetailsTableCell27);
			
			PdfPCell sgstValueTableCellNet=new PdfPCell(sgstValueTableNet);
			sgstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(sgstValueTableCellNet);
			
			PdfPTable igstValueTableNet = new PdfPTable(new float[] { 20 ,20});
			igstValueTableNet.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			igstValueTableNet.setWidthPercentage(100);
			
			PdfPCell taxDetailsTableCell28=new RightBorderPDFCell();
			Phrase taxDetailsTableCell28Phrase = new Phrase();
			taxDetailsTableCell28Phrase.setLeading(10);
			taxDetailsTableCell28Phrase.add(new Chunk(" ", mediumNormal));
			taxDetailsTableCell28.addElement(taxDetailsTableCell28Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell28);
			
			PdfPCell taxDetailsTableCell29=new PdfPCell();
			taxDetailsTableCell29.setBorder(Rectangle.NO_BORDER);
			Paragraph taxDetailsTableCell29Phrase = new Paragraph();
			taxDetailsTableCell29Phrase.setAlignment(Element.ALIGN_CENTER);
			taxDetailsTableCell29Phrase.setLeading(10);
			taxDetailsTableCell29Phrase.add(new Chunk(inventoryAddedInvoiceModel.getNetIgstAmt(), mediumNormal));
			taxDetailsTableCell29.addElement(taxDetailsTableCell29Phrase);
			igstValueTableNet.addCell(taxDetailsTableCell29);
			
			PdfPCell igstValueTableCellNet=new PdfPCell(igstValueTableNet);
			igstValueTableCellNet.setPadding(0);
			taxDetailsTable.addCell(igstValueTableCellNet);
			/* net amount in hsn code wise part end*/
		
			
			PdfPCell taxDetailsMainCell=new PdfPCell(taxDetailsTable);
			taxDetailsMainCell.setColspan(2);
			taxDetailsMainCell.setPadding(0);
			mainTable.addCell(taxDetailsMainCell);
			
			PdfPCell taxAmountTableCell=new PdfPCell();
			taxAmountTableCell.setColspan(2);
			Phrase taxAmountTableCellPhrase = new Phrase();
			taxAmountTableCellPhrase.setLeading(12);
			taxAmountTableCellPhrase.add(new Chunk("Tax Amount(in words)\n", mediumNormal));
			taxAmountTableCellPhrase.add(new Chunk("INR "+inventoryAddedInvoiceModel.getNetTaxAmtInWord(), mediumBold));			
			taxAmountTableCell.addElement(taxAmountTableCellPhrase);			
			mainTable.addCell(taxAmountTableCell);
			
			
			PdfPTable lastRowTable = new PdfPTable(new float[] { 60 ,20});
            lastRowTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            lastRowTable.setWidthPercentage(100);
            
            PdfPCell declarationTableCell=new RightBorderPDFCell();
            Phrase declarationTableCellPhrase = new Phrase();
            declarationTableCellPhrase.setLeading(12);
            declarationTableCellPhrase.add(new Chunk("Declaration\n", mediumBold));
            declarationTableCellPhrase.add(new Chunk("We declare that this invoice shows the actual price of the goods described and that all particulars are true and corrent", mediumNormal));            
            declarationTableCell.addElement(declarationTableCellPhrase);        
            lastRowTable.addCell(declarationTableCell);
            
            PdfPCell signTableCell=new PdfPCell();
            Phrase signTableCellPhrase = new Phrase();
            signTableCellPhrase.setLeading(12);
            signTableCellPhrase.add(new Chunk(inventoryAddedInvoiceModel.getCompanyName()+"\n\n\n", mediumBold));
            signTableCellPhrase.add(new Chunk("Authorized Signatory", mediumBold));            
            signTableCell.addElement(signTableCellPhrase);        
            lastRowTable.addCell(signTableCell);
            
            PdfPCell lastRowMainCell=new PdfPCell(lastRowTable);
            lastRowMainCell.setColspan(2);
            lastRowMainCell.setPadding(0);
            //lastRowMainCell.addElement(lastRowTable);    
            mainTable.addCell(lastRowMainCell);            
            
            document.add(mainTable);
            
			String footer="This is a Computer Generated Invoice";
			
			Paragraph paragraphFooter = new Paragraph(footer);
			//Paragraph paragraphFooter = new Paragraph("We offer a 7 Day Return for our valued customers. If you are not 100% satisfied with your purchase, \nwe accept returns for items within 7 days of delivery.\n\nThis is a Computer Generated Invoice", mediumNormal);
			paragraphFooter.setAlignment(Element.ALIGN_CENTER);
			paragraphFooter.add(new Paragraph(" "));
			document.add(paragraphFooter);
			
			// Paragraph paragraphFooter1 = new Paragraph("This is a Computer Generated Invoice", mediumNormal);
			// paragraphFooter1.setAlignment(Element.ALIGN_CENTER);
			// paragraphFooter1.add(new Paragraph(" "));
			// document.add(paragraphFooter1);
			
			document.add( Chunk.NEWLINE );
			
			/*InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
			pdfWriter.setPageEvent(invoiceGenerator.new MyFooter());*/
			
			document.close();
			System.out.println("done");
			return pdfFile;
	
		
	}
	
	
	
public static void main(String[] args) throws DocumentException, IOException {
	//generateInvoicePdf();
/*	Document document = new Document(PageSize.A4);
	PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(new File("D:\\abc.pdf")));
	InvoiceGenerator invoiceGenerator=new InvoiceGenerator();
	writer.setPageEvent(invoiceGenerator.new MyFooter());*/
	/*BillPrintDataModel billPrintDataModel=new BillPrintDataModel();
	BusinessName businessName=new BusinessName();
	Company company=new Company();
	company.setContact(new Contact());
	businessName.setCompany(company);
	billPrintDataModel.setBusinessName(null);
	billPrintDataModel.setCategoryWiseAmountForBills(new ArrayList<CategoryWiseAmountForBill>());
	Employee employee=new Employee();
	employee.setCompany(company);
	billPrintDataModel.setEmployeeGKCounterOrder(employee);
	billPrintDataModel.setProductListForBill(new ArrayList<>());
	
	generateSMOrderCreditNotePdf(billPrintDataModel, "E://Projects//BlueSquareNew//bluesquareWebApp10072018//sss.pdf");*/
}


}
