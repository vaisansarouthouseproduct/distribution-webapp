package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.OrderDetails;

@Component
public class CounterOrderIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public CounterOrderIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "CORD";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="select counterOrderId from CounterOrder where employeeGk.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by counterOrderPKId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("CounterOrderIdGenerator : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(4));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("CounterOrderIdGenerator : " + generatedId);
     	        return generatedId;	
        	}
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<CounterOrder> list=(List<CounterOrder>)query.list();
        	
        	if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("CounterOrderIdGenerator : " + generatedId);
                
                return generatedId;
        	}
        	        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(CounterOrder counterOrder : list)
        	{
        		ids[i]=Long.parseLong(counterOrder.getCounterOrderId().substring(4));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("CounterOrderIdGenerator : " + generatedId);
                return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("CounterOrderIdGenerator Error : "+e.toString());        }

        return null;
	}

    
    public String generateProformaOrderId() {

        try {
        	String prefix = "PORD";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="select proformaOrderId from ProformaOrder where employeeGk.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by proformaOrderPKId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                //system.out.println("CounterOrderIdGenerator : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(4));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        //system.out.println("CounterOrderIdGenerator : " + generatedId);
     	        return generatedId;	
        	}
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<CounterOrder> list=(List<CounterOrder>)query.list();
        	
        	if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                //system.out.println("CounterOrderIdGenerator : " + generatedId);
                
                return generatedId;
        	}
        	        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(CounterOrder counterOrder : list)
        	{
        		ids[i]=Long.parseLong(counterOrder.getCounterOrderId().substring(4));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                //system.out.println("CounterOrderIdGenerator : " + generatedId);
                return generatedId;*/
            
        } catch (Exception e) {
            //system.out.println("CounterOrderIdGenerator Error : "+e.toString());       
        	}

        return null;
	}

}
