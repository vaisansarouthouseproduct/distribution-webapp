/**
 * 
 */
package com.bluesquare.rc.utils;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;


/**
 * @author aNKIT
 *
 */
@Component
public class CreditNoteInvoiceNumberGenerate {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public CreditNoteInvoiceNumberGenerate(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generateInvoiceNumber() {

        try {
        	String prefix = "CN-";
        	String postfix="/"+displayFinancialDate(Calendar.getInstance());

        	String hql="SELECT max(CAST(reverse(SUBSTR(reverse(invoice_number),9,LENGTH(reverse(invoice_number)))) as unsigned))"+
			"FROM ("+
			"(select SUBSTR(invoice_number,4,LENGTH(invoice_number))'invoice_number' from return_counter_order where invoice_number is not null)"+
			" UNION "+
			"(select SUBSTR(invoice_number,4,LENGTH(invoice_number))'invoice_number' from return_order_product_permanent where invoice_number is not null)"+
			") AS i";
        	
        	SQLQuery query=sessionFactory.getCurrentSession().createSQLQuery(hql); 
        	List<BigInteger> list=query.list();
        	
            long id;
    		
    		if(list.get(0)==null){
        		String generatedId = prefix + new Long(1).toString()+postfix;
                System.out.println("generateOrderId : " + generatedId);
                
                return generatedId;
        	}else{
        		//long max=Long.parseLong(list.get(0));
        		
        		id=Long.parseLong((int)list.get(0).floatValue()+"")+1;
     	        String generatedId = prefix + new Long(id).toString()+postfix;
     	        System.out.println("return counter order invoice number : " + generatedId);
     	        return generatedId;	
        	}
        	
        	
        	/*//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	//OrderDetails Start
        	String hql="from OrderDetails where invoiceNumber is not null and businessName.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();        	      	
        	//OrderDetails End
        	
        	//CounterOrder Start
        	hql="from CounterOrder where invoiceNumber is not null and employeeGk.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();        	
        	query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
        	//CounterOrder End
        	
        	long ids[]=new long[orderDetailsList.size()+counterOrderList.size()]; 
        	int i=0;
        	
        	if(orderDetailsList.isEmpty() && counterOrderList.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
        	}else if(counterOrderList.isEmpty()){
        		for(OrderDetails orderDetails : orderDetailsList){
            		
            		ids[i]=Long.parseLong(orderDetails.getInvoiceNumber().substring(3));
    	        	i++;
            	}
        	}else if(orderDetailsList.isEmpty()){
        		for(CounterOrder counterOrder : counterOrderList){
            		
            		ids[i]=Long.parseLong(counterOrder.getInvoiceNumber().substring(3));
    	        	i++;
            	}
        	}else{
        		for(OrderDetails orderDetails : orderDetailsList){            		
            		ids[i]=Long.parseLong(orderDetails.getInvoiceNumber().substring(3));
    	        	i++;
            	}
				for(CounterOrder counterOrder : counterOrderList){					
					ids[i]=Long.parseLong(counterOrder.getInvoiceNumber().substring(3));
					i++;
				}
        	}        	
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
            
            if(ids.length==0)
            {
            	String generatedId = prefix + new Long(1).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("InvoiceNumberGenerate Id: " + generatedId);
            
            return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("InvoiceNumberGenerate Error : "+e.toString());        }

        return null;
	}
    
    public static void main(String[] args) {
    	System.out.println(displayFinancialDate(Calendar.getInstance()));
	}
    private static final int    FIRST_FISCAL_MONTH  = Calendar.APRIL;
    public static int getFiscalYear(Calendar calendarDate) {
    	
        int month = calendarDate.get(Calendar.MONTH);
        int year = calendarDate.get(Calendar.YEAR);
        return (month >= FIRST_FISCAL_MONTH) ? year : year - 1;
    }

    private static String displayFinancialDate(Calendar calendar) {
        int year = getFiscalYear(calendar);
        //System.out.println("Fiscal Years : " + year + "-" + (year-2000 + 1));
        return year + "-" + (year-2000 + 1);
    }

}
