package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Supplier;

@Component
public class SupplierIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public SupplierIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public SupplierIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
	
	public String generateSupplierId() {

        try {
        	
        	String prefix = "SUP";

        	String hql="select supplierId from Supplier where company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by supplierPKId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateSupplierId : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(3));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("generateSupplierId : " + generatedId);
     	        return generatedId;	
        	}
        	
        	
        	/*Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<Supplier> list=(List<Supplier>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateSupplierId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(Supplier supplier : list)
        	{
        		ids[i]=Long.parseLong(supplier.getSupplierId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("generateSupplierId : " + generatedId);
            
            return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("generateSupplierId Error : "+e.toString());        }

        return null;
	}

}
