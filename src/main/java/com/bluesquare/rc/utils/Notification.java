package com.bluesquare.rc.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;

public class Notification {
	
	public static boolean sendNotificationToGateKeeperForOrder(String token,String salesPersonId,String shopName,String orderId) {
		System.out.println("inside sendNotificationToGateKeeperForOrder");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				/*post.setHeader("Authorization",
						"key=AAAAaBuk8eg:APA91bF15u2SgD82WOYj8p6DDdz-vGcSt3aX_zXo4rbzIfNlu-4vD3wj4gia4O7ymrjsCfI2lbrD49_7oR4nAMTEUdA4jR-dLeWc2-sg47DHw88_6Patny50MgHTcLJkQ8LpnfGYQukr");*/
				post.setHeader("Authorization",
						"key=AAAA3nRsyCU:APA91bHp_ZcatdPKmIDH9hh7TVtH3IqlbeD5FrhaOwRhtS-jbN6Z-9apWyJES3kOYA0emgUHLkoZJiV0221dKL76EyFWq62yG4b-hYsOTYDMhJNbafDqDR_cfOhahBKaNH9lzJYZVrvR");
				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("orderDetailsId", orderId);

				message.put("data", data);

				JSONObject notification = new JSONObject();
				notification.put("title", "Blue Square");
				notification.put("click_action", "OPEN_ORDER_DETAILS_GK");
				notification.put("body", "Order Booked From " + salesPersonId + " for Shop Name : "+ shopName);

				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}
	
	public static boolean sendNotificationToDeliveryBoyForOrder(String token,String gkId,String shopName,String orderId,String clickAction) {
		System.out.println("inside sendNotificationToDeliveryBoyForOrder");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				post.setHeader("Authorization",
						"key=AAAAaBuk8eg:APA91bF15u2SgD82WOYj8p6DDdz-vGcSt3aX_zXo4rbzIfNlu-4vD3wj4gia4O7ymrjsCfI2lbrD49_7oR4nAMTEUdA4jR-dLeWc2-sg47DHw88_6Patny50MgHTcLJkQ8LpnfGYQukr");

				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("orderDetailsId", orderId);

				message.put("data", data);

				JSONObject notification = new JSONObject();
				notification.put("title", "Blue Square");
				notification.put("click_action", clickAction);
				notification.put("body", "Order Packed From " + gkId + " for Shop Name : "+ shopName);

				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send DB SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not DB Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}

	public static boolean sendNotificationToEmployeeForComplain(String token, String employeeName,long complainReplyId) {
		System.out.println("inside sendNotificationToEmployeeForComplain");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				post.setHeader("Authorization",
						"key=AAAAaBuk8eg:APA91bF15u2SgD82WOYj8p6DDdz-vGcSt3aX_zXo4rbzIfNlu-4vD3wj4gia4O7ymrjsCfI2lbrD49_7oR4nAMTEUdA4jR-dLeWc2-sg47DHw88_6Patny50MgHTcLJkQ8LpnfGYQukr");

				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("complainId", complainReplyId);

				message.put("data", data);

				JSONObject notification = new JSONObject();
				notification.put("title", "Blue Square");
				notification.put("click_action", "OPEN_COMPLAIN_DETAILS");
				notification.put("body", "Message get From " + employeeName);

				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send  SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not  Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}
}
