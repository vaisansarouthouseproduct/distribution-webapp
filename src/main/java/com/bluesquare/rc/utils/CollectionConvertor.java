package com.bluesquare.rc.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionConvertor {

	public static Set listToSetConvert(List objectList){
		Set objectSet=new HashSet<>();
		for(Object objects : objectList ){
			objectSet.add(objects);
		}
		return objectSet;
	}
	
	public static List arrayToList(Object[] arr){
		List objectList=new ArrayList<>();
		for(Object objects : arr ){
			objectList.add(objects);
		}
		return objectList;
	}
}
