package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.OrderDetails;

@Component
public class OrderIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public OrderIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "ORD";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="select orderId from OrderDetails where "
        			+ "businessName.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+
        			" order by orderPkId desc";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql);
        	query.setMaxResults(1);
        	List<String> list=(List<String>)query.list();
        	
        	long id;
    		
    		if(list.isEmpty()){
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateOrderId : " + generatedId);
                
                return generatedId;
        	}else{
        		long max=Long.parseLong(list.get(0).substring(3));
        		
        		id=max+1;
     	        String generatedId = prefix + new Long(id).toString();
     	        System.out.println("generateOrderId : " + generatedId);
     	        return generatedId;	
        	}
        	
   /*     	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<OrderDetails> list=(List<OrderDetails>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateOrderId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(OrderDetails orderDetails : list)
        	{
        		ids[i]=Long.parseLong(orderDetails.getOrderId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("OrderDetails Id: " + generatedId);
                return generatedId;*/
            
        } catch (Exception e) {
            System.out.println("OrderIdGenerator Error : "+e.toString());        }

        return null;
	}

}
