package com.bluesquare.rc.utils;
import java.text.DecimalFormat;

public class RoundOff {

	public static double findRoundOffAmount(double amount){
		/* roundOff */
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		//find round of amount
		float totalAmountAfterRoundOff=Float.parseFloat(decimalFormat.format(amount));
		totalAmountAfterRoundOff=Float.parseFloat(decimalFormat.format(totalAmountAfterRoundOff));
		float decimalAmount=totalAmountAfterRoundOff-(int)totalAmountAfterRoundOff;
		float roundOff;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOff=0.0f;
			roundOfAmount=""+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff+roundOff;
		}
		else if(decimalAmount>=0.5)
		{
			roundOff=1-decimalAmount;
			roundOfAmount="+"+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff+roundOff;
		}
		else
		{
			roundOff=decimalAmount;
			roundOfAmount="-"+decimalFormat.format(roundOff);
			totalAmountAfterRoundOff=totalAmountAfterRoundOff-roundOff;
		}	
		
		return totalAmountAfterRoundOff;
	}
}
