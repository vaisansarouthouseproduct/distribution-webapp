package com.bluesquare.rc.utils;

public class Constants {
	public static String SUCCESS_RESPONSE="Success";
	public static String FAILURE_RESPONSE="Failure";
	
	public static String OFFLINE_STATUS_INITIATED="initiated";
	public static String OFFLINE_STATUS_COMPLETE="complete";
	
	public static String RETURN_EMPTY_DATA="---------------------Returning Empty Data";
	public static String RETURN_DATA="---------------------Returning Data";
	
	public static String ORDER_RESPONSE="Order Succesfully Done";
	
	public static String ADMIN = "Admin";
	public static String COMPANY_ADMIN = "CompanyAdmin";
	public static String GATE_KEEPER_DEPT_NAME = "GateKeeper";
	public static String SALESMAN_DEPT_NAME = "SalesMan";
	public static String DELIVERYBOY_DEPT_NAME = "DeliveryBoy";
	public static String SUPPLIER_TYPE = "Supplier";
	
	public static String ALREADY_EXIST="Already  exist";
	public static String SAVE_SUCCESS="SuccessFully  Saved";
	public static String UPDATE_SUCCESS="SuccessFully  Updated";
	public static String NOT_FOUND="Not Found";
	public static String NO_CONTENT="Data Not Found";
	public static String LOGIN_FAILED="Invalid UserId or Password";
	public static String NONE="None";
	
	public static String MEETING_COMPLETE="complete";
	public static String MEETING_CANCEL="cancel";
	public static String MEETING_PENDING="pending";
	public static String MEETING_RESCHEDULE="reschedule";
	
	public static String ORDER_STATUS_BOOKED="Booked";
	//public static String ORDER_STATUS_DELIVERED_BOOKED="Delivered Booked";
	public static String ORDER_STATUS_PACKED="Packed";
	//public static String ORDER_STATUS_DELIVERED_PACKED="Delivered Packed";
	public static String ORDER_STATUS_ISSUED="Issued";
	//public static String ORDER_STATUS_DELIVERED_ISSUED="Delivered Issued";
	public static String ORDER_STATUS_DELIVERED="Delivered";
	public static String ORDER_STATUS_DELIVERED_PENDING="Delivered Pending";
	public static String ORDER_STATUS_CANCELED="Cancelled";
	
	public static String CASH_PAY_STATUS="Cash";
	public static String CHEQUE_PAY_STATUS="Cheque";
	public static String OTHER_PAY_STATUS="Other";
	
	public static String FULL_PAY_STATUS="Full Payment";
	public static String PARTIAL_PAY_STATUS="Partial Payment";	
	
	public static String PAY_STATUS_CURRENT_DATE_PAYMENT="Current";
	public static String PAY_STATUS_PENDING_DATE_PAYMENT="Pending";
	public static String PAY_STATUS_FUTURE_DATE_PAYMENT="Future";
	
	public static String PAY_STATUS_PAID_DATE_PAYMENT="Paid";
	
/*	public static String ISSUE_STATUS_ISSUED="Issued";
	public static String ISSUE_STATUS_REISSUED="ReIssued";*/
	
	public static String ORDER_PRODUCT_DETAIL_TYPE_FREE="Free";
	public static String ORDER_PRODUCT_DETAIL_TYPE_NON_FREE="NonFree";
	
	public static String RETURN_ORDER_PENDING="Pending";
	public static String RETURN_ORDER_COMPLETE="Completed";
	//public static String RETURN_ORDER_CANCEL="Cancel";
	
	public static String INTRA_STATUS="Intra";
	public static String INTER_STATUS="Inter";
	
	public static String MAHARASHTRA_STATE_NAME="Maharashtra";
	
	public static String EDIT_MODE = "Edit";
    public static String NON_EDIT_MODE = "Non Edit";
    
    //Employee Chat status 
    public static String CHAT_ONLINE = "Online";
    public static String CHAT_OFFLINE = "Offline";
    public static String CHAT_TYPING = "Typing";
    
    public static String CHAT_TYPE_TEXT = "Text";
    public static String CHAT_TYPE_IMAGE = "Image";
    public static String CHAT_TYPE_PDF = "Pdf";
    
    //Banking Controller
	public static String PAY_MODE_CASH="CASH";
	
	public static String PAY_MODE_CHEQUE="CHEQUE";
	
	public static String PAY_MODE_NEFT="NEFT";
	
	public static String PAY_MODE_IMPS="IMPS";
	
	public static String PAY_MODE_UPI="UPI";
	
	public static String TRANSACTION_PROCESSING="PROCESSING";
	
	public static String TRANSACTION_BOUNCED="BOUNCED";
	
	public static String TRANSACTION_CLEARED="CLEARED";
	
	public static String TRANSACTION_ORDERING_ASC = "ASCENDING";
	
	public static String TRANSACTION_ORDERING_DESC = "DESCENDING";
	
	public static String TRANSACTION_TYPE_ALL="ALL";
	
	public static String TRANSACTION_TYPE_CHEQUE_ISSUED="CHEQUE_ISSUED";
	
	public static String TRANSACTION_TYPE_CHEQUE_DEPOSITED="CHEQUE_DEPOSITED";
	
	public static String TRANSACTION_TYPE_CHEQUE_BOUNCED="CHEQUE_BOUNCED";
	
	public static String TARGET_STATUS_PENDING="Pending";
	public static String TARGET_STATUS_PARTIAL="Partial";
	public static String TARGET_STATUS_COMPLETE="Complete";
	
	public static String TARGET_PERIOD_DAILY="Daily";
	public static String TARGET_PERIOD_MONTHLY="Monthly";
	public static String TARGET_PERIOD_WEEKLY="Weekly";
	public static String TARGET_PERIOD_YEARLY="Yearly";
	
	public static String MAP_API_KEY="AIzaSyCvQXM4uYt8kfhXKCoIfGxYtK-LQTDeKTE";//"AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0";
	
	//issuer auth key for encode and decode jwt token
	public static String ISSUER="auth0";
	
	public static String DISCOUNT_TYPE_PERCENTAGE="Percentage";
	public static String DISCOUNT_TYPE_AMOUNT="Amount";
	
	public static String CHEQUE_PRINT_MANUAL="Manual";
	public static String CHEQUE_PRINT_FROM_CHEQUE_BOOK="ChequeBook";
	
	public static String CHEQUE_PRINT_PROCEED_STATUS="Proceed";
	public static String CHEQUE_PRINT_CANCEL_STATUS="Cancelled";
}
