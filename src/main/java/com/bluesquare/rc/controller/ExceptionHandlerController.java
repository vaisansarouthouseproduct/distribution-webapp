/**
 * 
 */
package com.bluesquare.rc.controller;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.utils.EmailSender;

/**
 * <pre>
 * @author Sachin Pawar 18-05-2018 Code Documentation
 * catch panel controller exception and show error page
 * </pre>
 */
@ControllerAdvice("com.bluesquare.rc.controller")
public class ExceptionHandlerController {

	
	@ExceptionHandler(value=Exception.class)
	public ModelAndView exceptionHandler(Model model,Exception e,HttpServletRequest request) {		
		
		
		model.addAttribute("url", request.getRequestURL());
		model.addAttribute("error", e.toString());
		System.err.println("Jump To Error : "+e.getStackTrace()[0]);
		//model.addAttribute("errorCode", request.getAttribute("javax.servlet.error.status_code"));
		System.err.println("Error Controller : "+e.toString());
		return new ModelAndView("errorPage"); 
	}
}
