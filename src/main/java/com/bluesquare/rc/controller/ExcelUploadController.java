package com.bluesquare.rc.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.BusinessTypeService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.utils.ExcelReadInString;

/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.excel
 * 2.upload-product
 * 2.upload-business
 * 3.downloadProductExcel
 * 4.downloadBusinessExcel
 * </pre>
 */
@Controller
public class ExcelUploadController {
	@Autowired
	ProductService productService;

	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	BusinessTypeService businessTypeService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	

	
	
	// Key For Product Insert Start Here
	public static String PRODUCT_NAME_KEY = "productName";
	public static String PRODUCT_CODE_KEY = "productCode";
	public static String BRAND_ID_KEY = "brandId";
	public static String MRP_KEY = "mrp";
	public static String CATEGORY_ID_KEY = "categoryId";
	public static String THRESHOLD_KEY = "threshold";
	
	// Key For business Insert Start Here
	public static String BUSINESS_TYPE_ID = "businessTypeId";
	public static String EMAIL_ID="emailId";
	public static String MOBILE_NUMBER="mobileNumber";
	public static String TELEPHONE_NUMBER="telephoneNumber";
	public static String AREA_ID="areaId";
	public static String ADDRESS="address";
	public static String OWNER_NAME="ownerName";
	public static String SHOP_NAME="shopName";
	public static String GSTIN_NUMBER="gstInNumber";
	public static String CREDIT_LIMIT="creditLimit";
	public static String BRANCH_ID="branchId";

	@Autowired
	HttpSession session;

	@Transactional
	@RequestMapping("excel")
	public String basePath() {
		return "productBulkUpload";
	}

	/*
	 * Fetch Data From Product Excel
	 */
	@Transactional
	@RequestMapping("/upload-product")
	public ResponseEntity<BaseDomain> productExcel(@RequestParam("productFile") MultipartFile file) {
		//ModelAndView mv = new ModelAndView("index");
		BaseDomain baseDomain = new BaseDomain();
		baseDomain.setSuccess(true);
		baseDomain.setMsg("Products Uploaded Successfully");
		try {
			List<Product> productList = new ArrayList<>();
			Map<String, Integer> excelIndexMap = new HashMap<>();
			excelIndexMap.put(PRODUCT_NAME_KEY, 0);
			excelIndexMap.put(PRODUCT_CODE_KEY, 1);
			excelIndexMap.put(BRAND_ID_KEY, 2);
			excelIndexMap.put(CATEGORY_ID_KEY, 3);
			excelIndexMap.put(MRP_KEY, 4);
			excelIndexMap.put(THRESHOLD_KEY, 5);

			// Creates a workbook object from the uploaded excelfile
			HSSFWorkbook  workbook = new HSSFWorkbook(file.getInputStream());
			
			// Creates a worksheet object representing the first sheet
			HSSFSheet worksheet = workbook.getSheetAt(0);
			//Validate if excel is empty
			HSSFRow row= worksheet.getRow(0);
			if(row==null){
				String msg = "Empty Excel File";
				baseDomain.setSuccess(false);
				baseDomain.setMsg(msg);
			}else{
				//validate header fields
				Integer productNameIndex = excelIndexMap.get(PRODUCT_NAME_KEY);
				Integer productCodeIndex = excelIndexMap.get(PRODUCT_CODE_KEY);
				Integer brandIdIndex = excelIndexMap.get(BRAND_ID_KEY);
				Integer categoryIdIndex = excelIndexMap.get(CATEGORY_ID_KEY);
				Integer mrpIndex = excelIndexMap.get(MRP_KEY);
				Integer thresholdIndex = excelIndexMap.get(THRESHOLD_KEY);
				
				String mrpHeader = ExcelReadInString.getCellValueAsString(row.getCell(mrpIndex));
				String productNameHeader = ExcelReadInString.getCellValueAsString(row.getCell(productNameIndex));
				String productCodeHeader = ExcelReadInString.getCellValueAsString(row.getCell(productCodeIndex));
				String brandIdHeader = ExcelReadInString.getCellValueAsString(row.getCell(brandIdIndex));
				String cateogryIdHeader = ExcelReadInString.getCellValueAsString(row.getCell(categoryIdIndex));
				String thresholdHeader = ExcelReadInString.getCellValueAsString(row.getCell(thresholdIndex));
				
				if(!mrpHeader.equals(MRP_KEY) || !productNameHeader.equals(PRODUCT_NAME_KEY) 
						|| !productCodeHeader.equals(PRODUCT_CODE_KEY) || !brandIdHeader.equals(BRAND_ID_KEY)
						|| !cateogryIdHeader.equals(CATEGORY_ID_KEY) || !thresholdHeader.equals(THRESHOLD_KEY)){
					String msg = "Incorrect headers. Please refer sample file.";
					baseDomain.setSuccess(false);
					baseDomain.setMsg(msg);
				}else{
					// Reads the data in excel file until last row is encountered
					String msg = processProducts(worksheet, excelIndexMap);
					if (!msg.equals("")) {
						//mv.addObject("msg", msg);
						baseDomain.setSuccess(false);
						baseDomain.setMsg(msg);
					}
				}
			}
			
			// productDAO.uploadInProduct(productList);
		} catch (Exception e) {
			String msg = "Something went wrong.";
			
			baseDomain.setSuccess(false);
			baseDomain.setMsg(msg);
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	public String processProducts(HSSFSheet worksheet, Map<String, Integer> excelIndexMap) {
		try {

			List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
			List<Brand> brandList=brandService.fetchBrandListForWebApp();
			
			String msg = "";
			Integer productNameIndex = excelIndexMap.get(PRODUCT_NAME_KEY);
			Integer productCodeIndex = excelIndexMap.get(PRODUCT_CODE_KEY);
			Integer brandIdIndex = excelIndexMap.get(BRAND_ID_KEY);
			Integer categoryIdIndex = excelIndexMap.get(CATEGORY_ID_KEY);
			Integer mrpIndex = excelIndexMap.get(MRP_KEY);
			Integer thresholdIndex = excelIndexMap.get(THRESHOLD_KEY);
			
			int i = 1;
			while (i <= worksheet.getLastRowNum()) {
				try{
					// Creates an object for the UserInfo Model
					Product product = new Product();
					// Creates an object representing a single row in excel
					HSSFRow row = worksheet.getRow(i++);
					// ExcelReadInString.getCellValueAsString(row.getCell(hsnCodeIndex));
					
					String productName = ExcelReadInString.getCellValueAsString(row.getCell(productNameIndex));
					String productCode = ExcelReadInString.getCellValueAsString(row.getCell(productCodeIndex));
					String brandIdStr = ExcelReadInString.getCellValueAsString(row.getCell(brandIdIndex));
					String categoryIdStr = ExcelReadInString.getCellValueAsString(row.getCell(categoryIdIndex));
					String mrpStr = ExcelReadInString.getCellValueAsString(row.getCell(mrpIndex));
					String thresholdStr = ExcelReadInString.getCellValueAsString(row.getCell(thresholdIndex));

					//validation start
					if(productName==null){
						msg = "Product Name is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(productName.trim().length()==0){
						msg = "Product Name can not be blank at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(productCode!=null){
						if(productCode.trim().length()==0){
							productCode=null;
						}
					}
					if(brandIdStr==null){
						msg = "Brand Id is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(brandIdStr)==false){
						msg = "Brand Id must be in numeric at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(categoryIdStr==null){
						msg = "Category Id is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(categoryIdStr)==false){
						msg = "Category Id must be in numeric at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(mrpStr==null){
						msg = "MRP is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(mrpStr)==false){
						msg = "MRP must be in numeric at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(thresholdStr==null){
						msg = "Threshold Qunatity is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(thresholdStr)==false){
						msg = "Threshold Qunatity must be in numeric at row : " + i;
						throw new ExcelValidationException(msg);
					}
					
					//valid category id or not check
					boolean isValidId=false;
					if(categoryList!=null){
						for(Categories category: categoryList){
							if(category.getCategoryId()==Long.parseLong(categoryIdStr)){
								isValidId=true;
							}
						}
					}
					if(isValidId==false){
						msg = "Category Id is not Valid at row : " + i;
						throw new ExcelValidationException(msg);
					}
					
					//valid brand id or not check
					isValidId=false;
					if(brandList!=null){
						for(Brand brand: brandList){
							if(brand.getBrandId()==Long.parseLong(brandIdStr)){
								isValidId=true;
							}
						}
					}
					if(isValidId==false){
						msg = "Brand Id is not Valid at row : " + i;
						throw new ExcelValidationException(msg);
					}
					
					//duplication check
					if (productService.findProductNameExist(productName,0)!=null) {
						msg = "Product Name Already Exist at row : " + i;
						// mv.addObject("msg", msg);
						throw new ExcelValidationException(msg);
					}
					//validation end
					
					// Tax Calculation For Product
					long brandId =Long.parseLong(brandIdStr);
					long categoryId =Long.parseLong(categoryIdStr);
					double mrp =Double.parseDouble(mrpStr);
					long thresold= Long.parseLong(thresholdStr);
					
					Branch branch=branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds());
					Categories categories=categoriesService.fetchCategoriesForWebApp(categoryId);
					
					CalculateProperTaxModel calculateProperTaxModel = productService.calculateProperAmountModel(mrp,categories.getIgst());

					product=new Product();
					product.setProductName(productName);
					product.setProductCode(productCode);
					Brand brand=brandService.fetchBrandForWebApp(brandId);
					product.setBrand(brand);
					product.setCategories(categories);					
					product.setRate(calculateProperTaxModel.getUnitprice());

					product.setThreshold(thresold);
					product.setProductAddedDatetime(new Date());
					product.setCurrentQuantity(0);
					
					product.setProductName((Character.toString(product.getProductName().charAt(0)).toUpperCase() + (product.getProductName().substring(1)).toLowerCase()));
					product.setBranch(branch);
					productService.saveProduct(product);
				}catch(HibernateException | IllegalStateException | NumberFormatException ex){
					msg = "Invalid record at row : " + i;
					// mv.addObject("msg", msg);
					throw new ExcelValidationException(msg);
				}
				
			}
			return msg;
		} catch (ExcelValidationException ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String msg = ex.getMessage();
			return msg;
			// mv.addObject("msg", msg);

		}

	}

	/*
	 * Fetch Data From Product Excel
	 */
	@Transactional
	@RequestMapping("/upload-business")
	public ResponseEntity<BaseDomain> businessExcel(@RequestParam("businessFile") MultipartFile file) {
		//ModelAndView mv = new ModelAndView("index");
		BaseDomain baseDomain = new BaseDomain();
		baseDomain.setSuccess(true);
		baseDomain.setMsg("Products Uploaded Successfully");
		try {
			
			Map<String, Integer> excelIndexMap = new HashMap<>();
			excelIndexMap.put(BUSINESS_TYPE_ID, 0);
			excelIndexMap.put(EMAIL_ID, 1);
			excelIndexMap.put(MOBILE_NUMBER, 2);
			excelIndexMap.put(TELEPHONE_NUMBER, 3);
			excelIndexMap.put(AREA_ID, 4);
			excelIndexMap.put(ADDRESS, 5);
			excelIndexMap.put(OWNER_NAME, 6);
			excelIndexMap.put(SHOP_NAME, 7);
			excelIndexMap.put(GSTIN_NUMBER, 8);
			excelIndexMap.put(CREDIT_LIMIT, 9);
			excelIndexMap.put(BRANCH_ID, 10);

			// Creates a workbook object from the uploaded excelfile
			HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
			
			// Creates a worksheet object representing the first sheet
			HSSFSheet worksheet = workbook.getSheetAt(0);
			//Validate if excel is empty
			HSSFRow row= worksheet.getRow(0);
			if(row==null){
				String msg = "Empty Excel File";
				baseDomain.setSuccess(false);
				baseDomain.setMsg(msg);
			}else{
				//validate header fields
				Integer businessTypeIndex = excelIndexMap.get(BUSINESS_TYPE_ID);
				Integer emailIdIndex = excelIndexMap.get(EMAIL_ID);
				Integer mobileNumberIndex = excelIndexMap.get(MOBILE_NUMBER);
				Integer telephoneNumberIndex = excelIndexMap.get(TELEPHONE_NUMBER);
				Integer areaIdIndex = excelIndexMap.get(AREA_ID);
				Integer addressIndex = excelIndexMap.get(ADDRESS);
				Integer ownerNameIndex = excelIndexMap.get(OWNER_NAME);
				Integer shopNameIndex = excelIndexMap.get(SHOP_NAME);
				Integer gstInIndex = excelIndexMap.get(GSTIN_NUMBER);
				Integer creditLimitIndex = excelIndexMap.get(CREDIT_LIMIT);
				Integer branchIdIndex = excelIndexMap.get(BRANCH_ID);
				
				String businessTypeHeader = ExcelReadInString.getCellValueAsString(row.getCell(businessTypeIndex));
				String emailIdHeader = ExcelReadInString.getCellValueAsString(row.getCell(emailIdIndex));
				String mobileNumberHeader = ExcelReadInString.getCellValueAsString(row.getCell(mobileNumberIndex));
				String telephoneNumberHeader = ExcelReadInString.getCellValueAsString(row.getCell(telephoneNumberIndex));
				String areaIdHeader = ExcelReadInString.getCellValueAsString(row.getCell(areaIdIndex));
				String addressHeader = ExcelReadInString.getCellValueAsString(row.getCell(addressIndex));
				String ownerNameHeader = ExcelReadInString.getCellValueAsString(row.getCell(ownerNameIndex));
				String shopNameHeader = ExcelReadInString.getCellValueAsString(row.getCell(shopNameIndex));
				String gstInHeader = ExcelReadInString.getCellValueAsString(row.getCell(gstInIndex));
				String creditLimitHeader = ExcelReadInString.getCellValueAsString(row.getCell(creditLimitIndex));
				String branchIdHeader = ExcelReadInString.getCellValueAsString(row.getCell(branchIdIndex));
				
				if(!businessTypeHeader.equals(BUSINESS_TYPE_ID)
					|| !emailIdHeader.equals(EMAIL_ID)
					|| !mobileNumberHeader.equals(MOBILE_NUMBER)
					|| !telephoneNumberHeader.equals(TELEPHONE_NUMBER)
					|| !areaIdHeader.equals(AREA_ID)
					|| !addressHeader.equals(ADDRESS)
					|| !ownerNameHeader.equals(OWNER_NAME)
					|| !shopNameHeader.equals(SHOP_NAME)
					|| !gstInHeader.equals(GSTIN_NUMBER)
					|| !creditLimitHeader.equals(CREDIT_LIMIT)
					|| !branchIdHeader.equals(BRANCH_ID)){
					String msg = "Incorrect headers. Please refer sample file.";
					baseDomain.setSuccess(false);
					baseDomain.setMsg(msg);
				}else{
					// Reads the data in excel file until last row is encountered
					String msg = processBusiness(worksheet, excelIndexMap);
					if (!msg.equals("")) {
						//mv.addObject("msg", msg);
						baseDomain.setSuccess(false);
						baseDomain.setMsg(msg);
					}
				}
			}
			
			// productDAO.uploadInProduct(productList);
		} catch (Exception e) {
			String msg = "Something went wrong.";
			
			baseDomain.setSuccess(false);
			baseDomain.setMsg(msg);
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	public String processBusiness(HSSFSheet worksheet, Map<String, Integer> excelIndexMap) {
		try {
			List<BusinessType> businessTypeList=businessTypeService.fetchBusinessTypeListForWebApp();
			if(businessTypeList==null){
				businessTypeList=new ArrayList<>();
			}
			List<Area> areaList=areaService.fetchAllAreaForWebApp();
			if(areaList==null){
				areaList=new ArrayList<>();
			}
			List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
			if(branchList==null){
				branchList=new ArrayList<>();
			}
			String msg = "";
			Integer businessTypeIdIndex = excelIndexMap.get(BUSINESS_TYPE_ID);
			Integer emailIdIndex = excelIndexMap.get(EMAIL_ID);
			Integer mobileNumberIndex = excelIndexMap.get(MOBILE_NUMBER);
			Integer telephoneNumberIndex = excelIndexMap.get(TELEPHONE_NUMBER);
			Integer areaIdIndex = excelIndexMap.get(AREA_ID);
			Integer addressIndex = excelIndexMap.get(ADDRESS);
			Integer ownerNameIndex = excelIndexMap.get(OWNER_NAME);
			Integer shopNameIndex = excelIndexMap.get(SHOP_NAME);
			Integer gstInIndex = excelIndexMap.get(GSTIN_NUMBER);
			Integer creditLimitIndex = excelIndexMap.get(CREDIT_LIMIT);
			Integer branchIdIndex = excelIndexMap.get(BRANCH_ID);
			
			int i = 1;
			while (i <= worksheet.getLastRowNum()) {
				try{
					// Creates an object representing a single row in excel
					HSSFRow row = worksheet.getRow(i++);
					// ExcelReadInString.getCellValueAsString(row.getCell(hsnCodeIndex));
					// check duplicate product and barcode
					String businessTypeId = ExcelReadInString.getCellValueAsString(row.getCell(businessTypeIdIndex));
					String emailId = ExcelReadInString.getCellValueAsString(row.getCell(emailIdIndex));
					String mobileNumber = ExcelReadInString.getCellValueAsString(row.getCell(mobileNumberIndex));
					String telephoneNumber = ExcelReadInString.getCellValueAsString(row.getCell(telephoneNumberIndex));
					String areaId = ExcelReadInString.getCellValueAsString(row.getCell(areaIdIndex));
					String address = ExcelReadInString.getCellValueAsString(row.getCell(addressIndex));
					String ownerName = ExcelReadInString.getCellValueAsString(row.getCell(ownerNameIndex));
					String shopName = ExcelReadInString.getCellValueAsString(row.getCell(shopNameIndex));
					String gstInNumber = ExcelReadInString.getCellValueAsString(row.getCell(gstInIndex));
					String creditLimit = ExcelReadInString.getCellValueAsString(row.getCell(creditLimitIndex));
					String branchId = ExcelReadInString.getCellValueAsString(row.getCell(branchIdIndex));
					
					//required validation start
					if(businessTypeId==null){
						msg = "Business Type Id is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(businessTypeId.trim())==false){
						msg = "Business Type Id is must be numeric at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(emailId!=null){
						if(isValidEmail(emailId.trim())==false){
							msg = "Email Id is not valid at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(mobileNumber==null){
						msg = "Mobile Number is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(mobileNumber.trim())==false){
						msg = "Mobile Number is must be numberic at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(telephoneNumber!=null){
						if(isNumeric(telephoneNumber.trim())==false){
							msg = "Telephone Number is must be numberic at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(areaId==null){
						msg = "Area Id is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(areaId.trim())==false){
						msg = "Area Id is must be numberic at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(address==null){
						msg = "Address is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(address.trim().length()==0){
						msg = "Address can not be blank at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(ownerName==null){
						msg = "Owner Name is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(ownerName.trim().length()==0){
						msg = "Owner Name can not be blank at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(shopName==null){
						msg = "Shop Name is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(shopName.trim().length()==0){
						msg = "Shop Name can not be blank at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(gstInNumber!=null){
						if(gstInNumber.trim().length()!=15){
							msg = "Gst In Number is must be 15 digit at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(creditLimit!=null){
						if(creditLimit.trim().length()==0){
							creditLimit=null;
						}else if(isNumeric(creditLimit.trim())==false){
							msg = "Credit Limit is must be in numeric at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					
					//valid brand id or not check
					boolean isValidId=false;
					if(businessTypeList!=null){
						for(BusinessType businessType: businessTypeList){
							if(businessType.getBusinessTypeId()==Long.parseLong(businessTypeId)){
								isValidId=true;
							}
						}
					}
					if(isValidId==false){
						msg = "Business Type Id is not Valid at row : " + i;
						throw new ExcelValidationException(msg);
					}
					//valid area Id or not check 
					isValidId=false;
					if(areaList!=null){
						for(Area area: areaList){
							if(area.getAreaId()==Long.parseLong(areaId)){
								isValidId=true;
							}
						}
					}
					if(isValidId==false){
						msg = "Area Id is not Valid at row : " + i;
						throw new ExcelValidationException(msg);
					}
					
					String response=businessNameService.checkBusinessDuplication(mobileNumber, "mobileNumber", "0");
					if(response.equals("Failed")){
						msg = "Mobile Number Already Exist at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(telephoneNumber!=null){
						response=businessNameService.checkBusinessDuplication(telephoneNumber.trim(), "telephoneNumber", "0");
						if(response.equals("Failed")){
							msg = "Telephone Number Already Exist at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(gstInNumber!=null){
						response=businessNameService.checkBusinessDuplication(gstInNumber.trim(), "gstNo", "0");	
						if(response.equals("Failed")){
							msg = "GST Number Already Exist at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(emailId!=null){
						response=businessNameService.checkBusinessDuplication(emailId.trim(), "emailId", "0");
						if(response.equals("Failed")){
							msg = "Email Id Already Exist at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					if(branchId==null){
						msg = "Branch Id is mandatory at row : " + i;
						throw new ExcelValidationException(msg);
					}
					if(isNumeric(branchId.trim())==false){
						//both
					}else{
						//valid branch Id or not check 
						isValidId=false;
						if(branchList!=null){
							for(BranchModel branch: branchList){
								if(branch.getBranchId()==Long.parseLong(branchId)){
									isValidId=true;
								}
							}
						}
						if(isValidId==false){
							msg = "Branch Id is not Valid at row : " + i;
							throw new ExcelValidationException(msg);
						}
					}
					//validation end
					
					if(branchId.equals("both")){
						for(BranchModel branch : branchList){
							BusinessType businessType=new BusinessType();
							businessType.setBusinessTypeId(Long.parseLong(businessTypeId));
							
							Contact contact=new Contact();
							contact.setEmailId(emailId);
							contact.setMobileNumber(mobileNumber);
							contact.setTelephoneNumber(telephoneNumber);
							
							Area area=new Area();
							area.setAreaId(Long.parseLong(areaId));
							
							BusinessName businessName=new BusinessName();
							businessName.setBusinessType(businessType);
							businessName.setArea(area);
							businessName.setAddress(address);
							businessName.setContact(contact);
							businessName.setOwnerName(ownerName);
							businessName.setShopName(shopName);
							businessName.setGstinNumber(gstInNumber);
							businessName.setCreditLimit(Double.parseDouble(creditLimit));
							businessName.setBusinessAddedDatetime(new Date());
							businessName.setDisableDatetime(new Date());
							businessName.setStatus(false);
							businessName.setBranch(branchService.fetchBranchByBranchId(branch.getBranchId()));
							businessNameService.saveForWebApp(businessName);
						}
					}else{
						Branch branch=branchService.fetchBranchByBranchId(Long.parseLong(branchId));
						BusinessType businessType=new BusinessType();
						businessType.setBusinessTypeId(Long.parseLong(businessTypeId));
						
						Contact contact=new Contact();
						contact.setEmailId(emailId);
						contact.setMobileNumber(mobileNumber);
						contact.setTelephoneNumber(telephoneNumber);
						
						Area area=new Area();
						area.setAreaId(Long.parseLong(areaId));
						
						BusinessName businessName=new BusinessName();
						businessName.setBusinessType(businessType);
						businessName.setArea(area);
						businessName.setAddress(address);
						businessName.setContact(contact);
						businessName.setOwnerName(ownerName);
						businessName.setShopName(shopName);
						businessName.setGstinNumber(gstInNumber);
						businessName.setCreditLimit(Double.parseDouble(creditLimit));
						businessName.setBusinessAddedDatetime(new Date());
						businessName.setDisableDatetime(new Date());
						businessName.setStatus(false);
						businessName.setBranch(branch);
						businessNameService.saveForWebApp(businessName);
					}
					
				}catch(HibernateException | IllegalStateException | NumberFormatException ex){
					msg = "Invalid record at row : " + i;
					// mv.addObject("msg", msg);
					throw new ExcelValidationException(msg);
				}
				
			}
			return msg;
		} catch (ExcelValidationException ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String msg = ex.getMessage();
			return msg;
			// mv.addObject("msg", msg);

		}

	}
	//numeric validation
	public boolean isNumeric(String s) {  
	    return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
	} 
	//email validation
	public static boolean isValidEmail(String email)
	{
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
							"[a-zA-Z0-9_+&*-]+)*@" +
							"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
							"A-Z]{2,7}$";
							
		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}
	
/*	@Transactional 	@RequestMapping("/downloadProductExcel")
	public void billPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
				
		  try {			    
				String filePath="/resources/pdfFiles/file.xls";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				 excel file creating start 
				WritableWorkbook workbook=Workbook.createWorkbook(new File(fullPath));
				WritableSheet sheet1=workbook.createSheet("Product List", 0);
				WritableSheet sheet2=workbook.createSheet("Brand List", 1);
				WritableSheet sheet3=workbook.createSheet("Category List", 2);
				
				//Sheet 1
				Label productName=new Label(0, 0, "productName");
				sheet1.addCell(productName); 
				
				Label productCode=new Label(1,0, "productCode");
				sheet1.addCell(productCode);
				
				Label brandId=new Label(2, 0, "brandId");
				sheet1.addCell(brandId);
				
				Label categoryId=new Label(3, 0, "categoryId");
				sheet1.addCell(categoryId);
				
				Label mrp=new Label(4, 0, "mrp");
				sheet1.addCell(mrp);
				
				Label threshold=new Label(5, 0, "threshold");
				sheet1.addCell(threshold);
				
				
				//Sheet 2
				Label BrandId=new Label(0, 0, "Brand Id");
				sheet2.addCell(BrandId);
				
				Label BrandName=new Label(1, 0, "Brand Name");
				sheet2.addCell(BrandName);
				
				List<Brand> brandList=brandService.fetchBrandListForWebApp();
				int i=1;
				for(Brand brand : brandList){
					Label BrandIdDb=new Label(0, i, ""+brand.getBrandId());
					sheet2.addCell(BrandIdDb);
					
					Label BrandNameDb=new Label(1, i, brand.getName());
					sheet2.addCell(BrandNameDb);
					
					i++;
				}
				
				//Sheet 3
				Label CategoryId=new Label(0, 0, "Category Id");
				sheet3.addCell(CategoryId);
				
				Label CategoryName=new Label(1, 0, "Category Name");
				sheet3.addCell(CategoryName);
				
				List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
				i=1;
				for(Categories categories : categoryList){
					Label CategoryIdDb=new Label(0, i, ""+categories.getCategoryId());
					sheet3.addCell(CategoryIdDb);
					
					Label CategoryNameDb=new Label(1, i, categories.getCategoryName());
					sheet3.addCell(CategoryNameDb);
					
					i++;
				}
				
				workbook.write();
				workbook.close();
				 excel file creating end 
				
				
				
				File dFile=new File(fullPath);				
				
				
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/vnd.ms-excel");
				  //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet //for .xlsx
				  //application/vnd.ms-excel //for .xls
				  response.setHeader("content-disposition", "attachment; filename=products.xls");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}*/
	@RequestMapping(value="/downloadProductExcel", method=RequestMethod.GET)
    public ModelAndView downloadProductExcel(HttpServletRequest request, HttpServletResponse response) throws SQLException{
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		if(brandList==null){
			brandList=new ArrayList<>();
		}
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		if(categoryList==null){
			categoryList=new ArrayList<>();
		}
		
		Map<String, Object> model_main = new HashMap<String, Object>();
		
		//Product
		Map<String, Object> model3 = new HashMap<String, Object>();
		//Sheet Name
        model3.put("sheetname", "Product List");
        //Headers List
        List<String> headers = new ArrayList<String>();
        headers.add("productName");
        headers.add("productCode");
        headers.add("brandId");
        headers.add("categoryId");
        headers.add("mrp");
        headers.add("threshold");
        model3.put("headers", headers);
        
        List<List<String>> rows = new ArrayList<List<String>>();
        model3.put("results",rows);
        model_main.put("0", model3);
		
		//Brand
		Map<String, Object> model1 = new HashMap<String, Object>();
        //Sheet Name
        model1.put("sheetname", "Brand List");
        //Headers List
        headers = new ArrayList<String>();
        headers.add("Brand Id");
        headers.add("Brand Name");
        model1.put("headers", headers);
        
        rows = new ArrayList<List<String>>();
        for(Brand brand : brandList){
        	List<String> l1 = new ArrayList<String>();
            l1.add(""+brand.getBrandId());
            l1.add(""+brand.getName());
            rows.add(l1);
        }
        model1.put("results",rows);
        model_main.put("1", model1);
        
        //category
        Map<String, Object> model2 = new HashMap<String, Object>();
      //Sheet Name
        model2.put("sheetname", "Category List");
        //Headers List
        headers = new ArrayList<String>();
        headers.add("Category Id");
        headers.add("Category Name");
        model2.put("headers", headers);
        
        rows = new ArrayList<List<String>>();
        for(Categories categories : categoryList){
        	List<String> l1 = new ArrayList<String>();
            l1.add(""+categories.getCategoryId());
            l1.add(""+categories.getCategoryName());
            rows.add(l1);
        }
        model2.put("results",rows);
        model_main.put("2", model2);
        response.setContentType( "application/vnd.ms-excel" );
        response.setHeader( "Content-disposition", "attachment; filename=products.xls" );         
        return new ModelAndView(new MyExcelView(), model_main);
    }

	@RequestMapping(value="/downloadBusinessExcel", method=RequestMethod.GET)
	public ModelAndView downloadBusinessExcel(HttpServletRequest request, HttpServletResponse response) throws SQLException{
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		if(areaList==null){
			areaList=new ArrayList<>();
		}
		List<BusinessType> businessTypes=businessTypeService.fetchBusinessTypeListForWebApp();
		if(businessTypes==null){
			businessTypes=new ArrayList<>();
		}
		List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		if(branchList==null){
			branchList=new ArrayList<>();
		}
		
		Map<String, Object> model_main = new HashMap<String, Object>();
		
		//Product
		Map<String, Object> model3 = new HashMap<String, Object>();
		//Sheet Name
	    model3.put("sheetname", "Business List");
	    //Headers List
	    List<String> headers = new ArrayList<String>();
	    headers.add("businessTypeId");
	    headers.add("emailId");
	    headers.add("mobileNumber");
	    headers.add("telephoneNumber");
	    headers.add("areaId");
	    headers.add("address");
	    headers.add("ownerName");
	    headers.add("shopName");
	    headers.add("gstInNumber");
	    headers.add("creditLimit");
	    headers.add("branchId");
	    model3.put("headers", headers);
	    
	    List<List<String>> rows = new ArrayList<List<String>>();
	    model3.put("results",rows);
	    model_main.put("0", model3);
		
		//Area
		Map<String, Object> model1 = new HashMap<String, Object>();
	    //Sheet Name
	    model1.put("sheetname", "Area List");
	    //Headers List
	    headers = new ArrayList<String>();
	    headers.add("Area Id");
	    headers.add("Area Name");
	    headers.add("Region Name");
	    headers.add("City Name");
	    model1.put("headers", headers);
	    
	    rows = new ArrayList<List<String>>();
	    for(Area area : areaList){
	    	List<String> l1 = new ArrayList<String>();
	        l1.add(""+area.getAreaId());
	        l1.add(""+area.getName());
	        l1.add(""+area.getRegion().getName());
	        l1.add(""+area.getRegion().getCity().getName());
	        rows.add(l1);
	    }
	    model1.put("results",rows);
	    model_main.put("1", model1);
	    
	    //Business Type
	    Map<String, Object> model2 = new HashMap<String, Object>();
	  //Sheet Name
	    model2.put("sheetname", "Business Types List");
	    //Headers List
	    headers = new ArrayList<String>();
	    headers.add("Business Type Id");
	    headers.add("Business Type Name");
	    model2.put("headers", headers);
	    
	    rows = new ArrayList<List<String>>();
	    for(BusinessType businessType : businessTypes){
	    	List<String> l1 = new ArrayList<String>();
	        l1.add(""+businessType.getBusinessTypeId());
	        l1.add(""+businessType.getName());
	        rows.add(l1);
	    }
	    model2.put("results",rows);
	    model_main.put("2", model2);
	    
	  //Business Type
	    Map<String, Object> model4 = new HashMap<String, Object>();
	  //Sheet Name
	    model4.put("sheetname", "Branch List");
	    //Headers List
	    headers = new ArrayList<String>();
	    headers.add("Branch Id");
	    headers.add("Branch Name");
	    model4.put("headers", headers);
	    
	    rows = new ArrayList<List<String>>();
	    for(BranchModel branch : branchList){
	    	List<String> l1 = new ArrayList<String>();
	        l1.add(""+branch.getBranchId());
	        l1.add(""+branch.getName());
	        rows.add(l1);
	    }
	    List<String> l1 = new ArrayList<String>();
        l1.add("both");
        l1.add("All branch");
        rows.add(l1);
	    model4.put("results",rows);
	    model_main.put("3", model4);
	    
	    response.setContentType( "application/vnd.ms-excel" );
	    response.setHeader( "Content-disposition", "attachment; filename=businesses.xls" );         
	    return new ModelAndView(new MyExcelView(), model_main);
	}
}

@SuppressWarnings("deprecation")
class MyExcelView extends AbstractExcelView
{
    @SuppressWarnings("unchecked")
    protected void buildExcelDocument(Map<String, Object> model_main,
            HSSFWorkbook workbook,
            HttpServletRequest request,
            HttpServletResponse response)
    {
        
    	for(int i=0; i<model_main.size(); i++){
    		Map<String, Object> model=(Map<String, Object>)model_main.get(""+i);
    		//VARIABLES REQUIRED IN MODEL
	        String sheetName = (String)model.get("sheetname");
	        List<String> headers = (List<String>)model.get("headers");
	        List<List<String>> results = (List<List<String>>)model.get("results");
	        List<String> numericColumns = new ArrayList<String>();
	        if (model.containsKey("numericcolumns"))
	            numericColumns = (List<String>)model.get("numericcolumns");
	        //BUILD DOC
	        HSSFSheet sheet = workbook.createSheet(sheetName);
	        sheet.setDefaultColumnWidth((short) 12);
	        int currentRow = 0;
	        short currentColumn = 0;
	        //CREATE STYLE FOR HEADER
	        HSSFCellStyle headerStyle = workbook.createCellStyle();
	        HSSFFont headerFont = workbook.createFont();
	        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        headerStyle.setFont(headerFont); 
	        //POPULATE HEADER COLUMNS
	        HSSFRow headerRow = sheet.createRow(currentRow);
	        for(String header:headers){
	            HSSFRichTextString text = new HSSFRichTextString(header);
	            HSSFCell cell = headerRow.createCell(currentColumn); 
	            cell.setCellStyle(headerStyle);
	            cell.setCellValue(text);            
	            currentColumn++;
	        }
	        //POPULATE VALUE ROWS/COLUMNS
	        currentRow++;//exclude header
	        for(List<String> result: results){
	            currentColumn = 0;
	            HSSFRow row = sheet.createRow(currentRow);
	            for(String value : result){//used to count number of columns
	                HSSFCell cell = row.createCell(currentColumn);
	                if (numericColumns.contains(headers.get(currentColumn))){
	                    cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
	                    cell.setCellValue(value);
	                } else {
	                    HSSFRichTextString text = new HSSFRichTextString(value);                
	                    cell.setCellValue(text);                    
	                }
	                currentColumn++;
	            }
	            currentRow++;
	        }
    	}
    }
}
class ExcelValidationException extends Exception {
	private String message;

	public ExcelValidationException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return message;
	}
	
}
