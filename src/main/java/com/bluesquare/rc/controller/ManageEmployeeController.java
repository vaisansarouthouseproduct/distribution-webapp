package com.bluesquare.rc.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeBasicSalaryStatus;
import com.bluesquare.rc.entities.EmployeeBranches;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.EmployeeIncentives;
import com.bluesquare.rc.entities.EmployeeProfilePicture;
import com.bluesquare.rc.entities.EmployeeRoutes;
import com.bluesquare.rc.entities.EmployeeSalary;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.models.EmployeeHolidayModel;
import com.bluesquare.rc.models.EmployeeIncentiveModel;
import com.bluesquare.rc.models.EmployeeLastLocation;
import com.bluesquare.rc.models.EmployeeListResponse;
import com.bluesquare.rc.models.EmployeePaymentModel;
import com.bluesquare.rc.models.EmployeeSalaryStatus;
import com.bluesquare.rc.models.EmployeeViewModel;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.CityModel;
import com.bluesquare.rc.responseEntities.EmployeeDetailsModel;
import com.bluesquare.rc.responseEntities.EmployeeHolidaysModel;
import com.bluesquare.rc.responseEntities.EmployeeIncentivesModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.SalesManReport;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.service.EmployeeAreaListService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.service.RegionService;
import com.bluesquare.rc.service.RouteService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.ImageConvertor;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * API End Points
 * 1.addEmployee
 * 2.checkEmployeeDuplicationForSave
 * 3.checkEmployeeDuplicationForUpdate
 * 4.saveEmployee
 * 5.fetchEmployee
 * 6.updateEmployee
 * 7.fetchEmployeeList
 * 8.fetchEmployeeListForGkView
 * 9.fetchEmployeeListAjax
 * 10.fetchEmployeeDetail
 * 11.fetchEmployeeSalaryByFilter
 * 12.fetchEmployeeHolidayDetail
 * 13.fetchEmployeeHolidayDetailByFilter
 * 14.fetchEmployeeIncentiveDetailByFilter
 * 15.fetchEmployeeIncentivesByEmployeeIncentivesId
 * 16.editIncentivesAjax
 * 17.deleteIncentive
 * 18.fetchEmployeeAreas
 * 19.bookHoildays
 * 20.fetchEmployeeHolidaysByEmployeeHolidaysId
 * 21.updateHoildays
 * 22.deleteHoliday
 * 23.checkHoliday
 * 24.checkUpdatingHoliday
 * 25.giveIncentives
 * 26.givePayment
 * 27.openPaymentModel
 * 28.sendSMS
 * 29.getEmployeeView
 * 30.viewEmployeeSalary
 * 31.paymentEmployee
 * 32.giveEmployeePayment
 * 33.editEmployeePayment
 * 34.updateEmployeePayment
 * 35.deleteEmployeePayment
 * 36.fetchSalesManReport
 * 37.disableEmployee
 * 38.findLocation
 * 39.fetchEmployeeLatLng
 * 40.fetchSingleEmployeeLatLng
 * 41.openForgetPasswordGateKeeper
 * 42.forgetPasswordGatekeeper
 * 43.checkGateKeeperPassword
 * 44.fetchEmployeeDetailsByDepartmentId/{deparmentId}
 * </pre>
 */
@Controller
public class ManageEmployeeController {

	
	@Autowired
	CountryService countryService;
	
	@Autowired
	Country country;
	
	@Autowired
	StateService stateService;
	
	@Autowired
	State state;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	City city;
	
	@Autowired
	RegionService regionService;
	
	@Autowired
	Region region;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	Area area;
	
	@Autowired
	Department department;

	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeAreaListService employeeAreaListService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	EmployeeDetails employeeDetails;
	
	@Autowired
	Employee employee;
	
	@Autowired
	EmployeeAreaList employeeAreaList;
	
	@Autowired
	Contact contact;
	
	@Autowired
	EmployeeHolidays employeeHolidays;
	
	@Autowired
	EmployeeIncentives employeeIncentives;
	
	@Autowired
	EmployeeSalary employeeSalary;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	RouteService routeService;
	
	@Autowired
	PaymentService paymentService;
	
	/**
	 * <pre>
	 * add employee page open with 
	 * city list which assigned to company by admin
	 * department list
	 *  </pre>
	 * @param model
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/addEmployee")
	public ModelAndView addEmployee(Model model,HttpSession session) {

		
		  model.addAttribute("pageName", "Add Employee");
		  
		System.out.println("in addEmployee controller");
		
		//this.country=countryService.fetchIndiaCountry();
		
		List<CityModel> cityModelList=new ArrayList<>();
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		for(CompanyCities companyCity: companyCities){
			cityModelList.add(new CityModel(companyCity.getCity().getCityId(), companyCity.getCity().getName()));
		}
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		//List<Country> coutryList = countryService.fetchCountryListForWebApp();
		
		model.addAttribute("departmentList", DepartmentList);
		model.addAttribute("cityList", cityModelList);
		//model.addAttribute("coutryList", coutryList);
		
		List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		model.addAttribute("branchList", branchList);
		
		List<Route> routeList=routeService.fetchRouteList();
		model.addAttribute("routeList", routeList);
		return new ModelAndView("addEmployee");
	}
	/**
	 * <pre>
	 * check employee adding validation with 
	 * userId,mobile number,email duplication
	 * userId check in company as well as employee section
	 * </pre>
	 * @param model
	 * @param request
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkEmployeeDuplicationForSave")
	public @ResponseBody String checkEmployeeDuplicationForSave(Model model,HttpServletRequest request) {

		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		
		return employeeService.checkEmployeeDuplication(checkText, type, 0);
	}
	/**
	 * <pre>
	 * check employee adding validation with 
	 * userId,mobile number,email duplication
	 * userId check in company as well as employee section
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkEmployeeDuplicationForUpdate")
	public @ResponseBody String checkEmployeeDuplicationForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return employeeService.checkEmployeeDuplication(checkText, type, employeeDetailsId);
	}
	/**
	 * <pre>
	 * save employee
	 * save contacts
	 * save employee details
	 * save employee areas
	 * save employee salary status
	 * </pre>
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/saveEmployee")
	public  ModelAndView saveEmployee(/*Model model,*/HttpServletRequest request,HttpSession session) {

		System.out.println("in saveEmployee controller");
		
		
		String userId=request.getParameter("userId");
		String password=request.getParameter("password");
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		String employeeName=request.getParameter("employeeName");
		String address=request.getParameter("address");
		String emailId=request.getParameter("emailId");
		String mobileNumber=request.getParameter("mobileNumber");
		String areaIdLists=request.getParameter("areaIdLists");
		double basicSalary=Double.parseDouble(request.getParameter("basicSalary"));	
		
		//branch saving
		String branchIdsStr[]=request.getParameterValues("branchIds");		
		long[] branchIds = new long[branchIdsStr.length]; 
		int i=0;
		for (String branch : branchIdsStr) {   
			branchIds[i] = Long.parseLong(branch);   
			i++;
		}		
		List<Branch> branchList=branchService.fetchBranchListByBrachIdList(branchIds);
		List<EmployeeBranches> employeeBranchesList=new ArrayList<>();
		for(Branch branch: branchList){
			EmployeeBranches employeeBranches=new EmployeeBranches();
			employeeBranches.setBranch(branch);
			employeeBranchesList.add(employeeBranches);
		}		
		//route
		List<EmployeeRoutes> employeeRoutesList=new ArrayList<>();
		String routeIdsStr[]=request.getParameterValues("routeIds");		
		if(routeIdsStr!=null){
			long[] routeIds = new long[routeIdsStr.length]; 
			i=0;
			for (String route : routeIdsStr) {   
				routeIds[i] = Long.parseLong(route);   
				i++;
			}		
			List<Route> routeList=routeService.fetchRouteListByRouteIdList(routeIds);
			for(Route route: routeList){
				EmployeeRoutes employeeRoutes=new EmployeeRoutes();
				employeeRoutes.setRoute(route);
				employeeRoutesList.add(employeeRoutes);
			}		
		}
		employee.setDepartment(departmentService.fetchDepartmentForWebApp(departmentId));
		employee.setPassword(password);
		employee.setUserId(userId);
		
		employeeService.saveForWebApp(employee,employeeBranchesList,employeeRoutesList);
		
		EmployeeProfilePicture employeeProfilePicture=new EmployeeProfilePicture();
		employeeProfilePicture.setUserImgBase64(ImageConvertor.employeeDefaultImage());
		employeeProfilePicture.setEmployee(employee);
		employeeDetailsService.saveEmployeeProfilePicture(employeeProfilePicture);
		
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		
		
		employeeDetails.setAddress(address);
		employeeDetails.setBasicSalary(basicSalary);
		employeeDetails.setName(employeeName);
		employeeDetails.setEmployee(employee);
		employeeDetails.setContact(contact);
		employeeDetails.setEmployeeDetailsAddedDatetime(new Date());
		employeeDetails.setEmployeeDetailsUpdatedDatetime(new Date());
		employeeDetails.setStatus(false);
		employeeDetails.setEmployeeDetailsDisableDatetime(new Date());
		employeeDetailsService.saveForWebApp(employeeDetails);
		
		employeeAreaListService.saveEmployeeAreasForWebApp(areaIdLists,employeeDetails.getEmployeeDetailsId());
		
		EmployeeBasicSalaryStatus employeeBasicSalaryNew=new EmployeeBasicSalaryStatus();
		employeeBasicSalaryNew.setEmployeeDetails(employeeDetails);
		employeeBasicSalaryNew.setBasicSalary(basicSalary);
		employeeBasicSalaryNew.setStartDate(new Date());
		employeeBasicSalaryNew.setEndDate(new Date());
		employeeDetailsService.saveEmployeeOldBasicSalary(employeeBasicSalaryNew);
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	/**
	 * <pre>
	 * fetch employee details (employee,employee details,area list)
	 * city list which assigned to company
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return updateEmployee.jsp 
	 */
	@Transactional 	@RequestMapping("/fetchEmployee")
	public  ModelAndView fetchEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Update Employee");
		
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		
		employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
		model.addAttribute("employeeDetails", employeeDetails);
		
		List<EmployeeAreaList> employeeAreaList=employeeAreaListService.fetchEmployeeAreaListByEmployeeDetailsId(Long.parseLong(employeeDetailsId));
		List<AreaModel> areaModelList=new ArrayList<>();
		for(EmployeeAreaList area : employeeAreaList){
			areaModelList.add(new AreaModel(area.getArea().getAreaId(), area.getArea().getName(), area.getArea().getPincode()));
		}
		
		model.addAttribute("employeeAreaList", areaModelList);
		
		String areaidlist="";
		model.addAttribute("count","");
		
		if(employeeAreaList!=null)
		{
			areaidlist=employeeAreaListService.getAreaIdList(employeeAreaList);
			model.addAttribute("count",employeeAreaList.size()+1);
		}
			
		model.addAttribute("areaidlist", areaidlist);
		
		/*this.country=countryService.fetchIndiaCountry();
		List<State> stateList=stateService.fetchStateByCountryIdforwebapp(this.country.getCountryId());*/
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		//List<Country> coutryList = countryService.fetchCountryListForWebApp();
		
		model.addAttribute("departmentList", DepartmentList);
		
		List<CityModel> cityModelList=new ArrayList<>();
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		for(CompanyCities companyCity: companyCities){
			cityModelList.add(new CityModel(companyCity.getCity().getCityId(), companyCity.getCity().getName()));
		}
		model.addAttribute("cityList", cityModelList);
		
		List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		model.addAttribute("branchList", branchList);
		
		List<Branch> branchSet=employeeService.fetchBranchListByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		List<Long> branchIds=new ArrayList<Long>();
		for(Branch branch : branchSet){
			branchIds.add(branch.getBranchId());
		}
		model.addAttribute("branchIds", branchIds);
		
		List<Route> routeList=routeService.fetchRouteList();
		model.addAttribute("routeList", routeList);
		
		List<Route> routeSet=employeeService.fetchRouteListByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		List<Long> routeIds=new ArrayList<Long>();
		for(Route route : routeSet){
			routeIds.add(route.getRouteId());
		}
		model.addAttribute("routeIds", routeIds);
		
		//model.addAttribute("stateList", stateList);
		//model.addAttribute("coutryList", coutryList);
		
		return new ModelAndView("updateEmployee");
	}
	/**
	 * <pre>
	 * update employee
	 * update contacts
	 * update employee details
	 * update employee areas
	 * update employee salary status
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/updateEmployee")
	public  ModelAndView updateEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		
		
		System.out.println("in saveEmployee controller");
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
		
		String userId=request.getParameter("userId");
		String password=request.getParameter("password");
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		long departmentIdOld=employeeDetails.getEmployee().getDepartment().getDepartmentId();
		String employeeName=request.getParameter("employeeName");
		String address=request.getParameter("address");
		String emailId=request.getParameter("emailId");
		String mobileNumber=request.getParameter("mobileNumber");
		String areaIdLists=request.getParameter("areaIdLists");
		double basicSalary=Double.parseDouble(request.getParameter("basicSalary"));
		double basicSalaryOld=employeeDetails.getBasicSalary();
		
		//branch
		String branchIdsStr[]=request.getParameterValues("branchIds");		
		long[] branchIds = new long[branchIdsStr.length]; 
		int i=0;
		for (String branch : branchIdsStr) {   
			branchIds[i] = Long.parseLong(branch);
			i++;
		}		
		List<Branch> branchList=branchService.fetchBranchListByBrachIdList(branchIds);
		List<EmployeeBranches> employeeBranchesList=new ArrayList<>();
		for(Branch branch: branchList){
			EmployeeBranches employeeBranches=new EmployeeBranches();
			employeeBranches.setBranch(branch);
			employeeBranchesList.add(employeeBranches);
		}	
		
		//route
		List<EmployeeRoutes> employeeRoutesList=new ArrayList<>();
		String routeIdsStr[]=request.getParameterValues("routeIds");
		if(routeIdsStr!=null){
			long[] routeIds = new long[routeIdsStr.length]; 
			i=0;
			for (String route : routeIdsStr) {   
				routeIds[i] = Long.parseLong(route);   
				i++;
			}		
			List<Route> routeList=routeService.fetchRouteListByRouteIdList(routeIds);
			for(Route route: routeList){
				EmployeeRoutes employeeRoutes=new EmployeeRoutes();
				employeeRoutes.setRoute(route);
				employeeRoutesList.add(employeeRoutes);
			}		
		}
		employee=employeeDetails.getEmployee();
		employee.setEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		employee.setDepartment(departmentService.fetchDepartmentForWebApp(departmentId));
		employee.setPassword(password);
		employee.setUserId(userId);
		
		employeeService.updateForWebApp(employee,employeeBranchesList,employeeRoutesList);
		
		contact.setContactId(employeeDetails.getContact().getContactId());
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		
		employeeDetails.setAddress(address);
		
		employeeDetails.setBasicSalary(basicSalary);
		
		employeeDetails.setName(employeeName);
		employeeDetails.setEmployee(employee);
		employeeDetails.setContact(contact);
		employeeDetails.setEmployeeDetailsAddedDatetime(employeeDetails.getEmployeeDetailsAddedDatetime());
		employeeDetails.setEmployeeDetailsUpdatedDatetime(new Date());
		employeeDetails.setStatus(employeeDetails.isStatus());
		if(departmentIdOld!=departmentId)
		{
			employeeDetailsService.updateForWebApp(employeeDetails,true);
		}
		else
		{
			employeeDetailsService.updateForWebApp(employeeDetails,false);
		}
		employeeAreaListService.updateEmployeeAreasForWebApp(areaIdLists,employeeDetails.getEmployeeDetailsId());
		
		if(basicSalary!=basicSalaryOld)
		{
			EmployeeBasicSalaryStatus employeeBasicSalary=employeeDetailsService.fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(Long.parseLong(employeeDetailsId));
			employeeBasicSalary.setEndDate(new Date());
			employeeDetailsService.updateEmployeeOldBasicSalary(employeeBasicSalary);
			
			Calendar cal=Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			
			EmployeeBasicSalaryStatus employeeBasicSalaryNew=new EmployeeBasicSalaryStatus();
			employeeBasicSalaryNew.setEmployeeDetails(employeeDetails);
			employeeBasicSalaryNew.setBasicSalary(basicSalary);
			employeeBasicSalaryNew.setStartDate(cal.getTime());
			employeeBasicSalaryNew.setEndDate(cal.getTime());
			employeeDetailsService.saveEmployeeOldBasicSalary(employeeBasicSalaryNew);
		}
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	/**
	 * <pre>
	 * open manage Employee page and data of employee will display from ajax response
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeList")
	public  ModelAndView fetchEmployeeList(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "HRM");
	
		session.setAttribute("lastUrl", "fetchEmployeeList");
		
		//List<EmployeeViewModel> employeeViewModelList=employeeDetailsService.fetchEmployeeDetailsForView();
		//model.addAttribute("employeeViewModelList", employeeViewModelList);
		
		model.addAttribute("saveMsg",session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageEmployee");
	}
	
	/**
	 * <pre>
	 * fetch Employee List according GateKeeper areas for GateKeeper Section
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return GkAreaWiseEmployeeView.jsp
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeListForGkView")
	public  ModelAndView fetchEmployeeListForGkView(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Employee Details");
		
		List<EmployeeViewModel> employeeViewModelList=employeeDetailsService.fetchEmployeeDetailsForGkEmployeeView();
		model.addAttribute("employeeViewModelList", employeeViewModelList);
		return new ModelAndView("GkAreaWiseEmployeeView");
	}
	/**
	 * <pre>
	 * fetch Employee List for Manage Employee Page -> fetch by Ajax
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeListAjax")
	public  @ResponseBody List<EmployeeViewModel> fetchEmployeeListAjax(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "HRM");
			
		List<EmployeeViewModel> employeeViewModelList=employeeDetailsService.fetchEmployeeDetailsForView();

		return employeeViewModelList;
	}
	/**
	 * <pre>
	 * fetch Employee Details by employeeDetailsId
	 * </pre>
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeDetail")
	public @ResponseBody  List<EmployeeSalaryStatus> fetchEmployeeDetail(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		List<EmployeeSalaryStatus> employeeSalaryStatus=employeeDetailsService.fetchEmployeeSalaryStatusForWebApp(employeeDetailsId);
		//model.addAttribute("employeeSalaryStatus", employeeSalaryStatus);
		
		return employeeSalaryStatus;
	}
	
	/**
	 * fetch employee salary details lists by filter,startDate,endDate,employeeDetailsId 
	 * @param session
	 * @param request
	 * @param model
	 * @return List EmployeeSalaryStatus
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeSalaryByFilter")
	public @ResponseBody  List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp( HttpSession session, HttpServletRequest request, Model model)
	{
		
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
	
		List<EmployeeSalaryStatus> employeeSalaryStatuslist = employeeDetailsService.tofilterRangeEmployeeSalaryStatusForWebApp(startDate, endDate,filter, employeeDetailsId);
		 
		 return employeeSalaryStatuslist;
	}
	/**
	 * fetch Employee Holiday Details by employeeDetails Id
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeHolidayDetail")
	public @ResponseBody  EmployeeHolidayModel fetchEmployeeHolidayDetail(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		//String filter=request.getParameter("filter");
		//String startDate=request.getParameter("startDate");
		//String endDate=request.getParameter("endDate");
		EmployeeHolidayModel employeeHolidayModel=employeeDetailsService.fetchEmployeeHolidayModelForWebApp(employeeDetailsId,"CurrentMonth",""	,"");
		//model.addAttribute("employeeSalaryStatus", employeeSalaryStatus);
		
		return employeeHolidayModel;
	}
	
	
	@Transactional 	@RequestMapping("/fetchEmployeeHolidayDetailByFilter")
	public @ResponseBody  EmployeeHolidayModel fetchEmployeeHolidayDetailByFilter(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		EmployeeHolidayModel employeeHolidayModel=employeeDetailsService.fetchEmployeeHolidayModelForWebApp(employeeDetailsId,filter,startDate	,endDate);
		//model.addAttribute("employeeSalaryStatus", employeeSalaryStatus);
		
		return employeeHolidayModel;
	}
	/**
	 * fetch Employee Incentive Details list by employeeDetails Id and filter,startDate,endDate
	 * @param model
	 * @param request
	 * @return EmployeeIncentives
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeIncentiveDetailByFilter")
	public @ResponseBody  EmployeeIncentiveModel fetchEmployeeIncentiveDetailByFilter(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		List<EmployeeIncentives> employeeIncentiveList=employeeDetailsService.fetchEmployeeIncentiveListByFilter(employeeDetailsId, filter, startDate, endDate);
		List<EmployeeIncentivesModel> employeeIncentivesModelList=new ArrayList<>();
		for(EmployeeIncentives employeeIncentives : employeeIncentiveList){
			employeeIncentivesModelList.add(new EmployeeIncentivesModel(
					employeeIncentives.getEmployeeIncentiveId(), 
					employeeIncentives.getIncentiveAmount(), 
					employeeIncentives.getReason(), 
					employeeIncentives.getIncentiveGivenDate()));
		}
		
		EmployeeDetails employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		EmployeeIncentiveModel employeeIncentiveModel=new EmployeeIncentiveModel(employeeDetails.getName(),employeeDetails.isStatus(), employeeIncentivesModelList); 
		
		return employeeIncentiveModel;
	}
	
	/**
	 * fetch Employee Incentive Details by employeeIncentivesId 
	 * @param model
	 * @param request
	 * @return EmployeeIncentives
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeIncentivesByEmployeeIncentivesId")
	public @ResponseBody  EmployeeIncentivesModel fetchEmployeeIncentivesByEmployeeIncentivesId(Model model,HttpServletRequest request) {
		
		EmployeeIncentives employeeIncentives = employeeDetailsService.fetchEmployeeIncentivesByEmployeeIncentivesId(Long.parseLong(request.getParameter("employeeIncentivesId")));
		
		return new EmployeeIncentivesModel(
				employeeIncentives.getEmployeeIncentiveId(), 
				employeeIncentives.getIncentiveAmount(), 
				employeeIncentives.getReason(), 
				employeeIncentives.getIncentiveGivenDate());
	}
	/**
	 * edit incentive (amount,reason)
	 * @param model
	 * @param request
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/editIncentivesAjax")
	public @ResponseBody  String editIncentivesAjax(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		double incentiveAmount=Double.parseDouble(request.getParameter("incentives"));
		long incentiveId=Long.parseLong(request.getParameter("incentiveId"));

		String reason=request.getParameter("reason");
		
		employeeIncentives=employeeDetailsService.fetchIncentives(incentiveId);
		
		employeeIncentives.setEmployeeIncentiveId(incentiveId);
		
		employeeDetails.setEmployeeDetailsId(employeeDetailsId);
		employeeIncentives.setEmployeeDetails(employeeDetails);
		employeeIncentives.setReason(reason);
		employeeIncentives.setIncentiveAmount(incentiveAmount);
		
		String response=employeeDetailsService.updateIncentive(employeeIncentives);
		
		return response;
	}
	/**
	 * <pre>
	 * delete incentives by employeeIncentiveId
	 * </pre>
	 * @param model
	 * @param request
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/deleteIncentive")
	public @ResponseBody  String deleteIncentive(Model model,HttpServletRequest request) {
		
		long incentiveId=Long.parseLong(request.getParameter("employeeIncentivesId"));
		
		employeeIncentives=employeeDetailsService.fetchIncentives(incentiveId);
		
		employeeIncentives.setStatus(true);
		
		String response=employeeDetailsService.updateIncentive(employeeIncentives);
		
		return response;
	}
	/**
	 * <pre>
	 * fetch Employee Areas By employeeDetails and logged company id
	 * </pre>
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeAreas")
	public @ResponseBody  EmployeeAreaDetails fetchEmployeeAreas(Model model,HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		EmployeeDetails  employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(employeeDetailsId);
		EmployeeAreaDetails employeeAreaDetails = employeeDetailsService.fetchEmployeeAreaDetails(employeeDetailsId,employeeDetails.getEmployee().getCompany().getCompanyId());

		
		return employeeAreaDetails;
	}
	
	/**
	 * <pre>
	 * fetch Employee Areas By employeeDetails and logged company id
	 * </pre>
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeDetailsByEmployeeDetailsId")
	public @ResponseBody  EmployeeDetailsModel fetchEmployeeDetailsByEmployeeDetailsId(Model model,HttpServletRequest request) {

		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		EmployeeDetails  employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		return new EmployeeDetailsModel(
				employeeDetails.getEmployeeDetailsId(),
				employeeDetails.getEmployeeDetailsGenId(), 
				employeeDetails.getName(), 
				employeeDetails.getAddress(),
				employeeDetails.getBasicSalary(), 
				employeeDetails.getContact(), 
				employeeDetails.getEmployeeDetailsAddedDatetime(), 
				employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
				employeeDetails.isStatus(),
				employeeDetails.getEmployeeDetailsDisableDatetime());
				
	}
	
	/**
	 * <pre>
	 * save employee holiday (noOfDays,oneDate,fromDate,toDate,paidStatus,reason,employeeDetailsId)
	 * </pre>
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/bookHoildays")
	public @ResponseBody  String bookHoildays(Model model,HttpServletRequest request) {
		
		String noOfDays=request.getParameter("noOfDays");
		String oneDate=request.getParameter("oneDate");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String paidStatus=request.getParameter("paidStatus");
		String reason=request.getParameter("reason");		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		
		
		try {
			if(Integer.parseInt(noOfDays)>1)
			{
				employeeHolidays.setFromDate(dateFormat.parse(fromDate));
				employeeHolidays.setToDate(dateFormat.parse(toDate));
			}
			else 
			{
				employeeHolidays.setFromDate(dateFormat.parse(oneDate));
				employeeHolidays.setToDate(dateFormat.parse(oneDate));
			}
			
			if(paidStatus.equals("paid"))
			{
				employeeHolidays.setPaidHoliday(true);
			}
			else
			{
				employeeHolidays.setPaidHoliday(false);
			}
			employeeHolidays.setGivenHolidayDate(new Date());
			employeeHolidays.setReason(reason);
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);		
			employeeHolidays.setEmployeeDetails(employeeDetails);
			employeeHolidays.setStatus(false);
			employeeDetailsService.bookHolidayForWebApp(employeeHolidays);
		} catch (Exception e) {
			return "Fail";
		}
		
		return "Success";
	}
	/**
	 * <pre>
	 * fetch Employee holiday by employeeHolidayId 
	 * </pre>
	 * @param model
	 * @param request
	 * @return EmployeeHolidays
	 */
	@Transactional 	@RequestMapping("/fetchEmployeeHolidaysByEmployeeHolidaysId")
	public @ResponseBody  EmployeeHolidaysModel fetchEmployeeHolidaysByEmployeeHolidaysId(Model model,HttpServletRequest request) {
		
		EmployeeHolidays employeeHolidays = employeeDetailsService.fetchEmployeeHolidayByEmployeeHolidayId(Long.parseLong(request.getParameter("employeeHolidaysId")));
		employeeDetails=employeeHolidays.getEmployeeDetails();
		return new EmployeeHolidaysModel(
				employeeHolidays.getEmployeeHolidaysId(), 
				employeeHolidays.getReason(), 
				new EmployeeDetailsModel(
						employeeDetails.getEmployeeDetailsId(),
						employeeDetails.getEmployeeDetailsGenId(), 
						employeeDetails.getName(), 
						employeeDetails.getAddress(),
						employeeDetails.getBasicSalary(), 
						employeeDetails.getContact(), 
						employeeDetails.getEmployeeDetailsAddedDatetime(), 
						employeeDetails.getEmployeeDetailsUpdatedDatetime(), 
						employeeDetails.isStatus(),
						employeeDetails.getEmployeeDetailsDisableDatetime()), 
				employeeHolidays.getFromDate(), 
				employeeHolidays.isPaidHoliday(), 
				employeeHolidays.getToDate(), 
				employeeHolidays.getGivenHolidayDate(), 
				employeeHolidays.isStatus());
	}
	
	/**
	 * update holiday details (noOfDays,oneDate,fromDate,toDate,paidStatus,reason,employeeDetailsId,employeeHolidaysId) 
	 * @param model
	 * @param request
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/updateHoildays")
	public @ResponseBody  String updateHoildays(Model model,HttpServletRequest request) {
		
		String noOfDays=request.getParameter("noOfDays");
		String oneDate=request.getParameter("oneDate");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String paidStatus=request.getParameter("paidStatus");
		String reason=request.getParameter("reason");		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		long employeeHolidaysId=Long.parseLong(request.getParameter("employeeHolidaysId"));
		
		try {
			employeeHolidays=employeeDetailsService.fetchEmployeeHolidayByEmployeeHolidayId(employeeHolidaysId);
			if(Integer.parseInt(noOfDays)>1)
			{
				employeeHolidays.setFromDate(dateFormat.parse(fromDate));
				employeeHolidays.setToDate(dateFormat.parse(toDate));
			}
			else 
			{
				employeeHolidays.setFromDate(dateFormat.parse(oneDate));
				employeeHolidays.setToDate(dateFormat.parse(oneDate));
			}
			
			if(paidStatus.equals("paid"))
			{
				employeeHolidays.setPaidHoliday(true);
			}
			else
			{
				employeeHolidays.setPaidHoliday(false);
			}
			
			employeeHolidays.setEmployeeHolidaysId(employeeHolidaysId);
			employeeHolidays.setGivenHolidayDate(new Date());
			employeeHolidays.setReason(reason);
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);		
			employeeHolidays.setEmployeeDetails(employeeDetails);
			employeeDetailsService.updateEmployeeHoliday(employeeHolidays);
		} catch (Exception e) {
			return "Fail";
		}
		
				
		return "Success";
	}
	/**
	 * disable holiday by employeeHolidaysId
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/deleteHoliday")
	public @ResponseBody  String deleteHoilday(Model model,HttpServletRequest request) {
		
		long employeeHolidaysId=Long.parseLong(request.getParameter("employeeHolidaysId"));
		
		try {
			employeeHolidays=employeeDetailsService.fetchEmployeeHolidayByEmployeeHolidayId(employeeHolidaysId);
			
			employeeHolidays.setStatus(true);
			
			employeeDetailsService.updateEmployeeHoliday(employeeHolidays);
		} catch (Exception e) {
			return "Fail";
		}		
		return "Success";
	}
	/**
	 * check holiday given or not given startDate and endDate for given employeeDetailsId for save
	 * @param request
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/checkHoliday")
	public @ResponseBody BaseDomain checkHoliday(HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		BaseDomain baseDomain=new BaseDomain(); 
		String msg = employeeDetailsService.checkHolidayGivenOrNot(startDate, endDate, employeeDetailsId);
		baseDomain.setStatus(msg);
		
		return baseDomain;
	}
	/**
	 * check holiday given or not given startDate and endDate for given employeeDetailsId for update
	 * @param request
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/checkUpdatingHoliday")
	public @ResponseBody BaseDomain checkUpdatingHoliday(HttpServletRequest request) {
		
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		long employeeHolidayId=Long.parseLong(request.getParameter("employeeHolidayId"));
		BaseDomain baseDomain=new BaseDomain(); 
		String msg = employeeDetailsService.checkUpdatingHolidayGivenOrNot(startDate, endDate, employeeDetailsId, employeeHolidayId);
		baseDomain.setStatus(msg);
		
		return baseDomain;
	}
	/**
	 * save incentive details by employeeDetailsId
	 * @param request
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/giveIncentives")
	public @ResponseBody String GiveIncentives(HttpServletRequest request) {
		
		try {
			long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
			double incentives=Double.parseDouble(request.getParameter("incentives"));
			String reason=request.getParameter("reason");
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			
			employeeIncentives.setEmployeeDetails(employeeDetails);
			employeeIncentives.setIncentiveAmount(incentives);
			employeeIncentives.setReason(reason);
			employeeIncentives.setIncentiveGivenDate(new Date());
			employeeIncentives.setStatus(false);
			employeeDetailsService.giveIncentives(employeeIncentives);
		} catch (NumberFormatException e) {
			return "Failed";
		}
		
		
		return "Success";
	}
	/**
	 * save salary of employee 
	 * entry in ledger debit side and company bal cut
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/givePayment")
	public @ResponseBody String givePayment(HttpServletRequest request) {
		
		try {
			long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			if(bankName.equals(""))
			{
				employeeSalary.setChequeDate(null);
				bankName=null;
				cheqNo=null;
			}
			else
			{
				//employeeSalary.setChequeDate(dateFormat.parse(cheqDate));
				employeeSalary.setChequeDate(new Date());
			}
			
			employeeSalary.setEmployeeDetails(employeeDetails);
			employeeSalary.setPayingAmount(amount);
			employeeSalary.setBankName(bankName);
			employeeSalary.setChequeNumber(cheqNo);
			
			employeeSalary.setComment(comment);
			employeeSalary.setPayingDate(new Date());			
			employeeDetailsService.givePayment(employeeSalary);
			
		} catch (Exception e) {
			
			System.out.println(e.toString());
			return "Failed";
			
		}
		
		
		return "Success";
	}
	
	/**
	 * <pre>
	 * open payment page with employee payment details(Amount Paid,Amount UnPaid, Total Amount)
	 * </pre>
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/openPaymentModel")
	public @ResponseBody EmployeePaymentModel openPaymentModel(HttpServletRequest request) {
		
		try {
			long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
			
			EmployeePaymentModel employeePaymentModel=employeeDetailsService.openPaymentModel(employeeDetailsId);
			
			return employeePaymentModel;
		} catch (Exception e) {
			System.out.println(e.toString());
			return null;
		}
		
		
		
	}
	/***
	 * send sms to selected emplyee 
	 * if one select then edited number  use for send sms
	 * else send on registered number
	 * @param request
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/sendSMS")
	public @ResponseBody String sendSMS(HttpServletRequest request) {
		
		try {
			
			String employeeDetailsIdList=request.getParameter("employeeDetailsId");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			
			employeeDetailsService.sendSMSTOEmployee(employeeDetailsIdList, smsText, mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
		
		
		
	}
	/***
	 * <pre>
	 * show employee details by employeeDetailsId
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return ManageEmployee.jsp
	 */
	@Transactional 	@RequestMapping("/getEmployeeView")
	public  ModelAndView fetchEmployeeDetails(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "HRM");
		
		
		List<EmployeeViewModel> employeeDetailModelList=employeeDetailsService.fetchEmployeeDetail(Long.parseLong(request.getParameter("employeeDetailsId")));
		/*fetching employeeDetailBy employeeId*/
		
		model.addAttribute("employeeViewModelList", employeeDetailModelList);// sending employeeDetailModelList this list to jsp page
		model.addAttribute("count",employeeDetailModelList.size());//optional, sending the count of list to the jsp page
		return new ModelAndView("ManageEmployee");
	}
	/**
	 * <pre>
	 * view employee salary details page and salary details 
	 * and employee info will display by ajax response 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return EmployeeSalaryDetails.jsp
	 */
	@Transactional 	@RequestMapping("/viewEmployeeSalary")
	public  ModelAndView viewEmployeeSalary(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Employee Salary Details");
	
		session.setAttribute("lastUrl","viewEmployeeSalary");
		model.addAttribute("employeeDetailsId", request.getParameter("employeeDetailsId"));
		model.addAttribute("saveMsg",session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("EmployeeSalaryDetails");
	}
	
	/***
	 * <pre>
	 * salary given page open with (unpaid salary,paid salary,total salary , etc) 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return makePayment.jsp
	 */
	@Transactional 	@RequestMapping("/paymentEmployee")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Employee Payment");
	
		
		  long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		String filter="ViewAll";
		
		List<EmployeeSalaryStatus> employeeSalaryStatuslist = employeeDetailsService.tofilterRangeEmployeeSalaryStatusForWebApp("", "",filter, employeeDetailsId);
		
		PaymentDoInfo paymentDoInfo=new PaymentDoInfo();
		paymentDoInfo.setPkId(employeeSalaryStatuslist.get(0).getEmployeeDetailsPkId());
		paymentDoInfo.setId(employeeSalaryStatuslist.get(0).getEmployeeDetailsId());
		paymentDoInfo.setName(employeeSalaryStatuslist.get(0).getName());
		paymentDoInfo.setType("employee");
		paymentDoInfo.setAmountPaid(employeeSalaryStatuslist.get(0).getAmountPaidCurrentMonth());
		paymentDoInfo.setAmountUnPaid(employeeSalaryStatuslist.get(0).getAmountPendingCurrentMonth());
		paymentDoInfo.setTotalAmount(employeeSalaryStatuslist.get(0).getTotalAmount());
		paymentDoInfo.setUrl("giveEmployeePayment");
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		return new ModelAndView("makePayment");
	}
	/**
	 * <pre>
	 * save given salary
	 * entry in ledger debit side and company bal cut
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return redirect to manage employee or employee salary details page
	 */
	@Transactional 	@RequestMapping("/giveEmployeePayment")
	public ModelAndView giveEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
		
			long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			String payType=request.getParameter("payType");
			
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			if(payType.equals(Constants.CASH_PAY_STATUS))
			{
				employeeSalary.setChequeDate(null);
				employeeSalary.setBankName(null);
				employeeSalary.setChequeNumber(null);
				
				employeeSalary.setTransactionReferenceNumber(null);
				employeeSalary.setPaymentMethod(null);
			}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
				employeeSalary.setTransactionReferenceNumber(transactionRefNo);
				employeeSalary.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
				
				employeeSalary.setChequeDate(null);
				employeeSalary.setBankName(null);
				employeeSalary.setChequeNumber(null);
			}else{
				employeeSalary.setTransactionReferenceNumber(null);
				employeeSalary.setPaymentMethod(null);
				
				employeeSalary.setBankName(bankName);
				employeeSalary.setChequeNumber(cheqNo);
				try {
					//employeeSalary.setChequeDate(dateFormat.parse(cheqDate));
					employeeSalary.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			employeeSalary.setPayType(payType);
			employeeSalary.setEmployeeDetails(employeeDetails);
			employeeSalary.setPayingAmount(amount);
			employeeSalary.setBankName(bankName);
			employeeSalary.setChequeNumber(cheqNo);
			
			employeeSalary.setComment(comment);
			employeeSalary.setPayingDate(new Date());			
			employeeDetailsService.givePayment(employeeSalary);
	
			session.setAttribute("saveMsg", "Payment Done SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl")+"?employeeDetailsId="+employeeDetailsId);
	}
	/**
	 * <pre>
	 * edit salary page open with given salary by employeeSalaryId,employeeDetailsId
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return makePayment.jsp
	 */
	@Transactional 	@RequestMapping("/editEmployeePayment")
	public ModelAndView editEmployeePayment(HttpServletRequest request,HttpSession session,Model model){
		
		model.addAttribute("pageName", "Employee Payment Edit");		
		long employeeSalaryId=Long.parseLong(request.getParameter("employeeSalaryId"));
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		  
		String filter="ViewAll";
		
		List<EmployeeSalaryStatus> employeeSalaryStatuslist = employeeDetailsService.tofilterRangeEmployeeSalaryStatusForWebApp("", "",filter, employeeDetailsId);
		EmployeeSalary employeeSalary=employeeDetailsService.fetchEmployeeSalary(employeeSalaryId);
		
		PaymentDoInfo paymentDoInfo=new PaymentDoInfo();
		paymentDoInfo.setPkId(employeeSalaryStatuslist.get(0).getEmployeeDetailsPkId());
		paymentDoInfo.setId(employeeSalaryStatuslist.get(0).getEmployeeDetailsId());
		paymentDoInfo.setName(employeeSalaryStatuslist.get(0).getName());
		paymentDoInfo.setType("employee");
		paymentDoInfo.setAmountPaid(employeeSalaryStatuslist.get(0).getAmountPaidCurrentMonth());
		paymentDoInfo.setAmountUnPaid(employeeSalaryStatuslist.get(0).getAmountPendingCurrentMonth());
		paymentDoInfo.setTotalAmount(employeeSalaryStatuslist.get(0).getTotalAmount());
		paymentDoInfo.setUrl("updateEmployeePayment");						
		
		if(employeeSalary.getPaymentMethod()!=null){
			paymentDoInfo.setPaymentMethodId(employeeSalary.getPaymentMethod().getPaymentMethodId());
			paymentDoInfo.setTransactionRefNo(employeeSalary.getTransactionReferenceNumber());
		}
		
		paymentDoInfo.setPaymentId(employeeSalary.getEmployeeSalaryId());
		paymentDoInfo.setBankName(employeeSalary.getBankName());
		paymentDoInfo.setCheckNumber(employeeSalary.getChequeNumber());
		paymentDoInfo.setPaidAmount(employeeSalary.getPayingAmount());
		paymentDoInfo.setComment(employeeSalary.getComment());
		paymentDoInfo.setPayType(employeeSalary.getPayType());
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		model.addAttribute("forEdit", "forEmployeeSalaryEdit");

		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		return new ModelAndView("makePayment");
	}
	/**
	 * <pre>
	 * edit salary by employeeSalaryId,employeeDetailsId
	 * update in ledger debit side and company bal change
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return redirect to manage employee or employee salary details page
	 */
	@Transactional 	@RequestMapping("/updateEmployeePayment")
	public ModelAndView updateEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
		
			long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			long employeeSalaryId=Long.parseLong(request.getParameter("paymentId"));
			EmployeeSalary employeeSalary=employeeDetailsService.fetchEmployeeSalary(employeeSalaryId);

			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			String payType=request.getParameter("payType");
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			if(payType.equals(Constants.CASH_PAY_STATUS))
			{
				employeeSalary.setBankName(null);
				employeeSalary.setChequeNumber(null);
				employeeSalary.setChequeDate(null);

				employeeSalary.setTransactionReferenceNumber(null);
				employeeSalary.setPaymentMethod(null);
				
			}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
				employeeSalary.setTransactionReferenceNumber(transactionRefNo);
				employeeSalary.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
				
				employeeSalary.setBankName(null);
				employeeSalary.setChequeNumber(null);
				employeeSalary.setChequeDate(null);
			}else{
				employeeSalary.setTransactionReferenceNumber(null);
				employeeSalary.setPaymentMethod(null);
				
				employeeSalary.setBankName(bankName);
				employeeSalary.setChequeNumber(cheqNo);
				try {
					//employeeSalary.setChequeDate(dateFormat.parse(cheqDate));
					employeeSalary.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			employeeSalary.setPayType(payType);
			employeeSalary.setEmployeeDetails(employeeDetails);
			employeeSalary.setPayingAmount(amount);
			employeeSalary.setBankName(bankName);
			employeeSalary.setChequeNumber(cheqNo);			
			employeeSalary.setComment(comment);		
			employeeDetailsService.updatePayment(employeeSalary);
	
			session.setAttribute("saveMsg", "Payment Updated SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl")+"?employeeDetailsId="+employeeDetailsId);
	}
	/**
	 * <pre>
	 * disable employee given payment
	 * update in ledger debit side and company bal
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return Success/Failure
	 */
	@Transactional 	@RequestMapping("/deleteEmployeePayment")
	public @ResponseBody String deleteEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
		
		long employeeSalaryId=Long.parseLong(request.getParameter("employeeSalaryId"));
		EmployeeSalary employeeSalary=employeeDetailsService.fetchEmployeeSalary(employeeSalaryId);
		employeeSalary.setStatus(true);
		employeeDetailsService.updatePayment(employeeSalary);
		
		return "Success";
	}
	/**
	 * <pre>
	 * salesman report showing how much business order/amount did
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return SalesManReport.jsp
	 */
	@Transactional 	@RequestMapping("/fetchSalesManReport")
	public ModelAndView fetchSalesManReport(Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Sales Man Report");
	
		
		    String range=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");
		  
		SalesManReport salesManReport=employeeDetailsService.fetchSalesManReport(range, startDate, endDate);
		model.addAttribute("salesManReport", salesManReport);
		
		String partUrl="";
		
		if(range.equals("range"))
		{
			partUrl="range=range&startDate="+startDate+"&endDate="+endDate;
		}
		else if(range.equals("today"))
		{
			partUrl="range=today";
		}
		else if(range.equals("yesterday"))
		{
			partUrl="range=yesterday";
		}
		else if(range.equals("last7days"))
		{
			partUrl="range=last7days";
		}
		else if(range.equals("last6month"))
		{
			partUrl="range=last6month";
		}
		else if(range.equals("last3months"))
		{
			partUrl="range=last3months";
		}
		else if(range.equals("last1month"))
		{
			partUrl="range=last1month";
		}
		else if(range.equals("pickDate"))
		{
			partUrl="range=pickDate&startDate="+startDate;
		}
		else if(range.equals("viewAll"))
		{
			partUrl="range=viewAll";
		}
		model.addAttribute("partUrl", partUrl);
		
		return new ModelAndView("SalesManReport");
	}
	/**
	 * <pre>
	 * disable employee and block him from working area 
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return  redirect fetchEmployeeList
	 */
	@Transactional 	@RequestMapping("/disableEmployee")
	public ModelAndView disableEmployee(Model model,HttpSession session,HttpServletRequest request) {
	
		
		
		  employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(request.getParameter("employeeDetailsId")));
		  if(employeeDetails.isStatus())
		  {
			  employeeDetails.setStatus(false);
		  }
		  else
		  {
			  employeeDetails.setStatus(true);
		  }
		  
		  employeeDetails.setEmployeeDetailsDisableDatetime(new Date());
		  employeeDetailsService.updateForWebApp(employeeDetails,false);
		  
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	
	/**
	 * <pre>
	 * open employee monitoring page with department list
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/findLocation")
	public ModelAndView findLocation(Model model,HttpSession session,HttpServletRequest request) {
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		return new ModelAndView("currentRoute");
	}
	/**
	 * <pre>
	 * fetch all employee location details fetch by department 
	 * </pre>
	 * @param request
	 * @return
	 */
	//fetchEmployeeLatLng
	@Transactional 	@RequestMapping("/fetchEmployeeLatLng")
	public @ResponseBody List<EmployeeLastLocation> fetchEmployeeLatLng(HttpServletRequest request) {
		
		List<EmployeeLastLocation> employeeDetails=employeeDetailsService.fetchEmployeeLastLocationByDepartmentId(Long.parseLong(request.getParameter("departmentId")),request.getParameter("pickDate"));
		
		return employeeDetails;
	}
	/**
	 * <pre>
	 * fetch single employee location details fetch by department 
	 * </pre>
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchSingleEmployeeLatLng")
	public @ResponseBody List<EmployeeLastLocation> fetchSingleEmployeeLatLng(HttpServletRequest request) {
		
		List<EmployeeLastLocation> employeeDetails=employeeDetailsService.fetchEmployeeLocationByEmployeeDetailsId(Long.parseLong(request.getParameter("employeeDetailsId")),request.getParameter("pickDate"));
		
		return employeeDetails;
	}
	
	/*@RequestMapping("/fetchEmployeeDetails")
	public @ResponseBody EmployeeLocation fetchEmployeeDetails(HttpServletRequest request) {
		EmployeeLocation employeeLocation=new EmployeeLocation();
		EmployeeDetails employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(request.getParameter("employeeDetailsId")));
		List<Area> areaList=employeeDetailsService.fetchAreaByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		
		employeeLocation.setEmployeeDetails(employeeDetails);
		employeeLocation.setAreaList(areaList);
		return employeeLocation;
	}*/
	/**
	 * open forget password screen for gatekeeper password reset
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/openForgetPasswordGateKeeper")
	public ModelAndView openForgetPasswordGateKeeper(Model model,HttpServletRequest request,HttpSession session){
		System.out.println("in openForgetPasswordGateKeeper");
		model.addAttribute("pageName", "Reset PassWord");
		return new ModelAndView("forget_password_gatekeeper");
	}
	
	/*@RequestMapping("/forgetPasswordSendOtp_gk")
	public @ResponseBody String forgetPasswordSendOtp(Model model,HttpServletRequest request,HttpSession session){
		System.out.println("in forgetPasswordSendOtp");
		
		
		String emailIdAndMobileNumber=request.getParameter("emailIdAndMobNo");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		String responseText="";
		String otpNumber;
		if(employeeDetails.getContact().getEmailId().equals(emailIdAndMobileNumber.trim())){			
			otpNumber=companyService.sendOTPToCompanyUsingMailAndSMS(emailIdAndMobileNumber, false);
			responseText="OTP send to Your EmailId";			
		}else if(employeeDetails.getContact().getMobileNumber().equals(emailIdAndMobileNumber.trim())){			
			otpNumber=companyService.sendOTPToCompanyUsingMailAndSMS(emailIdAndMobileNumber, true);
			responseText="OTP send to Your Mobile Number";			
		}else{
			otpNumber="";
			responseText="Failed";			
		}
		session.setAttribute("forgetPasswordOtpNumber_gk", otpNumber);
		return responseText;
	}
	
	@Transactional 	@RequestMapping("/checkGatekeeperOTP")
	public @ResponseBody String checkGatekeeperOTP(HttpServletRequest request,HttpSession session){
		String otp=request.getParameter("otp");
		
		if( ((String)session.getAttribute("forgetPasswordOtpNumber_gk")).equals(otp)){
			return "success";
		}else{
			return "failed";
		}
	}
*/	
	/**
	 * update/forget password of gatekeeper
	 * @param request
	 * @param session
	 * @return redirect to index page
	 */
	@Transactional 	@RequestMapping("/forgetPasswordGatekeeper")
	public ModelAndView forgetPasswordGatekeeper(HttpServletRequest request,HttpSession session,Model model){
		System.out.println("in forgetPasswordGatekeeper");
		
		String password=request.getParameter("password");		
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		Employee employee=employeeDetails.getEmployee();
		employee.setPassword(password);
		employeeService.updateForWebApp(employee);
		
		session.setAttribute("employeeDetails", employeeDetails);	
		
		session.setAttribute("msg","Forget Password SuccessFully");
		
		return new ModelAndView("redirect:/");
	}
	/**
	 * <pre>
	 * check gatekeeper password same or differenct
	 * </pre>
	 * @param request
	 * @param session
	 * @return Success/failure
	 */
	@Transactional 	@RequestMapping("/checkGateKeeperPassword") 
	public @ResponseBody String checkGateKeeperPassword(HttpServletRequest request,HttpSession session){
		System.out.println("in checkGateKeeperPassword");
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		Employee employee=employeeDetails.getEmployee();
				
		if(employee.getPassword().equals(request.getParameter("password"))){
			return "Success";
		}else{
			return "Failed";
		}
	}
	/**
	 * <pre>
	 * fetch employee by departmentId
	 * </pre>
	 * @param deparmentId
	 * @param request
	 * @param model
	 * @param session
	 * @return employee list
	 */
	@Transactional
	@RequestMapping(value="/fetchEmployeeDetailsByDepartmentId/{deparmentId}")
	public ResponseEntity<EmployeeListResponse> fetchEemployeeListByDepartmentId(@ModelAttribute("deparmentId") long deparmentId,HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("fetch employee list by departmentId");
		
		EmployeeListResponse employeeListResponse=new EmployeeListResponse();
		
		List<EmployeeDetails> employeeDetailsList=employeeDetailsService.fetchEmployeeDetailsByDepartmentId(deparmentId);
		if(employeeDetailsList==null){
			employeeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			employeeListResponse.setErrorMsg("Employee Not Found");
		}else{
			List<EmployeeNameAndId> employeeNameAndIds=new ArrayList<>();
			
			for(EmployeeDetails employeeDetails : employeeDetailsList){
				
				employeeNameAndIds.add(new EmployeeNameAndId(
															employeeDetails.getEmployee().getEmployeeId(), 
															employeeDetails.getName(),
															employeeDetails.getEmployee().getDepartment().getName(),
															employeeDetails.getEmployee().getDepartment().getShortNameForEmployeeId(),
															employeeDetails.getEmployee().getUserId(),
															employeeDetails.getUserCode()
															));
				
			}
			employeeListResponse.setEmployeeDetailsList(employeeNameAndIds);
			employeeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
		return new ResponseEntity<EmployeeListResponse>(employeeListResponse,HttpStatus.OK);
	}
}
