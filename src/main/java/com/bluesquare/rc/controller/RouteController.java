package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Route;
import com.bluesquare.rc.entities.RoutePoints;
import com.bluesquare.rc.models.RouteResponse;
import com.bluesquare.rc.models.RouteSaveRequest;
import com.bluesquare.rc.responseEntities.RouteModel;
import com.bluesquare.rc.responseEntities.RoutePointsModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.RouteService;
import com.bluesquare.rc.utils.Constants;

/**
 * <pre>
 * @author Sachin Pawar 05-06-2018 Make Controller 
 * API End Points :
 * 1.save_route
 * 2.update_route
 * 3.fetch_route_list
 * 4.fetch_route_points_list/{routeId}
 * 5.fetch_route/{routeId}
 * 6.fetch_route_point/{routePointsId}
 */
@Controller
public class RouteController {

	@Autowired
	RouteService routeService;
	
	@Transactional 	@RequestMapping("/addRoute")
	public ModelAndView indexPage(HttpSession session,Model model) 
	{		
		System.out.println("Employee Routes");
		model.addAttribute("pageName", "Employee Routes");
		
		return new ModelAndView("addRoute");
	}
	
	@Transactional
	@RequestMapping("/save_route")
	public ResponseEntity<BaseDomain> saveRoute(@RequestBody RouteSaveRequest routeSaveRequest, Model model,
			HttpSession session) {
		System.out.println("saving route");

		BaseDomain baseDomain = new BaseDomain();
		
		Route route=routeService.fetchRouteByRouteName(routeSaveRequest.getRoute().getRouteName());
		if(route!=null){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Route Name Already Used");
			return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
		}
		route=routeSaveRequest.getRoute();
		route.setAddedDatetime(new Date());
		routeService.saveRoute(route);
		
		List<RoutePoints> routePointsList=new ArrayList<>(); 
		for(RoutePoints routePoints: routeSaveRequest.getRoutePointsList()){
			routePoints.setRoute(route);
			routePointsList.add(routePoints);
		}
		routeService.saveRoutePointsList(routePointsList);
		
		baseDomain.setStatus(Constants.SAVE_SUCCESS);

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/update_route")
	public ResponseEntity<BaseDomain> updateRoute(@RequestBody RouteSaveRequest routeSaveRequest, Model model,
			HttpSession session) {
		System.out.println("update route");

		BaseDomain baseDomain = new BaseDomain();
		//Route oldRoute=routeService.fetchRouteByRouteId(routeSaveRequest.getRoute().getRouteId());
		
		Route route=routeService.checkRouteNameDuplicate(routeSaveRequest.getRoute().getRouteName(),routeSaveRequest.getRoute().getRouteId());
		if(route!=null){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Route Name Already Used");
			return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
		}
		
		//update route details
		Route routeNew=routeSaveRequest.getRoute();
		route=routeService.fetchRouteByRouteId(routeSaveRequest.getRoute().getRouteId());
		route.setRouteName(routeNew.getRouteName());
		routeService.updateRoute(route);
		
		//delete old route points
		routeService.deleteRoutePointsList(route.getRouteId());
		
		//save new route points
		List<RoutePoints> routePointsList=new ArrayList<>(); 
		for(RoutePoints routePoints: routeSaveRequest.getRoutePointsList()){
			routePoints.setRoute(route);
			routePointsList.add(routePoints);
		}
		routeService.saveRoutePointsList(routePointsList);
		
		baseDomain.setStatus(Constants.UPDATE_SUCCESS);

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/delete_route/{routeId}")
	public ResponseEntity<BaseDomain> deleteRoute(@ModelAttribute("routeId")long routeId, Model model,
			HttpSession session) {
		System.out.println("delete route");

		BaseDomain baseDomain = new BaseDomain();
		
		//update route details
		Route route=routeService.fetchRouteByRouteId(routeId);
		route.setStatus(true);
		routeService.updateRoute(route);
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/fetch_route_list")
	public ResponseEntity<RouteResponse> fetchRouteList(Model model,HttpSession session) {
		System.out.println("fetch route list");

		RouteResponse routeListResponse = new RouteResponse();
		
		List<Route> routeList=routeService.fetchRouteList();
		if(routeList==null){
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			routeListResponse.setErrorMsg("Route List Not Found");
		}else{
			List<RouteModel> routeModelList=new ArrayList<>();
			for(Route route: routeList){
				RouteModel routeModel=new RouteModel();
				routeModel.setRouteId(route.getRouteId());
				routeModel.setRouteGenId(route.getRouteGenId());
				routeModel.setRouteName(route.getRouteName());
				routeModelList.add(routeModel);
			}
			routeListResponse.setRouteList(routeModelList);
			routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}

		return new ResponseEntity<RouteResponse>(routeListResponse, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/fetch_route_points_list/{routeId}")
	public ResponseEntity<RouteResponse> fetchRoutePointsList(@ModelAttribute("routeId") long routeId,Model model,HttpSession session) {
		System.out.println("fetch route points list");

		RouteResponse routeListResponse = new RouteResponse();
		
		List<RoutePoints> routePointsList=routeService.fetchRoutePointsListByRouteId(routeId);
		if(routePointsList==null){
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			routeListResponse.setErrorMsg("Route Points List Not Found");
		}else{
			List<RoutePointsModel> routePointsModelList=new ArrayList<>();
			for(RoutePoints routePoints: routePointsList){
				
				RouteModel routeModel=new RouteModel();
				routeModel.setRouteId(routePoints.getRoute().getRouteId());
				routeModel.setRouteGenId(routePoints.getRoute().getRouteGenId());
				routeModel.setRouteName(routePoints.getRoute().getRouteName());
				
				RoutePointsModel routePointsModel=new RoutePointsModel(
						routePoints.getRoutePointsId(), 
						routePoints.getRouteAddress(), 
						routePoints.getLatitude(), 
						routePoints.getLongitude(),
						routeModel);
				routePointsModelList.add(routePointsModel);
			}
			routeListResponse.setRoutePointsList(routePointsModelList);
			routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}

		return new ResponseEntity<RouteResponse>(routeListResponse, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/fetch_route/{routeId}")
	public ResponseEntity<RouteResponse> fetchRoute(@ModelAttribute("routeId") long routeId,Model model,HttpSession session) {
		System.out.println("fetch route");

		RouteResponse routeListResponse = new RouteResponse();
		
		Route route =routeService.fetchRouteByRouteId(routeId);
		if(route==null){
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			routeListResponse.setErrorMsg("Route Not Found");
		}else{
			RouteModel routeModel=new RouteModel();
			routeModel.setRouteId(route.getRouteId());
			routeModel.setRouteGenId(route.getRouteGenId());
			routeModel.setRouteName(route.getRouteName());
			routeListResponse.setRoute(routeModel);
			routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}

		return new ResponseEntity<RouteResponse>(routeListResponse, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/fetch_route_point/{routePointsId}")
	public ResponseEntity<RouteResponse> fetchRoutePoints(@ModelAttribute("routePointsId") long routePointsId,Model model,HttpSession session) {
		System.out.println("fetch route point");

		RouteResponse routeListResponse = new RouteResponse();
		
		RoutePoints routePoints =routeService.fetchRoutePointsByRoutePointsId(routePointsId);
		if(routePoints==null){
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			routeListResponse.setErrorMsg("Route Not Found");
		}else{
			RouteModel routeModel=new RouteModel();
			routeModel.setRouteId(routePoints.getRoute().getRouteId());
			routeModel.setRouteGenId(routePoints.getRoute().getRouteGenId());
			routeModel.setRouteName(routePoints.getRoute().getRouteName());
			
			RoutePointsModel routePointsModel=new RoutePointsModel(
					routePoints.getRoutePointsId(), 
					routePoints.getRouteAddress(), 
					routePoints.getLatitude(), 
					routePoints.getLongitude(),
					routeModel);
			routeListResponse.setRoutePoints(routePointsModel);
			routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		}

		return new ResponseEntity<RouteResponse>(routeListResponse, HttpStatus.OK);
	}
}
