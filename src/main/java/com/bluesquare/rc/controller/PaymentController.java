package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.models.ChequePaymentReportModel;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.CollectionReportPaymentDetails;
import com.bluesquare.rc.models.EditOrderDetailsPaymentModel;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.models.PaymentListReport;
import com.bluesquare.rc.models.PaymentPendingList;
import com.bluesquare.rc.models.PaymentReportModel;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.LedgerService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.MathUtils;
import com.bluesquare.rc.utils.SendSMS;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * 1.fetchPaymentPendingList
 * 2.fetchLedgerPaymentView
 * 3.getCollectionReportDetails
 * 4.fetchPaymentListByOrderId
 * 5.paymentReport
 * 6.editOrderDetailsPayment
 * 7.deleteOrderDetailsPayment
 * 8.updateOrderDetailsPayment
 * 9.chequeReport
 * 10.defineChequeBounced
 * 11.sendSmsChequeReport
 * </pre>
 */
@Controller
public class PaymentController {
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	LedgerService ledgerService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	BranchDAO branchDAO;
	/**
	 * fetch payment pending list (Employee,Supplier)
	 * @param model
	 * @param request
	 * @param session
	 * @return payment.jsp
	 */
	@Transactional 	@RequestMapping("/fetchPaymentPendingList")
	public ModelAndView fetchPaymentPendingList(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Payment Pending List");
		
		session.setAttribute("lastUrl", "fetchPaymentPendingList");
		
		List<PaymentPendingList> paymentPendingLists=paymentService.fetchPaymentPendingList();
		
		model.addAttribute("paymentPendingLists", paymentPendingLists);
		
		return new ModelAndView("payment");
	}
	
/**
 * fetch ledger records by range,startDate,endDate
 * current balance of company 
 * @param model
 * @param request
 * @param session
 * @return ledger.jsp
 */
	@Transactional 	@RequestMapping("/fetchLedgerPaymentView")
	public ModelAndView fetchLedgerPaymentView(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Ledger");
		
		
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<LedgerPaymentView> ledgerPaymentViews=ledgerService.fetchLedgerPaymentView(startDate, endDate, range);
		model.addAttribute("ledgerPaymentViews", ledgerPaymentViews);
		
		Branch branch=branchDAO.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds());
		model.addAttribute("balance", branch.getBalance());
		
		return new ModelAndView("ledger");
	}
	/**
	 * <pre>
	 * fetch collection details of salesman-order , counter order by range,startDate,endDate
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return CollectionReport.jsp
	 */
	@Transactional 	@RequestMapping("/getCollectionReportDetails")
	public ModelAndView getCollectionReportDetails(Model model,HttpServletRequest request,HttpSession session){
		
		
		  model.addAttribute("pageName", "Collection Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		CollectionReportMain collectionReportMain=paymentService.getCollectionReportDetails(startDate, endDate, range);
		model.addAttribute("collectionReportMain", collectionReportMain);
		
		return new ModelAndView("CollectionReport");
	}
	/**
	 * fetch payment list by order id (salesman order) 
	 * @param model
	 * @param request
	 * @param session
	 * @return PaymentListReport list
	 */
	@Transactional 	@RequestMapping("/fetchPaymentListByOrderId")
	public @ResponseBody List<PaymentListReport> fetchPaymentListByOrderIdAndReOrderId(Model model,HttpServletRequest request,HttpSession session){
				
		List<PaymentListReport> paymentList=paymentService.fetchPaymentListByOrderDetailsIdForCollectionReport(request.getParameter("orderId"));
		
		return paymentList;
	}
	/**
	 *<pre>
	 *payment report where salesman order and counter order payment showing
	 *edit/delete option there 
	 *</pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return paymentReport
	 */
	@Transactional 	@RequestMapping("/paymentReport")
	public ModelAndView paymentReport(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Payment Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<PaymentReportModel> paymentReportModelList=paymentService.fetchPaymentReportModelList(startDate, endDate, range);
				
		model.addAttribute("paymentReportModelList", paymentReportModelList);
		
		double totalAmountPaid=0;
		if(paymentReportModelList!=null){
			for(PaymentReportModel paymentReportModel: paymentReportModelList){
				if(paymentReportModel.getPaymentMode().equals("Refund")){
					continue;
				}
				totalAmountPaid+=paymentReportModel.getAmountPaid();
			}
			model.addAttribute("noOfBills", paymentReportModelList.size());
		}else{
			model.addAttribute("noOfBills", 0);
		}			
		
		model.addAttribute("totalAmountPaid", MathUtils.round(totalAmountPaid, 2));
		
		
		return new ModelAndView("paymentReport");
	}
	/**
	 * <pre>
	 * open payment page for edit salesman order or counter order payment 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return editPayment.jsp
	 */
	@Transactional 	@RequestMapping("/editOrderDetailsPayment")
	public ModelAndView editOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId");
		EditOrderDetailsPaymentModel editOrderDetailsPaymentModel; 
		
		//order contains ORD for salesman order ans CORD for counter order
		if(orderId.contains("CORD")){
			List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(orderId);
			PaymentCounter paymentCounter=counterOrderService.fetchPaymentCounterByPaymentCounterId(paymentId);
			
			double totalAmount=paymentCounter.getCounterOrder().getTotalAmountWithTax(); 
			
			double amountPaid=0;
			for(PaymentCounter paymentCounter2: paymentCounterList){
				amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
			
			double totalAmountPaid=amountPaid;//paymentCounterList.get(0).getTotalAmountPaid(); 
			double balanceAmount=totalAmount-totalAmountPaid;
			
			String fullPartialStatus;
			if(totalAmount==totalAmountPaid){
				fullPartialStatus=Constants.FULL_PAY_STATUS;
			}else{
				fullPartialStatus=Constants.PARTIAL_PAY_STATUS;
			}
			editOrderDetailsPaymentModel=new EditOrderDetailsPaymentModel(
															(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
															orderId, 
															paymentCounter.getPaymentCounterId(),
															(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
															(paymentCounter.getCounterOrder().getBusinessName()==null)?"":paymentCounter.getCounterOrder().getBusinessName().getAddress(), 
															totalAmount, 
															totalAmountPaid, 
															balanceAmount, 
															fullPartialStatus, 
															paymentCounter.getPayType().equals("Wallet")?"Cash":paymentCounter.getPayType(), 
															paymentCounter.getBankName(), 
															(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
															balanceAmount, 
															paymentCounter.getChequeNumber(), 
															(paymentCounter.getChequeDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(paymentCounter.getChequeDate()),
															(paymentCounter.getDueDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(paymentCounter.getDueDate()),
															(paymentCounter.getPaymentMethod()!=null)?paymentCounter.getPaymentMethod().getPaymentMethodId():0,
															paymentCounter.getTransactionRefNo(),
															paymentCounter.getComment(),
															(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund");
			
		}else{
			List<Payment> paymentList=paymentService.fetchPaymentListByOrderDetailsId(orderId);
			Payment payment=paymentService.fetchPaymentBYPaymentId(paymentId);
			
			double totalAmount=payment.getOrderDetails().getIssuedTotalAmountWithTax();
			double balanceAmount=paymentList.get(0).getDueAmount();
			double totalAmountPaid=totalAmount-balanceAmount;
			
			String fullPartialStatus;
			if(totalAmount==totalAmountPaid){
				fullPartialStatus=Constants.FULL_PAY_STATUS;
			}else{
				fullPartialStatus=Constants.PARTIAL_PAY_STATUS;
			}
			editOrderDetailsPaymentModel=new EditOrderDetailsPaymentModel(
												payment.getOrderDetails().getBusinessName().getShopName(), 
												orderId,
												payment.getPaymentId(),
												payment.getOrderDetails().getBusinessName().getContact().getMobileNumber(), 
												payment.getOrderDetails().getBusinessName().getAddress(), 
												totalAmount, 
												totalAmountPaid, 
												balanceAmount,  
												fullPartialStatus, 
												payment.getPayType(), 
												payment.getBankName(), 
												payment.getPaidAmount(), 
												balanceAmount, 
												payment.getChequeNumber(), 
												(payment.getChequeDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(payment.getChequeDate()),
												(payment.getDueDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(payment.getDueDate()),
												(payment.getPaymentMethod()!=null)?payment.getPaymentMethod().getPaymentMethodId():0,
												payment.getTransactionReferenceNumber(),
												payment.getComment(),
												"Paid");
			
		}
		
		model.addAttribute("payment", editOrderDetailsPaymentModel);
		

		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		model.addAttribute("url", "updateOrderDetailsPayment");
		
		return new ModelAndView("editPayment");
	}
	/**
	 * <pre>
	 * delete order payment
	 * delete ledger 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/paymentReport?range=currentMonth
	 */
	@Transactional 	@RequestMapping("/deleteOrderDetailsPayment")
	public ModelAndView deleteOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId"); 
		
		if(orderId.contains("CORD")){
			counterOrderService.deletePayment(paymentId);
		}else{
			paymentService.deletePayment(paymentId);
		}
		
		return new ModelAndView("redirect:/paymentReport?range=currentMonth");
	}
	/**
	 * <pre>
	 * update salesman order/counter order payment
	 * update ledger and company balance 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/paymentReport?range=currentMonth3
	 */
	@Transactional 	@RequestMapping("/updateOrderDetailsPayment")
	public ModelAndView updateOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId"));  
		String orderId=request.getParameter("orderId");
		String dueDate=request.getParameter("dueDate");
		String bankName=request.getParameter("bankName");
		String chequeNo=request.getParameter("checkNo");
		String chequeDate=request.getParameter("checkDate");
		double paidAmount=Double.parseDouble(request.getParameter("paidAmount"));		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String comment=request.getParameter("comment");
		String transactionRefNo=request.getParameter("transactionRefNo");
		long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
		String payType=request.getParameter("payType");
		if(orderId.contains("CORD")){
			List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(orderId);
			 PaymentCounter paymentCounter=counterOrderService.fetchPaymentCounterByPaymentCounterId(paymentId);	
			 CounterOrder counterOrder=counterOrderService.fetchCounterOrder(orderId);
		 		
			 if(payType.equals(Constants.CASH_PAY_STATUS))
				{
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
				}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(transactionRefNo);
					paymentCounter.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
					paymentCounter.setComment(comment);
				}else{
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
					
					try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(chequeNo);
				}
				paymentCounter.setPayType(payType);
				
				double totalAmountPaid=0;
				for(PaymentCounter paymentCounter2: paymentCounterList){
					totalAmountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
				}
				
				if((totalAmountPaid+paidAmount)==counterOrder.getTotalAmountWithTax()){
					counterOrder.setPayStatus(true);
					paymentCounter.setDueDate(new Date());
					counterOrder.setPaymentDueDate(new Date());
				}else{
					try {
						paymentCounter.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
						counterOrder.setPaymentDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
					} catch (ParseException e) {}
					counterOrder.setPayStatus(false);
				}
				
				paymentCounter.setCurrentAmountPaid(paidAmount);
			paymentCounter.setCounterOrder(counterOrder);
			counterOrderService.updatePayment(paymentCounter);
			
		}else{
			List<Payment> paymentList=paymentService.fetchPaymentListByOrderDetailsId(orderId);
			Payment payment=paymentService.fetchPaymentBYPaymentId(paymentId);
			OrderDetails orderDetails=payment.getOrderDetails();
			
			payment.setPaidAmount(paidAmount);			
			
			 if(payType.equals(Constants.CASH_PAY_STATUS))
				{
					payment.setChequeDate(null);
					payment.setBankName(null);
					payment.setChequeNumber(null);
					
					payment.setTransactionReferenceNumber(null);
					payment.setPaymentMethod(null);
				}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
					payment.setChequeDate(null);
					payment.setBankName(null);
					payment.setChequeNumber(null);
					
					payment.setTransactionReferenceNumber(transactionRefNo);
					payment.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
					payment.setComment(comment);
				}else{
					payment.setTransactionReferenceNumber(null);
					payment.setPaymentMethod(null);
					
					try { payment.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					payment.setBankName(bankName);
					payment.setChequeNumber(chequeNo);
				}
				payment.setPayType(payType);

			
			double totalAmountPaid=0;
			for(Payment payment2: paymentList){
				totalAmountPaid+=payment2.getPaidAmount();
			}
			
			if((totalAmountPaid+paidAmount)==orderDetails.getIssuedTotalAmountWithTax()){
				orderDetails.setPayStatus(true);
				orderDetails.setOrderDetailsPaymentTakeDatetime(new Date());
				payment.setDueDate(new Date());
			}else{
				try {
					payment.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
					orderDetails.setOrderDetailsPaymentTakeDatetime(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
				} catch (ParseException e) {}
				orderDetails.setPayStatus(false);
			}
			payment.setPaidAmount(paidAmount);
			payment.setOrderDetails(orderDetails);
			paymentService.updatePayment(payment);		
		}		
		
		return new ModelAndView("redirect:/paymentReport?range=currentMonth");
	}
	/**
	 * <pre>
	 * cheque report where show only cheque payment transaction
	 * and there allow to define cheque cleared or bounced
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return chequePaymentReport
	 */
	@Transactional 	@RequestMapping("/chequeReport")
	public ModelAndView chequeReport(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Cheque Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<ChequePaymentReportModel> chequePaymentReportModelList=paymentService.fetchChequePaymentReportModelList(startDate, endDate, range);
		model.addAttribute("chequePaymentReportModelList", chequePaymentReportModelList);
		
		long totalNoOfCheques=0,totalNoCleared=0,totalNoBounced=0;
		if(chequePaymentReportModelList!=null){
			for(ChequePaymentReportModel chequePaymentReportModel: chequePaymentReportModelList){
				totalNoOfCheques+=1;
				if(chequePaymentReportModel.isCheckClearStatus()){
					totalNoCleared+=1;
				}else{
					totalNoBounced+=1;
				}
			}
		}
		
		model.addAttribute("totalNoOfCheques",totalNoOfCheques );
		model.addAttribute("totalNoCleared",totalNoCleared );
		model.addAttribute("totalNoBounced", totalNoBounced);
		
		
		session.setAttribute("chequeReportRange", range);
		session.setAttribute("chequeReportStartDate", startDate);
		session.setAttribute("chequeReportEndDate", range);
		
		return new ModelAndView("chequePaymentReport");
	}
	/**
	 * <pre>
	 * define cheque bounced and 
	 * delete entry from ledger and update company balance 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/defineChequeBounced")
	public ModelAndView defineChequeBounced(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId"); 
		
		if(orderId.contains("CORD")){
			counterOrderService.defineChequeBounced(paymentId);
		}else{
			paymentService.defineChequeBounced(paymentId);
		}
		
		String startDate=(String)session.getAttribute("chequeReportStartDate");
		String endDate=(String)session.getAttribute("chequeReportEndDate");
		String range=(String)session.getAttribute("chequeReportRange");
		
		return new ModelAndView("redirect:/chequeReport?range="+range+"&startDate="+startDate+"&endDate="+endDate);
	}
	/**
	 * <pre>
	 * send sms to given number regarding cheque payment
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional @RequestMapping("/sendSmsChequeReport")
	public @ResponseBody String sendSmsChequeReport(Model model,HttpServletRequest request,HttpSession session){
		
		String mobNo=request.getParameter("mobNo");
		String sms=request.getParameter("sms");
		boolean isSend=SendSMS.sendSMS(Long.parseLong(mobNo), sms);
		
		if(isSend)
			return "Success";
		else
			return "Failed";
	}
	
	@Transactional 	@RequestMapping("/getPendingPaymentListForAlert")
	public ModelAndView getPendingPaymentListForAlert(Model model,HttpServletRequest request,HttpSession session){
		
			
		  model.addAttribute("pageName", "Pending Payment List");
		List<CollectionReportPaymentDetails> collectionReportPaymentDetailsList=paymentService.getPendingPaymentList();
		model.addAttribute("collectionReportPaymentDetailsList", collectionReportPaymentDetailsList);		
		
		
		  return new ModelAndView("pendingPaymentList");
		  
	}
	
}
