package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Meeting;
import com.bluesquare.rc.models.MeetingListRequest;
import com.bluesquare.rc.models.MeetingScheduleCheckRequest;
import com.bluesquare.rc.models.MeetingScheduleModel;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BusinessNameModel;
import com.bluesquare.rc.responseEntities.BusinessTypeModel;
import com.bluesquare.rc.responseEntities.MeetingModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.MeetingRequest;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.MeetingService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 19-06-2018 Code Documentation
 * API End Points
 * 1.set_meeting
 * 2.save_meeting
 * 3.open_update_meeting
 * 4.update_meeting
 * 5.checkMeetingScheduleForSave
 * 6.checkMeetingScheduleForUpdate
 * 7.delete_meeting
 * 8.cancel_meeting
 * 9.fetch_scheduled_meeting
 * 10.fetch_scheduled_meeting_ajax
 * 11.fetchMeetingsByEmployeeIdAndPickDate
 * 12.reschedule_meeting
 * 13.open_reschedule_meeting
 * </pre>
 */
@Controller
public class MeetingController {

	@Autowired
	MeetingService meetingService;
	
	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	/**
	 * <pre>
	 * open set meeting page with department list
	 * </pre>
	 * @param model
	 * @param session
	 * @return set_meeting page with department list
	 */
	@Transactional 	@RequestMapping("/set_meeting")
	public ModelAndView setMeeting(Model model,HttpSession session) {

		model.addAttribute("pageName", "Set Meeting");
		
		System.out.println("In set meeting");
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		model.addAttribute("url", "save_meeting");
		return new ModelAndView("AddMeetingSchedule");
	}
	/**
	 * <pre>
	 * save meeting by existing business or new business
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return redirect fetch_scheduled_meeting
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/save_meeting")
	public ModelAndView saveMeeting(Model model,HttpSession session,HttpServletRequest request) throws ParseException {
		
		System.out.println("In Save meeting");
		
		long employeeId=Long.parseLong(request.getParameter("employeeId"));
		String businessNameId=request.getParameter("businessId");
		String ownerName=request.getParameter("ownerName");
		String shopName=request.getParameter("shopName");
		String mobileNumber=request.getParameter("mobileNumber");
		String address=request.getParameter("address");
		String date=request.getParameter("meetingDate");
		String fromTime=request.getParameter("meetingStartTime");
		String toTime=request.getParameter("meetingEndTime");
		String meetingVenue=request.getParameter("meetingVenue");
		
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
		
		Meeting meeting=new Meeting();
		meeting.setEmployeeDetails(employeeDetails);
		
		meeting.setOwnerName(ownerName);
		meeting.setShopName(shopName);
		meeting.setAddress(address);
		meeting.setMobileNumber(mobileNumber);
		
		if(businessNameId.equals("Other")){			
			meeting.setBusinessName(null);
		}else{
			meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(businessNameId));
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		meeting.setMeetingFromDateTime(sdf.parse(date+" "+fromTime));
		meeting.setMeetingToDateTime(sdf.parse(date+" "+toTime));

		if(meetingVenue.equals("")){
			meeting.setMeetingVenue(null);
		}else{
			meeting.setMeetingVenue(meetingVenue);
		}
		
		meeting.setMeetingStatus(Constants.MEETING_PENDING);
		meeting.setAddedDateTime(new Date());
		meeting.setUpdatedDateTime(null);
		meeting.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		
		meetingService.saveMeeting(meeting);
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");		
		return new ModelAndView("redirect:/fetch_scheduled_meeting?departmentId=0&pickDate="+dateFormat.format(new Date()));
	}
	/**
	 * <pre>
	 * open edit meeting page for edit meeting dates with filled meeting details
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return AddMeetingSchedule.jsp
	 * @throws Exception
	 */
	@Transactional 	@RequestMapping("/open_update_meeting")
	public ModelAndView openUpdateMeeting(Model model,HttpSession session,HttpServletRequest request) throws Exception {
		model.addAttribute("pageName", "Update Meeting");
		
		System.out.println("In Open Update Meeting");
		
		long meetingId=Long.parseLong(request.getParameter("meetingId"));
		Meeting meeting=meetingService.fetchMeetingByMeetingId(meetingId);

		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY-MM-DD");
		
		Calendar calendarDb=Calendar.getInstance();
		calendarDb.setTime(simpleDateFormat.parse(simpleDateFormat.format(meeting.getAddedDateTime())));
		calendarDb.set(Calendar.HOUR_OF_DAY, 0);
		calendarDb.set(Calendar.MINUTE, 0);
		calendarDb.set(Calendar.SECOND, 0);
		
		Calendar calendarCurrent=Calendar.getInstance();
		calendarCurrent.setTime(simpleDateFormat.parse(simpleDateFormat.format(calendarCurrent.getTime())));
		calendarCurrent.set(Calendar.HOUR_OF_DAY, 0);
		calendarCurrent.set(Calendar.MINUTE, 0);
		calendarCurrent.set(Calendar.SECOND, 0);
		
		
		if (calendarDb.getTime().getTime()<calendarCurrent.getTime().getTime() || !meeting.getMeetingStatus().equals(Constants.MEETING_PENDING)) {
		      throw new Exception("Meeting Expired Or Cancel. Update of this Meeting Not Allowed");
		}
		
		model.addAttribute("meeting", new MeetingModel(
										meeting.getMeetingId(), 
										meeting.getEmployeeDetails(), 
										meeting.getBusinessName()==null?null:
										new BusinessNameModel(
												meeting.getBusinessName().getBusinessNamePkId(), 
												meeting.getBusinessName().getBusinessNameId(), 
												meeting.getBusinessName().getShopName(), 
												meeting.getBusinessName().getOwnerName(), 
												meeting.getBusinessName().getCreditLimit(), 
												meeting.getBusinessName().getGstinNumber(), 
												new BusinessTypeModel(
														meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
														meeting.getBusinessName().getBusinessType().getName()), 
												meeting.getBusinessName().getAddress(), 
												meeting.getBusinessName().getTaxType(), 
												meeting.getBusinessName().getContact(), 
												new AreaModel(
														meeting.getBusinessName().getArea().getAreaId(), 
														meeting.getBusinessName().getArea().getName(), 
														meeting.getBusinessName().getArea().getPincode()), 
												meeting.getBusinessName().getEmployeeSM()==null?null:
												new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()), 
												meeting.getBusinessName().getStatus(),
												meeting.getBusinessName().getBadDebtsStatus()), 
										meeting.getOwnerName(), 
										meeting.getShopName(), 
										meeting.getMobileNumber(), 
										meeting.getAddress(), 
										meeting.getMeetingVenue(), 
										meeting.getCancelReason(), 
										meeting.getMeetingStatus(), 
										meeting.getMeetingReview(), 
										meeting.getMeetingFromDateTime(), 
										meeting.getMeetingToDateTime(), 
										meeting.getAddedDateTime(), 
										meeting.getUpdatedDateTime(), 
										meeting.getCancelDateTime()));
		model.addAttribute("url", "update_meeting");
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		
		return new ModelAndView("AddMeetingSchedule");
	}
	/**
	 * <pre>
	 * update meeting for existing business or new business 
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return redirect fetch_scheduled_meeting
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/update_meeting")
	public ModelAndView updateMeeting(Model model,HttpSession session,HttpServletRequest request) throws ParseException {
		
		System.out.println("In Update meeting");
		
		long meetingId=Long.parseLong(request.getParameter("meetingId"));
		long employeeId=Long.parseLong(request.getParameter("employeeId"));
		String businessNameId=request.getParameter("businessId");
		String ownerName=request.getParameter("ownerName");
		String shopName=request.getParameter("shopName");
		String mobileNumber=request.getParameter("mobileNumber");
		String address=request.getParameter("address");
		String date=request.getParameter("meetingDate");
		String fromTime=request.getParameter("meetingStartTime");
		String toTime=request.getParameter("meetingEndTime");
		String meetingVenue=request.getParameter("meetingVenue");
		
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
		
		Meeting meeting=meetingService.fetchMeetingByMeetingId(meetingId);
		meeting.setEmployeeDetails(employeeDetails);
		

		meeting.setOwnerName(ownerName);
		meeting.setShopName(shopName);
		meeting.setAddress(address);
		meeting.setMobileNumber(mobileNumber);
		
		if(businessNameId.equals("Other")){
			meeting.setBusinessName(null);
		}else{
			meeting.setBusinessName(businessNameService.fetchBusinessForWebApp(businessNameId));
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		meeting.setMeetingFromDateTime(sdf.parse(date+" "+fromTime));
		meeting.setMeetingToDateTime(sdf.parse(date+" "+toTime));

		if(meetingVenue.equals("")){
			meeting.setMeetingVenue(null);
		}else{
			meeting.setMeetingVenue(meetingVenue);
		}
		
		meeting.setMeetingStatus(Constants.MEETING_PENDING);
		meeting.setUpdatedDateTime(new Date());
		
		meetingService.updateMeeting(meeting);
				
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");		
		return new ModelAndView("redirect:/fetch_scheduled_meeting?departmentId=0&pickDate="+dateFormat.format(new Date()));
	}
	
	/*
	//check date and time string to date
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		String date="2018-06-07 06:25 AM";
		Date dateTime=sdf.parse(date);
		System.out.println(dateTime);
	}*/
	/**
	 * <pre>
	 * check  given from and to time is already set for meeting or not for save meeting 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return msg if found set
	 */
	@Transactional 	@RequestMapping(value="/checkMeetingScheduleForSave")
	public @ResponseBody String checkMeetingSchedule(@RequestBody MeetingScheduleCheckRequest meetingScheduleCheckRequest, Model model,HttpServletRequest request,HttpSession session)
	{
		long employeeId=meetingScheduleCheckRequest.getEmployeeId();
		String date=meetingScheduleCheckRequest.getDate();
		String fromTime=meetingScheduleCheckRequest.getFromTime();
		String toTime=meetingScheduleCheckRequest.getToTime();
		
		return meetingService.checkMeetingSchedule(employeeId, date, fromTime, toTime);
	}
	/**
	 * <pre>
	 * check  given from and to time is already set for updating meeting or not except current updating meeting for update meeting
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return msg if found already set
	 */
	@Transactional 	@RequestMapping("/checkMeetingScheduleForUpdate")
	public @ResponseBody String checkMeetingScheduleForUpdate(@RequestBody MeetingScheduleCheckRequest meetingScheduleCheckRequest,Model model,HttpServletRequest request,HttpSession session)
	{
		long meetingId=meetingScheduleCheckRequest.getMeetingId();
		long employeeId=meetingScheduleCheckRequest.getEmployeeId();
		String date=meetingScheduleCheckRequest.getDate();
		String fromTime=meetingScheduleCheckRequest.getFromTime();
		String toTime=meetingScheduleCheckRequest.getToTime();
		
		return meetingService.checkMeetingScheduleForUpdate(employeeId, date, fromTime, toTime,meetingId);
	}
	/**
	 * <pre>
	 * delete meeting permanent
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return Success
	 */
	@Transactional 	@RequestMapping("/delete_meeting")
	public @ResponseBody String deleteMeeting(@RequestBody MeetingRequest meetingRequest,Model model,HttpSession session,HttpServletRequest request) {
		
		System.out.println("In Delete Meeting");
		
		long meetingId=meetingRequest.getMeetingId();
		meetingService.deleteMeeting(meetingId);
		
		return Constants.SUCCESS_RESPONSE;
	}
	/**
	 * <pre>
	 * cancel meeting and change status of meeting
	 * record not delete from db
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return Success
	 */
	@Transactional 	@RequestMapping("/cancel_meeting")
	public @ResponseBody String cancelMeeting(@RequestBody MeetingRequest meetingRequest,Model model,HttpSession session,HttpServletRequest request) {
		
		System.out.println("In Cancel Meeting");
		
		long meetingId=meetingRequest.getMeetingId();
		String cancelReason=meetingRequest.getCancelReason();
		
		meetingService.updateCancelStatus(meetingId, cancelReason); 

		return Constants.SUCCESS_RESPONSE;
	}
	/**
	 * <pre>
	 * find meeting scheduled or not given date and department for employees
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return scheduled_meeting page
	 */
	@Transactional 	@RequestMapping("/fetch_scheduled_meeting")
	public ModelAndView employeeWiseMeeting(Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Employee Scheduled Meetings");
		
		System.out.println("Employee Scheduled Meetings");
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		model.addAttribute("departmentId", departmentId);
		String pickDate=request.getParameter("pickDate");
		model.addAttribute("pickDate", pickDate);
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		
		
		List<MeetingScheduleModel> meetingScheduleList=meetingService.fetchEmployeeWiseScheduledMeeting(departmentId, pickDate);
		model.addAttribute("meetingList", meetingScheduleList);
		
		return new ModelAndView("ManageMeetings");
	}
	/**
	 * <pre>
	 * find meeting scheduled or not given date and department for employees
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return MeetingScheduleModel list
	 */
	@Transactional 	@RequestMapping("/fetch_scheduled_meeting_ajax")
	public @ResponseBody List<MeetingScheduleModel> employeeWiseMeetingAjax(@RequestBody MeetingListRequest meetingListRequest,Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Employee Scheduled Meetings");
		
		System.out.println("Employee Scheduled Meetings Ajax");
		long departmentId=meetingListRequest.getDepartmentId();
		String pickDate=meetingListRequest.getPickDate();
		
		List<MeetingScheduleModel> meetingScheduleList=meetingService.fetchEmployeeWiseScheduledMeeting(departmentId, pickDate);
		model.addAttribute("meetingList", meetingScheduleList);
		
		return meetingScheduleList;
	}
	/**
	 * <pre>
	 * fetch scheduled meeting by pick-date and employee
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return Meeting list
	 */
	@Transactional 	@RequestMapping("/fetchMeetingsByEmployeeIdAndPickDate")
	public @ResponseBody List<MeetingModel> fetchMeetingsByEmployeeIdAndPickDate(@RequestBody MeetingListRequest meetingListRequest,Model model,HttpServletRequest request,HttpSession session){
		
		List<Meeting> meetingList=meetingService.fetchMeetingListbyEmployeeIdAndDate(meetingListRequest.getEmployeeDetailsId(), meetingListRequest.getPickDate());

		List<MeetingModel> meetingModelList=new ArrayList<>();
		if(meetingList!=null){
			for(Meeting meeting:meetingList){
				meetingModelList.add(new MeetingModel(
						meeting.getMeetingId(), 
						null, 
						meeting.getBusinessName()==null?null:
						new BusinessNameModel(
								meeting.getBusinessName().getBusinessNamePkId(), 
								meeting.getBusinessName().getBusinessNameId(), 
								meeting.getBusinessName().getShopName(), 
								meeting.getBusinessName().getOwnerName(), 
								meeting.getBusinessName().getCreditLimit(), 
								meeting.getBusinessName().getGstinNumber(), 
								new BusinessTypeModel(
										meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
										meeting.getBusinessName().getBusinessType().getName()), 
								meeting.getBusinessName().getAddress(), 
								meeting.getBusinessName().getTaxType(), 
								meeting.getBusinessName().getContact(), 
								new AreaModel(
										meeting.getBusinessName().getArea().getAreaId(), 
										meeting.getBusinessName().getArea().getName(), 
										meeting.getBusinessName().getArea().getPincode()),
								meeting.getBusinessName().getEmployeeSM()==null?null:
								new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()), 
								meeting.getBusinessName().getStatus(),
								meeting.getBusinessName().getBadDebtsStatus()), 
						meeting.getOwnerName(), 
						meeting.getShopName(), 
						meeting.getMobileNumber(), 
						meeting.getAddress(), 
						meeting.getMeetingVenue(), 
						meeting.getCancelReason(), 
						meeting.getMeetingStatus(), 
						meeting.getMeetingReview(), 
						meeting.getMeetingFromDateTime(), 
						meeting.getMeetingToDateTime(), 
						meeting.getAddedDateTime(), 
						meeting.getUpdatedDateTime(), 
						meeting.getCancelDateTime()));
			}
		}
		return meetingModelList;
	}
	/**
	 * <pre>
	 * open update meeting page for rescheduling meeting and fille meeting details
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return AddMeetingSchedule.jsp
	 * @throws Exception
	 */
	@Transactional 	@RequestMapping("/open_reschedule_meeting")
	public ModelAndView openRescheduleMeeting(Model model,HttpSession session,HttpServletRequest request) throws Exception {

		model.addAttribute("pageName", "Re-Scheduled Meeting");
		
		System.out.println("In Open ReSchedule Meeting");
		
		long meetingId=Long.parseLong(request.getParameter("meetingId"));
		Meeting meeting=meetingService.fetchMeetingByMeetingId(meetingId);

		Calendar calendarDb=Calendar.getInstance();
		calendarDb.set(Calendar.HOUR_OF_DAY, 0);
		calendarDb.set(Calendar.MINUTE, 0);
		calendarDb.set(Calendar.SECOND, 0);
		
		Calendar calendarCurrent=Calendar.getInstance();
		calendarCurrent.set(Calendar.HOUR_OF_DAY, 0);
		calendarCurrent.set(Calendar.MINUTE, 0);
		calendarCurrent.set(Calendar.SECOND, 0);
		
		if (calendarDb.before(calendarCurrent) || !meeting.getMeetingStatus().equals(Constants.MEETING_PENDING)) {
		      throw new Exception("Meeting Expired Or Cancel. Update of this Meeting Not Allowed");
		}
		
		model.addAttribute("meeting", new MeetingModel(
				meeting.getMeetingId(), 
				meeting.getEmployeeDetails(), 
				meeting.getBusinessName()==null?null:
				new BusinessNameModel(
						meeting.getBusinessName().getBusinessNamePkId(), 
						meeting.getBusinessName().getBusinessNameId(), 
						meeting.getBusinessName().getShopName(), 
						meeting.getBusinessName().getOwnerName(), 
						meeting.getBusinessName().getCreditLimit(), 
						meeting.getBusinessName().getGstinNumber(), 
						new BusinessTypeModel(
								meeting.getBusinessName().getBusinessType().getBusinessTypeId(), 
								meeting.getBusinessName().getBusinessType().getName()), 
						meeting.getBusinessName().getAddress(), 
						meeting.getBusinessName().getTaxType(), 
						meeting.getBusinessName().getContact(), 
						new AreaModel(
								meeting.getBusinessName().getArea().getAreaId(), 
								meeting.getBusinessName().getArea().getName(), 
								meeting.getBusinessName().getArea().getPincode()), 
						meeting.getBusinessName().getEmployeeSM()==null?null:
						new EmployeeNameAndId(meeting.getBusinessName().getEmployeeSM().getEmployeeId(), employeeDetailsService.getEmployeeDetailsByemployeeId(meeting.getBusinessName().getEmployeeSM().getEmployeeId()).getName()),
						meeting.getBusinessName().getStatus(),
						meeting.getBusinessName().getBadDebtsStatus()), 
				meeting.getOwnerName(), 
				meeting.getShopName(), 
				meeting.getMobileNumber(), 
				meeting.getAddress(), 
				meeting.getMeetingVenue(), 
				meeting.getCancelReason(), 
				meeting.getMeetingStatus(), 
				meeting.getMeetingReview(), 
				meeting.getMeetingFromDateTime(), 
				meeting.getMeetingToDateTime(), 
				meeting.getAddedDateTime(), 
				meeting.getUpdatedDateTime(), 
				meeting.getCancelDateTime()));
		model.addAttribute("url", "reschedule_meeting");
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		
		return new ModelAndView("AddMeetingSchedule");
	}
	
	/**
	 * <pre>
	 * update reschedule status to pending meeting and create new meeting
	 * save meeting by existing business or new business
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return redirect fetch_scheduled_meeting
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/reschedule_meeting")
	public ModelAndView rescheduleMeeting(Model model,HttpSession session,HttpServletRequest request) throws ParseException {
		
		System.out.println("In Save meeting");
		
		long meetingId=Long.parseLong(request.getParameter("meetingId"));
		String date=request.getParameter("meetingDate");
		String fromTime=request.getParameter("meetingStartTime");
		String toTime=request.getParameter("meetingEndTime");
		String meetingVenue=request.getParameter("meetingVenue");
		
		Meeting meetingOld=meetingService.fetchMeetingByMeetingId(meetingId);
		meetingOld.setMeetingStatus(Constants.MEETING_RESCHEDULE);
		meetingOld.setUpdatedDateTime(new Date());
		meetingService.updateMeeting(meetingOld);
		
		Meeting meetingNew=new Meeting(meetingOld); 
		
		meetingNew.setMeeting(meetingOld);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		meetingNew.setMeetingFromDateTime(sdf.parse(date+" "+fromTime));
		meetingNew.setMeetingToDateTime(sdf.parse(date+" "+toTime));

		if(meetingVenue.equals("")){
			meetingNew.setMeetingVenue(null);
		}else{
			meetingNew.setMeetingVenue(meetingVenue);
		}
		
		meetingNew.setMeetingStatus(Constants.MEETING_PENDING);
		meetingNew.setAddedDateTime(new Date());
		meetingNew.setUpdatedDateTime(null);
		meetingNew.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		meetingService.saveMeeting(meetingNew);
		
		List<Department> departmentList=departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", departmentList);
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");		
		return new ModelAndView("redirect:/fetch_scheduled_meeting?departmentId=0&pickDate="+dateFormat.format(new Date()));
	}
}
