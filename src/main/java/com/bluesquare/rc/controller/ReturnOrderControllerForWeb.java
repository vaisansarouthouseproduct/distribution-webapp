/**
 * 
 */
package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProductDetailsP;
import com.bluesquare.rc.entities.ReturnOrderProductP;
import com.bluesquare.rc.models.ReturnOrderReportModel;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.ReturnOrderProductDetailsReport;
import com.bluesquare.rc.rest.models.PReturnOrderDetailsModel;
import com.bluesquare.rc.rest.models.PReturnOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.PReturnOrderProductDetailsModel;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ReturnOrderService;

/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * 1.returnOrderReport
 * 2.returnOrderDetailsByReturnOrderId
 * 3.reIssueOrderReport
 * 4.reIssueOrderDetailsByReturnOrderId
 *</pre>
 */
@Controller
public class ReturnOrderControllerForWeb {

	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	/**
	 * <pre>
	 * return order shown by range,startDate,endDate
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return ReturnItemReport.jsp
	 */ 
	@Transactional 	@RequestMapping("/returnOrderReport")
	public ModelAndView returnOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Return Order Report");
		
		
			String filter=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");
		  
		List<ReturnOrderReportModel> returnOrderReportModelList= returnOrderService.fetchReturnOrderProductList(filter,startDate,endDate);
		model.addAttribute("returnOrderReportModelList", returnOrderReportModelList);
		
		return new ModelAndView("ReturnItemReport");
	}
	/**
	 * <pre>
	 * fetch return order details by return order id
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return ReturnItemDetails.jsp
	 */
	@Transactional 	@RequestMapping("/returnOrderDetailsByReturnOrderId")
	public ModelAndView returnOrderDetailsByReturnOrderId(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Return Order Details");
		
		ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(request.getParameter("returnOrderId"));
		EmployeeDetails employeeDetails= employeeDetailsService.getEmployeeDetailsByemployeeId(returnOrderProduct.getOrderDetails().getEmployeeSM().getEmployeeId());
		ReturnOrderReportModel returnOrderReportModel=new ReturnOrderReportModel(
				returnOrderProduct.getReturnOrderProductId(),
				returnOrderProduct.getOrderDetails().getOrderId(), 
				returnOrderProduct.getOrderDetails().getBusinessName().getShopName(), 
				employeeDetails.getEmployeeDetailsId(), 
				employeeDetails.getName(), 
				returnOrderProduct.getOrderDetails().getBusinessName().getArea().getName(), 
				returnOrderProduct.getOrderDetails().getBusinessName().getArea().getRegion().getName(), 
				returnOrderProduct.getOrderDetails().getBusinessName().getArea().getRegion().getCity().getName(),
				returnOrderProduct.getTotalQuantity(),
				returnOrderProduct.getTotalAmount(), 
				returnOrderProduct.getTotalAmountWithTax(), 
				returnOrderProduct.getReturnOrderProductDatetime(), 
				returnOrderProduct.getReIssueStatus(),
				returnOrderProduct.getOrderDetails().getBusinessName().getAddress(),
				returnOrderProduct.getOrderDetails().getOrderDetailsAddedDatetime());		
		
		model.addAttribute("returnOrderReportModel",returnOrderReportModel );
		
		
		List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsByReturnOrderProductId(request.getParameter("returnOrderId"));
		List<ReturnOrderProductDetailsReport> returnOrderProductDetailsReportList=new ArrayList<>();
		for(ReturnOrderProductDetails returnOrderProductDetails: returnOrderProductDetailsList){
			OrderUsedProduct product=returnOrderProductDetails.getProduct();
			Product product2=product.getProduct();
			returnOrderProductDetailsReportList.add(new ReturnOrderProductDetailsReport(
					new ProductModel(
							product.getProductId(), 
							product.getProductName(), 
							product.getProductCode(), 
							new CategoriesModel(
									product.getCategories().getCategoryId(), 
									product.getCategories().getCategoryName(), 
									"", 
									product.getCategories().getHsnCode(), 
									product.getCategories().getCgst(), 
									product.getCategories().getSgst(), 
									product.getCategories().getIgst(), 
									null, 
									null), 
							new BrandModel(
									product.getBrand().getBrandId(), 
									product.getBrand().getName(), 
									null, 
									null), 
							product.getRate(), 
							/*product.getProductContentType(),*/ 
							"", 
							product.getThreshold(), 
							product.getCurrentQuantity(), 
							product.getDamageQuantity(), 
							0,
							null, 
							null,
							"",
							new ProductModel(
									product2.getProductId(), 
									product2.getProductName(), 
									product2.getProductCode(), 
									new CategoriesModel(
											product2.getCategories().getCategoryId(), 
											product2.getCategories().getCategoryName(), 
											"", 
											product2.getCategories().getHsnCode(), 
											product2.getCategories().getCgst(), 
											product2.getCategories().getSgst(), 
											product2.getCategories().getIgst(), 
											null, 
											null), 
									new BrandModel(
											product2.getBrand().getBrandId(), 
											product2.getBrand().getName(), 
											null, 
											null), 
									product2.getRate(), 
									/*product.getProductContentType(),*/ 
									"", 
									product2.getThreshold(), 
									product2.getCurrentQuantity(), 
									product2.getDamageQuantity(), 
									0,
									null, 
									null,
									"",
									null)), 
					returnOrderProductDetails.getSellingRate(), 
					returnOrderProductDetails.getId(), 
					returnOrderProductDetails.getReturnQuantity(), 
					returnOrderProductDetails.getIssuedQuantity(), 
					returnOrderProductDetails.getReturnTotalAmountWithTax(), 
					returnOrderProductDetails.getReason(), 
					returnOrderProductDetails.getType()));
		}
		model.addAttribute("returnOrderProductDetailsList", returnOrderProductDetailsReportList);
		return new ModelAndView("ReturnItemDetails");
	}
	/**
	 * <pre>
	 * fetch reissue order list by range,startDate,endDate
	 * </pre>
	 * @param model
	 * @param session
	 * @return ReIssueItemReport
	 */
	/*@Transactional 	@RequestMapping("/reIssueOrderReport")
	public ModelAndView reIssueOrderReport(Model model,HttpSession session) {
		model.addAttribute("pageName", "ReIssue Report");
		
		
		List<ReIssueOrderReport> reIssueOrderReportList= returnOrderService.fetchReIssueOrderReportList();
		model.addAttribute("reIssueOrderReportList", reIssueOrderReportList);
		
		return new ModelAndView("ReIssueItemReport");
	}*/
	/**
	 * <pre>
	 * fetch reissue order details list by ReturnOrderId
	 * </pre>
	 * @param model
	 * @param session
	 * @return ReIssueItemReport
	 */
	/*@Transactional 	@RequestMapping("/reIssueOrderDetailsByReturnOrderId")
	public ModelAndView rIssueOrderDetailsByReturnOrderId(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "ReIssue Order Details");
		
		
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=orderDetailsService.fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(Long.parseLong(request.getParameter("reIssueOrderId")));
		model.addAttribute("reIssueOrderProductDetailsList", reIssueOrderProductDetailsList);
		
		return new ModelAndView("ReIssueItemDetails");
	}*/
	
	/**
	 * <pre>
	 * fetch return order details list by ReturnOrderId for parmanent return
	 * </pre>
	 * @param model
	 * @param session
	 * @return ReIssueItemReport
	 */
	@Transactional 	@RequestMapping("/returnOrderDetailsByReturnOrderIdForPReturn")
	public ModelAndView returnOrderDetailsByReturnOrderIdForPReturn(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Permanent Return");
		model.addAttribute("returnOrderId", request.getParameter("returnOrderId"));
		return new ModelAndView("gkReturnOrderDetailsForPermnantReturn");
	}
	
	/**
	 * <pre>
	 * return order shown by range,startDate,endDate
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return ReturnItemReport.jsp
	 */ 
	@Transactional 	@RequestMapping("/permanentReturnOrderReport")
	public ModelAndView permanentReturnOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Permanent Return Order Report");
	
		String filter=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		  
		List<PReturnOrderDetailsModel> pReturnOrderDetailsModelList= orderDetailsService.permanentReturnOrderReport(filter,startDate,endDate);
		model.addAttribute("returnItemReportMainList", pReturnOrderDetailsModelList);
		
		return new ModelAndView("gkPermanentReturnOrdersReport");
	}
	
	/**
	 * <pre>
	 * fetch return order product details by return order id
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return ReturnItemDetails.jsp
	 */
	@Transactional 	@RequestMapping("/permanentReturnOrderDetailsByReturnOrderId")
	public ModelAndView permanentReturnOrderDetailsByReturnOrderId(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Permanent Return Order Details");
		 
		ReturnOrderProductP returnOrderProductP=orderDetailsService.fetchReturnOrderProductPByReturnOrderProductId(request.getParameter("returnOrderProductId"));
		List<ReturnOrderProductDetailsP> pReturnOrderProductDetailsModelList=orderDetailsService.fetchReturnOrderProductDetailsPListByReturnOrderProductPkId(request.getParameter("returnOrderProductId"));
		
		List<PReturnOrderProductDetailsModel> returnOrderProductDetailsModelList=new ArrayList<>();
		long srno=1;
		for(ReturnOrderProductDetailsP returnOrderProductDetailsP : pReturnOrderProductDetailsModelList){
			returnOrderProductDetailsModelList.add(new PReturnOrderProductDetailsModel(
					srno, 
					returnOrderProductDetailsP.getProduct().getProductName(), 
					returnOrderProductDetailsP.getIssuedQuantity(),
					returnOrderProductDetailsP.getReturnQuantity(), 
					returnOrderProductDetailsP.getReturnTotalAmountWithTax(),
					returnOrderProductDetailsP.getType(),
					returnOrderProductDetailsP.getReason()));
			srno++;
		}
		
		EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnOrderProductP.getEmployee().getEmployeeId());
		
		PReturnOrderProductDetailsListModel pReturnOrderProductDetailsListModel=new PReturnOrderProductDetailsListModel();
		pReturnOrderProductDetailsListModel.setReturnOrderDetailsModel(new PReturnOrderDetailsModel(
				0,
				returnOrderProductP.getReturnOrderProductId(), 
				returnOrderProductP.getOrderDetails().getOrderId(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getShopName(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getAddress(),
				employeeDetails.getName(), 
				employeeDetails.getAddress(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getContact().getMobileNumber(),
				employeeDetails.getEmployeeDetailsId(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getArea().getName(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getName(), 
				returnOrderProductP.getOrderDetails().getBusinessName().getArea().getRegion().getCity().getName(), 
				returnOrderProductP.getTotalQuantity(), 
				returnOrderProductP.getTotalAmount(), 
				returnOrderProductP.getTotalAmountWithTax()-returnOrderProductP.getTotalAmount(), 
				returnOrderProductP.getTotalAmountWithTax(), 
				returnOrderProductP.getOrderDetails().getOrderDetailsAddedDatetime(), 
				returnOrderProductP.getReturnOrderProductDatetime()));
		pReturnOrderProductDetailsListModel.setReturnOrderProductDetailsModelList(returnOrderProductDetailsModelList);
		
		model.addAttribute("pReturnOrderProductDetailsListModel",pReturnOrderProductDetailsListModel);
		
		return new ModelAndView("gkPermanentReturnOrdersReportDetails");
	}
}
