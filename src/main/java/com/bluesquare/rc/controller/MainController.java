package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.models.CollectionReportPaymentDetails;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.SelectedAccess;
/**
 * <pre>
 * @author Sachin Pawar 19-05-2018 Code Documentation
 * API End Points
 * 1./ -> index
 * 2.login
 * 3.loginEmployee
 * 4.logoutEmployee
 * </pre>
 */
@Controller
public class MainController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	CompanyService companyService; 
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	InventoryService inventoryService;
	
	
	/**
	 * index chart page showing after login
	 * @param session
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/")
	public ModelAndView indexPage(HttpSession session,Model model) 
	{		
		System.out.println("in main controller");
		model.addAttribute("pageName", "Index");
		
		model.addAttribute("msg",session.getAttribute("msg"));
		session.setAttribute("msg", "");
		
		//ChatRegistration.chatRegistration("BS_1", "BS_1"); 
		
		return new ModelAndView("Index");
		//return new ModelAndView("gkOrderDetailsForIssue");
	}
	
	/**
	 * login page showing
	 * @param session
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/login")
	public ModelAndView login(HttpSession session,Model model) 
	{
		
		System.out.println("in login controller");
		//return new ModelAndView("home");
		return new ModelAndView("login");
		
	}
	/**
	 * login for GateKeeper,Company,Admin
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/loginEmployee")
	public ModelAndView loginEmployee(Model model,HttpServletRequest request,HttpSession session) 
	{
		System.out.println("in loginEmployee controller");
		//get Company and Employee basis of request userId and password
		Company company=companyService.validateCompany(request.getParameter("userId"), request.getParameter("password"));
		Employee employee=employeeService.validate(request.getParameter("userId"), request.getParameter("password"));
		if(employee!=null)
		{
			if(employee.getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
			{
				//logged employee details store in session
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
				session.setAttribute("employeeDetails", employeeDetails);			
				
				//logged employee city,state,area stored in session
				SelectedAccess selectedAccess=setSessionSelectedLocations(employeeDetails.getEmployeeDetailsId(),employeeDetails.getEmployee().getCompany().getCompanyId(),Constants.GATE_KEEPER_DEPT_NAME);				
				session.setAttribute("selectedAccess", selectedAccess);
				
				Long companyId=employee.getCompany().getCompanyId();
				List<Long> companyIdList=new ArrayList<>();
				companyIdList.add(companyId);

				//logged user name store in session
				session.setAttribute("loginName", employeeDetails.getName());
				
				//logged employee company name
				session.setAttribute("sessionCopamnyName", employee.getCompany().getCompanyName());
				
				//logged employee company list store in session
				session.setAttribute("selectedCompanyIds", companyIdList);
				
				//logged employee dept type store in session
				session.setAttribute("loginType", employee.getDepartment().getName());
				
				List<Branch> branchList=employeeService.fetchBranchListByEmployeeId(employee.getEmployeeId());				
				List<Long> branchIds=new ArrayList<Long>();
				int i=0;
				String selectedBranchName="";
				for(Branch branch : branchList){
					if(i==0){
						selectedBranchName=branch.getName();
					}
					branchIds.add(branch.getBranchId());
					i++;
				}
				session.setAttribute("selectedBranchIds", branchIds);
				
				session.setAttribute("companyBranchSelect", true);
				
				setNotificationCountInSession(session);
					
				if(branchList.size()==1){
					
					session.setAttribute("selectedBranchName", selectedBranchName);
					return new ModelAndView("redirect:/");
				}else{
					//set branches	
					session.setAttribute("branchList", branchList);
					
					session.setAttribute("companyBranchSelect", true);
					session.setAttribute("showNavbar", false);
					
					return new ModelAndView("redirect:/companySettingInitial");
				}
				
			}else if(employee.getDepartment().getName().equals(Constants.ADMIN)){
				
				//-start-//
				List<Long> companyIdList=new ArrayList<>();
				companyIdList.add((long)-1);
				session.setAttribute("sessionCopamnyName", "Co. Not Selected");
				session.setAttribute("selectedCompanyIds", companyIdList);
				//-end-//
				
				//set branch
				List<Long> branchIds=new ArrayList<Long>();
				branchIds.add((long)-1);
				session.setAttribute("selectedBranchIds", branchIds);
				
				SelectedAccess selectedAccess=setSessionSelectedLocations(employee.getEmployeeId(),employee.getCompany().getCompanyId(),Constants.ADMIN);				
				session.setAttribute("selectedAccess", selectedAccess);
				
				//EmployeeDetails employeeDetails=new EmployeeDetails();
				session.setAttribute("employeeDetails", null);
				
				session.setAttribute("loginName", "Admin");
				
				session.setAttribute("loginType", employee.getDepartment().getName());
				
				session.setAttribute("showNavbar", false);
				session.setAttribute("companyBranchSelect", false);
				
				List<Country> countryList=countryService.fetchCountryListForWebApp();
				if(countryList==null){
					return new ModelAndView("redirect:/");
				}else{
					setNotificationCountInSession(session);
					return new ModelAndView("redirect:/companySettingInitial");
				}
				
			}else{
				session.invalidate();
				model.addAttribute("validateMsg", "Login failed");
				return new ModelAndView("login");
			}
		}else if(company!=null){
			
			
			Long companyId=company.getCompanyId();
			List<Long> companyIdList=new ArrayList<>();
			companyIdList.add(companyId);

			//logged company details store in session
			session.setAttribute("companyDetails", company);
			
			
			
			//logged company name store in session
			session.setAttribute("sessionCopamnyName", company.getCompanyName());
			
			//logged company id store in session
			session.setAttribute("selectedCompanyIds", companyIdList);
			
			//logged company all city , area, region store in session
			SelectedAccess selectedAccess=setSessionSelectedLocations(0,0,Constants.COMPANY_ADMIN);				
			session.setAttribute("selectedAccess", selectedAccess);
			
			//logged company name store in session
			session.setAttribute("loginName", company.getCompanyName());
			
			//logged company type store in session
			session.setAttribute("loginType", Constants.COMPANY_ADMIN);
			
			//set branches
			List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(companyId);				
			session.setAttribute("branchList", branchList);
			
			//set branch
			List<Long> branchIds=new ArrayList<Long>();
			
			
			session.setAttribute("companyBranchSelect", true);
			session.setAttribute("showNavbar", false);
			//setNotificationCountInSession(session);
			if(branchList.size()==1){
				session.setAttribute("selectedBranchName", branchList.get(0).getName());
				branchIds.add(branchList.get(0).getBranchId());
				session.setAttribute("selectedBranchIds",branchIds );
				setNotificationCountInSession(session);
				return new ModelAndView("redirect:/");
			}else{
				branchIds.add((long)-1);
				session.setAttribute("selectedBranchIds", branchIds);
				setNotificationCountInSession(session);
				return new ModelAndView("redirect:/companySettingInitial");			
			}
			

			
		}else{
			session.invalidate();
			model.addAttribute("validateMsg", "Login failed");
			return new ModelAndView("login");
		}
		
	}
	
	/**
	 * log out logged user and make session clear
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/logoutEmployee")
	public ModelAndView logoutEmployee(HttpServletRequest request,HttpSession session) 
	{
		session.invalidate();
		
		return new ModelAndView("redirect:/");
	}
	
	/***
	 * store area,city,region according logged user
	 * @param employeeId 
	 * @param uesrType : type of user i.e GK,DB,SM,Other
	 * @return
	 */
	public SelectedAccess setSessionSelectedLocations(long employeeId,long companyId,String uesrType){
		
		if(uesrType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			//branch setting data All
			List<Object[]> locationIdsList = employeeDetailsService.fetchEmployeeLocationIdsDetails(employeeId,companyId);
			
			/*Set<Long> countryIdSet=new HashSet<>();
			Set<Long> stateIdSet=new HashSet<>();*/
			Set<Long> cityIdSet=new HashSet<>();
			Set<Long> regionIdSet=new HashSet<>();
			Set<Long> areaIdSet=new HashSet<>();
			
			/*for(Area employeeArea: areaList)
			{
				countryIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getCountry().getCountryId());
				stateIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getStateId());
				cityIdSet.add(employeeArea.getRegion().getCity().getCityId());
				regionIdSet.add(employeeArea.getRegion().getRegionId());
				areaIdSet.add(employeeArea.getAreaId()); 
			}*/
			
			for(Object[] locationIds: locationIdsList)
			{
				/*Integer[] locIds = new Integer[locationIds.length];
				System.arraycopy(locationIds, 0, locIds, 0, locationIds.length);*/
				
				/*countryIdSet.add(area.getRegion().getCity().getState().getCountry().getCountryId());
				stateIdSet.add(area.getRegion().getCity().getState().getStateId());*/
				cityIdSet.add((Long)locationIds[2]);
				regionIdSet.add((Long)locationIds[1]);
				areaIdSet.add((Long)locationIds[0]);
			}
			
			List<Long> areaIdList=new ArrayList<>();
			List<Long> regionIdList=new ArrayList<>();
			List<Long> cityIdList=new ArrayList<>();
			List<Long> stateIdList=new ArrayList<>();
			List<Long> countryIdList=new ArrayList<>();
			
			areaIdList.addAll(areaIdSet);
			regionIdList.addAll(regionIdSet);
			cityIdList.addAll(cityIdSet);
			/*stateIdList.addAll(stateIdSet);
			countryIdList.addAll(countryIdSet);	*/
			
			return new SelectedAccess(areaIdList, regionIdList, cityIdList, stateIdList, countryIdList);
		} else {
			
			//areaId,regionId,cityId,stateId,countryId
			List<Object[]> locationIdsList=areaService.fetchLocationIdsForWebApp();
			
			/*Set<Long> countryIdSet=new HashSet<>();
			Set<Long> stateIdSet=new HashSet<>();*/
			Set<Long> cityIdSet=new HashSet<>();
			Set<Long> regionIdSet=new HashSet<>();
			Set<Long> areaIdSet=new HashSet<>();
			
			if(locationIdsList!=null){
				for(Object[] locationIds: locationIdsList)
				{
					/*Integer[] locIds = new Integer[locationIds.length];
					System.arraycopy(locationIds, 0, locIds, 0, locationIds.length);*/
					
					/*countryIdSet.add(area.getRegion().getCity().getState().getCountry().getCountryId());
					stateIdSet.add(area.getRegion().getCity().getState().getStateId());*/
					cityIdSet.add((Long)locationIds[2]);
					regionIdSet.add((Long)locationIds[1]);
					areaIdSet.add((Long)locationIds[0]);
				}
			}
			
			List<Long> areaIdList=new ArrayList<>();
			List<Long> regionIdList=new ArrayList<>();
			List<Long> cityIdList=new ArrayList<>();
			List<Long> stateIdList=new ArrayList<>();
			List<Long> countryIdList=new ArrayList<>();
			
			areaIdList.addAll(areaIdSet);
			regionIdList.addAll(regionIdSet);
			cityIdList.addAll(cityIdSet);
		/*	stateIdList.addAll(stateIdSet);
			countryIdList.addAll(countryIdSet);	*/
			
			return new SelectedAccess(areaIdList, regionIdList, cityIdList, stateIdList, countryIdList);
			
		}
	}
	
	
	public void setNotificationCountInSession(HttpSession session){
		// To Show the pending Payment Order Count in notification
		 List<CollectionReportPaymentDetails> collectionReportPaymentDetailsList=paymentService.getPendingPaymentList();
		 long pendingCounterOrderPaymentCount=0;
		 if(collectionReportPaymentDetailsList!=null){
			 pendingCounterOrderPaymentCount  =collectionReportPaymentDetailsList.size();
		 }
		 
		
		 //to show the Product under threshold count
			List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
			List<InventoryProduct> inventoryProductsListNew=new ArrayList<>();
			if(inventoryProductsList!=null){
				long srno=1;		
				for(InventoryProduct inventoryProduct :inventoryProductsList)
				{
					ProductModel product=inventoryProduct.getProduct();
					if(product.getCurrentQuantity()<=product.getThreshold())
					{
						inventoryProduct.setSrno(srno++);
						inventoryProductsListNew.add(inventoryProduct);
					}
				}
			}
			
			
			
			session.setAttribute("productUnderThesoldCount", inventoryProductsListNew.size());
			session.setAttribute("pendingCounterOrderPaymentCount", pendingCounterOrderPaymentCount);
			
			
	}
	
}

