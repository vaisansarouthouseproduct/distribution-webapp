package com.bluesquare.rc.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.models.GstBillReport;
import com.bluesquare.rc.models.OrderDetailsListOfBusiness;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.SalesReportModel;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.responseEntities.SupplierOrderDetailsModel;
import com.bluesquare.rc.responseEntities.SupplierOrderModel;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.SupplierOrderService;
import com.bluesquare.rc.service.SupplierService;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation   
 * 1.invoiceDownload
 * 2.orderBook  		-> supplier
 * 3.editOrderBook
 * 4.deleteSupplierOrder
 * 5.showSupplierOrderReport
 * 6.fetchSupplierOrderDetails
 * 7.showOrderReportByBusinessNameId
 * 8.showOrderReportByEmployeeSMId
 * 9.showOrderReport
 * 10.cancelOrderReport
 * 11.orderProductDetailsListForWebApp
 * 12.fetchLast24HoursOrdersForEdit
 * 13.fetchProductBySupplierOrderId
 * 14.editSupplierOrderAjax
 * 15.fetchOrderDetailsListOfBusinessByBusinessNameId
 * 16.sendSMSTOShopsUsingOrderId
 * 17.salesReport
 * 18.fetchGstBillReport
 * </pre>
 */
@Controller
public class OrderController {

	@Autowired
	SupplierOrderService supplierOrderService; 
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	SupplierOrder supplierOrder;
	
	@Autowired
	SupplierOrderDetails supplierOrderDetails;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	/*@Transactional 	@RequestMapping("/invoiceDownload")
	public ModelAndView invoiceDownload(Model model,HttpSession session,HttpServletResponse response,HttpServletRequest request) throws DocumentException, IOException {
		
		String filePath="/resources/trial.pdf";
    	
    	ServletContext context = request.getServletContext();
        String appPath = context.getRealPath("");
        System.out.println("appPath = " + appPath);
 
        // construct the complete absolute path of the file
        String fullPath = appPath + filePath;      
        File downloadFile = new File(fullPath);
		File dFile;
		long companyId;
		try {
			companyId = Long.parseLong(request.getParameter("companyId"));
		} catch (Exception e) {
			companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
		}
		BillPrintDataModel billPrintDataModel=orderDetailsService.fetchBillPrintData(request.getParameter("orderId"),companyId);
			dFile = InvoiceGenerator.generateInvoicePdf(billPrintDataModel,fullPath);
		
		 // get your file as InputStream
	      InputStream is = new FileInputStream(dFile);
	      // copy it to response's OutputStream
	      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
	      response.flushBuffer();
		
		return null;
	}*/
	
	
	
	/**
	 * <pre>
	 * supplier order save and 
	 * send sms send to supplier 
	 * with product name and quantity
	 * </pre>
	 * @param request
	 * @param session
	 * @return success/failed
	 */
	@Transactional 	@RequestMapping("/orderBook")
	public @ResponseBody String orderBook(HttpServletRequest request,HttpSession session) {
		System.out.println("inside order book");
		try {
		/*	Map<Long,String[]> orderCartMap=(Map<Long,String[]>)session.getAttribute("supplierOrderCartMap");
			String productIdList="";
			for(Map.Entry<Long,String[]> entry : orderCartMap.entrySet())
			{
				productIdList=productIdList+entry.getValue()[0]+"-"+entry.getValue()[1]+"-"+entry.getValue()[2]+",";
      		}
			productIdList=productIdList.substring(0, productIdList.length()-1);*/
			supplierOrderService.saveSupplierOrder(request.getParameter("productIdList"));
		} catch (Exception e) {
			return "Failed";
		}
		
		return "Success"; 
	}
	/**
	 * <pre>
	 * supplier order edit and 
	 * send sms send to supplier 
	 * with product name and quantity
	 * </pre>
	 * @param request
	 * @param session
	 * @return success/failed
	 */
	@Transactional 	@RequestMapping("/editOrderBook")
	public @ResponseBody String editOrderBook(HttpServletRequest request) {
		System.out.println("inside editOrderBook");
		try {
			supplierOrder=supplierOrderService.fetchSupplierOrder(request.getParameter("supplierOrderId"));
			supplierOrderService.editSupplierOrder(request.getParameter("productIdList"),supplierOrder);
		} catch (Exception e) {
			return "Failed";
		}
		
		return "Success"; 
	}
	/**
	 * <pre>
	 * delete supplier order 
	 * send sms to supplier for cancel order
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return last24OrderDetailsForEdit.jsp
	 */
	@Transactional 	@RequestMapping("/deleteSupplierOrder")
	public ModelAndView deleteSupplierOrder(HttpServletRequest request,Model model,HttpSession session) {
				
		supplierOrderService.deleteSupplierOrder(request.getParameter("supplierOrderId"));
		
		List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour("CurrentDay","","");
		List<SupplierOrderModel> supplierOrderModelList=new ArrayList<>();
		if(supplierOrdersList!=null){
			for(SupplierOrder supplierOrder : supplierOrdersList){
				supplierOrderModelList.add(new SupplierOrderModel(
						supplierOrder.getSupplierOrderPkId(), 
						supplierOrder.getSupplierOrderId(), 
						supplierOrder.getTotalAmount(),
						supplierOrder.getTotalAmountWithTax(), 
						supplierOrder.getTotalQuantity(), 
						supplierOrder.getSupplierOrderDatetime(), 
						supplierOrder.getSupplierOrderUpdateDatetime(), 
						supplierOrder.isStatus()));
				
			}
		}
		model.addAttribute("last24Order", supplierOrderModelList);
		
		return new ModelAndView("last24OrderDetailsForEdit");
	}
	
	//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
	
	/*@RequestMapping("/showOrderReport")
	public ModelAndView showOrderReport(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Order Report");
		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReport();
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		return new ModelAndView("orderReport");
	}*/
	/**
	 * <pre>
	 * show supplier order report 
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return SupplierOrderView.jsp
	 */
	@Transactional 	@RequestMapping("/showSupplierOrderReport")
	public ModelAndView showSupplierOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Supplier Orders");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
				
		List<SupplierOrder> supplierOrderLists= supplierOrderService.fetchSupplierOrderList(range, startDate, endDate);
		List<SupplierOrderModel> supplierOrderModelList=new ArrayList<>();
		if(supplierOrderLists!=null){
			for(SupplierOrder supplierOrder : supplierOrderLists){
				supplierOrderModelList.add(new SupplierOrderModel(
						supplierOrder.getSupplierOrderPkId(), 
						supplierOrder.getSupplierOrderId(), 
						supplierOrder.getTotalAmount(),
						supplierOrder.getTotalAmountWithTax(), 
						supplierOrder.getTotalQuantity(), 
						supplierOrder.getSupplierOrderDatetime(), 
						supplierOrder.getSupplierOrderUpdateDatetime(), 
						supplierOrder.isStatus()));
				
			}
		}
		model.addAttribute("supplierOrderLists", supplierOrderModelList);
		
		return new ModelAndView("SupplierOrderView");
	}
	/**
	 * <pre>
	 * supplier order product details by  supplierOrderId
	 * </pre>
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchSupplierOrderDetails")
	public @ResponseBody List<SupplierOrderDetailsModel> showSupplierOrderDetails(HttpServletRequest request,Model model) {
		
		List<SupplierOrderDetails> supplierOrderDetailsLists= supplierOrderService.fetchSupplierOrderDetailsList(request.getParameter("supplierOrderId"));
		List<SupplierOrderDetailsModel> supplierOrderDetailsModelList=new ArrayList<>();
		if(supplierOrderDetailsLists!=null){
			for(SupplierOrderDetails supplierOrderDetails : supplierOrderDetailsLists){
				OrderUsedProduct product=supplierOrderDetails.getProduct();
				Product product2=product.getProduct();
				supplierOrderDetailsModelList.add(new SupplierOrderDetailsModel(
						supplierOrderDetails.getSupplierOrderDetailsId(), 
						new SupplierModel(
								supplierOrderDetails.getSupplier().getSupplierPKId(), 
								supplierOrderDetails.getSupplier().getSupplierId(), 
								supplierOrderDetails.getSupplier().getName(), 
								supplierOrderDetails.getSupplier().getContact()), 
						new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												"", 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												null, 
												null), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												null, 
												null), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										"", 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										0,
										null, 
										null,
										"",
										new ProductModel(
												product2.getProductId(), 
												product2.getProductName(), 
												product2.getProductCode(), 
												new CategoriesModel(
														product2.getCategories().getCategoryId(), 
														product2.getCategories().getCategoryName(), 
														"", 
														product2.getCategories().getHsnCode(), 
														product2.getCategories().getCgst(), 
														product2.getCategories().getSgst(), 
														product2.getCategories().getIgst(), 
														null, 
														null), 
												new BrandModel(
														product2.getBrand().getBrandId(), 
														product2.getBrand().getName(), 
														null, 
														null), 
												product2.getRate(), 
												/*product.getProductContentType(),*/ 
												"", 
												product2.getThreshold(), 
												product2.getCurrentQuantity(), 
												product2.getDamageQuantity(), 
												0, 
												null, 
												null,
												"",
												null)), 
						supplierOrderDetails.getQuantity(), 
						supplierOrderDetails.getSupplierRate(), 
						supplierOrderDetails.getTotalAmount(), 
						supplierOrderDetails.getTotalAmountWithTax()));
				
			}
		}
		
		return supplierOrderDetailsModelList;
	}
	/**
	 * <pre> 
	 * show SalesMan-order/Counter-Order report of business by businessNameId by range,startDate,endDate
	 * here salesman-order only show until delivered status  
	 * </pre> 
	 * @param request
	 * @param model
	 * @param session
	 * @return orderReport
	 */
	@Transactional 	@RequestMapping("/showOrderReportByBusinessNameId")
	public ModelAndView showOrderReportByBusinessNameId(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Report By Business");
		
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
		  
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReportByBusinessNameId(request.getParameter("businessNameId"), range, startDate, endDate);
		List<OrderReportList> counterOrderReportList=counterOrderService.showCounterOrderReportByBusinessNameId(request.getParameter("businessNameId"), range, startDate, endDate);
		
		//counterOrders add in salesman orders
		if(orderReportLists!=null && counterOrderReportList!=null){
			orderReportLists.addAll(counterOrderReportList);
		}else if(orderReportLists==null && counterOrderReportList!=null){
			orderReportLists=new ArrayList<>();
			orderReportLists.addAll(counterOrderReportList);
		}
		if(orderReportLists!=null){	
			//reset list by orderDate desc
			Collections.sort(orderReportLists,new OrderDateComparator());
			List<OrderReportList> orderReportListsTemp=new ArrayList<>();
			int i=1;
			for(OrderReportList orderReportList: orderReportLists){
				orderReportList.setSrno(i);
				i++;
				orderReportListsTemp.add(orderReportList);
			}
			orderReportLists=orderReportListsTemp;
		}
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		model.addAttribute("businessNameId", request.getParameter("businessNameId"));
		model.addAttribute("url", "showOrderReportByBusinessNameId");

		session.setAttribute("lastUrl","showOrderReportByBusinessNameId?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	//order date wise filter
	public class OrderDateComparator implements Comparator<OrderReportList> {
	    public int compare(OrderReportList p, OrderReportList q) {
	        if (p.getOrderDate().before(q.getOrderDate())) {
	            return 1;
	        } else if (p.getOrderDate().after(q.getOrderDate())) {
	            return -1;
	        } else {
	            return 0;
	        }        
	    }
	}
	/**
	 * <pre>
	 * order report by salesman id 
	 * for show how much business he did 
	 * filter in range,startDate,endDate
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return  orderReport
	 */
	@Transactional 	@RequestMapping("/showOrderReportByEmployeeSMId")
	public ModelAndView showOrderReportByemployeeSMId(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Report By Employee");
	
		  
		  String range=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReportByEmployeeSMId(request.getParameter("employeeSMId"), range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		model.addAttribute("employeeSMId", request.getParameter("employeeSMId"));
		model.addAttribute("url", "showOrderReportByEmployeeSMId");
		
		session.setAttribute("lastUrl","showOrderReportByEmployeeSMId?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	/**
	 * <pre>
	 * order report which order status only in (Delivered,Cancelled)
	 * area list for filter area wise also
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return orderReport
	 */
	@Transactional 	@RequestMapping("/showOrderReport")
	public ModelAndView showOrderReport(Model model,HttpServletRequest request,HttpSession session){		
		model.addAttribute("pageName", "Order Report");
		
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReport( range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		model.addAttribute("url", "showOrderReport");
		
		session.setAttribute("lastUrl","showOrderReport?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	/**
	 * <pre>
	 * order details show which status is Cancelled by range,startDate,endDate 
	 * area list for filter
	 * </pre> 
	 * @param model
	 * @param request
	 * @param session
	 * @return orderReport
	 */
	@Transactional 	@RequestMapping("/cancelOrderReport")
	public ModelAndView cancelOrderReport(Model model,HttpServletRequest request,HttpSession session){		
		model.addAttribute("pageName", "Cancel Order Report");
	
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.cancelOrderReport( range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		model.addAttribute("url", "cancelOrderReport");
		
		return new ModelAndView("orderReport");
	}
	/**
	 * <pre>
	 * salesman order details by orderId
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return OrderDetails
	 */
	@Transactional 	@GetMapping("/orderProductDetailsListForWebApp")
	public ModelAndView orderProductDetailsListForWebApp(Model model,HttpServletRequest request,HttpSession session){
		model.addAttribute("pageName", "Order Product Details");
		
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=orderDetailsService.orderProductDetailsListForWebApp(request.getParameter("orderDetailsId"));
		model.addAttribute("orderProductDetailListForWebApps", orderProductDetailListForWebApps);
		
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
		{
			totalAmount+=orderProductDetailListForWebApp.getTotalAmount();
			totalAmountWithTax+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			totalQuantity+=orderProductDetailListForWebApp.getQuantity();
		}
		
		model.addAttribute("totalQuantity", totalQuantity);
		model.addAttribute("totalAmount", totalAmount);
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		
		return new ModelAndView("OrderDetails");
	}
	
	/*@Transactional 	@RequestMapping("/fetchLast24HoursOrders")
	public ModelAndView fetchLast24HoursOrders(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Order Details");
		
		
		System.out.println("in fetchLast24HoursOrders controller");
		
		String supplierId=request.getParameter("supplierId");
		//String fetchBy=request.getParameter("fetchBy");
		String filter=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		session.setAttribute("last_range",filter);
		session.setAttribute("last_startDate",startDate);
		session.setAttribute("last_endDate",endDate);
		
		if(fetchBy==null)
		{
			List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour(filter,startDate,endDate);
			model.addAttribute("last24Order", supplierOrdersList);
		}
		else
		{
			List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hourBySupplierId(supplierId);
			model.addAttribute("last24Order", supplierOrdersList);
		}
		model.addAttribute("fetchRecordType", filter);
		
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();		
		model.addAttribute("supplierList", supplierList);
		
		return new ModelAndView("last24OrderDetails");
	}*/
	/**
	 * <pre>
	 * fetch supplier order which under 24 hour for edit 
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return last24OrderDetailsForEdit.jsp
	 */
	@Transactional 	@RequestMapping("/fetchLast24HoursOrdersForEdit")
	public ModelAndView fetchLast24HoursOrdersForEdit(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in fetchLast24HoursOrdersForEdit controller");
		
		model.addAttribute("pageName", "Editable Supplier Order List");
		
		List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour("CurrentDay","","");
		List<SupplierOrderModel> supplierOrderModelList=new ArrayList<>();
		if(supplierOrdersList!=null){
			for(SupplierOrder supplierOrder : supplierOrdersList){
				supplierOrderModelList.add(new SupplierOrderModel(
						supplierOrder.getSupplierOrderPkId(), 
						supplierOrder.getSupplierOrderId(), 
						supplierOrder.getTotalAmount(),
						supplierOrder.getTotalAmountWithTax(), 
						supplierOrder.getTotalQuantity(), 
						supplierOrder.getSupplierOrderDatetime(), 
						supplierOrder.getSupplierOrderUpdateDatetime(), 
						supplierOrder.isStatus()));
				
			}
		}
		model.addAttribute("last24Order", supplierOrderModelList);
		
		return new ModelAndView("last24OrderDetailsForEdit");
	}
	/**
	 * <pre>
	 * fetch supplier product details by supplier order id
	 * </pre>
	 * @param model
	 * @param request
	 * @return SupplierOrderDetails list
	 */
	@Transactional 	@RequestMapping("/fetchProductBySupplierOrderId")
	public @ResponseBody List<SupplierOrderDetailsModel> fetchSupplierOrderDetailsList(Model model,HttpServletRequest request) {

		System.out.println("in fetchProductBySupplierOrderId controller");
		
		List<SupplierOrderDetails> supplierOrderDetailList=orderDetailsService.fetchSupplierOrderDetailsListBySupplierOrderId(request.getParameter("supplierOrderId"));
		List<SupplierOrderDetailsModel> supplierOrderDetailsModelList=new ArrayList<>();
		if(supplierOrderDetailList!=null){
			for(SupplierOrderDetails supplierOrderDetails : supplierOrderDetailList){
				OrderUsedProduct product=supplierOrderDetails.getProduct();
				Product product2=product.getProduct();
				supplierOrderDetailsModelList.add(new SupplierOrderDetailsModel(
						supplierOrderDetails.getSupplierOrderDetailsId(), 
						new SupplierModel(
								supplierOrderDetails.getSupplier().getSupplierPKId(), 
								supplierOrderDetails.getSupplier().getSupplierId(), 
								supplierOrderDetails.getSupplier().getName(), 
								supplierOrderDetails.getSupplier().getContact()), 
						new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												"", 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												null, 
												null), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												null, 
												null), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										"", 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										0,
										null, 
										null,
										"",
										new ProductModel(
												product2.getProductId(), 
												product2.getProductName(), 
												product2.getProductCode(), 
												new CategoriesModel(
														product2.getCategories().getCategoryId(), 
														product2.getCategories().getCategoryName(), 
														"", 
														product2.getCategories().getHsnCode(), 
														product2.getCategories().getCgst(), 
														product2.getCategories().getSgst(), 
														product2.getCategories().getIgst(), 
														null, 
														null), 
												new BrandModel(
														product2.getBrand().getBrandId(), 
														product2.getBrand().getName(), 
														null, 
														null), 
												product2.getRate(), 
												/*product.getProductContentType(),*/ 
												"", 
												product2.getThreshold(), 
												product2.getCurrentQuantity(), 
												product2.getDamageQuantity(), 
												0, 
												null, 
												null,
												"",
												null)), 
						supplierOrderDetails.getQuantity(), 
						supplierOrderDetails.getSupplierRate(), 
						supplierOrderDetails.getTotalAmount(), 
						supplierOrderDetails.getTotalAmountWithTax()));
				
			}
		}
		return supplierOrderDetailsModelList;
	}
	
	
	/**
	 * <pre>
	 * edit supplier order and send sms about changes
	 * </pre>
	 * @param model
	 * @param request
	 * @return SupplierOrderDetails list
	 */
	@Transactional 	@RequestMapping("/editSupplierOrderAjax")
	public @ResponseBody List<SupplierOrderDetailsModel> editSupplierOrderAjax(Model model,HttpServletRequest request) {

		System.out.println("in editSupplierOrder controller");
		
		String supplierOrderId=request.getParameter("supplierOrderId");
		
		List<SupplierOrderDetails> supplierOrderDetaillist=supplierOrderService.fetchSupplierOrderDetailsList(supplierOrderId);
		List<SupplierOrderDetailsModel> supplierOrderDetailsModelList=new ArrayList<>();
		if(supplierOrderDetaillist!=null){
			for(SupplierOrderDetails supplierOrderDetails : supplierOrderDetaillist){
				OrderUsedProduct product=supplierOrderDetails.getProduct();
				Product product2=product.getProduct();
				supplierOrderDetailsModelList.add(new SupplierOrderDetailsModel(
						supplierOrderDetails.getSupplierOrderDetailsId(), 
						new SupplierModel(
								supplierOrderDetails.getSupplier().getSupplierPKId(), 
								supplierOrderDetails.getSupplier().getSupplierId(), 
								supplierOrderDetails.getSupplier().getName(), 
								supplierOrderDetails.getSupplier().getContact()), 
						new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												"", 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												null, 
												null), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												null, 
												null), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										"", 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										0,
										null, 
										null,
										"",
										new ProductModel(
												product2.getProductId(), 
												product2.getProductName(), 
												product2.getProductCode(), 
												new CategoriesModel(
														product2.getCategories().getCategoryId(), 
														product2.getCategories().getCategoryName(), 
														"", 
														product2.getCategories().getHsnCode(), 
														product2.getCategories().getCgst(), 
														product2.getCategories().getSgst(), 
														product2.getCategories().getIgst(), 
														null, 
														null), 
												new BrandModel(
														product2.getBrand().getBrandId(), 
														product2.getBrand().getName(), 
														null, 
														null), 
												product2.getRate(), 
												/*product.getProductContentType(),*/ 
												"", 
												product2.getThreshold(), 
												product2.getCurrentQuantity(), 
												product2.getDamageQuantity(), 
												0, 
												null, 
												null,
												"",
												null)), 
						supplierOrderDetails.getQuantity(), 
						supplierOrderDetails.getSupplierRate(), 
						supplierOrderDetails.getTotalAmount(), 
						supplierOrderDetails.getTotalAmountWithTax()));
				
			}
		}
		return supplierOrderDetailsModelList;
	}
	/**
	 * <pre>
	 * fetch order details by business by businessNameId
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return orderdetailsbybusiness.jsp
	 */
	@Transactional 	@RequestMapping("/fetchOrderDetailsListOfBusinessByBusinessNameId")
	public ModelAndView fetchOrderDetailsListOfBusinessByBusinessNameId(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Order Details By Business");
		
		
		String businessNameId=request.getParameter("businessNameId");
		String range=request.getParameter("range");
		String fromDate=request.getParameter("startDate");
		String toDate=request.getParameter("endDate");
		
		OrderDetailsListOfBusiness orderDetailsListOfBusiness=orderDetailsService.fetchOrderDetailsListOfBusinessByBusinessNameId(businessNameId, fromDate, toDate, range);
		model.addAttribute("orderDetailsListOfBusiness", orderDetailsListOfBusiness);
		
	return new ModelAndView("orderdetailsbybusiness");
	}
	/**
	 * <pre>
	 * send sms to business 
	 * if one selected then send sms on given mobile number other wise send on registered mobile number
	 * </pre>
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/sendSMSTOShopsUsingOrderId")
	public @ResponseBody String sendSMSTOShopsUsingOrderId(HttpServletRequest request) {
		
		try {
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			orderDetailsService.sendSMSTOShopsUsingOrderId(shopsIds, smsText, mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	/**
	 * <pre>
	 * order sales report showing orders which  delivered
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return SalesReport
	 */
	@Transactional 	@RequestMapping("/salesReport")
	public ModelAndView salesReport(Model model,HttpServletRequest request,HttpSession session)
	{
		model.addAttribute("pageName", "Sales Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		List<SalesReportModel> salesManReportModelList=orderDetailsService.fetchSalesReportModel(startDate, endDate, range);
		model.addAttribute("salesManReportModelList", salesManReportModelList);
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		double totalAmount=0;
		long noOfOrders=0;
		if(salesManReportModelList!=null)
		{
			for(SalesReportModel salesManReportModel:salesManReportModelList)
			{
				totalAmount+=salesManReportModel.getOrderAmountWithTax();
				noOfOrders+=1;
			}
		}
		model.addAttribute("noOfOrders", noOfOrders);
		model.addAttribute("totalAmount", Double.parseDouble(decimalFormat.format(totalAmount)));
		return new ModelAndView("SalesReport");
	}
	/**
	 * <pre>
	 * fetch gst bill report by start date,end date,type
	 * </pre>
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchGstBillReport")
	public ModelAndView fetchGstBillReport(HttpServletRequest request,Model model) {
		
			model.addAttribute("pageName", "GST Report");
		
			String startDate=request.getParameter("startDate");		
			String endDate=request.getParameter("endDate");	
			String type=request.getParameter("type");	
			
			GstBillReport gstBillReport=null;
			if(type!=null)
			{
				gstBillReport=orderDetailsService.fetchGstBillReport(startDate, endDate,type);
			}
			model.addAttribute("gstBillReport", gstBillReport);
			model.addAttribute("type", type);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			
			return new ModelAndView("gstBillReport");
	}
	
	/**
	 * <pre> 
	 * show SalesMan-order/Counter-Order report of business by businessNameId for Bad Debts report
	 * </pre> 
	 * @param request
	 * @param model
	 * @param session
	 * @return orderReport
	 */
	@Transactional 	@RequestMapping("/showOrderReportByBusinessNameIdForBadDebts")
	public ModelAndView showOrderReportByBusinessNameIdForBadDebts(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Report By Business");
		String businessNameId=request.getParameter("businessNameId");
		String counterOrderId=request.getParameter("counterOrderId");
		if(!businessNameId.equals("NA")){
			List<OrderReportList> orderReportLists= orderDetailsService.showOrderReportByBusinessNameIdOfBadDebts(businessNameId);
			List<OrderReportList> counterOrderReportList=counterOrderService.showCounterOrderReportByBusinessNameIdForBadDebts(businessNameId);
			
			//counterOrders add in salesman orders
			if(orderReportLists!=null && counterOrderReportList!=null){
				orderReportLists.addAll(counterOrderReportList);
			}else if(orderReportLists==null && counterOrderReportList!=null){
				orderReportLists=new ArrayList<>();
				orderReportLists.addAll(counterOrderReportList);
			}
			if(orderReportLists!=null){	
				//reset list by orderDate desc
				Collections.sort(orderReportLists,new OrderDateComparator());
				List<OrderReportList> orderReportListsTemp=new ArrayList<>();
				int i=1;
				for(OrderReportList orderReportList: orderReportLists){
					orderReportList.setSrno(i);
					i++;
					orderReportListsTemp.add(orderReportList);
				}
				orderReportLists=orderReportListsTemp;
			}
			model.addAttribute("orderReportLists", orderReportLists);
		}else{
			List<OrderReportList> orderReportLists= counterOrderService.showCounterOrderReportByExternalCustomerCounterOrderIdForBadDebts(counterOrderId);
			model.addAttribute("orderReportLists", orderReportLists);
		}
		
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		model.addAttribute("businessNameId", request.getParameter("businessNameId"));
		model.addAttribute("counterOrderId", counterOrderId);
		model.addAttribute("url", "showOrderReportByBusinessNameIdForBadDebts");
		
		return new ModelAndView("orderReport");
	}
}
