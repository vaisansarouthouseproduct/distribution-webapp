package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.TargetAssign;
import com.bluesquare.rc.entities.TargetAssignTargets;
import com.bluesquare.rc.models.CommissionAssignAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.EmployeeIdsResponse;
import com.bluesquare.rc.models.TargetAssignDeleteRequest;
import com.bluesquare.rc.models.TargetAssignDetails;
import com.bluesquare.rc.models.TargetListsRequest;
import com.bluesquare.rc.models.TargetsAndEmployeeByDeptResponse;
import com.bluesquare.rc.models.TargetsSaveRequest;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.CommissionAssignService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.TargetAssignService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.WeeksFind;

/**
 * <pre>
 * author Sachin Pawar	02-06-2018 Code Documentation
 * API End Points:
 * 1. fetch_target_types_by_department/{departmentId}
 * 2. save_target_assign
 * 3. update_target_assign
 * 4. check_employee_target_assign
 * 5. fetch_target_assign_list
 * 6. delete_target_assign
 * 7. fetch_week_dates_by_year/{year}
 * 8. fetch_employee_target_period/{employeeId}
 * </pre>
 */

@RestController
public class TargetAssignController {

	@Autowired
	TargetAssignService targetAssignService;

	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	CommissionAssignService commissionAssignService;
	
	/**
	 * <pre>
	 * open add new target assign screen
	 * with department list
	 * </pre>
	 * @param model
	 * @param session
	 * @return new_target.jsp
	 */
	@Transactional 	@RequestMapping("/add_new_target")
	public ModelAndView addTargets(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Assign target");
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		model.addAttribute("mode", "add");
		
		return new ModelAndView("target_add_edit");
	}
	
	/**
	 * <pre>
	 * open add new target assign screen
	 * with department list
	 * </pre>
	 * @param model
	 * @param session
	 * @return new_target.jsp
	 */
	@Transactional 	@RequestMapping("/edit_target")
	public ModelAndView editTargetAssigned(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Update target assigned");
		
		String targetAssignId=request.getParameter("targetAssignId");
		
		List<TargetAssignTargets> targetAssignTargets=targetAssignService.fetchTargetAssignTargetsByTargetAssignId(Long.parseLong(targetAssignId));
		model.addAttribute("targetAssignTargets", targetAssignTargets);
		
		TargetAssign targetAssign=targetAssignService.fetchTargetAssignByTargetAssignId(Long.parseLong(targetAssignId));
		model.addAttribute("targetAssign", targetAssign);
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		model.addAttribute("mode","edit");
		
		return new ModelAndView("target_add_edit");
	}
	
	/**
	 * <pre>
	 * fetch commission assign  and employee list by department id
	 * </pre>
	 * @param departmentId
	 * @param request
	 * @param model
	 * @param session
	 * @return TargetTypesResponse
	 */
	@Transactional
	@RequestMapping("/fetchCommissionAssignAndEmployeeListByDepartment/{departmentId}")
	public ResponseEntity<CommissionAssignAndEmployeeByDeptResponse> fetchCommissionAssignAndEmployeeListByDepartment(@ModelAttribute("departmentId") long departmentId,HttpServletRequest request, Model model,HttpSession session) {
				
		CommissionAssignAndEmployeeByDeptResponse targetsAndEmployeeByDeptResponse = commissionAssignService.fetchTargetsAndEmployeeListByDepartmentId(departmentId);
		
		return new ResponseEntity<CommissionAssignAndEmployeeByDeptResponse>(targetsAndEmployeeByDeptResponse, HttpStatus.OK);
	}
	
	/**
	 * <pre>
	 * fetch target types and employee list by department id
	 * </pre>
	 * @param departmentId
	 * @param request
	 * @param model
	 * @param session
	 * @return TargetTypesResponse
	 */
	@Transactional
	@RequestMapping("/fetchTargetsAndEmployeeListByDepartment/{departmentId}")
	public ResponseEntity<TargetsAndEmployeeByDeptResponse> fetchTargetTypesByDepartment(@ModelAttribute("departmentId") long departmentId,HttpServletRequest request, Model model,HttpSession session) {
				
		TargetsAndEmployeeByDeptResponse targetsAndEmployeeByDeptResponse = targetAssignService.fetchTargetsAndEmployeeListByDepartmentId(departmentId);
		
		return new ResponseEntity<TargetsAndEmployeeByDeptResponse>(targetsAndEmployeeByDeptResponse, HttpStatus.OK);
	}
	
	/**
	 * <pre>
	 * save target assign current
	 * save target assign next period
	 * save target assign regular
	 * </pre>
	 * @param model
	 * @param session
	 * @return redirect:/fetch_manage_Target_assign
	 */
	@Transactional
	@RequestMapping(value="/save_target_assign")
	public ResponseEntity<BaseDomain> saveTargetAssign(@RequestBody TargetsSaveRequest targetsSaveRequest,HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("saving target assigned to employee");
		
		BaseDomain baseDomain = new BaseDomain();
		//Date fromDate = new Date(), toDate = new Date();

		Calendar calendarStartDate=Calendar.getInstance();
		calendarStartDate.add(Calendar.DAY_OF_MONTH, 1);
		
		Calendar calendarEndDate=Calendar.getInstance();
		calendarEndDate.add(Calendar.DAY_OF_MONTH, 1);
		
		for(int i=0; i<targetsSaveRequest.getEmployeeIds().length; i++){
			
			TargetAssign targetAssign=new TargetAssign();
			targetAssign.setAddedDatetime(new Date());
			targetAssign.setEmployeeDetails(employeeDetailsService.getEmployeeDetailsByemployeeId(Long.parseLong(targetsSaveRequest.getEmployeeIds()[i])));
						
			/*if(targetAssign.getTargetPeriod().equals(Constants.TARGET_PERIOD_DAILY)) {
				
				targetAssign.setFromDatetime(fromDate);
				targetAssign.setToDatetime(toDate);
				
			} else if (targetAssign.getTargetPeriod().equals(Constants.TARGET_PERIOD_WEEKLY)) {
				
				targetAssign.setFromDatetime(DateUtils.getCurrentWeekStartDate());
				targetAssign.setToDatetime(DateUtils.getCurrentWeekEndDate());
				
			} else if (targetAssign.getTargetPeriod().equals(Constants.TARGET_PERIOD_MONTHLY)) {
				
				targetAssign.setFromDatetime(DateUtils.getCurrentMonthStartDate());
				targetAssign.setToDatetime(DateUtils.getCurrentMonthLastDate());
				
			} else if (targetAssign.getTargetPeriod().equals(Constants.TARGET_PERIOD_YEARLY)) {
				
				targetAssign.setFromDatetime(DateUtils.getCurrentYearStartDate());
				targetAssign.setToDatetime(DateUtils.getCurrentYearLastDate());
			}*/
			
			targetAssignService.saveTargetAssign(targetAssign);	
				
			
			for(int j=0; j<targetsSaveRequest.getTargetWithValue().size(); j++){
				String targetList[]=targetsSaveRequest.getTargetWithValue().get(j);
				
				TargetAssignTargets targetAssignTargets=new TargetAssignTargets(); 
				
				targetAssignTargets.setTargetAssign(targetAssign);
				
				CommissionAssign commissionAssign=new CommissionAssign();
				commissionAssign.setCommissionAssignId(Long.parseLong(targetList[0]));
				
				targetAssignTargets.setCommissionAssign(commissionAssign);
				targetAssignTargets.setTargetValue(Double.parseDouble(targetList[2]));
				
				targetAssignService.saveTargetAssignedTargets(targetAssignTargets);
			}
		}		

		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	/**
	 * <pre>
	 * update target assign current
	 * update in regular if updateInRegular is Yes
	 * delete old targetAssignTargets and add new
	 * </pre>
	 * @param model
	 * @param session
	 * @return redirect:/fetch_manage_Target_assign
	 */
	@Transactional
	@RequestMapping(value="/update_target_assign")
	public ResponseEntity<BaseDomain> updateTargetAssign(@RequestBody TargetsSaveRequest targetsSaveRequest,HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("updating target assigned to employee");
		
		BaseDomain baseDomain = new BaseDomain();
		//Date fromDate = new Date(), toDate = new Date();

		TargetAssign targetAssign=targetAssignService.fetchTargetAssignByTargetAssignId(targetsSaveRequest.getTargetAssignedId());
		targetAssign.setUpdatedDatetime(new Date());
		targetAssign.setEmployeeDetails(employeeDetailsService.getEmployeeDetailsByemployeeId(Long.parseLong(targetsSaveRequest.getEmployeeIds()[0])));
		
		targetAssignService.updateTargetAssign(targetAssign);
				
		List<TargetAssignTargets> targetAssignTargetsList=new ArrayList<>(); 
		for(int j=0; j<targetsSaveRequest.getTargetWithValue().size(); j++){
			String targetList[]=targetsSaveRequest.getTargetWithValue().get(j);
			
			TargetAssignTargets targetAssignTargets=new TargetAssignTargets(); 
			
			targetAssignTargets.setTargetAssign(targetAssign);
			
			CommissionAssign commissionAssign=new CommissionAssign();
			commissionAssign.setCommissionAssignId(Long.parseLong(targetList[0]));
			
			targetAssignTargets.setCommissionAssign(commissionAssign);
			targetAssignTargets.setTargetValue(Double.parseDouble(targetList[2]));
			
			targetAssignTargetsList.add(targetAssignTargets);		
		}		

		//delete old targets list
		targetAssignService.deleteTargetAssignedTargets(targetAssign.getTargetAssignId());
		
		//save and update new targets list
		for(TargetAssignTargets targetAssignTargets: targetAssignTargetsList){
			targetAssignService.saveTargetAssignedTargets(targetAssignTargets);
		}
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	/**
	 * <pre>
	 * fetch employee ids which already have target assigned
	 * </pre>
	 * @param employeeDetailsId
	 * @param request
	 * @param model
	 * @param session
	 * @return BaseDomain with status and message
	 */
	@Transactional
	@RequestMapping("/fetch_employee_target_assigned")
	public  ResponseEntity<EmployeeIdsResponse> checkEmployeeTargetAssign(HttpServletRequest request, Model model,HttpSession session) {
		EmployeeIdsResponse employeeIdsResponse=new EmployeeIdsResponse();
		List<Long> employeeIds=targetAssignService.commissionAssignedEmployeeIds();
		if(employeeIds==null){
			employeeIdsResponse.setStatus(Constants.FAILURE_RESPONSE);
		}else{
			employeeIdsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			employeeIdsResponse.setEmployeeIds(employeeIds);
		}
		
		return new ResponseEntity<EmployeeIdsResponse>(employeeIdsResponse, HttpStatus.OK);
	}
	/**
	 * <pre>
	 * fetch target assign list by targetPeriod,currentFuture,departmentId
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return target_list.jsp
	 */
	/*@Transactional
	@RequestMapping("/fetch_target_assign_list")
	public ModelAndView fetchTargetAssignList(@RequestBody TargetListsRequest targetListsRequest, HttpServletRequest request, Model model,HttpSession session){
		
		//TargetListsResponse targetListsResponse=new TargetListsResponse();
		
		long departmentId=targetListsRequest.getDepartmentId();
		
		List<TargetAssignDetails> targetLists=targetAssignService.fetchTargetListByTargetPeriod( departmentId);
		
		model.addAttribute("targetLists",targetLists);
		
		if(targetLists==null){
			targetListsResponse.setStatus(Constants.FAILURE_RESPONSE);
			targetListsResponse.setErrorMsg("Target Assign Not Found");
		}else{
			targetListsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			targetListsResponse.setTargetLists(targetLists);			
		}
		
		return new ModelAndView("");
	}*/
	
	@Transactional @RequestMapping("/open_target_assign_list")
	public ModelAndView openTargetAssignList(Model model,HttpSession session,HttpServletRequest request) {
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		
		List<TargetAssignDetails> targetLists=targetAssignService.fetchTargetListByTargetPeriod( departmentId);
		
		model.addAttribute("targetLists",targetLists);
		
		//targetAssignService.createNextDayTarget(Constants.TARGET_PERIOD_DAILY);
		
		return new ModelAndView("TargetsView");
	}
	/**
	 * <pre>
	 * delete assign target of any employee by targetAssignId
	 * delete regular also if deleteRegular status is Yes
	 * </pre>
	 * @param model
	 * @param session
	 * @param request
	 * @return redirect delete_target_assign
	 */
	@Transactional 	@RequestMapping("/delete_target_assign")
	public ResponseEntity<BaseDomain> deleteTargetAssignList(@RequestBody TargetAssignDeleteRequest targetAssignDeleteRequest,Model model,HttpSession session,HttpServletRequest request) {
		
		BaseDomain baseDomain=new BaseDomain();
		/*String deleteRegular=targetAssignDeleteRequest.getDeleteRegular();
		long targetAssignId=targetAssignDeleteRequest.getTargetAssignId();
		targetAssignService.deleteTargetAssigned(targetAssignId);
		
		if(deleteRegular.equals("Yes")){
			targetAssignService.deleteTargetAssignRegular(targetAssignId);
		}
		
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);*/
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/target_graphs")
	public ModelAndView targetGraphs(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Target Graphs");
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		return new ModelAndView("target_achivement_graph");
	}
	/**
	 * <pre>
	 * fetch week dates by year
	 * </pre>
	 * @param year
	 * @param model
	 * @param session
	 * @param request
	 * @return week dates list
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/fetch_week_dates_by_year/{year}")
	public @ResponseBody List<String[]> fetchWeekDatesByYear(@ModelAttribute("year") long year,Model model,HttpSession session,HttpServletRequest request) throws ParseException {
		return WeeksFind.weekFinderByYearForWebApp(year);
	}
	
	@Transactional 	@RequestMapping("/fetch_employee_target_period/{employeeId}")
	public @ResponseBody String fetch_employee_target_period(@ModelAttribute("employeeId") long employeeId,Model model,HttpSession session,HttpServletRequest request) throws ParseException {
		return targetAssignService.getTargetPeriodByEmployee(employeeId);
	}
	
	
}
