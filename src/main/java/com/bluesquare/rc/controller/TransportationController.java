package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Order;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.models.GetTransportationInfoRequest;
import com.bluesquare.rc.models.GetTransportationInfoResponse;
import com.bluesquare.rc.models.UpdateTransportForAccountingRequest;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.TransportationResponse;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.utils.Constants;

/**<pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * 1.transportation
 * 2.getTransportationInfo
 * 3.getTransportationInfoByTransportId
 * 4.deleteTransportation
 * 5.updateTransportation
 * 6.addTransportation
 * </pre>
 */
@Controller
public class TransportationController {
	
	@Autowired
	TransportationDAO transportationDAO;
		
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	CompanyDAO companyDAO;
	
	@Autowired
	OrderDetailsService orderDetailsService;

	/**
	 * <pre>
	 * open transportation details page with Transportation list 
	 * </pre>
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional
	@RequestMapping("/transportation")
	public ModelAndView transportation(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("transportation");
		List<Transportation> transportations = transportationDAO.fetchAll();
		List<TransportationModel> transportationModelList=new ArrayList<>();
		for(Transportation transportation : transportations){
			transportationModelList.add(new TransportationModel(
					transportation.getId(), 
					transportation.getTransportName(), 
					transportation.getMobNo(), 
					transportation.getGstNo(), 
					transportation.getVehicleNo()));
		}
		mv.addObject("transportations", transportationModelList);
		mv.addObject("pageName", "Manage Transportation");
		return mv;
	}
	
	/**
	 * get transportation details by transportaionId
	 * @param getTransportationInfoRequest
	 * @param request
	 * @param session
	 * @return transportation details with httpStatus
	 */
	@Transactional
	@RequestMapping("/getTransportationInfo")
	public ResponseEntity<GetTransportationInfoResponse> getTransportationInfo(
			@RequestBody GetTransportationInfoRequest getTransportationInfoRequest, HttpServletRequest request,
			HttpSession session) {
		String id = getTransportationInfoRequest.getId();
		Transportation transportation =transportationDAO.fetchById(id);
		GetTransportationInfoResponse getTransportationInfoResponse = new GetTransportationInfoResponse();
		getTransportationInfoResponse.setSuccess(true);
		getTransportationInfoResponse.setMsg("");
		getTransportationInfoResponse.setTransportation(new TransportationModel(
				transportation.getId(), 
				transportation.getTransportName(), 
				transportation.getMobNo(), 
				transportation.getGstNo(), 
				transportation.getVehicleNo()));
		return new ResponseEntity<GetTransportationInfoResponse>(getTransportationInfoResponse, HttpStatus.OK);
	}
	
	/**
	 * get transportation details by transportaionId
	 * @param getTransportationInfoRequest
	 * @param request
	 * @param session
	 * @return Transportation
	 */
	@Transactional
	@RequestMapping("/getTransportationInfoByTransportId")
	public @ResponseBody TransportationModel getTransportationInfoByTransportId(HttpServletRequest request,
			HttpSession session) {
		String id = request.getParameter("id");
		Transportation transportation =transportationDAO.fetchById(id);
		return new TransportationModel(
				transportation.getId(), 
				transportation.getTransportName(), 
				transportation.getMobNo(), 
				transportation.getGstNo(), 
				transportation.getVehicleNo());
	}
	/**
	 * <pre>
	 * disable transportation by transportationId
	 * </pre> 
	 * @param getTransportationInfoRequest
	 * @param request
	 * @param session
	 * @return baseDomain with httpStatus
	 */
	@Transactional
	@RequestMapping("/deleteTransportation")
	public ResponseEntity<BaseDomain> deleteTransportation(
			@RequestBody GetTransportationInfoRequest getTransportationInfoRequest, HttpServletRequest request,
			HttpSession session) {
		String id = getTransportationInfoRequest.getId();
		Transportation transportation =transportationDAO.fetchById(id);
		transportationDAO.delete(transportation);
		BaseDomain baseDomain = new BaseDomain();
		baseDomain.setSuccess(true);
		baseDomain.setMsg("Transportation Deleted Successfully");
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	/**
	 * <pre>
	 * update transportation  
	 * </pre>
	 * @param transportation
	 * @param request
	 * @param session
	 * @return baseDomain with httpStatus
	 */
	@Transactional
	@RequestMapping("/updateTransportation")
	public ResponseEntity<BaseDomain> updateTransportation(
			@RequestBody Transportation transportation, HttpServletRequest request,
			HttpSession session) {

		BaseDomain baseDomain = new BaseDomain();
		
		if(orderDetailsService.checkTransaportationUsed(transportation.getId())){
			baseDomain.setSuccess(true);
			baseDomain.setMsg("Order Already Exist for this Transportation");
			
		}else{
			transportation.setCompany(companyDAO.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds())));
			Transportation orgTransportation = transportationDAO.fetchById(transportation.getId()+"");
			transportation.setInsertedDate(orgTransportation.getInsertedDate());
			transportation.setStatus(true);
			transportationDAO.update(transportation);
			baseDomain.setSuccess(true);
			baseDomain.setMsg("Transportation Updated Successfully");
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/addTransportation")
	public ResponseEntity<BaseDomain> addTransportation(
			@RequestBody Transportation transportation, HttpServletRequest request,
			HttpSession session) {
		BaseDomain baseDomain = new BaseDomain();
		
			baseDomain.setSuccess(true);
			baseDomain.setMsg("Transportation Added Successfully");
			transportation.setCompany(companyDAO.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds())));
			transportationDAO.save(transportation);
			
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
		
		
	}
	
	
	
}
