package com.bluesquare.rc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Branch;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.PermanentDamageDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.CategoryAndBrandList;
import com.bluesquare.rc.models.InventoryAddedInvoiceModel;
import com.bluesquare.rc.models.InventoryDetailsForEdit;
import com.bluesquare.rc.models.InventoryDetailsModel;
import com.bluesquare.rc.models.InventoryDetailsResponseModel;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.models.InventoryRequest;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.responseEntities.SupplierProductListModule;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.bluesquare.rc.utils.RoundOff;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 *  API End Points
 * 1.fetchProductListForInventory
 * 2.fetchProductListForUnderThresholdValue
 * 3.fetchProductListForInventoryAjax
 * 4.fetchSupplierListForAddQuantity
 * 5.fetchSupplierRateByProductIdamdSupplierId
 * 6.saveInventory
 * 7.saveOneInventory
 * 8.openAddMultipleInventory
 * 9.fetchProductBySupplierId
 * 10.fetchProductBySupplierIdAndCategoryIdAndBrandId
 * 11.fetchBrandAndCategoryList
 * 12.fetchSupplierBySupplierId
 * 13.fetchInventoryReportView
 * 14.fetchInventoryReportViewAjax
 * 15.fetchTrasactionDetailsByInventoryId
 * 16.openEditMultipleInventory
 * 17.editInventory
 * 18.deleteInventory
 * 19.defineDamage
 * </pre>
 */
@Controller
public class ManageInventory {

	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	ProductDAO productDAO;
	
	
	@Autowired
	InventoryDetails inventoryDetails;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	BrandService brandService; 
	
	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	Supplier supplier;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	BranchService branchService;
	/**
	 * open manage inventory details
	 * @param model
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchProductListForInventory")
	public ModelAndView fetchProductListForInventory(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage inventory");
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		model.addAttribute("inventoryProductsList", inventoryProductsList);
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
		model.addAttribute("categorieslist", categoriesList);
		
		/*List<Product> productList=productService.fetchProductListForWebApp();
		model.addAttribute("productlist", productList);*/
		
		return new ModelAndView("ManageInventory");
	}
	/**
	 * show product details which quanity under threshold in manage inventory
	 * @param model
	 * @param session
	 * @return ManageInventory.jsp
	 */
	@Transactional 	@RequestMapping("/fetchProductListForUnderThresholdValue")
	public ModelAndView fetchProductListForUnderThresholdValue(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage Inventory");
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		List<InventoryProduct> inventoryProductsListNew=new ArrayList<>();
		if(inventoryProductsList!=null){
			long srno=1;		
			for(InventoryProduct inventoryProduct :inventoryProductsList)
			{
				ProductModel product=inventoryProduct.getProduct();
				if(product.getCurrentQuantity()<=product.getThreshold())
				{
					inventoryProduct.setSrno(srno++);
					inventoryProductsListNew.add(inventoryProduct);
				}
			}
		}
		
		model.addAttribute("inventoryProductsList", inventoryProductsListNew);
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
		model.addAttribute("categorieslist", categoriesList);
		
		return new ModelAndView("ManageInventory");
	}
	/***
	 * fetch product details for manage inventory list refresh
	 * @return InventoryProduct list
	 */
	@Transactional 	@RequestMapping("/fetchProductListForInventoryAjax")
	public @ResponseBody List<InventoryProduct> fetchProductListForInventoryAjax() {
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		//inventoryProductsList=inventoryService.makeInventoryProductViewProductNull(inventoryProductsList);
		
		return inventoryProductsList;
	}
	
	/***
	 * fetch supplier list by product 
	 * @param request
	 * @return SupplierProductList list
	 */
	@Transactional 	@RequestMapping("/fetchSupplierListForAddQuantity")
	public @ResponseBody List<SupplierProductListModule> fetchSupplierListForAddQuantity(HttpServletRequest request) {
		
		List<SupplierProductList> supplierProductLists = inventoryService.fetchSupplierListByProductId(Long.parseLong(request.getParameter("productId")));
		List<SupplierProductListModule> supplierProductListModuleList=new ArrayList<>();
		if(supplierProductLists!=null)
		{
			for(SupplierProductList supplierProductList: supplierProductLists){
				supplierProductListModuleList.add(new SupplierProductListModule(
						supplierProductList.getProduct().getCategories().getIgst(), 
						supplierProductList.getSupplier().getSupplierId(),
						supplierProductList.getSupplier().getName(),
						supplierProductList.getProduct().getProductId(),
						supplierProductList.getProduct().getProductName(), 
						supplierProductList.getProduct().getCategories().getCategoryId(), 
						supplierProductList.getProduct().getCategories().getCategoryName(), 
						supplierProductList.getProduct().getBrand().getBrandId(), 
						supplierProductList.getProduct().getBrand().getName(), 
						supplierProductList.getSupplierRate()));
			}
		}
		
		return supplierProductListModuleList;
	}
	/**
	 * fetch supplier product info by supplier id and product id
	 * @param request
	 * @return SupplierProductList
	 */
	@Transactional 	@RequestMapping("/fetchSupplierRateByProductIdamdSupplierId")
	public @ResponseBody SupplierProductListModule fetchSupplierRateByProductIdamdSupplierId(HttpServletRequest request) {
		
		SupplierProductList supplierProductList=supplierService.fetchSupplierProductRate(Long.parseLong(request.getParameter("productId")),request.getParameter("supplierId"));
						
		if(supplierProductList!=null){
			return new SupplierProductListModule(
					supplierProductList.getProduct().getCategories().getIgst(), 
					supplierProductList.getSupplier().getSupplierId(),
					supplierProductList.getSupplier().getName(),
					supplierProductList.getProduct().getProductId(),
					supplierProductList.getProduct().getProductName(), 
					supplierProductList.getProduct().getCategories().getCategoryId(), 
					supplierProductList.getProduct().getCategories().getCategoryName(), 
					supplierProductList.getProduct().getBrand().getBrandId(), 
					supplierProductList.getProduct().getBrand().getName(), 
					supplierProductList.getSupplierRate()); 
		}else{
			return null;
		}
		
	}
	/**
	 * save inventory and inventory product into with bill details (bill date,number)
	 * increase current inventory 	 * 
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect fetchProductListForInventory
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/saveInventory")
	public @ResponseBody BaseDomain saveInventory(@RequestBody InventoryRequest inventoryRequest,Model model,HttpServletRequest request,HttpSession session) throws ParseException {
		
			System.out.println("inside add inventory");
//			String productIdList=request.getParameter("productIdList");
//			String supplierId=request.getParameter("supplierId");
//			String paymentDate=request.getParameter("paymentDate");
//			String billDate=request.getParameter("billDate");
//			String billNumber=request.getParameter("billNumber");
//			inventory.setInventoryAddedDatetime(new Date());
//			
//			supplier=supplierService.fetchSupplier(supplierId);		
//			inventory.setSupplier(supplier);
//			inventory.setBillDate(dateFormat.parse(billDate));
//			inventory.setBillNumber(billNumber);
//			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			inventoryService.addInventory(inventoryRequest);
		
		
		//return new ModelAndView("redirect:/fetchProductListForInventory");
			BaseDomain baseDomain=new BaseDomain();
			baseDomain.setSuccess(true);
			baseDomain.setMsg("Inventory Added Successfully");
		return baseDomain;
	}
	/**
	 * save one inventory
	 * increase current inventory
	 * @param model
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/saveOneInventory")
	public @ResponseBody String saveOneInventory(@RequestBody InventoryRequest inventoryRequest,Model model,HttpServletRequest request) throws ParseException {
		
		System.out.println("inside add inventory");
		
//			String productIdList=request.getParameter("productIdList");
//			String supplierId=request.getParameter("supplierId");
//			String paymentDate=request.getParameter("paymentDate");
//			String billDate=request.getParameter("billDate");
//			String billNumber=request.getParameter("billNumber");
//			inventory.setInventoryAddedDatetime(new Date());
//			
//			supplier=supplierService.fetchSupplier(supplierId);			
//			inventory.setBillDate(dateFormat.parse(billDate));
//			inventory.setBillNumber(billNumber);
//			inventory.setSupplier(supplier);
//			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			
			inventoryService.addInventory(inventoryRequest);
		
		
		return "Success"; 
	}
	/**
	 * open multiple inventory page with supplier,brand,category list
	 * @param request
	 * @param model
	 * @param session
	 * @return addMultipleInventory.jsp
	 */
	@Transactional 	@RequestMapping("/openAddMultipleInventory")
	public ModelAndView openAddMultipleInventory(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Add Multiple Inventory");
		
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		List<SupplierModel> supplierModelList=new ArrayList<>();
		if(supplierList!=null){
			for(Supplier supplier : supplierList){
				supplierModelList.add(new SupplierModel(
						supplier.getSupplierPKId(), 
						supplier.getSupplierId(), 
						supplier.getName(), 
						supplier.getContact()));
			}
		}		
		model.addAttribute("supplierList", supplierModelList);
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
		model.addAttribute("categorieslist", categoriesList);
		
		model.addAttribute("invtUrl", "saveInventory");
		
		model.addAttribute("discountGiven", false);
		
		return new ModelAndView("addMultipleInventory"); 
	}
	
	@Transactional 	@RequestMapping("/fetchProductBySupplierId")
	public @ResponseBody List<ProductModel> fetchProductBySupplierId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListBySupplierId(request.getParameter("supplierId"));
		List<ProductModel> productModelList=new ArrayList<>();
		if(list!=null){
			for(Product product : list){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						product.getProductCode(), 
						new CategoriesModel(
								product.getCategories().getCategoryId(), 
								product.getCategories().getCategoryName(), 
								product.getCategories().getCategoryDescription(), 
								product.getCategories().getHsnCode(), 
								product.getCategories().getCgst(), 
								product.getCategories().getSgst(), 
								product.getCategories().getIgst(), 
								product.getCategories().getCategoryDate(), 
								product.getCategories().getCategoryUpdateDate()), 
						new BrandModel(
								product.getBrand().getBrandId(), 
								product.getBrand().getName(), 
								product.getBrand().getBrandAddedDatetime(), 
								product.getBrand().getBrandUpdateDatetime()), 
						product.getRate(), 
						/*product.getProductContentType(),*/ 
						product.getProductDescription(), 
						product.getThreshold(), 
						product.getCurrentQuantity(), 
						product.getDamageQuantity(), 
						product.getFreeQuantity(), 
						product.getProductAddedDatetime(), 
						product.getProductQuantityUpdatedDatetime(),
						product.getProductBarcode(),
						null));
			}
		}
		return productModelList; 
	}
	
	@Transactional 	@RequestMapping("/fetchProductBySupplierIdAndCategoryIdAndBrandId")
	public @ResponseBody List<ProductModel> fetchProductBySupplierIdAndCategoryIdAndBrandId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(request.getParameter("supplierId"), Long.parseLong(request.getParameter("categoryId")), Long.parseLong(request.getParameter("brandId")));
		
		List<ProductModel> productModelList=new ArrayList<>();
		if(list!=null){
			for(Product product : list){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						product.getProductCode(), 
						new CategoriesModel(
								product.getCategories().getCategoryId(), 
								product.getCategories().getCategoryName(), 
								product.getCategories().getCategoryDescription(), 
								product.getCategories().getHsnCode(), 
								product.getCategories().getCgst(), 
								product.getCategories().getSgst(), 
								product.getCategories().getIgst(), 
								product.getCategories().getCategoryDate(), 
								product.getCategories().getCategoryUpdateDate()), 
						new BrandModel(
								product.getBrand().getBrandId(), 
								product.getBrand().getName(), 
								product.getBrand().getBrandAddedDatetime(), 
								product.getBrand().getBrandUpdateDatetime()), 
						product.getRate(), 
						/*product.getProductContentType(),*/ 
						product.getProductDescription(), 
						product.getThreshold(), 
						product.getCurrentQuantity(), 
						product.getDamageQuantity(), 
						product.getFreeQuantity(), 
						product.getProductAddedDatetime(), 
						product.getProductQuantityUpdatedDatetime(),
						product.getProductBarcode(),
						null));
			}
		}
		return productModelList; 
	}
	
	@Transactional 	@RequestMapping("/fetchBrandAndCategoryList")
	public @ResponseBody CategoryAndBrandList fetchBrandAndCategoryList(HttpSession session) {
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		List<SupplierModel> supplierModelList=new ArrayList<>();
		if(supplierList!=null){
			for(Supplier supplier : supplierList){
				supplierModelList.add(new SupplierModel(
						supplier.getSupplierPKId(), 
						supplier.getSupplierId(), 
						supplier.getName(), 
						supplier.getContact()));
			}
		}
		return new CategoryAndBrandList(categoriesList, brandList,supplierModelList); 
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierBySupplierId")
	public @ResponseBody SupplierModel fetchSupplierBySupplierId(HttpServletRequest request) {
		
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		
		return new SupplierModel(
				supplier.getSupplierPKId(), 
				supplier.getSupplierId(), 
				supplier.getName(), 
				supplier.getContact()); 
	}
	/**
	 * <pre>
	 * fetch inventory report details by range where show
	 * paid/unpaid/partial paid status
	 * product wise details
	 * payment list details
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return InventoryReport.jsp
	 */
	@Transactional 	@RequestMapping("/fetchInventoryReportView")
	public ModelAndView fetchInventoryReportView(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Inventory Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String supplierId=request.getParameter("supplierId");
		if(supplierId==null)
		{
			supplierId="";
		}
		
		List<InventoryReportView> inventoryReportViews=inventoryService.fetchInventoryReportView(supplierId, startDate, endDate, range);
		model.addAttribute("inventoryReportViews", inventoryReportViews);
		
		model.addAttribute("supplierId", supplierId);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("range", range);
		
		model.addAttribute("saveMsg",session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		
		session.setAttribute("lastUrl", "fetchInventoryReportView?range="+range+"&startDate="+startDate+"&endDate="+endDate+"&supplierId="+supplierId);
		
		return new ModelAndView("InventoryReport"); 
	}
	/**
	 * <pre>
	 * fetch inventory report details by range where show
	 * paid/unpaid/partial paid status
	 * product wise details
	 * payment list details
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return InventoryReport.jsp
	 */
	@Transactional 	@RequestMapping("/fetchInventoryReportViewAjax")
	public @ResponseBody List<InventoryReportView> fetchInventoryReportViewAjax(HttpServletRequest request,Model model,HttpSession session) {
				
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String supplierId=request.getParameter("supplierId");
		if(supplierId==null)
		{
			supplierId="";
		}
		
		List<InventoryReportView> inventoryReportViews=inventoryService.fetchInventoryReportView(supplierId, startDate, endDate, range);
		
		return inventoryReportViews; 
	}
	
	@Transactional 	@RequestMapping("/fetchTrasactionDetailsByInventoryId")
	public @ResponseBody InventoryDetailsResponseModel fetchTrasactionDetailsByInventoryId(HttpServletRequest request) {
		Inventory inventory= inventoryService.fetchInventory(request.getParameter("inventoryId"));
		List<InventoryDetails> inventoryDetailList=inventoryService.fetchTrasactionDetailsByInventoryId(request.getParameter("inventoryId"));
		
		double totalBeforeAllDiscount=0,
				totalProductsAllDiscount=0;
		
		List<InventoryDetailsModel> inventoryDetailsModelList=new ArrayList<>();
		for(InventoryDetails inventoryDetails : inventoryDetailList){
			OrderUsedProduct product=inventoryDetails.getProduct();
			Product product2=product.getProduct();
			inventoryDetailsModelList.add(new InventoryDetailsModel(
					inventoryDetails.getInventoryDetailsId(), 
					new ProductModel(
							product.getProductId(), 
							product.getProductName(), 
							product.getProductCode(), 
							new CategoriesModel(
									product.getCategories().getCategoryId(), 
									product.getCategories().getCategoryName(), 
									"", 
									product.getCategories().getHsnCode(), 
									product.getCategories().getCgst(), 
									product.getCategories().getSgst(), 
									product.getCategories().getIgst(), 
									null, 
									null), 
							new BrandModel(
									product.getBrand().getBrandId(), 
									product.getBrand().getName(), 
									null, 
									null), 
							product.getRate(), 
							/*product.getProductContentType(),*/ 
							"", 
							product.getThreshold(), 
							product.getCurrentQuantity(), 
							product.getDamageQuantity(), 
							0, 
							null, 
							null,
							"",
							new ProductModel(
									product2.getProductId(), 
									product2.getProductName(), 
									product2.getProductCode(), 
									new CategoriesModel(
											product2.getCategories().getCategoryId(), 
											product2.getCategories().getCategoryName(), 
											"", 
											product2.getCategories().getHsnCode(), 
											product2.getCategories().getCgst(), 
											product2.getCategories().getSgst(), 
											product2.getCategories().getIgst(), 
											null, 
											null), 
									new BrandModel(
											product2.getBrand().getBrandId(), 
											product2.getBrand().getName(), 
											null, 
											null), 
									product2.getRate(), 
									/*product.getProductContentType(),*/ 
									"", 
									product2.getThreshold(), 
									product2.getCurrentQuantity(), 
									product2.getDamageQuantity(), 
									0, 
									null, 
									null,
									"",
									null)),
					inventoryDetails.getRate(), 
					inventoryDetails.getQuantity(), 
					inventoryDetails.getAmount(),
					inventoryDetails.getDiscountAmount(),
					inventoryDetails.getDiscountPercentage(),
					inventoryDetails.getDiscountType(),
					inventoryDetails.getAmountBeforeDiscount(),
					inventoryDetails.getDiscountOnMRP()));
			
			totalBeforeAllDiscount+=inventoryDetails.getAmountBeforeDiscount();
			totalProductsAllDiscount+=inventoryDetails.getDiscountAmount();
		}
		
		InventoryReportView inventoryReportView=new InventoryReportView(
				0, 
				inventory.getInventoryTransactionId(), 
				new SupplierModel(
						inventory.getSupplier().getSupplierPKId(), 
						inventory.getSupplier().getSupplierId(), 
						inventory.getSupplier().getName(), 
						inventory.getSupplier().getContact()),
				inventory.getTotalQuantity(), 
				inventory.getTotalAmount(), 
				RoundOff.findRoundOffAmount(inventory.getTotalAmountTax()), 
				0, 
				0, 
				inventory.getInventoryAddedDatetime(),
				inventory.getInventoryPaymentDatetime(), 
				"",
				"",
				inventory.getBillDate(),
				inventory.getBillNumber(),
				inventory.getDiscountAmount(),
				inventory.getDiscountPercentage(),
				inventory.getDiscountType(),
				inventory.getTotalAmountBeforeDiscount(),
				inventory.getTotalAmountTaxBeforeDiscount(),
				inventory.getDiscountGiven(),
				inventory.getDiscountOnMRP());
		 
		
		
		return new InventoryDetailsResponseModel(inventoryReportView, inventoryDetailsModelList,totalBeforeAllDiscount,totalProductsAllDiscount); 
	}
	/**
	 * edit inventory valid till 72 hour or before payment  
	 * edit inventory details open for change/add/delete product details
	 * @param request
	 * @param model
	 * @param session
	 * @return addMultipleInventory.jsp
	 */
	@Transactional 	@RequestMapping("/openEditMultipleInventory")
	public ModelAndView openEditMultipleInventory(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Edit Multiple Inventory");
				
	    List<InventoryDetails> inventoryDetailsList=inventoryService.fetchTrasactionDetailsByInventoryId(request.getParameter("inventoryId"));
	    Inventory inventory=inventoryService.fetchInventory(request.getParameter("inventoryId"));
	    
	    double totalProductsDiscount=0;
	    double totalBeforeAllDiscount=0;
	    double totalAfterDiscountAmount=0;
	    
	    List<InventoryDetailsForEdit> inventoryDetailsList2=new ArrayList<>();
	    for(InventoryDetails inventoryDetails:inventoryDetailsList)
	    {
	    	long qty=inventoryDetails.getQuantity();
      		double rate=  supplierService.fetchSupplierByProductIdAndSupplierId(inventoryDetails.getProduct().getProduct().getProductId(),inventory.getSupplier().getSupplierId()).getSupplierRate();
      		float tempIgst=inventoryDetails.getProduct().getCategories().getIgst();
      		double ttl=qty*rate;
      		//double amtwithtax=ttl + ( (ttl*tempIgst)/100 ); 
	    	
      		double mrp=rate+((rate*tempIgst)/100);
			mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(mrp, tempIgst);
			
			double amtwithtax=inventoryDetails.getAmount();//calculateProperTaxModel.getMrp()*qty;
      		
      		//totalAmountWithoutTax+=ttl;
      		totalBeforeAllDiscount+=inventoryDetails.getAmountBeforeDiscount();
      		totalAfterDiscountAmount+=amtwithtax;
      		
      		//js needed for process
			//[productId,quantity,unitPrice,mrp,taxableAmountBeforeDiscount,totalAmountBeforeDiscount,
      		//discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
      		
      		/*
      		    private double totalAmountWithoutTax;
				private double totalAmountWithTax;
				private long productId;
				private long quantity;
			
				private double unitPrice;
				private double mrp;
				private double totalAmountBeforeDiscount;
				private double discountAmount;
				private double discountPercentage;
				private String discountType;
      		 * */
      		
      		if(inventoryDetails.getDiscountType().equals("PERCENTAGE")){
      			ttl=ttl-((ttl*inventoryDetails.getDiscountPercentage())/100);
      		}else{
      			double discper=(inventoryDetails.getDiscountAmount()/amtwithtax)*100;
      			ttl=ttl-((ttl*discper)/100);
      		}
      		
      		totalProductsDiscount+=inventoryDetails.getDiscountAmount();
      		
	    	inventoryDetailsList2.add(new InventoryDetailsForEdit(
	    			ttl, 
	    			amtwithtax, 
	    			inventoryDetails.getProduct(), 
	    			qty,
	    			rate,
	    			mrp,
	    			inventoryDetails.getAmountBeforeDiscount(),
	    			inventoryDetails.getDiscountAmount(),
	    			inventoryDetails.getDiscountPercentage(),
	    			inventoryDetails.getDiscountType(),
	    			inventoryDetails.getDiscountOnMRP()
	    			));
	    	
	    }
	    
	    model.addAttribute("totalBeforeAllDiscount", totalBeforeAllDiscount);
	    model.addAttribute("totalProductsDiscount", totalProductsDiscount);
	    model.addAttribute("totalAfterDiscountAmount", totalAfterDiscountAmount);
	    
	    model.addAttribute("discountGiven", inventory.getDiscountGiven());
	    model.addAttribute("inventoryDetailsList", inventoryDetailsList2);
	    	    
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		model.addAttribute("supplierList", supplierList);
		
		
		List<Product> productList=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(inventory.getSupplier().getSupplierId(), 0, 0);
		model.addAttribute("productList", productList);
		model.addAttribute("paymentDate", new SimpleDateFormat("yyyy-MM-dd").format(inventory.getInventoryPaymentDatetime()));
		model.addAttribute("billDate", new SimpleDateFormat("yyyy-MM-dd").format(inventory.getBillDate()));
		model.addAttribute("invtAddedDate", new SimpleDateFormat("yyyy-MM-dd").format(inventory.getInventoryAddedDatetime()));
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		model.addAttribute("invtUrl", "editInventory");
		model.addAttribute("inventory", inventory);
		
		return new ModelAndView("addMultipleInventory"); 
	}	
	/**
	 * <pre>
	 * change inventory details like
	 * add new product,
	 * delete products,
	 * change quantity,
	 * change supplier,
	 * bill details,
	 * payment details
	 * </pre>
	 * @param model
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@Transactional 	@RequestMapping("/editInventory")
	public @ResponseBody BaseDomain editInventory(@RequestBody InventoryRequest inventoryRequest,Model model,HttpServletRequest request) throws ParseException {
		
		System.out.println("inside edit inventory");
//			String inventoryId=request.getParameter("inventoryId");
//			String productIdList=request.getParameter("productIdList");
//			String supplierId=request.getParameter("supplierId");
//			String paymentDate=request.getParameter("paymentDate");
//			String billDate=request.getParameter("billDate");
//			String billNumber=request.getParameter("billNumber");
//			
//			inventory=inventoryService.fetchInventory(inventoryId);
//			
//			supplier=supplierService.fetchSupplier(supplierId);		
//			inventory.setBillDate(dateFormat.parse(billDate));
//			inventory.setBillNumber(billNumber);
//			inventory.setSupplier(supplier);
//			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			
			inventoryService.editInventory(inventoryRequest);
				
			BaseDomain baseDomain=new BaseDomain();
			baseDomain.setSuccess(true);
			baseDomain.setMsg("Inventory Updated Successfully");
		return baseDomain; 
	}
	/**
	 * delete inventory 
	 * before payment and 72 hour
	 * @param model
	 * @param request
	 * @return
	 */
	@Transactional 	@RequestMapping("/deleteInventory")
	public ModelAndView deleteInventory(Model model,HttpServletRequest request) {
		System.out.println("inside delete inventory");
		inventoryService.deleteInventory(request.getParameter("inventoryId"));
		return new ModelAndView("redirect:/fetchInventoryReportView?range=currentMonth&supplierId=");
	}
	/**
	 * define damange by gatekeeper 
	 * if damage product in godown or any other reason
	 * define in DamageDefine both cases Claim/Not-Claim
	 * define in Damange Recovery where can get recovery if only damage in claim mode only
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/defineDamage")
	public ModelAndView defineDamage(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("inside define Damage");
		
		long productId=Long.parseLong(request.getParameter("productId"));;
		long damageQuantity=Long.parseLong(request.getParameter("damageQuantity"));
		String damageType=request.getParameter("damageType");
		String reason=request.getParameter("damageQuantityReason");
		
		Product product=productService.fetchProductForWebApp(productId);
		product.setDamageQuantity(product.getDamageQuantity()+damageQuantity);
		product.setCurrentQuantity(product.getCurrentQuantity()-damageQuantity);
		productService.updateProductForWebApp(product);
		
		//update OrderUsedProduct Current Quantity same as Product
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		if(damageType.equals("claimed")){
			productService.saveUpdateDamageRecoveryMonthWise(productId,damageQuantity);
			
		}else{
			//notClaimed
			PermanentDamageDetails permanentDamageDetails=new PermanentDamageDetails();
			permanentDamageDetails.setQuantityDamage(damageQuantity);
			permanentDamageDetails.setRejectQuantityReason(reason);
			permanentDamageDetails.setDamageDate(new Date());
			permanentDamageDetails.setDamageFrom("Manage Inventory");
			productService.savePermanenetDamageDetails(permanentDamageDetails, productId);
		}
		
		
		//damagedefine for report
		String orderId="",collectingPerson="",departmentName="";
		long companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
		EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
		
		if(employeeDetails==null){
			orderId="Company";
			collectingPerson="Company";
			departmentName="Company";
		}else{
			orderId=employeeDetails.getName();
			collectingPerson=employeeDetails.getName();
			departmentName=employeeDetails.getEmployee().getDepartment().getName();
		}
		Branch branch=branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds());
		DamageDefine damageDefine=new DamageDefine( orderId, product, collectingPerson, departmentName, damageQuantity, new Date(), reason,branch);
		productService.saveDamageDefine(damageDefine);
		
		return new ModelAndView("redirect:/fetchProductListForInventory");
	}
	
	/***
	 * create inventory added invoice and add in response
	 * @param request
	 * @param model
	 * @param session
	 * @param response
	 */
	@Transactional 	@RequestMapping("/inventory-invoice.pdf")
	public void counterBillPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Invoice");
				
		  try {
			String inventoryTsnId=request.getParameter("inventoryTsnId");
				InventoryAddedInvoiceModel invoicePrintDataModel=inventoryService.getInvoiceDetails(inventoryTsnId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/invoice.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdfInventory(invoicePrintDataModel, fullPath);
								
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
}
