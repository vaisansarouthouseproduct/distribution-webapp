package com.bluesquare.rc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.ExpenseType;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.models.ExpenseListModel;
import com.bluesquare.rc.responseEntities.ExpenseModel;
import com.bluesquare.rc.responseEntities.ExpenseTypeModel;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.ExpenseService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;

/**
 * <pre>
 * &#64;author Sachin Pawar 18-05-2018 Code Documentation
 * 1.addExpense
 * 2.saveExpense
 * 3.fetchExpense
 * 4.updateExpense
 * 5.deleteExpense
 * 6.fetchExpenseList
 * 7.fetchExpense
 * 8.updateExpense
 * 9.deleteExpense
 * 10.fetchExpenseList
 * 11.expense_type_page
 * 12.expense_type_check_for_add
 * 13.expense_type_check_for_update
 * 14.save_expense_type
 * 15.update_expense_type
 * 16.delete_expense_type
 * 17.fetchExpenseType
 * 18.fetchExpenseTypeList
 * </pre>
 */
@Controller
public class ExpenseController {

	@Autowired
	ExpenseService expenseService;

	@Autowired
	TokenHandlerDAO tokenHandlerDAO;

	@Autowired
	BranchService branchService;
	
	@Autowired
	PaymentService paymentService;

	@Transactional
	@RequestMapping("/addExpense")
	public ModelAndView addExpense(Model model, HttpSession session) {

		System.out.println("in addExpense controller");
		model.addAttribute("pageName", "Add Expense");

		List<ExpenseType> expenseTypeList = expenseService.fetchExpenseTypeList();
		List<ExpenseTypeModel> expenseTypeModelList = new ArrayList<>();
		for(int i=0; i<expenseTypeList.size(); i++){
			ExpenseType expenseType=expenseTypeList.get(i);
			expenseTypeModelList.add(new ExpenseTypeModel(
					expenseType.getExpenseTypeId(), 
					expenseType.getName(), 
					expenseType.getAddedDate(), 
					expenseType.getUpdatedDate()));
		}
		model.addAttribute("expenseTypeList", expenseTypeModelList);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);

		return new ModelAndView("addExpense");
	}

	/**
	 * <pre>
	 * save expense 
	 * entry in ledger
	 * </pre>
	 * 
	 * @param model
	 * @param session
	 * @param request
	 * @return
	 */
	@Transactional
	@RequestMapping("/saveExpense")
	public ModelAndView saveExpense(Model model, HttpSession session, HttpServletRequest request) {

		System.out.println("in saveExpense controller");

		String amount = request.getParameter("amount");
		String bankName = request.getParameter("bankName");
		String chequeNumber = request.getParameter("chequeNumber");
		String expenseTypeId = request.getParameter("expenseTypeId");
		String refernce = request.getParameter("reference");
		String type = request.getParameter("type");
		String transactionRefNo=request.getParameter("transactionRefNo");
		long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
		String comment=request.getParameter("comment");

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Expense expense = new Expense();

		expense.setAddDate(new Date());
		expense.setAmount(Double.parseDouble(amount));

		//if pay mode is cash then bank name and cheque number is null
		if(type.equals(Constants.CASH_PAY_STATUS)){
			expense.setBankName(null);
			expense.setChequeNumber(null);
			
			expense.setPaymentMethod(null);
			expense.setTransactionReferenceNumber(null);
			expense.setComment(null);
		}else if(type.equals(Constants.OTHER_PAY_STATUS)){//if pay mode is other then payment mode and transaction ref no and comment is fill
			expense.setBankName(null);
			expense.setChequeNumber(null);
			
			expense.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
			expense.setTransactionReferenceNumber(transactionRefNo);
			expense.setComment(comment);
		}else{//if pay mode is Cheque then bank name and cheque number is fill
			expense.setPaymentMethod(null);
			expense.setTransactionReferenceNumber(null);
			expense.setComment(null);
			expense.setBankName(bankName);
			expense.setChequeNumber(chequeNumber);
		}


		ExpenseType expenseType = expenseService.fetchExpenseTypeByExpenseTypeId(Long.parseLong(expenseTypeId));
		expense.setExpenseType(expenseType);

		expense.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		expense.setReference(refernce);
		expense.setType(type);
		expense.setStatus(true);

		expenseService.saveExpense(expense);

		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}

	@Transactional
	@RequestMapping("/fetchExpense")
	public ModelAndView fetchExpense(Model model, HttpSession session, HttpServletRequest request) {

		System.out.println("in fetchExpense controller");
		model.addAttribute("pageName", "Update Expense");

		ExpenseModel expenseModel = expenseService.fetchExpenseModel(request.getParameter("expenseId"));
		model.addAttribute("expense", expenseModel);

		List<ExpenseType> expenseTypeList = expenseService.fetchExpenseTypeList();
		model.addAttribute("expenseTypeList", expenseTypeList);

		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		return new ModelAndView("updateExpense");
	}

	/**
	 * <pre>
	 * update expense 
	 * update entry in ledger by expenseId
	 * </pre>
	 * 
	 * @param model
	 * @param session
	 * @param request
	 * @return
	 */
	@Transactional
	@RequestMapping("/updateExpense")
	public ModelAndView updateExpense(Model model, HttpSession session, HttpServletRequest request) {

		System.out.println("in updateExpense controller");

		String expenseId = request.getParameter("expenseId");
		String amount = request.getParameter("amount");
		String bankName = request.getParameter("bankName");
		String chequeNumber = request.getParameter("chequeNumber");
		String expenseTypeId = request.getParameter("expenseTypeId");
		String refernce = request.getParameter("reference");
		String type = request.getParameter("type");
		String transactionRefNo=request.getParameter("transactionRefNo");
		long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
		String comment=request.getParameter("comment");

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Expense expense = expenseService.fetchExpense(expenseId);

		expense.setUpdateDate(new Date());
		expense.setAmount(Double.parseDouble(amount));

		// if pay mode is cash then bank name and cheque number is null
		if(type.equals(Constants.CASH_PAY_STATUS)){
			expense.setBankName(null);
			expense.setChequeNumber(null);
			
			expense.setPaymentMethod(null);
			expense.setTransactionReferenceNumber(null);
			expense.setComment(null);
		}else if(type.equals(Constants.OTHER_PAY_STATUS)){//if pay mode is other then payment mode and transaction ref no and comment is fill
			expense.setBankName(null);
			expense.setChequeNumber(null);
			
			expense.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
			expense.setTransactionReferenceNumber(transactionRefNo);
			expense.setComment(comment);
		}else{//if pay mode is Cheque then bank name and cheque number is fill
			expense.setPaymentMethod(null);
			expense.setTransactionReferenceNumber(null);
			expense.setComment(null);
			expense.setBankName(bankName);
			expense.setChequeNumber(chequeNumber);
		}


		ExpenseType expenseType = expenseService.fetchExpenseTypeByExpenseTypeId(Long.parseLong(expenseTypeId));
		expense.setExpenseType(expenseType);

		expense.setReference(refernce);
		expense.setType(type);

		expenseService.updateExpense(expense);

		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}

	/**
	 * <pre>
	 * delete expense 
	 * delete entry from ledger by expenseId
	 * </pre>
	 * 
	 * @param model
	 * @param session
	 * @param request
	 * @return
	 */
	@Transactional
	@RequestMapping("/deleteExpense")
	public ModelAndView deleteExpense(Model model, HttpSession session, HttpServletRequest request) {

		System.out.println("in deleteExpense controller");
		Expense expense = expenseService.fetchExpense(request.getParameter("expenseId"));
		expense.setStatus(false);
		expenseService.deleteExpense(expense);

		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}

	@Transactional
	@RequestMapping("/fetchExpenseList")
	public ModelAndView fetchExpenseList(Model model, HttpSession session, HttpServletRequest request) {

		System.out.println("in fetchExpenseList controller");
		model.addAttribute("pageName", "Expense List");

		String range = request.getParameter("range");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		List<ExpenseListModel> expenseListModelList = expenseService.fetchExpenseListByRange(range, startDate, endDate);
		model.addAttribute("expenseList", expenseListModelList);

		float total = 0;
		if (expenseListModelList != null) {
			for (ExpenseListModel expenseListModel : expenseListModelList) {
				total += expenseListModel.getAmount();
			}
		}
		model.addAttribute("total", total);
		return new ModelAndView("ManageExpense");
	}

	// New Code For Expense Module and Related to Profit Loss

	@RequestMapping("expense_type_page")
	public ModelAndView displayExpenseType(HttpSession session, HttpServletRequest request, Model model) {
		return new ModelAndView("expense_type");
	}

	@RequestMapping("expense_type_check_for_add")
	public @ResponseBody String expenseTypeCheckForAdd(HttpSession session, HttpServletRequest request, Model model) {
		String expenseTypeName = request.getParameter("name");
		return expenseService.checkExistByExpenseTypeIdForAdd(expenseTypeName);
	}

	@RequestMapping("expense_type_check_for_update")
	public @ResponseBody String expenseTypeCheckForUpdate(HttpSession session, HttpServletRequest request,
			Model model) {
		String expenseTypeName = request.getParameter("name");
		long expenseTypeId = Long.parseLong(request.getParameter("expenseTypeId"));
		return expenseService.checkExistByExpenseTypeIdForUpdate(expenseTypeName, expenseTypeId);
	}

	// Add Expense Type
	@RequestMapping("save_expense_type")
	public @ResponseBody String saveExpenseType(@ModelAttribute ExpenseType expenseType, Model model,
			HttpSession session, HttpServletRequest request) {
		expenseService.saveExpenseType(expenseType);
		return Constants.SUCCESS_RESPONSE;
	}

	@RequestMapping("update_expense_type")
	public @ResponseBody String updateExpenseType(@ModelAttribute ExpenseType expenseType, Model model,
			HttpSession session, HttpServletRequest request) {
		ExpenseType expenseTypeOld = expenseService.fetchExpenseTypeByExpenseTypeId(expenseType.getExpenseTypeId());
		expenseTypeOld.setName(expenseType.getName());
		expenseTypeOld.setUpdatedDate(new Date());
		expenseService.updateExpenseType(expenseTypeOld);
		return Constants.SUCCESS_RESPONSE;
	}

	@RequestMapping("delete_expense_type")
	public @ResponseBody String deleteExpenseType(@ModelAttribute ExpenseType expenseType, Model model,
			HttpSession session, HttpServletRequest request) {
		ExpenseType expenseTypeOld = expenseService.fetchExpenseTypeByExpenseTypeId(expenseType.getExpenseTypeId());
		expenseTypeOld.setStatus(true);
		expenseService.updateExpenseType(expenseTypeOld);
		return Constants.SUCCESS_RESPONSE;
	}

	@RequestMapping("/fetchExpenseType")
	public @ResponseBody ExpenseTypeModel fetchExpenseType(Model model, HttpServletRequest request) {

		long expenseTypeId = Long.parseLong(request.getParameter("expenseTypeId"));
		ExpenseType expenseType = expenseService.fetchExpenseTypeByExpenseTypeId(expenseTypeId);
		return new ExpenseTypeModel(
				expenseType.getExpenseTypeId(), 
				expenseType.getName(),
				expenseType.getAddedDate(),
				expenseType.getUpdatedDate());
	}

	@RequestMapping("/fetchExpenseTypeList")
	public @ResponseBody List<ExpenseTypeModel> fetchExpenseTypeList(Model model, HttpServletRequest request) {

		List<ExpenseType> expenseTypeList = expenseService.fetchExpenseTypeList();
		List<ExpenseTypeModel> expenseTypeModelList = new ArrayList<>();
		if (expenseTypeList != null) {
			for (ExpenseType expenseType : expenseTypeList) {
				expenseTypeModelList.add(new ExpenseTypeModel(
						expenseType.getExpenseTypeId(), 
						expenseType.getName(),
						expenseType.getAddedDate(),
						expenseType.getUpdatedDate()));
			}
		}
		return expenseTypeModelList;
	}

}
