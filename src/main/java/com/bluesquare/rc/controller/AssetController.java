package com.bluesquare.rc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.AssetsCategory;
import com.bluesquare.rc.entities.BankPayeeDetails;
import com.bluesquare.rc.service.AssetService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class AssetController {
	
	@Autowired
	AssetService assetService;
	
	/*Payee Module*/
	/**
	 * fetch Asset Category List
	 * @param model 
	 * @param session
	 * @return ModelAndView addBankPayee.jsp
	 */
	@Transactional 	@RequestMapping("/fetchAssetCategoryList")
	public ModelAndView fetchAssetCategoryList(Model model,HttpSession session) {
		System.out.println("in Asset controller");
		model.addAttribute("pageName", "Manage Assets Category");
				 
	  
		
		List<AssetsCategory> assetsCategoryList = assetService.fetchAssetCategory();
		model.addAttribute("assetsCategoryList", assetsCategoryList);
		
		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		
		return new ModelAndView("addAssetCategory");
	}

	/**
	 * <pre>
	 * fetch Asset Category By Id
	 * For Ajax
	 * @param model
	 * @param request
	 * @return BankPayee
	 * </pre> 
	 */
	@Transactional 	@RequestMapping("/fetchAssetCategoryById")
	public @ResponseBody AssetsCategory fetchAssetCategoryById(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");
		
		long assetCategoryId = Long.parseLong(request.getParameter("assetCategoryId"));
		AssetsCategory assetsCategory = assetService.fetchAssetsCategoryById(assetCategoryId);

		return assetsCategory;
	}

	/**
	 * save Bank  
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	/* @Transactional 	@RequestMapping("/saveBankPayee")
	public ModelAndView saveBankPayee(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in Save BankPayee");

		this.bankPayeeDetails.setPayeeId(Long.parseLong(request.getParameter("payeeId")));
		this.bankPayeeDetails.setPayeeName(request.getParameter("payeeName"));
		this.bankPayeeDetails.setPayeeAddress(request.getParameter("address"));

		session.setAttribute("saveMsg", "");

		//here make first character Capital and other in small case
		bankPayeeDetails.setPayeeName((Character.toString(bankPayeeDetails.getPayeeName().charAt(0)).toUpperCase()
				+ bankPayeeDetails.getPayeeName().substring(1)));

		

		//here check payee name already exist or not 
		boolean flag = false;
		List<BankPayeeDetails> bankPayeeList = bankingService.fetchPayeeList();
		
		//check payee name with all old name
		//if found its already exist then flag will true otherwise false
		if (bankPayeeDetails.getPayeeId() == 0) {
			
			if (bankPayeeList != null) {
				for (BankPayeeDetails b : bankPayeeList) {
					if (b.getPayeeName().trim().toUpperCase()
							.equals(bankPayeeDetails.getPayeeName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			//if flag is false is then record insert other wise it send msg as Already exist
			if (flag == false) {
				bankPayeeDetails.setPayeeAddedDatetime(new Date());
				bankPayeeDetails.setPayeeUpdatedDatetime(null);
				bankingService.savePayeeDetails(bankPayeeDetails);
				session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchPayeeList");
			}
		}
		//check payee name with all old name
		//if found its already exist except checking self record then flag will true otherwise false
		if (bankPayeeDetails.getPayeeId()!= 0) {
			
			if (bankPayeeList != null) {
				for (BankPayeeDetails b : bankPayeeList) {
					if (b.getPayeeName().trim().toUpperCase().equals(bankPayeeDetails.getPayeeName().trim().toUpperCase())
							&& b.getPayeeId()!=bankPayeeDetails.getPayeeId()) {
						flag = true;
						break;
					}
				}
			}
			
			//if flag is false is then record update other wise it send msg as Already exist
			if (flag == false) 
			{
				System.out.println("Moving request for Update");
				session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
				
				return updateBankPayee(bankPayeeDetails, model,session);
			}
		}

		session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchPayeeList");

	}

	 *//**
		 * update bank Payee
		 * @param request
		 * @param model
		 * @param session
		 * @return ModelAndView redirect:/fetchBankList
		 *//*
		@Transactional 	@RequestMapping("/updateBankPayee")
		public ModelAndView updateBankPayee(@ModelAttribute BankPayeeDetails bankPayeeDetails, Model model,HttpSession session) {

			System.out.println("in Update Bank");
				
			//only user allowed to update Bank Payee  if its payeeId is not zero
			if (bankPayeeDetails.getPayeeId() == 0 || bankPayeeDetails.getPayeeName() == null
					|| bankPayeeDetails.getPayeeName().equals("")) {
				System.out.println("can not update because 0 Payee not available");
				return new ModelAndView("redirect:/fetchPayeeList");
			}
			
			this.bankPayeeDetails = bankingService.fetchPayeeById(bankPayeeDetails.getPayeeId());
			bankPayeeDetails.setPayeeAddedDatetime(this.bankPayeeDetails.getPayeeAddedDatetime());
			bankPayeeDetails.setPayeeUpdatedDatetime(new Date());
			bankingService.updatePayeeDetails(bankPayeeDetails);
			session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
			return new ModelAndView("redirect:/fetchPayeeList");

		}*/

		

}
