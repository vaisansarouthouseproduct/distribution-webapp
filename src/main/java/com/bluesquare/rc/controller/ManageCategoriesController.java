package com.bluesquare.rc.controller;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 19-05-2018 Code Documentation
 * API End Points
 * 1.fetchCategoriesList
 * 2.fetchCategories
 * 3.saveCategories
 * 4.updateCategories
 * </pre>
 */
@Controller
public class ManageCategoriesController {

	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SupplierService supplierService; 
	
	@Autowired
	Categories categories;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	BranchService branchService;
	
	@Transactional 	@RequestMapping("/fetchCategoriesList")
	public ModelAndView fetchCategoriesList(Model model,HttpSession session) {
		System.out.println("in fetchCategoriesList controller");
		
		model.addAttribute("pageName", "Category List");
	
		List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
		model.addAttribute("categorieslist", categoriesList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageCategories");
	}
	
	@Transactional 	@RequestMapping("/fetchCategories")
	public @ResponseBody CategoriesModel fetchCategories(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchCategories controller");
		
		long categoriesId=Long.parseLong(request.getParameter("categoriesId"));
		Categories categories=categoriesService.fetchCategoriesForWebApp(categoriesId);
		
		return new CategoriesModel(
				categories.getCategoryId(), 
				categories.getCategoryName(), 
				categories.getCategoryDescription(), 
				categories.getHsnCode(), 
				categories.getCgst(),
				categories.getSgst(), 
				categories.getIgst(), 
				categories.getCategoryDate(), 
				categories.getCategoryUpdateDate());
	}
	
		/**
		 * check category name duplication
		 * if category id is zero then save otherwise update category 
		 * @param request
		 * @param model
		 * @param session
		 * @return
		 */
		@Transactional 	@RequestMapping("/saveCategories")
		public ModelAndView saveCategoriesForWeb(HttpServletRequest request,Model model,HttpSession session)  {
			System.out.println("in Save Categories");		
		
			this.categories.setCategoryId(Long.parseLong(request.getParameter("categoriesId")));
			this.categories.setCategoryName(request.getParameter("categoryName"));
			this.categories.setHsnCode(request.getParameter("hsnCode"));
			this.categories.setIgst(Float.parseFloat(request.getParameter("igst")));
			this.categories.setCgst(Float.parseFloat(request.getParameter("cgst")));
			this.categories.setSgst(Float.parseFloat(request.getParameter("sgst")));
						
			session.setAttribute("saveMsg", "");
						
			categories.setCategoryName((Character.toString(categories.getCategoryName().charAt(0)).toUpperCase() + categories.getCategoryName().substring(1)));
			
			
			
			boolean flag = false;
			List<CategoriesModel> categoriesList=categoriesService.fetchCategoriesModelList();
			if(categoriesList!=null){
				for (CategoriesModel categoriesFromDb : categoriesList) {
					if (categoriesFromDb.getCategoryName().trim().toUpperCase().equals(categories.getCategoryName().trim().toUpperCase())
							) {
						flag = true;
						break;
					}
				}
			}
			
			if (categories.getCategoryId() == 0 ) 
			{
				if(flag == false)
				{
					categories.setCategoryDate(new Date());
					categories.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
					categoriesService.saveCategoriesForWebApp(categories);
					session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
					return new ModelAndView("redirect:/fetchCategoriesList");
				}
			}
			
			if (categories.getCategoryId() != 0 ) {
				
				flag = false;
				if(categoriesList!=null){
					for (CategoriesModel categoriesFromDb : categoriesList) {
						if ( categoriesFromDb.getCategoryName().trim().toUpperCase().equals(categories.getCategoryName().trim().toUpperCase())
							 && categoriesFromDb.getCategoryId()!=categories.getCategoryId()) {
							flag = true;
							break;
						}
					}
				}
				
				if(flag == false)
				{
					System.out.println("Moving request for Update");
					session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateCategoriesForWeb(categories,model,session);
				}
			}
			
			/*if (!flag) 
			{
				
				if (Categories.getCategoriesId() != 0) 
				{
					System.out.println("Moving request for Update");
					model.addAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateCategoriesForWeb(Categories);
				}
			}	*/	
			
			session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchCategoriesList");

		}
		/**
		 * update category
		 * when change category tax slab then change unit price basis of tax slab
		 * @param categories
		 * @param model
		 * @param session
		 * @return
		 */
		@Transactional 	@RequestMapping("/updateCategories")
		public ModelAndView updateCategoriesForWeb(@ModelAttribute Categories categories,Model model,HttpSession session) {

			System.out.println("in Update Country");

			
			if (categories.getCategoryId() == 0 || categories.getCategoryName() == null || categories.getCategoryName().equals("")) {
				System.out.println("con not update because 0 city not available");
				return new ModelAndView("redirect:/fetchCategoriesList");
			}
			
			this.categories=categoriesService.fetchCategoriesForWebApp(categories.getCategoryId());
			float igst=this.categories.getIgst();
			categories.setCategoryDate(this.categories.getCategoryDate());
			categories.setCompany(this.categories.getCompany());
			categories.setBranch(this.categories.getBranch());
			categories.setCategoryUpdateDate(new Date());
			categoriesService.updateCategoriesForWebApp(categories);

			List<Product> productList=productService.fetchProductByCategoryIdForWebApp(categories.getCategoryId());
			DecimalFormat decimalFormat=new DecimalFormat("#.##");
			if(productList!=null)
			{
				for(Product product: productList)
				{
					double mrp=product.getRate()+((product.getRate()*igst)/100);
					mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
					//double unipPrice=(mrp)
					double divisor=(categories.getIgst()/100)+1;
					double unitprice=(mrp/divisor);
					product.setRate((float)Double.parseDouble(decimalFormat.format(unitprice)));
					productService.updateProductForWebApp(product);
				}
			}
			
			List<SupplierProductList> supplierProductLists=supplierService.fetchSupplierProductListForChangeUnitPriceByTaxSlab(categories.getCategoryId());
			if(supplierProductLists!=null)
			{
				for(SupplierProductList supplierProductList: supplierProductLists)
				{
					double mrp=supplierProductList.getSupplierRate()+((supplierProductList.getSupplierRate()*igst)/100);
					mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
					//double unipPrice=(mrp)
					double divisor=(categories.getIgst()/100)+1;
					double unitprice=(mrp/divisor);
					supplierProductList.setSupplierRate((float)Double.parseDouble(decimalFormat.format(unitprice)));
					supplierService.updateSupplierProductList(supplierProductList);
				}
			}
			
			return new ModelAndView("redirect:/fetchCategoriesList");

		}

}
