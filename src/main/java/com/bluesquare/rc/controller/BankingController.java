package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.BankingDAO;
import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Bank;
import com.bluesquare.rc.entities.BankAccount;
import com.bluesquare.rc.entities.BankAccountTempTransaction;
import com.bluesquare.rc.entities.BankAccountTransaction;
import com.bluesquare.rc.entities.BankCBTemplateDesign;
import com.bluesquare.rc.entities.BankChequeBook;
import com.bluesquare.rc.entities.BankChequeEntry;
import com.bluesquare.rc.entities.BankPayeeDetails;
import com.bluesquare.rc.models.BankChequeBookModel;
import com.bluesquare.rc.models.BankChequeEntryModel;
import com.bluesquare.rc.models.BankModel;
import com.bluesquare.rc.models.BankPayeeDetailsModel;
import com.bluesquare.rc.models.BulkPrintSaveModel;
import com.bluesquare.rc.models.BulkPrintSaveModelRequest;
import com.bluesquare.rc.models.CheckBankAccountNumberRequest;
import com.bluesquare.rc.models.ChequePrintReportData;
import com.bluesquare.rc.models.FetchAccountInfoRequest;
import com.bluesquare.rc.models.FetchAccountInfoResponse;
import com.bluesquare.rc.models.FetchBankTransactionsRequest;
import com.bluesquare.rc.models.FetchBankTransactionsResponse;
import com.bluesquare.rc.models.UpdateSelectedBankAccountRequest;
import com.bluesquare.rc.models.UpdateTempTransactionRequest;
import com.bluesquare.rc.responseEntities.BankAccountModel;
import com.bluesquare.rc.responseEntities.BankAccountTempTransactionModel;
import com.bluesquare.rc.responseEntities.BankAccountTransactionModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.BankingService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DateUtils;

/**
 * <pre>
 * &#64;author Parag -- Code 
 * API End Points 
 * 1.banking
 * 2.addBankAccount
 * 3.editBankAccount
 * 4.addTransaction
 * 5.accountDetails
 * 6.updateSelectedBankAccount
 * 7.createAccount
 * 8.checkAccountNumber
 * 9.updateAccount
 * 10.fetchAccountInfo
 * 11.fetchTransactionsForAccount
 * 12.insertTransaction
 * 13.insertTempTransaction
 * 14.updateTempTransaction
 * 
 * 15.fetchBankList
 * 16.fetchBankById
 * 17.saveBankForWeb
 * 18.updateBankForWeb
 * 
 * 19.fetchPayeeList
 * 20.fetchBankPayeeById
 * 21.saveBankPayee
 * 22.updateBankPayee
 * 
 * 23.fetchChequeBookTemplateDesignList
 * 24.fetchBankCBTemplateDesignByBankId
 * fetchChequeNoListByCBId
 * 25.printChequeBook
 * 26.fetchBankCBListByBankAccountId
 * 27.manageChequeTemplate
 * 28.addChequeTemplate
 * 29.saveChequeTemplate
 * 30.updateChequeTemplate
 * 31.fetchBankCBTemplateDesignById
 * 32.manualChequePrint
 * 33.saveManualCheque
 * 34.savePrintedChequeFromCB
 * 35.fetchChqTemplateById
 * 36. fetchChequeTempalteDesigntByCBId
 * 37.chequeCancelPage
 * 38.fetchChequeDetaisByChequeNo
 * 39.saveCancelChequeEntry
 * 40.saveBulkPrintCheque
 * 41.openChequeReport
 * </pre>
 */

@Controller
public class BankingController {

	@Autowired
	private BankingDAO bankingDAO;

	@Autowired
	private TokenHandlerDAO tokenHandlerDAO;

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private BankingService bankingService;

	@Autowired
	Bank bank;

	@Autowired
	BankPayeeDetails bankPayeeDetails;

	@Autowired
	BankChequeBook bankChequeBook;

	@Transactional
	@RequestMapping("/banking")
	public ModelAndView banking(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("banking");
		List<BankAccount> bankAccounts = bankingDAO.fetchBankAccounts();

		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccounts != null) {
			for (BankAccount bankAccount : bankAccounts) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		mv.addObject("bankAccounts", bankAccountModelList);
		mv.addObject("pageName", "Manage Banking");
		return mv;
	}

	@RequestMapping("/addBankAccount")
	public ModelAndView addBankAccount(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("addBankAccount");
		mv.addObject("pageName", "Add Bank Account");

		List<Bank> bankList = bankingService.fetchBankList();

		mv.addObject("bankList", bankList);
		return mv;
	}

	@RequestMapping("/editBankAccount")
	public ModelAndView editBankAccount(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("editBankAccount");
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");
		mv.addObject("bankAccount", new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
				bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
				bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(), bankAccount.getIfscCode(),
				bankAccount.getBranchCode(), bankAccount.getBankAddress(), bankAccount.getMinimumBalance(),
				bankAccount.getActualBalance(), bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
				bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(), bankAccount.getUpdatedDate(),
				bankAccount.isStatus(), bankAccount.getPrimary()));

		List<Bank> bankList = bankingService.fetchBankList();
		List<BankModel> bankListRes = new ArrayList<>();
		for (int i = 0; i < bankList.size(); i++) {
			Bank bank = bankList.get(i);
			bankListRes.add(new BankModel(bank.getBankName(), bank.getShortName(), bank.getBankAddedDatetime(),
					bank.getBankUpdatedDatetime(), bank.getBankId()));
		}
		mv.addObject("bankList", bankListRes);

		mv.addObject("pageName", "Update Bank Account");
		return mv;
	}

	@RequestMapping("/addTransaction")
	public ModelAndView addTransaction(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("addTransaction");
		mv.addObject("pageName", "Add Transaction");
		return mv;
	}

	@Transactional
	@RequestMapping("/accountDetails")
	public ModelAndView accountDetails(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("accountDetails");
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");
		mv.addObject("bankAccount", new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
				bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
				bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(), bankAccount.getIfscCode(),
				bankAccount.getBranchCode(), bankAccount.getBankAddress(), bankAccount.getMinimumBalance(),
				bankAccount.getActualBalance(), bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
				bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(), bankAccount.getUpdatedDate(),
				bankAccount.isStatus(), bankAccount.getPrimary()));
		return mv;
	}

	@Transactional
	@RequestMapping("/updateSelectedBankAccount")
	public ResponseEntity<BaseDomain> updateSelectedBankAccount(
			@RequestBody UpdateSelectedBankAccountRequest selectedBankAccountRequest, HttpServletRequest request,
			HttpSession session) {
		session.setAttribute("bankAccountId", selectedBankAccountRequest.getBankAccountId());
		BaseDomain baseDomain = new BaseDomain();
		baseDomain.setSuccess(true);
		baseDomain.setMsg("");
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);

	}

	@Transactional
	@RequestMapping("/createAccount")
	public ResponseEntity<BaseDomain> createAccount(@RequestBody BankAccount bankAccount, HttpServletRequest request,
			HttpSession session) {

		Bank bank = new Bank();
		bank = (bankingService.fetchBankByBankId(bankAccount.getBank().getBankId()));
		bankAccount.setBank(bank);

		bankAccount.setCompany(
				companyDAO.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds())));
		String currentDateTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		bankAccount.setInsertedDate(currentDateTime);
		BaseDomain baseDomain = new BaseDomain();
		if (bankingDAO.accountNumberUsed(bankAccount.getAccountNumber())) {
			baseDomain.setSuccess(false);
			baseDomain.setMsg("Account Number Already Used'");
		} else {

			bankingDAO.insertAccount(bankAccount);
			baseDomain.setSuccess(true);
			baseDomain.setMsg("");
		}

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping("/checkAccountNumber")
	public ResponseEntity<BaseDomain> checkAccountNumber(
			@RequestBody CheckBankAccountNumberRequest accountNumberRequest, HttpServletRequest request,
			HttpSession session) {
		BaseDomain baseDomain = new BaseDomain();
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		List<BankAccount> bankAccounts = bankingDAO.fetchBankAccounts();
		if (bankAccounts.size() == 0) {

			baseDomain.setSuccess(true);
			baseDomain.setMsg("");

		} else {

			BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");

			if (!bankAccount.getAccountNumber().equalsIgnoreCase(accountNumberRequest.getAccountNumber())
					&& bankingDAO.accountNumberUsed(accountNumberRequest.getAccountNumber())) {
				baseDomain.setSuccess(false);
				baseDomain.setMsg("Account Number Already Used'");
			} else {
				baseDomain.setSuccess(true);
				baseDomain.setMsg("");
			}
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);

	}

	@Transactional
	@RequestMapping("/updateAccount")
	public ResponseEntity<BaseDomain> updateAccount(@RequestBody BankAccount bankAccount, HttpServletRequest request,
			HttpSession session) {
		BaseDomain baseDomain = new BaseDomain();
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccountSession = bankingDAO.fetchBankAccountById(bankAccountId + "");

		if (!bankAccountSession.getAccountNumber().equalsIgnoreCase(bankAccount.getAccountNumber())
				&& bankingDAO.accountNumberUsed(bankAccount.getAccountNumber())) {
			baseDomain.setSuccess(false);
			baseDomain.setMsg("Account Number Already Used'");
		} else {
			String currentDateTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
			bankAccountSession.setUpdatedDate(currentDateTime);
			bankAccountSession.setAccountHolderName(bankAccount.getAccountHolderName());
			bankAccountSession.setAccountNumber(bankAccount.getAccountNumber());
			bankAccountSession.setAccountType(bankAccount.getAccountType());
			bankAccountSession.setBankAddress(bankAccount.getBankAddress());
			bankAccountSession.setBank(bankAccount.getBank());
			bankAccountSession.setBankAddress(bankAccount.getBankAddress());
			bankAccountSession.setBranchCode(bankAccount.getBranchCode());
			bankAccountSession.setIfscCode(bankAccount.getIfscCode());
			bankAccountSession.setMinimumBalance(bankAccount.getMinimumBalance());
			bankAccountSession.setPrimary(bankAccount.getPrimary());
			double drawableBalance = bankAccountSession.getActualBalance() - bankAccountSession.getMinimumBalance();
			bankAccountSession.setDrawableBalance(drawableBalance < 0 ? 0 : drawableBalance);
			bankingDAO.updateAccount(bankAccountSession);
			baseDomain.setSuccess(true);
			baseDomain.setMsg("");
		}

		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);

	}

	@Transactional
	@RequestMapping("/fetchAccountInfo")
	public ResponseEntity<FetchAccountInfoResponse> fetchAccountInfo(
			@RequestBody FetchAccountInfoRequest fetchAccountInfoRequest, HttpServletRequest request,
			HttpSession session) {
		FetchAccountInfoResponse fetchAccountInfoResponse = new FetchAccountInfoResponse();
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(fetchAccountInfoRequest.getId());
		fetchAccountInfoResponse.setBankAccount(new BankAccountModel(bankAccount.getId(),
				bankAccount.getAccountHolderName(), bankAccount.getAccountNumber(), bankAccount.getAccountType(),
				bankAccount.getBank().getBankId(), bankAccount.getBank().getBankName(),
				bankAccount.getBank().getShortName(), bankAccount.getIfscCode(), bankAccount.getBranchCode(),
				bankAccount.getBankAddress(), bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
				bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
				bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(), bankAccount.getUpdatedDate(),
				bankAccount.isStatus(), bankAccount.getPrimary()));
		fetchAccountInfoResponse.setSuccess(true);
		return new ResponseEntity<FetchAccountInfoResponse>(fetchAccountInfoResponse, HttpStatus.OK);

	}

	@Transactional
	@RequestMapping("/fetchTransactionsForAccount")
	public ResponseEntity<FetchBankTransactionsResponse> fetchAccounts(
			@RequestBody FetchBankTransactionsRequest fetchBankTransactionsRequest, HttpServletRequest request,
			HttpSession session) {
		FetchBankTransactionsResponse fetchBankTransactionsResponse = new FetchBankTransactionsResponse();
		String transactionType = fetchBankTransactionsRequest.getTransactionType();
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		String fromDate = fetchBankTransactionsRequest.getFromDate();
		String toDate = fetchBankTransactionsRequest.getToDate();
		fromDate += " 00:00:01";
		toDate += " 23:59:59";
		int transactionCount = fetchBankTransactionsRequest.getTransactionCount();
		List<BankAccountTransaction> bankAccountTransactions = new ArrayList<>();
		List<BankAccountTempTransaction> bankAccountTempTransactions = new ArrayList<>();

		if (transactionType.equalsIgnoreCase(Constants.TRANSACTION_TYPE_ALL)) {
			bankAccountTransactions = bankingDAO.fetchTransactions(bankAccountId, fromDate, toDate, transactionCount);
			if (!bankAccountTransactions.isEmpty()) {
				int lastIndex = bankAccountTransactions.size() - 1;
				fetchBankTransactionsResponse.setOpening(bankAccountTransactions.get(lastIndex).getOpening());
				fetchBankTransactionsResponse.setClosing(bankAccountTransactions.get(0).getClosing());
			}
		} else if (transactionType.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_ISSUED)) {
			bankAccountTempTransactions = bankingDAO.fetchTempTransactions(bankAccountId, fromDate, toDate,
					Constants.TRANSACTION_TYPE_CHEQUE_ISSUED, transactionCount);
		} else if (transactionType.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_DEPOSITED)) {
			bankAccountTempTransactions = bankingDAO.fetchTempTransactions(bankAccountId, fromDate, toDate,
					Constants.TRANSACTION_TYPE_CHEQUE_DEPOSITED, transactionCount);
		} else if (transactionType.equalsIgnoreCase(Constants.TRANSACTION_TYPE_CHEQUE_BOUNCED)) {
			bankAccountTempTransactions = bankingDAO.fetchTempTransactions(bankAccountId, fromDate, toDate,
					Constants.TRANSACTION_TYPE_CHEQUE_BOUNCED, transactionCount);
		}

		// reverse the list if ordering needed is ascending default ordering is
		// descending
		if (fetchBankTransactionsRequest.getOrdering().equalsIgnoreCase(Constants.TRANSACTION_ORDERING_ASC)) {
			Collections.reverse(bankAccountTransactions);
			Collections.reverse(bankAccountTempTransactions);
		}
		List<BankAccountTransactionModel> bankAccountTransactionList = new ArrayList<>();
		List<BankAccountTempTransactionModel> bankAccountTempTransactionList = new ArrayList<>();

		if (bankAccountTransactions != null) {
			for (BankAccountTransaction bankAccountTransaction : bankAccountTransactions) {
				bankAccountTransactionList.add(new BankAccountTransactionModel(bankAccountTransaction.getId(),
						bankAccountTransaction.getTransactionId(), bankAccountTransaction.getPartyName(),
						bankAccountTransaction.getOpening(), bankAccountTransaction.getCredit(),
						bankAccountTransaction.getDebit(), bankAccountTransaction.getClosing(),
						bankAccountTransaction.getPayMode(), bankAccountTransaction.getReferenceNo(),
						bankAccountTransaction.getBankName(), bankAccountTransaction.getInsertedDate(),
						bankAccountTransaction.getChequeDate()));
			}
		}
		if (bankAccountTempTransactions != null) {
			for (BankAccountTempTransaction bankAccountTempTransaction : bankAccountTempTransactions) {
				bankAccountTempTransactionList.add(new BankAccountTempTransactionModel(
						bankAccountTempTransaction.getId(), bankAccountTempTransaction.getTransactionId(),
						bankAccountTempTransaction.getPartyName(), bankAccountTempTransaction.getCredit(),
						bankAccountTempTransaction.getDebit(), bankAccountTempTransaction.getPayMode(),
						bankAccountTempTransaction.getReferenceNo(), bankAccountTempTransaction.getBankName(),
						bankAccountTempTransaction.getInsertedDate(), bankAccountTempTransaction.getChequeDate(),
						bankAccountTempTransaction.getTransactionStatus(), bankAccountTempTransaction.getReason(),
						bankAccountTempTransaction.getUpdatedDate()));
			}
		}
		fetchBankTransactionsResponse.setBankAccountTempTransactions(bankAccountTempTransactionList);
		fetchBankTransactionsResponse.setBankAccountTransactions(bankAccountTransactionList);
		fetchBankTransactionsResponse.setSuccess(true);

		return new ResponseEntity<FetchBankTransactionsResponse>(fetchBankTransactionsResponse, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping("/insertTransaction")
	public ResponseEntity<BaseDomain> createTransaction(@RequestBody BankAccountTransaction bankAccountTransaction,
			HttpServletRequest request, HttpSession session) {
		String currentDateTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		bankAccountTransaction.setInsertedDate(currentDateTime);
		String payMode = bankAccountTransaction.getPayMode();
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");
		double debit = bankAccountTransaction.getDebit();
		BaseDomain baseDomain = new BaseDomain();
		if (!payMode.equalsIgnoreCase(Constants.PAY_MODE_CHEQUE) && debit > bankAccount.getDrawableBalance()) {
			baseDomain.setMsg("Insufficient Balance");
			baseDomain.setSuccess(false);
		} else {
			if (payMode.equalsIgnoreCase(Constants.PAY_MODE_CHEQUE)) {
				BankAccountTempTransaction bankAccountTempTransaction = new BankAccountTempTransaction();
				bankAccountTempTransaction.setBankAccount(bankAccount);
				bankAccountTempTransaction.setBankName(bankAccountTransaction.getBankName());
				bankAccountTempTransaction.setChequeDate(bankAccountTransaction.getChequeDate());
				bankAccountTempTransaction.setInsertedDate(bankAccountTransaction.getInsertedDate());
				bankAccountTempTransaction.setCredit(bankAccountTransaction.getCredit());
				bankAccountTempTransaction.setDebit(bankAccountTransaction.getDebit());
				bankAccountTempTransaction.setPartyName(bankAccountTransaction.getPartyName());
				bankAccountTempTransaction.setPayMode(payMode);
				bankAccountTempTransaction.setTransactionStatus(Constants.TRANSACTION_PROCESSING);
				bankAccountTempTransaction.setReferenceNo(bankAccountTransaction.getReferenceNo());
				bankingDAO.insertTempTransaction(bankAccountTempTransaction);
			} else {
				bankAccountTransaction.setBankAccount(bankAccount);
				bankingDAO.insertTransaction(bankAccountTransaction);
			}
			baseDomain.setMsg("");
			baseDomain.setSuccess(true);
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping("/insertTempTransaction")
	public ResponseEntity<BaseDomain> insertTempTransaction(@RequestBody BankAccountTransaction bankAccountTransaction,
			HttpServletRequest request, HttpSession session) {
		String currentDateTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		bankAccountTransaction.setInsertedDate(currentDateTime);
		String payMode = bankAccountTransaction.getPayMode();
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");
		double debit = bankAccountTransaction.getDebit();
		BaseDomain baseDomain = new BaseDomain();
		if (debit > bankAccount.getDrawableBalance()) {
			baseDomain.setMsg("Insufficient Balance");
			baseDomain.setSuccess(false);
		} else {
			if (payMode.equalsIgnoreCase(Constants.PAY_MODE_CHEQUE)) {
				BankAccountTempTransaction bankAccountTempTransaction = new BankAccountTempTransaction();
				bankAccountTempTransaction.setBankAccount(bankAccount);
				bankAccountTempTransaction.setBankName(bankAccountTransaction.getBankName());
				bankAccountTempTransaction.setChequeDate(bankAccountTransaction.getChequeDate());
				bankAccountTempTransaction.setInsertedDate(bankAccountTransaction.getInsertedDate());
				bankAccountTempTransaction.setCredit(bankAccountTransaction.getCredit());
				bankAccountTempTransaction.setDebit(bankAccountTransaction.getDebit());
				bankAccountTempTransaction.setPartyName(bankAccountTransaction.getPartyName());
				bankAccountTempTransaction.setPayMode(payMode);
				bankAccountTempTransaction.setTransactionStatus(Constants.TRANSACTION_PROCESSING);
				bankAccountTempTransaction.setReferenceNo(bankAccountTransaction.getReferenceNo());
				bankingDAO.insertTempTransaction(bankAccountTempTransaction);
			} else {
				bankAccountTransaction.setBankAccount(bankAccount);
				bankingDAO.insertTransaction(bankAccountTransaction);
			}
			baseDomain.setMsg("");
			baseDomain.setSuccess(true);
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping("/updateTempTransaction")
	public ResponseEntity<BaseDomain> updateTempTransaction(
			@RequestBody UpdateTempTransactionRequest updateTempTransactionRequest, HttpServletRequest request,
			HttpSession session) {
		BankAccountTempTransaction bankAccountTempTransaction = bankingDAO
				.fetchTempTransactionById(updateTempTransactionRequest.getId());
		String currentDateTime = DateUtils.getDateTimeStringFromCalendar(Calendar.getInstance());
		bankAccountTempTransaction.setUpdatedDate(currentDateTime);
		bankAccountTempTransaction.setReason(updateTempTransactionRequest.getReason());
		Long bankAccountId = (Long) session.getAttribute("bankAccountId");
		BankAccount bankAccount = bankingDAO.fetchBankAccountById(bankAccountId + "");
		double debit = bankAccountTempTransaction.getDebit();
		BaseDomain baseDomain = new BaseDomain();
		String updatedStatus = updateTempTransactionRequest.getUpdatedStatus();
		if (!updatedStatus.equalsIgnoreCase(Constants.TRANSACTION_BOUNCED)
				&& debit > bankAccount.getDrawableBalance()) {
			baseDomain.setMsg("Insufficient Balance");
			baseDomain.setSuccess(false);
		} else {
			bankAccountTempTransaction.setTransactionStatus(updatedStatus);
			bankingDAO.updateTempTransaction(bankAccountTempTransaction);
			if (updatedStatus.equalsIgnoreCase(Constants.TRANSACTION_CLEARED)) {
				BankAccountTransaction bankAccountTransaction = new BankAccountTransaction();
				bankAccountTransaction.setBankAccount(bankAccount);
				bankAccountTransaction.setBankName(bankAccountTempTransaction.getBankName());
				bankAccountTransaction.setChequeDate(bankAccountTempTransaction.getChequeDate());
				bankAccountTransaction.setInsertedDate(currentDateTime);
				bankAccountTransaction.setCredit(bankAccountTempTransaction.getCredit());
				bankAccountTransaction.setDebit(bankAccountTempTransaction.getDebit());
				bankAccountTransaction.setPartyName(bankAccountTempTransaction.getPartyName());
				bankAccountTransaction.setPayMode(bankAccountTempTransaction.getPayMode());
				bankAccountTransaction.setReferenceNo(bankAccountTempTransaction.getReferenceNo());
				bankingDAO.insertTransaction(bankAccountTransaction);
			}
			baseDomain.setMsg("");
			baseDomain.setSuccess(true);
		}
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	/* Banking Module */

	/**
	 * fetch Bank list
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView addBank.jsp
	 */
	@Transactional
	@RequestMapping("/fetchBankList")
	public ModelAndView fetchBankList(Model model, HttpSession session) {
		System.out.println("in manageSupplier controller");
		model.addAttribute("pageName", "Manage Bank");

		List<Bank> bankList = bankingService.fetchBankList();
		if (bankList != null) {
			List<BankModel> bankModels = new ArrayList<>();
			for (int i = 0; i < bankList.size(); i++) {
				Bank bank = bankList.get(i);
				bankModels.add(new BankModel(bank.getBankName(), bank.getShortName(), bank.getBankAddedDatetime(),
						bank.getBankUpdatedDatetime(), bank.getBankId()));
			}
			model.addAttribute("bankList", bankModels);
		}
		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");

		return new ModelAndView("addBank");
	}

	/**
	 * <pre>
	 * fetch Bank  by bank  Id
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return Bank
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchBankById")
	public @ResponseBody BankModel fetchBankById(Model model, HttpServletRequest request) {
		System.out.println("in fetchBrand controller");

		long bankId = Long.parseLong(request.getParameter("bankId"));
		bank = bankingService.fetchBankByBankId(bankId);

		return new BankModel(bank.getBankName(), bank.getShortName(), bank.getBankAddedDatetime(),
				bank.getBankUpdatedDatetime(), bank.getBankId());
	}

	/**
	 * save Bank
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	@Transactional
	@RequestMapping("/saveBank")
	public ModelAndView saveBankForWeb(HttpServletRequest request, Model model, HttpSession session) {
		System.out.println("in Save Bank");

		this.bank.setBankId(Long.parseLong(request.getParameter("bankId")));
		this.bank.setBankName(request.getParameter("bankName"));
		this.bank.setShortName(request.getParameter("bankShortName"));

		session.setAttribute("saveMsg", "");

		// here make first character Capital and other in small case
		bank.setBankName(
				(Character.toString(bank.getBankName().charAt(0)).toUpperCase() + bank.getBankName().substring(1)));

		// here check businessType name already exist or not
		boolean flag = false;
		List<Bank> bankList = bankingService.fetchBankList();

		// check business type name with all old name
		// if found its already exist then flag will true otherwise false
		if (bank.getBankId() == 0) {

			if (bankList != null) {
				for (Bank b : bankList) {
					if (b.getBankName().trim().toUpperCase().equals(bank.getBankName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record insert other wise it send msg as
			// Already exist
			if (flag == false) {
				bank.setBankAddedDatetime(new Date());
				bank.setBankUpdatedDatetime(null);
				bankingService.saveBank(bank);
				session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchBankList");
			}
		}
		// check business type name with all old name
		// if found its already exist except checking self record then flag will
		// true otherwise false
		if (bank.getBankId() != 0) {

			if (bankList != null) {
				for (Bank b : bankList) {
					if (b.getBankName().trim().toUpperCase().equals(bank.getBankName().trim().toUpperCase())
							&& b.getBankId() != bank.getBankId()) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record update other wise it send msg as
			// Already exist
			if (flag == false) {
				System.out.println("Moving request for Update");
				session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);

				return updateBankForWeb(bank, model, session);
			}
		}

		session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchBankList");

	}

	/**
	 * update bank
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	@Transactional
	@RequestMapping("/updateBank")
	public ModelAndView updateBankForWeb(@ModelAttribute Bank bank, Model model, HttpSession session) {

		System.out.println("in Update Bank");

		// only user allowed to update business type if its businessTypeId is
		// not zero
		if (bank.getBankId() == 0 || bank.getBankName() == null || bank.getBankName().equals("")) {
			System.out.println("can not update because 0  not available");
			return new ModelAndView("redirect:/fetchBankList");
		}

		this.bank = bankingService.fetchBankByBankId(bank.getBankId());
		bank.setBankAddedDatetime(this.bank.getBankAddedDatetime());
		bank.setBankUpdatedDatetime(new Date());
		bankingService.updateBank(bank);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchBankList");

	}

	/* Payee Module */
	/**
	 * fetch PAyee list
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView addBankPayee.jsp
	 */
	@Transactional
	@RequestMapping("/fetchPayeeList")
	public ModelAndView fetchPayeeList(Model model, HttpSession session) {
		System.out.println("in Banking controller");
		model.addAttribute("pageName", "Manage Payee");

		List<BankPayeeDetails> bankPayeeDetailList = bankingService.fetchPayeeList();
		List<BankPayeeDetailsModel> bankPayeeDetailListRes = new ArrayList<>();
		if (bankPayeeDetailList != null) {
			for (int i = 0; i < bankPayeeDetailList.size(); i++) {
				BankPayeeDetails bankPayeeDetails = bankPayeeDetailList.get(i);
				bankPayeeDetailListRes.add(new BankPayeeDetailsModel(bankPayeeDetails.getPayeeId(),
						bankPayeeDetails.getPayeeName(), bankPayeeDetails.getPayeeAddress(),
						bankPayeeDetails.getPayeeAddedDatetime(), bankPayeeDetails.getPayeeUpdatedDatetime()));
			}
		}
		model.addAttribute("bankPayeeDetailList", bankPayeeDetailListRes);

		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");

		return new ModelAndView("addBankPayee");
	}

	/**
	 * <pre>
	 * fetch Bank Payee  by   Id
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankPayee
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchBankPayeeById")
	public @ResponseBody BankPayeeDetailsModel fetchBankPayeeById(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");

		long payeeId = Long.parseLong(request.getParameter("payeeId"));
		bankPayeeDetails = bankingService.fetchPayeeById(payeeId);

		return new BankPayeeDetailsModel(bankPayeeDetails.getPayeeId(), bankPayeeDetails.getPayeeName(),
				bankPayeeDetails.getPayeeAddress(), bankPayeeDetails.getPayeeAddedDatetime(),
				bankPayeeDetails.getPayeeUpdatedDatetime());
	}

	/**
	 * save Bank
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	@Transactional
	@RequestMapping("/saveBankPayee")
	public ModelAndView saveBankPayee(HttpServletRequest request, Model model, HttpSession session) {
		System.out.println("in Save BankPayee");

		this.bankPayeeDetails.setPayeeId(Long.parseLong(request.getParameter("payeeId")));
		this.bankPayeeDetails.setPayeeName(request.getParameter("payeeName"));
		this.bankPayeeDetails.setPayeeAddress(request.getParameter("address"));

		session.setAttribute("saveMsg", "");

		// here make first character Capital and other in small case
		bankPayeeDetails.setPayeeName((Character.toString(bankPayeeDetails.getPayeeName().charAt(0)).toUpperCase()
				+ bankPayeeDetails.getPayeeName().substring(1)));

		// here check payee name already exist or not
		boolean flag = false;
		List<BankPayeeDetails> bankPayeeList = bankingService.fetchPayeeList();

		// check payee name with all old name
		// if found its already exist then flag will true otherwise false
		if (bankPayeeDetails.getPayeeId() == 0) {

			if (bankPayeeList != null) {
				for (BankPayeeDetails b : bankPayeeList) {
					if (b.getPayeeName().trim().toUpperCase()
							.equals(bankPayeeDetails.getPayeeName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record insert other wise it send msg as
			// Already exist
			if (flag == false) {
				bankPayeeDetails.setPayeeAddedDatetime(new Date());
				bankPayeeDetails.setPayeeUpdatedDatetime(null);
				bankingService.savePayeeDetails(bankPayeeDetails);
				session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchPayeeList");
			}
		}
		// check payee name with all old name
		// if found its already exist except checking self record then flag will
		// true otherwise false
		if (bankPayeeDetails.getPayeeId() != 0) {

			if (bankPayeeList != null) {
				for (BankPayeeDetails b : bankPayeeList) {
					if (b.getPayeeName().trim().toUpperCase()
							.equals(bankPayeeDetails.getPayeeName().trim().toUpperCase())
							&& b.getPayeeId() != bankPayeeDetails.getPayeeId()) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record update other wise it send msg as
			// Already exist
			if (flag == false) {
				System.out.println("Moving request for Update");
				session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);

				return updateBankPayee(bankPayeeDetails, model, session);
			}
		}

		session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchPayeeList");

	}

	/**
	 * update bank Payee
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	@Transactional
	@RequestMapping("/updateBankPayee")
	public ModelAndView updateBankPayee(@ModelAttribute BankPayeeDetails bankPayeeDetails, Model model,
			HttpSession session) {

		System.out.println("in Update Bank");

		// only user allowed to update Bank Payee if its payeeId is not zero
		if (bankPayeeDetails.getPayeeId() == 0 || bankPayeeDetails.getPayeeName() == null
				|| bankPayeeDetails.getPayeeName().equals("")) {
			System.out.println("can not update because 0 Payee not available");
			return new ModelAndView("redirect:/fetchPayeeList");
		}

		this.bankPayeeDetails = bankingService.fetchPayeeById(bankPayeeDetails.getPayeeId());
		bankPayeeDetails.setPayeeAddedDatetime(this.bankPayeeDetails.getPayeeAddedDatetime());
		bankPayeeDetails.setPayeeUpdatedDatetime(new Date());
		bankingService.updatePayeeDetails(bankPayeeDetails);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchPayeeList");

	}

	/* CheckBook Module */
	/**
	 * fetch PAyee list
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView addBankPayee.jsp
	 */
	@Transactional
	@RequestMapping("/fetchCheckBookList")
	public ModelAndView fetchCheckBookList(Model model, HttpSession session) {
		System.out.println("In CheckBook Controller");
		model.addAttribute("pageName", "Manage Cheque Book");

		List<BankChequeBook> bankChequeBookDetailList = bankingService.fetchChequeBookList();

		List<BankChequeBookModel> bankChequeBookModelList = new ArrayList<>();
		if (bankChequeBookDetailList != null) {

			for (int i = 0; i < bankChequeBookDetailList.size(); i++) {
				BankChequeBook bankChequeBook = bankChequeBookDetailList.get(i);
				BankAccount bankAccount = bankChequeBook.getBankAccount();
				bankChequeBookModelList.add(new BankChequeBookModel(
						new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
								bankAccount.getAccountNumber(), bankAccount.getAccountType(),
								bankAccount.getBank().getBankId(), bankAccount.getBank().getBankName(),
								bankAccount.getBank().getShortName(), bankAccount.getIfscCode(),
								bankAccount.getBranchCode(), bankAccount.getBankAddress(),
								bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
								bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
								bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
								bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()),
						bankChequeBook.getSrtChqNo(), bankChequeBook.getEndChqNo(), bankChequeBook.getNumberOfLeaves(),
						bankChequeBook.getNumberOfUnusedLeaves()));
			}
		}
		model.addAttribute("bankChequeBookDetailList", bankChequeBookModelList);

		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute(""));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("manageChequeBook");

	}

	/**
	 * add Cheque Book
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView addChequeBook.jsp
	 */
	@Transactional
	@RequestMapping("/addChequeBook")
	public ModelAndView addChequeBook(Model model, HttpSession session) {

		model.addAttribute("pageName", "Add Cheque Book");

		System.out.println("in Banking controller");

		List<BankAccount> bankAccountList = bankingDAO.fetchBankAccounts();

		model.addAttribute("bankAccountList", bankAccountList);

		return new ModelAndView("addChequeBook");
	}

	/**
	 * Cheque Book
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 * @throws ParseException
	 */
	@Transactional
	@RequestMapping("/saveChequeBook")
	public ModelAndView saveChequeBook(HttpServletRequest request, Model model, HttpSession session)
			throws ParseException {
		System.out.println("in Save Cheque Book");

		// this.bankChequeBook.setChequeBookId(Long.parseLong(request.getParameter("chequeBookId")));

		String bankAccountId = request.getParameter("bankAccountId");
		bankAccountId = bankAccountId.split("~")[1];
		long bankAccountId1 = Long.parseLong(bankAccountId);
		BankAccount bankAccount = new BankAccount();
		bankAccount.setId(bankAccountId1);
		bankAccount = bankingService.fetchBankAccountByLongId(bankAccountId1);

		long chqTemplateDesignId = Long.parseLong(request.getParameter("bankCBTemplateId"));
		BankCBTemplateDesign bankCBTemplateDesign = bankingService.fetchChequeTemplateDesignById(chqTemplateDesignId);

		this.bankChequeBook.setBankCBTemplateDesign(bankCBTemplateDesign);
		this.bankChequeBook.setBankAccount(bankAccount);
		this.bankChequeBook.setNumberOfLeaves(Long.parseLong(request.getParameter("noOfLeaves")));
		this.bankChequeBook.setNumberOfUnusedLeaves(Long.parseLong(request.getParameter("noOfLeaves")));
		this.bankChequeBook.setSrtChqNo(request.getParameter("startingChqNo"));
		this.bankChequeBook.setEndChqNo(request.getParameter("endChequeNo"));

		this.bankChequeBook
				.setChqBookAddedDateTime(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("chequeDate")));
		this.bankChequeBook.setChqBookName(bankAccount.getBank().getShortName() + ":"
				+ this.bankChequeBook.getSrtChqNo() + "-" + this.bankChequeBook.getEndChqNo());
		session.setAttribute("saveMsg", "");

		boolean flag = false;
		List<BankChequeBook> bankChequeBookList = bankingService.fetchChequeBookList();
		if (bankChequeBookList != null) {
			for (BankChequeBook b : bankChequeBookList) {
				for (long i = Long.parseLong(request.getParameter("startingChqNo")); i <= Long
						.parseLong(request.getParameter("endChequeNo")); i++) {
					if (i == Long.parseLong(b.getSrtChqNo())) {
						flag = true;
					}

				}
			}

		}

		if (flag == false) {
			bankChequeBook.setChqBookAddedDateTime(new Date());
			bankChequeBook.setChqBookUpdateddateTime(null);
			bankingService.saveChequeBook(bankChequeBook);
			session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
			return new ModelAndView("redirect:/fetchCheckBookList");
		}

		session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchCheckBookList");

	}

	/**
	 * update bank Payee
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchCheckBookList
	 */
	@Transactional
	@RequestMapping("/updateChequeBook")
	public ModelAndView updateChequeBook(@ModelAttribute BankChequeBook bankChequeBook, Model model,
			HttpSession session) {

		System.out.println("in Update Bank");

		// only user allowed to update Cheque Book if its chequeBookId is not
		// zero
		if (bankChequeBook.getChequeBookId() == 0) {
			System.out.println("can not update because 0 Cheque Book not available");
			return new ModelAndView("redirect:/fetchCheckBookList");
		}

		this.bankChequeBook = bankingService.fetchBankChequeBookById(this.bankChequeBook.getChequeBookId());
		bankChequeBook.setChqBookAddedDateTime(this.bankChequeBook.getChqBookAddedDateTime());
		bankChequeBook.setChqBookUpdateddateTime(new Date());
		bankingService.updateChequeBook(bankChequeBook);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchCheckBookList");

	}

	/**
	 * Fetch cheque Book Template Design List
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchCheckBookList
	 */
	@Transactional
	@RequestMapping("/fetchChequeBookTemplateDesignList")
	public ModelAndView fetchChequeBookTemplateDesignList(Model model, HttpSession session) {
		System.out.println("In Banking Controller");
		model.addAttribute("pageName", "Manage Cheque Design template");

		List<BankCBTemplateDesign> bankCBTemplateDesignList = bankingService.fetchChequeTemplateDesignList();
		model.addAttribute("bankCBTemplateDesignList", bankCBTemplateDesignList);

		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute(""));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("manageChequeBookTemplate");
	}

	/**
	 * <pre>
	 * fetchBankCBTemplateDesignByBankId
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankCBTemplateDesign
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchBankCBTemplateDesignByBankId")
	public @ResponseBody List<BankCBTemplateDesign> fetchBankCBTemplateDesignByBankId(Model model,
			HttpServletRequest request) {
		System.out.println("in Banking controller");

		long bankId = Long.parseLong(request.getParameter("bankId"));
		List<BankCBTemplateDesign> bankCBTemplateDesignList = bankingService.fetchChequeTemplateDesignByBankId(bankId);

		return bankCBTemplateDesignList;
	}

	/**
	 * <pre>
	 * fetchChequeNoListByCBId
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return chequeNoList
	 * </pre>
	 */

	@Transactional
	@RequestMapping("/fetchChequeNoListByCBId")
	public @ResponseBody List<String> fetchChequeNoListByCBId(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");

		long chequeBookId = Long.parseLong(request.getParameter("chequeBookId"));
		List<String> chequeNoList = bankingService.fetchUnusedChequeNoListByCBId(chequeBookId);

		return chequeNoList;
	}

	/**
	 * print Cheque Book
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView chequeBookPrint.jsp
	 */

	@Transactional
	@RequestMapping("/printChequeBook")
	public ModelAndView printChequeBook(Model model, HttpSession session) {

		model.addAttribute("pageName", "Print Cheque Book");

		System.out.println("in Banking controller");

		List<BankAccount> bankAccountList = bankingDAO.fetchBankAccounts();
		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccountList != null) {
			for (BankAccount bankAccount : bankAccountList) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		

		model.addAttribute("bankAccountList", bankAccountModelList);

		return new ModelAndView("chequeBookPrint");
	}

	/**
	 * <pre>
	 * fetchBankCBTemplateDesignByBankId
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankCBTemplateDesign
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchBankCBListByBankAccountId")
	public @ResponseBody List<BankChequeBook> fetchBankCBListByBankAccountId(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");

		long bankAccountId = Long.parseLong(request.getParameter("bankAccountId"));
		List<BankChequeBook> bankChequeBookList = bankingService.fetchBankChequeBookListByAcoountId(bankAccountId);

		return bankChequeBookList;
	}

	/**
	 * manage Cheque Template Design
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView manageChequeTemplate.jsp
	 */
	@Transactional
	@RequestMapping("/manageChequeTemplate")
	public ModelAndView manageChequeTemplate(Model model, HttpSession session) {

		model.addAttribute("pageName", "Manage Cheque Template");

		System.out.println("in Banking controller");
		List<BankCBTemplateDesign> bankCBTemplateDesignList = bankingService.fetchChequeTemplateDesignList();

		model.addAttribute("bankCBTemplateDesignList", bankCBTemplateDesignList);

		return new ModelAndView("manageChequeTemplate");
	}

	/**
	 * add Cheque Template Design
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView manageChequeTemplate.jsp
	 */
	@Transactional
	@RequestMapping("/addChequeTemplate")
	public ModelAndView addChequeTemplate(Model model, HttpSession session) {

		model.addAttribute("pageName", "Manage Cheque Template");

		System.out.println("in Banking controller");
		List<Bank> bankList = bankingService.fetchBankList();

		model.addAttribute("bankList", bankList);

		return new ModelAndView("addChequeTemplate");
	}

	/**
	 * Save Cheque Template Design
	 * 
	 * @param model
	 * @param session
	 * @return ModelAndView manageChequeTemplate.jsp
	 */
	@Transactional
	@RequestMapping("/saveChequeTemplate")
	public ModelAndView saveChequeTemplate(Model model, HttpSession session, HttpServletRequest request) {

		model.addAttribute("pageName", "Add Cheque Template");
		System.out.println("Inside saveChequeTemplate ");

		BankCBTemplateDesign bankCBTemplateDesignNew = new BankCBTemplateDesign();
		bankCBTemplateDesignNew.setTemplateImgBase64(request.getParameter("chequeImageBase64"));
		bankCBTemplateDesignNew.setId(Long.parseLong(request.getParameter("chequeTemplateId")));
		bankCBTemplateDesignNew.setTemplateName(request.getParameter("chequeTemplateName"));
		long bankId = Long.parseLong(request.getParameter("bankId"));
		Bank bank = bankingService.fetchBankByBankId(bankId);

		List<BankCBTemplateDesign> bankCBTemplateDesignList = bankingService.fetchChequeTemplateDesignList();
		session.setAttribute("saveMsg", "");

		boolean flag = false;

		// check Template name with all old name
		// if found its already exist then flag will true otherwise false
		if (bankCBTemplateDesignNew.getId() == 0) {

			if (bankCBTemplateDesignList != null) {
				for (BankCBTemplateDesign b : bankCBTemplateDesignList) {
					if (b.getTemplateName().trim().toUpperCase()
							.equals(bankCBTemplateDesignNew.getTemplateName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record insert other wise it send msg as
			// Already exist
			if (flag == false) {

				bankCBTemplateDesignNew.setTemplateAddedDatetime(new Date());
				bankCBTemplateDesignNew.setTemplateUpdatedDatetime(null);
				bankCBTemplateDesignNew.setBank(bank);

				bankCBTemplateDesignNew.setChqDateTop(Double.parseDouble(request.getParameter("chqDateTop")));
				bankCBTemplateDesignNew.setChqDateLeft(Double.parseDouble(request.getParameter("chqDateLeft")));
				bankCBTemplateDesignNew.setChqDateWidth(Double.parseDouble(request.getParameter("chqDateWidth")));
				// bankCBTemplateDesignNew.setChqDateHeight(Double.parseDouble(request.getParameter("chqDateHeight")));

				bankCBTemplateDesignNew.setPayeeNameTop(Double.parseDouble(request.getParameter("payeeNameTop")));
				bankCBTemplateDesignNew.setPayeeNameLeft(Double.parseDouble(request.getParameter("payeeNameLeft")));
				bankCBTemplateDesignNew.setPayeeNameWidth(Double.parseDouble(request.getParameter("payeeNameWidth")));

				bankCBTemplateDesignNew.setAmountTop(Double.parseDouble(request.getParameter("amountTop")));
				bankCBTemplateDesignNew.setAmountLeft(Double.parseDouble(request.getParameter("amountLeft")));
				bankCBTemplateDesignNew.setAmountWidth(Double.parseDouble(request.getParameter("amountWidth")));

				bankCBTemplateDesignNew.setAmountInWordTop(Double.parseDouble(request.getParameter("amountInWordTop")));
				bankCBTemplateDesignNew
						.setAmountInWordLeft(Double.parseDouble(request.getParameter("amountInWordLeft")));
				bankCBTemplateDesignNew
						.setAmountInWordWidth(Double.parseDouble(request.getParameter("amountInWordWidth")));

				bankCBTemplateDesignNew.setAcPayeeTop(Double.parseDouble(request.getParameter("acPayeeTop")));
				bankCBTemplateDesignNew.setAcPayeeLeft(Double.parseDouble(request.getParameter("acPayeeLeft")));
				bankCBTemplateDesignNew.setAcPayeeWidth(Double.parseDouble(request.getParameter("acPayeeWidth")));

				bankCBTemplateDesignNew.setDateFormate(request.getParameter("dateFormate"));

				/*
				 * bankCBTemplateDesignNew.setD1left(Double.parseDouble(request.
				 * getParameter("d1Left")));
				 * bankCBTemplateDesignNew.setD1Top(Double.parseDouble(request.
				 * getParameter("d1Top")));
				 * bankCBTemplateDesignNew.setD1Width(Double.parseDouble(request
				 * .getParameter("d1Width")));
				 * 
				 * bankCBTemplateDesignNew.setD2left(Double.parseDouble(request.
				 * getParameter("d2Left")));
				 * bankCBTemplateDesignNew.setD2Top(Double.parseDouble(request.
				 * getParameter("d2Top")));
				 * bankCBTemplateDesignNew.setD2Width(Double.parseDouble(request
				 * .getParameter("d2Width")));
				 * 
				 * bankCBTemplateDesignNew.setM1left(Double.parseDouble(request.
				 * getParameter("m1Left")));
				 * bankCBTemplateDesignNew.setM1Top(Double.parseDouble(request.
				 * getParameter("m1Top")));
				 * bankCBTemplateDesignNew.setM1Width(Double.parseDouble(request
				 * .getParameter("m1Width")));
				 * 
				 * bankCBTemplateDesignNew.setM2left(Double.parseDouble(request.
				 * getParameter("m2Left")));
				 * bankCBTemplateDesignNew.setM2Top(Double.parseDouble(request.
				 * getParameter("m2Top")));
				 * bankCBTemplateDesignNew.setM2Width(Double.parseDouble(request
				 * .getParameter("m2Width")));
				 * 
				 * bankCBTemplateDesignNew.setY1left(Double.parseDouble(request.
				 * getParameter("y1Left")));
				 * bankCBTemplateDesignNew.setY1Top(Double.parseDouble(request.
				 * getParameter("y1Top")));
				 * bankCBTemplateDesignNew.setY1Width(Double.parseDouble(request
				 * .getParameter("y1Width")));
				 * 
				 * bankCBTemplateDesignNew.setY2left(Double.parseDouble(request.
				 * getParameter("y2Left")));
				 * bankCBTemplateDesignNew.setY2Top(Double.parseDouble(request.
				 * getParameter("y2Top")));
				 * bankCBTemplateDesignNew.setY2Width(Double.parseDouble(request
				 * .getParameter("y2Width")));
				 * 
				 * bankCBTemplateDesignNew.setY3left(Double.parseDouble(request.
				 * getParameter("y3Left")));
				 * bankCBTemplateDesignNew.setY3Top(Double.parseDouble(request.
				 * getParameter("y3Top")));
				 * bankCBTemplateDesignNew.setY3Width(Double.parseDouble(request
				 * .getParameter("y3Width")));
				 * 
				 * bankCBTemplateDesignNew.setY4left(Double.parseDouble(request.
				 * getParameter("y4Left")));
				 * bankCBTemplateDesignNew.setY4Top(Double.parseDouble(request.
				 * getParameter("y4Top")));
				 * bankCBTemplateDesignNew.setY4Width(Double.parseDouble(request
				 * .getParameter("y4Width")));
				 */

				bankingService.saveChequeTemplateDesign(bankCBTemplateDesignNew);
				session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/manageChequeTemplate");
			}
		}
		// check payee name with all old name
		// if found its already exist except checking self record then flag will
		// true otherwise false
		if (bankCBTemplateDesignNew.getId() != 0) {

			if (bankCBTemplateDesignList != null) {
				for (BankCBTemplateDesign b : bankCBTemplateDesignList) {
					if (b.getTemplateName().trim().toUpperCase()
							.equals(bankCBTemplateDesignNew.getTemplateName().trim().toUpperCase())
							&& b.getId() != bankCBTemplateDesignNew.getId()) {
						flag = true;
						break;
					}
				}
			}

			// if flag is false is then record update other wise it send msg as
			// Already exist
			if (flag == false) {
				System.out.println("Moving request for Update");
				session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);

				bankCBTemplateDesignNew.setBank(bank);
				bankCBTemplateDesignNew.setTemplateImgBase64(request.getParameter("chequeImageBase64"));

				bankCBTemplateDesignNew.setChqDateTop(Double.parseDouble(request.getParameter("chqDateTop")));
				bankCBTemplateDesignNew.setChqDateLeft(Double.parseDouble(request.getParameter("chqDateLeft")));
				bankCBTemplateDesignNew.setChqDateWidth(Double.parseDouble(request.getParameter("chqDateWidth")));
				// bankCBTemplateDesignNew.setChqDateHeight(Double.parseDouble(request.getParameter("chqDateHeight")));

				bankCBTemplateDesignNew.setPayeeNameTop(Double.parseDouble(request.getParameter("payeeNameTop")));
				bankCBTemplateDesignNew.setPayeeNameLeft(Double.parseDouble(request.getParameter("payeeNameLeft")));
				bankCBTemplateDesignNew.setPayeeNameWidth(Double.parseDouble(request.getParameter("payeeNameWidth")));

				bankCBTemplateDesignNew.setAmountTop(Double.parseDouble(request.getParameter("amountTop")));
				bankCBTemplateDesignNew.setAmountLeft(Double.parseDouble(request.getParameter("amountLeft")));
				bankCBTemplateDesignNew.setAmountWidth(Double.parseDouble(request.getParameter("amountWidth")));

				bankCBTemplateDesignNew.setAmountInWordTop(Double.parseDouble(request.getParameter("amountInWordTop")));
				bankCBTemplateDesignNew
						.setAmountInWordLeft(Double.parseDouble(request.getParameter("amountInWordLeft")));
				bankCBTemplateDesignNew
						.setAmountInWordWidth(Double.parseDouble(request.getParameter("amountInWordWidth")));

				bankCBTemplateDesignNew.setAcPayeeTop(Double.parseDouble(request.getParameter("acPayeeTop")));
				bankCBTemplateDesignNew.setAcPayeeLeft(Double.parseDouble(request.getParameter("acPayeeLeft")));
				bankCBTemplateDesignNew.setAcPayeeWidth(Double.parseDouble(request.getParameter("acPayeeWidth")));

				bankCBTemplateDesignNew.setDateFormate(request.getParameter("dateFormate"));

				/*
				 * bankCBTemplateDesignNew.setD1left(Double.parseDouble(request.
				 * getParameter("d1Left")));
				 * bankCBTemplateDesignNew.setD1Top(Double.parseDouble(request.
				 * getParameter("d1Top")));
				 * bankCBTemplateDesignNew.setD1Width(Double.parseDouble(request
				 * .getParameter("d1Width")));
				 * 
				 * bankCBTemplateDesignNew.setD2left(Double.parseDouble(request.
				 * getParameter("d2Left")));
				 * bankCBTemplateDesignNew.setD2Top(Double.parseDouble(request.
				 * getParameter("d2Top")));
				 * bankCBTemplateDesignNew.setD2Width(Double.parseDouble(request
				 * .getParameter("d2Width")));
				 * 
				 * bankCBTemplateDesignNew.setM1left(Double.parseDouble(request.
				 * getParameter("m1Left")));
				 * bankCBTemplateDesignNew.setM1Top(Double.parseDouble(request.
				 * getParameter("m1Top")));
				 * bankCBTemplateDesignNew.setM1Width(Double.parseDouble(request
				 * .getParameter("m1Width")));
				 * 
				 * bankCBTemplateDesignNew.setM2left(Double.parseDouble(request.
				 * getParameter("m2Left")));
				 * bankCBTemplateDesignNew.setM2Top(Double.parseDouble(request.
				 * getParameter("m2Top")));
				 * bankCBTemplateDesignNew.setM2Width(Double.parseDouble(request
				 * .getParameter("m2Width")));
				 * 
				 * bankCBTemplateDesignNew.setY1left(Double.parseDouble(request.
				 * getParameter("y1Left")));
				 * bankCBTemplateDesignNew.setY1Top(Double.parseDouble(request.
				 * getParameter("y1Top")));
				 * bankCBTemplateDesignNew.setY1Width(Double.parseDouble(request
				 * .getParameter("y1Width")));
				 * 
				 * bankCBTemplateDesignNew.setY2left(Double.parseDouble(request.
				 * getParameter("y2Left")));
				 * bankCBTemplateDesignNew.setY2Top(Double.parseDouble(request.
				 * getParameter("y2Top")));
				 * bankCBTemplateDesignNew.setY2Width(Double.parseDouble(request
				 * .getParameter("y2Width")));
				 * 
				 * bankCBTemplateDesignNew.setY3left(Double.parseDouble(request.
				 * getParameter("y3Left")));
				 * bankCBTemplateDesignNew.setY3Top(Double.parseDouble(request.
				 * getParameter("y3Top")));
				 * bankCBTemplateDesignNew.setY3Width(Double.parseDouble(request
				 * .getParameter("y3Width")));
				 * 
				 * bankCBTemplateDesignNew.setY4left(Double.parseDouble(request.
				 * getParameter("y4Left")));
				 * bankCBTemplateDesignNew.setY4Top(Double.parseDouble(request.
				 * getParameter("y4Top")));
				 * bankCBTemplateDesignNew.setY4Width(Double.parseDouble(request
				 * .getParameter("y4Width")));
				 */

				return updateChequeTemplate(bankCBTemplateDesignNew, model, session);
			}
		}

		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/manageChequeTemplate");
	}

	/**
	 * update Cheque Template
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBankList
	 */
	@Transactional
	@RequestMapping("/updateChequeTemplate")
	public ModelAndView updateChequeTemplate(@ModelAttribute BankCBTemplateDesign bankCBTemplateDesign, Model model,
			HttpSession session) {

		System.out.println("in Update Bank");

		// only user allowed to update Bank Payee if its payeeId is not zero
		if (bankCBTemplateDesign.getId() == 0 || bankCBTemplateDesign.getTemplateName() == null
				|| bankCBTemplateDesign.getTemplateName().equals("")) {
			System.out.println("can not update because 0 Template not available");
			return new ModelAndView("redirect:/manageChequeTemplate");
		}

		BankCBTemplateDesign bankCBTemplateDesignOld = bankingService
				.fetchChequeTemplateDesignById(bankCBTemplateDesign.getId());

		bankCBTemplateDesign.setTemplateAddedDatetime(bankCBTemplateDesignOld.getTemplateAddedDatetime());
		bankCBTemplateDesign.setTemplateUpdatedDatetime(new Date());

		bankingService.updateChequeTemplateDesign(bankCBTemplateDesign);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/manageChequeTemplate");

	}

	/**
	 * <pre>
	 * fetchBankCBTemplateDesignByBankId
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankCBTemplateDesign
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchBankCBTemplateDesignById")
	public ModelAndView fetchBankCBTemplateDesignById(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");

		long id = Long.parseLong(request.getParameter("id"));

		BankCBTemplateDesign bankCBTemplateDesign = bankingService.fetchChequeTemplateDesignById(id);
		model.addAttribute("bankCBTemplateDesign", bankCBTemplateDesign);

		List<Bank> bankList = bankingService.fetchBankList();
		model.addAttribute("pageName", "Update Cheque Template");
		model.addAttribute("bankList", bankList);

		return new ModelAndView("updateChequeTemplate");

	}

	/**
	 * <pre>
	 * ManualCheque Print
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankCBTemplateDesign
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/manualChequePrint")
	public ModelAndView manualChequePrint(Model model, HttpServletRequest request) {
		System.out.println("in Banking controller");

		List<BankCBTemplateDesign> bankCBTemplateDesignList = bankingService.fetchChequeTemplateDesignList();
		model.addAttribute("bankCBTemplateDesignList", bankCBTemplateDesignList);

		/*
		 * List<Bank> bankList=bankingService.fetchBankList();
		 * model.addAttribute("bankList", bankList);
		 */

		return new ModelAndView("chequePrintManual");

	}

	/**
	 * <pre>
	 * save Cheque Print
	 * &#64;param model
	 * &#64;param request
	 * </pre>
	 */

	@Transactional
	@RequestMapping("/saveManualCheque")
	public ModelAndView saveManualCheque(Model model, HttpServletRequest request) {

		System.out.println("inside Banking Conntroller");
		BankChequeEntry bankChequeEntry = new BankChequeEntry();

		bankChequeEntry.setChequeNo(request.getParameter("chequeNo"));
		bankChequeEntry.setPayeeName(request.getParameter("payeeName"));
		bankChequeEntry.setPrintType(Constants.CHEQUE_PRINT_MANUAL);
		bankChequeEntry.setChequeStatus(Constants.CHEQUE_PRINT_PROCEED_STATUS);
		bankChequeEntry.setChequeAmt(Double.parseDouble(request.getParameter("amount")));
		bankChequeEntry.setRemark(request.getParameter("remark"));

		String chequeDate = request.getParameter("chequeDate");
		Date chequeDate1 = null;
		try {
			chequeDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(chequeDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bankChequeEntry.setChequeDate(chequeDate1);
		bankChequeEntry.setChequeAddedDate(new Date());
		bankingService.saveManualPrintCheque(bankChequeEntry);
		return new ModelAndView("chequeBookPrint");

	}

	/**
	 * <pre>
	 * save Cheque Print
	 * &#64;param model
	 * &#64;param request
	 * </pre>
	 */

	@Transactional
	@RequestMapping("/savePrintedChequeFromCB")
	public ModelAndView savePrintedCheque(Model model, HttpServletRequest request) {

		System.out.println("inside Banking Conntroller");
		BankChequeEntry bankChequeEntry = new BankChequeEntry();

		bankChequeEntry.setChequeNo(request.getParameter("chqNoh"));
		bankChequeEntry.setPayeeName(request.getParameter("payeeNameh"));
		bankChequeEntry.setPrintType(Constants.CHEQUE_PRINT_FROM_CHEQUE_BOOK);
		bankChequeEntry.setChequeStatus(Constants.CHEQUE_PRINT_PROCEED_STATUS);
		bankChequeEntry.setChequeAmt(Double.parseDouble(request.getParameter("chqAmth")));
		bankChequeEntry.setRemark(request.getParameter("remarkh"));

		BankChequeBook bankChequeBook = new BankChequeBook();
		bankChequeBook.setChequeBookId(Long.parseLong(request.getParameter("chqBookIdh")));

		bankChequeEntry.setBankChequeBook(bankChequeBook);

		String chequeDate = request.getParameter("chqDateh");
		Date chequeDate1 = null;
		try {
			chequeDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(chequeDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bankChequeEntry.setChequeDate(chequeDate1);
		bankChequeEntry.setChequeAddedDate(new Date());
		bankingService.saveManualPrintCheque(bankChequeEntry);
		return new ModelAndView("manageChequeBook");

	}

	/**
	 * <pre>
	 * Ajax call
	 * save Cheque Print
	 * &#64;param model
	 * &#64;param request
	 * </pre>
	 */

	@Transactional
	@RequestMapping("/fetchChqTemplateById")
	public @ResponseBody BankCBTemplateDesign fetchChqTemplateById(Model model, HttpServletRequest request) {
		long templateId = Long.parseLong(request.getParameter("id"));
		BankCBTemplateDesign bankCBTemplateDesign = bankingService.fetchChequeTemplateDesignById(templateId);
		return bankCBTemplateDesign;
	}

	/**
	 * <pre>
	 * fetchChequeTempalteDesigntByCBId
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return chequeNoList
	 * </pre>
	 */

	@Transactional
	@RequestMapping("/fetchChequeTempalteDesigntByCBId")
	public @ResponseBody BankCBTemplateDesign fetchChequeTempalteDesigntByCBId(Model model,
			HttpServletRequest request) {
		System.out.println("in Banking controller");

		long chequeBookId = Long.parseLong(request.getParameter("id"));

		BankChequeBook bankChequeBook = bankingService.fetchBankChequeBookById(chequeBookId);
		BankCBTemplateDesign bankCBTemplateDesign = bankingService
				.fetchChequeTemplateDesignById(bankChequeBook.getBankCBTemplateDesign().getId());

		return bankCBTemplateDesign;
	}

	// Cancel Cheque Api

	/**
	 * <pre>
	 * chequeCancelPage
	 * to open cancel Cheque Page
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankChequeEntry
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/chequeCancelPage")
	public ModelAndView cancelChequePage(Model model, HttpServletRequest request) {
		model.addAttribute("pageName", "Cancel Cheque");
		return new ModelAndView("chequeCancel");
	}

	/**
	 * <pre>
	 * fetchChequeDetaisByChequeNo
	 * For Ajax
	 * &#64;param model
	 * &#64;param request
	 * &#64;return BankChequeEntry
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/fetchChequeDetaisByChequeNo")
	public @ResponseBody BankChequeEntry fetchChequeDetaisByChequeNo(Model model, HttpServletRequest request) {
		String chqNo = request.getParameter("chqNo");
		BankChequeEntry bankChequeEntry = bankingService.fetchChequeEntryByChequeNo(chqNo);
		if (bankChequeEntry == null) {
			return null;
		}
		return bankChequeEntry;
	}

	/**
	 * <pre>
	 * saveCancelChequeEntr
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView and redirect to manage Cheque Book Page
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/saveCancelChequeEntry")
	public ModelAndView saveCancelChequeEntry(Model model, HttpServletRequest request) {
		String chqNo = request.getParameter("chqNo");
		String cancelReason = request.getParameter("cancelReason");

		BankChequeEntry bankChequeEntry = bankingService.fetchChequeEntryByChequeNo(chqNo);
		bankChequeEntry.setChequeStatus(Constants.CHEQUE_PRINT_CANCEL_STATUS);
		bankChequeEntry.setCancelDate(new Date());
		bankChequeEntry.setCancelReason(cancelReason);
		bankingService.updateBankChequeEntry(bankChequeEntry);

		return new ModelAndView("manageChequeBook");
	}

	/**
	 * <pre>
	 * save Bulk Cheque Print
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView and redirect to manage Cheque Book Page
	 * </pre>
	 */
	@Transactional
	/*
	 * @RequestMapping(value="/saveBulkPrintCheque", method =
	 * RequestMethod.POST)
	 */
	@RequestMapping("/saveBulkPrintCheque")
	public ModelAndView saveBulkPrintCheque(@RequestBody BulkPrintSaveModelRequest bulkPrintSaveModelRequest) {

		// long cbId=Long.parseLong(request.getParameter("chqBookIdh"));
		BaseDomain baseDomain = new BaseDomain();
		System.out.println(bulkPrintSaveModelRequest);

		System.out.println("Inside save Bulk Model");
		for (BulkPrintSaveModel b : bulkPrintSaveModelRequest.getBulkPrintSaveModelList()) {
			BankChequeEntry bankChequeEntry = new BankChequeEntry();

			bankChequeEntry.setChequeNo(b.getChequeNo());
			bankChequeEntry.setPayeeName(b.getPayeeName());
			bankChequeEntry.setPrintType(Constants.CHEQUE_PRINT_FROM_CHEQUE_BOOK);
			bankChequeEntry.setChequeStatus(Constants.CHEQUE_PRINT_PROCEED_STATUS);
			bankChequeEntry.setChequeAmt(b.getChequeAmt());
			bankChequeEntry.setRemark(b.getRemark());
			bankChequeEntry.setIsEMICheque(bulkPrintSaveModelRequest.getIsEMICheque());

			BankChequeBook bankChequeBook = new BankChequeBook();
			bankChequeBook.setChequeBookId(bulkPrintSaveModelRequest.getCbId());

			bankChequeEntry.setBankChequeBook(bankChequeBook);

			String chequeDate = b.getChequeDate();
			Date chequeDate1 = null;
			try {
				chequeDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(chequeDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bankChequeEntry.setChequeDate(chequeDate1);
			bankChequeEntry.setChequeAddedDate(new Date());
			bankingService.saveManualPrintCheque(bankChequeEntry);
		}

		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);

		return new ModelAndView("redirect:/fetchCheckBookList");
	}

	/**
	 * <pre>
	 * open Cheque Report 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/openChequeReport")
	public ModelAndView openChequeReport(Model model, HttpServletRequest request) {

		model.addAttribute("pageName", "Cheque Book Summary");
		List<BankAccount> bankAccounts = bankingDAO.fetchBankAccounts();
		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccounts != null) {
			for (BankAccount bankAccount : bankAccounts) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		model.addAttribute("bankAccountList", bankAccountModelList);

		return new ModelAndView("chequeBookReport");
	}

	/**
	 * <pre>
	 * Ajax 
	 *  Cheque Report 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return List<ChequePrintReportData>
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/chequeBookReport")
	public @ResponseBody List<ChequePrintReportData> chequeReport(Model model, HttpServletRequest request) {

		List<ChequePrintReportData> chequePrintReportDataList = new ArrayList<>();

		long accountId = Long.parseLong(request.getParameter("bankAccountId"));
		List<BankChequeBook> bankChequeBookList = bankingService.fetchBankChequeBookListByAcoountId(accountId);
		for (BankChequeBook bankChequeBook : bankChequeBookList) {
			ChequePrintReportData chequePrintReportData = new ChequePrintReportData();
			chequePrintReportData.setChqBookSeriesNo(bankChequeBook.getChqBookName());
			chequePrintReportData.setChequeBookId(bankChequeBook.getChequeBookId());
			chequePrintReportData.setTotalChequeCount(bankChequeBook.getNumberOfLeaves());

			List<BankChequeEntry> cancelChequeEntryList = bankingService
					.fetchCancelChequeEntryListByCBId(bankChequeBook.getChequeBookId());
			List<BankChequeEntry> printedChequeEntryList = bankingService
					.fetchPrintedChequeEntryListByCBId(bankChequeBook.getChequeBookId());
			chequePrintReportData.setCancelChequeCount(cancelChequeEntryList.size());
			chequePrintReportData.setPrintedChequeCount(printedChequeEntryList.size());

			chequePrintReportData.setUnusedChequeCount(
					bankChequeBook.getNumberOfLeaves() - cancelChequeEntryList.size() - printedChequeEntryList.size());

			chequePrintReportDataList.add(chequePrintReportData);

		}
		// model.addAttribute("bankAccountList", bankAccountList);

		return chequePrintReportDataList;

	}

	/**
	 * <pre>
	 * open Cancel Cheque Report 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/openCancelChequeReport")
	public ModelAndView openCancelChequeReport(Model model, HttpServletRequest request) {

		model.addAttribute("pageName", "Cancel Cheque Report");
		List<BankAccount> bankAccounts = bankingDAO.fetchBankAccounts();
		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccounts != null) {
			for (BankAccount bankAccount : bankAccounts) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		model.addAttribute("bankAccountList", bankAccountModelList);

		return new ModelAndView("chequeCancelReport");
	}

	/**
	 * <pre>
	 * Ajax 
	 *  chequeCancelEntryListReport 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return List<BankChequeEntry>
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/chequeCancelEntryListReport")
	public @ResponseBody List<BankChequeEntryModel> chequeCancelEntryListReport(Model model,
			HttpServletRequest request) {

		List<BankChequeEntryModel> cancelChequeEntryList = new ArrayList<>();

		long accountId = Long.parseLong(request.getParameter("bankAccountId"));
		List<BankChequeBook> bankChequeBookList = bankingService.fetchBankChequeBookListByAcoountId(accountId);
		for (BankChequeBook bankChequeBook : bankChequeBookList) {

			List<BankChequeEntry> cancelChequeEntry1 = bankingService
					.fetchCancelChequeEntryListByCBId(bankChequeBook.getChequeBookId());
			List<BankChequeEntryModel> bankChequeEntryModels = new ArrayList<>();
			for (int i = 0; i < cancelChequeEntry1.size(); i++) {
				BankChequeEntry bankChequeEntry = cancelChequeEntry1.get(i);
				bankChequeEntryModels.add(new BankChequeEntryModel(bankChequeEntry.getChequeAddedDate(),
						bankChequeEntry.getChequeNo(), bankChequeEntry.getPayeeName(), bankChequeEntry.getChequeAmt(),
						bankChequeEntry.getChequeDate(), bankChequeEntry.getRemark()));
			}
			cancelChequeEntryList.addAll(bankChequeEntryModels);

		}
		// model.addAttribute("bankAccountList", bankAccountList);

		return cancelChequeEntryList;

	}

	/**
	 * <pre>
	 * open Printed Cheque Report 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/openPrintedChequeReport")
	public ModelAndView openPrintedChequeReport(Model model, HttpServletRequest request) {

		model.addAttribute("pageName", "Printed Cheque Report");
		List<BankAccount> bankAccounts = bankingDAO.fetchBankAccounts();
		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccounts != null) {
			for (BankAccount bankAccount : bankAccounts) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		model.addAttribute("bankAccountList", bankAccountModelList);

		return new ModelAndView("chequePrintedEntryReport");
	}

	/**
	 * <pre>
	 * Ajax 
	 *  chequePrintedEntryListReport 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return List<BankChequeEntry>
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/chequePrintedEntryListReport")
	public @ResponseBody List<BankChequeEntryModel> chequePrintedEntryListReport(Model model,
			HttpServletRequest request) {

		List<BankChequeEntryModel> printedChequeEntryList = new ArrayList<>();

		long accountId = Long.parseLong(request.getParameter("bankAccountId"));
		List<BankChequeBook> bankChequeBookList = bankingService.fetchBankChequeBookListByAcoountId(accountId);
		for (BankChequeBook bankChequeBook : bankChequeBookList) {

			List<BankChequeEntry> printedChequeList = bankingService
					.fetchPrintedChequeEntryListByCBId(bankChequeBook.getChequeBookId());
			List<BankChequeEntryModel> bankChequeEntryModels = new ArrayList<>();
			for (int i = 0; i < printedChequeList.size(); i++) {
				BankChequeEntry bankChequeEntry = printedChequeList.get(i);
				bankChequeEntryModels.add(new BankChequeEntryModel(bankChequeEntry.getChequeAddedDate(),
						bankChequeEntry.getChequeNo(), bankChequeEntry.getPayeeName(), bankChequeEntry.getChequeAmt(),
						bankChequeEntry.getChequeDate(), bankChequeEntry.getRemark()));
			}
			printedChequeEntryList.addAll(bankChequeEntryModels);

		}
		// model.addAttribute("bankAccountList", bankAccountList);

		return printedChequeEntryList;

	}

	/**
	 * <pre>
	 * Ajax 
	 *  chequeBookDetailsEntryListReport 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return List<BankChequeEntry>
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/chequeBookDetailsEntryListReport")
	public ModelAndView chequeBookDetailsEntryListReport(Model model, HttpServletRequest request) {

		List<BankChequeEntry> chequeBookDetailsEntryList = new ArrayList<>();

		model.addAttribute("pageName", "Cheque Book Details");

		long chqBookId = Long.parseLong(request.getParameter("chqBookId"));
		BankChequeBook bankChequeBook = bankingService.fetchBankChequeBookById(chqBookId);
		List<BankChequeEntry> cancelChequeEntry1 = bankingService
				.fetchCancelChequeEntryListByCBId(bankChequeBook.getChequeBookId());
		chequeBookDetailsEntryList.addAll(cancelChequeEntry1);

		List<BankChequeEntry> printedChequeEntry = bankingService
				.fetchPrintedChequeEntryListByCBId(bankChequeBook.getChequeBookId());
		chequeBookDetailsEntryList.addAll(printedChequeEntry);

		// model.addAttribute("bankAccountList", bankAccountList);

		model.addAttribute("chequeBookDetailsEntryList", chequeBookDetailsEntryList);

		return new ModelAndView("chequeBookDetailsReport");

	}

	/**
	 * <pre>
	 * Ajax 
	 *  cheque Manual Print Cheque no if already printed or not 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return List<BankChequeEntry>
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/checkChequeNoAlreadyPrintedOrNot")
	public @ResponseBody String checkChequeNoAlreadyPrintedOrNot(Model model, HttpServletRequest request) {

		String chqNo = request.getParameter("chqNo");
		BankChequeEntry bankChequeEntry = bankingService.fetchChequeEntryByChequeNo(chqNo);
		if (bankChequeEntry == null) {
			return "notPrinted";

		} else {
			return "printed";
		}

	}

	/**
	 * <pre>
	 * open Emi Printed Cheque Form 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/openEMIChequePrintForm")
	public ModelAndView openEMIChequePrintForm(Model model, HttpServletRequest request) {

		model.addAttribute("pageName", "EMI Cheque Print Form");
		List<BankAccount> bankAccountList = bankingDAO.fetchBankAccounts();
		
		List<BankAccountModel> bankAccountModelList = new ArrayList<>();
		if (bankAccountList != null) {
			for (BankAccount bankAccount : bankAccountList) {
				bankAccountModelList.add(new BankAccountModel(bankAccount.getId(), bankAccount.getAccountHolderName(),
						bankAccount.getAccountNumber(), bankAccount.getAccountType(), bankAccount.getBank().getBankId(),
						bankAccount.getBank().getBankName(), bankAccount.getBank().getShortName(),
						bankAccount.getIfscCode(), bankAccount.getBranchCode(), bankAccount.getBankAddress(),
						bankAccount.getMinimumBalance(), bankAccount.getActualBalance(),
						bankAccount.getDrawableBalance(), bankAccount.getChequeIssuedAmount(),
						bankAccount.getChequeDepositedAmount(), bankAccount.getInsertedDate(),
						bankAccount.getUpdatedDate(), bankAccount.isStatus(), bankAccount.getPrimary()));
			}
		}
		
		
		
		model.addAttribute("bankAccountList", bankAccountModelList);

		return new ModelAndView("emiChequePrintForm");
	}

	/**
	 * <pre>
	 * openEMICalculatorForm 
	 * &#64;param model
	 * &#64;param request
	 * &#64;return ModelAndView
	 * </pre>
	 */
	@Transactional
	@RequestMapping("/openEMICalculatorForm")
	public ModelAndView openEMICalculatorForm(Model model, HttpServletRequest request) {

		model.addAttribute("pageName", "EMI Calculator");

		return new ModelAndView("emiCalculator");
	}

}
