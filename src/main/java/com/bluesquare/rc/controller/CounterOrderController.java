package com.bluesquare.rc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.TransportationDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ProformaOrder;
import com.bluesquare.rc.entities.ProformaOrderProductDetails;
import com.bluesquare.rc.entities.ReturnCounterOrder;
import com.bluesquare.rc.entities.Transportation;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CounterOrderDetailsModel;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.CounterReturnOrderModel;
import com.bluesquare.rc.models.EditOrderDetailsPaymentModel;
import com.bluesquare.rc.models.InvoiceDetails;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.ProformaOrderDetailsResponse;
import com.bluesquare.rc.models.ProformaOrderReport;
import com.bluesquare.rc.models.ReturnCounterRequest;
import com.bluesquare.rc.responseEntities.BusinessNameModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.TransportationModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.EmailSender;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.bluesquare.rc.utils.MathUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;


/**
 * <pre>
 * @author Sachin Pawar 18-05-2018 Code Documentation
 * 
 * API EndPoints
 * 1.openCounter
 * 2.syncProductListForCounterOrder
 * 3.fetchSyncBusinessList
 * 4.openCounterForEdit
 * 5.fetchCounterOrderProductDetailsByCounterOrderId
 * 6.fetchCounterOrderByCounterOrderId
 * 7.saveCounterOrder
 * 8.updateCounterOrderForEdit
 * 9.deleteCounterOrder
 * 10.counterOrderProductDetailsListForWebApp
 * 11.counterOrderReport
 * 12.fetchPaymentCounterOrder
 * 13.paymentCounterOrder
 * 14.giveCounterOrderPayment
 * 15.counterOrderInvoice.pdf 
 * <pre>
 */
@Controller
public class CounterOrderController {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TransportationDAO transportationDAO;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	JavaMailSender mailSender;
	/**
	 * open counter page with product list and businessName list
	 * @param session
	 * @param model
	 * @return ModelAndView counterOrder.jsp
	 */
	@Transactional 	@RequestMapping("/openCounter")
	public ModelAndView openCounter(HttpSession session,Model model){	
		System.out.println("in openCounter controller");
		model.addAttribute("pageName", "Counter");
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						"",
						null,
						null,
						0f,
						/*"",*/ //product content type
						"",
						0l,
						0l,
						0l,
						0l, 
						null,
						null,
						product.getProductBarcode(),
						null));
			}
		}
		model.addAttribute("productList", productModelList);
		
		List<BusinessName> businessNameList=businessNameService.getBusinessNameListForApp();
		List<BusinessNameModel> businessNameModelList=new ArrayList<>();
		for(BusinessName businessName :businessNameList){
			businessNameModelList.add(new BusinessNameModel(
					businessName.getBusinessNamePkId(), 
					businessName.getBusinessNameId(), 
					businessName.getShopName(), 
					null, 
					0, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null,
					false,
					false));
		}
		model.addAttribute("businessNameList", businessNameModelList);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		List<Transportation> transportations = transportationDAO.fetchAll();
		List<TransportationModel> transportationModelList=new ArrayList<>();
		for(Transportation transportation : transportations){
			transportationModelList.add(new TransportationModel(
					transportation.getId(), 
					transportation.getTransportName(), 
					transportation.getMobNo(), 
					transportation.getGstNo(), 
					transportation.getVehicleNo()));
		}
		model.addAttribute("transportations", transportationModelList);
		
		Calendar calendar=Calendar.getInstance();		
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String startDate=simpleDateFormat.format(new Date());
		String endDate=simpleDateFormat.format(calendar.getTime());
		List<InvoiceDetails> invoiceDetails=counterOrderService.fetchInvoiceDetails(startDate, endDate);
		model.addAttribute("invoiceDetails", invoiceDetails);
		
		model.addAttribute("isEdit", false);
		
		return new ModelAndView("counterOrder");
	}
	
	/**
	 * sync Product List For Counter Order
	 * @param session
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional
	@RequestMapping("/syncProductListForCounterOrder")
	public @ResponseBody List<ProductModel> fetchSyncProductList(HttpSession session,HttpServletRequest request,Model model){
		List<Product> productList=productDAO.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						"",
						null,
						null,
						0f,
						/*"",*/ //product content type
						"",
						0l,
						0l,
						0l,
						0l, 
						null,
						null,
						product.getProductBarcode(),
						null));
			}
		}
		return productModelList;
	}
	
	/**
	 * sync Business List For Counter Order
	 * @param session
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional
	@RequestMapping("/fetchSyncBusinessList")
	public @ResponseBody List<BusinessNameModel> fetchSyncBusinessList(HttpSession session,HttpServletRequest request,Model model){
		List<BusinessName> businessNameList=businessNameService.getBusinessNameListForApp();
		List<BusinessNameModel> businessNameModelList=new ArrayList<>();
		for(BusinessName businessName :businessNameList){
			businessNameModelList.add(new BusinessNameModel(
					businessName.getBusinessNamePkId(), 
					businessName.getBusinessNameId(), 
					businessName.getShopName(), 
					null, 
					0, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					false, 
					false)); 
		}
		return businessNameModelList;
	}
	
	/**
	 * open counter page with product list and businessName list for Edit
	 * @param session
	 * @param model
	 * @return ModelAndView counterOrder.jsp
	 */
	@Transactional 	@RequestMapping("/openCounterForEdit")
	public ModelAndView openCounterForEdit(HttpSession session,Model model,HttpServletRequest request){	
		System.out.println("in openCounter controller");
		model.addAttribute("pageName", "Counter");
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						"",
						null,
						null,
						0f,
						/*"",*/ //product content type
						"",
						0l,
						0l,
						0l,
						0l, 
						null,
						null,
						product.getProductBarcode(),
						null));
			}
		}
		model.addAttribute("productList", productModelList);
		
		List<BusinessName> businessNameList=businessNameService.getBusinessNameListForApp();
		List<BusinessNameModel> businessNameModelList=new ArrayList<>();
		for(BusinessName businessName :businessNameList){
			businessNameModelList.add(new BusinessNameModel(
					businessName.getBusinessNamePkId(), 
					businessName.getBusinessNameId(), 
					businessName.getShopName(), 
					null, 
					0, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null,
					false,
					false));
		}
		model.addAttribute("businessNameList", businessNameModelList);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		List<Transportation> transportations = transportationDAO.fetchAll();
		List<TransportationModel> transportationModelList=new ArrayList<>();
		for(Transportation transportation : transportations){
			transportationModelList.add(new TransportationModel(
					transportation.getId(), 
					transportation.getTransportName(), 
					transportation.getMobNo(), 
					transportation.getGstNo(), 
					transportation.getVehicleNo()));
		}
		model.addAttribute("transportations", transportationModelList);
		
		model.addAttribute("isEdit", true);
		
		model.addAttribute("counterOrder", counterOrderService.fetchCounterOrder(request.getParameter("counterOrderId")));
		
		Calendar calendar=Calendar.getInstance();		
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String startDate=simpleDateFormat.format(new Date());
		String endDate=simpleDateFormat.format(calendar.getTime());
		List<InvoiceDetails> invoiceDetails=counterOrderService.fetchInvoiceDetails(startDate, endDate);
		model.addAttribute("invoiceDetails", invoiceDetails);
		
		return new ModelAndView("counterOrder");
	}
	
	/**
	 * fetch counter order product details by counter order id
	 * @param session
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchCounterOrderProductDetailsByCounterOrderId")
	public @ResponseBody ArrayList<Map<String, Object>> fetchCounterOrderProductDetailsByCounterOrderId(HttpSession session,HttpServletRequest request,Model model){	
		List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderService.fetchCounterOrderProductDetails(request.getParameter("counterOrderId"));
		return makeGKfetchCounterOrderProductDetailsByCounterOrderIdResponse(counterOrderProductDetailsList);
	}
	
	public ArrayList<Map<String, Object>> makeGKfetchCounterOrderProductDetailsByCounterOrderIdResponse(List<CounterOrderProductDetails> counterOrderProductDetailsList) {
		ObjectMapper oMapper = new ObjectMapper();

		ArrayList<Map<String, Object>> objArrayMod = new ArrayList<>();
		for (CounterOrderProductDetails counterOrderProductDetails : counterOrderProductDetailsList) {
			Map<String, Object> map = oMapper.convertValue(counterOrderProductDetails, Map.class);
			map.remove("counterOrder");  			
			objArrayMod.add(map);
		}
		
		return objArrayMod;
	}
	
	/**
	 * fetch counter order details for counter order
	 * @param session
	 * @param request
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchCounterOrderByCounterOrderId")
	public @ResponseBody CounterOrderDetailsModel fetchCounterOrderByCounterOrderId(HttpSession session,HttpServletRequest request,Model model){	
		CounterOrder counterOrder=counterOrderService.fetchCounterOrder(request.getParameter("counterOrderId"));
		EmployeeDetails employeeDetails=employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId());
		return new CounterOrderDetailsModel(counterOrder, employeeDetails.getName());
	}
	
	
	/***
	 * save counter order
	 * 
	 * @param session
	 * @param model
	 * @return ModelAndView redirect:/openCounter
	 */
	@Transactional 	@RequestMapping("/saveCounterOrder")
	public @ResponseBody BaseDomain saveCounterOrder(HttpSession session,HttpServletRequest request,Model model){	
		BaseDomain baseDomain=new BaseDomain();
		String counterOrderId;
	
		//delete Proforma Order
				long id=Long.parseLong(request.getParameter("proFormaId"));
				
				if(id!=0){
					counterOrderService.deleteProformaOrderById(id);	
				}
				
		
		String transactionRefNo=request.getParameter("transactionRefNo");
		long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
		String comment=request.getParameter("comment");
	
		
			
			String counterOrderProductDetails=request.getParameter("productList");
			String businessNameId=request.getParameter("businessNameId");
			String paidAmount=request.getParameter("paidAmount");
			String balAmount=request.getParameter("balAmount");
			String dueDate=request.getParameter("dueDate");
			dueDate+=" 23:59:59";
			String payType=request.getParameter("payType");
			String paymentType=request.getParameter("paymentType");
			String bankName=request.getParameter("bankName");
			String chequeNumber=request.getParameter("chequeNumber");
			String chequeDate=request.getParameter("chequeDate");
			String custName=request.getParameter("custName");
			String mobileNo=request.getParameter("mobileNo");
			String gstNo=request.getParameter("gstNo");
			String discountTypeId=request.getParameter("discountType");
			String discountAmount=request.getParameter("discountAmount");
			
			String isTransportationHave=request.getParameter("isTransportationHave");
			
			String transportCharge = request.getParameter("transportationChargesH");

			long transportationId=0;
			String vehicalNumber="";
			String docketNumber="";
			double trasportationCharge = 0;
			if(isTransportationHave.equals("Yes")){
				transportationId=Long.parseLong(request.getParameter("transportationId"));
				vehicalNumber=request.getParameter("vehicalNumber");
				docketNumber=request.getParameter("docketNumber");
				if (transportCharge.equals("")) {
					trasportationCharge = 0;
				} else {
					trasportationCharge = Double.parseDouble(transportCharge);
				}
			}
			
			counterOrderId=counterOrderService.saveCounterOrder(counterOrderProductDetails, 
																businessNameId, 
																paidAmount, 
																balAmount, 
																dueDate, 
																payType, 
																paymentType,
																bankName, 
																chequeNumber, 
																chequeDate, 
																custName, 
																mobileNo, 
																gstNo,
																transportationId,
																vehicalNumber,
																docketNumber,
																transactionRefNo,
																comment,
																paymentMethodId,
																discountTypeId,
																discountAmount,
																trasportationCharge);
			
			
			// to send the invoice on emaild id start
			
			
			CounterOrder counterOrder=counterOrderService.fetchCounterOrder(counterOrderId);
			if(counterOrder.getBusinessName()!=null){
				if(counterOrder.getBusinessName().getContact().getEmailId()!=null){
					if(!counterOrder.getBusinessName().getContact().getEmailId().isEmpty()){
						BillPrintDataModel billPrintDataModel=counterOrderService.fetchCounterBillPrintData(counterOrderId);
						
						//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
						String filePath="/resources/pdfFiles/invoice.pdf";
						
						ServletContext context = request.getServletContext();
						String appPath = context.getRealPath("/");
						//system.out.println("appPath = " + appPath);

						// construct the complete absolute path of the file
						String fullPath = appPath + filePath;      
						//File downloadFile = new File(fullPath);
						
						File dFile = null;
						try {
							dFile = InvoiceGenerator.generateInvoicePdfCounterOrder(billPrintDataModel, fullPath);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
										
						String fileName=counterOrder.getInvoiceNumber()+".pdf";
						
						EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
						emailSender.sendEmail("Order Invoice "+fileName, " ", counterOrder.getBusinessName().getContact().getEmailId(), true, dFile,fileName);
					}
					
	
				}	
			}
		
		//to send the invoice on emaild id end
		
		baseDomain.setErrorMsg(counterOrderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		return baseDomain;
	}
	

	/***
	 * update counter order
	 * @param session
	 * @param model
	 * @return ModelAndView redirect:/openCounter
	 */
	@Transactional 	@RequestMapping("/updateCounterOrderForEdit")
	public @ResponseBody BaseDomain updateCounterOrderForEdit(HttpSession session,HttpServletRequest request,Model model){	
		BaseDomain baseDomain=new BaseDomain();
		String counterOrderId;
		/*try {*/
			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			String comment=request.getParameter("comment");
		
			
				   counterOrderId=request.getParameter("counterOrderId");
				   String counterOrderProductDetails=request.getParameter("productList");
					String businessNameId=request.getParameter("businessNameId");
					String paidAmount=request.getParameter("paidAmount");
					String balAmount=request.getParameter("balAmount");
					String dueDate=request.getParameter("dueDate");
					dueDate+=" 23:59:59";
					String payType=request.getParameter("payType");
					String paymentType=request.getParameter("paymentType");
					String bankName=request.getParameter("bankName");
					String chequeNumber=request.getParameter("chequeNumber");
					String chequeDate=request.getParameter("chequeDate");
					String custName=request.getParameter("custName");
					String mobileNo=request.getParameter("mobileNo");
					String gstNo=request.getParameter("gstNo");
					String refAmount=request.getParameter("refAmount");
					String paymentSituation=request.getParameter("paymentSituation");
					String discountTypeId=request.getParameter("discountType");
					String discountAmount=request.getParameter("discountAmount");
					
					String isTransportationHave = request.getParameter("isTransportationHave");
					String transportCharge = request.getParameter("transportationChargesH");

					long transportationId = 0;
					String vehicalNumber = "";
					String docketNumber = "";
					double trasportationCharge = 0;
					if (isTransportationHave.equals("Yes")) {
						transportationId = Long.parseLong(request.getParameter("transportationId"));
						vehicalNumber = request.getParameter("vehicalNumber");
						docketNumber = request.getParameter("docketNumber");
						if (transportCharge.equals("")) {
							trasportationCharge = 0;
						} else {
							trasportationCharge = Double.parseDouble(transportCharge);
						}

					}
					
					counterOrderId=counterOrderService.updateCounterOrderForEdit(counterOrderId,
																		counterOrderProductDetails, 
																		businessNameId, 
																		paidAmount, 
																		balAmount, 
																		refAmount,
																		paymentSituation,
																		dueDate, 
																		payType, 
																		paymentType,
																		bankName, 
																		chequeNumber, 
																		chequeDate, 
																		custName, 
																		mobileNo, 
																		gstNo,
																		transportationId,
																		vehicalNumber,
																		docketNumber,
																		transactionRefNo,
																		comment,
																		paymentMethodId,
																		discountTypeId,
																		discountAmount,
																		trasportationCharge);
		/*} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Something Went Wrong");
			return baseDomain;
		}*/
					
// to send the invoice on emaild id start
					
					
					CounterOrder counterOrder=counterOrderService.fetchCounterOrder(counterOrderId);
					if(counterOrder.getBusinessName()!=null){
						if(counterOrder.getBusinessName().getContact().getEmailId()!=null){
							if(!counterOrder.getBusinessName().getContact().getEmailId().isEmpty()){
								BillPrintDataModel billPrintDataModel=counterOrderService.fetchCounterBillPrintData(counterOrderId);
								
								//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
								String filePath="/resources/pdfFiles/invoice.pdf";
								
								ServletContext context = request.getServletContext();
								String appPath = context.getRealPath("/");
								//system.out.println("appPath = " + appPath);

								// construct the complete absolute path of the file
								String fullPath = appPath + filePath;      
								//File downloadFile = new File(fullPath);
								
								File dFile = null;
								try {
									dFile = InvoiceGenerator.generateInvoicePdfCounterOrder(billPrintDataModel, fullPath);
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (DocumentException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
												
								String fileName=counterOrder.getInvoiceNumber()+".pdf";
								
								try {
									EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
									emailSender.sendEmail("Updated Order Invoice " +fileName, " ", counterOrder.getBusinessName().getContact().getEmailId(), true, dFile,fileName);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
			
						}	
					}
				
				//to send the invoice on emaild id end
		
		baseDomain.setErrorMsg(counterOrderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		return baseDomain;
	}
	
	/**
	 * delete counter order by counter order id
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@GetMapping("/deleteCounterOrder")
	public ModelAndView deleteCounterOrder(Model model,HttpServletRequest request,HttpSession session){
		
		counterOrderService.deleteCounterOrder(request.getParameter("counterOrderId"));
		
		return new ModelAndView("redirect:/counterOrderReport?range=today");
	}
	/**
	 * set bad debts counter order by counter order id
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@GetMapping("/setBadDebtsCounterOrder")
	public ModelAndView setBadDebtsCounterOrder(Model model,HttpServletRequest request,HttpSession session){
		
		counterOrderService.setBadDebtsOfCounter(request.getParameter("counterOrderId"));
		
		return new ModelAndView("redirect:/counterOrderReport?range=today");
	}
	/***
	 * fetch order product details and show 
	 * @param model
	 * @param request
	 * @param session
	 * @return ModelAndView OrderDetails.jsp
	 */
	@Transactional 	@GetMapping("/counterOrderProductDetailsListForWebApp")
	public ModelAndView orderProductDetailsListForWebApp(Model model,HttpServletRequest request,HttpSession session){
		model.addAttribute("pageName", "Order Product Details");
		
		CounterOrder counterOrder=counterOrderService.fetchCounterOrder(request.getParameter("counterOrderId"));
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=counterOrderService.fetchCounterOrderProductDetailsForShowOrderDetails(request.getParameter("counterOrderId"));
		model.addAttribute("counterOrderProductDetailListForWebApps", orderProductDetailListForWebApps);
		
		double totalDiscountAmount=0;
		double totalAmountWithTax=0,totalAmountWithTaxMain=0;
		long totalQuantity=0,totalReturnQuantity=0;
		double totalNetAmount=0;
		
		for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
		{
			totalDiscountAmount+=orderProductDetailListForWebApp.getDiscountAmt();
			totalAmountWithTaxMain+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			totalQuantity+=orderProductDetailListForWebApp.getQuantity();
			totalReturnQuantity+=orderProductDetailListForWebApp.getReturnQuantity();
			totalAmountWithTax+=orderProductDetailListForWebApp.getNetAmount();
		}
		
		double discountAmtCut=0,discountPerCut=0;
		if(counterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=counterOrder.getDiscount();
			discountPerCut=(counterOrder.getDiscount()/totalAmountWithTax)*100;			
		}else{
			discountPerCut=counterOrder.getDiscount();
			discountAmtCut=(counterOrder.getDiscount()*totalAmountWithTax)/100;
		}
		
		double totalAmountWithTaxWithDisc=totalAmountWithTax-discountAmtCut;		
		float roundOffAmount=(float)(MathUtils.round(totalAmountWithTaxWithDisc,0)-MathUtils.round(totalAmountWithTaxWithDisc,2));
		String roundAmt="";
		
		if(roundOffAmount>0){
			roundAmt="+"+roundOffAmount;
		}else if(roundOffAmount<0){
			roundAmt=roundOffAmount+"";
		}else{			
			roundAmt="0";
		}
		

		totalAmountWithTaxWithDisc=MathUtils.round(totalAmountWithTaxWithDisc, 2);
		
		discountAmtCut=MathUtils.round(discountAmtCut,2);
		discountPerCut=MathUtils.round(discountPerCut,2);
		
		model.addAttribute("totalQuantity", totalQuantity);
		model.addAttribute("totalReturnQuantity", totalReturnQuantity);
		model.addAttribute("totalAmountWithTaxMain", totalAmountWithTaxMain);
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		model.addAttribute("totalDiscountAmount", totalDiscountAmount);
		model.addAttribute("discountAmtCut", discountAmtCut);
		model.addAttribute("discountPerCut", discountPerCut);
		model.addAttribute("totalAmountWithTaxWithDisc", totalAmountWithTaxWithDisc);
		model.addAttribute("roundOffAmount",roundAmt);
		model.addAttribute("totalNetPayable", MathUtils.round(totalAmountWithTaxWithDisc,0));
		model.addAttribute("report", "counterOrder");
		return new ModelAndView("counterOrderDetails");
	}
	
	/***
	 * counter order report by range
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView counterOrderReoprt.jsp
	 */
	@Transactional 	@RequestMapping("/counterOrderReport")
	public ModelAndView counterOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Counter Order Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
	
		List<CounterOrderReport> counterOrderReportList=counterOrderService.fetchCounterOrderReport(range, startDate, endDate);
		model.addAttribute("counterOrderReportList", counterOrderReportList);
		
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", null);
		return new ModelAndView("counterOrderReoprt");
	}
	
	
	/**
	 * fetch payment did list for collection report
	 * @param session
	 * @param request
	 * @param model
	 * @return List PaymentCounter
	 */
	@Transactional 	@RequestMapping("/fetchPaymentCounterOrder")
	public @ResponseBody List<PaymentCounterReport> fetchPaymentCounterOrder(HttpSession session,HttpServletRequest request,Model model){	
		
		List<PaymentCounterReport> paymentCounterList=counterOrderService.fetchPaymentCounterReportListByCounterOrderId(request.getParameter("counterOrderId"));
		
		return paymentCounterList;
	}
	
	/**
	 * open payment page with counter order payment details by counter order id
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/paymentCounterOrder")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Counter Order Payment");
	
		/*PaymentDoInfo paymentDoInfo = counterOrderService.fetchPaymentInfoByCounterOrderId(request.getParameter("counterOrderId"));
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);*/
		
		String orderId=request.getParameter("counterOrderId");
		List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(orderId);
		CounterOrder counterOrder=counterOrderService.fetchCounterOrder(orderId);
		double totalAmount=counterOrder.getTotalAmountWithTax();
		
		double totalAmountPaid=0;
		if(paymentCounterList!=null){
			for(PaymentCounter paymentCounter2: paymentCounterList){
				totalAmountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
		}
		
		double balanceAmount=totalAmount-totalAmountPaid;
		
	
		String shopName="";
		String mobileNumber="";
		String businessAddress="";
		
		if(counterOrder.getBusinessName()!=null){
			shopName=counterOrder.getBusinessName().getShopName();
			mobileNumber=counterOrder.getBusinessName().getContact().getMobileNumber();
			businessAddress=counterOrder.getBusinessName().getAddress();
		}else{
			shopName=counterOrder.getCustomerName();
			mobileNumber=counterOrder.getCustomerMobileNumber();
			businessAddress="NA";
		}
		
		EditOrderDetailsPaymentModel editOrderDetailsPaymentModel=new EditOrderDetailsPaymentModel(
																	shopName, 
																	orderId,
																	0l,
																	mobileNumber, 
																	businessAddress, 
																	totalAmount, 
																	totalAmountPaid, 
																	balanceAmount,  
																	"", 
																	"", 
																	"", 
																	0, 
																	balanceAmount, 
																	"", 
																	"",
																	"",
																	0l,
																	"",
																	"",
																	"Paid");
		model.addAttribute("payment", editOrderDetailsPaymentModel);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		model.addAttribute("url", "giveCounterOrderPayment");
		
		return new ModelAndView("editPayment");
	}
	
	/**
	 * save payment details of counter order
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */
	@Transactional 	@RequestMapping("/giveCounterOrderPayment")
	public ModelAndView giveEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
					
			/*double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String dueDate=request.getParameter("dueDate");
			String comment=request.getParameter("comment");
			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			String payType=request.getParameter("payType");*/
	
		String orderId=request.getParameter("orderId");
		String dueDate=request.getParameter("dueDate");
		String bankName=request.getParameter("bankName");
		String chequeNo=request.getParameter("checkNo");
		String chequeDate=request.getParameter("checkDate");
		double paidAmount=Double.parseDouble(request.getParameter("paidAmount"));		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String comment=request.getParameter("comment");
		String transactionRefNo=request.getParameter("transactionRefNo");
		long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
		String payType=request.getParameter("payType");
		
			CounterOrder counterOrder=counterOrderService.fetchCounterOrder(orderId);
			
			//take counterOrderId from inventoryId
			List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(orderId);
			
			PaymentCounter paymentCounter=new PaymentCounter();	
			paymentCounter.setComment(comment);
			if(paymentCounterList==null){
				
				if(payType.equals(Constants.CASH_PAY_STATUS))
				{
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
				}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(transactionRefNo);
					paymentCounter.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
					paymentCounter.setComment(comment);
				}else{
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
					
					try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(chequeDate);
				}
				paymentCounter.setPayType(payType);
				paymentCounter.setLastDueDate(counterOrder.getPaymentDueDate());
				paymentCounter.setCurrentAmountPaid(paidAmount);
				//paymentCounter.setTotalAmountPaid(amount);
				//paymentCounter.setBalanceAmount(counterOrder.getTotalAmountWithTax()-amount);	
				paymentCounter.setPaidDate(new Date());
				paymentCounter.setCounterOrder(counterOrder);
				paymentCounter.setChequeClearStatus(true);
				counterOrderService.savePaymentCounter(paymentCounter);
				
				if(paidAmount==counterOrder.getTotalAmountWithTax()){
					counterOrder.setPayStatus(true);
				}else{
					try { counterOrder.setPaymentDueDate(dateFormat.parse(dueDate)); } catch (Exception e) {}
					counterOrder.setPayStatus(false);
				}
				counterOrderService.updateCounterOrder(counterOrder);
				
			}else{
				double amountPaid=0;
				for(PaymentCounter paymentCounter2: paymentCounterList){
					amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
				}
				double totalAmountPaid=amountPaid;//paymentCounterList.get(0).getTotalAmountPaid();
				
				if(payType.equals(Constants.CASH_PAY_STATUS))
				{
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
				}else if(payType.equals(Constants.OTHER_PAY_STATUS)){
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					
					paymentCounter.setTransactionRefNo(transactionRefNo);
					paymentCounter.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
				}else{
					paymentCounter.setTransactionRefNo(null);
					paymentCounter.setPaymentMethod(null);
					
					try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(chequeNo);
				}
				paymentCounter.setPayType(payType);
				paymentCounter.setLastDueDate(counterOrder.getPaymentDueDate());
				paymentCounter.setCurrentAmountPaid(paidAmount);
				//paymentCounter.setTotalAmountPaid(totalAmountPaid+amount);
				//paymentCounter.setBalanceAmount(counterOrder.getTotalAmountWithTax()-(totalAmountPaid+amount));	
				paymentCounter.setPaidDate(new Date());
				paymentCounter.setCounterOrder(counterOrder);
				paymentCounter.setChequeClearStatus(true);
				
				counterOrderService.savePaymentCounter(paymentCounter);
				
				if((totalAmountPaid+paidAmount)==counterOrder.getTotalAmountWithTax()){
					counterOrder.setPayStatus(true);
					paymentCounter.setDueDate(new Date());
				}else{
					try {
						paymentCounter.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
						counterOrder.setPaymentDueDate(dateFormat.parse(dueDate)); } catch (Exception e) {}
					counterOrder.setPayStatus(false);
				}
				counterOrderService.updateCounterOrder(counterOrder);
			}
	
			session.setAttribute("saveMsg", "Payment Done SuccessFully");
		return new ModelAndView("redirect:/counterOrderReport?range=today");
	}
	
	/***
	 * create counter order invoice and add in response
	 * @param request
	 * @param model
	 * @param session
	 * @param response
	 */
	@Transactional 	@RequestMapping("/counterOrderInvoice.pdf")
	public void counterBillPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Invoice");
				
		  try {
			String counterOrderId=request.getParameter("counterOrderId");
				BillPrintDataModel billPrintDataModel=counterOrderService.fetchCounterBillPrintData(counterOrderId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/invoice.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdfCounterOrder(billPrintDataModel, fullPath);
								
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
	
	// Counter Order Invoice for whatsapp
	/***
	 * create counter order invoice and add in response
	 * @param request
	 * @param model
	 * @param session
	 * @param response
	 */
	@Transactional 	@RequestMapping("/counterOrderInvoiceForWhatsApp.pdf")
	public void counterBillPrintForWhatsApp(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Invoice");
				
		  try {
			String counterOrderId=request.getParameter("counterOrderId");
			//long companyId=2;//Long.parseLong(request.getParameter("companyId"));
			long companyId=2;//Long.parseLong(request.getParameter("companyId"));
			long branchId=1; //Long.parseLong(request.getParameter("branchId"));
			
			
				BillPrintDataModel billPrintDataModel=counterOrderService.fetchCounterBillPrintDataForWhatsApp(counterOrderId, companyId, branchId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/invoice.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdfCounterOrder(billPrintDataModel, fullPath);
								
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
	@Transactional
	@RequestMapping("/saveCounterReturnOrder")
	public ResponseEntity<BaseDomain> saveCounterReturnOrder(@RequestBody ReturnCounterRequest returnCounterRequest,HttpServletRequest request){
		BaseDomain baseDomain=new BaseDomain();
		
			
		long returnCounterOrderId =counterOrderService.saveReturnCounterOrderDetails(returnCounterRequest);
			
			
			ReturnCounterOrder returnCounterOrder=counterOrderService.fetchReturnCounterOrder(returnCounterOrderId);
			
			//send return invoice on mail Start here
			if(returnCounterOrder.getCounterOrder().getBusinessName()!=null){
				if(returnCounterOrder.getCounterOrder().getBusinessName().getContact().getEmailId()!=null){
					if(!returnCounterOrder.getCounterOrder().getBusinessName().getContact().getEmailId().isEmpty()){
						BillPrintDataModel billPrintDataModel=counterOrderService.fetchReturnCounterBillPrintData(returnCounterOrder.getReturnCounterOrderId());
						
						//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
						String filePath="/resources/pdfFiles/credit_note.pdf";
						
						ServletContext context = request.getServletContext();
						String appPath = context.getRealPath("/");
						//system.out.println("appPath = " + appPath);

						// construct the complete absolute path of the file
						String fullPath = appPath + filePath;      
						//File downloadFile = new File(fullPath);
						
						//File dFile= InvoiceGenerator.generateInvoicePdfCreditNote(billPrintDataModel, fullPath);
						
						File dFile = null;
						try {
							dFile = InvoiceGenerator.generateInvoicePdfCreditNote(billPrintDataModel, fullPath);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
										
						String fileName=returnCounterOrder.getInvoiceNumber()+".pdf";
						
						EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
						emailSender.sendEmail(" Return Credit Note "+returnCounterOrder.getInvoiceNumber(), " ", returnCounterOrder.getCounterOrder().getBusinessName().getContact().getEmailId(), true, dFile,fileName);
						 
					}
					
				}
				
			}
			
			  
			//send return invoice on mail end here
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/openCounterOrderForReturn")
	public ModelAndView openCounterOrderForReturn(HttpServletRequest request,HttpSession session,Model model) {
		model.addAttribute("counterOrderId", request.getParameter("counterOrderId"));
		return new ModelAndView("counterReturnOrder");
	}
	
	/***
	 * fetch order product details and show 
	 * @param model
	 * @param request
	 * @param session
	 * @return ModelAndView OrderDetails.jsp
	 */
	@Transactional 	@GetMapping("/returnCounterOrderProductDetailsListForWebApp")
	public ModelAndView returnCounterOrderProductDetailsListForWebApp(Model model,HttpServletRequest request,HttpSession session){
		model.addAttribute("pageName", "Return Counter Product Details");
		
		ReturnCounterOrder returnCounterOrder=counterOrderService.fetchReturnCounterOrder(Long.parseLong(request.getParameter("returnCounterOrderId")));
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=counterOrderService.fetchReturnCounterOrderProductModelList(Long.parseLong(request.getParameter("returnCounterOrderId")));
		model.addAttribute("counterOrderProductDetailListForWebApps", orderProductDetailListForWebApps);
		
		double totalDiscountAmount=0;
		double totalAmountWithTax=0,totalAmountWithTaxMain=0;
		long totalQuantity=0;
		double totalNetAmount=0;
		
		for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
		{
			totalDiscountAmount+=orderProductDetailListForWebApp.getDiscountAmt();
			totalAmountWithTaxMain+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			totalQuantity+=orderProductDetailListForWebApp.getQuantity();
			totalAmountWithTax+=orderProductDetailListForWebApp.getNetAmount();
		}
		
		double discountAmtCut=0,discountPerCut=0;
		if(returnCounterOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)){
			discountAmtCut=returnCounterOrder.getDiscount();
			discountPerCut=(returnCounterOrder.getDiscount()/totalAmountWithTax)*100;			
		}else{
			discountPerCut=returnCounterOrder.getDiscount();
			discountAmtCut=(returnCounterOrder.getDiscount()*totalAmountWithTax)/100;
		}
		double totalAmountWithTaxWithDisc=totalAmountWithTax-discountAmtCut;
		
		float roundOffAmount=(float)(MathUtils.round(totalAmountWithTaxWithDisc,0)-MathUtils.round(totalAmountWithTaxWithDisc,2));
		String roundAmt="";
		
		if(roundOffAmount>0){
			roundAmt="+"+roundOffAmount;
		}else if(roundOffAmount<0){
			roundAmt=roundOffAmount+"";
		}else{			
			roundAmt="0";
		}

		totalAmountWithTaxWithDisc=MathUtils.round(totalAmountWithTaxWithDisc, 2);
		
		discountAmtCut=MathUtils.round(discountAmtCut,2);
		discountPerCut=MathUtils.round(discountPerCut,2);
		
		model.addAttribute("totalQuantity", totalQuantity);
		model.addAttribute("totalAmountWithTaxMain", totalAmountWithTaxMain);
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		model.addAttribute("totalDiscountAmount", totalDiscountAmount);
		model.addAttribute("discountAmtCut", discountAmtCut);
		model.addAttribute("discountPerCut", discountPerCut);
		model.addAttribute("totalAmountWithTaxWithDisc", totalAmountWithTaxWithDisc);
		model.addAttribute("roundOffAmount",roundAmt);
		model.addAttribute("totalNetPayable", MathUtils.round(totalAmountWithTaxWithDisc,0));
		model.addAttribute("report", "returnCounterOrder");
		return new ModelAndView("counterOrderDetails");
	}
	
	/***
	 * return counter order report by range
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView counterOrderReoprt.jsp
	 */
	@Transactional 	@RequestMapping("/returnCounterOrderReport")
	public ModelAndView returnCounterOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Return Counter Order Report");
		
		String range=request.getParameter("range"); 
		String startDate=request.getParameter("startDate"); 
		String endDate=request.getParameter("endDate");	
	
		List<CounterReturnOrderModel> returnCounterOrderReportList=counterOrderService.counterReturnOrderReport(range, startDate, endDate); 
		model.addAttribute("returnCounterOrderReportList", returnCounterOrderReportList); 

		return new ModelAndView("counterReturnOrderReport");
	}
	
	/***
	 * create counter order invoice and add in response
	 * @param request
	 * @param model
	 * @param session
	 * @param response
	 */
	@Transactional 	@RequestMapping("/counterReturnOrderInvoice.pdf")
	public void counterReturnBillPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Invoice");
				
		  try {
			long returnCounterOrderId=Long.parseLong(request.getParameter("returnCounterOrderId"));
				BillPrintDataModel billPrintDataModel=counterOrderService.fetchReturnCounterBillPrintData(returnCounterOrderId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/credit_note.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdfCreditNote(billPrintDataModel, fullPath);
								
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
	
	// Proforma Order Start here
	
	
		@Transactional
		@RequestMapping("/saveProformaOrder")
		public @ResponseBody BaseDomain saveProformaOrder(HttpSession session, HttpServletRequest request, Model model) {
			BaseDomain baseDomain = new BaseDomain();
			String proformaOrderId;
			/* try { */

			String transactionRefNo = request.getParameter("transactionRefNo");
			long paymentMethodId = Long.parseLong(request.getParameter("paymentMethodId"));
			String comment = request.getParameter("comment");

			String counterOrderProductDetails = request.getParameter("productList");
			String businessNameId = request.getParameter("businessNameId");
			/*
			 * String paidAmount=request.getParameter("paidAmount"); String
			 * balAmount=request.getParameter("balAmount"); String
			 * dueDate=request.getParameter("dueDate"); String
			 * payType=request.getParameter("payType"); String
			 * paymentType=request.getParameter("paymentType"); String
			 * bankName=request.getParameter("bankName"); String
			 * chequeNumber=request.getParameter("chequeNumber"); String
			 * chequeDate=request.getParameter("chequeDate"); String
			 * custName=request.getParameter("custName"); String
			 * mobileNo=request.getParameter("mobileNo"); String
			 * gstNo=request.getParameter("gstNo");
			 */
			String discountTypeId = request.getParameter("discountType");
			String discountAmount = request.getParameter("discountAmount");

			/*
			 * String
			 * isUsableBalanceUsed=request.getParameter("isUsableBalanceUsed");
			 * String usableBalance=request.getParameter("usableBalance");
			 */

			String isTransportationHave = request.getParameter("isTransportationHave");
			String orderComment = request.getParameter("orderComment");

			String transportCharge = request.getParameter("transportationChargesH");

			long transportationId = 0;
			String vehicalNumber = "";
			String docketNumber = "";
			double trasportationCharge = 0;
			if (isTransportationHave.equals("Yes")) {
				transportationId = Long.parseLong(request.getParameter("transportationId"));
				vehicalNumber = request.getParameter("vehicalNumber");
				docketNumber = request.getParameter("docketNumber");
				if (transportCharge.equals("")) {
					trasportationCharge = 0;
				} else {
					trasportationCharge = Double.parseDouble(transportCharge);
				}

			}

			proformaOrderId = counterOrderService.saveProformaOrder(counterOrderProductDetails, businessNameId,
					transportationId, vehicalNumber, docketNumber, transactionRefNo, comment, paymentMethodId,
					discountTypeId, discountAmount, orderComment, trasportationCharge);

			// to send the invoice on emaild id start

			/*ProformaOrder proformaOrder = counterOrderService.fetchProformaOrder(proformaOrderId);
			if (proformaOrder.getBusinessName() != null) {
				if (proformaOrder.getBusinessName().getContact().getEmailId() != null) {
					if (!proformaOrder.getBusinessName().getContact().getEmailId().isEmpty()) {
						BillPrintDataModel billPrintDataModel = counterOrderService
								.fetchProformaOrderBillPrintData(proformaOrderId);

						// OrderDetails
						// orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
						String filePath = "/resources/pdfFiles/invoice.pdf";

						ServletContext context = request.getServletContext();
						String appPath = context.getRealPath("/");
						// system.out.println("appPath = " + appPath);

						// construct the complete absolute path of the file
						String fullPath = appPath + filePath;
						// File downloadFile = new File(fullPath);

						File dFile = null;
						try {
							//generateInvoicePdfProformaOrder
							dFile = InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String fileName = proformaOrder.getProformaOrderId() + ".pdf";

						EmailSender emailSender = new EmailSender(mailSender, sessionFactory);
						emailSender.sendEmail("Order Invoice " + fileName, " ",
								proformaOrder.getBusinessName().getContact().getEmailId(), true, dFile, fileName);
					}

				}
			}*/

			// to send the invoice on emaild id end

			baseDomain.setErrorMsg(proformaOrderId);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			return baseDomain;
		}

		// Pro-forma Order Report
		@Transactional
		@RequestMapping("/proformaOrderReport")
		public ModelAndView proformaOrderReport(HttpServletRequest request, Model model, HttpSession session) {
			model.addAttribute("pageName", "Pro-forma Invoice Order Report");

			String range = request.getParameter("range");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");

			List<ProformaOrderReport> proformaOrderReports = counterOrderService.fetchProformaOrderReport(range, startDate,
					endDate);
			model.addAttribute("proformaOrderReports", proformaOrderReports);

			model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
			session.setAttribute("saveMsg", null);
			return new ModelAndView("proformaOrderReoprt");
		}

		/**
		 * pro-forma order details by id form edit, convert to counter order , view
		 * @param request
		 * @param response
		 * @return
		 */
		@Transactional
		@RequestMapping("/get-proforma-order-details")
		public ResponseEntity<ProformaOrderDetailsResponse> fetchProFormaOrder(HttpServletRequest request,
				HttpServletResponse response) {
			ProformaOrderDetailsResponse proformaOrderDetailsResponse = new ProformaOrderDetailsResponse();
			long id = Long.parseLong(request.getParameter("id"));

			ProformaOrder proformaOrder = counterOrderService.fetchProformaOrderById(id);
			if (proformaOrder == null) {
				proformaOrderDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
				proformaOrderDetailsResponse.setErrorMsg("Order Details not found");
			} else {
				List<ProformaOrderProductDetails> orderProductDetails = counterOrderService
						.fetchProformaOrderProductDetailsById(id);
				proformaOrderDetailsResponse.setProformaOrder(proformaOrder);
				proformaOrderDetailsResponse.setProductDetailsList(orderProductDetails);
				proformaOrderDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
			}
			return new ResponseEntity<ProformaOrderDetailsResponse>(proformaOrderDetailsResponse, HttpStatus.OK);
		}
		
		@Transactional @RequestMapping("/openProFormaForEdit")
		public ModelAndView openEditProforma(HttpServletRequest request,
				Model model){
			long id = Long.parseLong(request.getParameter("id"));
			model.addAttribute("proformaOrderId", id);
			
			model.addAttribute("pageName", "Counter");

			List<Product> productList=productDAO.fetchProductListForWebApp();
			List<ProductModel> productModelList=new ArrayList<>();
			if(productList!=null){
				for(Product product : productList){
					productModelList.add(new ProductModel(
							product.getProductId(), 
							product.getProductName(), 
							"",
							null,
							null,
							0f,
							/*"",*/ //product content type
							"",
							0l,
							0l,
							0l,
							0l, 
							null,
							null,
							product.getProductBarcode(),
							null));
				}
			}
			model.addAttribute("productList", productModelList);
			
			List<BusinessName> businessNameList=businessNameService.getBusinessNameListForApp();
			List<BusinessNameModel> businessNameModelList=new ArrayList<>();
			for(BusinessName businessName :businessNameList){
				businessNameModelList.add(new BusinessNameModel(
						businessName.getBusinessNamePkId(), 
						businessName.getBusinessNameId(), 
						businessName.getShopName(), 
						null, 
						0, 
						null, 
						null, 
						null, 
						null, 
						null, 
						null, 
						null,
						false,
						false));
			}
			model.addAttribute("businessNameList", businessNameModelList);
			
			List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
			model.addAttribute("paymentMethodList", paymentMethodList);
			
			List<Transportation> transportations = transportationDAO.fetchAll();
			List<TransportationModel> transportationModelList=new ArrayList<>();
			for(Transportation transportation : transportations){
				transportationModelList.add(new TransportationModel(
						transportation.getId(), 
						transportation.getTransportName(), 
						transportation.getMobNo(), 
						transportation.getGstNo(), 
						transportation.getVehicleNo()));
			}
			model.addAttribute("transportations", transportationModelList);

			model.addAttribute("isEdit", false);

			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, -7);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = simpleDateFormat.format(new Date());
			String endDate = simpleDateFormat.format(calendar.getTime());

			List<InvoiceDetails> invoiceDetails = counterOrderService.fetchInvoiceDetails(startDate, endDate);
			model.addAttribute("invoiceDetails", invoiceDetails);
			
			return new ModelAndView("counterOrder");
		}

		/***
		 * create counter order invoice and add in response
		 * 
		 * @param request
		 * @param model
		 * @param session
		 * @param response
		 */
		@Transactional
		@RequestMapping("/proformaOrderInvoice.pdf")
		public void proformaBillPrint(HttpServletRequest request, Model model, HttpSession session,
				HttpServletResponse response) {

			model.addAttribute("pageName", "Invoice");

			try {
				String proformaOrderId = request.getParameter("proformaOrderId");
				BillPrintDataModel billPrintDataModel = counterOrderService
						.fetchProformaOrderBillPrintData(proformaOrderId);

				// OrderDetails
				// orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath = "/resources/pdfFiles/invoice.pdf";

				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				// system.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;
				// File downloadFile = new File(fullPath);
				//generateInvoicePdfProformaOrder
				File dFile = InvoiceGenerator.generateInvoicePdfProformaOrder(billPrintDataModel, fullPath);

				// get your file as InputStream
				InputStream is = new FileInputStream(dFile);
				// copy it to response's OutputStream
				response.setContentType("application/pdf");
				org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				response.flushBuffer();
				response.getOutputStream().close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		
		//Update Proforma Order 
		// Proforma Order Update Start here
			@Transactional
			@RequestMapping("/updateProformaOrder")
			public @ResponseBody BaseDomain updateProformaOrder(HttpSession session, HttpServletRequest request, Model model) {
				BaseDomain baseDomain = new BaseDomain();
				String proformaOrderId;
				/* try { */

				long id=Long.parseLong(request.getParameter("proFormaId"));
				String transactionRefNo = request.getParameter("transactionRefNo");
				long paymentMethodId = Long.parseLong(request.getParameter("paymentMethodId"));
				String comment = request.getParameter("comment");

				String counterOrderProductDetails = request.getParameter("productList");
				String businessNameId = request.getParameter("businessNameId");
				/*
				 * String paidAmount=request.getParameter("paidAmount"); String
				 * balAmount=request.getParameter("balAmount"); String
				 * dueDate=request.getParameter("dueDate"); String
				 * payType=request.getParameter("payType"); String
				 * paymentType=request.getParameter("paymentType"); String
				 * bankName=request.getParameter("bankName"); String
				 * chequeNumber=request.getParameter("chequeNumber"); String
				 * chequeDate=request.getParameter("chequeDate"); String
				 * custName=request.getParameter("custName"); String
				 * mobileNo=request.getParameter("mobileNo"); String
				 * gstNo=request.getParameter("gstNo");
				 */
				String discountTypeId = request.getParameter("discountType");
				String discountAmount = request.getParameter("discountAmount");

				/*
				 * String
				 * isUsableBalanceUsed=request.getParameter("isUsableBalanceUsed");
				 * String usableBalance=request.getParameter("usableBalance");
				 */

				String isTransportationHave = request.getParameter("isTransportationHave");
				String orderComment = request.getParameter("orderComment");

				String transportCharge = request.getParameter("transportationChargesH");

				long transportationId = 0;
				String vehicalNumber = "";
				String docketNumber = "";
				double trasportationCharge = 0;
				if (isTransportationHave.equals("Yes")) {
					transportationId = Long.parseLong(request.getParameter("transportationId"));
					vehicalNumber = request.getParameter("vehicalNumber");
					docketNumber = request.getParameter("docketNumber");
					if (transportCharge.equals("")) {
						trasportationCharge = 0;
					} else {
						trasportationCharge = Double.parseDouble(transportCharge);
					}

				}

				proformaOrderId = counterOrderService.updateProformaOrder(id,counterOrderProductDetails, businessNameId,
						transportationId, vehicalNumber, docketNumber, transactionRefNo, comment, paymentMethodId,
						discountTypeId, discountAmount, orderComment, trasportationCharge);

				// to send the invoice on emaild id start

				/*ProformaOrder proformaOrder = counterOrderService.fetchProformaOrder(proformaOrderId);
				if (proformaOrder.getBusinessName() != null) {
					if (proformaOrder.getBusinessName().getContact().getEmailId() != null) {
						if (!proformaOrder.getBusinessName().getContact().getEmailId().isEmpty()) {
							BillPrintDataModel billPrintDataModel = counterOrderService
									.fetchProformaOrderBillPrintData(proformaOrderId);

							// OrderDetails
							// orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
							String filePath = "/resources/pdfFiles/invoice.pdf";

							ServletContext context = request.getServletContext();
							String appPath = context.getRealPath("/");
							// system.out.println("appPath = " + appPath);

							// construct the complete absolute path of the file
							String fullPath = appPath + filePath;
							// File downloadFile = new File(fullPath);

							File dFile = null;
							try {
								//generateInvoicePdfProformaOrder
								dFile = InvoiceGenerator.generateInvoicePdfProformaOrder(billPrintDataModel, fullPath);
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (DocumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							String fileName = proformaOrder.getProformaOrderId() + ".pdf";

							EmailSender emailSender = new EmailSender(mailSender, sessionFactory);
							emailSender.sendEmail("Proforma Invoice " + fileName, " ",
									proformaOrder.getBusinessName().getContact().getEmailId(), true, dFile, fileName);
						}

					}
				}*/

				// to send the invoice on emaild id end

				baseDomain.setErrorMsg(proformaOrderId);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				return baseDomain;
			}

			
			@Transactional
			@RequestMapping("/deleteProFormaOrder")
			public ModelAndView deleteProformaOrder(HttpSession session, HttpServletRequest request, Model model) {
				
				
				long id=Long.parseLong(request.getParameter("id"));
				
				counterOrderService.deleteProformaOrderById(id);
				
				
				model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
				session.setAttribute("saveMsg", null);
				return new ModelAndView("proformaOrderReoprt");
				
			}
			
			/**
			 * fetch counter order product details by counter order id
			 * 
			 * @param session
			 * @param request
			 * @param model
			 * @return
			 */
			@Transactional
			@RequestMapping("/fetchProformaOrderProductDetailsByProformaOrderId")
			public ModelAndView fetchProformaOrderProductDetailsByProformaOrderId(
					HttpSession session, HttpServletRequest request, Model model) {
				ProformaOrder proformaOrder=counterOrderService.fetchProformaOrderById(Long.parseLong(request.getParameter("proformaOrderId")));
				
				List<ProformaOrderProductDetails> proformaOrderProductDetailsList = counterOrderService
						.fetchProformaOrderProductDetailsById(Long.parseLong(request.getParameter("proformaOrderId")));
				
				List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=new ArrayList<>();
				for(int i=0; i<proformaOrderProductDetailsList.size(); i++){
					ProformaOrderProductDetails proformaOrderProductDetails=proformaOrderProductDetailsList.get(i);
								
					long qty = proformaOrderProductDetails.getPurchaseQuantity();
					double total = qty * proformaOrderProductDetails.getSellingRate();
					
					double discAmt =0;
					if(proformaOrderProductDetails.getDiscountType().equals("Amount")){
						discAmt = proformaOrderProductDetails.getDiscount();
					}else{
						discAmt = (total * proformaOrderProductDetails.getDiscountPer()) / 100;
					}
					total = total - discAmt;
					//Sachin 28Jan
					orderProductDetailListForWebApps
							.add(new OrderProductDetailListForWebApp((i+1),
									proformaOrderProductDetails.getProformaOrder().getProformaOrderId(),
									proformaOrderProductDetails.getProduct().getCategories().getCategoryName(),
									proformaOrderProductDetails.getProduct().getBrand().getName(),
									proformaOrderProductDetails.getProduct().getProductName(),
									"",
									0,
									"",
									proformaOrderProductDetails.getSellingRate(),
									proformaOrderProductDetails.getPurchaseQuantity(),
									0, 
									proformaOrderProductDetails.getDiscountPer(),
									discAmt, total, proformaOrderProductDetails.getSellingRate() * qty,
									proformaOrderProductDetails.getProformaOrder().getDateOfOrderTaken(),
									proformaOrderProductDetails.getType(), ""));
				}
				
				model.addAttribute("pageName", "Proforma Order Product Details");

				model.addAttribute("orderProductDetailListForWebApps", orderProductDetailListForWebApps);

				double totalDiscountAmount = 0;
				double totalAmountWithTax = 0, totalAmountWithTaxMain = 0;
				long totalQuantity = 0, totalReturnQuantity = 0;
				double totalNetAmount = 0;

				for (OrderProductDetailListForWebApp orderProductDetailListForWebApp : orderProductDetailListForWebApps) {
					totalDiscountAmount += orderProductDetailListForWebApp.getDiscountAmt();
					totalAmountWithTaxMain += orderProductDetailListForWebApp.getTotalAmountWithTax();
					totalQuantity += orderProductDetailListForWebApp.getQuantity();
					totalReturnQuantity += orderProductDetailListForWebApp.getReturnQuantity();
					totalAmountWithTax += orderProductDetailListForWebApp.getNetAmount();
				}

				double discountAmtCut = 0, discountPerCut = 0;
				if (proformaOrder.getDiscountType().equals(Constants.DISCOUNT_TYPE_AMOUNT)) {
					discountAmtCut = proformaOrder.getDiscount();
					discountPerCut = (proformaOrder.getDiscount() / totalAmountWithTax) * 100;
				} else {
					discountPerCut = proformaOrder.getDiscount();
					discountAmtCut = (proformaOrder.getDiscount() * totalAmountWithTax) / 100;
				}

				double totalAmountWithTaxWithDisc = totalAmountWithTax - discountAmtCut;
				float roundOffAmount = (float) (MathUtils.round(totalAmountWithTaxWithDisc, 0)
						- MathUtils.round(totalAmountWithTaxWithDisc, 2));
				String roundAmt = "";

				if (roundOffAmount > 0) {
					roundAmt = "+" + roundOffAmount;
				} else if (roundOffAmount < 0) {
					roundAmt = roundOffAmount + "";
				} else {
					roundAmt = "0";
				}

				totalAmountWithTaxWithDisc = MathUtils.round(totalAmountWithTaxWithDisc, 2);

				discountAmtCut = MathUtils.round(discountAmtCut, 2);
				discountPerCut = MathUtils.round(discountPerCut, 2);

				model.addAttribute("totalQuantity", totalQuantity);
				model.addAttribute("totalAmountWithTaxMain", totalAmountWithTaxMain);
				model.addAttribute("totalAmountWithTax", totalAmountWithTax);
				model.addAttribute("totalDiscountAmount", totalDiscountAmount);
				model.addAttribute("discountAmtCut", discountAmtCut);
				model.addAttribute("discountPerCut", discountPerCut);
				model.addAttribute("totalAmountWithTaxWithDisc", totalAmountWithTaxWithDisc);
				model.addAttribute("roundOffAmount", roundAmt);
				model.addAttribute("totalNetPayable", MathUtils.round(totalAmountWithTaxWithDisc, 0));
				model.addAttribute("report", "counterOrder");
							
				return new ModelAndView("proformaOrderProductDetails");
			}
}
