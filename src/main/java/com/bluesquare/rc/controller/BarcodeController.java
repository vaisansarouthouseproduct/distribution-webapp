package com.bluesquare.rc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * <pre>
 * @author Parag -- Code 
 * API End Points 
 * 1.openBarcodeSeriesGeneratorForm
 * 2.openBarcodeGeneratorForm
 */
@Controller
public class BarcodeController {
	
	/**
	 * <pre>
	 * openBarcodeGeneratorForm 
	 * @param model
	 * @param request
	 * @return ModelAndView 
	 * </pre> 
	 */
	@Transactional
	@RequestMapping("/openBarcodeSeriesGeneratorForm")
	public ModelAndView openEMICalculatorForm(Model model,HttpServletRequest request){
		System.out.println("Hii My first barcode code");
		
		model.addAttribute("pageName", "Barcode Generator");
		
		return new ModelAndView("barcodeSeriesGenerator");
	}
	
	/**
	 * <pre>
	 * openBarcodeCopyGeneratorForm 
	 * @param model
	 * @param request
	 * @return ModelAndView 
	 * </pre> 
	 */
	@Transactional
	@RequestMapping("/openBarcodeGeneratorForm")
	public ModelAndView openBarcodeCopyGeneratorForm(Model model,HttpServletRequest request){
		System.out.println("Hii My first barcode code");
		
		model.addAttribute("pageName", "Barcode Generator");
		
		return new ModelAndView("barcodeCopyGenerator");
	}
}
