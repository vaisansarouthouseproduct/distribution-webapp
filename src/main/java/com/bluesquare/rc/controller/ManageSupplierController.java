package com.bluesquare.rc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.PaymentMethod;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.FetchSupplierList;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.StateModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.responseEntities.SupplierProductListModule;
import com.bluesquare.rc.rest.models.PaymentPaySupplierListResponse;
import com.bluesquare.rc.rest.models.PaymentPaySupplierModule;
import com.bluesquare.rc.rest.models.SupplierProductListResponse;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.RoundOff;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * API End Points
 * 1.addSupplier
 * 2.checkSupplierDuplicationForSave
 * 3.checkSupplierDuplicationForUpdate
 * 4.saveSupplier
 * 5.fetchProductListByBrandIdAndCategoryId
 * 6.fetchSupplierList
 * 7.fetchSupplierListBySupplierId
 * 8.fetchSupplierProductList
 * 9.fechSupplier
 * 10.updateSupplier
 * 11.sendSMSTOSupplier
 * 12.paymentSupplier
 * 13.giveSupplierOrderPayment
 * 14.updatePaymentSupplier
 * 15.fetchPaymentSupplier
 * 16.deletePaymentSupplier
 * 17.editPaymentSupplier
 * </pre>
 */
@Controller
public class ManageSupplierController {

	@Autowired
	SupplierService supplierService;
	
	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@Autowired
	Contact contact;
	
	@Autowired
	PaymentPaySupplier paymentPaySupplier;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	Supplier supplier;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	StateService stateService;
	
	/***
	 * open add supplier section with brand,category,product list
	 * @param model
	 * @param session
	 * @return addSupplier.jsp
	 */
	@Transactional 	@RequestMapping("/addSupplier")
	public ModelAndView manageSupplier(Model model,HttpSession session) {
		System.out.println("in addSupplier controller");
		
		model.addAttribute("pageName", "Add Supplier");
		
				

		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		List<State> stateList=stateService.fetchAllStateForWebApp();
		List<StateModel> stateModelList=new ArrayList<>();
		if(stateList!=null){
			for(State state:stateList){
				stateModelList.add(new StateModel(state.getStateId(), state.getCode(), state.getName()));
				
			}
		}
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		List<CategoriesModel> categoryList=categoriesService.fetchCategoriesModelList();
		List<Product> productList=productService.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												product.getCategories().getCategoryDescription(), 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												product.getCategories().getCategoryDate(), 
												product.getCategories().getCategoryUpdateDate()), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												product.getBrand().getBrandAddedDatetime(), 
												product.getBrand().getBrandUpdateDatetime()), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										product.getProductDescription(), 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										product.getFreeQuantity(), 
										product.getProductAddedDatetime(), 
										product.getProductQuantityUpdatedDatetime(),
										product.getProductBarcode(),
										null));
			}
		}
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		model.addAttribute("productlist", productModelList);
		model.addAttribute("stateList", stateModelList);
		
		return new ModelAndView("addSupplier");
	}
	/**
	 * <pre>
	 * check duplication when adding supplier with 
	 * 1.name
	 * 2.gst number
	 * 3.mobile number
	 * 4.emailid
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkSupplierDuplicationForSave")
	public @ResponseBody String checkSupplierDuplicationForSave(Model model,HttpServletRequest request,HttpSession session)
	{
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return supplierService.checkSupplierDuplication(checkText, type, "0");
	}
	/**
	 * <pre>
	 * check duplication when updating supplier with 
	 * 1.name
	 * 2.gst number
	 * 3.mobile number
	 * 4.emailid
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkSupplierDuplicationForUpdate")
	public @ResponseBody String checkSupplierDuplicationForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		String supplierId=request.getParameter("supplierId");
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return supplierService.checkSupplierDuplication(checkText, type, supplierId);
	}
	/**
	 * <pre>
	 * save supplier details with product details
	 * </pre>
	 * @param supplier
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/fetchSupplierList
	 */
	@Transactional 	@RequestMapping("/saveSupplier")
	public ModelAndView saveSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in saveSupplier controller");
		
		
		
		String productIdList=request.getParameter("productIdList");
		
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		
		supplier.setContact(contact);
		
		/*long stateId = Long.parseLong(request.getParameter("stateId"));
		State state = stateService.fetchState(stateId);
		supplier.setState(state);*/
		
		String taxType=request.getParameter("taxType");
		supplier.setTaxType(taxType);
		supplier.setSupplierAddedDatetime(new Date());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplier.setBranch(branchService.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		supplierService.saveSupplier(supplier);
		
		supplierService.saveSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListByBrandIdAndCategoryId")
	public @ResponseBody List<ProductModel> fetchProductListByBrandIdAndCategoryId(HttpServletRequest request)
	{
		long categoryId=Long.parseLong(request.getParameter("categoryId"));
		long brandId=Long.parseLong(request.getParameter("brandId"));
		
		List<Product> productList=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(categoryId,brandId);
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												product.getCategories().getCategoryDescription(), 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												product.getCategories().getCategoryDate(), 
												product.getCategories().getCategoryUpdateDate()), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												product.getBrand().getBrandAddedDatetime(), 
												product.getBrand().getBrandUpdateDatetime()), 
										0, 
										/*product.getProductContentType(),*/ 
										null, 
										0, 
										product.getCurrentQuantity(), 
										0, 
										0, 
										null, 
										null,
										null,
										null));
			}
		}	
		return productModelList;
	}
	
	/**
	 * <pre>
	 * fetch supplier details list for manage supplier section
	 * </pre>
	 * @param model
	 * @param session
	 * @return ManageSupplier.jsp
	 */
	@Transactional 	@RequestMapping("/fetchSupplierList")
	public ModelAndView fetchSupplierList(Model model,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		List<FetchSupplierList> fetchSupplierList=supplierService.fetchSupplierForWebApp();
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageSupplier");
	}
	/**
	 * <pre>
	 * fetch one supplier details list for manage supplier section
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return ManageSupplier.jsp
	 */
	@Transactional 	@RequestMapping("/fetchSupplierListBySupplierId")
	public ModelAndView fetchSupplierListBySupplierId(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		List<FetchSupplierList> fetchSupplierList=new ArrayList<>();
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		fetchSupplierList.add(new FetchSupplierList(supplier.getSupplierId(), 1, supplier.getName(), supplier.getContact().getEmailId(), supplier.getContact().getMobileNumber(), supplier.getAddress(), supplier.getGstinNo(), supplier.getSupplierAddedDatetime(),supplier.getSupplierUpdatedDatetime()));
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		
		return new ModelAndView("ManageSupplier");
	}
	/**
	 * fetch supplier product list by supplierId
	 * @param model
	 * @param request
	 * @return SupplierProductList
	 */
	@Transactional 	@RequestMapping("/fetchSupplierProductList")
	public @ResponseBody SupplierProductListResponse fetchSupplierProductList(Model model,HttpServletRequest request) {
		System.out.println("in fetchSupplierProductList controller");
		
		String supplierId=request.getParameter("supplierId");
		Supplier supplier=supplierService.fetchSupplier(supplierId);
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
		if(fetchSupplierProductList==null){
			fetchSupplierProductList=new ArrayList<>();
		}
		List<SupplierProductListModule> supplierProductListModuleList=new ArrayList<>();
		for(SupplierProductList supplierProductList: fetchSupplierProductList){
			supplierProductListModuleList.add(new SupplierProductListModule(
					supplierProductList.getProduct().getCategories().getIgst(), 
					supplierProductList.getSupplier().getSupplierId(),
					supplierProductList.getSupplier().getName(),
					supplierProductList.getProduct().getProductId(),
					supplierProductList.getProduct().getProductName(), 
					supplierProductList.getProduct().getCategories().getCategoryId(), 
					supplierProductList.getProduct().getCategories().getCategoryName(), 
					supplierProductList.getProduct().getBrand().getBrandId(), 
					supplierProductList.getProduct().getBrand().getName(), 
					supplierProductList.getSupplierRate()));
		}
		SupplierProductListResponse supplierProductListResponse=new SupplierProductListResponse(
				supplier.getName(), 
				supplier.getContact().getMobileNumber(), 
				supplier.getAddress(), 
				supplier.getGstinNo(), 
				supplierProductListModuleList);
		return supplierProductListResponse;
	}
	
	/**
	 * <pre>
	 * fetch supplier details with products 
	 * brand,category,product list for change product details
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return updateSupplier.jsp
	 */
	@Transactional 	@RequestMapping("/fechSupplier")
	public ModelAndView fechSupplier(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		
		
		  model.addAttribute("pageName", "Update Supplier");
		
		String supplierId=request.getParameter("supplierId");
		Supplier supplier=supplierService.fetchSupplier(supplierId);	
		
		/*State supplierState=stateService.fetchState(supplier.getState().getStateId());
		StateModel stateModel=new StateModel(supplierState.getStateId(), supplierState.getCode(), supplierState.getName());*/
				
		SupplierModel supplierModel=new SupplierModel(
				supplier.getSupplierPKId(), 
				supplier.getSupplierId(), 
				supplier.getName(), 
				supplier.getContact(), 
				supplier.getAddress(), 
				supplier.getGstinNo(),
				supplier.getTaxType(),
				supplier.getSupplierAddedDatetime(), 
				supplier.getSupplierUpdatedDatetime());		
		model.addAttribute("supplier", supplierModel);
		
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);

		String productidlist="";
		model.addAttribute("count","");

		List<SupplierProductListModule> supplierProductListModuleList=new ArrayList<>();
		if(fetchSupplierProductList!=null)
		{
			for(SupplierProductList supplierProductList: fetchSupplierProductList){
				supplierProductListModuleList.add(new SupplierProductListModule(
						supplierProductList.getProduct().getCategories().getIgst(), 
						supplierProductList.getSupplier().getSupplierId(),
						supplierProductList.getSupplier().getName(),
						supplierProductList.getProduct().getProductId(),
						supplierProductList.getProduct().getProductName(), 
						supplierProductList.getProduct().getCategories().getCategoryId(), 
						supplierProductList.getProduct().getCategories().getCategoryName(), 
						supplierProductList.getProduct().getBrand().getBrandId(), 
						supplierProductList.getProduct().getBrand().getName(), 
						supplierProductList.getSupplierRate()));
			}
			model.addAttribute("count",supplierProductListModuleList.size()+1);
		}
		
		model.addAttribute("supplierProductList", supplierProductListModuleList);	
		
		productidlist=supplierService.getProductIdList(fetchSupplierProductList);
		model.addAttribute("productidlist", productidlist);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		List<State> stateList=stateService.fetchAllStateForWebApp();
		List<StateModel> stateModelList=new ArrayList<>();
		if(stateList!=null){
			for(State state:stateList){
				stateModelList.add(new StateModel(state.getStateId(), state.getCode(), state.getName()));
				
			}
		}
//		List<Product> productList=productService.fetchProductListForWebApp();
//		List<ProductModel> productModelList=new ArrayList<>();
//		if(productList!=null){
//			for(Product product : productList){
//				productModelList.add(new ProductModel(
//										product.getProductId(), 
//										product.getProductName(), 
//										product.getProductCode(), 
//										new CategoriesModel(
//												product.getCategories().getCategoryId(), 
//												product.getCategories().getCategoryName(), 
//												product.getCategories().getCategoryDescription(), 
//												product.getCategories().getHsnCode(), 
//												product.getCategories().getCgst(), 
//												product.getCategories().getSgst(), 
//												product.getCategories().getIgst(), 
//												product.getCategories().getCategoryDate(), 
//												product.getCategories().getCategoryUpdateDate()), 
//										new BrandModel(
//												product.getBrand().getBrandId(), 
//												product.getBrand().getName(), 
//												product.getBrand().getBrandAddedDatetime(), 
//												product.getBrand().getBrandUpdateDatetime()), 
//										product.getRate(), 
//										/*product.getProductContentType(),*/ 
//										product.getProductDescription(), 
//										product.getThreshold(), 
//										product.getCurrentQuantity(), 
//										product.getDamageQuantity(), 
//										product.getFreeQuantity(), 
//										product.getProductAddedDatetime(), 
//										product.getProductQuantityUpdatedDatetime(),
//										product.getProductBarcode(),
//										null));
//			}
//		}
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		List<CategoriesModel> categoryList=categoriesService.fetchCategoriesModelList();
		
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		//model.addAttribute("stateList", stateModelList);
//		model.addAttribute("productlist", productModelList);
		
		return new ModelAndView("updateSupplier");
	}
	/**
	 * <pre>
	 * update supplier and its product details
	 * </pre>
	 * @param supplier
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/fetchSupplierList
	 */
	@Transactional 	@RequestMapping("/updateSupplier")
	public ModelAndView updateSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in updateSupplier controller");
		
		
		
		String supplierId=request.getParameter("supplierId");
		Supplier supler=supplierService.fetchSupplier(supplierId);
		supplier.setSupplierPKId(supler.getSupplierPKId());
		supplier.setBranch(supler.getBranch());
		String productIdList=request.getParameter("productIdList"); 
		
		contact.setContactId(supler.getContact().getContactId());
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		supplier.setContact(contact);
		
		String taxType=request.getParameter("taxType");
		supplier.setTaxType(taxType);
		
	
		/*long stateId = Long.parseLong(request.getParameter("stateId"));
		State state = stateService.fetchState(stateId);
		supplier.setState(state);*/
		
		supplier.setSupplierAddedDatetime(supler.getSupplierAddedDatetime());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplierService.updateSupplier(supplier);
		
		supplierService.updateSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	/**
	 * <pre>
	 * send sms to selected supplier
	 * if one supplier selected then send sms on given number otherwise registered numbers
	 * </pre>
	 * @param request
	 * @return Success/failure
	 */
	@Transactional 	@RequestMapping("/sendSMSTOSupplier")
	public @ResponseBody String sendSMSTOSupplier(HttpServletRequest request) {
		
		try {
			String supplierIds=request.getParameter("supplierIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			supplierService.sendSMSTOSupplier(supplierIds, smsText, mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	/**
	 * <pre>
	 * payment given to supplier page open with payment details according inventory transaction   
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return makePayment.jsp
	 */
	@Transactional 	@RequestMapping("/paymentSupplier")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Make Supplier Payment");
		
		
		String inventoryTransactionId=request.getParameter("inventoryTransactionId");
				
		PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryTransactionId);
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		return new ModelAndView("makePayment");
	}
	/*** 
	 * <pre>
	 * give payment to supplier according inventory transaction
	 * entry in ledger debit side and company balance
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return redirect to where it from redirect for update payment
	 */
	@Transactional 	@RequestMapping("/giveSupplierOrderPayment")
	public ModelAndView giveSupplierOrderPayment(HttpServletRequest request,HttpSession session,Model model) {
		
		
		
					
			//String employeeDetailsId=request.getParameter("employeeDetailsId");
			String inventoryId=request.getParameter("inventoryId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			
			inventory=inventoryService.fetchInventory(inventoryId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryId);
			

			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			
			
			
			String payType=request.getParameter("payType");
			if(payType.equals(Constants.CASH_PAY_STATUS))
			{
				paymentPaySupplier.setChequeDate(null);
				paymentPaySupplier.setBankName(null);
				paymentPaySupplier.setChequeNumber(null);
				
				paymentPaySupplier.setTransactionReferenceNumber(null);
				paymentPaySupplier.setPaymentMethod(null);
				
			}else if(payType.equals(Constants.OTHER_PAY_STATUS)) {
				paymentPaySupplier.setChequeDate(null);
				paymentPaySupplier.setBankName(null);
				paymentPaySupplier.setChequeNumber(null);
				
				paymentPaySupplier.setTransactionReferenceNumber(transactionRefNo);
				paymentPaySupplier.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
			}
			else
			{
				paymentPaySupplier.setTransactionReferenceNumber(null);
				paymentPaySupplier.setPaymentMethod(null);
				
				paymentPaySupplier.setBankName(bankName);
				paymentPaySupplier.setChequeNumber(cheqNo);
				try {
					//paymentPaySupplier.setChequeDate(dateFormat.parse(cheqDate));
					paymentPaySupplier.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			paymentPaySupplier.setComment(comment);
			paymentPaySupplier.setBankName(bankName);
			paymentPaySupplier.setChequeNumber(cheqNo);
			paymentPaySupplier.setDueAmount(paymentDoInfo.getAmountUnPaid()-amount);
			paymentPaySupplier.setDueDate(new Date());
			paymentPaySupplier.setPaidAmount(amount);
			paymentPaySupplier.setPaidDate(new Date());			
			
			if(paymentPaySupplier.getDueAmount()==0)
			{
				inventory.setPayStatus(true);
			}
			else
			{
				inventory.setPayStatus(false);
			}
			paymentPaySupplier.setPayType(payType);
			paymentPaySupplier.setInventory(inventory);
			inventoryService.givePayment(paymentPaySupplier);			
		
			session.setAttribute("saveMsg", "Payment Done SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl"));
	}
	/*** 
	 * <pre>
	 * update payment to supplier according inventory transaction
	 * update entry in ledger debit side and comapny bal
	 * </pre>
	 * @param request
	 * @param session
	 * @param model
	 * @return redirect to where it from redirect for update payment
	 */
	@Transactional 	@RequestMapping("/updatePaymentSupplier")
	public ModelAndView updatePaymentSupplier(HttpServletRequest request,HttpSession session,Model model) {
		
			//String employeeDetailsId=request.getParameter("employeeDetailsId");
			String inventoryId=request.getParameter("inventoryId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			//String comment=request.getParameter("comment");
			
			inventory=inventoryService.fetchInventory(inventoryId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			//PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryId);

			String transactionRefNo=request.getParameter("transactionRefNo");
			long paymentMethodId=Long.parseLong(request.getParameter("paymentMethodId"));
			
			paymentPaySupplier=inventoryService.fetchPaymentPaySupplierListByPaymentPaySupplierId(request.getParameter("paymentId"));
			String payType=request.getParameter("payType");
			if(payType.equals(Constants.CASH_PAY_STATUS))
			{
				paymentPaySupplier.setChequeDate(null);
				paymentPaySupplier.setBankName(null);
				paymentPaySupplier.setChequeNumber(null);
				
				paymentPaySupplier.setTransactionReferenceNumber(null);
				paymentPaySupplier.setPaymentMethod(null);
				
			}else if(payType.equals(Constants.OTHER_PAY_STATUS)) {
				paymentPaySupplier.setChequeDate(null);
				paymentPaySupplier.setBankName(null);
				paymentPaySupplier.setChequeNumber(null);
				
				paymentPaySupplier.setTransactionReferenceNumber(transactionRefNo);
				paymentPaySupplier.setPaymentMethod(paymentService.fetchPaymentMethodById(paymentMethodId));
			}
			else
			{
				paymentPaySupplier.setTransactionReferenceNumber(null);
				paymentPaySupplier.setPaymentMethod(null);
				
				paymentPaySupplier.setBankName(bankName);
				paymentPaySupplier.setChequeNumber(cheqNo);
				try {
					//paymentPaySupplier.setChequeDate(dateFormat.parse(cheqDate));
					paymentPaySupplier.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			paymentPaySupplier.setBankName(bankName);
			paymentPaySupplier.setChequeNumber(cheqNo);
			//paymentPaySupplier.setDueAmount(paymentDoInfo.getAmountUnPaid()-amount);
			paymentPaySupplier.setDueDate(new Date());
			paymentPaySupplier.setPaidAmount(amount);	
			paymentPaySupplier.setPayType(payType);
			
			paymentPaySupplier.setInventory(inventory);
			inventoryService.updateSupplierPayment(paymentPaySupplier);			
		
			session.setAttribute("saveMsg", "Payment Updated SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl"));
	}
	/**
	 * <pre>
	 * fetch payment details of supplier according inventory transaction 
	 * </pre>
	 * @param request
	 * @return PaymentPaySupplier list
	 */
	@Transactional 	@RequestMapping("/fetchPaymentSupplier")
	public @ResponseBody PaymentPaySupplierListResponse fetchPaymentSupplier(HttpServletRequest request) {
		
		String inventoryId=request.getParameter("inventoryId");		
		Inventory inventory=inventoryService.fetchInventory(inventoryId);
		List<PaymentPaySupplier>  paymentPaySupplierList=inventoryService.fetchPaymentPaySupplierListByInventoryId(inventoryId);
		List<PaymentPaySupplierModule>  paymentPaySupplierModuleList=new ArrayList<>();
		if(paymentPaySupplierList!=null){
			for(PaymentPaySupplier paymentPaySupplier : paymentPaySupplierList){
				String mode="";
				if (paymentPaySupplier.getPayType().equals("Cash")) {
					mode = "Cash";
				} else if (paymentPaySupplier.getPayType().equals("Other")) {
					mode = paymentPaySupplier.getPaymentMethod().getPaymentMethodName() + "-" + paymentPaySupplier.getTransactionReferenceNumber();
				} else {
					mode = paymentPaySupplier.getBankName() + "-" + paymentPaySupplier.getChequeNumber();
				}
				
				paymentPaySupplierModuleList.add(new PaymentPaySupplierModule(
						paymentPaySupplier.getPaymentPayId(), 
						paymentPaySupplier.getDueAmount(), 
						paymentPaySupplier.getDueDate(), 
						paymentPaySupplier.getPaidAmount(), 
						paymentPaySupplier.getPaidDate(), 
						mode, 
						paymentPaySupplier.getChequeNumber(), 
						paymentPaySupplier.getBankName(), 
						paymentPaySupplier.getChequeDate(), 
						paymentPaySupplier.isStatus()));
			}
		}
		
		double roundOffAmt=RoundOff.findRoundOffAmount(inventory.getTotalAmountTax());
		
		PaymentPaySupplierListResponse paymentPaySupplierListResponse=new PaymentPaySupplierListResponse(
				inventory.getInventoryTransactionId(), 
				inventory.getSupplier().getSupplierId(), 
				roundOffAmt, 
				paymentPaySupplierModuleList);
		return paymentPaySupplierListResponse;
	}
	/***
	 * <pre>
	 * delete payment of supplier 
	 * delete ledger entry and company bal increase
	 * </pre>
	 * @param request
	 * @return PaymentPaySupplier list
	 */
	@Transactional 	@RequestMapping("/deletePaymentSupplier")
	public @ResponseBody PaymentPaySupplierListResponse deletePaymentSupplier(HttpServletRequest request) {
		
		String inventoryId=request.getParameter("inventoryId");
		String paymentPayId=request.getParameter("paymentPayId");
		
		inventoryService.deletePaymentPaySupplierListByInventoryId(paymentPayId);
		Inventory inventory=inventoryService.fetchInventory(inventoryId);
		List<PaymentPaySupplier>  paymentPaySupplierList=inventoryService.fetchPaymentPaySupplierListByInventoryId(inventoryId);
		List<PaymentPaySupplierModule>  paymentPaySupplierModuleList=new ArrayList<>();
		if(paymentPaySupplierList!=null){
			for(PaymentPaySupplier paymentPaySupplier : paymentPaySupplierList){
				String mode="";
				if (paymentPaySupplier.getPayType().equals("Cash")) {
					mode = "Cash";
				} else if (paymentPaySupplier.getPayType().equals("Other")) {
					mode = paymentPaySupplier.getPaymentMethod().getPaymentMethodName() + "-" + paymentPaySupplier.getTransactionReferenceNumber();
				} else {
					mode = paymentPaySupplier.getBankName() + "-" + paymentPaySupplier.getChequeNumber();
				}
				
				paymentPaySupplierModuleList.add(new PaymentPaySupplierModule(
						paymentPaySupplier.getPaymentPayId(), 
						paymentPaySupplier.getDueAmount(), 
						paymentPaySupplier.getDueDate(), 
						paymentPaySupplier.getPaidAmount(), 
						paymentPaySupplier.getPaidDate(), 
						mode, 
						paymentPaySupplier.getChequeNumber(), 
						paymentPaySupplier.getBankName(), 
						paymentPaySupplier.getChequeDate(), 
						paymentPaySupplier.isStatus()));
			}
		}
		
		double roundOffAmt=RoundOff.findRoundOffAmount(inventory.getTotalAmountTax());
		
		PaymentPaySupplierListResponse paymentPaySupplierListResponse=new PaymentPaySupplierListResponse(
				inventory.getInventoryTransactionId(), 
				inventory.getSupplier().getSupplierId(), 
				roundOffAmt, 
				paymentPaySupplierModuleList);
		return paymentPaySupplierListResponse;
	}
	/**
	 * edit supplier page open with payment details according inventory transaction
	 * @param model
	 * @param request
	 * @param session
	 * @return makePayment.jsp
	 */
	@Transactional 	@RequestMapping("/editPaymentSupplier")
	public  ModelAndView editPaymentSupplier(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Make Supplier Payment");
		
		String paymentPayId=request.getParameter("paymentPayId");
				
		PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentPaySupplierForEdit(paymentPayId);
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		model.addAttribute("forEdit", "forSupplierPaymentEdit");
		
		List<PaymentMethod> paymentMethodList=paymentService.fetchPaymentMethodList();
		model.addAttribute("paymentMethodList", paymentMethodList);
		
		return new ModelAndView("makePayment");
	}
}
