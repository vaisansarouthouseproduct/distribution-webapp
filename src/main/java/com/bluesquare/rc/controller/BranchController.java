package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.models.BranchListResponse;
import com.bluesquare.rc.models.BranchModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.NewTokenResponse;
import com.bluesquare.rc.service.BranchService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Code And Documentation
 * API End Points : 
 * 1.changeBranchPanel/{branchId}
 * 2.changeBranchApp/{branchId}
 * 3.fetch_branch_list_by_companyId/{companyId}
 * </pre>
 */
@Controller
public class BranchController {

	@Autowired
	BranchService branchService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Transactional
	@RequestMapping("/changeBranchPanel/{branchId}")
	public ResponseEntity<BaseDomain> changeBranchPanel(@ModelAttribute("branchId")long branchId, HttpServletRequest request,HttpSession session) {
		List<Long> branchIds=new ArrayList<Long>();
		branchIds.add(branchId);
		session.setAttribute("selectedBranchIds", branchIds);
		
		BaseDomain baseDomain=new BaseDomain();
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping("/changeBranchApp/{branchId}")
	public ResponseEntity<NewTokenResponse> changeBranchApp(@ModelAttribute("branchId")long branchId, HttpServletRequest request,HttpSession session) throws Exception {
				
		NewTokenResponse newTokenResponse=new NewTokenResponse();
		newTokenResponse.setStatus(Constants.SUCCESS_RESPONSE);
		
		String token=tokenHandlerDAO.createNewToken(request.getHeader("Authorization"), branchId);
		newTokenResponse.setToken(token);
		
		return new ResponseEntity<NewTokenResponse>(newTokenResponse, HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/fetch_branch_list_by_companyId/{companyId}")
	public ResponseEntity<BranchListResponse> fetchBranchListByCompanyId(@ModelAttribute("companyId")long companyId, HttpServletRequest request,HttpSession session) throws Exception {
		
		List<BranchModel> branchList=branchService.fetchBranchListByCompanyId(companyId);
		BranchListResponse branchListResponse=new BranchListResponse();
		if(branchList==null){
			branchListResponse.setStatus(Constants.FAILURE_RESPONSE);	
			branchListResponse.setErrorMsg("Branches Not Found");
		}else{
			branchListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			branchListResponse.setBranchList(branchList);
		}
		return new ResponseEntity<BranchListResponse>(branchListResponse, HttpStatus.OK);
	}
}
