package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.CityModel;
import com.bluesquare.rc.responseEntities.RegionModel;
import com.bluesquare.rc.responseEntities.StateModel;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.RegionService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.utils.Constants;

/**
 * <pre>
 * @author Sachin Pawar 19-05-2018 Code Documentation
 * API Endpoints:
 * 1.location_admin
 * 2.fetchCountryList
 * 3.fetchCountry
 * 4.saveCountry
 * 5.updateCountry
 * 6.fetchStateList
 * 7.fetchState
 * 8.saveState
 * 9.updateState
 * 10.fetchCityList
 * 11.fetchCity
 * 12.fetchCityListByStateId
 * 13.saveCity
 * 14.updateCity
 * 15.fetchStateListByCountryId 
 * 16.location_company
 * 17.fetchCityListByCompanyId
 * 18.saveRegion
 * 19.updateRegion
 * 20.fetchRegionListByCityId
 * 21.fetchRegion
 * 22.fetchRegionList
 * 23.fetchAreaList
 * 24.fetchArea
 * 25.saveArea
 * 26.updateArea
 * 27.fetchAreaListByRegionId
 * </pre>
 */
@Controller
public class LocationController {

	@Autowired
	CountryService countryService;

	@Autowired
	Country country;

	@Autowired
	StateService stateService;

	@Autowired
	State state;

	@Autowired
	CityService cityService;

	@Autowired
	City city;

	@Autowired
	RegionService regionService;

	@Autowired
	Region region;

	@Autowired
	AreaService areaService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	Area area;
	
	@Autowired 
	TokenHandlerDAO tokenHandlerDAO; 

	/**
	 * location section open for Admin 
	 * where Admin can manage City,State,Country
	 * @param model
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/location_admin")
	public ModelAndView location(Model model, HttpSession session) {

		model.addAttribute("pageName", "Location Management");

		return new ModelAndView("location_admin");
	}

	@Transactional 	@RequestMapping("/fetchCountryList")
	public @ResponseBody List<Country> fetchCountryList(Model model) {
		System.out.println("in manageSupplier controller");

		List<Country> countryList = countryService.fetchCountryListForWebApp();

		return countryList;
	}

	@Transactional 	@RequestMapping("/fetchCountry")
	public @ResponseBody Country fetchCountry(HttpServletRequest request) {
		System.out.println("in manageSupplier controller");

		long countryId = Long.parseLong(request.getParameter("countryId"));
		Country country = countryService.fetchCountryForWebApp(countryId);

		return country;
	}

	/**
	 * check country name duplication
	 * if country id  zero then save else update
	 * @param request
	 * @return success/failed/already exist
	 */
	@Transactional 	@RequestMapping("/saveCountry")
	public @ResponseBody String saveCountryForWeb(HttpServletRequest request) {
		System.out.println("in Save Country");

		this.country.setCountryId(Long.parseLong(request.getParameter("countryId")));
		this.country.setName(request.getParameter("name"));
		boolean flag = false;
		List<Country> coutryList = countryService.fetchCountryListForWebApp();

		country.setName(
				(Character.toString(country.getName().charAt(0)).toUpperCase() + (country.getName().substring(1)).toLowerCase()));

		if (country.getCountryId() == 0) {

			if (coutryList != null) {
				for (Country coutryFromDb : coutryList) {
					if (coutryFromDb.getName().trim().toUpperCase().equals(country.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}

			if (!flag) {
				/* country.setDisable(false); */
				countryService.saveCountryForWeb(country);
				return Constants.SUCCESS_RESPONSE;
			}
		}
		if (country.getCountryId() != 0) {

			if (coutryList != null) {
				for (Country coutryFromDb : coutryList) {
					if (coutryFromDb.getName().trim().toUpperCase().equals(country.getName().trim().toUpperCase())
							&& coutryFromDb.getCountryId() != country.getCountryId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				return updateCountryForWeb(country);
			}
		}

		return Constants.ALREADY_EXIST;
	}

	@Transactional 	@RequestMapping("/updateCountry")
	public @ResponseBody String updateCountryForWeb(@ModelAttribute Country country) {

		System.out.println("in Update Country");

		if (country.getCountryId() == 0 || country.getName() == null || country.getName().equals("")) {
			System.out.println("can not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		countryService.updateCountryForWeb(country);

		return Constants.SUCCESS_RESPONSE;
	}

	@Transactional 	@RequestMapping("/fetchStateList")
	public @ResponseBody List<State> fetchStateList(Model model) {
		System.out.println("in manageSupplier controller");

		List<State> stateList = stateService.fetchAllStateForWebApp();
		return stateList;
	}

	@Transactional 	@RequestMapping("/fetchState")
	public @ResponseBody State fetchState(HttpServletRequest request) {
		System.out.println("in fetchState controller");

		long stateId = Long.parseLong(request.getParameter("stateId"));
		State state = stateService.fetchState(stateId);

		return state;
	}

	/**
	 * check state name in selected country duplication
	 * if state id  zero then save else update
	 * @param request
	 * @return success/failed/already exist
	 */
	@Transactional 	@RequestMapping("/saveState")
	public @ResponseBody String saveStateForWeb(HttpServletRequest request) {
		System.out.println("in Save State");
		this.country.setCountryId(Long.parseLong(request.getParameter("countryId")));
		this.state.setCountry(this.country);
		this.state.setStateId(Long.parseLong(request.getParameter("stateId")));
		this.state.setCode(request.getParameter("code"));
		this.state.setName(request.getParameter("name"));

		boolean flag = false;
		List<State> stateList = stateService.fetchStateByCountryIdforwebapp(this.state.getCountry().getCountryId());
		state.setName((Character.toString(state.getName().charAt(0)).toUpperCase() + (state.getName().substring(1)).toLowerCase()));

		if (state.getStateId() == 0) {
			if (stateList != null) {
				for (State stateFromDb : stateList) {
					if (stateFromDb.getName().trim().toUpperCase().equals(state.getName().trim().toUpperCase())
							|| stateFromDb.getCode().trim().equals(state.getCode())) {
						flag = true;
						break;
					}
				}
			}

			if (!flag) {
				/* country.setDisable(false); */
				stateService.saveForWebApp(state);
				return Constants.SUCCESS_RESPONSE;
			}
		}

		if (state.getStateId() != 0) {

			flag = false;
			if (stateList != null) {
				for (State stateFromDb : stateList) {
					if ((stateFromDb.getName().trim().toUpperCase().equals(state.getName().trim().toUpperCase())
							|| stateFromDb.getCode().trim().equals(state.getCode()))
							&& stateFromDb.getStateId() != state.getStateId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				return updateStateForWeb(state);
			}
		}

		return Constants.ALREADY_EXIST;
	}

	@Transactional 	@RequestMapping("/updateState")
	public @ResponseBody String updateStateForWeb(@ModelAttribute State state) {

		System.out.println("in Update State");

		if (state.getStateId() == 0 || state.getName() == null || state.getName().equals("")) {
			System.out.println("can not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		stateService.updateForWebApp(state);

		return Constants.SUCCESS_RESPONSE;
	}

	@Transactional 	@RequestMapping("/fetchCityList")
	public @ResponseBody List<CityModel> fetchCityList(Model model) {
		System.out.println("in fetchCityList controller");

		List<City> cityList = cityService.fetchAllCityForWebApp();
		List<CityModel> cityModelList=new ArrayList<>();
		if(cityList!=null){
			for(City city : cityList){
				cityModelList.add(new CityModel(
									city.getCityId(), 
									city.getName(), 
										new StateModel(
												city.getState().getStateId(), 
												city.getState().getCode(), 
												city.getState().getName(), 
												city.getState().getCountry())));
			}
		}
		
		return cityModelList;
	}
	//not used
	@Transactional 	@RequestMapping("/fetchCity")
	public @ResponseBody City fetchCity(HttpServletRequest request) {
		System.out.println("in fetchState controller");

		long cityId = Long.parseLong(request.getParameter("cityId"));
		City city = cityService.fetchCityForWebApp(cityId);

		return city;
	}

	// not used
	@Transactional 	@RequestMapping("/fetchCityListByStateId")
	public @ResponseBody List<CityModel> fetchCityListByStateId(HttpServletRequest request) {
		System.out.println("in fetchCityListByStateId controller");

		long stateId = Long.parseLong(request.getParameter("stateId"));
		List<City> cityList = cityService.fetchCityByStateIdForWebApp(stateId);

		List<CityModel> cityModelList=new ArrayList<>();
		if(cityList!=null){
			for(City city : cityList){
				cityModelList.add(new CityModel(
									city.getCityId(), 
									city.getName(), 
										new StateModel(
												city.getState().getStateId(), 
												city.getState().getCode(), 
												city.getState().getName(), 
												city.getState().getCountry())));
			}
		}
		
		return cityModelList;
	}

	/**
	 * check city name in selected state duplication
	 * if city id zero then save else update
	 * @param request
	 * @return success/failed/already exist
	 */
	@Transactional 	@RequestMapping("/saveCity")
	public @ResponseBody String saveCityForWeb(HttpServletRequest request) {
		System.out.println("in Save City");
		this.state.setStateId(Long.parseLong(request.getParameter("stateId")));
		this.city.setState(this.state);
		this.city.setName(request.getParameter("name"));
		this.city.setCityId(Long.parseLong(request.getParameter("cityId")));

		boolean flag = false;
		List<City> cityList = cityService.fetchCityByStateIdForWebApp(this.state.getStateId());
		city.setName((Character.toString(city.getName().charAt(0)).toUpperCase() + (city.getName().substring(1)).toLowerCase()));

		if (city.getCityId() == 0) {
			if (cityList != null) {
				for (City cityFromDb : cityList) {
					if (cityFromDb.getName().trim().toUpperCase().equals(city.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				cityService.saveForWebApp(city);
				return Constants.SUCCESS_RESPONSE;
			}
		}
		if (city.getCityId() != 0) {

			if (cityList != null) {
				for (City cityFromDb : cityList) {
					if (cityFromDb.getName().trim().toUpperCase().equals(city.getName().trim().toUpperCase())
							&& cityFromDb.getCityId() != city.getCityId()) {
						flag = true;
						break;
					}
				}
			}

			if (!flag) {
				System.out.println("Moving request for Update");
				return updateCityForWeb(city);
			}
		}

		return Constants.ALREADY_EXIST;

	}

	@Transactional 	@RequestMapping("/updateCity")
	public @ResponseBody String updateCityForWeb(@ModelAttribute City city) {

		System.out.println("in Update City");

		if (city.getCityId() == 0 || city.getName() == null || city.getName().equals("")) {
			System.out.println("con not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		cityService.updateForWebApp(city);

		return Constants.SUCCESS_RESPONSE;
	}

	// fetchStateListByCountryId
	@Transactional 	@RequestMapping("/fetchStateListByCountryId")
	public @ResponseBody List<StateModel> fetchStateListByCountryId(HttpServletRequest request) {

		System.out.println("in fetchStateListByCountryId");

		long countryId = Long.parseLong(request.getParameter("countryId"));
		List<State> statelist = stateService.fetchStateByCountryIdforwebapp(countryId);
		
		List<StateModel> stateModelList=new ArrayList<>();
		if(statelist!=null){
			for(State state: statelist){
				stateModelList.add(new StateModel(state.getStateId(), state.getCode(), state.getName()));
			}
		}
		
		
		return stateModelList;
	}
	
	@Transactional 	@RequestMapping("/location_company")
	public ModelAndView location_company(Model model, HttpSession session) {

		model.addAttribute("pageName", "Location Management");

		return new ModelAndView("location_company");
	}

	@Transactional 	@RequestMapping("/fetchCityListByCompanyId")
	public @ResponseBody List<CityModel> fetchCityListByCompanyId(HttpServletRequest request){
		
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		List<CityModel> cityList=new ArrayList<>();
		for(CompanyCities companyCity : companyCities){
			cityList.add(new CityModel(companyCity.getCity().getCityId(), companyCity.getCity().getName()));
		}
		return cityList;
	}
	
	/**
	 * check region name in selected city duplication
	 * if region id zero then save else update
	 * @param request
	 * @return success/failed/already exist
	 */
	@Transactional 	@RequestMapping("/saveRegion")
	public @ResponseBody String saveRegionForWeb(HttpServletRequest request) {
		System.out.println("in Save Region");
		this.city.setCityId(Long.parseLong(request.getParameter("cityId")));
		this.region.setRegionId(Long.parseLong(request.getParameter("regionId")));
		this.region.setCity(this.city);
		this.region.setName(request.getParameter("name"));

		boolean flag = false;
		List<Region> regionList = regionService.fetchSpecifcRegionsForWebApp(city.getCityId());
		region.setName((Character.toString(region.getName().charAt(0)).toUpperCase() + (region.getName().substring(1)).toLowerCase()));

		if (region.getRegionId() == 0) {
			if (regionList != null) {
				for (Region regionFromDb : regionList) {
					if (regionFromDb.getName().trim().toUpperCase().equals(region.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {

				regionService.saveForWebApp(region);
				return Constants.SUCCESS_RESPONSE;
			}
		}
		if (region.getRegionId() != 0) {
			if (regionList != null) {
				for (Region regionFromDb : regionList) {
					if (regionFromDb.getName().trim().toUpperCase().equals(region.getName().trim().toUpperCase())
							&& regionFromDb.getRegionId() != region.getRegionId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				return updateRegionForWeb(region);
			}
		}

		return Constants.ALREADY_EXIST;

	}

	@Transactional 	@RequestMapping("/updateRegion")
	public @ResponseBody String updateRegionForWeb(@ModelAttribute Region region) {

		System.out.println("in Update Region");

		if (region.getRegionId() == 0 || region.getName() == null || region.getName().equals("")) {
			System.out.println("con not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}
		this.region=regionService.fetchRegionForWebApp(region.getRegionId());
		region.setCompany(this.region.getCompany());
		regionService.updateForWebApp(region);

		return Constants.SUCCESS_RESPONSE;
	}

	@Transactional 	@RequestMapping("/fetchRegionListByCityId")
	public @ResponseBody List<RegionModel> fetchRegionListByCityId(HttpServletRequest request) {

		System.out.println("in fetchStateListByCountryId");

		long cityId = Long.parseLong(request.getParameter("cityId"));
		List<Region> regionlist = regionService.fetchSpecifcRegionsForWebApp(cityId);

		List<RegionModel> regionModelList=new ArrayList<>();
		if(regionlist!=null){
			for(Region region: regionlist){
				regionModelList.add(new RegionModel(region.getRegionId(), region.getName()));
			}
		}
		return regionModelList;
	}

	@Transactional 	@RequestMapping("/fetchRegion")
	public @ResponseBody RegionModel fetchRegion(HttpServletRequest request) {
		System.out.println("in fetchRegion controller");

		long regionId = Long.parseLong(request.getParameter("regionId"));
		Region region = regionService.fetchRegionForWebApp(regionId);

		return new RegionModel(region.getRegionId(), region.getName(), new CityModel(region.getCity().getCityId(), region.getCity().getName()));
	}

	@Transactional 	@RequestMapping("/fetchRegionList")
	public @ResponseBody List<Region> fetchRegionList(Model model) {
		System.out.println("in fetchRegionList controller");

		List<Region> regionList = regionService.fetchAllRegionForWebApp();
		List<RegionModel> regionModelList=new ArrayList<>();
		if(regionList!=null){
			for(Region region : regionList){
				regionModelList.add(new RegionModel(
						region.getRegionId(), 
						region.getName(), 
						new CityModel(
								region.getCity().getCityId(), 
								region.getCity().getName(), 
								new StateModel(
										region.getCity().getState().getStateId(), 
										region.getCity().getState().getCode(), 
										region.getCity().getState().getName(), 
										region.getCity().getState().getCountry()))));
			}
		}
		return regionList;
	}

	@Transactional 	@RequestMapping("/fetchAreaList")
	public @ResponseBody List<AreaModel> fetchAreaList(Model model) {
		System.out.println("in fetchRegionList controller");

		List<Area> areaList = areaService.fetchAllAreaForWebApp();

		List<AreaModel> areaModelList=new ArrayList<>();
		if(areaList!=null){
			for(Area area: areaList){
				areaModelList.add(new AreaModel(
						area.getAreaId(), 
						area.getName(), 
						area.getPincode(), 
						new RegionModel(
								area.getRegion().getRegionId(), 
								area.getRegion().getName(), 
								new CityModel(
										area.getRegion().getCity().getCityId(), 
										area.getRegion().getCity().getName(), 
										new StateModel(
												area.getRegion().getCity().getState().getStateId(), 
												area.getRegion().getCity().getState().getCode(), 
												area.getRegion().getCity().getState().getName(), 
												area.getRegion().getCity().getState().getCountry())))));
			}
		}
		return areaModelList;
	}

	@Transactional 	@RequestMapping("/fetchArea")
	public @ResponseBody AreaModel fetchAreaListByAreaId(HttpServletRequest request) {

		System.out.println("in fetchAreaListByAreaId");

		long areaId = Long.parseLong(request.getParameter("areaId"));
		area = areaService.fetchAreaForWebApp(areaId);

		return new AreaModel(
				area.getAreaId(), 
				area.getName(), 
				area.getPincode(), 
				new RegionModel(
						area.getRegion().getRegionId(), 
						area.getRegion().getName(), 
						new CityModel(
								area.getRegion().getCity().getCityId(), 
								area.getRegion().getCity().getName(), 
								new StateModel(
										area.getRegion().getCity().getState().getStateId(), 
										area.getRegion().getCity().getState().getCode(), 
										area.getRegion().getCity().getState().getName(), 
										area.getRegion().getCity().getState().getCountry()))));
	}
	/**
	 * check area name in selected region duplication
	 * if area id zero then save else update
	 * @param request
	 * @return success/failed/already exist
	 */
	@Transactional 	@RequestMapping("/saveArea")
	public @ResponseBody String saveAreaForWeb(HttpServletRequest request) {
		System.out.println("in Save Area");
		this.region.setRegionId(Long.parseLong(request.getParameter("regionId")));
		this.area.setRegion(this.region);
		this.area.setAreaId(Long.parseLong(request.getParameter("areaId")));
		this.area.setName(request.getParameter("name"));
		this.area.setPincode(Long.parseLong(request.getParameter("pincode")));
		
		if(areaService.checkAreaDuplication(this.area.getPincode(),this.area.getAreaId())){
			return "Entered Picode Already Used";
		}
		
		boolean flag = false;
		List<Area> areaList = areaService.fetchAreaListForWebApp(this.region.getRegionId());
		area.setName((Character.toString(area.getName().charAt(0)).toUpperCase() + (area.getName().substring(1)).toLowerCase()));
		if (area.getAreaId() == 0) {
			
			if (areaList != null) {
				for (Area areaFromDb : areaList) {
					if (areaFromDb.getName().trim().toUpperCase().equals(area.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				areaService.saveForWebApp(area);
				return Constants.SUCCESS_RESPONSE;
			}
		}
		if (area.getAreaId() != 0) {
			if (areaList != null) {
				for (Area areaFromDb : areaList) {
					if ((areaFromDb.getName().trim().toUpperCase().equals(area.getName().trim().toUpperCase()))
							&& areaFromDb.getAreaId() != area.getAreaId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				System.out.println("Moving request for Update");
				return updateAreaForWeb(area);
			}
		}

		return Constants.ALREADY_EXIST;

	}

	@Transactional 	@RequestMapping("/updateArea")
	public @ResponseBody String updateAreaForWeb(@ModelAttribute Area area) {

		System.out.println("in Update Region");

		if (area.getAreaId() == 0 || area.getName() == null || area.getName().equals("")) {
			System.out.println("con not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}
		this.area=areaService.fetchAreaForWebApp(area.getAreaId());
		area.setCompany(this.area.getCompany());
		areaService.updateForWebApp(area);

		return Constants.SUCCESS_RESPONSE;
	}

	@Transactional 	@RequestMapping("/fetchAreaListByRegionId")
	public @ResponseBody List<AreaModel> fetchAreaListByRegionId(HttpServletRequest request) {

		System.out.println("in fetchAreaListByAreaId");

		long regionId = Long.parseLong(request.getParameter("regionId"));
		List<Area> areaList = areaService.fetchAreaListForWebApp(regionId);

		List<AreaModel> areaModelList=new ArrayList<>();
		if(areaList!=null){
			for(Area area: areaList){
				areaModelList.add(new AreaModel(
						area.getAreaId(), 
						area.getName(), 
						area.getPincode(), 
						new RegionModel(
								area.getRegion().getRegionId(), 
								area.getRegion().getName(), 
								new CityModel(
										area.getRegion().getCity().getCityId(), 
										area.getRegion().getCity().getName(), 
										new StateModel(
												area.getRegion().getCity().getState().getStateId(), 
												area.getRegion().getCity().getState().getCode(), 
												area.getRegion().getCity().getState().getName(), 
												area.getRegion().getCity().getState().getCountry())))));
			}
		}
		
		return areaModelList;
	}

	/*// BranchConfig list for select branch	
	@Transactional 	@RequestMapping("/branch_setting")
	public ModelAndView branchSetting(Model model,HttpSession session) {
		System.out.println("in branch_setting");

		model.addAttribute("pageName", "Branch Setting");
		
		List<Country> countryList = countryService.fetchCountryListForBranchConfig();		
		
		model.addAttribute("countryList", countryList);
		
		return new ModelAndView("setBranch");
	}
	
	@Transactional 	@RequestMapping("/branch_setting_initial")
	public ModelAndView branchSettingIntial(Model model,HttpSession session) {
		System.out.println("in branch_setting");

		model.addAttribute("pageName", "Branch Setting");
		
		List<Country> countryList = countryService.fetchCountryListForBranchConfig();		
		
		model.addAttribute("countryList", countryList);
		
		return new ModelAndView("setBranchInitial");
	}
	
	@Transactional 	@RequestMapping("/fetchCountryListBranchConfig")
	public @ResponseBody List<Country> fetchCountryListBranchConfig(Model model) {
		System.out.println("in fetchCountryListBranchConfig");

		List<Country> countryList = countryService.fetchCountryListForWebApp();

		return countryList;
	}

	@Transactional 	@RequestMapping("/fetchStateListByCountryIdBranchConfig")
	public @ResponseBody List<State> fetchStateListByCountryIdBranchConfig(HttpServletRequest request) {

		System.out.println("in fetchStateListByCountryIdBranchConfig");

		long countryId = Long.parseLong(request.getParameter("countryId"));
		List<State> statelist = stateService.fetchStateByCountryIdForBranchConfig(countryId);

		return statelist;
	}

	@Transactional 	@RequestMapping("/fetchCityListByStateIdBranchConfig")
	public @ResponseBody List<City> fetchCityListByStateIdBranchConfig(HttpServletRequest request) {
		System.out.println("in fetchCityListByStateIdBranchConfig");

		long stateId = Long.parseLong(request.getParameter("stateId"));
		List<City> cityList = cityService.fetchCityByStateIdForWebAppBranchConfig(stateId);

		return cityList;
	}

	@Transactional 	@RequestMapping("/fetchRegionListByCityIdBranchConfig")
	public @ResponseBody List<Region> fetchRegionListByCityIdBranchConfig(HttpServletRequest request) {

		System.out.println("in fetchRegionListByCityIdBranchConfig");

		long cityId = Long.parseLong(request.getParameter("cityId"));
		List<Region> regionlist = regionService.fetchSpecifcRegionsBranchConfig(cityId);

		return regionlist;
	}

	@Transactional 	@RequestMapping("/fetchAreaListByRegionIdBranchConfig")
	public @ResponseBody List<Area> fetchAreaListByRegionIdBranchConfig(HttpServletRequest request) {

		System.out.println("in fetchAreaListByRegionIdBranchConfig");

		long regionId = Long.parseLong(request.getParameter("regionId"));
		List<Area> areaList = areaService.fetchAreaListForBranchConfig(regionId);

		return areaList;
	}*/
}
