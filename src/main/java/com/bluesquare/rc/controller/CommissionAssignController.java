package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.CommissionAssign;
import com.bluesquare.rc.entities.CommissionAssignProducts;
import com.bluesquare.rc.entities.CommissionAssignTargetSlabs;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.CommissionAssignRequest;
import com.bluesquare.rc.models.EmployeeCommissionModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.CommissionAssignModel;
import com.bluesquare.rc.service.CommissionAssignService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.utils.Constants;
/**
 * @author Sachin Pawar Code And Documentation
 * API End Points : 
 * 1.commissionAssign
 * 2.save_commission_assign
 * 3.update_commission_assign
 * 4.disable_commission_assign/{commissionAssignId}
 * 5.fetch_commission_assign
 * 6.fetch_commission_assign_slab/{commissionAssignId}
 */
@Controller
public class CommissionAssignController {

	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	CommissionAssignService commissionAssignService; 
	
	@Autowired
	ProductDAO productDAO;
	
	@Transactional 	@RequestMapping("/commissionAssign")
	public ModelAndView commissionAssing(Model model,HttpSession session,HttpServletRequest request) {
		
		model.addAttribute("pageName", "Assign Commission");
		
		long commissionAssignId=Long.parseLong(request.getParameter("commissionAssignId"));
		model.addAttribute("commissionAssignId", commissionAssignId);
		
		if(commissionAssignId!=0){
			model.addAttribute("mode", "edit");
		}else{
			model.addAttribute("mode", "add");
		}
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						"",
						null,
						null,
						0f,
						/*"",*/ //product content type
						"",
						0l,
						0l,
						0l,
						0l, 
						null,
						null,
						product.getProductBarcode(),
						null));
			}
		}
		model.addAttribute("productList", productModelList);
		
		
		
		return new ModelAndView("CommissionAssignAddUpdate");
	}
	
	@Transactional 	@RequestMapping("/fetchCommissionAssign")
	public ModelAndView fetchCommissionAssignPage(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Commission Slab");
		model.addAttribute("mode", "show");
		return new ModelAndView("CommissionAssign");
	}
	
	@Transactional 	@RequestMapping("/save_commission_assign")
	public ResponseEntity<BaseDomain> saveCommissionAssign(@RequestBody CommissionAssignRequest commissionAssignSaveRequest, Model model,HttpSession session) {
		
		BaseDomain baseDomain=new BaseDomain();
		
		commissionAssignService.saveCommissionAssignment(commissionAssignSaveRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/update_commission_assign")
	public ResponseEntity<BaseDomain> updateCommissionAssign(@RequestBody CommissionAssignRequest commissionAssignSaveRequest, Model model,HttpSession session) {
		
		BaseDomain baseDomain=new BaseDomain();
		
		commissionAssignService.updateCommissionAssignment(commissionAssignSaveRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/disable_commission_assign/{commissionAssignId}")
	public ResponseEntity<BaseDomain> disableCommissionAssign(@ModelAttribute("commissionAssignId") long commissionAssignId, Model model,HttpSession session) {
		
		BaseDomain baseDomain=new BaseDomain();
		
		commissionAssignService.disableCommissionAssignment(commissionAssignId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/fetch_commission_assign")
	public ResponseEntity<CommissionAssignModel> fetchCommissionAssign( Model model,HttpSession session) {
		
		CommissionAssignModel commissionAssignModel= commissionAssignService.fetchCommissionAssign();
		if(commissionAssignModel.getCommissionAssignList()==null){
			commissionAssignModel.setStatus(Constants.FAILURE_RESPONSE);
			commissionAssignModel.setErrorMsg("Commission Assign List Not Found");
		}else{
			commissionAssignModel.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
		return new ResponseEntity<CommissionAssignModel>(commissionAssignModel,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/fetch_commission_assign_slab/{commissionAssignId}")
	public ResponseEntity<CommissionAssignRequest> fetchCommissionAssignSlab(@ModelAttribute("commissionAssignId")long commissionAssignId, Model model,HttpSession session) {
		
		CommissionAssignRequest commissionAssignRequest=new CommissionAssignRequest();
		CommissionAssign commissionAssign=commissionAssignService.fetchCommissionAssign(commissionAssignId);
		List<CommissionAssignTargetSlabs> commissionAssignTargetSlabsList=commissionAssignService.fetchCommissionAssignTargetSlabs(commissionAssignId);
		List<CommissionAssignProducts> commissionAssignProductsList=commissionAssignService.fetchCommissionAssignProducts(commissionAssignId);
		
		if(commissionAssignTargetSlabsList==null){
			commissionAssignRequest.setStatus(Constants.FAILURE_RESPONSE);
			commissionAssignRequest.setErrorMsg("Commission Assign Slab List Not Found");
		}else{
			commissionAssignRequest.setStatus(Constants.SUCCESS_RESPONSE);
			commissionAssignRequest.setCommissionAssignTargetSlabsList(commissionAssignTargetSlabsList);
			commissionAssignRequest.setCommissionAssignProductsList(commissionAssignProductsList);
			commissionAssignRequest.setCommissionAssign(commissionAssign);
		}
		
		return new ResponseEntity<CommissionAssignRequest>(commissionAssignRequest,HttpStatus.OK);
	}
	
	@Transactional 	@RequestMapping("/check_commission_assign_name_exist")
	public ResponseEntity<BaseDomain> checkCommissionAssignName(HttpServletRequest request, Model model,HttpSession session) {
		BaseDomain baseDomain=new BaseDomain();
		long commissionAssignId=Long.parseLong(request.getParameter("commissionAssignId"));
		String commissionAssignName =request.getParameter("commissionAssignName");
		boolean checkName=commissionAssignService.checkCommissionAssignNameAlreadyUsedOrNot(commissionAssignName, commissionAssignId);
	
		if(checkName){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Commission Assign List Not Found");
		}else{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		}
		
		return new ResponseEntity<BaseDomain>(baseDomain,HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping("/open_product_commission_assign")
	public ModelAndView openProductCommissionAssign(Model model,HttpServletRequest request){
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		List<ProductModel> productModelList=new ArrayList<>();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						null, 
						null, 
						null, 
						product.getRate(), 
						null, 
						/*null,*/ //product content type 
						0, 
						0, 
						0, 
						0, 
						null, 
						null,
						product.getProductBarcode(),
						null));
			}
		}
		model.addAttribute("productList", productModelList);
		
		return new ModelAndView("comissionProductWise");
	}
	/**
	 * <pre>
	 * employee commission details
	 * </pre>
	 * @param monthId
	 * @param yearId
	 * @return
	 */
	@Transactional 
	@RequestMapping("/employeeCommission")
	public ModelAndView fetchEmployeeCommissionResponse(HttpServletRequest request ,Model model){
		//EmployeeCommissionResponse employeeCommissionResponse=new EmployeeCommissionResponse();
		long monthId=Long.parseLong(request.getParameter("monthId"));
		long yearId=Long.parseLong(request.getParameter("yearId"));
		List<EmployeeCommissionModel> employeeCommissionModelsList=commissionAssignService.fetchEmployeeCommissionModel(monthId, yearId);
		
		/*if(employeeCommissionModelsList==null){
			employeeCommissionResponse.setStatus(Constants.SUCCESS_RESPONSE);
			employeeCommissionResponse.setErrorMsg("Date Not Found");
		}else{
			employeeCommissionResponse.setStatus(Constants.FAILURE_RESPONSE);
			employeeCommissionResponse.setEmployeeCommissionModelsList(employeeCommissionModelsList);
		}*/
		
		model.addAttribute("employeeCommissionModelsList", employeeCommissionModelsList);
		
		return new ModelAndView("EmployeeCommission");
	}
	
}
