package com.bluesquare.rc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.models.ProfitAndLossEntity;
import com.bluesquare.rc.models.ProfitAndLossRequest;
import com.bluesquare.rc.models.ProfitAndLossResponse;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.ExpenseService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.OrderDetailsService;
/**
 * <pre>
 * @author Sachin Pawar 22-05-2018 Code Documentation
 * API End Points
 * 1.profit-loss
 * 2.profit-loss-info
 * <pre>
 */
@Controller
public class ProfitAndLossController {
	
	@Autowired
	OrderDetailsService orderDetailsService;

	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	ExpenseService expenseService;
	
	//New Code For Profit And Loss Using Expense
		@RequestMapping("profit-loss")
		public ModelAndView profitLoss(HttpServletRequest request, HttpSession session) {
			ModelAndView mv = new ModelAndView("profit_and_loss");
			return mv;
		}
	
		@RequestMapping("profit-loss-info")
		public ResponseEntity<ProfitAndLossResponse> profitLossInfo(@RequestBody ProfitAndLossRequest profitAndLossRequest,
				HttpServletRequest request,HttpSession session){
			ProfitAndLossResponse profitAndLossResponse=new ProfitAndLossResponse();
			String startDate= profitAndLossRequest.getFromDate();
			String endDate=profitAndLossRequest.getEndDate();
			
			double amount=0,totalSales=0,otherIncome=0;
			
			//Calculation For Income Part of profit and loss
			//total sales get from OrderDetails and CounterOrder table
			totalSales=orderDetailsService.totalSaleAmountForProfitAndLoss(startDate, endDate);
			totalSales+=counterOrderService.totalSaleAmountForProfitAndLoss(startDate, endDate);
			
			List<ProfitAndLossEntity> profitAndLossEntities=expenseService.expenseListForProfitAndLoss(startDate, endDate);
			
			//get sum of supplier payment from Inventory Supplier Payment Table
			profitAndLossEntities.add(new ProfitAndLossEntity("Supplies",inventoryService.totalSupplierPaidAmountForProfitAndLoss(startDate, endDate)));
			
			//get sum of employee salary from employee salary table
			profitAndLossEntities.add(new ProfitAndLossEntity("Salary",employeeDetailsService.totalEmployeeeSalaryAmountForProfitAndLoss(startDate, endDate)));
			
			double profitOrLoss=0,services=0,totalExpenses=0;
			
			//profit
			profitOrLoss+=totalSales+otherIncome;
			
			//loss
			for(ProfitAndLossEntity profitAndLossEntity: profitAndLossEntities){
				profitOrLoss-=profitAndLossEntity.getAmount();
				totalExpenses+=profitAndLossEntity.getAmount();
			}
			
			profitAndLossResponse.setTotalExpenses(totalExpenses);
			profitAndLossResponse.setServices(services);
			profitAndLossResponse.setProfitOrLoss(profitOrLoss);
			profitAndLossResponse.setOtherIncome(otherIncome);
			
			profitAndLossResponse.setTotalSales(totalSales);
			profitAndLossResponse.setTotalIncome(totalSales+otherIncome);			
			profitAndLossResponse.setProfitAndLossEntities(profitAndLossEntities);
			
			return new ResponseEntity<ProfitAndLossResponse>(profitAndLossResponse, HttpStatus.OK);
		}
	
}
