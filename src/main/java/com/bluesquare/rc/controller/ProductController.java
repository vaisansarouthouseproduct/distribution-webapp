package com.bluesquare.rc.controller;

import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.BranchDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.PermanentDamageDetails;
import com.bluesquare.rc.entities.PermanentDamageMonthWise;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.models.PermanentDamageDetailsList;
import com.bluesquare.rc.models.PermanentDamageDetailsListResponse;
import com.bluesquare.rc.models.PermanentDamageReport;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.responseEntities.AreaModel;
import com.bluesquare.rc.responseEntities.BrandModel;
import com.bluesquare.rc.responseEntities.CategoriesModel;
import com.bluesquare.rc.responseEntities.DamageDefineModel;
import com.bluesquare.rc.responseEntities.DamageRecoveryDetailsModel;
import com.bluesquare.rc.responseEntities.DamageRecoveryMonthWiseModel;
import com.bluesquare.rc.responseEntities.ProductModel;
import com.bluesquare.rc.responseEntities.SupplierModel;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;
/**
 * <pre>
 * @author Sachin Pawar 21-05-2018 Code Documentation
 * API End Points 
 * 1.fetchProductList
 * 2.fetchProduct
 * 3.addProduct
 * 4.saveProduct
 * 5.updateProduct
 * 6.downloadProductImage/productId/companyId
 * 7.downloadProductImage/productId
 * 8.fetchProductByProductId
 * 9.fetchProductListForReport
 * 10.fetchProductListAjax
 * 11.damageRecoveryList
 * 12.fetchDamageRecoveryByDamageRecoveryId
 * 13.fetchDamageRecoveryDetailsListByDamageRecoveryId
 * 14.saveDamageRecoveryDetails
 * 15.updateDamageRecoveryDetails
 * 16.damageReport
 * 17.permanentDamageReport
 * 18.permanentDamageDetailsList
 * </pre>
 */
@Controller
public class ProductController {

	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	/**
	 * <pre>
	 * fetch product list
	 * </pre>
	 * @param model
	 * @param session
	 * @return ManageProduct
	 */
	@Transactional 	@RequestMapping("/fetchProductList")
	public ModelAndView fetchProductList(Model model,HttpSession session) {

		System.out.println("in fechProductList controller");
		model.addAttribute("pageName", "Manage Product");
				
		  
		List<ProductViewList> productList=productService.fetchProductViewListForWebApp();		
		model.addAttribute("productlist", productList);		
		
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));		
		session.setAttribute("saveMsg", null);
		return new ModelAndView("ManageProduct");
	}
	
	@Transactional 	@RequestMapping("/fetchProduct")
	public ModelAndView fechProduct(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in fechProductList controller");
		
		model.addAttribute("pageName", "Update Product");
		
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		
		model.addAttribute("product", new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												product.getCategories().getCategoryDescription(), 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												product.getCategories().getCategoryDate(), 
												product.getCategories().getCategoryUpdateDate()), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												product.getBrand().getBrandAddedDatetime(), 
												product.getBrand().getBrandUpdateDatetime()), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										product.getProductDescription(), 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										product.getFreeQuantity(), 
										product.getProductAddedDatetime(), 
										product.getProductQuantityUpdatedDatetime(),
										product.getProductBarcode(),
										null));	
		
		/*model.addAttribute("isProductImg", (product.getProductImage()==null)?false:true);*/
		
	    List<Area> areaList=areaService.fetchAllAreaForWebApp();
	    List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		List<CategoriesModel> categoryList=categoriesService.fetchCategoriesModelList();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		return new ModelAndView("updateProducts");
	}
	
	@Transactional 	@RequestMapping("/addProduct")
	public ModelAndView addProduct(Model model,HttpSession session) {

		System.out.println("in addProduct controller");
		
		model.addAttribute("pageName", "Add Product");
		
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
	    List<AreaModel> areaModelList=new ArrayList<>();
	    if(areaList!=null){
	    	for(Area area : areaList){
	    		areaModelList.add(new AreaModel(area.getAreaId(), area.getName(), area.getPincode()));
	    	}
	    }
		model.addAttribute("areaList", areaModelList);
		
		List<BrandModel> brandList=brandService.fetchBrandModelListForWebApp();
		List<CategoriesModel> categoryList=categoriesService.fetchCategoriesModelList();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		
		return new ModelAndView("addProducts");
	}
	
	/**
	 * <pre>
	 * check product name duplication
	 * if product id zero then save otherwise go for update
	 * </pre>
	 * @param file
	 * @param session
	 * @param request
	 * @param model
	 * @return fetchProductList
	 */
	@Transactional 	@RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
	public ModelAndView saveCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session ,HttpServletRequest request,Model model)  {
		System.out.println("in Save Product");		
		
		this.product=new Product();
		this.product.setProductName(request.getParameter("productname"));
		/*this.product.setProductContentType(request.getParameter("imageType"));*/
		this.product.setProductCode(request.getParameter("productcode"));
		//this.product.setProductImage(productImage);
		this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
		this.product.setBrand(brand);
		this.categories.setCategoryId(Long.parseLong(request.getParameter("categoryId")));
		this.product.setCategories(categories);
		
		this.product.setRate(Float.parseFloat(request.getParameter("productRate")));

		this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
		this.product.setProductAddedDatetime(new Date());
		this.product.setCurrentQuantity(0);
		
		this.product.setProductBarcode(request.getParameter("productBarcode"));
		
		this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + (this.product.getProductName().substring(1)).toLowerCase()));
		boolean flag = false;
		List<Product> productList = productService.fetchProductListForWebApp();
		if(productList!=null){
			for (Product productFromDb : productList) {
				if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
			
		if(flag==true)
		{ 
			session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchProductList");		
		}
		this.product.setBranch(branchDAO.fetchBranchByBranchId(tokenHandlerDAO.getSessionSelectedBranchIds()));
		productService.saveProductForWebApp(file, this.product);
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchProductList");
	}
	
	@Transactional 	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	public ModelAndView updateCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session,HttpServletRequest request,Model model) {

			System.out.println("in Update Country");
			this.product=new Product();
			this.product.setProductId(Long.parseLong(request.getParameter("productId")));
			
			if (product.getProductId() == 0) {
				System.out.println("con not update because 0 city not available");
				
				session.setAttribute("saveMsg", "Re-Click to Edit Button");
				return new ModelAndView("redirect:/fetchProductList");
			}
			
			Product prdct=productService.fetchProductForWebApp(Long.parseLong(request.getParameter("productId")));
			this.product.setCompany(prdct.getCompany());
			this.product.setBranch(prdct.getBranch());
			this.product.setProductName(request.getParameter("productname"));
			/*this.product.setProductContentType(request.getParameter("imageType"));*/
			this.product.setProductCode(request.getParameter("productcode"));
			//this.product.setProductImage(productImage);
			this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
			this.product.setBrand(brand);
			this.categories.setCategoryId(Long.parseLong(request.getParameter("categoryId")));
			this.product.setCategories(categories);
			
			this.product.setRate(Float.parseFloat(request.getParameter("productRate")));
			
			this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
			this.product.setProductAddedDatetime(prdct.getProductAddedDatetime());
			this.product.setCurrentQuantity(prdct.getCurrentQuantity());
			this.product.setProductQuantityUpdatedDatetime(new Date());
			this.product.setProductBarcode(request.getParameter("productBarcode"));
			
			this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + (this.product.getProductName().substring(1)).toLowerCase()));
			boolean flag = false;
			List<Product> productList = productService.fetchProductListForWebApp();
			if(productList!=null){
				for (Product productFromDb : productList) {
					if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase()) && prdct.getProductId()!=this.product.getProductId()) {
						flag = true;
						break;
					}
				}
			}
				
			if(flag==true)
			{ 
				session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
				return new ModelAndView("redirect:/fetchProductList");		
			}			
			
			product.setProductName((Character.toString(product.getProductName().charAt(0)).toUpperCase() + product.getProductName().substring(1)));
			
			//As Product Image is hide
			/*if(file.isEmpty())
			{
				this.product.setProductImage(prdct.getProductImage());
			}*/
			
			productService.updateProductForWebApp(file, this.product);

			session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
			return new ModelAndView("redirect:/fetchProductList");

	}
	
	/**
	 * <pre>
	 * download product image by giving productid and company id
	 * it made for app
	 * blob image bytes copy in response which display image in requested area
	 * </pre>
	 * @param productId
	 * @param companyId
	 * @param response
	 * @param session
	 */
	/*@RequestMapping("/downloadProductImage/{productId}/{companyId}/{branchId}")
	public void downloadAdvertImage(@PathVariable("productId") long productId,@PathVariable("companyId") long companyId,@PathVariable("branchId") long branchId, HttpServletResponse response,HttpSession session) {
		  
		try {
			Product product = productService.fetchProductForImage(productId,companyId,branchId);
				
			if(product.getProductImage()==null){
				product=productService.setNoImageToProductForImage(product);
			}
			
			response.setHeader("Content-Disposition", "inline;filename=\"" + product.getProductName() + "\"");
			OutputStream out = response.getOutputStream();
			response.setContentType(product.getProductContentType());
			IOUtils.copy(product.getProductImage().getBinaryStream(), out);
			out.flush();
			out.close();
			
			System.out.println("get image" + product.getProductName()+"."+product.getProductContentType());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		return;
	}*/
	/**
	 * <pre>
	 * download image by product id
	 * blob image bytes copy in response which display image in requested area
	 * </pre>
	 * @param productId
	 * @param response
	 * @param session
	 */
	/*@Transactional 	@RequestMapping("/downloadProductImage/{productId}")
	public void downloadAdvertImage(@PathVariable("productId") long productId, HttpServletResponse response,HttpSession session) {
		  
		try {
			Product product = productService.fetchProductForWebApp(productId);
			
			if(product.getProductImage()==null){
				product=productService.setNoImageToProductForImage(product);
			}
			
			response.setHeader("Content-Disposition", "inline;filename=\"" + product.getProductName() + "\"");
			OutputStream out = response.getOutputStream();
			response.setContentType(product.getProductContentType());
			IOUtils.copy(product.getProductImage().getBinaryStream(), out);
			out.flush();
			out.close();
			
			System.out.println("get image" + product.getProductName()+"."+product.getProductContentType());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		return;
	}*/
	@Transactional 	@RequestMapping("/fetchProductByProductId")
	public @ResponseBody ProductModel fetchProductByProductId(Model model,HttpServletRequest request) {

		System.out.println("in fechProductList controller");
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		return new ProductModel(
				product.getProductId(), 
				product.getProductName(), 
				product.getProductCode(), 
				new CategoriesModel(
						product.getCategories().getCategoryId(), 
						product.getCategories().getCategoryName(), 
						product.getCategories().getCategoryDescription(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst(), 
						product.getCategories().getCategoryDate(), 
						product.getCategories().getCategoryUpdateDate()), 
				new BrandModel(
						product.getBrand().getBrandId(), 
						product.getBrand().getName(), 
						product.getBrand().getBrandAddedDatetime(), 
						product.getBrand().getBrandUpdateDatetime()), 
				product.getRate(), 
				/*product.getProductContentType(),*/ 
				product.getProductDescription(), 
				product.getThreshold(), 
				product.getCurrentQuantity(), 
				product.getDamageQuantity(), 
				product.getFreeQuantity(), 
				product.getProductAddedDatetime(), 
				product.getProductQuantityUpdatedDatetime(),
				product.getProductBarcode(),
				null);
	}
	/**
	 * <pre>
	 * fetch product details by 
	 * quantity sold,quantity damange,quantity free given 
	 * in range,start date,end date
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 */
	@Transactional 	@RequestMapping("/fetchProductListForReport")
	public ModelAndView fetchProductListForReport(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Product Report");
		
		List<ProductReportView> productReportViews=productService.fetchProductListForReport(request.getParameter("range"), request.getParameter("startDate"), request.getParameter("endDate"),request.getParameter("topProductNo"));
		model.addAttribute("productReportViews", productReportViews);
		double totalAmountWithTax=0;
		for(ProductReportView productReportView:productReportViews)
		{
			totalAmountWithTax+=productReportView.getAmountWithTax();
		}
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		return new ModelAndView("ProductReport");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListAjax")
	public @ResponseBody List<ProductModel> fetchProductListAjax(Model model,HttpSession session) {

		List<ProductModel> productModelList=new ArrayList<>();
		List<Product> productList=productService.fetchProductListForWebApp();
		
		for(Product product : productList){
			productModelList.add(new ProductModel(
										product.getProductId(), 
										product.getProductName(), 
										product.getProductCode(), 
										new CategoriesModel(
												product.getCategories().getCategoryId(), 
												product.getCategories().getCategoryName(), 
												product.getCategories().getCategoryDescription(), 
												product.getCategories().getHsnCode(), 
												product.getCategories().getCgst(), 
												product.getCategories().getSgst(), 
												product.getCategories().getIgst(), 
												product.getCategories().getCategoryDate(), 
												product.getCategories().getCategoryUpdateDate()), 
										new BrandModel(
												product.getBrand().getBrandId(), 
												product.getBrand().getName(), 
												product.getBrand().getBrandAddedDatetime(), 
												product.getBrand().getBrandUpdateDatetime()), 
										product.getRate(), 
										/*product.getProductContentType(),*/ 
										product.getProductDescription(), 
										product.getThreshold(), 
										product.getCurrentQuantity(), 
										product.getDamageQuantity(), 
										product.getFreeQuantity(), 
										product.getProductAddedDatetime(), 
										product.getProductQuantityUpdatedDatetime(),
										product.getProductBarcode(),
										null));
		}
		
		return productModelList;
	}
	/**
	 * <pre>
	 * damage recovery main list by range,startDate,endDate
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return damageProductReport
	 */
	@Transactional 	@RequestMapping("/damageRecoveryList")
	public ModelAndView damageRecoveryDayWiseList(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Damage Recovery Report");
		
		
		List<DamageRecoveryMonthWise> damageRecoveryDayWiseList=productService.fetchDamageRecoveryDayWise(request.getParameter("startMonth"), 
																										request.getParameter("startYear"),
																										request.getParameter("endMonth"), 
																										request.getParameter("endYear"));
		List<DamageRecoveryMonthWiseModel> damageRecoveryDayWiseModelList=new ArrayList<>();
		if(damageRecoveryDayWiseList!=null){
			for(DamageRecoveryMonthWise damageRecoveryMonthWise : damageRecoveryDayWiseList){
				Product product=damageRecoveryMonthWise.getProduct();
				damageRecoveryDayWiseModelList.add(new DamageRecoveryMonthWiseModel(
														damageRecoveryMonthWise.getDamageRecoveryId(), 
														new ProductModel(
																product.getProductId(), 
																product.getProductName(), 
																product.getProductCode(), 
																new CategoriesModel(
																		product.getCategories().getCategoryId(), 
																		product.getCategories().getCategoryName(), 
																		product.getCategories().getCategoryDescription(), 
																		product.getCategories().getHsnCode(), 
																		product.getCategories().getCgst(), 
																		product.getCategories().getSgst(), 
																		product.getCategories().getIgst(), 
																		product.getCategories().getCategoryDate(), 
																		product.getCategories().getCategoryUpdateDate()), 
																new BrandModel(
																		product.getBrand().getBrandId(), 
																		product.getBrand().getName(), 
																		product.getBrand().getBrandAddedDatetime(), 
																		product.getBrand().getBrandUpdateDatetime()), 
																product.getRate(), 
																/*product.getProductContentType(),*/ 
																product.getProductDescription(), 
																product.getThreshold(), 
																product.getCurrentQuantity(), 
																product.getDamageQuantity(), 
																product.getFreeQuantity(), 
																product.getProductAddedDatetime(), 
																product.getProductQuantityUpdatedDatetime(),
																product.getProductBarcode(),
																null), 
														damageRecoveryMonthWise.getQuantityDamage(), 
														damageRecoveryMonthWise.getQuantityGiven(), 
														damageRecoveryMonthWise.getQuantityReceived(), 
														damageRecoveryMonthWise.getQuantityNotClaimed(), 
														damageRecoveryMonthWise.getDatetime()));
			}
		}
		
		model.addAttribute("damageRecoveryDayWiseList", damageRecoveryDayWiseList);
		
		List<ProductModel> productModelList=new ArrayList<>();
		List<Product> productList=productService.fetchProductListForWebApp();
		if(productList!=null){
			for(Product product : productList){
				productModelList.add(new ProductModel(
											product.getProductId(), 
											product.getProductName(), 
											product.getProductCode(), 
											new CategoriesModel(
													product.getCategories().getCategoryId(), 
													product.getCategories().getCategoryName(), 
													product.getCategories().getCategoryDescription(), 
													product.getCategories().getHsnCode(), 
													product.getCategories().getCgst(), 
													product.getCategories().getSgst(), 
													product.getCategories().getIgst(), 
													product.getCategories().getCategoryDate(), 
													product.getCategories().getCategoryUpdateDate()), 
											new BrandModel(
													product.getBrand().getBrandId(), 
													product.getBrand().getName(), 
													product.getBrand().getBrandAddedDatetime(), 
													product.getBrand().getBrandUpdateDatetime()), 
											product.getRate(), 
											/*product.getProductContentType(),*/ 
											product.getProductDescription(), 
											product.getThreshold(), 
											product.getCurrentQuantity(), 
											product.getDamageQuantity(), 
											product.getFreeQuantity(), 
											product.getProductAddedDatetime(), 
											product.getProductQuantityUpdatedDatetime(),
											product.getProductBarcode(),
											null));
			}	
		}
			
		model.addAttribute("productList", productModelList);
		
		return new ModelAndView("damageProductReport");
	}
	/**
	 * <pre>
	 * fetch DamageRecovery main detaiks By DamageRecoveryId
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return DamageRecoveryMonthWise
	 */
	@Transactional 	@RequestMapping("/fetchDamageRecoveryByDamageRecoveryId")
	public @ResponseBody DamageRecoveryMonthWiseModel fetchDamageRecoveryByDamageRecoveryId(Model model,HttpServletRequest request,HttpSession session) {
		
		DamageRecoveryMonthWise damageRecoveryMonthWise=productService.fetchDamageRecoveryByDamageRecoveryId(Long.parseLong(request.getParameter("damageRecoveryId")));
		//damageRecoveryDayWise.getProduct().setProductImage(null);
		Product product=damageRecoveryMonthWise.getProduct();
		return new DamageRecoveryMonthWiseModel(
				damageRecoveryMonthWise.getDamageRecoveryId(), 
				new ProductModel(
						product.getProductId(), 
						product.getProductName(), 
						product.getProductCode(), 
						new CategoriesModel(
								product.getCategories().getCategoryId(), 
								product.getCategories().getCategoryName(), 
								product.getCategories().getCategoryDescription(), 
								product.getCategories().getHsnCode(), 
								product.getCategories().getCgst(), 
								product.getCategories().getSgst(), 
								product.getCategories().getIgst(), 
								product.getCategories().getCategoryDate(), 
								product.getCategories().getCategoryUpdateDate()), 
						new BrandModel(
								product.getBrand().getBrandId(), 
								product.getBrand().getName(), 
								product.getBrand().getBrandAddedDatetime(), 
								product.getBrand().getBrandUpdateDatetime()), 
						product.getRate(), 
						/*product.getProductContentType(),*/ 
						product.getProductDescription(), 
						product.getThreshold(), 
						product.getCurrentQuantity(), 
						product.getDamageQuantity(), 
						product.getFreeQuantity(), 
						product.getProductAddedDatetime(), 
						product.getProductQuantityUpdatedDatetime(),
						product.getProductBarcode(),
						null), 
				damageRecoveryMonthWise.getQuantityDamage(), 
				damageRecoveryMonthWise.getQuantityGiven(), 
				damageRecoveryMonthWise.getQuantityReceived(), 
				damageRecoveryMonthWise.getQuantityNotClaimed(), 
				damageRecoveryMonthWise.getDatetime());
	}
	/**
	 * <pre>
	 * fetch damage recovery details by damage recovery main id
	 * </pre> 
	 * @param model
	 * @param request
	 * @param session
	 * @return DamageRecoveryDetails list
	 */
	@Transactional 	@RequestMapping("/fetchDamageRecoveryDetailsListByDamageRecoveryId")
	public @ResponseBody List<DamageRecoveryDetailsModel> fetchDamageRecoveryByProductId(Model model,HttpServletRequest request,HttpSession session) {

		List<DamageRecoveryDetailsModel> damageRecoveryDetailsModelList=new ArrayList<>();
		List<DamageRecoveryDetails> damageRecoveryDetailsList=productService.fetchDamageRecoveryDetailsListByDamageRecoveryId(Long.parseLong(request.getParameter("damageRecoveryId")));
		if(damageRecoveryDetailsList!=null){
			for(DamageRecoveryDetails damageRecoveryDetails : damageRecoveryDetailsList){
				damageRecoveryDetailsModelList.add(new DamageRecoveryDetailsModel(
						damageRecoveryDetails.getDamageRecoveryDetailsPkId(), 
						damageRecoveryDetails.getDamageRecoveryDetailsId(), 
						new SupplierModel(
								damageRecoveryDetails.getSupplier().getSupplierPKId(), 
								damageRecoveryDetails.getSupplier().getSupplierId(), 
								damageRecoveryDetails.getSupplier().getName(), 
								damageRecoveryDetails.getSupplier().getContact()), 
						damageRecoveryDetails.getQuantityGiven(), 
						damageRecoveryDetails.getQuantityReceived(), 
						damageRecoveryDetails.getQuantityNotClaimed(), 
						damageRecoveryDetails.getGivenDate(), 
						damageRecoveryDetails.getReceiveDate(), 
						damageRecoveryDetails.isReceiveStatus(),
						(damageRecoveryDetails.getRejectQuantityReason()==null)?"NA":damageRecoveryDetails.getRejectQuantityReason()));
			}
		}		
		
		return damageRecoveryDetailsModelList;
	}
	/**
	 * <pre>
	 * save Damage recovery details where some qty given to supplier for recovery
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year
	 */
	@Transactional 	@RequestMapping("/saveDamageRecoveryDetails")
	public ModelAndView saveDamageRecoveryDetails(Model model,HttpServletRequest request,HttpSession session) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		DamageRecoveryDetails damageRecoveryDetails=new DamageRecoveryDetails();
		try {
			damageRecoveryDetails.setGivenDate(dateFormat.parse(request.getParameter("givenDate")));
		} catch (ParseException e) {}
		damageRecoveryDetails.setQuantityGiven(Long.parseLong(request.getParameter("quantityGiven")));
		damageRecoveryDetails.setReceiveStatus(false);
		Supplier supplier=new Supplier();
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));		
		damageRecoveryDetails.setSupplier(supplier);
		
		productService.saveDamageRecoveryDetails(damageRecoveryDetails, Long.parseLong(request.getParameter("damageRecoveryId")));
		
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		
		int year=Integer.parseInt(yearFormat.format(new Date()));
		int month=Integer.parseInt(monthFormat.format(new Date()));
		
		return new ModelAndView("redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year);
	}
	/**
	 * <pre>
	 * update damage recovery details when supplier giving recovery of products
	 * if product quantity is recovered then add into current inventory
	 * otherwise its add in not claimed
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year
	 */
	@Transactional 	@RequestMapping("/updateDamageRecoveryDetails")
	public ModelAndView updateDamageRecoveryDetails(Model model,HttpServletRequest request,HttpSession session) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String rejectQuantityReason=request.getParameter("rejectReason");
		
		DamageRecoveryDetails damageRecoveryDetails=productService.fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(request.getParameter("damageRecoveryDetailsId"));
		try {
			damageRecoveryDetails.setReceiveDate(dateFormat.parse(request.getParameter("receivedDate")));
		} catch (ParseException e) {}
		damageRecoveryDetails.setQuantityReceived(Long.parseLong(request.getParameter("quantityReceived")));
		damageRecoveryDetails.setQuantityNotClaimed(damageRecoveryDetails.getQuantityGiven()-damageRecoveryDetails.getQuantityReceived());
		if(damageRecoveryDetails.getQuantityNotClaimed()>0){
			damageRecoveryDetails.setRejectQuantityReason(rejectQuantityReason);
			//permanent damage for not recovered qty
			PermanentDamageDetails permanentDamageDetails=new PermanentDamageDetails();
			permanentDamageDetails.setQuantityDamage(damageRecoveryDetails.getQuantityGiven()-damageRecoveryDetails.getQuantityReceived());
			permanentDamageDetails.setRejectQuantityReason(rejectQuantityReason);
			permanentDamageDetails.setDamageDate(new Date());
			permanentDamageDetails.setDamageFrom("Supplier Rejected");
			productService.savePermanenetDamageDetails(permanentDamageDetails, damageRecoveryDetails.getDamageRecoveryMonthWise().getProduct().getProductId());
		}else{
			damageRecoveryDetails.setRejectQuantityReason("NA");
		}
		damageRecoveryDetails.setReceiveStatus(true);
		
		productService.updateDamageRecoveryDetails(damageRecoveryDetails);
		
		
		
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		
		int year=Integer.parseInt(yearFormat.format(new Date()));
		int month=Integer.parseInt(monthFormat.format(new Date()));
		
		return new ModelAndView("redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year);
	}
	
	/**
	 * <pre>
	 * damage report show which shows how much damage did by range ,startDate,endDate
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return damageReport.jsp
	 */
	@Transactional 	@RequestMapping("/damageReport")
	public ModelAndView damageReport(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Damage Report");
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
		
		List<DamageDefine> damageDefineList=productService.fetchDamageDefineList(range, startDate, endDate);
		List<DamageDefineModel> damageDefineModelList=new ArrayList<>();
		if(damageDefineList!=null){
			for(DamageDefine damageDefine : damageDefineList){
				Product product=damageDefine.getProduct();
				damageDefineModelList.add(new DamageDefineModel(
						damageDefine.getDamageDefineId(), 
						damageDefine.getOrderId(), 
						product.getProductName(), 
						damageDefine.getCollectingPerson(), 
						damageDefine.getDepartmentName(), 
						damageDefine.getQty(), 
						damageDefine.getDate(), 
						damageDefine.getReason()));
			}
		}
		model.addAttribute("damageDefineList", damageDefineModelList);
		
		return new ModelAndView("damageReport");
	}
	
	/**
	 * <pre>
	 * permanent damage report show by year and month Number
	 * </pre>
	 * @param model
	 * @param request
	 * @param session
	 * @return permanentDamageReport.jsp
	 */
	@Transactional 	@RequestMapping("/permanentDamageReport")
	public ModelAndView permanentDamageReport(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Permanent Damage Report");
		long year=Long.parseLong(request.getParameter("year"));
		long monthNumber=Long.parseLong(request.getParameter("monthNumber"));
		
		List<PermanentDamageMonthWise> permanentDamageMonthWiseList=productService.fetchPermanentDamageMonthWiseByMonth(year, monthNumber);
		List<PermanentDamageReport> permanentDamageMainList=new ArrayList<>();
		if(permanentDamageMonthWiseList!=null){
			long srno=1;
			for(PermanentDamageMonthWise permanentDamageMonthWise : permanentDamageMonthWiseList){
				permanentDamageMainList.add(new PermanentDamageReport(
						srno, 
						permanentDamageMonthWise.getPermanentDamageMonthWiseId(),
						permanentDamageMonthWise.getProduct().getProductName(),
						permanentDamageMonthWise.getTotalQuantityDamage(), 
						permanentDamageMonthWise.getDamageDate()));
				srno++;
			}
		}
		model.addAttribute("permanentDamageReport", permanentDamageMainList);
		
		return new ModelAndView("permanentDamageReport");
	}
	
	@Transactional @RequestMapping("/permanentDamageDetailsList")
	public ResponseEntity<PermanentDamageDetailsListResponse> fetchPermanentDamageDetailsListResponse(HttpServletRequest request){
		PermanentDamageDetailsListResponse permanentDamageDetailsListResponse=new PermanentDamageDetailsListResponse();
		List<PermanentDamageDetailsList> permanentDamageDetailsLists=new ArrayList<>();
		
		List<PermanentDamageDetails> permanentDamageDetailsList=productService.fetchPermanentDamageDetailsListByPermanentDamageMonthWiseId(Long.parseLong(request.getParameter("permanentDamageMonthWiseId")));
		if(permanentDamageDetailsList!=null){
			
			long srno=1;
			for(PermanentDamageDetails permanentDamageDetails : permanentDamageDetailsList){
				permanentDamageDetailsLists.add(new PermanentDamageDetailsList(
						srno, 
						permanentDamageDetails.getQuantityDamage(),
						permanentDamageDetails.getRejectQuantityReason(),
						permanentDamageDetails.getDamageFrom(),
						permanentDamageDetails.getDamageDate()));
				srno++;
			}
			
			permanentDamageDetailsListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			permanentDamageDetailsListResponse.setPermanentDamageDetailsList(permanentDamageDetailsLists);
		}else{
			permanentDamageDetailsListResponse.setErrorMsg("Permanent Damage List Not Found");
			permanentDamageDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
		}
		
		return new ResponseEntity<PermanentDamageDetailsListResponse>(permanentDamageDetailsListResponse,HttpStatus.OK);
	}
	
	/**
	 * check Barcode save duplication
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkProductBarcodeDuplicationForSave")
	public @ResponseBody String checkProductBarcodeDuplicationForSave(Model model,HttpServletRequest request,HttpSession session)
	{
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return productService.checkProductBarcodeDuplication(checkText, type,0);
	}
	
	/**
	 * check business update duplication for telephoneNumber,gst_number,mobile number,emailId
	 * @param model
	 * @param request
	 * @param session
	 * @return success/failure
	 */
	@Transactional 	@RequestMapping("/checkProductBarcodeDuplicationForUpdate")
	public @ResponseBody String checkProductBarcodeDuplicationForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		long productId=Long.parseLong(request.getParameter("productId"));
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return productService.checkProductBarcodeDuplication(checkText, type, productId);
	}
	
	/**
	 * <pre>
	 * fetch Product By Barcode
	 * </pre>
	 * @param request
	 * @return Product
	 */
	@Transactional
	@RequestMapping("/fetchProductByBarcode")
	public @ResponseBody Product fetchProductByBarcode(HttpServletRequest request){
		String barcodeNo=request.getParameter("barcode");
		Product product=productService.fetchProductByBarcodeNumber(barcodeNo);
		return product;
		
	}
}
