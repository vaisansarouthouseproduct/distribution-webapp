var routeListCart=[];//routeAddress,lat,lng
var name="",lat="",lng="";
var RoutePointsList=[];
var isProperRoute=false;
function initializeNewInput(){
	var input = document.getElementById("routePoint");
	var autocomplete = new google.maps.places.Autocomplete(input);
	
	google.maps.event.addListener(autocomplete,'place_changed',function() {		
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			name="";
			lat="";
			lng="";
			isProperRoute=false;
			Materialize.Toast.removeAll();
			Materialize.toast("Please select proper route", '2000', 'teal lighten-2');
		}else{
			//for end route
			name = place.name;
			lat = place.geometry.location.lat();
			lng = place.geometry.location.lng();
			isProperRoute=true;
		}
	});
}
google.maps.event.addDomListener(window, 'load', initializeNewInput);

function routePointCartRefresh(){
	RoutePointsList=[];
	$('#routePoint').val('');
	$("#routePointTable").empty();
	for(var i=0; i<routeListCart.length; i++){
		var routePoint=routeListCart[i];
		var editDeleteButton="<a href='#' class='btn btn-flat blue-text' onclick='deleteRoutePoint("+i+")'><i class='material-icons'>delete</i></a>";
		$("#routePointTable").append('<tr>'
									+ '<td>'+(i+1)+'</td>'
									+ '<td>'+routePoint[0]+'</td>'
									+ '<td>'+editDeleteButton+'</td>'
									+'</tr>');
		var RoutePoint = {
			routeAddress : routePoint[0],
			latitude : routePoint[1], 
			longitude :	routePoint[2]	 
		}
		RoutePointsList.push(RoutePoint);
	}
}
function deleteRoutePoint(index){
	routeListCart.splice(index,1);
	routePointCartRefresh();
}

function fetchRouteList(){
	$('#routeTableData').empty();
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_route_list",
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				routeList=response.routeList;
				
				
				
				for(var i=0; i<routeList.length; i++){
					
					var editDeleteButton="<a href='#' class='btn btn-flat blue-text' onclick='editRoute("+routeList[i].routeId+")'><i class='material-icons'>edit</i></a>"+
					"<a href='#' class='btn btn-flat blue-text' onclick='deleteRouteConfirmation("+routeList[i].routeId+")'><i class='material-icons'>delete</i></a>";
					
					$('#routeTableData').append('<tr>'+
													'<td>'+(i+1)+'</td>'+
													'<td>'+routeList[i].routeName+'</td>'+
													'<td>'+routeList[i].routeGenId+'</td>'+
													'<td><a class="btn-flat" onclick="fetchRoutePointList('+routeList[i].routeId+')">View</a></td>'+
													'<td>'+editDeleteButton+'</td>'+
												'</tr>');
				}
				
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
			}					
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function fetchRoutePointList(routeId){
	$('#routePointList').empty();
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_route_points_list/"+routeId,
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				routePointsList=response.routePointsList;
				routePointListHtml='<ul class="center" id="routUl">';
				for(var i=0; i<routePointsList.length; i++){
					if((routePointsList.length-1)!=i){
						routePointListHtml+='<li>'+routePointsList[i].routeAddress+'</li>'+
      												'<div class="vertical-line"></div>';
					}else{
						routePointListHtml+='<li>'+routePointsList[i].routeAddress+'</li></ul>';
					}					
				}
				$('#routePointList').append(routePointListHtml);
				$('#routeModalView').modal('open');
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast(response.errorMsg, '2000', 'teal lighten-2');
			}					
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function editRoute(routeId){
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_route_points_list/"+routeId,
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				routePointsList=response.routePointsList;
				$('#routeName').val(routePointsList[0].route.routeName);
				$('#routeName').change();
				$('#routeId').val(routePointsList[0].route.routeId);
				for(var i=0; i<routePointsList.length; i++){
					routeListCart.push([routePointsList[i].routeAddress,routePointsList[i].longitude,routePointsList[i].latitude]);
				}
				routePointCartRefresh();
				$('#routeAddUpdateForm').show();
				$('#routeDataTable').hide();
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
			}					
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function deleteRoute(routeId){
	
	$.ajax({
		type:'POST',
		url: myContextPath+"/delete_route/"+routeId,
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				Materialize.Toast.removeAll();
				Materialize.toast('Route Deleted SuccessFully', '2000', 'teal lighten-2');
				fetchRouteList();
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
			}			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}
//delete route confirmation before delete any route
function deleteRouteConfirmation(id){
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deleteRoute('+id+')">Delete</button>  '));
	  Materialize.toast($toastContent, "abc");
}
$(document).ready(function() {
	$("#addRoutePoint").click(function() {
		if(isProperRoute==false){
			Materialize.Toast.removeAll();
			Materialize.toast("Please select proper route", '2000', 'teal lighten-2');
			return false;
		}
		$('#routePoint').val('');
		isProperRoute=false;
		routeListCart.push([name,lat,lng]);
		routePointCartRefresh();
	});
	$("#clearRoute").click(function(){
		routeListCart=[];
		$('#routeName').val('');
		$('#routeId').val('0');
		$("#routePointTable").empty();
		routePointCartRefresh();
	});
	$('#saveRoute').click(function(){
		
		if(routeListCart.length<2){
			Materialize.Toast.removeAll();
			Materialize.toast("Add at least 2 Route Points", '2000', 'teal lighten-2');
			return false;
		}
		
		var routeName=$('#routeName').val();
		if(routeName==""){
			Materialize.Toast.removeAll();
			Materialize.toast("Enter Route Name", '2000', 'teal lighten-2');
			return false;
		}
		
		/*RoutePoints : {
			routeAddress : 
			longitude :
			latitude :		
		}*/
		
		RouteSaveRequest={
				route : {
					routeId:$('#routeId').val(),
					routeName : routeName
				},
				routePointsList : RoutePointsList
		}
		var path="save_route";
		if($('#routeId').val()!=0){
			path="update_route";
		}
		$.ajax({
			type:'POST',
			url: myContextPath+"/"+path,
			async: false,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
	        data:JSON.stringify(RouteSaveRequest),
			success : function(response){
				if(response.status=="SuccessFully  Saved"){
					fetchRouteList();
					$("#clearRoute").click();
					Materialize.Toast.removeAll();
					Materialize.toast('Route Created SuccessFully', '2000', 'teal lighten-2');
					$('#routeAddUpdateForm').hide();
					$('#routeDataTable').show();
				}else if(response.status="SuccessFully  Updated"){
					fetchRouteList();
					$("#clearRoute").click();
					Materialize.Toast.removeAll();
					Materialize.toast('Route Updated SuccessFully', '2000', 'teal lighten-2');
					$('#routeAddUpdateForm').hide();
					$('#routeDataTable').show();
				}else{
					Materialize.Toast.removeAll();
					Materialize.toast(response.errorMsg, '2000', 'teal lighten-2');
				}					
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
	});
	
	$('#addNewRoute').click(function(){
		$("#clearRoute").click();
		$('#routeAddUpdateForm').show();
		$('#routeDataTable').hide();
	});
	$('#routeListId').click(function(){
		$("#clearRoute").click();
		$('#routeAddUpdateForm').hide();
		$('#routeDataTable').show();
	});
	$('#routeAddUpdateForm').hide();
	fetchRouteList();
});

