var productList;
var orderProductDetails;
var radioButton="NonFree";
var orderStatus;
$(document).ready(function() 
{
	orderStatus=$('#orderStatusId').val();
	$('#updateId').click(function(){
		//[productId,purchaseQuantity,issuedQuantity,rate,sellingRate,purchaseAmount,issuedAmount,type]
		/*
		 * var orderProduct=
			{
				"purchaseQuantity" :qty,
				"issuedQuantity":'',
				"sellingRate":mrp,
				"purchaseAmount":purchaseAmt,
				"issueAmount":'',
				"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":"NonFree"
			};
		 * */
		var updateOrderProductList="";
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			
			updateOrderProductList =updateOrderProductList + 
									orderProduct.product.product.productId+","+
									orderProduct.purchaseQuantity+","+
									orderProduct.issuedQuantity+","+
									orderProduct.rate+","+
									orderProduct.sellingRate+","+
									orderProduct.purchaseAmount+","+
									orderProduct.issueAmount+","+
									orderProduct.type+"-";
		}
		updateOrderProductList=updateOrderProductList.slice(0,-1);
		$('#updateOrderProductListId').val(updateOrderProductList);
	});
	
	$('#forUpdateTableId').click(function(){
		var orderProductDetailsTemp=[];
		var length=1;
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			orderProductDetailsTemp.push
			({
				"purchaseQuantity" :orderProduct.purchaseQuantity,
				"issuedQuantity":orderProduct.issuedQuantity,
				"rate":orderProduct.product.rate,
				"sellingRate":orderProduct.sellingRate,
				"purchaseAmount":orderProduct.purchaseAmount,
				"issueAmount":orderProduct.issueAmount,
				"orderDetails":orderProduct.orderDetails,
				"product":{"product":orderProduct.product.product},
				"type":orderProduct.type
			});
			
			var product=orderProduct.product.product;
			
			if(orderProduct.type==="Free")
			{
				$('#delPrdFree_'+product.productId).click(function(){
					var id=$(this).attr('id')
					var productId=id.split('_')[1];
					var orderProductDetailsTemp=[];
					
					$('#producctListtb').empty();
					var srno=1;
					for(var j=0; j<orderProductDetails.length; j++)
					{
						var orderProduct=orderProductDetails[j];
						if(orderProduct.type==="NonFree")
						{
							orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
							
							$('#producctListtb').append(
									'<tr>'+
					        		'<td>'+srno+'</td>'+
					        		'<td>'+orderProduct.product.product.productName+'</td>'+
					        		'<td><input style="text-align:center;" type="text" id="qtyNonFree_'+orderProduct.product.product.productId+'" value="'+orderProduct.purchaseQuantity+'"></td>'+
					        		'<td>'+orderProduct.sellingRate+'</td>'+
					        		'<td><span id="purchaseAmtNonFree_'+orderProduct.product.product.productId+'">'+orderProduct.purchaseAmount+'</span></td>'+
					        		'<td><button id="delPrdNonFree_'+orderProduct.product.product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
					        		'</tr>');
							srno++;
						}		
						if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
						{
							orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
							
							$('#producctListtb').append(
									'<tr>'+
					        		'<td>'+srno+'</td>'+
					        		'<td>'+orderProduct.product.product.productName+'<font color="green">-(Free)</font></td>'+
					        		'<td><input style="text-align:center;" type="text" id="qtyFree_'+orderProduct.product.product.productId+'" value="'+orderProduct.purchaseQuantity+'"></td>'+
					        		'<td>'+orderProduct.sellingRate+'</td>'+
					        		'<td><span id="purchaseAmtFree_'+orderProduct.product.product.productId+'">'+orderProduct.purchaseAmount+'</span></td>'+
					        		'<td><button id="delPrdFree_'+orderProduct.product.product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
					        		'</tr>');
							srno++;
						}				
					}
					orderProductDetails=orderProductDetailsTemp;
					
					findTotal();
				});
			}
			else
			{
				$('#delPrdNonFree_'+product.productId).click(function(){
					var id=$(this).attr('id')
					var productId=id.split('_')[1];
					var orderProductDetailsTemp=[];
					
					$('#producctListtb').empty();
					var srno=1;
					for(var j=0; j<orderProductDetails.length; j++)
					{
						var orderProduct=orderProductDetails[j];
						
						if(orderProduct.type==="Free")
						{	
							orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
							
							$('#producctListtb').append(
									'<tr>'+
					        		'<td>'+srno+'</td>'+
					        		'<td>'+orderProduct.product.product.productName+'<font color="green">-(Free)</font></td>'+
					        		'<td><input style="text-align:center;" type="text" id="qtyFree_'+orderProduct.product.product.productId+'" value="'+orderProduct.purchaseQuantity+'"></td>'+
					        		'<td>'+orderProduct.sellingRate+'</td>'+
					        		'<td><span id="purchaseAmtFree_'+orderProduct.product.product.productId+'">'+orderProduct.purchaseAmount+'</span></td>'+
					        		'<td><button id="delPrdFree_'+orderProduct.product.product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
					        		'</tr>');
							srno++;
						}	
						if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
						{							
							orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
							
							$('#producctListtb').append(
									'<tr>'+
					        		'<td>'+srno+'</td>'+
					        		'<td>'+orderProduct.product.product.productName+'</td>'+
					        		'<td><input style="text-align:center;" type="text" id="qtyNonFree_'+orderProduct.product.product.productId+'" value="'+orderProduct.purchaseQuantity+'"></td>'+
					        		'<td>'+orderProduct.sellingRate+'</td>'+
					        		'<td><span id="purchaseAmtNonFree_'+orderProduct.product.product.productId+'">'+orderProduct.purchaseAmount+'</span></td>'+
					        		'<td><button id="delPrdNonFree_'+orderProduct.product.product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
					        		'</tr>');
							srno++;
						}				
					}
					orderProductDetails=orderProductDetailsTemp;
					
					findTotal();
				});
			}
			if(orderProduct.type==="Free")
			{
				$('#qtyFree_'+product.productId).keyup(function(){
					var id=$(this).attr('id')
					var productId=id.split('_')[1];
					var orderProductDetailsTemp=[];
					
					
					var srno=1;
					for(var j=0; j<orderProductDetails.length; j++)
					{
						var orderProduct=orderProductDetails[j];
						if(orderProduct.type==="NonFree")
						{
							orderProductDetailsTemp.push(orderProduct);
						}	
						if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
						{
							orderProductDetailsTemp.push(orderProduct);
						}				
					}
					
					var product;
					for(var i=0; i<productList.length; i++)
					{			
						if(productList[i].productId==productId)
						{
							product=productList[i];
							break;
						}			
					}
					var qty=$(this).val();	
					if(qty=='' || qty==undefined)
					{
						qty=0;
					}
					var mrp=0;
					var purchaseAmt=0;
					var issuedAmt=0;
					
					var orderProduct=
					{
						"purchaseQuantity" :qty,
						"issuedQuantity":qty,
						"rate":product.rate,
						"sellingRate":mrp,
						"purchaseAmount":purchaseAmt.toFixedVSS(2),
						"issueAmount":issuedAmt.toFixedVSS(2),
						"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"Free"
					};
					orderProductDetailsTemp.push(orderProduct);
					
					orderProductDetails=orderProductDetailsTemp;
					$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
					findTotal();
				});
			}
			else
			{
				$('#qtyNonFree_'+product.productId).keyup(function(){
					var id=$(this).attr('id')
					var productId=id.split('_')[1];
					var orderProductDetailsTemp=[];
					
					
					var srno=1;
					for(var j=0; j<orderProductDetails.length; j++)
					{
						var orderProduct=orderProductDetails[j];
						if(orderProduct.type==="Free")
						{
							orderProductDetailsTemp.push(orderProduct);
						}	
						if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
						{
							orderProductDetailsTemp.push(orderProduct);
						}				
					}
					
					var product;
					for(var i=0; i<productList.length; i++)
					{			
						if(productList[i].productId==productId)
						{
							product=productList[i];
							break;
						}			
					}
					var qty=$(this).val();	
					if(qty=='' || qty==undefined)
					{
						qty=0;
					}
					
					var mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
					var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
					var issuedAmt=parseFloat(mrp)*parseFloat(qty);
					
					var orderProduct;
					if(orderStatus==="Booked")
					{
						orderProduct=
						{
							"purchaseQuantity" :qty,
							"issuedQuantity":0,
							"rate":product.rate,
							"sellingRate":mrp,
							"purchaseAmount":purchaseAmt.toFixedVSS(2),
							"issueAmount":0,
							"orderDetails":orderProductDetails[0].orderDetails,
							"product":{"product":product},
							"type":"NonFree"
						};
					}
					else
					{
						orderProduct=
						{
							"purchaseQuantity" :0,
							"issuedQuantity":qty,
							"rate":product.rate,
							"sellingRate":mrp,
							"purchaseAmount":0,
							"issueAmount":issuedAmt.toFixedVSS(2),
							"orderDetails":orderProductDetails[0].orderDetails,
							"product":{"product":product},
							"type":"NonFree"
						};
					}
					orderProductDetailsTemp.push(orderProduct);
					
					orderProductDetails=orderProductDetailsTemp;
					
					$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
					findTotal();
				});
			}
			var qty;
			if(orderStatus==="Booked")
			{
				qty=orderProduct.purchaseQuantity
			}
			else
			{
				qty=orderProduct.issuedQuantity
			}
			
			var mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
			var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
			
			if(orderProduct.type==="NonFree")
			{
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'</td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyNonFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+parseFloat(mrp)+'</td>'+
		        		'<td><span id="purchaseAmtNonFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdNonFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			else
			{
				mrp=0;
				purchaseAmt=0;
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'<font color="green">-(Free)</font></td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+parseFloat(mrp)+'</td>'+
		        		'<td><span id="purchaseAmtFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			length++;
		}
		orderProductDetails=orderProductDetailsTemp;	
	});
	
	$('#brandid').change(function(){
		var brandId=$('#brandid').val();
		var categoryId=$('#categoryid').val();		
		filterProductList(brandId,categoryId);
	});
	$('#categoryid').change(function(){
		var brandId=$('#brandid').val();
		var categoryId=$('#categoryid').val();		
		filterProductList(brandId,categoryId);
	});
	
	//here privious orderProductDetails store in array
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchOrderProductDetailsEditOrder?orderId="+orderId,
		dataType : "json",
		success:function(data)
		{
			orderProductDetails=data;
			$('#forUpdateTableId').click();
		},
		error: function(xhr, status, error) {
			
		}
	});
	
	
	//on ready productList Data fetch
	productListData();
	filterProductList(0,0);
	//fetch All ProductList and store array every 10 second
	window.setInterval(productListData, 10000);
	
	$('#freeId').click(function(){
		radioButton="Free";
	});
	$('#nonFreeId').click(function(){
		radioButton="NonFree";
	});
	
	$('#orderProductAddId').click(function(){
		
		var productId=$('#productid').val();
		
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			if(orderProduct.product.product.productId==productId && orderProduct.type===radioButton)
			{ 
				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Product Adding Warning");
       	     	$('#msg').text("Product Already added");
       	     	return false;
			}
		}
		
		var product;
		for(var i=0; i<productList.length; i++)
		{			
			if(productList[i].productId==productId)
			{
				product=productList[i];
				break;
			}			
		}
		var qty=$('#qty').val();
		if(qty=='' || qty==undefined)
		{
			qty=0;
		}
		var mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
		var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
		var issuedAmt=parseFloat(mrp)*parseFloat(qty);
		
		if(radioButton==="Free")
		{
			mrp=0;
			purchaseAmt=0;
		}
		
		var orderProduct;
		if(orderStatus==="Booked")
		{
			orderProduct=
			{
				"purchaseQuantity" :qty,
				"issuedQuantity":0,
				"rate":product.rate,
				"sellingRate":mrp,
				"purchaseAmount":purchaseAmt.toFixedVSS(2),
				"issueAmount":0,
				"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":"NonFree"
			};
		}
		else
		{
			orderProduct=
			{
				"purchaseQuantity" :0,
				"issuedQuantity":qty,
				"rate":product.rate,
				"sellingRate":mrp,
				"purchaseAmount":0,
				"issueAmount":issuedAmt.toFixedVSS(2),
				"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":"NonFree"
			};
		}
		orderProductDetails.push(orderProduct);
		$('#forUpdateTableId').click();
		findTotal();
	});
});




function findTotal()
{
	var totalQuantity=0;
	var totalAmount=0;
	for(var j=0; j<orderProductDetails.length; j++)
	{
		var orderProduct=orderProductDetails[j];
		if(orderStatus==="Booked")
		{
			totalQuantity+=parseInt(orderProduct.purchaseQuantity);
			totalAmount+=parseInt(orderProduct.purchaseAmount);
		}
		else
		{
			totalQuantity+=parseInt(orderProduct.issuedQuantity);
			totalAmount+=parseInt(orderProduct.issueAmount);
		}
	}
	
	$('#totalQuantityId').text(parseInt(totalQuantity));
	$('#totalAmountId').text(totalAmount.toFixedVSS(2));
}

function productListData()
{
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductListAjax",
		dataType : "json",
		async:false,
		success:function(data)
		{
			productList=data;
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			//console.log("product data list not loaded");
		}
	});

}

function filterProductList(brandId,categoryId)
{
	var select = document.getElementById('productid');
	select.options.length = 0;
	select.options.add(new Option("Choose Product", 0));
	var options, index, option;
	if(brandId!=="0" && categoryId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(categoryId!=="0" && brandId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(brandId!=="0" && categoryId!=="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId && product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			select.options.add(new Option(product.productName, product.productId));
		}
	}
}
