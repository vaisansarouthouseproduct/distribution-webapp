var targetValueType; 
var commissionAssignTargetSlabsList=[];
var commissionAssignTargetSlabsListMap=new Map();
var isUpdating=false;
var pkId=0;
var commissionAssignListOld;
let commissionAssignListDB;
//for store old selected items
var productIdsOld=[];
$(document).ready(function() {
	//for filter tab in commission modal 
		$(".filterTab").hide();
      $('#addNewSlabId').click(function(){
    	  if(validateCommission()){
    		  addNewSlab();
    	  }
      });
     
      $('#departmentId').change(function(){
  		  		
  		var deptId=$('#departmentId').val();
  		
  		
  		// Get the raw DOM object for the select box
  		var targetsIdselect = document.getElementById('targetTypeId');

  		// Clear the old options
  		targetsIdselect.options.length = 0;
  		
  		targetsIdselect.options.add(new Option("Target Type", '0'));
  		
  		if(deptId==0){
  			return false;
  		}
  		
  		$.ajax({
  			type:'POST',
  			url: myContextPath+"/fetchTargetsAndEmployeeListByDepartment/"+deptId,
  			async: false,
  			beforeSend: function() {
  				$('.preloader-background').show();
  				$('.preloader-wrapper').show();
  	           },
  			success : function(targetsAndEmployeeByDeptResponse){
  				// setting target type list
  				var targetTypeList=targetsAndEmployeeByDeptResponse.targetTypeList;		
  				if(targetTypeList!=null){
  					var options, index, option;
  					targetsIdselect = document.getElementById('targetTypeId');
  					for (var i = 0, len = targetTypeList.length; i < len; ++i) {
  						var targetType = targetTypeList[i];
  						targetsIdselect.options.add(new Option(targetType.type, targetType.targetTypeId+"_"+targetType.valueType));
  					}  					
  				}
  				
  				$('.preloader-wrapper').hide();
  				$('.preloader-background').hide();
  			},
  			error: function(xhr, status, error){
  				$('.preloader-wrapper').hide();
  				$('.preloader-background').hide();
  				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
  			}		
  		});
  		
  	});
      
    $('#targetTypeId').change(function(){
    	var targetTypeId=$('#targetTypeId').val();
    	var targetTypeName=$('#targetTypeId option:selected').text();
    	if(targetTypeId!=0){
    		targetValueType=targetTypeId.split("_")[1];
    		targetTypeId=targetTypeId.split("_")[0];
    	}else{
    		targetValueType=undefined;
    		targetTypeId="0";
    		return false;
    	}
    	/*for(var i=0; i<commissionSlabArr.length; i++){
    		if(targetValueType=="both"){
        		$("#commissionValueType_"+i).prop('disabled', false);
        	}else{
        		var source2 = $("#commissionValueType_"+i);
    			var v2=targetValueType;
    			source2.val(v2);
    			
        		$("#commissionValueType_"+i).prop('disabled', "disabled");
        		$("#commissionValueType_"+i).change();
        	}
    	}*/
    	if(targetValueType=="both"){
    		$("#commissionValueTypeMain").prop('disabled', false);
    	}else{
    		var source2 = $("#commissionValueTypeMain");
			var v2=targetValueType;
			source2.val(v2);
			
    		$("#commissionValueTypeMain").prop('disabled', "disabled");    		
    	}
    	$("#commissionValueTypeMain").change();
    	
    	if(targetTypeName.trim()=="Product Wise Target"){
    		$(".productDiv").show();
    		$(".forProductChange").removeClass("offset-l2");
    		$(".forProductChangeHide").show();
    	}else{
    		$(".productDiv").hide();
    		$(".forProductChange").addClass("offset-l2");
    		$(".forProductChangeHide").hide();
    	}
    	
    	/*$('#commissionName').val(targetTypeName);
    	$('#commissionName').change();*/
    });
    
   $('#commissionValueTypeMain').change(function(){
	   var commissionValueTypeMain=$('#commissionValueTypeMain').val();
	   for(var i=0; i<commissionSlabArr.length; i++){
   			var source2 = $("#commissionValueType_"+i);
   			var v2=commissionValueTypeMain;
   			source2.val(v2);
       		$("#commissionValueType_"+i).change();
   	  }
   });
    
    $('#commissionAssignIdSubmit').click(function(){
    	
    	
    	
    	var departmentId=$("#departmentId").val();
    	if(departmentId==0){
    		Materialize.toast('Select Department', '2000', 'teal lighten-2');
    		return false;
    	}
    	var targetTypeId=$("#targetTypeId").val();
    	if(targetTypeId==0){
    		Materialize.toast('Select Target Type', '2000', 'teal lighten-2');
    		return false;
    	}
    	var targetPeriodId=$("#targetPeriodId").val();
    	if(targetPeriodId=='0'){
    		Materialize.toast('Select Target Period', '2000', 'teal lighten-2');
    		return false;
    	}
    	var commissionName=$('#commissionName').val();
    	if(commissionName==''){
    		Materialize.toast('Enter Commission Assign Name', '2000', 'teal lighten-2');
    		return false;
    	}else if(checkCommissionAssignNameAlreadyUsedOrNot(commissionName)){
    		Materialize.toast('Commission Assign Name Already Used', '2000', 'teal lighten-2');
    		return false;
    	}
    	var productIds=$("#productId").val();
    	
    	var targetTypeName=$('#targetTypeId option:selected').text();
    	if(targetTypeName.trim()=="Product Wise Target"){    		
        	
    		var prodcutSlabType="";
    		if($('#amtPrdct').prop('checked')==true){
    			prodcutSlabType="amount";
    		}else{
    			prodcutSlabType="quantity";
    		}
    		
    		if(productIds.length==0){
        		Materialize.toast('select Products', '2000', 'teal lighten-2');
        		return false;
        	}
    		var commissionAssignProductsList=[];
    		if(productIds[0]=="0"){
    			$("#productId option").each(function()
				{
    				var productId=$(this).val();
    				if(productId!=0){
    					var commissionAssignProducts={
                    			product : {
                    					productId : productId
                    				}
                    	}
        				commissionAssignProductsList.push(commissionAssignProducts);
    				}
				});
        	}else{
        		var i=0;
        		for(i=0; i<productIdsOld.length; i++){
        			var productId=productIdsOld[i];
        			if(productId!=0){
	        			var commissionAssignProducts={
	                			product : {
	                					productId : productIdsOld[i]
	                				}
	                	}
	    				commissionAssignProductsList.push(commissionAssignProducts);
        			}
        		}
        	}
    	}
    	var includeTax=0;
    	if($('#includeTax').prop("checked") == true){
    		includeTax=1;
    	}else{
    		includeTax=0;
    	}
    	var cascadComm=0;
    	if($('#cascadComm').prop("checked") == true){
    		cascadComm=1;
    	}else{
    		cascadComm=0;
    	}
    	
    	if(validateCommission()){
    		var url="save_commission_assign";
    		if(isUpdating){
    			url="update_commission_assign";
    		}
    		CommissionAssignSaveRequest={
    				commissionAssign:{
    					commissionAssignId:pkId,
    					targetType:{
        					targetTypeId:parseInt(targetTypeId.split("_")[0]),
        					type :  targetTypeName
        				},
        				targetPeriod:targetPeriodId,
    					commissionAssignName:commissionName,
        				includeTax : includeTax,
        				cascadingCommission : cascadComm,
        				prodcutSlabType : prodcutSlabType
    				},
    				commissionAssignTargetSlabsList:commissionAssignTargetSlabsList,
    				commissionAssignProductsList:commissionAssignProductsList
    		};

      		$.ajax({
      			type:'POST',
      			url: myContextPath+"/"+url,
      			async: false,
      			headers : {
      				'Content-Type' : 'application/json'
      			},
      			beforeSend: function() {
      				$('.preloader-background').show();
      				$('.preloader-wrapper').show();
      	           },
      			data:JSON.stringify(CommissionAssignSaveRequest),
      			success : function(response){
      				if(response.status=="Success"){
      					if(isUpdating==false){
							  Materialize.toast('Target Commission Save SuccessFully', '2000', 'teal lighten-2');
      					}else{
      						Materialize.toast('Target Commission Update SuccessFully', '2000', 'teal lighten-2');
      					}
      					window.location.href=myContextPath+"/fetchCommissionAssign";
      					//fetchCommisionAssignList();
      					$('#openCommissionAssignForm').modal('close');
      				}else{
      					Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
      				}
      				
      				$('.preloader-wrapper').hide();
      				$('.preloader-background').hide();
      			},
      			error: function(xhr, status, error){
      				$('.preloader-wrapper').hide();
      				$('.preloader-background').hide();
      				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
      			}		
      		});
    		
    	}else{
    		return false;
    	}
    });
    
   
    $("#productId").change(function(){
    	//get new selected items
    	var productIds=$("#productId").val();
    	
    	//get current selected item by checking with  productIdsOld=[]
    	var productIdsDiff = productIds.filter(function(obj) { 
    		//console.log(obj+" -> "+productIdsOld.indexOf(obj));
    		return productIdsOld.indexOf(obj) == -1; 
    	});
    	//console.log(productIdsDiff);
    	
    	//if any new item added then its go for check all or other items
    	if(productIdsDiff.length!=0){
    		//check new item text is zero or not
    		var findAllChecked=productIdsDiff.find(function(productId){
				   return productId=="0";
			  });
    		
    		 //check current select values have All options selected or not
    		 var findZero=productIds.find(function(prodId){
				   return prodId=="0";
			  });
    		
    		//length of options in select tag
    		var lengthPrdt = $('#productId > option').length;
    		
    		//if zero then select set zero item only in select
			if(findAllChecked=="0" || ((lengthPrdt-1)==productIds.length && findZero!="0")){
				productIds=["0"];					
				$('#productId').val(productIds);
			}else{
				//if current selected item other than zero then remove zero item from selection
				var allSelectIndex=productIds.indexOf("0");
				if(allSelectIndex!=-1){
					productIds.splice(allSelectIndex, 1);
				}
				$('#productId').val(productIds);
			}
			//set current select items to old
			productIdsOld=productIds;
			
			//refresh select UI
			$('#productId').change();
    	}
    	//set current select items to old
		productIdsOld=productIds;
    });
    					
	
	
	if(mode=="edit"){
		editCommissionAssign(commissionAssignId);
	}else if(mode=="show"){
		fetchCommisionAssignList();
	}else if(mode=="add"){
		openCommissionAssign();
		$('#productId').val(productIdsOld);
		$('#productId').change();
	}
});

function addNewSlab(){
	var length=commissionSlabArr.length;
	
	var deleteButton="";
	if(length!=0){
	    deleteButton='<td class="input-field col s1 l1 m1  push-l1">'+
		               '<button class="btn-flat red" type="button" onclick="deleterow(' + (length) + ')"><i class="material-icons">clear</i></button>'+
		             '</td>';
	}else{
		
		deleteButton='';
	}
	var commissionValueType='<select id="commissionValueType_'+length+'" name="targetTypeId" disabled>';
	/*if(targetValueType!="both" && targetValueType!=undefined){
		commissionValueType='<select id="commissionValueType_'+length+'" name="targetTypeId" disabled>';
	}*/
	commissionSlabArr.push([1,0,0,'',0]);
		/*$('#addCommSlab').append('<div class="row" id="commissionSlabId_'+length+'">'+
									'<div class="input-field col s1 m1 l1 push-l1">'+
								        '<span style="font-size:34px;">'+commissionSlabArr.length+'.</span>'+
								    '</div>'+
									'<div class="input-field col s4 m4 l2 push-l1">'+
					              	    '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="from_'+length+'" type="text" name="rate">'+
						                '<label for="from_'+length+'" class="active">From</label>'+
						            '</div>'+
						            '<div class="input-field col s4 m4 l2 push-l1"> '+
						                '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="to_'+length+'" type="text" name="rate">'+
						                '<label for="to_'+length+'" class="active">To</label>'+
						            '</div>'+
						            '<div class="input-field col s4 m4 l3 push-l1">  '+
						                '<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>   '+                      
						                		  commissionValueType+
						                         '<option value="0">Value Type</option>'+
						                         '<option value="percentage">By Percentage</option>'+
						                         '<option value="value">By Value</option>'+
						                '</select>'+
						            '</div>  '+
						            '<div class="input-field col s4 m4 l2 push-l1"> '+
						                '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="value_'+length+'" type="text" name="rate">'+
						                '<label for="value_'+length+'" class="active">Value</label>'+
						            '</div>'+
						             deleteButton+
						       		'</div>');*/
	$('#addCommSlab').append('<table id="commissionSlabId_'+length+'"><tr>'+
							'<td class="input-field col s1 m1 l1 push-l1">'+
						        '<span style="font-size:25px;">'+commissionSlabArr.length+'.</span>'+
						    '</td>'+
							'<td class="input-field col s5 m2 l2 push-l1">'+
				          	    /*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
				                '<input id="from_'+length+'" type="text" name="rate">'+
				                '<label for="from_'+length+'" class="active">From</label>'+
				            '</td>'+
				            '<td class="input-field col s6 m2 l2 push-l1"> '+
				                /*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
				                '<input id="to_'+length+'" type="text" name="rate">'+
				                '<label for="to_'+length+'" class="active">To</label>'+
				            '</td>'+
				            '<td class="input-field col s5 m3 l3 push-l1 push-s1">  '+
				                /*'<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>   '+ */                     
				                		  commissionValueType+
				                         /*'<option value="0">Value Type</option>'+*/
				                         '<option value="percentage">By Percentage</option>'+
				                         '<option value="value">By Value</option>'+
				                '</select>'+
				            '</td>  '+
				            '<td class="input-field col s6 m2 l2 push-l1 push-s1"> '+
				                /*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
				                '<input id="value_'+length+'" type="text" name="rate">'+
				                '<label for="value_'+length+'" class="active">Value</label>'+
				            '</td>'+
				             deleteButton+
				       		'</tr></table>');
			$('select').material_select();	
			
			$('#targetTypeId').change();
			
			  defineAmountValidation("from_"+length);
			  $('#from_'+length).off("keyup");//previous event delete
		      $('#from_'+length).keyup(function(){
		    	  var id=$(this).attr('id')
				  var num=id.split('_')[1];
		    	  var from0=$('#from_'+num).val();
		    	  var to0=$('#to_'+num).val();
		    	  var valueType=$('#commissionValueType_'+num).val();
		    	  var value0=$('#value_'+num).val();
		    	  
		    	  if(valueType==0){
		    		  valueType='';
		    	  }
		    	  if(from0==''){ 
		    		  from0=0;
		    	  }
		    	  if(to0==''){
		    		  to0=0;
		    	  }
		    	  if(value0==''){
		    		  value0=0;
		    	  }
		    	  commissionSlabArr[num]=[1,from0,to0,valueType,value0,commissionSlabArr[num][5]];
		      });
		      defineAmountValidation("to_"+length);
		      $('#to'+length).off("keyup");//previous event delete
		      $('#to_'+length).keyup(function(){
		    	  var id=$(this).attr('id')
				  var num=id.split('_')[1];
		    	  var from0=$('#from_'+num).val();
		    	  var to0=$('#to_'+num).val();
		    	  var valueType=$('#commissionValueType_'+num).val();
		    	  var value0=$('#value_'+num).val();
		    	  
		    	  if(valueType==0){
		    		  valueType='';
		    	  }
		    	  if(from0==''){ 
		    		  from0=0;
		    	  }
		    	  if(to0==''){
		    		  to0=0;
		    	  }
		    	  if(value0==''){
		    		  value0=0;
		    	  }
		    	  commissionSlabArr[num]=[1,from0,to0,valueType,value0,commissionSlabArr[num][5]];
		      });
		      $('#commissionValueType_'+length).off("change");//previous event delete
		      $('#commissionValueType_'+length).change(function(){
		    	  var id=$(this).attr('id')
				  var num=id.split('_')[1];
		    	  var from0=$('#from_'+num).val();
		    	  var to0=$('#to_'+num).val();
		    	  var valueType=$('#commissionValueType_'+num).val();
		    	  var value0=$('#value_'+num).val();
		    	  
		    	  if(valueType==0){
		    		  valueType='';
		    	  }
		    	  if(from0==''){ 
		    		  from0=0;
		    	  }
		    	  if(to0==''){
		    		  to0=0;
		    	  }
		    	  if(value0==''){
		    		  value0=0;
		    	  }
		    	 var amount=0;
		    	 if(valueType=="percentage"){
		    		 amount=(parseFloat(to0)*parseFloat(value0))/100;
	    		 }else{
	    			 amount=value0;
	    		 }
		    	 commissionSlabArr[num]=[1,from0,to0,valueType,value0,parseFloat(amount).toFixed(2)];
		      });
		      defineAmountValidation("value_"+length);
		      $('#value_'+length).off("change");//previous event delete
		      $('#value_'+length).keyup(function(){
		    	  var id=$(this).attr('id')
				  var num=id.split('_')[1];
		    	  var from0=$('#from_'+num).val();
		    	  var to0=$('#to_'+num).val();
		    	  var valueType=$('#commissionValueType_'+num).val();
		    	  var value0=$('#value_'+num).val();
		    	  
		    	  if(valueType==0){
		    		  valueType='';
		    	  }
		    	  if(from0==''){ 
		    		  from0=0;
		    	  }
		    	  if(to0==''){
		    		  to0=0;
		    	  }
		    	  if(value0==''){
		    		  value0=0;
		    	  }
		    	  var amount=0;
		    	 if(valueType=="percentage"){
		    		 amount=(parseFloat(to0)*parseFloat(value0))/100;
	    		 }else{
	    			 amount=value0;
	    		 }
		    	  commissionSlabArr[num]=[1,from0,to0,valueType,value0,parseFloat(amount).toFixed(2)];
		      });
}
function deleterow(index){
	commissionSlabArr.splice(index,1);
	$("#commissionSlabId_"+index).remove();
}
function defineAmountValidation(id){
	// only numbers allowed with decimal
    $('#'+id).keydown(function(e){            	
			-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
	  });
	  
	  document.getElementById(id).onkeypress=function(e){
	  	
	  	if (e.keyCode === 46 && this.value.split('.').length === 2) {
	    		 return false;
		}

    }
}

function validateCommission(){
	if(commissionSlabArr.length==0){
		  Materialize.toast('Fill 1st slab', '2000', 'teal lighten-2');
		  return false;
	  }
	commissionAssignTargetSlabsList=[];
	  var endValue=0,endAmount=0,endPercentage=0;
	  for(var i=0; i<commissionSlabArr.length; i++){
		  var valueType=$('#commissionValueType_'+i).val();
		  
		  var commSlab=commissionSlabArr[i];
		  var endValueType='';
		 if(i==0){
			 if(parseFloat(commSlab[1])==0){
				 Materialize.toast((i+1)+' slab : From Value Can not be Zero or Empty', '2000', 'teal lighten-2');
				 return false;
			 }
		 }
		 if(parseFloat(commSlab[1])==0 || parseFloat(commSlab[2])==0 || commSlab[3]=='' || parseFloat(commSlab[4])==0){
			 Materialize.toast((i+1)+' slab : Fill', '2000', 'teal lighten-2');
			 return false;
		 }
		 if(parseFloat(commSlab[1])<=parseFloat(endValue)){
			  Materialize.toast((i+1)+' slab : \'from\' value must be greater than previous \'to\' value', '2000', 'teal lighten-2');
			  return false;
		 }
		 if(parseFloat(commSlab[1])!=(parseFloat(endValue)+1) && i!=0){
			  Materialize.toast((i+1)+' slab : \'from\' value must be next continue value of previous \'to\' value', '2000', 'teal lighten-2');
			  return false;
		 }
		 if(parseFloat(commSlab[1])>=parseFloat(commSlab[2])){
			  Materialize.toast((i+1)+' slab : \'from\' value must be less than \'to\' value to', '2000', 'teal lighten-2');
			  return false;
		 }
		 if(commSlab[3]=='percentage' && (parseFloat(commSlab[4])>100 || parseFloat(commSlab[4])<0)){
			Materialize.toast((i+1)+' slab : Invalid \'Percentage\' ', '2000', 'teal lighten-2');
			return false;
		 }
		 
		 /*if(parseFloat(commSlab[5])<=parseFloat(endAmount)){
			 var msgText="";
			 if(valueType=="percentage"){
				 msgText=' slab : \'Incentive '+valueType+'\'('+parseFloat(commSlab[4])+'%) is ('+parseFloat(commSlab[5])+') must be greater than previous '; 
			 }else{
				 msgText=' slab : \'Incentive '+valueType+'\' is ('+parseFloat(commSlab[5])+') must be greater than previous ';
			 }
			 
			 if(endValueType=="percentage"){
				 msgText+=endValueType+'\'('+endPercentage+'%) is ('+endAmount+')'; 
			 }else{
				 msgText=endValueType+'\' is ('+endAmount+')';
			 }
			 
			 Materialize.toast((i+1)+msgText, '2000', 'teal lighten-2');
			  return false;
		 }*/
		 endValueType=commSlab[3];
		 endPercentage=parseFloat(commSlab[4]);
		 endAmount=parseFloat(commSlab[5]);
		 endValue=parseFloat(commSlab[2]);
		 
		 commissionAssignTargetSlabsList.push({
			 fromValue:parseFloat(commSlab[1]),
			 toValue:parseFloat(commSlab[2]),
			 commissionType:commSlab[3],
			 targetValue:parseFloat(commSlab[4]) 
		 });
	  }
	  return true;
}

function fetchCommisionAssignList(){
	
	var t = $('#tblData').DataTable();
	t.clear().draw();
	$.ajax({
			type:'POST',
			url: myContextPath+"/fetch_commission_assign",
			async: false,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(response){
				if(response.status=="Success"){
					var commissionAssignList=response.commissionAssignList;
					commissionAssignListOld=commissionAssignList;
					for(var i=0; i<commissionAssignList.length; i++){
						var commissionAssign=commissionAssignList[i].commissionAssign1;
						var commissionAssignTargetSlabsList=commissionAssignList[i].commissionAssignTargetSlabsList;
						//commissionAssignTargetSlabsListMap.set(commissionAssign.commissionAssignId,commissionAssignList[i]);
						t.row.add(
								[
									(i+1),
									'<button class="btn-flat" type="button" onclick="showCommissionAssign(' + commissionAssign.commissionAssignId + ')"><b>'+commissionAssign.commissionAssignName+'</b></button>',
									commissionAssign.targetType.type,
									commissionAssign.targetPeriod,
									'<a class="btn-flat" type="button" href="'+myContextPath+'/commissionAssign?commissionAssignId='+commissionAssign.commissionAssignId+'"><i class="material-icons">edit</i></a>'+
									'<a class="btn-flat" type="button" onclick="commissionAssignDisableConfirmation(' + commissionAssign.commissionAssignId + ')"><i class="material-icons">clear</i></a>'
								]
								).draw(false); 
						
					}
					
				}else{
					Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
}
function editCommissionAssign(commissionAssignId){
	isUpdating=true;
	$('.preloader-wrapper').show();
	 $('.preloader-background').show();

	 fetchCommissionAssignByCommissionAssignId(commissionAssignId); 
	var commissionAssignList=commissionAssignListDB;
	var commissionAssign=commissionAssignList.commissionAssign;
	var commissionAssignTargetSlabsList=commissionAssignList.commissionAssignTargetSlabsList;
	
	if(commissionAssign.cascadingCommission==0){
		$('#cascadComm').prop("checked",false);
	}else{
		$('#cascadComm').prop("checked",true);
	}

	if(commissionAssign.includeTax==0){
		$('#includeTax').prop("checked",false);
	}else{
		$('#includeTax').prop("checked",true);
	}

	if(commissionAssign.prodcutSlabType=="amount"){
		$('#amtPrdct').prop("checked",true);
	}else{
		$('#qtyPrdct').prop("checked",true);
	}
	
	if(commissionAssignList.commissionAssign.targetType.type=="Product Wise Target"){
		var commissionAssignProductsList=commissionAssignList.commissionAssignProductsList;
		
		for(i=0; i<commissionAssignProductsList.length; i++){
			productIdsOld.push(commissionAssignProductsList[i].product.productId);
		}
		
		$("#productId").val(productIdsOld);
		$("#productId").change();
		
		$(".productDiv").show();
		$(".forProductChange").removeClass("offset-l2");
		$(".forProductChangeHide").show();
	}else{
		/*$("#productId").val(0);
		$("#productId").change();*/
		
		$(".productDiv").hide();
		$(".forProductChange").addClass("offset-l2");
		$(".forProductChangeHide").hide();
	}
	
	pkId=commissionAssign.commissionAssignId;
	
	var departmentId = $("#departmentId");
	departmentId.val(commissionAssign.targetType.department.departmentId);
	$("#departmentId").change();
	
	$('.preloader-wrapper').show();
	$('.preloader-background').show();

	var targetPeriodId = $("#targetPeriodId");
	targetPeriodId.val(commissionAssign.targetPeriod);
	$("#targetPeriodId").change();
	
	$('.preloader-wrapper').show();
	$('.preloader-background').show();

	//slab making
	commissionSlabArr=[];
	$('#addCommSlab').empty();
	for(var i=0; i<commissionAssignTargetSlabsList.length; i++){
		var commissionAssignTargetSlab=commissionAssignTargetSlabsList[i];
		var length=i;
		commissionSlabArr.push([length,commissionAssignTargetSlab.fromValue,commissionAssignTargetSlab.toValue,commissionAssignTargetSlab.commissionType,commissionAssignTargetSlab.targetValue]);
		
		$('#commissionName').val(commissionAssign.commissionAssignName);
		 $('#commissionName').change();
		 
		var deleteButton="";
		if(length!=0){
		    deleteButton='<td class="input-field col s1 l1 m1  push-l1">'+
			               '<button class="btn-flat red" type="button" onclick="deleterow(' + (length) + ')"><i class="material-icons">clear</i></button>'+
			             '</td>';
		}else{
			
			deleteButton='';
		}
		targetValueType=commissionAssignTargetSlab.commissionType;
		var commissionValueType='<select id="commissionValueType_'+length+'" name="targetTypeId" disabled>';
		// if(targetValueType!="both" && targetValueType!=undefined){
		// 	commissionValueType='<select id="commissionValueType_'+length+'" name="targetTypeId" disabled>';
		// }

		var selectedComValType='';
		if(targetValueType=="percentage"){
			selectedComValType='<option value="percentage" selected>By Percentage</option>'+
								'<option value="value">By Value</option>';
		}else if(targetValueType=="value"){
			selectedComValType='<option value="percentage" >By Percentage</option>'+
								'<option value="value" selected>By Value</option>';
		}	

			$('#addCommSlab').append('<table id="commissionSlabId_'+length+'"><tr>'+
										'<td class="input-field col s1 m1 l1 push-l1">'+
											'<span style="font-size:25px;">'+commissionSlabArr.length+'.</span>'+
										'</td>'+
										'<td class="input-field col s5 m2 l2 push-l1">'+
											/*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
											'<input id="from_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.fromValue+'">'+
											'<label for="from_'+length+'" class="active">From</label>'+
										'</td>'+
										'<td class="input-field col s6 m2 l2 push-l1"> '+
											/*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
											'<input id="to_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.toValue+'">'+
											'<label for="to_'+length+'" class="active">To</label>'+
										'</td>'+
										'<td class="input-field col s5 m3 l3 push-l1 push-s1">  '+
											/*'<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>   '+ */                     
													commissionValueType+
													/*'<option value="0">Value Type</option>'+*/
													selectedComValType+
											'</select>'+
										'</td>  '+
										'<td class="input-field col s6 m2 l2 push-l1 push-s1"> '+
											/*'<i class="fa fa-inr prefix" aria-hidden="true"></i>'+*/
											'<input id="value_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.targetValue+'">'+
											'<label for="value_'+length+'" class="active">Value</label>'+
										'</td>'+
										deleteButton+
										'</tr></table>');

										/*
										('<div class="row" id="commissionSlabId_'+length+'">'+
									'<div class="input-field col s1 m1 l1 push-l1">'+
								        '<span style="font-size:34px;">'+commissionSlabArr.length+'.</span>'+
								    '</div>'+
									'<div class="input-field col s4 m4 l2 push-l1">'+
					              	    '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="from_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.from+'">'+
						                '<label for="from_'+length+'" class="active">From</label>'+
						            '</div>'+
						            '<div class="input-field col s4 m4 l2 push-l1"> '+
						                '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="to_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.to+'">'+
						                '<label for="to_'+length+'" class="active">To</label>'+
						            '</div>'+
						            '<div class="input-field col s4 m4 l3 push-l1">  '+
						                '<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>   '+                      
						                		  commissionValueType+
						                         '<option value="0">Value Type</option>'+
						                         selectedComValType+
						                '</select>'+
						            '</div>  '+
						            '<div class="input-field col s4 m4 l2 push-l1"> '+
						                '<i class="fa fa-inr prefix" aria-hidden="true"></i>'+
						                '<input id="value_'+length+'" type="text" name="rate" value="'+commissionAssignTargetSlab.targetValue+'">'+
						                '<label for="value_'+length+'" class="active">Value</label>'+
						            '</div>'+
						             deleteButton+
									   '</div>');
										*/
			$('select').material_select();	
			
			//$('#targetTypeId').change();
			
			defineAmountValidation("from_"+length);
			$('#from_'+length).off("keyup");//previous event delete
			$('#from_'+length).keyup(function(){
				  var id=$(this).attr('id')
				  var num=id.split('_')[1];
				  var from0=$('#from_'+num).val();
				  var to0=$('#to_'+num).val();
				  var valueType=$('#commissionValueType_'+num).val();
				  var value0=$('#value_'+num).val();
				  
				  if(valueType==0){
					  valueType='';
				  }
				  if(from0==''){ 
					  from0=0;
				  }
				  if(to0==''){
					  to0=0;
				  }
				  if(value0==''){
					  value0=0;
				  }
				  commissionSlabArr[num]=[1,from0,to0,valueType,value0,commissionSlabArr[num][5]];
			});
			defineAmountValidation("to_"+length);
			$('#to'+length).off("keyup");//previous event delete
			$('#to_'+length).keyup(function(){
				  var id=$(this).attr('id')
				  var num=id.split('_')[1];
				  var from0=$('#from_'+num).val();
				  var to0=$('#to_'+num).val();
				  var valueType=$('#commissionValueType_'+num).val();
				  var value0=$('#value_'+num).val();
				  
				  if(valueType==0){
					  valueType='';
				  }
				  if(from0==''){ 
					  from0=0;
				  }
				  if(to0==''){
					  to0=0;
				  }
				  if(value0==''){
					  value0=0;
				  }
				  commissionSlabArr[num]=[1,from0,to0,valueType,value0,commissionSlabArr[num][5]];
			});
			$('#commissionValueType_'+length).off("change");//previous event delete
			$('#commissionValueType_'+length).change(function(){
				  var id=$(this).attr('id')
				  var num=id.split('_')[1];
				  var from0=$('#from_'+num).val();
				  var to0=$('#to_'+num).val();
				  var valueType=$('#commissionValueType_'+num).val();
				  var value0=$('#value_'+num).val();
				  
				  if(valueType==0){
					  valueType='';
				  }
				  if(from0==''){ 
					  from0=0;
				  }
				  if(to0==''){
					  to0=0;
				  }
				  if(value0==''){
					  value0=0;
				  }
				 var amount=0;
				 if(valueType=="percentage"){
					 amount=(parseFloat(to0)*parseFloat(value0))/100;
				 }else{
					 amount=value0;
				 }
				 commissionSlabArr[num]=[1,from0,to0,valueType,value0,parseFloat(amount).toFixed(2)];
			});
			defineAmountValidation("value_"+length);
			$('#value_'+length).off("change");//previous event delete
			$('#value_'+length).keyup(function(){
				  var id=$(this).attr('id')
				  var num=id.split('_')[1];
				  var from0=$('#from_'+num).val();
				  var to0=$('#to_'+num).val();
				  var valueType=$('#commissionValueType_'+num).val();
				  var value0=$('#value_'+num).val();
				  
				  if(valueType==0){
					  valueType='';
				  }
				  if(from0==''){ 
					  from0=0;
				  }
				  if(to0==''){
					  to0=0;
				  }
				  if(value0==''){
					  value0=0;
				  }
				  var amount=0;
				 if(valueType=="percentage"){
					 amount=(parseFloat(to0)*parseFloat(value0))/100;
				 }else{
					 amount=value0;
				 }
				  commissionSlabArr[num]=[1,from0,to0,valueType,value0,parseFloat(amount).toFixed(2)];
			});
	}
	setTimeout(function(){
		 var targetTypeId = $("#targetTypeId");
		 targetTypeId.val(commissionAssign.targetType.targetTypeId+"_"+commissionAssign.targetType.valueType);
		 targetTypeId.change();
		  
		 $('.preloader-wrapper').hide();
		 $('.preloader-background').hide();
	}, 2000);
	
}
function showCommissionAssign(commissionAssignId){
	$('.filterTab').find('li>a').removeClass('active');
	$('#commissionTab').click();
	$('#commissionAssignSlabId').empty();
	fetchCommissionAssignByCommissionAssignId(commissionAssignId); 
	var commissionAssignList=commissionAssignListDB;
	if(commissionAssignList.commissionAssign.targetType.type=="Product Wise Target"){
		var commissionAssignProductsList=commissionAssignList.commissionAssignProductsList;
		
		$("#productsNamesBody").empty();
		var i=0;
		var tblData="";
		for(i=0; i<commissionAssignProductsList.length;){
			
								tblData+='<tr>'+
										'<td>'+(commissionAssignProductsList[i]!=undefined?commissionAssignProductsList[i].product.productName : '') +'</td>'+
										'<td>'+(commissionAssignProductsList[i+1]!=undefined?commissionAssignProductsList[i+1].product.productName : '')+'</td>'+
										'<td>'+(commissionAssignProductsList[i+2]!=undefined?commissionAssignProductsList[i+2].product.productName : '') +'</td>'+				
										'</tr>';
			i+=3;
			
			//productNames+=commissionAssignProductsList[i].product.productName+",";
		}
		$("#productsNamesBody").append(tblData);		
		$(".filterTab").show();
		$('.productNameHide').show();
		
	}else{
		$(".filterTab").hide();
		$('#productNameId').text("");
		$('.productNameHide').hide();
	}
	for(var i=0; i<commissionAssignList.commissionAssignTargetSlabsList.length; i++){
		var commissionAssignTargetSlabsList=commissionAssignList.commissionAssignTargetSlabsList;
		
		var targetValue;
		if(commissionAssignTargetSlabsList[i].commissionType=="percentage"){
			targetValue='<td>'+commissionAssignTargetSlabsList[i].targetValue+'%</td>';
		}else{
			targetValue='<td>&#8377; '+commissionAssignTargetSlabsList[i].targetValue+'</td>';
		}
		
		$('#commissionAssignSlabId').append('<tr>'+
										        '<td>'+(i+1)+'</td>'+
										        '<td>'+commissionAssignTargetSlabsList[i].fromValue+'</td>'+
										        '<td>'+commissionAssignTargetSlabsList[i].toValue+'</td>'+
										        '<td>'+commissionAssignTargetSlabsList[i].commissionType+'</td>'+
										        '<td>'+commissionAssignTargetSlabsList[i].commissionAssign.targetPeriod+'</t>'+
										        targetValue+
										    '</tr>');
	}
	$('#viewDetails').modal('open');
}

function checkCommissionAssignNameAlreadyUsedOrNot(name){
	return checkCommissionAssignName(name,pkId);
} 

function commissionAssignDisableConfirmation(commissionAssignId){
	/* $('.modal').modal();
	$('#delete'+id).modal('open'); */
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="disableCommissionAssign('+commissionAssignId+')">Yes</button>  '));
	  Materialize.toast($toastContent, "abc");
}

function disableCommissionAssign(commissionAssignId){
	Materialize.Toast.removeAll();
	$.ajax({
			type:'GET',
			url: myContextPath+"/disable_commission_assign/"+commissionAssignId,
			async: false,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(response){
				if(response.status=="Success"){
					Materialize.toast('Target Commission Deleted SuccessFully', '2000', 'teal lighten-2');
					fetchCommisionAssignList();
				}else{
					Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
				}
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
} 
function fetchCommissionAssignByCommissionAssignId(commissionAssignId){
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_commission_assign_slab/"+commissionAssignId,
		async: false,
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				commissionAssignListDB=response;				
			}else{
				Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
			}
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}
	function checkCommissionAssignName(commissionAssignName,commissionAssignId){
		var isFindSameName=false;
		var obj={
				'commissionAssignName' : commissionAssignName,
				'commissionAssignId' : commissionAssignId
				}
		$.ajax({
			type:'POST',
			url: myContextPath+"/check_commission_assign_name_exist",
			async: false,
			/*headers : {
				'Content-Type' : 'application/json' 
			},*/
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
	        data:obj,
			success : function(response){ 
				if(response.status=="Success"){
					isFindSameName=false;			
				}else{
					isFindSameName=true;
					//Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		return isFindSameName;
  }

  function openCommissionAssign(){
	isUpdating=false;
	pkId=0;
	$('#addCommSlab').empty();
	commissionSlabArr=[];
	addNewSlab();
	
	$("#departmentId").val(0);
	$("#departmentId").change();
	
	$("#targetTypeId").val(0);
	$("#targetTypeId").change();
	/*
	$("#productId").val(0);
	$("#productId").change();*/
	
	$(".productDiv").hide();
	$(".forProductChange").addClass("offset-l2");
	$(".forProductChangeHide").hide();
	
	$('#commissionName').val('');
  }
