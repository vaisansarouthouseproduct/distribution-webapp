var employeeDetailsIdOld,pickDateOld;
$(document).ready(function() {
	
	$('#cancelReasonButtonId').click(function(){
		var meetingId=$('#meetingIdCancelReaons').val();
		var cancelReason=$('#cancelReason').val();
		if(cancelReason=="" || cancelReason==undefined){
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Cancel Reason', '2000', 'teal lighten-2');
			return false;
		}
		$.ajax({
			type:'POST',
			url: myContextPath+"/cancel_meeting",
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			data:JSON.stringify({'meetingId':meetingId,'cancelReason':cancelReason}),
			async: false,
			success : function(response){
				Materialize.Toast.removeAll();
				viewMeetings(employeeDetailsIdOld,pickDateOld);
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
	});
	mainTableInitializeWithDatable();
});

function viewMeetings(employeeDetailsId,pickDate){
	
	employeeDetailsIdOld=employeeDetailsId;
	pickDateOld=pickDate;
	
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetchMeetingsByEmployeeIdAndPickDate",
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify({'employeeDetailsId':employeeDetailsId,'pickDate':pickDate}),
		async: false,
		success : function(response){
			
			meetingList=response;
			setMeetingList(meetingList);
			$('#viewDetails').modal('open');
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
	
}

function setMeetingList(meetingList){
	 $("#meetingList").empty();
	 for(var i=0; i<meetingList.length; i++){
		 var meeting=meetingList[i];
		 
		 var startTime=moment(meeting.meetingFromDateTime).format('hh:mm a');
		 var endTime=moment(meeting.meetingToDateTime).format('hh:mm a');
		 
		 var status="",buttons="";
		 if(meeting.meetingStatus.toUpperCase()=="CANCEL"){
			 status='<button class="btn-flat" onclick="showCancelReason(\''+meeting.cancelReason+'\')">'+
		         	'<b>'+
		         		meeting.meetingStatus.toUpperCase()+
		         	'</b>'+
		         	'</button>';
		}else{
			 status=meeting.meetingStatus.toUpperCase();
		}
		
		 var meetingVenue="";
		 if(meeting.meetingVenue=="" || meeting.meetingVenue==null || meeting.meetingVenue==undefined){
			 meetingVenue="NA";
		 }else{
			 meetingVenue='<button class="btn-flat" onclick="showMeetingVenue(\''+meeting.meetingVenue+'\')">'+
	         	'<i class="material-icons">event_note</i>'+
	         	'</button>';
		 }
		 
		 
		 var currentDate=new Date(new Date());
		 var meetingDate=new Date(meeting.meetingToDateTime);
		 
		 if(currentDate<=meetingDate && meeting.meetingStatus.toUpperCase()!="CANCEL" && meeting.meetingStatus.toUpperCase()!="COMPLETE" && meeting.meetingStatus.toUpperCase()!="RESCHEDULE"){
			 	buttons='<a class="btn-flat" title="Update"  href="'+myContextPath+'/open_update_meeting?meetingId='+meeting.meetingId+'"><i class="material-icons">edit</i></a>&nbsp;&nbsp;'+
			     		'<button class="btn-flat" title="Cancel"  type="button" onclick="deleteCancelConfirmation('+meeting.meetingId+')"><i class="material-icons">clear</i></button>';
		 }else if(meeting.meetingStatus.toUpperCase()=="PENDING"){
			    buttons='<a class="btn-flat" title="ReSchedule" href="'+myContextPath+'/open_reschedule_meeting?meetingId='+meeting.meetingId+'"><i class="material-icons">autorenew</i></a>&nbsp;&nbsp;';
		 }else{
			    buttons="NA";
		 }
		 
		 $('#meetingList').append('<tr>'+
					                 '<td>'+(i+1)+'</td>'+
					                 '<td>'+startTime.toUpperCase()+'-'+endTime.toUpperCase()+'</td>'+
					                 '<td>'+meeting.shopName+'</td>'+
					                 '<td>'+meeting.ownerName+'</td>'+
					                 '<td>'+meeting.mobileNumber+'</td>'+
					                 '<td>'+meeting.address+'</td>'+
					                 '<td>'+meetingVenue+'</td>'+
					                 '<td>'+status+'</td>'+
					                 '<td>'+buttons+'</td>'+
					             '</tr>');
	 }
}

function showCancelReason(reason){
	$('#cancelReasonShowId').text(reason);
	$('#cancelReasonShowModal').modal('open');
}
function showMeetingVenue(venue){
	$('#venueReasonShowId').text(venue);
	$('#venueReasonShowModal').modal('open');
}

function deleteCancelConfirmation(meetingId){
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Cancel Or Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Close</button><button class="btn white-text toast-action" onclick="deleteMeeting('+meetingId+')">Delete</button><button class="btn white-text toast-action" onclick="cancelMeetingPre('+meetingId+')">Cancel</button>'));
	  Materialize.toast($toastContent, "abc");
}

function cancelMeetingPre(meetingId){
	Materialize.Toast.removeAll();
	$('#meetingIdCancelReaons').val(meetingId);
	$('#cancelReasonModal').modal('open');
} 

function deleteMeeting(meetingId){
	Materialize.Toast.removeAll();
	$.ajax({
		type:'POST',
		url: myContextPath+"/delete_meeting",
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify({'meetingId':meetingId}),
		async: false,
		success : function(response){
			
			viewMeetings(employeeDetailsIdOld,pickDateOld);
			refreshEmployeeMeetings(pickDateMainOld,departmentIdOld);
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function refreshEmployeeMeetings(pickDateMainOld,departmentIdOld){
	$('#employeeTblData').empty();
	
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_scheduled_meeting_ajax",
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify({'pickDate':pickDateMainOld,'departmentId':departmentIdOld}),
		async: false,
		success : function(response){
			
			var employeeMeetingList=response;
			
			for(var i=0; i<employeeMeetingList.length; i++){
				var meeting=employeeMeetingList[i];
				
				var buttonYes="";
				if(meeting.isMeetingScheduled==true){
					buttonYes='<button class="btn-flat" onclick="viewMeetings(\''+meeting.employeeDetailsId+'\',\''+pickDateMainOld+'\')" type="button">Yes</button>';
				}else{
					buttonYes='<button class="btn-flat" type="button">No</button>';
				}
				
				$('#employeeTblData').append('<tr>'+
		                    '<td>'+meeting.srno+'</td>'+
	                        '<td>'+meeting.employeeName+'</td>'+
	                        '<td>'+meeting.departmentName+'</td>'+
	                        '<td>'+buttonYes+'</td>'+
		             '</tr>');
			}
			//mainTableInitializeWithDatable();
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function mainTableInitializeWithDatable(){
	var table = $('#tblData').DataTable();
	 table.destroy();
	 $('#tblData').DataTable({
        "oLanguage": {
            "sLengthMenu": "Show _MENU_",
            "sSearch": "_INPUT_" //search
        },
        autoWidth: false,
        columnDefs: [  
                     { 'width': '1%', 'targets': 0},
                     { 'width': '5%', 'targets': 1},
                     { 'width': '5%', 'targets': 2},
                     { 'width': '2%', 'targets': 3}
                     ],
        lengthMenu: [
            [50, 75, 100, -1],
            ['50 ', '75 ', '100', 'All']
        ],
        
       
        //dom: 'lBfrtip',
        dom:'<lBfr<"scrollDivTable"t>ip>',
        buttons: {
            buttons: [
                //      {
                //      extend: 'pageLength',
                //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                //  }, 
                {
                    extend: 'pdf',
                    className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: '.print-col'
                    },
                    customize: function(doc, config) {
                   	 doc.content.forEach(function(item) { 
                   		  if (item.table) {
                   		  item.table.widths = ['*','*','*','*']  
                   		 } 
                   		    })
                        var tableNode;
                        for (i = 0; i < doc.content.length; ++i) {
                          if(doc.content[i].table !== undefined){
                            tableNode = doc.content[i];
                            break;
                          }
                        }
       
                        var rowIndex = 0;
                        var tableColumnCount = tableNode.table.body[rowIndex].length;
                         
                        if(tableColumnCount > 6){
                          doc.pageOrientation = 'landscape';
                        }
                        /*for customize the pdf content*/ 
                        doc.pageMargins = [5,20,10,5];
                        
                        doc.defaultStyle.fontSize = 8	;
                        doc.styles.title.fontSize = 12;
                        doc.styles.tableHeader.fontSize = 11;
                        doc.styles.tableFooter.fontSize = 11;
                        doc.styles.tableHeader.alignment = 'center';
                        doc.styles.tableBodyEven.alignment = 'center';
                        doc.styles.tableBodyOdd.alignment = 'center';
                      },
                },
                {
                    extend: 'excel',
                    className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: '.print-col'
                    },
                },
                {
                    extend: 'print',
                    className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: '.print-col'
                    },
                },
                {
                    extend: 'colvis',
                    className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                    collectionLayout: 'fixed two-column',
                    align: 'left'
                },
            ]
        }

    });
	 $("select")
     .change(function() {
         var t = this;
         var content = $(this).siblings('ul').detach();
         setTimeout(function() {
             $(t).parent().append(content);
             $("select").material_select();
         }, 200);
     });
 $('select').material_select();
 $('.dataTables_filter input').attr("placeholder", "Search");
 /* var table = $('#tblData').DataTable(); // note the capital D to get the API instance
 var column = table.columns('.toggle');
 column.visible(false);
 $('#showColumn').on('click', function () {
 	 column.visible( ! column.visible()[0] );
 }); */
}