 var commissionAssignList=[];
 var TargetsSaveRequest;
 var updateInRegular="No";
//fetch employee which already assigned targets
var employeeIdsAssigned="";
$(document).ready(function(){
	$(".filterTab").hide();
	//only number taken without decimal 
	/*$('#targetValueId').keypress(function(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        return false;
	    }
	    return true;
	});*/
	
	//only numbers allowed with decimal 
    $('#targetValueId').keydown(function(e){            	
		-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
	 });
    
	document.getElementById('targetValueId').onkeypress=function(e){
    	
    	if (e.keyCode === 46 && this.value.split('.').length === 2) {
      		 return false;
  		 }

    }
	
	
	$('#departmentId').change(function(){
		
		//clear all details if department change
		commissionAssignList=[];
		resetForm();
		
		var deptId=$('#departmentId').val();
		
		// Get the raw DOM object for the select box
		var employeeIdselect = document.getElementById('employeeId');

		// Clear the old options
		employeeIdselect.options.length = 0;
		
		employeeIdselect.options.add(new Option("Employee", '0'));
		employeeIdselect.options[0].disabled = true;
		
		// Get the raw DOM object for the select box
		var commissionAssignIdselect = document.getElementById('commissionAssignId');

		// Clear the old options
		commissionAssignIdselect.options.length = 0;
		
		commissionAssignIdselect.options.add(new Option("Commission Assign", '0'));
		commissionAssignIdselect.options[0].disabled = true;
		
		if(deptId==0){
			return false;
		}
		
		$.ajax({
			type:'POST',
			url: myContextPath+"/fetchCommissionAssignAndEmployeeListByDepartment/"+deptId,
			async: false,
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(targetsAndEmployeeByDeptResponse){
				//setting target type list
				var commissionAssignList=targetsAndEmployeeByDeptResponse.commissionAssignList;		
				if(commissionAssignList!=null){
					var options, index, option;
					commissionAssignIdselect = document.getElementById('commissionAssignId');
					for (var i = 0, len = commissionAssignList.length; i < len; ++i) {
						var commissionAssign = commissionAssignList[i];
						commissionAssignIdselect.options.add(new Option(commissionAssign.commissionAssignName, commissionAssign.commissionAssignId));
					}
					
				}
				
				//setting employee list
				var employeeDetailsList=targetsAndEmployeeByDeptResponse.employeeDetailsList;
				if(employeeDetailsList!=null){
					var options, index, option;
					employeeIdselect = document.getElementById('employeeId');
					for (var i = 0, len = employeeDetailsList.length; i < len; ++i) {
						var employeeDetails = employeeDetailsList[i];
						if(employeeIdsAssigned.includes(employeeDetails.employee.employeeId)==false){
							employeeIdselect.options.add(new Option(employeeDetails.name, employeeDetails.employee.employeeId));	
						}
					}
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
	});
	
	$('#addTarget').click(function(){
		
		var departmentId=$('#departmentId').val();
		var employeeId=$('#employeeId').val();
		//var targetPeriodId=$('#targetPeriodId').val();
		var commissionAssignId=$('#commissionAssignId').val();
		var commissionAssignIdText=$('#commissionAssignId option:selected').text();
		var targetValueId=$('#targetValueId').val();
		
		if(departmentId==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Department', '2000', 'teal lighten-2');
			return false;
		}
		
		if(employeeId.length==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Employee', '2000', 'teal lighten-2');
			return false;
		}
		
		/*if(targetPeriodId==''){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Target Period', '2000', 'teal lighten-2');
			return false;
		}*/
		
		if(commissionAssignId==null){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Commission Assign', '2000', 'teal lighten-2');
			return false;
		}
		
		if(targetValueId=="0" || targetValueId=="" || targetValueId==undefined){
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Target Value', '2000', 'teal lighten-2');
			return false;
		}
		
		if(isNaN(targetValueId)){
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Numeric Target Value', '2000', 'teal lighten-2');
			return false;
		}
		
		if(alreadyHaveOrNot(commissionAssignId)){
			Materialize.Toast.removeAll();
			Materialize.toast('This Commission Assign Already Assigned', '2000', 'teal lighten-2');
			return false;
		}
		
		//add target type with values in array 
		commissionAssignList.push([commissionAssignId,commissionAssignIdText,targetValueId]);
		
		//reset form
		resetForm();
	});
	
	$('#saveTargetSubmit').click(function(){
		
		var departmentId=$('#departmentId').val();
		var employeeIds=$('#employeeId').val();
		//var targetPeriodId=$('#targetPeriodId').val();
		
		if(departmentId==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Department', '2000', 'teal lighten-2');
			return false;
		}
		
		if(employeeId.length==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Employee', '2000', 'teal lighten-2');
			return false;
		}
		
		/*if(targetPeriodId==''){
			Materialize.Toast.removeAll();
			Materialize.toast('Select Target Period', '2000', 'teal lighten-2');
			return false;
		}*/
		
		if(commissionAssignList.length==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Atlist one Target Assign to Employee', '2000', 'teal lighten-2');
			return false;
		}

		var $toastContent;
		

		if(mode=="add"){
			TargetsSaveRequest={
					targetWithValue : commissionAssignList,
					//targetPeriod : targetPeriodId,
			        employeeIds : employeeIds,
			        targetAssignedId : targetAssignedId
			};
			Materialize.Toast.removeAll();
		   $toastContent = $('<span>Are you Confirm?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="saveSubmit()">Submit</button>  '));
		   Materialize.toast($toastContent, "abc");
		}else{
			TargetsSaveRequest={
					targetWithValue : commissionAssignList,
					//targetPeriod : targetPeriodId,
			        employeeIds : employeeIds,
			        targetAssignedId : targetAssignedId
			};
			Materialize.Toast.removeAll();
		 $toastContent = $('<span>Do you want to save?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="saveSubmit()">Submit</button>  '));
		 Materialize.toast($toastContent, "abc");
		}
		
		
	});
	
	if(mode=="edit"){
		//for edit target details of employee	
		$('#departmentId').change();
		$('.preloader-wrapper').show();
		$('.preloader-background').show();
		setTimeout(function(){
					  var source2 = $("#employeeId");
						var v2=employeeId;
						source2.val(v2);
						source2.change();
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
				  }, 2000);
		/*setTimeout(function(){
					  var source2 = $("#targetPeriodId");
						var v2=targetPeriodId;
						source2.val(v2);
						source2.change();
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
				  }, 2000);*/
		
		commissionAssignList=commissionAssignListOld;
		
		resetForm();
	}else{
		//fetch employee which already assigned targets
		employeeIdsAssigned=[];//fetchEmployeeWhichAlreadyTargetAssigned();
	}
	
});

function saveSubmit(){
	saveTarget(TargetsSaveRequest);
}
function alreadyHaveOrNot(commissionAssignId){
	for(var i=0; i<commissionAssignList.length; i++){
		 var commissionAssign=commissionAssignList[i];
		 if(commissionAssign[0]==commissionAssignId){
			 return true;
		 }
	}
	return false;
}

function resetForm(){
	 $("#tblDataTargetRows").empty();
	 for(var i=0; i<commissionAssignList.length; i++){
		 var commissionAssign=commissionAssignList[i];
		 $('#tblDataTargetRows').append('<tr>'+
						                 '<td>'+(i+1)+'</td>'+
						                 '<td><button class="btn-flat" type="button" onclick="showCommissionAssign(' + commissionAssign[0] + ')"><b>'+commissionAssign[1]+'</b></button></td>'+
						                 '<td>'+commissionAssign[2]+'</td>'+
						                 '<td class="print-col">'+
						                 	'<button class="btn-flat" type="button" onclick="deleteRowConfirmation('+i+')"><i class="material-icons">clear</i></button>'+ 
						                 '</td>'+
						             '</tr>');
	 }
	 
	 var commissionAssignId = $("#commissionAssignId");
	 commissionAssignId.val(0);
	 commissionAssignId.change();
	 
	 $('#targetValueId').val('');
}
function deleteRowConfirmation(index){
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Remove Row?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deleterow('+index+')">Remove</button>  '));
	  Materialize.toast($toastContent, "abc");
}
function deleterow(index){
	Materialize.Toast.removeAll();
	commissionAssignList.splice(index, 1);
	resetForm();
}

function saveTarget(TargetsSaveRequest){
	console.log(JSON.stringify(TargetsSaveRequest));
	Materialize.Toast.removeAll();
	var partUrl="";
	if(mode=="add"){
		partUrl="save_target_assign";
	}else{
		partUrl="update_target_assign";
	}
	$.ajax({
		type:'POST',
		url: myContextPath+"/"+partUrl,
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify(TargetsSaveRequest),
		async: false,
		success : function(response){
			if(response.status=="Success"){
				if(mode=="add"){
					Materialize.Toast.removeAll();
					Materialize.toast('Target Assigned SuccessFully', '2000', 'teal lighten-2');
				}else{
					Materialize.Toast.removeAll();
					Materialize.toast('Update Target Assigned SuccessFully', '2000', 'teal lighten-2');
				}
				window.location.href=myContextPath+"/open_target_assign_list?departmentId=0";
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}

function fetchEmployeeWhichAlreadyTargetAssigned(){
	var employeeIds=[];
	$.ajax({
			type:'POST',
			url: myContextPath+"/fetch_employee_target_assigned",
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async: false,
			success : function(response){
				if(response.status=="Success"){
					employeeIds=response.employeeIds;
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				employeeIds=[];
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
	
	return employeeIds;	
}
function showCommissionAssign(commissionAssignId){
	$('#commissionAssignSlabId').empty();
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_commission_assign_slab/"+commissionAssignId,
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				$('.filterTab').find('li>a').removeClass('active');
				$('#commissionTab').click();
				var commissionAssignTargetSlabsList=response.commissionAssignTargetSlabsList;
				var commissionAssign=response.commissionAssign;
				if(commissionAssign.targetType.type=="Product Wise Target"){
					var commissionAssignProductsList=response.commissionAssignProductsList;
					var productNames="";
					$("#productsNamesBody").empty();
					var i=0;
					var tblData="";
					for(i=0; i<commissionAssignProductsList.length; i++){
						
						tblData+='<tr>'+
						'<td>'+(commissionAssignProductsList[i]!=undefined?commissionAssignProductsList[i].product.productName : '') +'</td>'+
						'<td>'+(commissionAssignProductsList[i+1]!=undefined?commissionAssignProductsList[i+1].product.productName : '')+'</td>'+
						'<td>'+(commissionAssignProductsList[i+2]!=undefined?commissionAssignProductsList[i+2].product.productName : '') +'</td>'+				
						'</tr>';
						i+=3;
						
					}
					
					
					$("#productsNamesBody").append(tblData);
					$(".filterTab").show();
					$('.productNameHide').show();
				}else{
					$(".filterTab").hide();
					$('#productNameId').text("");
					$('.productNameHide').hide();
				}
				for(var i=0; i<commissionAssignTargetSlabsList.length; i++){
					var targetValue; 
					
					if(commissionAssignTargetSlabsList[i].commissionType=="percentage"){
						targetValue='<td>'+commissionAssignTargetSlabsList[i].targetValue+'%</td>';
					}else{
						targetValue='<td>&#8377; '+commissionAssignTargetSlabsList[i].targetValue+'</td>';
					}
					
					$('#commissionAssignSlabId').append(
														'<tr>'+
													        '<td>'+(i+1)+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].fromValue+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].toValue+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].commissionType+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].commissionAssign.targetPeriod+'</td>'+
													        targetValue+
													    '</tr>');
				}
				$('#viewDetails').modal('open');
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}
