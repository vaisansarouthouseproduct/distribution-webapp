let totalAmount=0;
let totalAmountWithTax=0;
let totalAmountBeforeDisc=0;
let totalAmountWithTaxBeforeDisc=0;
let totalQuantity=0;
let discountAmountMain=0;
let discountPercentageMain=0;
let discountTypeMain="PERCENTAGE";
let isDiscountGiven=false;
let oldSupplierId=undefined;
let mainDiscountOnMRP="YES";
$(document).ready(function() {
	
	
//	discount process
	$('#discountMainCheck').prop('checked', false);
	$('.discountMainClass').hide();
	//whole discount checkbox event
	$('#discountMainCheck').click(function(){
		if($('#discountMainCheck').prop("checked") == true){
			if(productList.length==0){
				$('#discountMainCheck').prop('checked', false);
				Materialize.Toast.removeAll();
				Materialize.toast('Please add product first in Inventory cart', '3000', 'teal lighten-2');
				return false;
			}
		}

		discountMainCheckFunction();
		
	});
	
	function discountMainCheckFunction(){
		if($('#discountMainCheck').prop("checked") == true){
			$('.discountMainClass').show();
			findAmountAfterDisc();
			$('#discountMainId').val(0);
			$('#discountMainId').focus();
			$('#discountMainId').blur();
			$('#discountMainId').keyup();
			$('#percentageMainCheck').prop('checked', true);
			$('.editable').attr('disabled','disabled');
		}else{
			$('#discountMainId').val(0);
			$('#discountMainId').focus();
			$('#discountMainId').blur();
			$('#discountMainId').keyup();
			$('#percentageMainCheck').prop('checked', true);
			findAmountAfterDisc();
			$('.discountMainClass').hide();
			$('.editable').removeAttr('disabled');			
		}
	}
	
	$('#percentageMainCheck').click(function(){
		$('#discountMainId').keyup();
	});
	
	$('#discountOnMRPMainCheck').click(function(){
		$('#discountMainId').keyup();
	});
	
	$('#discountMainId').keyup(function(){
	
		var discMain=$('#discountMainId').val();		
		if(discMain==""){
			discMain=0;
		}
		discMain=parseFloat(discMain);

		if($('#percentageMainCheck').prop("checked") == true){
			if(discMain>100 || discMain<0){
				$('#discountMainId').val(0);
				$('#discountMainId').keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
				return false;
			}
		}else{
			
			// new code to handle the overall discount on MRP and unit Price
			if ($('#discountOnMRPMainCheck').prop("checked") == true) {
				if (discMain > totalAmountWithTaxBeforeDisc) {
					$('#discountMainId').val(0);
					$('#discountMainId').keyup();
					Materialize.Toast.removeAll();
					Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
					return false;
				}
			}else{
				if (discMain > totalAmountBeforeDisc) {
					$('#discountMainId').val(0);
					$('#discountMainId').keyup();
					Materialize.Toast.removeAll();
					Materialize.toast('Discount Amount Must be  Must be Less then Total Taxable Amount', '3000', 'teal lighten-2');
					return false;
				}
			}
			
			//old code
			/*
			 if(discMain>totalAmountWithTaxBeforeDisc){
				$('#discountMainId').val(0);
				$('#discountMainId').keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
				return false;
			}*/
		}

		findAmountAfterDisc();

		$('.editable').attr('disabled','disabled');
	
	});
	
						//quantity number only validation 
						$('#quantity').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						//reset fields after add product into table
						$('#resetAddQuantitySubmit').click(function()
						{
							count = 1;
							productList=[];
							$('#t1').empty();
							
							/*var source=$('#supplierId');
							source.val(0);
							source.change();*/
							
						// var source=$('#brandId');
							// source.val(0);
							// source.change();
							
							// var source=$('#categoryId');
							// source.val(0);
							// source.change();

							var source=$('#productId');
							source.val(0);
							source.change();
							
							//$('#productId').change();
							
							$('#hsncode').val('');									
							//$('#cgstPer').html(0);
							$('#igstPer').html(0);
							//$('#sgstPer').html(0);
							$('#totalAmt').val('');
							$('#amountWithTax').val('');
							//$('#cgst').val('');	
							$('#igst').val('');	
							//$('#sgst').val('');	
							
							$('#hsncode').change();
							//$('#cgstPer').change();
							$('#igstPer').change();
							//$('#sgstPer').change();
							$('#totalAmt').change();
							$('#amountWithTax').change();
							//$('#cgst').change();
							$('#igst').change();
							//$('#sgst').change();
							
							
							$('#AmtPerUnit').val(0);
							$('#AmtPerUnit').change();
							
							$('#oldquantity').val("");
							
							$('#quantity').val("");
							
							/*$('#paymentDate').val('');
							$('#paymentDate').change();*/
							
							// $('#finalTotalAmount').text(0);
							// $('#finalTotalAmountWithTax').text(0);

							TotalAmountAndTaxCalculate();

							$('#discountMainCheck').prop('checked', false);
							discountMainCheckFunction();
						});
						/**
						 * when supplier change all inventory data reset
						 * user need to newly add products  
						 * supplier suppling product add in product drop down
						 */
						$('#supplierId').change(function(){
							var supplierId=$('#supplierId').val();
							
							if(oldSupplierId==supplierId){
								return false;
							}
							
							$('#resetAddQuantitySubmit').click();
							
							$('#productId').empty();
							$("#productId").append('<option value="0">Choose Product</option>');
							if(supplierId==="0")
							{
								return false;
							}
							
							oldSupplierId=supplierId;
							
							$.ajax({
								url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									for(var i=0; i<data.length; i++)
									{								
										//alert(data[i].productId +"-"+ data[i].productName);
										$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
									}	
									//alert("done");
									$("#productId").change();
									$('#hsncode').val('');									
									//$('#cgstPer').html(0);
									$('#igstPer').html(0);
									//$('#sgstPer').html(0);
									$('#totalAmt').val('');
									$('#amountWithTax').val('');
									//$('#cgst').val('');	
									$('#igst').val('');	
									//$('#sgst').val('');	
									
									$('#hsncode').change();
									//$('#cgstPer').change();
									$('#igstPer').change();
									//$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									//$('#cgst').change();
									$('#igst').change();
									//$('#sgst').change();
									
									var source=$('#categoryId');
									source.val(0);
									source.change();
									
									var source=$('#brandId');
									source.val(0);
									source.change();							
									
									$('#oldquantity').val(data.currentQuantity);
									$('#oldquantity').change();
									
									$('#AmtPerUnit').val('');
									$('#AmtPerUnit').change();
									
									$('#AmtMRP').val('');
									$('#AmtMRP').change();
									
									$('#quantity').val("");
									
									$('#paymentDate').val();
									
									count = 1;
									productList=[];
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product List Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);
									}
							});
							
						});
						/**
						 * on product change 
						 * set mrp, unitprice of supplier against selected product
						 * set hsn code,old quantity from selected product
						 * if quantity is non zero then amount,tax,amount with tax
						 */
						$('#productId').change(function(){
							
							var productId=$('#productId').val();
							if(productId==="0")
							{
								$('#hsncode').val('');
								$('#hsncode').change();
								$('#AmtPerUnit').val('');
								$('#AmtPerUnit').change();
								$('#oldquantity').val('');
								$('#oldquantity').change();
								$('#totalAmt').val('');
								$('#totalAmt').change();
								$('#igst').val('');
								$('#igst').change();
								$('#igstPer').text('');
								$('#igstPer').change();
								$('#amountWithTax').val('');
								$('#amountWithTax').change();
								return false;
							}
							
							//AmtPerUnit
							
							var productId=$('#productId').val();
							var supplierId=$('#supplierId').val();
							
							if(productId==0){
								return false;
							}
							if(supplierId==0){
								return false;
							}
							
							//get supplier product info by product id
							$.ajax({
								url : myContextPath+"/fetchSupplierRateByProductIdamdSupplierId?supplierId="+supplierId+"&productId="+productId,
								dataType : "json",
								async:false,
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									var supplierproduct=data;
									var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.igst))/100)).toFixedVSS(0);
									//calculate unit price,igst,sgst,cgst basis of MRP,igst percentage
									var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.igst));
									
			    					$('#AmtMRP').val(parseFloat(correctAmoutWithTaxObj.mrp).toFixedVSS(2));
									$('#AmtMRP').change();
									
									$('#AmtPerUnit').val(parseFloat(data.supplierRate).toFixedVSS(2));
									$('#AmtPerUnit').change();
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Product Details Not Found!', '2000', 'teal lighten-3');
									/*$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
							//fetch product data by product id
							$.ajax({
								url : myContextPath+"/fetchProductByProductId?productId="+productId,
								dataType : "json",
								async:false,
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									var category=data.categories;
									//$('#cgstPer').html(category.cgst);
									$('#igstPer').html(category.igst);
									//$('#sgstPer').html(category.sgst);
									var enteredQuantity=$('#quantity').val();
									if(enteredQuantity!=="" && enteredQuantity!==undefined)
									{
										//alert($('#igstPer').html());
										

										var mrp=((parseFloat($('#AmtPerUnit').val()))+((parseFloat($('#AmtPerUnit').val())*parseFloat($('#igstPer').html()))/100)).toFixedVSS(0);
										//calculate unit price,igst,sgst,cgst basis of MRP,igst percentage
										var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat($('#igstPer').html()));
										
										$('#AmtMRP').val(correctAmoutWithTaxObj.mrp);
										$('#AmtPerUnit').val(correctAmoutWithTaxObj.unitPrice)
										
										var igstAmount=
											(((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixedVSS(2))
											-((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixedVSS(2))).toFixedVSS(2);
										
										//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixedVSS(2);
										//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
										//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
										$('#totalAmt').val((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixedVSS(2));
										$('#amountWithTax').val((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixedVSS(2));
										//$('#cgst').val(cgstAmount);
										$('#igst').val(igstAmount);
										//$('#sgst').val(sgstAmount);
									}
									else
									{
										//$('#cgst').val('');
										$('#igst').val('');
										//$('#sgst').val('');
										$('#amountWithTax').val('');
										$('#totalAmt').val('');
									}
									
									$('#hsncode').change();
									//$('#cgstPer').change();
									$('#igstPer').change();
									//$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									//$('#cgst').change();
									$('#igst').change();
									//$('#sgst').change();							
									
									$('#hsncode').val(data.categories.hsnCode);
									$('#hsncode').change();
									
									$('#oldquantity').val(data.currentQuantity);
									$('#oldquantity').change();
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Product Details Not Found!', '2000', 'teal lighten-3');
									/*$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
							
							
						});
						//change product list according selected brand
						$('#brandId').change(function(){
						
									var categoryId=$('#categoryId').val();
									var brandId=$('#brandId').val();
									var supplierId=$('#supplierId').val();
									
									if(supplierId==="0")
									{
										return false;
									}
									
									$('#productId').empty();
									$("#productId").append('<option value="0">Choose Product</option>');
									$.ajax({ 
										url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
										dataType : "json",
										async :false,
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },
										success : function(data) {
											
											for(var i=0; i<data.length; i++)
											{								
												//alert(data[i].productId +"-"+ data[i].productName);
												$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
											}	
											//alert("done");
											$("#productId").change();
											
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
										},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											  Materialize.Toast.removeAll();
											Materialize.toast('Product List Not Found!', '2000', 'teal lighten-3');
											/*$('#addeditmsg').modal('open');
							       	     	$('#msgHead').text("Message : ");
							       	     	$('#msg').text("Product List Not Found"); 
							       	     		setTimeout(function() 
												  {
								     					$('#addeditmsg').modal('close');
												  }, 1000);*/
											}
										});
							
						});
						//change product list according selected categories
						$('#categoryId').change(function(){
							
							var categoryId=$('#categoryId').val();
							var brandId=$('#brandId').val();
							var supplierId=$('#supplierId').val();
							if(supplierId==="0")
							{
								return false;
							}
							/*$.ajax({
								url : myContextPath+"/fetchCategories?categoriesId="+categoryId,
								dataType : "json",
								async :false,
								success : function(data) {
									var category=data;
									
									//$('#hsncode').val(category.hsnCode);
									$('#cgstPer').html(category.cgst);
									$('#igstPer').html(category.igst);
									$('#sgstPer').html(category.sgst);
									var enteredQuantity=$('#quantity').val();
									if(enteredQuantity!=="" && enteredQuantity!==undefined)
									{
										$('#totalAmt').val(enteredQuantity*parseFloat($('#AmtPerUnit').val()));
										var igstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.igst))/100;
										var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.cgst))/100;;
										var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.sgst))/100;;
										$('#amountWithTax').val(igstAmount+cgstAmount+sgstAmount+parseFloat($('#totalAmt').val()));
										$('#cgst').val(cgstAmount);
										$('#igst').val(igstAmount);
										$('#sgst').val(sgstAmount);
									}
									else
									{
										$('#cgst').val('');
										$('#igst').val('');
										$('#sgst').val('');
										$('#amountWithTax').val('');
										$('#totalAmt').val('');
									}
									
									$('#hsncode').change();
									$('#cgstPer').change();
									$('#igstPer').change();
									$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									$('#cgst').change();
									$('#igst').change();
									$('#sgst').change();
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									//alert("Error "+xhr.responseText);
									$('#hsncode').val('');									
									$('#cgstPer').html(0);
									$('#igstPer').html(0);
									$('#sgstPer').html(0);
									$('#totalAmt').val('');
									$('#amountWithTax').val('');
									$('#cgst').val('');	
									$('#igst').val('');	
									$('#sgst').val('');	
									
									$('#hsncode').change();
									$('#cgstPer').change();
									$('#igstPer').change();
									$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									$('#cgst').change();
									$('#igst').change();
									$('#sgst').change();
								}
							});*/
							
							
									
									$('#productId').empty();
									$("#productId").append('<option value="0">Choose Product</option>');
									$.ajax({ 
										url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
										dataType : "json",
										async :false,
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },
										success : function(data) {
											
											for(var i=0; i<data.length; i++)
											{								
												//alert(data[i].productId +"-"+ data[i].productName);
												$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
											}	
											//alert("done");
											$("#productId").change();
											 
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
										},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											  Materialize.Toast.removeAll();
											Materialize.toast('Product List Not Found!', '2000', 'teal lighten-3');
											/*$('#addeditmsg').modal('open');
							       	     	$('#msgHead').text("Message : ");
							       	     	$('#msg').text("Product List Not Found"); 
							       	     		setTimeout(function() 
												  {
								     					$('#addeditmsg').modal('close');
												  }, 1000);*/
											}
										});
							
								
							
						});
						/**
						 * when change quantity or enter quantity
						 * calculate amount ,tax, amount with tax and set on ui
						 */
						$('#quantity').on("keyup change blur",function(){
							
							var enteredQuantity=$('#quantity').val();
							if(enteredQuantity!=="" && enteredQuantity!==undefined && $('#igstPer').html()!=="" /*&& $('#cgstPer').html()!=="" && $('#sgstPer').html()!==""*/)
							{
								//alert($('#igstPer').html());
								
								
								var mrp=((parseFloat($('#AmtPerUnit').val()))+((parseFloat($('#AmtPerUnit').val())*parseFloat($('#igstPer').html()))/100)).toFixedVSS(0);
								//calculate unit price,igst,sgst,cgst basis of MRP,igst percentage
								var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat($('#igstPer').html()));
								
								$('#AmtMRP').val(correctAmoutWithTaxObj.mrp);
								$('#AmtPerUnit').val(correctAmoutWithTaxObj.unitPrice)
								
								var igstAmount=
									(((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixedVSS(2))
									-((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixedVSS(2))).toFixedVSS(2);
									//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixedVSS(2);
								//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
								//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
								$('#totalAmt').val((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixedVSS(2));
								$('#amountWithTax').val((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixedVSS(2));
								//$('#cgst').val(cgstAmount);
								$('#igst').val(igstAmount);
								//$('#sgst').val(sgstAmount);
							}
							else
							{
								//$('#cgst').val('');
								$('#igst').val('');
								//$('#sgst').val('');
								$('#amountWithTax').val('');
								$('#totalAmt').val('');
							}
							
							$('#hsncode').change();
							//$('#cgstPer').change();
							$('#igstPer').change();
							//$('#sgstPer').change();
							$('#totalAmt').change();
							$('#amountWithTax').change();
							//$('#cgst').change();
							$('#igst').change();
							//$('#sgst').change();
						});
						
						/**
						 * add product to table
						 * validation of product selection, quantity entered
						 * both add or update products manage by button name Add/Update
						 * product already exist validation
						 * add product to table
						 * calculate total product total taxable amount and amount  
						 */
						$("#productAddId").click( function() {
							
							var product=$("#productId").val();
							if(product=="0")
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Select Product!', '2000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Select Product");*/
			           	     	return false;
							}
							var category=$("#categoryId").val();
							/*if(category=="0")
							{
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Select category");
			           	     	return false;
							}*/
							var qua=$("#quantity").val();
							if(qua==="")
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Enter Quantity!', '2000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Enter Quantity");*/
			           	     	return false;
							}

							
							
							//if Button Value is Update then go to update method 
			            	var buttonStatus=$('#productAddButtonId').text();
			            	if(buttonStatus==="Update")
			            	{
			            		updaterow($('#currentUpdateProductId').val());
			            		return false;
			            	}
			            	$("#productAddId").html('<span id="productAddButtonId">Add</span><i class="material-icons right">add</i>');
			                var val = $("#productId").val(); 
			                var qty = $('#quantity').val();
			              	var rate = $('#AmtPerUnit').val();
			              	var rate_mrp = $('#AmtMRP').val();
			              	//alert(buttonStatus);
			              	if(val==0)
			              	{
			              		return false;
			              	}
			              	if(qty==0 || qty==="" || qty===undefined)
			              	{
			              		return false;
			              	}
			              	/* if(jQuery.inArray(val, productidlist) !== -1)
			                {	
			              		$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Select Warning");
			           	     	$('#msg').text("This Product is already added");               	     	
			                } */
			              	else
			           		{
			              		
			              		//alert(productList.size());
			              		/* for (let key of productList.keys()) {
			              		    alert(key+"-"+productList[key]);
			              		} */
			              		//alert(productList.entries());
			              		/* for (let entry of productList.entries()) {
			              			alert(entry[0]+"-"+ entry[1]);
			              		} */
			              		/* for (let [key, value] of productList.entries()) {
			              			alert(key+"-"+value);
			              			productList.remove(key);
			              		} */
				             	//alert(productList.key +"-"+ productList.value);
				             	 
			              		//already exist or not check
			              		for (var i=0; i<productList.length; i++) { 
			              			var value=productList[i]; 
			              			if(value[0]===val)
			              			{  
										Materialize.Toast.removeAll();
			              				Materialize.toast('This Product is already added!', '2000', 'teal lighten-3');
			              				/*$('#addeditmsg').modal('open');
			                   	     	$('#msgHead').text("Product Select Warning");
			                   	     	$('#msg').text("This Product is already added");*/ 
			                   	     	return false;
			              			}
			              		}
			              		
				             	/* $('#productratelistinput').val(productRateidlist);
				             	$('#productlistinput').val(productidlist); */
				                var text=$("#productId option:selected").text();
				                var vl=$("#productId option:selected").val();
								//alert(value+"-"+text);
								var rowCount = $('#productcart tr').length;
								
								//var tempCgst=$('#cgstPer').html();
								var tempIgst=$('#igstPer').html();
								//var tempSgst=$('#sgstPer').html();
								
								var mrp=((parseFloat(rate))+((parseFloat(rate)*parseFloat(tempIgst))/100)).toFixedVSS(0);
								//calculate unit price,igst,sgst,cgst basis of MRP,igst percentage
								var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(tempIgst));
								
								var ttl=( parseInt(qty) * parseFloat(rate) );
								var amtwithtax=(parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(qty)).toFixedVSS(2);// + ( (parseFloat(ttl)*parseFloat(tempCgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;
								
								var discount=$('#discountId').val();
								if(discount==''){
									discount=0;
								}
								discount=parseFloat(discount);
								if($('#percentageCheck').prop("checked") == true){
									if(discount>100 || discount<0){
										Materialize.Toast.removeAll();
										Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
										return false;
									}
								}else{
									// Discount on Amount
									if ($('#discountOnMRPCheck').prop("checked") == true) {
										if (discount > parseFloat(amtwithtax)) {
											Materialize.Toast.removeAll();
											Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
											return false;
										}
									} else {
										if (discount > parseFloat(ttl)) {
											Materialize.Toast.removeAll();
											Materialize.toast('Discount Amount Must be  Must be Less then Total Taxable Amount', '3000', 'teal lighten-2');
											return false;
										}
									}
									
									/*if(discount>parseFloat(amtwithtax)){
										Materialize.Toast.removeAll();
										Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
										return false;
									}*/
								}

								let discountAmount,discountPercentage,discountType,taxableAmount,totalAmount,discountOnMRP;
																											
									let discAmount,disc,percentageCheck=false;
									if($('#percentageCheck').prop("checked") == true){
										disc=discount;
										if ($('#discountOnMRPCheck').prop("checked") == true) {
											discountOnMRP="YES";
											discAmount=(amtwithtax*discount)/100;
										}else{
											discountOnMRP="NO";
											discAmount = (ttl * discount) / 100;
										}
									
										
										discountType="PERCENTAGE"
										discountPercentage=parseFloat(disc);
										discountAmount=parseFloat(discAmount);

										percentageCheck=true;
									}else{
										
										//new code 11-10-2019
										if ($('#discountOnMRPCheck').prop("checked") == true) {
											// discount on MRP
											discountOnMRP="YES";
											discAmount = discount;
											disc = (discAmount / amtwithtax) * 100;

											discountType = "AMOUNT"
											discountPercentage = parseFloat(disc);
											discountAmount = parseFloat(discAmount);

											percentageCheck = false;
										} else {
											// Discount on Unit price
											discountOnMRP="NO";
											discAmount = discount;
											disc = (discAmount / ttl) * 100;

											discountType = "AMOUNT"
											discountPercentage = parseFloat(disc);
											discountAmount = parseFloat(discAmount);

											percentageCheck = false;
										}
										//old Code
										/*discAmount=discount;
										disc=(discAmount/amtwithtax)*100;
										
										discountType="AMOUNT"
										discountPercentage=parseFloat(disc);
										discountAmount=parseFloat(discAmount);

										percentageCheck=false;*/
									}

									taxableAmount=parseFloat(ttl)-((parseFloat(ttl)*discountPercentage)/100);
									//totalAmount=parseFloat(amtwithtax)-discountAmount;
									totalAmount = parseFloat(amtwithtax) - ((parseFloat(amtwithtax) * discountPercentage) / 100);
									
								
								//[productId,quantity,unitPrice,mrp,totalAmountBeforeDiscount,discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
								var prdData=[val,qty,rate,rate_mrp,amtwithtax,discountAmount,discountPercentage,discountType,taxableAmount,totalAmount,discountOnMRP];
								productList.push(prdData);
								
								/*
								 <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>MRP</th>
                                    <!-- <th>Unit Price</th> -->
                                    <th>Quantity</th>
                                    <th>Disc.(%)</th>
                                    <th>Disc.Amt.</th>
                                    <th>Total(&#8377;)</th>
                                    <!-- <th>Edit</th> -->
                                    <th><i class="material-icons">delete</i></th>
                                </tr>
								 * */

								discountPercentage=parseFloat(discountPercentage).toFixedVSS(2);
								discountAmount=parseFloat(discountAmount).toFixedVSS(2);

								var discountField;
								if(percentageCheck){
									discountField='<td><input type="text" id="rowproductdiscper_'+count+'" value='+discountPercentage+' class="editable center qty"  style="width: 4em;" /></td>'+
									  '<td><span id="rowproductdiscamt_'+count+'">'+discountAmount+'</span></td>';
								}else{
									discountField='<td><span id="rowproductdiscper_'+count+'">'+discountPercentage+'</span></td>'+
									  '<td><input type="text" id="rowproductdiscamt_'+count+'" value='+discountAmount+' class="editable center qty"  style="width: 4em;" /></td>';
								}

								amtwithtax=parseFloat(amtwithtax).toFixedVSS(2);
								ttl=parseFloat(ttl).toFixedVSS(2);
								totalAmount=parseFloat(totalAmount).toFixedVSS(2);

								// append product details in table
				                $("#t1").append("<tr id='rowdel_" + count + "' >"+
					               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

					               				"<td id='rowproductname_" + count + "'>"+					               				
					               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+taxableAmount+"'>"+
					               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+totalAmount+"'>"+
					               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'>" +
					               				"<center><span id='tbproductname_" + count + "'>"+text+"</span></center>" +
					               				"</td>"+
					               				(	
				            						(discountOnMRP=="YES")?
				            								"<td id='rowproductunitprice_" + count + "'>" + rate + "</td>"
				            								:
				            								"<td id='rowproductunitprice_" + count + "' class='black-text'> <b>" + rate + " </b></td>"
				            					)+
				            					(	
				            								(discountOnMRP=="YES")?
				            										"<td id='rowproductmrp_" + count + "' class='black-text'> <b>" + rate_mrp + " </b></td>" 
				            										:
				            											"<td id='rowproductmrp_" + count + "'>" + rate_mrp + "</td>" 
				            					)+
					               				/*"<td id='rowproductmrp_" + count + "'>" + rate_mrp + "</td>"+*/					               				
					               				/*"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+*/
												"<td id='rowproductqty_" + count + "'>" + qty + "</td>"+
												"<td id='rowproducttaxableamt_" + count + "'>" + ttl + "</td>" +
												"<td id='rowproductamt_" + count + "'>" + amtwithtax + "</td>"+
					               				discountField+
					               				"<td id='rowproducttotalamt_" + count + "'>" + totalAmount + "</td>"+
					               				/*"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+*/
					               				"<td id='rowdelbutton_" + count + "'><button class='editable btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons'>delete</i></button></td>"+
					               				"</tr>");
								
													/* discount change event */
												   discountChangeEvents(count);
								
								count++;

								//final total taxable amount and amount find and set
								TotalAmountAndTaxCalculate();
			           		}
							  //alert(productList.entries());
							  
							
			              	
			              	//clearRecords();
			              	
			              	//reset product drop down to 0 position,quantity clear,mrp clear
			              	var source=$('#productId');
			            	source.val(0);
			            	source.change();
			            	
			            	$('#quantity').val("");
							$('#AmtMRP').val('');
							$('#AmtPerUnit').val('');
							$('#discountId').val('');
			            });
						/**
						 * final submission of add multiple inventory
						 * validation of product list have or not in table
						 * convert product details to string
						 * open payment and bill details modal
						 */
						$('#saveAddQuantitySubmit').click(function(){
							
						 	//alert(productlistinput);
			            	if(productList.length==0)
			           	 	{
								Materialize.Toast.removeAll();
			            		Materialize.toast('Select Atlist 1 Product!', '2000', 'teal lighten-3');
			            	/*	$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
			           	     $('#addeditmsg').modal('open');
			           	     $('#msgHead').text("Supplier Adding Message");
			           	     $('#msg').text("Select Atlist 1 Product");*/
			           	     return false;
			           	 	}		            	
			            				            	
							if($('#discountMainCheck').prop("checked") == true){
								if($('#discountMainId').val()==""){
									Materialize.Toast.removeAll();
									Materialize.toast('Enter Discount Value!', '2000', 'teal lighten-3');
								}
							}

			            	//alert(productList.entries());
			            	// var productIdList="";
			            	// for (var i=0; i<productList.length; i++) {
			            	// 	var value=productList[i];
			            	// 	productIdList=productIdList+value[0]+"-"+value[1]+",";
			          		// }
			            	// productIdList=productIdList.slice(0,-1)
			            	// //alert(productIdList);
			            	// $('#productlistinput').val(productIdList);
			            	//$('#resetAddQuantitySubmit').click(); 
			            	
						    $('#chooseDate').modal('open');			   			            	
			                      	          	   
			       		});
						/**
						 * validation of payment date,bill date,bill number
						 * payment date is must be after inventory added validation if on update inventory
						 */
						$("#submitModal").click(function(){
							var payDate=$('#paymentDate').val();
							if(payDate==="" || payDate==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorPaymentDate').text("Select Payment date");
								return false;
							}
							var billDate=$('#selectedBillDate').val();
							if(billDate==="" || billDate==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorBillDate').text("Select Bill date");
								return false;
							}
							var bill_number=$('#bill_number').val();
							if(bill_number==="" || bill_number==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorBillNumber').text("Enter Bill Number");
								return false;
							}
							if(!isEdit){
								var paymentDate=new Date($('#paymentDate').val()).setHours(0,0,0,0);
								var today=new Date().setHours(0,0,0,0);
								if(paymentDate < today)
								{
									//$('#addeditmsg').modal('open');
				           	     	//$('#msgHead').text("Product Add Cart Warning");
				           	     	$('#errorPaymentDate').text("Payment date must be today or after todays date");
									return false;
								}
							}else{
								var paymentDate=new Date($('#paymentDate').val()).setHours(0,0,0,0);
								
								var addedDate=new Date(invtAddedDate).setHours(0,0,0,0);
								
								
								if(paymentDate < addedDate)
								{
									$('#errorPaymentDate').text("Payment date must be same or after inventory added date");
									return false;
								}
							}
							/**
							 * Making Inventory Add Request
							 */
							var paymentDate=new Date($('#paymentDate').val()).setHours(0,0,0,0);
							let inventoryRequest = {};
							inventoryRequest.inventory={
								supplier:{
									supplierId : $('#supplierId').val()
								},
								totalAmount:totalAmount,
								totalAmountTax:totalAmountWithTax,
								totalQuantity:totalQuantity,
								billNumber:bill_number,
								billDate:new Date(billDate),
								inventoryPaymentDatetime:paymentDate,
								discountAmount:discountAmountMain,
								discountPercentage:discountPercentageMain,
								discountType:discountTypeMain,
								totalAmountBeforeDiscount:totalAmountBeforeDisc,
								totalAmountTaxBeforeDiscount:totalAmountWithTaxBeforeDisc,
								isDiscountGiven:isDiscountGiven,
								discountOnMRP:mainDiscountOnMRP
							}
							

							inventoryRequest.inventoryDetailsList=[];

							//[productId,quantity,unitPrice,mrp,totalAmountBeforeDiscount,
							//discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
							for (var i=0; i<productList.length; i++) {
			            		var data=productList[i];
								let productId=data[0],
								quantity=data[1],
								unitPrice=data[2],
								mrp=data[3],
								totalAmountBeforeDiscount=data[4],
								discountAmount=data[5],
								discountPercentage=data[6],
								discountType=data[7],
								taxableAmount=data[8],
								totalAmount=data[9],
								discountOnMRP=data[10];

								inventoryRequest.inventoryDetailsList.push(
									{
										//inventoryDetailsId,
										product:{
											productId : productId,
										},
										rate:mrp,
										quantity:quantity,
										amount:totalAmount,
										discountAmount:discountAmount,
										discountPercentage:discountPercentage,
										discountType:discountType,
										amountBeforeDiscount:totalAmountBeforeDiscount,
										discountOnMRP:discountOnMRP
									}
								);
			          		}						
							let url= myContextPath+"/saveInventory";
							let inventoryId = $('#inventoryId').val();
							if(inventoryId!=null && inventoryId!=undefined){
								inventoryRequest.inventory.inventoryTransactionId=inventoryId;
								url= myContextPath+"/editInventory"
							}
							
							/* request for inventory data add */
							$.ajax({
								url :url,
								type : "POST",
								data : JSON.stringify(inventoryRequest),
								headers: {
									'Content-Type': 'application/json'
								},
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									let msg ="";
									if(data.success==true){
										msg =data.msg;
										if(inventoryId!=null && inventoryId!=undefined){
											window.location.href=myContextPath+"/fetchInventoryReportView?range=currentMonth&supplierId=";
										}else{
											window.location.href=myContextPath+"/fetchProductListForInventory";
										}
									}else{
										msg="Something Went wrong";
									}
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();

									$('#addeditmsg').modal('open');
									$('#msgHead').text("Message : ");
									$('#msg').text(msg); 
										setTimeout(function() 
										{
												$('#addeditmsg').modal('close');
										}, 1000);
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Something Went Wrong"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);
								}
							});
						});
						
						
					});
/**
 * select product 
 * set quantity
 * make value of Add Button to Update 
 * @param id
 * @returns
 */
function editrow(id) {
	$('#currentUpdateProductId').val(id);
	var productId=$('#rowproductkey_'+id).val();
	//alert($('#rowproductrate_'+id).val());
		
	var source3 = $("#productId");
	var v3=productId;
	source3.val(v3);
	source3.change();
	
	$('#quantity').val($('#rowproductqty_'+id).text());
	$('#quantity').focus();
	$('#quantity').trigger('blur');
	$('#quantity').keyup();
	$("#productAddId").html('<span id="productAddButtonId">Update</span><i class="material-icons right">send</i>');
	
}
/**
 * delete product from productList as well as displaying table 
 * and recalculate final total taxable amount and amount
 * @param id
 * @returns
 */
function deleterow(id) {
	
    //alert('#rowproductkey_'+id);
    var removeItem=$('#rowproductkey_' + id).val();
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
  //  productList.remove(removeItem);
    
    // var productListTemp=[];
    // for(var i=0; i<productList.length; i++)
    // {
    // 	var value=productList[i];
    // 	if(value[0]!==removeItem)
    // 	{
    // 		productListTemp.push(productList[i]);
    // 	}
    // }
    // productList=[];
    // for(var i=0; i<productListTemp.length; i++)
    // {
    // 	productList.push(productListTemp[i]);
	// }
	
	/*
	remove product from list
	*/
	var index = productList.map(function(el) {
		return el[0];
	  }).indexOf(removeItem);
	productList.splice(index, 1);
    
    var rowCount = $('#t1 tr').length;
	//alert(rowCount);
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!==i)
		{
			//alert(count);
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
					   "</tr>"; */

					var index = productList.map(function(el) {
						return el[0];
					}).indexOf(($('#rowproductkey_'+i).val()));
				
					let productData = productList[index];
					let percentageCheck=false;
					if(productData[7]=="PERCENTAGE"){
						percentageCheck=true;
					}
					   
					let discountOnMRP="YES";
					if(productData[10]=="YES"){
						discountOnMRP="YES"
					}else{
						discountOnMRP="NO"	
					}
					var discountField;
					if(percentageCheck){
						discountField='<td><input type="text" id="rowproductdiscper_'+count+'" value="'+$('#rowproductdiscper_'+i).val()+'" class="editable center qty"  style="width: 4em;" /></td>'+
							'<td><span id="rowproductdiscamt_'+count+'">'+$('#rowproductdiscamt_'+i).text()+'</span></td>';
					}else{
						discountField='<td><span id="rowproductdiscper_'+count+'">'+$('#rowproductdiscper_'+i).text()+'</span></td>'+
							'<td><input type="text" id="rowproductdiscamt_'+count+'" value="'+$('#rowproductdiscamt_'+i).val()+'" class="editable center qty"  style="width: 4em;" /></td>';
					}

       				trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

               				"<td id='rowproductname_" + count + "'>" +
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+$('#rowproduct_total_amt_' + i).val()+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+$('#rowproduct_total_amt_with_tax' + i).val()+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center>" +
               				"</td>"+
               				(	
            						(discountOnMRP=="YES")?
            								"<td id='rowproductunitprice_" + count + "'>" + $('#rowproductunitprice_' + i).text() + "</td>" 
            								:
            								"<td id='rowproductunitprice_" + count + "'> <b>" + $('#rowproductunitprice_' + i).text() + "<b> </td>" 
            					)+
            					(	
            								(discountOnMRP=="YES")?
            										"<td id='rowproductmrp_" + count + "'> <b>" + $('#rowproductmrp_'+i).text() + " </b></td>"
            										:
            										"<td id='rowproductmrp_" + count + "'>" + $('#rowproductmrp_'+i).text() + "</td>" 
            					)+
               				/*"<td id='rowproductunitprice_" + count + "'>" + $('#rowproductunitprice_' + i).text() + "</td>" +
               				"<td id='rowproductmrp_" + count + "'>" + $('#rowproductmrp_'+i).text() + "</td>"+	*/
               				/*"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+*/
               				"<td id='rowproductqty_" + count + "'>" + $('#rowproductqty_'+i).text() + "</td>"+
               				"<td id='rowproducttaxableamt_" + count + "'>" + $('#rowproducttaxableamt_' + i).text() + "</td>" +
							"<td id='rowproductamt_" + count + "'>" + $('#rowproductamt_'+i).text() + "</td>"+
               				discountField+
               				"<td id='rowproducttotalamt_" + count + "'>" + $('#rowproducttotalamt_'+i).text() + "</td>"+
               				/*"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+*/
               				"<td id='rowdelbutton_" + count + "'><button class='editable btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons'>delete</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	TotalAmountAndTaxCalculate();

	for(let i=1; i<=count; i++){
		/* set discount input events */
		discountChangeEvents(i);
	}

	/* remove over all discount */
	if(productList.length==0){
		$('#discountMainCheck').prop('checked', false);
		$('.discountMainClass').hide();
		$('.editable').removeAttr('disabled');	

		Materialize.Toast.removeAll();
		Materialize.toast('Please add product first in Inventory cart', '3000', 'teal lighten-2');
	}

	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);
	//clearRecords();
}
/**
 * change value of Add Button to Add
 * update product details in productList 
 * update product details on table
 * recalculate total taxable amount and amount
 * @param id
 * @returns
 */
function updaterow(id) {
	$("#productAddId").html('<span id="productAddButtonId">Add</span><i class="material-icons right">add</i>');
    var val = $("#productId").val(); 
    var qty = $('#quantity').val();
  	var rate = $('#AmtPerUnit').val();
  	
  	var buttonStatus=$('#productAddButtonId').text();
  	var text=$("#productId option:selected").text();
    var vl=$("#productId option:selected").val();
  	
  	//alert(buttonStatus);
  	if(val==0)
  	{
  		return false;
  	}
  	if(qty==0)
  	{
  		return false;
  	}
	
  	//alert($('#rowproductkey_' + id).val());
  	var removeItem=$('#rowproductkey_' + id).val();

  	// var productListTemp=[];
    // for(var i=0; i<productList.length; i++)
    // {
    // 	var value=productList[i];
    // 	if(value[0]!==removeItem)
    // 	{
    // 		productListTemp.push(productList[i]);
    // 	}
    // }
    // productList=[];
    // for(var i=0; i<productListTemp.length; i++)
    // {
    // 	productList.push(productListTemp[i]);
	// }
	
		/*
	remove product from list
	*/
	var index = productList.map(function(el) {
		return el[0];
	  }).indexOf(removeItem);
	let productData = productList[index];
	let percentageCheck=false;
	if(productData[7]=="PERCENTAGE"){
		percentageCheck=true;
	}
	productList.splice(index, 1);
	  
  	
    //alert('#rowproductkey_'+id);
    
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
    
    var rowCount = $('#t1 tr').length;
	//alert(rowCount);
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!=i)
		{
			//alert("predata");
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
					   "</tr>"; */
					   
					var discountField;
					if(percentageCheck){
						discountField='<td><input type="text" id="rowproductdiscper_'+count+'" value='+$('#rowproductdiscper_'+i).text()+' class="editable center qty"  style="width: 4em;" /></td>'+
							'<td><span id="rowproductdiscamt_'+count+'">'+$('#rowproductdiscamt_'+i).text()+'</span></td>';
					}else{
						discountField='<td><span id="rowproductdiscper_'+count+'">'+$('#rowproductdiscper_'+i).text()+'</span></td>'+
							'<td><input type="text" id="rowproductdiscamt_'+count+'" value='+$('#rowproductdiscamt_'+i).text()+' class="editable center qty"  style="width: 4em;" /></td>';
					}

       				trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

               				"<td id='rowproductname_" + count + "'>" +
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+$('#rowproduct_total_amt_' + i).val()+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+$('#rowproduct_total_amt_with_tax' + i).val()+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center>" +
               				"</td>"+
               				"<td id='rowproductunitprice_" + count + "'>" + $('#rowproductunitprice_' + i).text() + "</td>" +
               				"<td id='rowproductmrp_" + count + "'>" + $('#rowproductmrp_'+i).text() + "</td>"+
               				/*"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+*/
               				"<td id='rowproductqty_" + count + "'>" + $('#rowproductqty_'+i).text() + "</td>"+
               				"<td id='rowproducttaxableamt_" + count + "'>" + $('#rowproducttaxableamt_' + i).text() + "</td>" +
               				"<td id='rowproductamt_" + count + "'>" + $('#rowproductamt_'+i).text() + "</td>"+
               				discountField+
               				"<td id='rowproducttotalamt_" + count + "'>" + $('#rowproducttotalamt_'+i).text() + "</td>"+
               				/*"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+*/
               				"<td id='rowdelbutton_" + count + "'><button class='editable btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons'>delete</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		else
		{
			//alert("newdata");
			//var tempCgst=$('#cgstPer').html();
			var tempIgst=parseFloat($('#igstPer').html()).toFixedVSS(2);
			//var tempSgst=$('#sgstPer').html();
			var ttl=( parseInt(qty) * parseFloat(rate) );
			//var amtwithtax=(parseFloat(ttl) + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 )).toFixedVSS(2);// + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;
			

			var mrp=(parseFloat(rate)+((rate*parseFloat(tempIgst))/100)).toFixedVSS(0);
			var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(tempIgst));

			var igstAmount=correctAmoutWithTaxObj.igst;//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixedVSS(2);
			//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
			//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
			var amtwithtax=((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(qty)).toFixedVSS(2));
			//$('#cgst').val(cgstAmount);
			//$('#igst').val(igstAmount);
			
			let discountAmount,discountPercentage,discountType,taxableAmount,totalAmount,discountOnMRP;
			
			var discount=$('#discountId').val();
			if(discount==''){
				discount=0;
			}
			discount=parseFloat(discount);
				if($('#percentageCheck').prop("checked") == true){
					if(discount>100 || discount<0){
						Materialize.Toast.removeAll();
						Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
						return false;
					}
				}else{
					
					// Discount on Amount
					if ($('#discountOnMRPCheck').prop("checked") == true) {
						if (discount > parseFloat(amtwithtax)) {
							Materialize.Toast.removeAll();
							Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
							return false;
						}
					} else {
						if (discount > parseFloat(ttl)) {
							Materialize.Toast.removeAll();
							Materialize.toast('Discount Amount Must be  Must be Less then Total Taxable Amount', '3000', 'teal lighten-2');
							return false;
						}
					}
					//old code
					/*if(discount>parseFloat(amtwithtax)){
						Materialize.Toast.removeAll();
						Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
						return false;
					}*/
				}

				let discAmount,disc,percentageCheck=false;
				if($('#percentageCheck').prop("checked") == true){
					disc=discount;
					if ($('#discountOnMRPCheck').prop("checked") == true) {
						discountOnMRP="YES";
						discAmount = (amtwithtax * discount) / 100;
					}else{
						discountOnMRP="NO";
						discAmount = (ttl * discount) / 100;
					}
					//discAmount=(amtwithtax*discount)/100;
					
					discountType="PERCENTAGE"
					discountPercentage=parseFloat(disc);
					discountAmount=parseFloat(discAmount);

					percentageCheck=true;
				}else{
					
					//new code 07-10-2019
					if ($('#discountOnMRPCheck').prop("checked") == true) {
						// discount on MRP
						discountOnMRP="YES";
						discAmount = discount;
						disc = (discAmount / amtwithtax) * 100;

						discountType = "AMOUNT"
						discountPercentage = parseFloat(disc);
						discountAmount = parseFloat(discAmount);

						percentageCheck = false;
					} else {
						// Discount on Unit price
						discountOnMRP="NO";
						discAmount = discount;
						disc = (discAmount / ttl) * 100;

						discountType = "AMOUNT"
						discountPercentage = parseFloat(disc);
						discountAmount = parseFloat(discAmount);

						percentageCheck = false;
					}
					// old code
					/*discAmount=discount;
					disc=(discAmount/amtwithtax)*100;
					
					discountType="AMOUNT"
					discountPercentage=parseFloat(disc);
					discountAmount=parseFloat(discAmount);

					percentageCheck=false;
*/				}

				taxableAmount=parseFloat(ttl)-((parseFloat(ttl)*discountPercentage)/100);
				//totalAmount=parseFloat(amtwithtax)-discountAmount;
				totalAmount = parseFloat(amtwithtax) - ((parseFloat(amtwithtax) * discountPercentage) / 100);
				

			//[productId,quantity,unitPrice,mrp,totalAmountBeforeDiscount,discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
			var prdData=[val,qty,rate,mrp,amtwithtax,discountAmount,discountPercentage,discountType,taxableAmount,totalAmount,discountOnMRP];
			productList.push(prdData);

			discountPercentage=parseFloat(discountPercentage).toFixedVSS(2);
			discountAmount=parseFloat(discountAmount).toFixedVSS(2);

			var discountField;
			if(percentageCheck){
				discountField='<td><input type="text" id="rowproductdiscper_'+count+'" value='+discountPercentage+' class="editable center qty"  style="width: 4em;" /></td>'+
					'<td><span id="rowproductdiscamt_'+count+'">'+discountAmount+'</span></td>';
			}else{
				discountField='<td><span id="rowproductdiscper_'+count+'">'+discountPercentage+'</span></td>'+
					'<td><input type="text" id="rowproductdiscamt_'+count+'" value='+discountAmount+' class="editable center qty"  style="width: 4em;" /></td>';
			}
			
			amtwithtax=parseFloat(amtwithtax).toFixedVSS(2);
			ttl = parseFloat(ttl).toFixedVSS(2);

			trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
               				"<td id='rowproductname_" + count + "'>"+					               				
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+taxableAmount+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+totalAmount+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+text+"</span></center>" +
               				"</td>"+
               				"<td id='rowproductunitprice_" + count + "'>" + correctAmoutWithTaxObj.unitPrice + "</td>" +
               				"<td id='rowproductmrp_" + count + "'>" + correctAmoutWithTaxObj.mrp + "</td>"+
               				/*"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+*/
							"<td id='rowproductqty_" + count + "'>" + qty + "</td>"+
							"<td id='rowproducttaxableamt_" + count + "'>" + ttl + "</td>" +
							"<td id='rowproductamt_" + count + "'>" + amtwithtax + "</td>"+
               				discountField+
							"<td id='rowproducttotalamt_" + count + "'>" + totalAmount + "</td>"+
               				/*"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+*/
               				"<td id='rowdelbutton_" + count + "'><button class='editable btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	TotalAmountAndTaxCalculate();
  	//clearRecords();
  	var source=$('#productId');
	source.val(0);
	source.change();
	$('#quantity').val("");
	$('#AmtMRP').val('')
	$('#AmtPerUnit').val('')
	$('#discountId').val('');
	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);
	//clearRecords();
}
/**+
 * calculate total taxable amount and amount using table product data
 * @returns
 */
function TotalAmountAndTaxCalculate()
{
	totalAmountBeforeDisc=0;
	totalAmountWithTaxBeforeDisc=0;
	totalQuantity=0;
	let totalProductsDiscount=0,totalBeforeAllDiscount=0;
	let totalTaxableAmtBeforeAllDiscount=0;
	for(var i=1; i<count; i++)
	{
		totalAmountBeforeDisc=(parseFloat(totalAmountBeforeDisc) + parseFloat($('#rowproduct_total_amt_'+i).val())).toFixedVSS(2);
		totalAmountWithTaxBeforeDisc=(parseFloat(totalAmountWithTaxBeforeDisc) + parseFloat($('#rowproduct_total_amt_with_tax'+i).val())).toFixedVSS(2);
		totalQuantity=(parseInt(totalQuantity) + parseInt($('#rowproductqty_'+i).text())).toFixedVSS(2);
		totalTaxableAmtBeforeAllDiscount=(parseFloat(totalTaxableAmtBeforeAllDiscount) + parseFloat($('#rowproducttaxableamt_' + i).text())).toFixedVSS(2);
		totalBeforeAllDiscount=(parseFloat(totalBeforeAllDiscount) + parseFloat($('#rowproductamt_'+i).text())).toFixedVSS(2);
		totalProductsDiscount=(parseFloat(totalProductsDiscount) + parseFloat(
			($('#rowproductdiscamt_'+i).text()=="")?$('#rowproductdiscamt_'+i).val():$('#rowproductdiscamt_'+i).text()
			)).toFixedVSS(2);
	}

//	$('#finalTotalAmount').text(parseFloat(totalAmountBeforeDisc).toFixedVSS(2));
//	$('#finalTotalAmountWithTax').text(parseFloat(totalAmountWithTaxBeforeDisc).toFixedVSS(2));
	
	$("#totalQuantityId").text(totalQuantity);
	$("#totalTaxableAmtBeforeAllDiscountId").text(totalTaxableAmtBeforeAllDiscount);
    $("#totalBeforeAllDiscountId").text(totalBeforeAllDiscount);
    $("#totalProductsDiscountId").text(totalProductsDiscount);
    $("#totalAfterDiscountAmountId").text(totalAmountWithTaxBeforeDisc);

	findAmountAfterDisc();
}


let findAmountAfterDisc = ()=>{
	totalAmount=0;
	totalAmountWithTax=0;

	if($('#discountMainCheck').prop("checked") == true){
		var discMain=$('#discountMainId').val();		
		if(discMain==""){
			discMain=0;
		}
		
		if($('#percentageMainCheck').prop("checked") == true){	
			
			totalAmount=parseFloat(totalAmountBeforeDisc)-((parseFloat(totalAmountBeforeDisc)*parseFloat(discMain))/100);
			totalAmountWithTax=parseFloat(totalAmountWithTaxBeforeDisc)-((parseFloat(totalAmountWithTaxBeforeDisc)*parseFloat(discMain))/100);

			discountTypeMain="PERCENTAGE";
			if ($('#discountOnMRPMainCheck').prop("checked") == true) {
				discountAmountMain=(parseFloat(discMain)*parseFloat(totalAmountWithTaxBeforeDisc))/100;	
			}else{
				discountAmountMain=(parseFloat(discMain)*parseFloat(totalAmount))/100;
			}
			//discountAmountMain=(parseFloat(discMain)*parseFloat(totalAmountWithTaxBeforeDisc))/100;
			discountPercentageMain=parseFloat(discMain);
			$("#discountMainId").parent().find('label').text('Discount Percent');
			$("#discountCalculated").parent().find('label').text('Discount Amount');
			$("#discountCalculated").val(parseFloat(discountAmountMain).toFixedVSS(2)).change();
			
		}else{
			//Sachin 11-10-2019
			if ($('#discountOnMRPMainCheck').prop("checked") == true) {
				
				mainDiscountOnMRP="YES";
				totalAmountWithTax = parseFloat(totalAmountWithTaxBeforeDisc) - (parseFloat(discMain));

				discountTypeMain = "AMOUNT";
				discountAmountMain = parseFloat(discMain);
				discountPercentageMain = (parseFloat(discMain) / parseFloat(totalAmountWithTaxBeforeDisc)) * 100;

				totalAmount = parseFloat(totalAmountBeforeDisc) - ((parseFloat(totalAmountBeforeDisc) * parseFloat(discountPercentageMain)) / 100);

			}else{
				mainDiscountOnMRP="NO";
				totalAmount = parseFloat(totalAmountBeforeDisc) - (parseFloat(discMain));
				//totalAmountWithTax = parseFloat(totalAmountWithTaxBeforeDisc) - (parseFloat(discMain));

				discountTypeMain = "AMOUNT";
				discountAmountMain = parseFloat(discMain);
				discountPercentageMain = (parseFloat(discMain) / parseFloat(totalAmount)) * 100;

				totalAmountWithTax = parseFloat(totalAmountWithTaxBeforeDisc) - ((parseFloat(totalAmountWithTaxBeforeDisc) * parseFloat(discountPercentageMain)) / 100);
				//totalAmount = parseFloat(totalAmountBeforeDisc) - ((parseFloat(totalAmountBeforeDisc) * parseFloat(discountPercentageMain)) / 100);

			}
			// old code
			/*totalAmountWithTax=parseFloat(totalAmountWithTaxBeforeDisc)-(parseFloat(discMain));

			discountTypeMain="AMOUNT";
			discountAmountMain=parseFloat(discMain);
			discountPercentageMain=(parseFloat(discMain)/parseFloat(totalAmountWithTaxBeforeDisc))*100;*/

			totalAmount=parseFloat(totalAmountBeforeDisc)-((parseFloat(totalAmountBeforeDisc)*parseFloat(discountPercentageMain))/100);
			$("#discountMainId").parent().find('label').text('Discount Amount');
			$("#discountCalculated").parent().find('label').text('Discount Percent');
			$("#discountCalculated").val(parseFloat(discountPercentageMain).toFixedVSS(2)).change();
		}
		isDiscountGiven=true;
	}else{
		totalAmount=totalAmountBeforeDisc;
		totalAmountWithTax=totalAmountWithTaxBeforeDisc;
		isDiscountGiven=false;

		$("#discountMainId").parent().find('label').text('Discount Percent');
		$("#discountCalculated").parent().find('label').text('Discount Amount');
		$("#discountCalculated").val(parseFloat(0).toFixedVSS(2)).change();
	}

	
	if(isNaN(totalAmount)){
		totalAmount=0
	}
	if(isNaN(totalAmountWithTax)){
		totalAmountWithTax=0;
	}
	
	$('#netfinalTotalAmount').text(parseFloat(totalAmount).toFixedVSS(2));
	$('#netfinalTotalAmountWithTax').text(Math.round(parseFloat(totalAmountWithTax).toFixedVSS(2)));
	
}

let discountChangeEvents=(count)=>{
	/**
	 * only number allowed without decimal 
	 */
	$(`#rowproductdiscper_${count}, #rowproductdiscamt_${count}`).off("keypress");
	$(`#rowproductdiscper_${count}, #rowproductdiscamt_${count}`).keypress(function( event ){
		var key = event.which;						    
		if( ! ( key >= 48 && key <= 57 || key === 13) )
			event.preventDefault();
	});
	/**
	 * disc change event in table rows
	 * then change total amount according mrp*qty
	 */
	$(`#rowproductdiscper_${count}, #rowproductdiscamt_${count}`).off("keyup");
	$(`#rowproductdiscper_${count}, #rowproductdiscamt_${count}`).keyup(function(){
		var id=$(this).attr('id')
		var count=id.split('_')[1];
		
		let productId = $("#rowproductkey_" + count).val();
		var index = productList.map(function(el) {
			return el[0];
		}).indexOf(productId);
		productData = productList[index];

		var discount=0;
		if(productData[7]=="PERCENTAGE"){
			discount=$('#rowproductdiscper_'+count).val();
		}else{
			discount=$('#rowproductdiscamt_'+count).val();
		}

		if(discount==''){
			discount=0;
		}
		discount=parseFloat(discount);

		var amtwithtax=productData[4];
		var ttl=( parseInt(productData[1]) * parseFloat(productData[2]) );
			
		let discountAmount,discountPercentage,discountType,taxableAmount,totalAmount,discountOnMRP;
												
		if(productData[7]=="PERCENTAGE"){
			if(discount>100 || discount<0){
				$(this).val(0);
				$(this).keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
				return false;
			}
		}else{
			// Discount on Amount
			if ($('#discountOnMRPCheck').prop("checked") == true) {
				if (discount > parseFloat(amtwithtax)) {
					Materialize.Toast.removeAll();
					Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
					return false;
				}
			} else {
				if (discount > parseFloat(ttl)) {
					Materialize.Toast.removeAll();
					Materialize.toast('Discount Amount Must be  Must be Less then Total Taxable Amount', '3000', 'teal lighten-2');
					return false;
				}
			}
			/*if(discount>parseFloat(amtwithtax)){
				$(this).val(0);
				$(this).keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
				return false;
			}*/
		}
		
		let discAmount,disc,percentageCheck=false;
		if(productData[7]=="PERCENTAGE"){
			disc=discount;
			if(productData[10]=="YES"){
				discountOnMRP="YES";
				discAmount = (amtwithtax * discount) / 100;
			}else{
				discountOnMRP="NO";
				discAmount = (ttl * discount) / 100;
			}
			//discAmount=(amtwithtax*discount)/100;
			
			discountType="PERCENTAGE"
			discountPercentage=parseFloat(disc);
			discountAmount=parseFloat(discAmount);

			percentageCheck=true;
		}else{
			//new code 07-10-2019
			//if ($('#discountOnMRPCheck').prop("checked") == true) {
			if(productData[10]=="YES"){
				// discount on MRP
				discountOnMRP="YES";
				discAmount = discount;
				disc = (discAmount / amtwithtax) * 100;

				discountType = "AMOUNT"
				discountPercentage = parseFloat(disc);
				discountAmount = parseFloat(discAmount);

				percentageCheck = false;
			} else {
				// Discount on Unit price
				discountOnMRP="NO";
				discAmount = discount;
				disc = (discAmount / ttl) * 100;

				discountType = "AMOUNT"
				discountPercentage = parseFloat(disc);
				discountAmount = parseFloat(discAmount);

				percentageCheck = false;
			}
			
			//old code
			/*discAmount=discount;
			disc=(discAmount/amtwithtax)*100;
			
			discountType="AMOUNT"
			discountPercentage=parseFloat(disc);
			discountAmount=parseFloat(discAmount);

			percentageCheck=false;*/
		}

		

		taxableAmount=parseFloat(ttl)-((parseFloat(ttl)*discountPercentage)/100);
		//totalAmount=parseFloat(amtwithtax)-discountAmount;
		totalAmount = parseFloat(amtwithtax) - ((parseFloat(amtwithtax) * discountPercentage) / 100);
		
		productList.splice(index, 1);
		//[productId,quantity,unitPrice,mrp,totalAmountBeforeDiscount,discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
		var prdData=[
			productData[0],
			productData[1],
			productData[2],
			productData[3],
			productData[4],
			discountAmount,
			discountPercentage,
			productData[7],
			taxableAmount,
			totalAmount,
			discountOnMRP
		];
		productList.push(prdData);

		discountPercentage=parseFloat(discountPercentage).toFixedVSS(2);
		discountAmount=parseFloat(discountAmount).toFixedVSS(2);

		if(percentageCheck){
			$("#rowproductdiscamt_" + count).text(discountAmount);
		}else{
			$("#rowproductdiscper_" + count).text(discountPercentage);
		}
		
		$("#rowproduct_total_amt_" + count).val(taxableAmount);
		$("#rowproduct_total_amt_with_tax" + count).val(totalAmount);
		$("#rowproducttotalamt_" + count).text(totalAmount.toFixedVSS(2));
						
		TotalAmountAndTaxCalculate();
		
	});
}

/*function clearRecords()
{
	var source=$('#supplierId');
	source.val(0);
	source.change();
	
	var source=$('#brandId');
	source.val(0);
	source.change();
	
	var source=$('#categoryId');
	source.val(0);
	source.change();

	var source=$('#productId');
	source.val(0);
	source.change();
	
	$('#hsncode').val('');									
	//$('#cgstPer').html(0);
	$('#igstPer').html(0);
	//$('#sgstPer').html(0);
	$('#totalAmt').val('');
	$('#amountWithTax').val('');
	//$('#cgst').val('');	
	$('#igst').val('');	
	//$('#sgst').val('');	
	
	$('#hsncode').change();
	//$('#cgstPer').change();
	$('#igstPer').change();
	//$('#sgstPer').change();
	$('#totalAmt').change();
	$('#amountWithTax').change();
	//$('#cgst').change();
	$('#igst').change();
	//$('#sgst').change();
	
	
	$('#AmtPerUnit').val(0);
	$('#AmtPerUnit').change();
	
	$('#oldquantity').val("");
	
	$('#quantity').val("");
	
}*/