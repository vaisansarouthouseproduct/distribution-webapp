
var currentTargetDaily=false,nextTargetDaily=false,
	currentTargetWeekly=false,nextTargetWeekly=false,
	currentTargetMonthly=false,nextTargetMonthly=false,
	currentTargetYearly=false,nextTargetYearly=false;
var departmentId=0;
$(document).ready(function() {
	$(".filterTab").hide();
	$('#departmentId').change(function(){		
		departmentId=parseInt($('#departmentId').val());
	});
	
	
	
	
});

/*function fetchTargetList(departmentId){
	Materialize.Toast.removeAll();
	
	var TargetListsRequest={
			departmentId
	};
	var table = $('#tblDataTarget').DataTable();
	table.clear().draw();
	
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_target_assign_list",
		headers : {
			'Content-Type' : 'application/json'
		},
		data:JSON.stringify(TargetListsRequest),
		async: false,
		success : function(TargetListsResponse){
			if(TargetListsResponse.status=="Success"){
				var targetLists=TargetListsResponse.targetLists;
				
				for(var i=0; i<targetLists.length; i++){
					
					var targetAssign=targetLists[i].targetAssign;
					var targetAssignTargetList=targetLists[i].targetAssignTargetList;
					
					var targetTypeList='<ol type="1">';					
					for(var j=0; j<targetAssignTargetList.length; j++){
						targetTypeList+='<li  align="left">'+
										'<a class="btn-flat" type="button" onclick="showCommissionAssign(' + targetAssignTargetList[j].commissionAssign.commissionAssignId + ')"><b>'+targetAssignTargetList[j].commissionAssign.commissionAssignName+'</b></a>'
										+'-<font color="blue">'+targetAssignTargetList[j].targetValue+'</font></li>';
					}
					targetTypeList+='</ol>';
					
					var action='<a class="btn-flat" href="'+myContextPath+'/edit_target?targetAssignId='+targetAssign.targetAssignId+'"><i class="material-icons">edit</i></a>&nbsp;&nbsp;'+
				    		   '<button class="btn-flat" type="button" onclick="deleteConfirmation('+targetAssign.targetAssignId+',\''+targetAssign.targetPeriod+'\')"><i class="material-icons">clear</i></button>';
					table.row.add([
									(i+1),
									targetAssign.employeeDetails.name,
									targetTypeList,
									action
								  ]).draw(false);
					
				}
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('Target Assign List Not Found', '2000', 'red lighten-2');
			}
		},
		error: function(xhr, status, error){
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}*/

function deleteConfirmation(targetAssignId,targetPeriod){
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deleteTargetAssign('+targetAssignId+',\''+targetPeriod+'\')">Yes</button>  '));
	Materialize.toast($toastContent, "abc");
}

function deleteTargetAssign(targetAssignId,targetPeriod){
	$.ajax({
		type:'POST',
		url: myContextPath+"/delete_target_assign",
		headers : {
			'Content-Type' : 'application/json'
		},
		data:JSON.stringify({
							 targetAssignId : targetAssignId,
							 deleteRegular  : "No"
							}),
		async: false,
		success : function(response){
			if(response.status=="Success"){
				Materialize.Toast.removeAll();
				Materialize.toast('Target Assign Deleted SuccessFully', '2000', 'red lighten-2');
				
				//refresh table after delete
				if(targetPeriod=="Daily"){
					$('#dailywork').click();
				}else if(targetPeriod=="Weekly"){
					$('#weeklywork').click();
				}else if(targetPeriod=="Monthly"){
					$('#monthlywork').click();
				}else if(targetPeriod=="Yearly"){
					$('#yearlywork').click();
				}
				
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('Target Assign List Not Found', '2000', 'teal lighten-2');
			}
		},
		error: function(xhr, status, error){
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}
function showCommissionAssign(commissionAssignId){
	$('#commissionAssignSlabId').empty();
	$.ajax({
		type:'POST',
		url: myContextPath+"/fetch_commission_assign_slab/"+commissionAssignId,
		async: false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(response){
			if(response.status=="Success"){
				$('.filterTab').find('li>a').removeClass('active');
				$('#commissionTab').click();
				var commissionAssignTargetSlabsList=response.commissionAssignTargetSlabsList;
				var commissionAssign=response.commissionAssign;
				if(commissionAssign.targetType.type=="Product Wise Target"){
					var commissionAssignProductsList=response.commissionAssignProductsList;
					var productNames="";
					var i=0;
					$("#productsNamesBody").empty();
					var tblData="";
					for(i=0; i<commissionAssignProductsList.length; i++){
						tblData+='<tr>'+
						'<td>'+(commissionAssignProductsList[i]!=undefined?commissionAssignProductsList[i].product.productName : '') +'</td>'+
						'<td>'+(commissionAssignProductsList[i+1]!=undefined?commissionAssignProductsList[i+1].product.productName : '')+'</td>'+
						'<td>'+(commissionAssignProductsList[i+2]!=undefined?commissionAssignProductsList[i+2].product.productName : '') +'</td>'+				
						'</tr>';
						i+=3;	
					}
					$("#productsNamesBody").append(tblData);
					$(".filterTab").show();
					$('.productNameHide').show();
					
				}else{
					$(".filterTab").hide();
					$('#productNameId').text("");
					$('.productNameHide').hide();
					
				}
				for(var i=0; i<commissionAssignTargetSlabsList.length; i++){
					var targetValue; 
					
					if(commissionAssignTargetSlabsList[i].commissionType=="percentage"){
						targetValue='<td>'+commissionAssignTargetSlabsList[i].targetValue+'%</td>';
					}else{
						targetValue='<td>&#8377; '+commissionAssignTargetSlabsList[i].targetValue+'</td>';
					}
					
					$('#commissionAssignSlabId').append(
														'<tr>'+
													        '<td>'+(i+1)+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].fromValue+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].toValue+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].commissionType+'</td>'+
													        '<td>'+commissionAssignTargetSlabsList[i].commissionAssign.targetPeriod+'</td>'+
													        targetValue+
													    '</tr>');
				}
				$('#viewDetails').modal('open');
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
}