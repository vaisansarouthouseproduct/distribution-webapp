var targetPeriodMain;
$(document).ready(function() {
	  
	  $('#departmentId').change(function(){
		  showFilterOption("");
			var departmentId=$('#departmentId').val();
			
			// Get the raw DOM object for the select box
			var employeeIdselect = document.getElementById('employeeId');

			// Clear the old options
			employeeIdselect.options.length = 0;
			
			employeeIdselect.options.add(new Option("Employee", '0'));
			
			if(departmentId==0){
				return false;
			}
			
			$.ajax({
				type:'POST',
				url: myContextPath+"/fetchEmployeeDetailsByDepartmentId/"+departmentId,
				headers : {
					'Content-Type' : 'application/json'
				},
				beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
				async: false,
				success : function(response){
					if(response.status=="Success"){
						//setting employee list
						var employeeDetailsList=response.employeeDetailsList;
						if(employeeDetailsList!=null){
							
							var options, index, option;
							employeeIdselect = document.getElementById('employeeId');
							for (var i = 0, len = employeeDetailsList.length; i < len; ++i) {
								var employeeDetails = employeeDetailsList[i];
								employeeIdselect.options.add(new Option(employeeDetails.name, employeeDetails.employeeId));
							}
						}
					}else{
						Materialize.Toast.removeAll();
						Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
					}
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error){
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					Materialize.Toast.removeAll();
					Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
				}		
			});
			
		});
	  
	  
	  $('#employeeId').change(function(){
			
			var employeeId=$('#employeeId').val();
			if(employeeId==0){
				showFilterOption("");
				return false;
			}
			$.ajax({
				type:'POST',
				url: myContextPath+"/fetch_employee_target_period/"+employeeId,
				headers : {
					'Content-Type' : 'application/json'
				},
				beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
				async: false,
				success : function(response){
					if(response!=""){
						showFilterOption(response);
						employeeChangeShowChart(response);
						targetPeriodMain=response;
					}else{
						targetPeriodMain="";
						Materialize.Toast.removeAll();
						Materialize.toast("", '2000', 'red lighten-2');
					}
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error){
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					Materialize.Toast.removeAll();
					Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
				}		
			});
		});
	  
	  $('#yearId3').change(function(){
		  var yearId=$('#yearId3').val();
		  if(yearId!=0){
			  fetchWeekDatesList(yearId);
		  }
	  });
	  
	  $('#showChartId').click(function(){
		  employeeChangeShowChart(targetPeriodMain);
	  });
	  
	  //hide all filter on load
	  showFilterOption("");
	  
	  //Create the chart (sample)
	  //showChart('Target Completed',['T1','T2','T3','T4','T5','T6'],'Target Complete (%)','Daily Target',[20,30,40,50,60,70]);  
	  
	  
        //set years
		for(var j=0; j<3; j++){
			// Get the raw DOM object for the select box
			var yearSelect = document.getElementById('yearId'+(j+1));
		
			// Clear the old options
			yearSelect.options.length = 0;
			
			yearSelect.options.add(new Option("Years", '0'));
			var options, index, option;
			yearSelect = document.getElementById('yearId'+(j+1));
			for (var i = 2018; i<2050; i++){
				yearSelect.options.add(new Option(i, i)); 
			}
			
			$('#yearId'+(j+1)).change();
		}
		
		//set month
		// Get the raw DOM object for the select box
		var monthSelect = document.getElementById('monthshow_monthId');
	
		// Clear the old options
		monthSelect.options.length = 0;
		
		monthSelect.options.add(new Option("Months", '0'));
		monthSelect.options.add(new Option("Jan", 1)); 
		monthSelect.options.add(new Option("Feb", 2)); 
		monthSelect.options.add(new Option("March", 3)); 
		monthSelect.options.add(new Option("April", 4)); 
		monthSelect.options.add(new Option("May", 5)); 
		monthSelect.options.add(new Option("June", 6)); 
		monthSelect.options.add(new Option("July", 7)); 
		monthSelect.options.add(new Option("Aug", 8)); 
		monthSelect.options.add(new Option("Sept", 9)); 
		monthSelect.options.add(new Option("Oct", 10)); 
		monthSelect.options.add(new Option("Nov", 11)); 
		monthSelect.options.add(new Option("Dec", 12)); 
		
		$('#monthshow_monthId').change();
	});

function employeeChangeShowChart(targetPeriod){
	var TargetGraphRequest;
	
	var employeeId=$('#employeeId').val();
	
	if(targetPeriod=="Daily"){
		var pickDate=$('#dayshow_pickdateId').val();
		TargetGraphRequest={
				  employeeId:employeeId,
				  targetPeriod:targetPeriod,
				  pickDate: pickDate
		  }
		getRecordsAccording(TargetGraphRequest);
	}else if(targetPeriod=="Weekly"){
		
		var weekDates=$('#weekshow_weekId').val();
				
		if(weekDates!=undefined && weekDates!=0){
			var dates=weekDates.split("_");
			TargetGraphRequest={
				  employeeId:employeeId,
				  targetPeriod:targetPeriod,
				  weekStartDate:dates[0],
				  weekEndDate:dates[1]
			}			
			getRecordsAccording(TargetGraphRequest);
		}
		
		
	}else if(targetPeriod=="Monthly"){
		
		var month=$('#monthshow_monthId').val();
		var year3=$('#yearId3').val();
		
		if(month!=0){
			TargetGraphRequest={
					  employeeId:employeeId,
					  targetPeriod:targetPeriod,
					  monthNo:month,
					  monthYear:year3
			  }
			getRecordsAccording(TargetGraphRequest);
		}
	}else if(targetPeriod=="Yearly"){
		
		var year1=$('#yearId1').val();
		
		if(year1!=0){
		TargetGraphRequest={
				  employeeId:employeeId,
				  targetPeriod:targetPeriod,
				  Year:year1
		  }
		getRecordsAccording(TargetGraphRequest);
		}
	}
	
	
}

  function showFilterOption(period){
	 
	  if(period=="Daily"){
		  $('#yearShow').hide(); 
		  $('#monthShow').hide();
		  $('#weekShow').hide();
		  $('#dayShow').show();
		  $('#buttonShow').show();
		  $('#graphId').show();
		  $('#targetTable').show();
	  }else if(period=="Weekly"){
		  $('#yearShow').hide(); 
		  $('#monthShow').hide();
		  $('#weekShow').show();
		  $('#dayShow').hide();
		  $('#buttonShow').show();
		  $('#graphId').show();
		  $('#targetTable').show();
	  }else if(period=="Monthly"){
		  $('#yearShow').hide(); 
		  $('#monthShow').show();
		  $('#weekShow').hide();
		  $('#dayShow').hide();
		  $('#buttonShow').show();
		  $('#graphId').show();
		  $('#targetTable').show();
	  }else if(period=="Yearly"){
		  $('#yearShow').show(); 
		  $('#monthShow').hide();
		  $('#weekShow').hide();
		  $('#dayShow').hide();
		  $('#buttonShow').show();
		  $('#graphId').show();
		  $('#targetTable').show();
	  }else {
		  $('#yearShow').hide(); 
		  $('#monthShow').hide();
		  $('#weekShow').hide();
		  $('#dayShow').hide();
		  $('#buttonShow').hide();
		  $('#targetTable').hide();
		  $('#graphId').hide();
	  }
  }
  
  function getRecordsAccording(TargetGraphRequest){
	  
	  /*TargetGraphRequest={
			  employeeId:
			  targetPeriod:
			  pickDate:
			  weekStartDate:
			  weekEndDate:
			  monthNo:
			  monthYear:
			  Year:Year
	  }*/
	  $('#targetTblData').empty();
	  var categoriesArr=[],targetDataCompletedArr=[],targetDataPendingArr=[];
	  $.ajax({
			type:'POST',
			url: myContextPath+"/target_list_by_periods",
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async: false,
			data:JSON.stringify(TargetGraphRequest),
			success : function(response){
				if(response.status=="Success"){
					targetResponseList=response.targetResponseList;
					for(var i=0; i<targetResponseList.length; i++){
						targetResponse=targetResponseList[i];
						categoriesArr.push("T"+(i+1));
						
						var per=(targetResponse.targetAchieveValue/targetResponse.targetAssignValue)*100;
						
						targetDataCompletedArr.push(parseFloat(per.toFixed(2)));
						targetDataPendingArr.push(100-per.toFixed(2));
						$('#targetTblData').append('<tr>'+
													'<td>T'+targetResponse.srno+'</td>'+
													'<td>'+targetResponse.targetName+'</td>'+
													'<td>'+targetResponse.targetAssignValue+'</td>'+
													'<td>'+targetResponse.targetAchieveValue+'</td>'+
													'<td>'+per.toFixed(2)+'%</td>'+
												   '</tr>');
					}
					
					showChart("Target Completed",categoriesArr,"Target Complete (%)",TargetGraphRequest.targetPeriod+" Targets",targetDataPendingArr,targetDataCompletedArr);
				}else{
					Materialize.Toast.removeAll();
					Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
  }
  function fetchWeekDatesList(year){
	  
	// Get the raw DOM object for the select box
	var WeekSelect = document.getElementById('weekshow_weekId');

	// Clear the old options
	WeekSelect.options.length = 0;
	
	WeekSelect.options.add(new Option("Weeks", '0'));
	  
	  $.ajax({
			type:'POST',
			url: myContextPath+"/fetch_week_dates_by_year/"+year,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async: false,
			success : function(response){
				
				var options, index, option;
				WeekSelect = document.getElementById('weekshow_weekId');
				for (var i = 0; i< response.length; ++i) {
					var weekDates=response[i];
					WeekSelect.options.add(new Option(weekDates[0]+" - "+weekDates[2], weekDates[1]+"_"+weekDates[3])); 
				}
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
  }
  function showChart(titleText,categoriesArr,yAxisText,targetName,targetDataPendingArr,targetDataCompletedArr){
	  Highcharts.chart('dailyContainer', {
		  chart: {
		    type: 'column'
		  },
		  title: {
		    text: titleText
		  },
		  subtitle: {
		    text: ''
		  },
		  xAxis: {
		    categories: categoriesArr,
		    crosshair: true
		  },
		  yAxis: {
		    min: 0,
		    /*max: 100,*/
		    title: {
		      text: yAxisText
		    }
		  },
		  tooltip: {
		    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		      				 '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
		    footerFormat: '</table>',
		    shared: true,
		    useHTML: true
		  },
		  plotOptions: {
		    column: {
		      pointPadding: 0.2,
		      borderWidth: 0
		    }
		  },
		  series: [{
		    color:'#3760a3',
			name: targetName,
		    data: targetDataCompletedArr
		  }]
		});
	  
	  

	 /* Highcharts.chart('dailyContainer', {
	      chart: {
	          type: 'column'
	      },
	      title: {
	          text: titleText
	      },
	      xAxis: {
	          categories: categoriesArr
	      },
	      yAxis: {
	          min: 0,
	          max:100,
	          title: {
	              text: yAxisText
	          }
	      },
	      tooltip: {
	          pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
	          shared: true
	      },
	      plotOptions: {
	          column: {
	              stacking: 'percent'
	          }
	      },
	      series: [{
	    	    color:'#5d9fdd',
				name: "Pending",
			    data: targetDataPendingArr
	      },{
	    	    color:'#5cdd67',
				name: "Completed",
			    data: targetDataCompletedArr
	      }]
	  });*/
  }