/*function for clear form data for calculator*/
	function clearFunction(){
		$('#loanAmt').val("");
		$('#timePeriod').val("");
		$('#interestRate').val("");
		$('#emiAmt').text("")
		$('#totalInterestAmt').text("");
		$('#totalPayableAmt').text("");
		
		/* TDS Modal feild */
		$('#salaryAmt').val("");
		$('#percentageSlab').val("");
		$('#tdsAmt').text("");
	}
	/*function for calculate tds*/
	function calculateTDS(){
		var salaryAmt=$('#salaryAmt').val();
		var percentSlab=$('#percentageSlab').val();
		
		if(salaryAmt==""){
			var $toastContent = $('<span>Please Enter Salary Amount<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else if(percentSlab==""){
			var $toastContent = $('<span>Please Enter Percentage Slab<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}
		
		
		var tdsAmt=(salaryAmt*percentSlab/100).toFixedVSS(2);
		
		$('#tdsAmt').text(tdsAmt);
	}
	
	/*function for calculate emi*/
	function calculateEMI(){
		var p=$('#loanAmt').val();
		var noOfMonths=$('#timePeriod').val();
		var r=$('#interestRate').val();
		if(p==""){
			var $toastContent = $('<span>Please Enter Loan Amount<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else if(noOfMonths==""){
			var $toastContent = $('<span>Please Enter Time Period<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else if(r==""){
			var $toastContent = $('<span>Please Enter Rate Of Interest<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else{
			var monthlyInterestRate=r/(12*100);
			var top=Math.pow((1 + monthlyInterestRate), noOfMonths);
			var emiAmt=(p*monthlyInterestRate*top)/(top-1);
			emiAmt=emiAmt.toFixedVSS(2);
			
			var totalPaymentAmt=(emiAmt)*noOfMonths;
			var totalInterestAmt=totalPaymentAmt-p;
			console.log(emiAmt);
			$('#emiCalculatedDiv').show();
			$('#emiAmt').text(Math.round(emiAmt));
			$('#totalInterestAmt').text(totalInterestAmt.toFixedVSS(2));
			$('#totalPayableAmt').text(totalPaymentAmt.toFixedVSS(2));
		}
		
		
	}