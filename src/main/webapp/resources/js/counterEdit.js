var productCart = [];
var freeNonFreeRdo = "NonFree";
var payTypeRdo = "Cash";
var paymentRdo = "InstantPay";
var isPrintBill = false;
var counterOrderOld
var refAmount = 0;
var paymentSituation;
var balAmt;
let newTotalAmount = 0;
let gateKeeperName = "";
$(document).ready(function () {
	//fetch counter details by counter id
	fetchCounterOrderByCounterOrderId(counterOrderId);

	$('.invoiceRadioBtnClass').hide();//hide Invoice and Proforma Invoice Radio button

	if (counterOrderOld.discount != 0) {
		$('.discountMainClass').show();
		$('#discountMainCheck').prop('checked', true);
		discountMainCheckFunction();
		$('#discountMainId').val(counterOrderOld.discount);

		if (counterOrderOld.discountType == "Percentage") {
			$('#percentageMainCheck').prop('checked', true);
		} else {
			$('#percentageMainCheck').prop('checked', false);
		}
		$('#discountMainId').keyup();

	} else {

		$('.discountMainClass').hide();
		$('#discountMainCheck').prop('checked', false);
		$('#discountMainId').val(0);
		discountMainCheckFunction();

	}




	//if counter order have business details then select business from drop down 
	//else set customer info on modal
	if (counterOrderOld.businessName == '' || counterOrderOld.businessName == undefined) {
		$('#custName').val(counterOrderOld.customerName);
		$('#custName').change();
		$('#mobileNo').val(counterOrderOld.customerMobileNumber);
		$('#mobileNo').change();
		$('#gstNo').val(counterOrderOld.customerGstNumber);
		$('#gstNo').change();
	} else {
		var source = $('#businessListId');
		source.val(counterOrderOld.businessName.businessNameId);
		source.change();
	}


	//if transportation details got from counter order then set 
	var transportation = counterOrderOld.transportation;
	if (transportation != '' && transportation != undefined) {
		var source = $('#transport_select');
		var val = transportation.transportName + "~" + transportation.mobNo + "~" + transportation.gstNo + "~" + transportation.vehicleNo + "~" + transportation.id;
		source.val(val);
		source.change();
	}


	//fetch counter product details list and add its on productCart array
	var counterOrderProductDetails = fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId);
	for (var i = 0; i < counterOrderProductDetails.length; i++) {
		productCart.push([
			counterOrderProductDetails[i].product.product,
			counterOrderProductDetails[i].purchaseQuantity,
			counterOrderProductDetails[i].sellingRate,
			counterOrderProductDetails[i].purchaseAmount,
			counterOrderProductDetails[i].type,

			counterOrderProductDetails[i].discountPer,
			counterOrderProductDetails[i].discount,
			(counterOrderProductDetails[i].discountType == "Percentage"),

			counterOrderProductDetails[i].discountPerOld,
			counterOrderProductDetails[i].discountOld,
			counterOrderProductDetails[i].purchaseAmountOld,
		]);
	}



	//appends table row using productCart[] 
	refreshTable();

	/**
	 * only number allowed without decimal 
	 */
	$('#mobileNo').keypress(function (event) {
		var key = event.which;
		if (!(key >= 48 && key <= 57 || key === 13))
			event.preventDefault();
	});


	//whole discount checkbox event
	$('#discountMainCheck').click(function () {

		discountMainCheckFunction();

	});

	function discountMainCheckFunction() {
		if ($('#discountMainCheck').prop("checked") == true) {
			$('.discountMainClass').show();
			resetToOld();
			$('#discountMainId').val(0);
			$('#discountMainId').focus();
			$('#discountMainId').blur();
			$('#discountMainId').keyup();
			$('#percentageMainCheck').prop('checked', true);
			$('.editable').attr('disabled', 'disabled');
		} else {
			$('#discountMainId').val(0);
			$('#discountMainId').focus();
			$('#discountMainId').blur();
			$('#discountMainId').keyup();
			$('#percentageMainCheck').prop('checked', true);
			resetToOld();
			$('.discountMainClass').hide();
			$('.editable').removeAttr('disabled');
		}
	}

	$('#percentageMainCheck').click(function () {
		$('#discountMainId').keyup();
	});
	$('#discountMainId').keyup(function () {
		/*var oldTotal=0;
		for(var i=0; i<productCart.length; i++){
			oldTotal+=parseFloat(productCart[i][10]);
		}
		
		
		var discMain=$(this).val();		
		if(discMain==""){
			discMain=0;
		}
		if($('#percentageMainCheck').prop("checked") == true){
			if(discMain>100 || discMain<0){
				$(this).val(0);
				$(this).keyup();
				Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
				return false;
			}
		}else{
			if(discMain>oldTotal){
				$(this).val(0);
				$(this).keyup();
				Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
				return false;
			}
		}
		
		
		
		
		var discountAmount;
		if($('#percentageMainCheck').prop("checked") == true){
			discountAmount=(oldTotal*discMain)/100;			
		}else{
			discountAmount=discMain;
		}
		
		var newTotalAmount=(oldTotal-discountAmount);
		
		var productCartTemp=[];
		for(var i=0; i<productCart.length; i++){
		
			var product=productCart[i][0];
			var qty=productCart[i][1];	
			var mrp=productCart[i][2];
			var total=productCart[i][3];
			var type=productCart[i][4];
			var disc=productCart[i][5];
			var discAmt=productCart[i][6];
			var percentageCheck =productCart[i][7];
			
			var discP=productCart[i][8];
			var discAmtP=productCart[i][9];
			var totalP=productCart[i][10];
			
			var newProductTotalAmount=(totalP*newTotalAmount)/oldTotal;
			var totalAmt=(mrp*qty);
						
			if(type=="NonFree"){
				discAmt=totalAmt-newProductTotalAmount;
				disc=(discAmt/totalAmt)*100;
				total=totalAmt-discAmt;
			}	
			
			if(percentageCheck){					
				disc=(discAmt/totalAmt)*100;					
			}else{
				disc=0;
			}
			
			
			//add record to productCart[]
			productCartTemp.push([
			                      product,
			                      qty,
			                      mrp,
			                      parseFloat(total).toFixedVSS(2),
			                      type,
			                      parseFloat(disc).toFixedVSS(2),
			                      parseFloat(discAmt).toFixedVSS(2),
			                      percentageCheck,
				                  
				                  parseFloat(discP).toFixedVSS(2),
				                  parseFloat(discAmtP).toFixedVSS(2),
				                  parseFloat(totalP).toFixedVSS(2)]);
		}
		productCart=[];
		for(var i=0; i<productCartTemp.length; i++){
			productCart.push(productCartTemp[i]);
		}*/

		var finalTotal = 0;
		for (var i = 0; i < productCart.length; i++) {
			finalTotal += parseFloat(productCart[i][3]);
		}
		finalTotal = finalTotal.toFixedVSS(2);
		finalTotal = parseFloat(finalTotal);

		var discMain = $('#discountMainId').val();
		if (discMain == "") {
			discMain = 0;
		}
		discMain = parseFloat(discMain);

		if ($('#percentageMainCheck').prop("checked") == true) {
			if (discMain > 100 || discMain < 0) {
				$('#discountMainId').val(0);
				$('#discountMainId').keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
				return false;
			}
		} else {
			if (discMain > finalTotal) {
				$('#discountMainId').val(0);
				$('#discountMainId').keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
				return false;
			}
		}

		//appends table row using productCart[] 
		refreshTable();

		//find final total Amount
		findTotal();

		$('.editable').attr('disabled', 'disabled');
	});

	function resetToOld() {

		/*var productCartTemp=[];
		for(var i=0; i<productCart.length; i++){
		
			var product=productCart[i][0];
			var qty=productCart[i][1];	
			var mrp=productCart[i][2];
			var total=productCart[i][3];
			var type=productCart[i][4];
			var disc=productCart[i][5];
			var discAmt=productCart[i][6];
			var percentageCheck =productCart[i][7];
			var discP=productCart[i][5];
			var discAmtP=productCart[i][6];
			var totalP=productCart[i][3];			

			productCartTemp.push([
			                      product,
			                      qty,
			                      mrp,
			                      parseFloat(total).toFixedVSS(2),
			                      type,
			                      parseFloat(disc).toFixedVSS(2),
			                      parseFloat(discAmt).toFixedVSS(2),
			                      percentageCheck,
			                     
			                      discP,
			                      discAmtP,
			                      totalP]);
		}
		productCart=[];
		for(var i=0; i<productCartTemp.length; i++){
			productCart.push(productCartTemp[i]);
		}
		
		var oldTotal=0,oldTotalUP=0;
		for(var i=0; i<productCart.length; i++){
			oldTotal+=parseFloat(productCart[i][10]);
		}
		
		var newTotalAmount=oldTotal;
		
		var productCartTemp=[];
		for(var i=0; i<productCart.length; i++){
		
			var product=productCart[i][0];
			var qty=productCart[i][1];	
			var mrp=productCart[i][2];
			var total=productCart[i][3];
			var type=productCart[i][4];
			var disc=productCart[i][5];
			var discAmt=productCart[i][6];
			var percentageCheck =productCart[i][7];
			var discP=productCart[i][8];
			var discAmtP=productCart[i][9];
			var totalP=productCart[i][10];
			
			var newProductTotalAmount,totalAmt;
			if(type=="NonFree"){
				newProductTotalAmount=(totalP*newTotalAmount)/oldTotal;
				totalAmt=(mrp*qty);
				
				discAmt=totalAmt-newProductTotalAmount;
				disc=(discAmt/totalAmt)*100;
				
				total=totalAmt-discAmt;
			}	
			
			if(percentageCheck){					
				disc=(discAmt/totalAmt)*100;					
			}else{
				disc=0;
			}
			
			//add record to productCart[]
			productCartTemp.push([
			                      product,
			                      qty,
			                      mrp,
			                      parseFloat(total).toFixedVSS(2),
			                      type,
			                      parseFloat(disc).toFixedVSS(2),
			                      parseFloat(discAmt).toFixedVSS(2),
			                      percentageCheck,
			                      
			                      discP,
			                      discAmtP,
			                      totalP]);
		}
		productCart=[];
		for(var i=0; i<productCartTemp.length; i++){
			productCart.push(productCartTemp[i]);
		}*/

		//appends table row using productCart[] 
		refreshTable();

		//find final total Amount
		findTotal();
	}

	//free nonFree radio button
	$('#freeId').click(function () {
		freeNonFreeRdo = "Free";
		$('.discountClass').hide();
	});

	//free nonFree radio button
	$('#nonFreeId').click(function () {
		freeNonFreeRdo = "NonFree";
		$('.discountClass').show();
		$('#discountId').val(null);
		$('#discountId').focus();
		$('#discountId').blur();
		$('#percentageCheck').prop('checked', true);

	});

	$('.paymentCls').click(function () {
		var id = $(this).attr('id');
		if (id == "cash") {
			payTypeRdo = "Cash";
		} else if (id == "cheque") {
			payTypeRdo = "Cheque";
		} else {
			payTypeRdo = "Other";
		}
	});

	$('.payTypesCls').click(function () {
		var id = $(this).attr('id');
		if (id == "instantPay") {
			paymentRdo = "InstantPay";
		} else if (id == "credit") {
			paymentRdo = "Credit";
		} else {
			paymentRdo = "PartialPay";
		}
	});

	/**
	 * add product record to table
	 */
	$('#addCartButtonId').click(function () {

		var productId = $('#productId').val();
		if (productId == "0") {
			Materialize.Toast.removeAll();
			Materialize.toast('Select Product', '3000', 'teal lighten-2');
			return false;
		}

		var qty = $('#quantityId').val();
		if (qty == "0" || qty == undefined || qty == '') {
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Quantity', '3000', 'teal lighten-2');
			return false;
		}

		for (var i = 0; i < productCart.length; i++) {
			var product = productCart[i][0];
			var type = productCart[i][4];
			if (product.productId == productId && freeNonFreeRdo == type) {
				Materialize.Toast.removeAll();
				Materialize.toast('Product Already added', '3000', 'teal lighten-2');
				return false;
			}
		}

		var product = fetchProductByProductId(productId);
		var mrp = ((parseFloat(product.rate)) + ((parseFloat(product.rate) * parseFloat(product.categories.igst)) / 100)).toFixedVSS(0);
		var total = parseInt(qty) * parseFloat(mrp);

		var discount = $('#discountId').val();
		if ($('#percentageCheck').prop("checked") == true) {
			if (discount > 100 || discount < 0) {
				Materialize.Toast.removeAll();
				Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
				return false;
			}
		} else {
			if (discount > total) {
				Materialize.Toast.removeAll();
				Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
				return false;
			}
		}

		if (discount == '') {
			discount = 0;
		}


		var type = freeNonFreeRdo;



		var percentageCheck, disc, discAmt;
		if ($('#percentageCheck').prop("checked") == true) {
			percentageCheck = true;
			disc = discount;
			discAmt = (total * disc) / 100;
			total = total - discAmt;
		} else {
			disc = (discount / total) * 100;
			discAmt = discount;
			total = total - discAmt;

			percentageCheck = false;
		}

		if (type == "Free") {
			total = 0;
			mrp = 0;
			disc = 0;
			discAmt = 0;
			percentageCheck = false;
		}

		var availableQty = qtyAvailable(productId);

		//check product current qty exceeds with entered qty
		if (parseInt(qty) > parseInt(availableQty)) {
			Materialize.Toast.removeAll();
			Materialize.toast('Qty Must be Below or equal Current Quantity. Max : ' + availableQty, '3000', 'teal lighten-2');
			return false;
		}

		//add record to productCart[]
		productCart.push([
			product,
			qty,
			mrp,
			parseFloat(total).toFixedVSS(2),
			type,
			parseFloat(disc).toFixedVSS(2),
			parseFloat(discAmt).toFixedVSS(2),
			percentageCheck,

			parseFloat(disc).toFixedVSS(2),
			parseFloat(discAmt).toFixedVSS(2),
			parseFloat(total).toFixedVSS(2)]);

		//appends table row using productCart[] 
		refreshTable();

		//find final total Amount
		findTotal();

		//reset product
		resetProduct();
	});
	/***
	 * clear productCart and transportation details
	 */
	$('#clearButtonId').click(function () {
		productCart.splice(0, productCart.length);
		refreshTable();
		$('#transport_details').prop("checked", false);
		$('#transport_details_info').hide();
		$('#transGstNo').val("");
		$('#transVehicleNo').val("");
		$('#transDocketNo').val("");
		$('#transMobNo').val("");
		$('#transportName').val("");
		$("#transport_select option:first").attr('selected', 'selected');
		resetProduct();
		$('#discountMainCheck').prop('checked', false);
		discountMainCheckFunction();
	});
	/**
	 * on page
	 * on transportion selection change 
	 * set mobilenumber , gst number ,vehical number
	 */
	$('#transport_select').change(function () {
		var transportValue = $('#transport_select').val();
		var info = transportValue.split("~");
		$('#transMobNo').prop('readonly', false);
		$('#transGstNo').prop('readonly', false);
		//$('#transVehicleNo').prop('readonly',false);
		$('#transportName').val(info[0]);
		$('#transMobNo').val(info[1]);
		$('#transGstNo').val(info[2]);
		$('#transVehicleNo').val(info[3]);
		$('#transMobNo').prop('readonly', true);
		$('#transGstNo').prop('readonly', true);
		//$('#transVehicleNo').prop('readonly',true);

	});
	/**
	 * if user select Partial Credit
	 * check user entered partially amount or not
	 * according entered amount change balance amount
	 */
	$('#amountPartial').keyup(function () {
		var amountPartial = $('#amountPartial').val();
		var balAmountPartial = $('#balAmountPartial').val();

		if (amountPartial == '' || amountPartial == undefined) {
			amountPartial = 0;
		}

		/* var finalTotal=0;
		for(var i=0; i<productCart.length; i++){
			finalTotal+=parseFloat(productCart[i][3]);
		} */

		if (parseFloat(balAmt) < parseFloat(amountPartial)) {
			Materialize.Toast.removeAll();
			Materialize.toast('Paid amount must be less than Total Amount : ' + Math.round(balAmt), '3000', 'teal lighten-2');
			$('#amountPartial').val('');
			$('#balAmountPartial').val(Math.round(balAmt));
			return false;
		} else if (parseFloat(balAmt) == parseFloat(amountPartial)) {
			Materialize.Toast.removeAll();
			Materialize.toast('Please Enter Partial Amount only <br> For Full Payment Select Instant Pay', '3000', 'teal lighten-2');
			$('#amountPartial').val('');
			$('#balAmountPartial').val(Math.round(balAmt));
			return false;
		}

		$('#balAmountPartial').val(Math.round(balAmt - parseFloat(amountPartial)));
	});
	/**
   * if pay button click 
   * verify counter details filled or not using
   * if filled properly
   * if business select then submit else
   * ask for customer details(name,mobile number,gst number)
   * disable pay buttons
   */
	$('#payButtonId').click(function () {
		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		if (verifySubmit()) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();

			var businessListId = $('#businessListId').val();
			if (businessListId == 0) {
				$("#custInfo").modal('open');
				return false;
			}
			isPrintBill = false;
			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);
			/*var formData=$("#saveOrderWithBusines").serialize()
			submitCounterOrder(formData);*/
			confirmToast();
		}

		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();
	});
	/**
	 * if pay and print button click 
	 * verify counter details filled or not using
	 * if filled properly
	 * if business select then submit else
	 * ask for customer details(name,mobile number,gst number)
	 * disable pay buttons
	 * after saved counter order one bill pdf in next tab for anything(print,view,mail)
	 */
	$('#payAndPrintButtonId').click(function () {

		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		if (verifySubmit()) {

			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();

			var businessListId = $('#businessListId').val();
			if (businessListId == 0) {
				$("#custInfo").modal('open');
				return false;
			}
			isPrintBill = true;
			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);

			/*var formData=$("#saveOrderWithBusines").serialize()
			submitCounterOrder(formData);*/

			confirmToast();
		}
		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();
	});
	/**
	 * if customer info submit button click 
	 * validation of cutomer name,gst number,mobile number
	 * verify counter details filled or not using
	 * if filled properly
	 * disable submit buttons
	 * after saved counter order if user clicked on Pay And Print Button button then bill pdf in next tab for anything(print,view,mail)
	 */
	$('#submitCustomerInfo').click(function () {
		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		var custName = $('#custName').val();
		var mobileNo = $('#mobileNo').val();
		var gstNo = $('#gstNo').val();

		if (custName == '' || custName == undefined) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Customer Name', '3000', 'teal lighten-2');
			return false;
		}

		if (mobileNo == '' || mobileNo == undefined) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Mobile Number', '3000', 'teal lighten-2');
			return false;
		}
		if (mobileNo.length != 10 && mobileNo.length > 0) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Correct Mobile Number', '3000', 'teal lighten-2');
			return false;
		}
		if (gstNo.length != 15 && gstNo.length > 0) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Correct Gst In Number', '3000', 'teal lighten-2');
			return false;
		}

		if (verifySubmit()) {

			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();

			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);
			$('#submitCustomerInfo').prop('disabled', true);

			/*var formData=$("#saveCustomerInfoWithOrder").serialize()
			submitCounterOrder(formData);*/

			confirmToast();
		}

		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();
	});



	if (transportation != '' && transportation != undefined) {
		$('#transport_select').change();
		$('#transport_details').prop("checked", true);
		//$('#transport_details_info').toggle();

		$('#transVehicleNo').val(counterOrderOld.vehicleNo);
		$('#transDocketNo').val(counterOrderOld.docketNo);
		$('#transportationChargesId').val(counterOrderOld.transportationCharges);
		$('#transport_details_info').show();
	} else {
		$('#transport_details_info').hide();
	}

});

function confirmToast() {
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Submit?</span>').add($('<button class="btn red white-text toast-action" onclick="cancelSubmit()">Cancel</button><button class="btn white-text toast-action" onclick="submit()">Pay</button>  '));
	Materialize.toast($toastContent, "abc");
}

function submit() {
	Materialize.Toast.removeAll();
	var formData = $("#saveCustomerInfoWithOrder").serialize()
	submitCounterOrder(formData);
}

function cancelSubmit() {
	Materialize.Toast.removeAll();
	$('#payButtonId').prop('disabled', false);
	$('#payAndPrintButtonId').prop('disabled', false);
	$('#submitCustomerInfo').prop('disabled', false);
}

/**
 * verify when click on pay,payAndPrint,CustInfoSubmit
 */
function verifySubmit() {

	if (productCart.length == 0) {
		Materialize.Toast.removeAll();
		Materialize.toast('Add Product In order', '3000', 'teal lighten-2');
		return false;
	}

	var finalTotal = 0;
	var productListData = "";
	var nonFreeFound = false;

	//qty verify with current invt start
	var productListWithQty = new HashMap();
	for (var i = 0; i < productCart.length; i++) {

		if (productCart[i][1] == 0) {

			var freeNonFreeText = "";
			if (productCart[i][4] == "Free") {
				freeNonFreeText = "-Free";
			} else {
				freeNonFreeText = "";
			}
			Materialize.Toast.removeAll();
			Materialize.toast(productCart[i][0].productName + freeNonFreeText + ' Quantity must be non zero ', '3000', 'teal lighten-2');

			return false;
		}

		/* For External use */

		/*productCartTemp.push([
        product,
        qty,
        mrp,
        parseFloat(total).toFixedVSS(2),
        type,
        parseFloat(disc).toFixedVSS(2),
        parseFloat(discAmt).toFixedVSS(2),
        percentageCheck,
        
        discP,
        discAmtP,
        totalP]);*/

		/*if(parseFloat(productCart[i][3])<=0 && productCart[i][4]=="NonFree"){
		Materialize.toast(productCart[i][0].productName+' Total must be greater than zero ', '3000', 'teal lighten-2');
		return false;
		}*/

		var percentageCheck = productCart[i][7];
		var percentageCheckRes;
		if (percentageCheck) {
			percentageCheckRes = "Percentage";
		} else {
			percentageCheckRes = "Amount";
		}

		var percentageCheckOld = productCart[i][10];
		var percentageCheckOldRes;
		if (percentageCheckOld) {
			percentageCheckOldRes = "Percentage";
		} else {
			percentageCheckOldRes = "Amount";
		}

		//finalTotal+=parseFloat(productCart[i][3]);

		var product = productCart[i][0];
		var qty = productCart[i][1];
		var mrp = productCart[i][2];
		var total = productCart[i][3];
		var type = productCart[i][4];

		var correctAmoutWithTaxObj = calculateProperTax(parseFloat(total), parseFloat(product.categories.igst));

		productListData += productCart[i][0].productId + ","
			+ productCart[i][1] + ","
			+ productCart[i][2] + ","
			+ productCart[i][3] + ","
			+ productCart[i][4] + ","
			+ productCart[i][5] + ","
			+ productCart[i][6] + ","
			+ percentageCheckRes + ","
			+ productCart[i][8] + ","
			+ productCart[i][9] + ","
			+ productCart[i][10] + ","
			+ percentageCheckOldRes + ","
			+ correctAmoutWithTaxObj.unitPrice
			+ "-";
		/* End */


		if (productListWithQty.containsKey(product.productId)) {
			var entry = productListWithQty.get(product.productId);
			productListWithQty.remove(product.productId);
			productListWithQty.put(product.productId, parseInt((entry.value)[0]) + parseInt(qty));

		} else {
			productListWithQty.put(product.productId, parseInt(qty));
		}

		if (productCart[i][4] == "NonFree") {
			nonFreeFound = true;
		}
	}

	if (nonFreeFound == false) {
		Materialize.Toast.removeAll();
		Materialize.toast('Minimum One Non-Free Product Required For Order', '3000', 'teal lighten-2');
		return false;
	}

	var entrySet = productListWithQty.getAll();

	var counterOrderProductDetails = fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId);
	for (var p = 0; p < entrySet.length; p++) {
		var key = entrySet[p].key;
		var val = entrySet[p].value;
		var product = fetchProductByProductId(key);

		var oldQty = 0;
		for (var q = 0; q < counterOrderProductDetails.length; q++) {
			var product = counterOrderProductDetails[q].product.product;
			if (product.productId == key) {
				oldQty += counterOrderProductDetails[q].purchaseQuantity;
			}
		}
		var checkQty = parseFloat(val) - parseFloat(oldQty);
		if (product.currentQuantity < checkQty) {
			Materialize.Toast.removeAll();
			Materialize.toast(product.productName + ' qty exceeds Current Qty. Max Available :  ' + parseInt(parseFloat(product.currentQuantity) + parseFloat(oldQty)), '3000', 'teal lighten-2');
			return false;
		}

	}
	//qty verify with current invt end

	//remove last '-'
	productListData = productListData.substring(0, productListData.length - 1);

	/* var paymentCounterList=fetchPaymentCounterOrder(counterOrderId);		
	var paidAmt=0
	if(paymentCounterList.length>0){
		paidAmt=paymentCounterList[0].totalAmountPaid;
	}			
	if(paidAmt==counterOrderOld.totalAmountWithTax && paidAmt<finalTotal){
		var dueDate=$('#dueDateExtraPay').val();
		if(dueDate=='' || dueDate==undefined){
			Materialize.toast('Select Due Date' , '3000', 'teal lighten-2');
			return false;
		}

		var dueDate2=new Date($('#dueDateExtraPay').val()).setHours(0,0,0,0);
		var currDate=new Date().setHours(0,0,0,0);
		if(dueDate2<currDate){
			Materialize.toast('Please Select Correct Due Date' , '3000', 'teal lighten-2');
			return false;
		}
		$('.dueDateCls').val(dueDate);
	} */

	var amountPartial = $('#amountPartial').val();
	if (amountPartial == "" || amountPartial == undefined) {
		amountPartial = 0;
	}

	var balAmountPartial = $('#balAmountPartial').val();
	var dueDate = $('#dueDate').val();
	var dueDateCredit = $('#dueDateCredit').val();
	var bankName = $('#bankName').val();
	var chqNo = $('#chqNo').val();
	var chequeDate = $('#chequeDate').val();

	var paymentMethodId = $('#paymentMethodId').val();
	var transactionRefNo = $('#transactionRefNo').val();
	var comment = $('#comment').val();

	if (paymentRdo == "PartialPay") {

		if (amountPartial == 0) {
			Materialize.Toast.removeAll();
			Materialize.toast('Please Enter Partial Amount ', '3000', 'teal lighten-2');
			return false;
		}

		if (amountPartial >= balAmt) {
			Materialize.Toast.removeAll();
			Materialize.toast('Please Enter Partial Amount only <br> For Full Payment Select Instant Pay', '3000', 'teal lighten-2');
			return false;
		}

		if (dueDate == '' || dueDate == undefined) {
			Materialize.Toast.removeAll();
			Materialize.toast('Select Due Date', '3000', 'teal lighten-2');
			return false;
		}

		var dueDate2 = new Date($('#dueDate').val()).setHours(0, 0, 0, 0);
		var currDate = new Date().setHours(0, 0, 0, 0);
		if (dueDate2 < currDate) {
			Materialize.Toast.removeAll();
			Materialize.toast('Please Select Correct Due Date', '3000', 'teal lighten-2');
			return false;
		}
		$('.dueDateCls').val(dueDate);

		if (payTypeRdo == "Cheque") {

			if (bankName == '' || bankName == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter Bank Name', '3000', 'teal lighten-2');
				return false;
			}

			if (chqNo == '' || chqNo == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter Cheque Number', '3000', 'teal lighten-2');
				return false;
			}

			if (chequeDate == '' || chequeDate == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Select Cheque Date', '3000', 'teal lighten-2');
				return false;
			}

			var chequeDate2 = new Date($('#chequeDate').val()).setHours(0, 0, 0, 0);
			var currDate = new Date().setHours(0, 0, 0, 0);
			if (chequeDate2 < currDate) {
				Materialize.Toast.removeAll();
				Materialize.toast('Please Select Correct Cheque Date', '3000', 'teal lighten-2');
				return false;
			}
		} else if (payTypeRdo == "Other") {
			if (paymentMethodId == 0) {
				Materialize.Toast.removeAll();
				Materialize.toast('Please Select Payment Method', '3000', 'teal lighten-2');
				return false;
			} else if (transactionRefNo.trim() == "") {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter transaction reference number', '3000', 'teal lighten-2');
				return false;
			}/*else if(comment.trim()==""){
					Materialize.toast('Enter comment' , '3000', 'teal lighten-2');
					return false;
				}
				*/

		}

	} else if (paymentRdo == "Credit") {

		if (dueDateCredit == '' || dueDateCredit == undefined) {
			Materialize.Toast.removeAll();
			Materialize.toast('Select Due Date', '3000', 'teal lighten-2');
			return false;
		}

		var dueDateCredit2 = new Date($('#dueDateCredit').val()).setHours(0, 0, 0, 0);
		var currDate = new Date().setHours(0, 0, 0, 0);
		if (dueDateCredit2 < currDate) {
			Materialize.Toast.removeAll();
			Materialize.toast('Please Select Correct Due Date', '3000', 'teal lighten-2');
			return false;
		}

		$('.dueDateCls').val(dueDateCredit);
	} else {

		if (payTypeRdo == "Cheque") {

			if (bankName == '' || bankName == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter Bank Name', '3000', 'teal lighten-2');
				return false;
			}

			if (chqNo == '' || chqNo == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter Cheque Number', '3000', 'teal lighten-2');
				return false;
			}

			if (chequeDate == '' || chequeDate == undefined) {
				Materialize.Toast.removeAll();
				Materialize.toast('Select Cheque Date', '3000', 'teal lighten-2');
				return false;
			}

			var chequeDate2 = new Date($('#chequeDate').val()).setHours(0, 0, 0, 0);
			var currDate = new Date().setHours(0, 0, 0, 0);
			if (chequeDate2 < currDate) {
				Materialize.Toast.removeAll();
				Materialize.toast('Please Select Correct Cheque Date', '3000', 'teal lighten-2');
				return false;
			}
		} else if (payTypeRdo == "Other") {
			if (paymentMethodId == 0) {
				Materialize.Toast.removeAll();
				Materialize.toast('Please Select Payment Method', '3000', 'teal lighten-2');
				return false;
			} else if (transactionRefNo.trim() == "") {
				Materialize.Toast.removeAll();
				Materialize.toast('Enter transaction reference number', '3000', 'teal lighten-2');
				return false;
			}/*else if(comment.trim()==""){
					Materialize.toast('Enter comment' , '3000', 'teal lighten-2');
					return false;
				}*/


		}
	}

	var isTransportationHaveCls = 'No';
	if ($("#transport_details").prop('checked') == true) {
		var transport_select_val = $('#transport_select').val();
		var transVehicleNo = $('#transVehicleNo').val();
		var transDocketNo = $('#transDocketNo').val();
		var transportCharges = $('#transportationChargesId').val();

		if (transport_select_val == 0) {
			Materialize.Toast.removeAll();
			Materialize.toast('Select Transportation', '3000', 'teal lighten-2');
			return false;
		} else if (transVehicleNo == '' || transVehicleNo == undefined) {
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Vehical Number Transportation', '3000', 'teal lighten-2');
			return false;
		}

		isTransportationHaveCls = 'Yes';

		var transportValue = $('#transport_select').val();
		var info = transportValue.split("~");

		$('.isTransportationHaveCls').val(isTransportationHaveCls);
		$('.transportationIdCls').val(info[4]);
		$('.vehicalNumberCls').val($('#transVehicleNo').val().trim());
		$('.docketNumberCls').val(transDocketNo);
		$('.transportationChargesH').val(transportCharges);
	} else {
		$('.isTransportationHaveCls').val('');
		$('.transportationIdCls').val('');
		$('.vehicalNumberCls').val('');
		$('.docketNumberCls').val('');
		$('.transportationChargesH').val('');
	}

	if ($('#discountMainCheck').prop("checked") == true) {
		if ($('#percentageMainCheck').prop("checked") == true) {
			$('.dicountTypeIdCls').val('Percentage');
		} else {
			$('.dicountTypeIdCls').val('Amount');
		}
		$('.discountAmountCls').val($('#discountMainId').val());
	} else {
		$('.dicountTypeIdCls').val('NA');
		$('.discountAmountCls').val(0);
	}

	var businessListId = $('#businessListId').val();

	$('.productListCls').val(productListData);
	$('.businessNameIdCls').val(businessListId);
	$('.paidAmountCls').val(amountPartial);
	$('.balAmountCls').val(balAmountPartial);
	$('.refAmountCls').val(refAmount);
	$('.paymentSituationCls').val(paymentSituation);
	$('.payTypeCls').val(payTypeRdo);
	$('.paymentCls').val(paymentRdo);
	$('.bankNameCls').val(bankName);
	$('.chequeNumberCls').val(chqNo);
	$('.chequeDateCls').val(chequeDate);

	$('.paymentMethodIdCls').val(paymentMethodId);
	$('.transactionRefNoCls').val(transactionRefNo);
	$('.commentCls').val(comment);

	return true;
}

/**
 * find available qty
 * @param {*} productId 
 */
function qtyAvailable(productId) {
	var productDB = fetchProductByProductId(productId);
	var counterOrderProductDetails = fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId);
	var oldQty = 0;
	for (var i = 0; i < counterOrderProductDetails.length; i++) {
		var product = counterOrderProductDetails[i].product.product;

		if (product.productId == productId) {
			oldQty += counterOrderProductDetails[i].purchaseQuantity;
		}
	}

	var qty = 0;
	for (var i = 0; i < productCart.length; i++) {
		var product = productCart[i][0];
		if (product.productId == productId) {
			qty += parseInt(productCart[i][1]);
		}
	}
	var availableQty = parseInt(productDB.currentQuantity) + parseInt(oldQty) - parseInt(qty);
	return availableQty;
}

/**
 * appends table rows using productCart[] 
 * @returns
 */
function refreshTable() {
	//productCart.push([product,qty,mrp,total,freeNonFreeRdo,disc,discAmt,percentageCheck]);
	$('#cartTbl').empty();
	for (var i = 0; i < productCart.length; i++) {
		var product = productCart[i][0];
		var qty = productCart[i][1];
		var mrp = productCart[i][2];
		var total = parseFloat(productCart[i][3]).toFixedVSS(2);
		var type = productCart[i][4];
		var disc = parseFloat(productCart[i][5]).toFixedVSS(2);
		var discAmt = parseFloat(productCart[i][6]).toFixedVSS(2);
		var percentageCheck = productCart[i][7];

		var productName = '<td>' + product.productName + '</td>';
		var mrpCell = '<td><input type="text" id="productMrp_' + type + '_' + product.productId + '" value=' + mrp + ' class="editable center qty"  style="width: 4em;" /></td>';

		var discountField;
		if (percentageCheck) {
			discountField = '<td><input type="text" id="productDisc_' + type + '_' + product.productId + '" value=' + disc + ' class="editable center numWithDecimal"  style="width: 4em;" /></td>' +
				'<td><span id="productDiscAmt_' + type + '_' + product.productId + '">' + discAmt + '</span></td>';
		} else {
			discountField = '<td><span id="productDisc_' + type + '_' + product.productId + '">' + disc + '</span></td>' +
				'<td><input type="text" id="productDiscAmt_' + type + '_' + product.productId + '" value=' + discAmt + ' class="editable center qty"  style="width: 4em;" /></td>';
		}

		if (type == "Free") {
			productName = '<td>' + product.productName + '<font color="green">-(Free)</font></td>';
			mrpCell = '<td>0</td>';
			discountField = '<td><span id="productDisc_' + type + '_' + product.productId + '">' + disc + '</span></td>' +
				'<td><span id="productDiscAmt_' + type + '_' + product.productId + '">' + discAmt + '</span></td>';
		}

		$('#cartTbl').append('<tr>' +
			//'<td>'+(i+1)+'</td>'+
			productName +
			//'<td><span id="productCurrQty_'+product.productId+'"'+product.currentQuantity+'</span></td>'+
			mrpCell +
			'<td><input type="text" id="productQty_' + type + '_' + product.productId + '" value=' + qty + ' class="editable center qty"  style="width: 4em;" /></td>' +
			discountField +
			'<td><span id="productTotal_' + type + '_' + product.productId + '">' + total + '</span></td>' +
			'<td><button class="editable deleteIcon btn-flat" onclick="deleteFromCart(' + product.productId + ',\'' + type + '\')" ><i class="material-icons" >delete</i></button></td>' +
			'</tr>');



		/**
		* only number allowed without decimal
		*/
		/*	 $('#productMrp_'+type+'_'+product.productId).keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });*/
		/**
		* only number allowed without decimal
		*/
		$('#productMrp_' + type + '_' + product.productId).off("keypress");
		$('#productMrp_' + type + '_' + product.productId).keypress(function (evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		});

		/**
		 * mrp change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productMrp_' + type + '_' + product.productId).off("keyup");
		$('#productMrp_' + type + '_' + product.productId).keyup(function () {
			var id = $(this).attr('id')
			var type = id.split('_')[1];
			var productId = id.split('_')[2];
			var productData;

			for (var i = 0; i < productCart.length; i++) {
				if (productId == productCart[i][0].productId && type == productCart[i][4]) {
					productData = productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}

			var product = productData[0];
			var qty = productData[1];
			var mrp = $('#productMrp_' + type + '_' + productId).val();
			if (mrp == undefined || mrp == '') {
				mrp = 0;
			}
			var total = parseInt(qty) * parseFloat(mrp);

			var percentageCheck = productData[7];
			var disc = productData[5];
			var discAmt = productData[6];

			var discP = productData[8];
			var discAmtP = productData[9];
			var totalP = productData[10];

			/*if(percentageCheck){
				discAmt=(total*disc)/100;
				total=total-discAmt;
				$('#productDiscAmt_'+type+'_'+productId).text(discAmt);
			}else{
				total=total-discAmt;
			}*/
			if (mrp == 0 || qty == 0) {
				if (percentageCheck) {
					discAmt = 0;
				} else {
					disc = 0;
				}
				total = 0;
			} else {
				if (percentageCheck) {
					discAmt = (total * disc) / 100;
					disc = (discAmt / total) * 100;
				} else {
					disc = (discAmt / total) * 100;
					discAmt = (total * disc) / 100;
				}
				total = total - discAmt;
			}


			if (type == "Free") {
				total = 0;
				mrp = 0;
				disc = 0;
				discAmt = 0;

				percentageCheck = false;
			}

			if (percentageCheck) {
				$('#productDisc_' + type + '_' + productId).val(parseFloat(disc).toFixedVSS(2));
				$('#productDiscAmt_' + type + '_' + productId).text(parseFloat(discAmt).toFixedVSS(2));
			} else {
				$('#productDisc_' + type + '_' + productId).text(parseFloat(disc).toFixedVSS(2));
				$('#productDiscAmt_' + type + '_' + productId).val(parseFloat(discAmt).toFixedVSS(2));
			}
			$('#productTotal_' + type + '_' + productId).text(parseFloat(total).toFixedVSS(2));


			productCart.push([
				product,
				qty,
				mrp,
				parseFloat(total).toFixedVSS(2),
				type,
				parseFloat(disc).toFixedVSS(2),
				parseFloat(discAmt).toFixedVSS(2),
				percentageCheck,

				parseFloat(discP).toFixedVSS(2),
				parseFloat(discAmtP).toFixedVSS(2),
				parseFloat(totalP).toFixedVSS(2)]);

			//find final total Amount
			findTotal();

		});
		/**
		 * only number allowed without decimal 
		 */
		$('#productQty_' + type + '_' + product.productId).off("keypress");
		$('#productQty_' + type + '_' + product.productId).keypress(function (event) {
			var key = event.which;
			if (!(key >= 48 && key <= 57 || key === 13))
				event.preventDefault();
		});
		/**
		 * qty change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productQty_' + type + '_' + product.productId).off("keyup");
		$('#productQty_' + type + '_' + product.productId).keyup(function () {
			var id = $(this).attr('id')
			var type = id.split('_')[1];
			var productId = id.split('_')[2];
			var productData;

			for (var i = 0; i < productCart.length; i++) {
				if (productId == productCart[i][0].productId && type == productCart[i][4]) {
					productData = productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}

			var product = productData[0];
			var qty = $('#productQty_' + type + '_' + productId).val();
			var mrp = productData[2];
			if (qty == undefined || qty == '') {
				qty = 0;
			}
			var total = parseInt(qty) * parseFloat(mrp);

			var percentageCheck = productData[7];
			var disc = productData[5];
			var discAmt = productData[6];

			var discP = productData[8];
			var discAmtP = productData[9];
			var totalP = productData[10];

			/*if(percentageCheck){
				discAmt=(total*disc)/100;
				total=total-discAmt;
				$('#productDiscAmt_'+type+'_'+productId).text(discAmt);
			}else{
				total=total-discAmt;
			}*/
			if (mrp == 0 || qty == 0) {
				if (percentageCheck) {
					discAmt = 0;
				} else {
					disc = 0;
				}
				total = 0;
			} else {
				if (percentageCheck) {
					discAmt = (total * disc) / 100;
					disc = (discAmt / total) * 100;
				} else {
					disc = (discAmt / total) * 100;
					discAmt = (total * disc) / 100;
				}
				total = total - discAmt;
			}

			if (type == "Free") {
				total = 0;
				mrp = 0;
				disc = 0;
				discAmt = 0;

				percentageCheck = false;
			}


			if (percentageCheck) {
				$('#productDisc_' + type + '_' + productId).val(parseFloat(disc).toFixedVSS(2));
				$('#productDiscAmt_' + type + '_' + productId).text(parseFloat(discAmt).toFixedVSS(2));
			} else {
				$('#productDisc_' + type + '_' + productId).text(parseFloat(disc).toFixedVSS(2));
				$('#productDiscAmt_' + type + '_' + productId).val(parseFloat(discAmt).toFixedVSS(2));
			}
			$('#productTotal_' + type + '_' + productId).text(parseFloat(total).toFixedVSS(2));


			productCart.push([
				product,
				qty,
				mrp,
				parseFloat(total).toFixedVSS(2),
				type,
				parseFloat(disc).toFixedVSS(2),
				parseFloat(discAmt).toFixedVSS(2),
				percentageCheck,

				parseFloat(discP).toFixedVSS(2),
				parseFloat(discAmtP).toFixedVSS(2),
				parseFloat(totalP).toFixedVSS(2)]);

			//find final total Amount
			findTotal();

		});

		/**
		 * only number allowed without decimal 
		 */
		// This commented on 20-11-2019
		/*$('#productDisc_'+type+'_'+product.productId).off("keypress");
		$('#productDisc_'+type+'_'+product.productId).keypress(function( event ){
		    var key = event.which;						    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});*/

		// 20-11-2019
		$('#productDisc_' + type + '_' + product.productId).keydown(function (e) {
			-1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
		});
		//allowed only numbers with decimal 
		document.getElementById('productDisc_' + type + '_' + product.productId).onkeypress = function (e) {

			if (e.keyCode === 46 && this.value.split('.').length === 2) {
				return false;
			}

		}
		/**
		 * disc change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productDisc_' + type + '_' + product.productId).off("keyup");
		$('#productDisc_' + type + '_' + product.productId).keyup(function () {
			var id = $(this).attr('id')
			var type = id.split('_')[1];
			var productId = id.split('_')[2];
			var productData;

			var disc = $('#productDisc_' + type + '_' + productId).val();
			if (disc > 100 || disc < 0) {
				$(this).val(0);
				$(this).keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Invalid Discount Percent', '3000', 'teal lighten-2');
				return false;
			}

			for (var i = 0; i < productCart.length; i++) {
				if (productId == productCart[i][0].productId && type == productCart[i][4]) {
					productData = productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}

			var product = productData[0];
			var qty = productData[1];
			var mrp = productData[2];

			var discP = productData[8];
			var discAmtP = productData[9];
			var totalP = productData[10];

			var total = parseInt(qty) * parseFloat(mrp);

			var percentageCheck = productData[7];

			if (disc == undefined || disc == '') {
				disc = 0;
			}
			var discAmt = productData[6];



			/*if(percentageCheck){
				discAmt=(total*disc)/100;
				total=total-discAmt;
				$('#productDiscAmt_'+type+'_'+productId).text(discAmt);
			}else{
				total=total-discAmt;
			}*/
			discAmt = (total * disc) / 100;
			total = total - discAmt;

			if (type == "Free") {
				total = 0;
				mrp = 0;
				disc = 0;
				discAmt = 0;

				percentageCheck = false;
			}


			$('#productDiscAmt_' + type + '_' + productId).text(parseFloat(discAmt).toFixedVSS(2));

			$('#productTotal_' + type + '_' + productId).text(parseFloat(total).toFixedVSS(2));


			productCart.push([
				product,
				qty,
				mrp,
				parseFloat(total).toFixedVSS(2),
				type,
				parseFloat(disc).toFixedVSS(2),
				parseFloat(discAmt).toFixedVSS(2),
				percentageCheck,

				parseFloat(discP).toFixedVSS(2),
				parseFloat(discAmtP).toFixedVSS(2),
				parseFloat(totalP).toFixedVSS(2)]);

			//find final total Amount
			findTotal();

		});
		/**
		 * only number allowed without decimal 
		 */
		$('#productDiscAmt_' + type + '_' + product.productId).off("keypress");
		$('#productDiscAmt_' + type + '_' + product.productId).keypress(function (event) {
			var key = event.which;
			if (!(key >= 48 && key <= 57 || key === 13))
				event.preventDefault();
		});
		/**
		 * disc amt change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productDiscAmt_' + type + '_' + product.productId).off("keyup");
		$('#productDiscAmt_' + type + '_' + product.productId).keyup(function () {
			var id = $(this).attr('id')
			var type = id.split('_')[1];
			var productId = id.split('_')[2];
			var productData;

			for (var i = 0; i < productCart.length; i++) {
				if (productId == productCart[i][0].productId && type == productCart[i][4]) {
					productData = productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}

			var product = productData[0];
			var qty = productData[1];
			var mrp = productData[2];

			var discP = productData[8];
			var discAmtP = productData[9];
			var totalP = productData[10];

			var total = parseInt(qty) * parseFloat(mrp);

			var percentageCheck = productData[7];
			var disc = productData[5];
			var discAmt = $('#productDiscAmt_' + type + '_' + productId).val();
			if (discAmt == undefined || discAmt == '') {
				discAmt = 0;
			}

			if (discAmt > total) {
				productCart.push(productData);
				$(this).val(0);
				$(this).keyup();
				Materialize.Toast.removeAll();
				Materialize.toast('Discount Amount Cant be greater than Product Total Amount', '3000', 'teal lighten-2');
				return false;
			}


			/*if(percentageCheck){
				discAmt=(total*disc)/100;
				total=total-discAmt;
				$('#productDiscAmt_'+type+'_'+productId).text(discAmt);
			}else{
				total=total-discAmt;
			}*/

			disc = (discAmt / total) * 100;
			total = total - discAmt;

			if (type == "Free") {
				total = 0;
				mrp = 0;
				disc = 0;
				discAmt = 0;

				percentageCheck = false;
			}



			$('#productDisc_' + type + '_' + productId).text(parseFloat(disc).toFixedVSS(2));

			$('#productTotal_' + type + '_' + productId).text(parseFloat(total).toFixedVSS(2));
			productCart.push([
				product,
				qty,
				mrp,
				parseFloat(total).toFixedVSS(2),
				type,
				parseFloat(disc).toFixedVSS(2),
				parseFloat(discAmt).toFixedVSS(2),
				percentageCheck,

				parseFloat(discP).toFixedVSS(2),
				parseFloat(discAmtP).toFixedVSS(2),
				parseFloat(totalP).toFixedVSS(2)]);

			//find final total Amount
			findTotal();

		});

		if ($('#discountMainCheck').prop("checked") == true) {
			$('.editable').attr('disabled', 'disabled');
		} else {
			$('.editable').removeAttr('disabled');
		}
	}
	$('#cartTbl').append('<tr style="border-top: 2px solid black;">' +
		'<td align="center" >Total </td>' +
		'<td></td>' +
		'<td><span id="totalQty">0</span></td>' +
		'<td></td>' +
		'<td></td>' +
		'<td><span id="totalValue">0</span></td>' +
		'<td></td>' +
		'</tr>');
	$('#cartTbl').append('<tr>' +
		'<td align="center" >Whole Discount</td>' +
		'<td></td>' +
		'<td></td>' +
		'<td><span id="totalDisc">0</span>%</td>' +
		'<td></td>' +
		'<td><span id="totalDiscAmt">0</span></td>' +
		'<td></td>' +
		'</tr>');
	$('#cartTbl').append('<tr style="border-top: 2px solid black;">' +
		'<td align="center" >Net Payable</td>' +
		'<td></td>' +
		'<td></td>' +
		'<td></td>' +
		'<td></td>' +
		'<td><span id="totalValueAftDisc">0</span></td>' +
		'<td></td>' +
		'</tr>');
	//find final total Amount
	findTotal();

}
/**
 * delete product from cart
 * @param {*} productId 
 */
function deleteFromCart(productId, type) {
	for (var i = 0; i < productCart.length; i++) {
		var productCt = productCart[i][0];
		if (productId == productCt.productId && type == productCart[i][4]) {
			//removing productData from current index				
			productCart.splice(i, 1);
			refreshTable();

			if (productCart.length == 0) {

				$('#transport_details').prop("checked", false);
				$('#transport_details_info').toggle();
				$('#transGstNo').val("");
				$('#transVehicleNo').val("");
				$('#transDocketNo').val("");
				$('#transMobNo').val("");
				$('#transportName').val("");
				$("#transport_select option:first").attr('selected', 'selected');

			}

			return false;
		}
	}
}

/**
 * find final totalAmount
 */
function findTotal() {
	var finalTotal = 0;
	var finalQty = 0;
	for (var i = 0; i < productCart.length; i++) {
		finalTotal += parseFloat(productCart[i][3]);
		finalQty += parseInt(productCart[i][1]);
	}
	finalTotal = finalTotal.toFixedVSS(2);

	var discMain = $('#discountMainId').val();
	if (discMain == "") {
		discMain = 0;
	}

	var discountAmount, disc;
	if ($('#percentageMainCheck').prop("checked") == true) {
		disc = discMain;
		discountAmount = (finalTotal * discMain) / 100;
	} else {
		discountAmount = discMain;
		disc = (discountAmount / finalTotal) * 100;
	}
	disc = parseFloat(disc).toFixedVSS(2);
	discountAmount = parseFloat(discountAmount).toFixedVSS(2);

	newTotalAmount = (finalTotal - discountAmount);
	newTotalAmount = Math.round(newTotalAmount);

	$('#totalQty').html('<font color="blue" ><b>' + finalQty + '</b></font>');
	$('#totalValue').html('<font color="blue" ><b>' + finalTotal + '</b></font>');
	$('#totalDisc').html('<font color="green" ><b>' + disc + '</b></font>');
	$('#totalDiscAmt').html('<font color="green" ><b>' + discountAmount + '</b></font>');
	$('#totalValueAftDisc').html('<font color="red" ><b>' + newTotalAmount + '</b></font>');


	var paymentCounterList = fetchPaymentCounterOrder(counterOrderId);
	var paidAmt = 0
	if (paymentCounterList.length > 0) {
		//paidAmt=paymentCounterList[0].totalAmountPaid;
		var paidAmt = 0;
		for (var i = 0; i < paymentCounterList.length; i++) {
			paidAmt += paymentCounterList[i].currentAmountPaid.toFixedVSS(2) - paymentCounterList[i].currentAmountRefund.toFixedVSS(2)
		}
	}
	var totalAmountWithTax = counterOrderOld.totalAmountWithTax;

	if (paidAmt != 0) {
		balAmt = 0;
		var refundAmt = 0, balAmtForPaid = 0;
		//if(paidAmt==counterOrderOld.totalAmountWithTax){ //Fully paid
		if (paidAmt >= newTotalAmount) {//refund
			refundAmt = parseFloat(paidAmt) - parseFloat(newTotalAmount);
			refAmount = Math.round(refundAmt);
			paymentSituation = "Refund";
			$(".paymentSection").hide();
			$('#balanceTotalValue').html('Refund &#8377; <font color="green" ><b>' + Math.round(refundAmt) + '</b></font>');
		} else {//bal amt
			balAmt = parseFloat(newTotalAmount) - parseFloat(paidAmt);
			paymentSituation = "Partiallypaid";
			$(".paymentSection").show();
			refAmount = 0;
			$('#balanceTotalValue').html('Balance &#8377; <font color="red" ><b>' + Math.round(balAmt) + '</b></font>');
		}
		/* }else{// partially paid
			balAmtForPaid=parseFloat(totalAmountWithTax)-parseFloat(paidAmt);
			if(balAmtForPaid>=finalTotal){//refund
				refundAmt=parseFloat(balAmtForPaid)-parseFloat(finalTotal);
				refAmount=refundAmt;
				$('#balanceTotalValue').html('Refund &#8377; <font color="green" ><b>'+refundAmt+'</b></font>');
				paymentSituation="Refund";
				$(".paymentSection").hide();
			}else{//bal amt
				balAmt=parseFloat(finalTotal)-parseFloat(balAmtForPaid);
				paymentSituation="Partiallypaid";
				$(".paymentSection").show();
				refAmount=0;
				$('#balanceTotalValue').html('Balance &#8377; <font color="red" ><b>'+(parseFloat(balAmt)+parseFloat(balAmtForPaid))+'</b></font>');
			}
		} */
		$('#amountPartial').val('');
		$('#amountPartial').change();
		$('#balAmountPartial').val(Math.round(balAmt));
		$('#balAmountPartial').change();

	} else {
		//unpaid 
		paymentSituation = "Partiallypaid";
		$(".paymentSection").show();
		refAmount = 0;
		balAmt = newTotalAmount;
		$('#balanceTotalValue').html('Balance &#8377; <font color="red" ><b>' + Math.round(newTotalAmount) + '</b></font>');

		$('#amountPartial').val('');
		$('#amountPartial').change();
		$('#balAmountPartial').val(Math.round(newTotalAmount));
		$('#balAmountPartial').change();
	}

	//$('#totalValue').html('&#8377; <font color="blue" ><b>'+Math.round(finalTotal)+'</b></font>');

	/* 
	if(paidAmt==counterOrderOld.totalAmountWithTax && paidAmt<finalTotal){
		$(".dueDateExtraPayDiv").show();
	}else{
		$(".dueDateExtraPayDiv").hide();
	} 
	*/
}

/**
 * check current qty in db and change cart current qty according it
 * @param {*} productList 
 */
/* function refreshProductCurrentQuantity(){
	
	var productList=productListData();
	//alert(productList);
	for(var i=0; i<productList.length; i++){
		var productDb=productList[i];
		for(var i=0; i<productCart.length; i++){
			var productCt=productCart[i][0];
			if(productDb.productId==productCt.productId){
				
				productData=productCart[i];
				
				//removing productData from current index				
				productCart.splice(i, 1);
				
				var product=productDb;
				var qty=productData[1];	
				var mrp=productData[2];
				var total=productData[3];
				var type=productData[4];
				
				productCart.push([product,qty,mrp,total,type]);
			}
		}
	}
	
	refreshTable();	
}  */

/**
 * fetch all product list
 */
function productListData() {

	var productList;

	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchProductListAjax",
		dataType: "json",
		async: false,
		success: function (data) {
			productList = data;
		},
		error: function (xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;
}

/**
 * fetch product details by productId
 * @param {*} productId 
 */
function fetchProductByProductId(productId) {

	var product;

	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchProductByProductId?productId=" + productId,
		dataType: "json",
		async: false,
		success: function (data) {
			product = data;
		},
		error: function (xhr, status, error) {
			console.log("product not loaded");
		}
	});
	return product;
}

function fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId) {
	var counterOrderProductDetails;
	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchCounterOrderProductDetailsByCounterOrderId?counterOrderId=" + counterOrderId,
		dataType: "json",
		async: false,
		success: function (data) {
			counterOrderProductDetails = data;
		},
		error: function (xhr, status, error) {
			console.log("counterOrderProductDetails not loaded");
		}
	});
	return counterOrderProductDetails;
}

function fetchCounterOrderByCounterOrderId(counterOrderId) {
	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchCounterOrderByCounterOrderId?counterOrderId=" + counterOrderId,
		dataType: "json",
		async: false,
		success: function (data) {
			counterOrderOld = data.counterOrder;
			gateKeeperName = data.gateKeeperName;
		},
		error: function (xhr, status, error) {
			console.log("counterOrder not loaded");
		}
	});
}

function fetchPaymentCounterOrder(counterOrderId) {
	var paymentCounterList;
	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchPaymentCounterOrder?counterOrderId=" + counterOrderId,
		dataType: "json",
		async: false,
		success: function (data) {
			paymentCounterList = data;
		},
		error: function (xhr, status, error) {
			paymentCounterList = '';
			console.log("paymentCounterList not loaded");
		}
	});
	return paymentCounterList;
}

function submitCounterOrder(formData) {
	$.ajax({
		type: "POST",
		url: myContextPath + "/updateCounterOrderForEdit",
		dataType: "json",
		data: formData,
		async: false,
		beforeSend: function () {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
		},
		success: function (data) {

			if (data.status == "Success") {
				if (isPrintBill) {
					//url to get pdf pring
					var win = window.open(myContextPath + "/counterOrderInvoice.pdf?counterOrderId=" + data.errorMsg, '_blank');
					win.focus();
					win.print();
					Materialize.Toast.removeAll();
					Materialize.toast('Order Updated SuccessFully', '3000', 'teal lighten-2');

					/*setTimeout(function() {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();

						//url to redirect openCounter
						win.close();
						window.location.href=myContextPath+"/counterOrderReport?range=today";
					},5000);*/
					window.location.href = myContextPath + "/counterOrderReport?range=today";
				} else {
					//$('.preloader-wrapper').hide();
					//$('.preloader-background').hide();
					Materialize.Toast.removeAll();
					Materialize.toast('Order Updated SuccessFully', '3000', 'teal lighten-2');
					setTimeout(function () {
						window.location.href = myContextPath + "/counterOrderReport?range=today";
					}, 3000);
				}
			} else {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Order Creating Failed', '3000', 'red lighten-2');
			}
		},
		error: function (xhr, status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			console.log("Something Went Wrong");
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '3000', 'teal lighten-2');
		}
	});
}

function resetProduct() {
	var source1 = $("#productId");
	source1.val(0);
	source1.change();

	$('#quantityId').val('');

	$('#nonFreeId').click();
}