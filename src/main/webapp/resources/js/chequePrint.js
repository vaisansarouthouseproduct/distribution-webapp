var intervalType="";
	var allPreviewId = "";
	var selectedPreviewListId = [];
	var indexOfPreview = 0;
	var imgSrc = "";
	var dataList=[];
	var cbId=0;
	
	/*for clicking on toast no button print data div display none*/
	function onNoClick(){
		$('.contentData').css('display','none');
		Materialize.Toast.removeAll();
	}
	
	/*convert numbers into words for cheque amount*/
	function convertNumberToWords(amount) {
		var words = new Array();
		words[0] = '';
		words[1] = 'One';
		words[2] = 'Two';
		words[3] = 'Three';
		words[4] = 'Four';
		words[5] = 'Five';
		words[6] = 'Six';
		words[7] = 'Seven';
		words[8] = 'Eight';
		words[9] = 'Nine';
		words[10] = 'Ten';
		words[11] = 'Eleven';
		words[12] = 'Twelve';
		words[13] = 'Thirteen';
		words[14] = 'Fourteen';
		words[15] = 'Fifteen';
		words[16] = 'Sixteen';
		words[17] = 'Seventeen';
		words[18] = 'Eighteen';
		words[19] = 'Nineteen';
		words[20] = 'Twenty';
		words[30] = 'Thirty';
		words[40] = 'Forty';
		words[50] = 'Fifty';
		words[60] = 'Sixty';
		words[70] = 'Seventy';
		words[80] = 'Eighty';
		words[90] = 'Ninety';
		amount = amount.toString();
		var atemp = amount.split(".");
		var number = atemp[0].split(",").join("");
		var n_length = number.length;
		var words_string = "";
		if (n_length <= 9) {
			var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
			var received_n_array = new Array();
			for (var i = 0; i < n_length; i++) {
				received_n_array[i] = number.substr(i, 1);
			}
			for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
				n_array[i] = received_n_array[j];
			}
			for (var i = 0, j = 1; i < 9; i++, j++) {
				if (i == 0 || i == 2 || i == 4 || i == 7) {
					if (n_array[i] == 1) {
						n_array[j] = 10 + parseInt(n_array[j]);
						n_array[i] = 0;
					}
				}
			}
			value = "";
			for (var i = 0; i < 9; i++) {
				if (i == 0 || i == 2 || i == 4 || i == 7) {
					value = n_array[i] * 10;
				} else {
					value = n_array[i];
				}
				if (value != 0) {
					words_string += words[value] + " ";
				}
				if ((i == 1 && value != 0)
						|| (i == 0 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Crores ";
				}
				if ((i == 3 && value != 0)
						|| (i == 2 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Lakhs ";
				}
				if ((i == 5 && value != 0)
						|| (i == 4 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Thousand ";
				}
				if (i == 6 && value != 0
						&& (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
					words_string += "Hundred and ";
				} else if (i == 6 && value != 0) {
					words_string += "Hundred ";
				}
			}
			words_string = words_string.split("  ").join(" ");
		}
		return words_string;
	}	// to print the Data
		
	/*function called after cliccking on print btn*/
	function printData() {
		console.log(selectedPreviewListId.length);
		console.log(selectedPreviewListId);
		$('.modal-content').empty();
		$('.sliderBtnsDiv').css('display','none');
		if (selectedPreviewListId.length > 0) {
			/* $('.sliderBtnsDiv').css('display','none'); */
			for (var i = 0; i < selectedPreviewListId.length; i++) {
				var id = selectedPreviewListId[i];
				
				// blocking previous button initially
				$("#previousPreviewtBtn").attr('disabled','disabled');
			
				previewDataSet(id);
				
			}
			
			if (selectedPreviewListId.length > 1) {
				$('.sliderBtnsDiv').css('display','block');
			}
			$('.container1').css('display','none');
		
			$('.container1').eq(0).css('display','block');
			//$('#chqPreviewModal').modal('open');
			
			var content=$('.modal-content').html();
			$('#contentData').html(content);
			$('.container1').css('display','block');
			var $toastContent = $('<span>Do you want to print?<span>').add('<button class="btn teal" style="margin-left:10px" onclick="sendingBulkData()">Yes</button><button class="btn red" style="margin-left:10px" onClick="onNoClick()">No</button>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent);
			
			

		}else{
			// if no row selected for print
			var $toastContent = $('<span>No cheque is available for Print<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}
	}
	
	/*function to show preview modal*/
	function showPreview() {
		console.log(selectedPreviewListId.length);
		console.log(selectedPreviewListId);
		$('.modal-content').empty();
		$('.sliderBtnsDiv').css('display','none');
		if (selectedPreviewListId.length > 0) {
			/* $('.sliderBtnsDiv').css('display','none'); */
			for (var i = 0; i < selectedPreviewListId.length; i++) {
				var id = selectedPreviewListId[i];
				
				// blocking previous button initially
				$("#previousPreviewtBtn").attr('disabled','disabled');			
				previewDataSet(id);
				
			}
			
			if (selectedPreviewListId.length > 1) {
				$('.sliderBtnsDiv').css('display','block');
			}
			$('.container1').css('display','none');
		
			$('.container1').eq(0).css('display','block');
			$('#chqPreviewModal').modal('open');
		}else{
			// if no row selected for print
			var $toastContent = $('<span>No cheque is available for Print<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}
	}
	
	

	
	/*function to change the format of date*/
	function dateFormatter(date){
		var d = new Date(date);
		var date = d.getDate();
		var month = d.getMonth()+1;
		var year = d.getFullYear();
		//var name = $(".heading").text();
		return date + '-'
				+ month + '-' + year;
	
}
	
	//to get the end date with intervalType,startDate,and actual Date ex. startDate:29/09/2018 interverType:"Monthly/BiMonthly" date:29
	function getEndDateByIntervalAndStartDate(startDate,intervalType,date){
		//var endDate="";
		if(intervalType=="Monthly"){
			var endDate=startDate;
			var lastMonth=endDate.getMonth();
			var nextMonth=0;
			
	
				if(startDate.getMonth()==11){
					endDate=new Date(startDate.getFullYear()+1, startDate.getMonth()-11, startDate.getDate());
				}else{
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+1, startDate.getDate())
				}
				if(date!=endDate.getDate()){
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+1, date)
				}
				nextMonth=endDate.getMonth();
				if(nextMonth!=0){
					if(nextMonth-lastMonth!=1){
						var lastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
						endDate = new Date(endDate.getFullYear(), endDate.getMonth()-1, lastDay.getDate());
					}
				}
				
				//startDate=endDate;
				
			
		}else if(intervalType=="BiMonthly"){
			var endDate=startDate;
			var lastMonth=endDate.getMonth();
			var nextMonth=0;
			
	
				if(startDate.getMonth()==11){
					endDate=new Date(startDate.getFullYear()+1, startDate.getMonth()-10, startDate.getDate());
				}else{
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+2, startDate.getDate())
				}
				if(date!=endDate.getDate()){
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+2, date)
				}
				nextMonth=endDate.getMonth();
				if(nextMonth!=0){
					if(nextMonth-lastMonth!=2){
						var lastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
						endDate = new Date(endDate.getFullYear(), endDate.getMonth()-1, lastDay.getDate());
					}
				}
		}else{
			var endDate=startDate;
			var lastMonth=endDate.getMonth();
			var nextMonth=0;
			
	
				if(startDate.getMonth()==11){
					endDate=new Date(startDate.getFullYear()+1, startDate.getMonth()-9, startDate.getDate());
				}else{
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+3, startDate.getDate())
				}
				if(date!=endDate.getDate()){
					endDate=new Date(startDate.getFullYear(), startDate.getMonth()+3, date)
				}
				nextMonth=endDate.getMonth();
				if(nextMonth!=0 || nextMonth!=11){
					
					if(nextMonth-lastMonth!=3){
						var lastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
						endDate = new Date(endDate.getFullYear(), endDate.getMonth()-1, lastDay.getDate());
					}
					if(nextMonth-lastMonth<3){
						var last = new Date(endDate.getFullYear(), endDate.getMonth()+1, 0);
						endDate = new Date(endDate.getFullYear(), endDate.getMonth()+1, lastDay.getDate());
					}
					
					//to adjust the Date with the given Date
					if(date!=endDate.getDate()){
						endDate=new Date(endDate.getFullYear(), endDate.getMonth(), date)
					}
				}
		}
		
		return endDate;
	
		
	} 
	$(document).ready(function(){
		$("#nextPreviewtBtn").click(function() {
			$("#previousPreviewtBtn").removeAttr('disabled','disabled');
							if (indexOfPreview == selectedPreviewListId.length - 1) {
								//window.alert("This is the last Cheque of Preview")
								$(this).attr('disabled','disabled');
							} else {
								indexOfPreview = indexOfPreview + 1;
								/* previewDataSet(selectedPreviewListId[indexOfPreview]) */
								$('.container1').css('display','none');
								var id=selectedPreviewListId[indexOfPreview];
								$('#container_'+id).css('display','block');
							}
							if(indexOfPreview==selectedPreviewListId.length - 1){
								$(this).attr('disabled','disabled');
							}

						});

		$("#previousPreviewtBtn").click(function() {
			$("#nextPreviewtBtn").removeAttr('disabled','disabled');
							if (indexOfPreview == 0) {
								/* window.alert("This is the First Cheque of preview"); */
								$(this).attr('disabled','disabled');
							} else {
								indexOfPreview = indexOfPreview - 1;
								/* previewDataSet(selectedPreviewListId[indexOfPreview]) */
								$('.container1').css('display','none');
								var id=selectedPreviewListId[indexOfPreview];
								$('#container_'+id).css('display','block');
							}
							if(indexOfPreview==0){
								$(this).attr('disabled','disabled');	
							}

						});
		
		 //onclicking on Print button
        $("#printBtn").click(function() {
				var content=$('.modal-content').html();
				$('#contentData').html(content);
				$('.container1').css('display','block');
				var $toastContent = $('<span>Do you want to print?<span>').add('<button class="btn teal" style="margin-left:10px" onclick="sendingBulkData()">Yes</button><button class="btn red" style="margin-left:10px" onClick="onNoClick()">No</button>');
				Materialize.Toast.removeAll();
				Materialize.toast($toastContent);
				

			});
	});