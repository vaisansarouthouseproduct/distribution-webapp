//tofixed all decimal to given places
Number.prototype.toFixedVSS = function(places) {
	try{
		var value = this;
		//console.log("toFixedVSS value : "+value);
		var stringValue = value+"";
	    var strInfo = stringValue.split(".");
	    if(strInfo.length==1)
	    	return value+"";
	    if(places<0) throw new Error('Invalid Argument for places');
	    if(places==0){
	    	return parseFloat(value).toFixed(0)+"";
	    }else{
	    	var length = parseInt(strInfo[1].length);
	    	for(var i=length;i>places;i--){
		    	var roundOffPlaces = parseInt(i-1);
		    	value=roundFromFloatMain(value,roundOffPlaces);
		    	stringValue = value+"";
		    	strInfo = stringValue.split(".");
		    	if(strInfo.length==1)
		        	return value+"";
			    var newLength = parseInt(strInfo[1].length);
			    if(newLength<=places){
			    	return value+"";
			    }else{
			    	var diff=roundOffPlaces -  newLength;
			    	if(diff!=0)
			    		i=i-diff;
			    }
		    }
		    return value+"";
	    }
	}catch(err){
		return value.toFixed(places);
	}
	
};

//round of number to given places
var roundFromFloatMain =  function(value,places){
	if (places < 0) throw new IllegalArgumentException();
    var info = (value+"").split(".");
    
    var integralPartStr = info[0];
    var fractionalPartStr = info[1];
    
    var valueAfterRoundingPlace = parseInt(fractionalPartStr.substring(places));
    if(valueAfterRoundingPlace>=5){
    	var valueToBeAdded="0.";
    	for(var i=0;i<places-1;i++){
	    	valueToBeAdded +="0";
	    }
	    valueToBeAdded+="1";
	    fractionalPartStr = fractionalPartStr.substring(0, places);
	    var oldValue = parseFloat(integralPartStr+"."+fractionalPartStr);
	    var floatAddition = parseFloat(valueToBeAdded);
	    var newValue = oldValue+floatAddition;
	    newValue = parseFloat(newValue.toFixed(places));
	    return newValue;
    }else{
    	var valueToBeAdded="0.";
    	for(var i=0;i<places-1;i++){
	    	valueToBeAdded +="0";
	    }
	    valueToBeAdded+="0";
	    fractionalPartStr = fractionalPartStr.substring(0, places);
	    var oldValue = parseFloat(integralPartStr+"."+fractionalPartStr);
	    var floatAddition = parseFloat(valueToBeAdded);
	    var newValue = oldValue+floatAddition;
	    newValue = parseFloat(newValue.toFixed(places));
	    return newValue;
    }	
}


//calculate unit price,igst,sgst,cgst amount basis of mrp and igst percentage
function calculateProperTax(mrp,igstTaxPercentage){
	mrp=parseFloat(mrp)
	
	//unit price
	var unitPrice=(
					mrp/
						(
							(igstTaxPercentage+100)/100
						)
				  );
	
	//cgst amount
	var cgst=(
				unitPrice.toFixedVSS(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//sgst amount
	var sgst=(
				unitPrice.toFixedVSS(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//Igst amount
	var igst=parseFloat(cgst.toFixedVSS(2))+parseFloat(sgst.toFixedVSS(2));
	//Newmrp for comparing entered mrp  and calculated mrp
	var newMrp=parseFloat(unitPrice.toFixedVSS(2))+parseFloat(igst);
	
	//go on second case when calculated mrp and given mrp is not matched
	if(parseFloat(mrp)!==parseFloat(newMrp))
	{
		igst=(
				unitPrice.toFixedVSS(2)*
				(
					igstTaxPercentage/100
				)
			  );
		igst=parseFloat(igst).toFixedVSS(3);
		
		//calculate cgst,sgst basis of igst amount
		cgst=parseFloat(parseFloat(igst).toFixedVSS(2))/2;
		sgst=parseFloat(parseFloat(igst).toFixedVSS(2))/2;
		
		newMrp=parseFloat(unitPrice.toFixedVSS(2))+parseFloat(parseFloat(igst).toFixedVSS(2));
		
		//if calculated mrp and old mrp not match then
		//get diff and add it to unitprice
		if(parseFloat(mrp)!==parseFloat(newMrp)){
			
			var mrpDiff=mrp-newMrp;
			unitPrice=unitPrice+mrpDiff;
			
			var data={
					mrp:mrp.toFixedVSS(2),
					unitPrice:unitPrice.toFixedVSS(2),
					cgst:cgst.toFixedVSS(3),
					sgst:sgst.toFixedVSS(3),
					igst:parseFloat(igst).toFixedVSS(2)
				};
				return data;
			
		}else{
		
			var data={
					mrp:newMrp.toFixedVSS(2),
					unitPrice:unitPrice.toFixedVSS(2),
					cgst:cgst.toFixedVSS(3),
					sgst:sgst.toFixedVSS(3),
					igst:parseFloat(igst).toFixedVSS(2)
				};
				return data;
		}
	}
	else
	{
		var data={
			mrp:mrp.toFixedVSS(2),
			unitPrice:unitPrice.toFixedVSS(2),
			cgst:cgst.toFixedVSS(2),
			sgst:sgst.toFixedVSS(2),
			igst:parseFloat(igst).toFixedVSS(2)
		};
		return data;
	}
}

//not using
function calculateProperTaxOld(mrp,igstTaxPercentage){
	mrp=parseFloat(mrp)
	
	//unit price
	var unitPrice=(
					mrp/
						(
							(igstTaxPercentage+100)/100
						)
				  );
	
	//cgst amount
	var cgst=(
				unitPrice.toFixedVSS(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//sgst amount
	var sgst=(
				unitPrice.toFixedVSS(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//Igst amount
	var igst=parseFloat(cgst.toFixedVSS(2))+parseFloat(sgst.toFixedVSS(2));
	//Newmrp for comparing entered mrp  and calculated mrp
	var newMrp=parseFloat(unitPrice.toFixedVSS(2))+parseFloat(igst);
	
	
	if(parseFloat(mrp)!==parseFloat(newMrp))
	{
		igst=(
				unitPrice.toFixedVSS(2)*
				(
					igstTaxPercentage/100
				)
			  );
		igst=parseFloat(igst).toFixedVSS(3);
		cgst=parseFloat(parseFloat(igst).toFixedVSS(2))/2;
		sgst=parseFloat(parseFloat(igst).toFixedVSS(2))/2;
		newMrp=parseFloat(unitPrice.toFixedVSS(2))+parseFloat(parseFloat(igst).toFixedVSS(2));
		
		if(parseFloat(mrp)!==parseFloat(newMrp)){
			
			var mrpDiff=mrp-newMrp;
			unitPrice=unitPrice+mrpDiff;
			
			var data={
					mrp:mrp.toFixedVSS(2),
					unitPrice:unitPrice.toFixedVSS(2),
					cgst:cgst.toFixedVSS(3),
					sgst:sgst.toFixedVSS(3),
					igst:parseFloat(igst).toFixedVSS(2)
				};
				return data;
			
		}else{
		
			var data={
					mrp:newMrp.toFixedVSS(2),
					unitPrice:unitPrice.toFixedVSS(2),
					cgst:cgst.toFixedVSS(3),
					sgst:sgst.toFixedVSS(3),
					igst:parseFloat(igst).toFixedVSS(2)
				};
				return data;
		}
	}
	else
	{
		var data={
			mrp:mrp.toFixedVSS(2),
			unitPrice:unitPrice.toFixedVSS(2),
			cgst:cgst.toFixedVSS(2),
			sgst:sgst.toFixedVSS(2),
			igst:parseFloat(igst).toFixedVSS(2)
		};
		return data;
	}
}