var barCodeArray =[];

function maskAllowedCharacter(event) {
    var regex = new RegExp("[$*]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function maskModelOpen(){
	$('#maskDetailsModal').modal('open');
}
function barcodeImageModelOpen(){
	$('.modal-content').empty();
	callGeneratebarCode();
	
}

function generateSingleBarcode(value1, btype, renderer, t) {
    // var value = $("#barcodeValue").val();
    // var btype = 'code128';
    // var renderer ='css';


    var bgColor=$('#background-color-picker').val();
   
    var barColor=$('#bar-color-picker').val();
    
    if(renderer=="svg"){
	 bgColor = bgColor.replace('#','%23');
	 barColor = barColor.replace('#','%23');
    }
    var barHeight=$('#barHeight').val();
    var barWidth=$('#barWidth').val();
    
    var settings = {
        output: renderer,
        bgColor: bgColor,
        color: barColor,
        barWidth: barWidth,
        barHeight: barHeight,
        moduleSize: "1",
        posX: "10",
        posY: "50",
        addQuietZone: "1"
    };

    value1=value1.toString();

    var mask= $("#maskId").val();
    var paddingCount=mask.length;
    var padding="";
    
    if(mask.indexOf('$') > -1){
        padding="zero"; 
        for(var j1=0;j1<mask.length;j1++){
            value1="0"+value1;
        }
    }else{
        for(var j1=0;j1<mask.length;j1++){
            value1="*"+value1;
        }
        padding="asterisk";
    }
    var barcodeDivId = "barcodeTarget" + t;
   
    /* $('#'+barcodeDivId).html("").show().barcode(value1, btype, settings); */
    var span='<div id="' + barcodeDivId + '" class="barcode1 spanBarcode barcodeImage modal left svg-content"></div>';
    $('.modal-content').append(span);
    //$('.modal-content').append('<div id="' + barcodeDivId + '" class="barcode1 barcodeImage modal left" style="position: relative;left:20px;top:40px"></div>');
    $('#'+barcodeDivId).barcode(value1, btype, settings);
    barCodeArray.push($('#'+barcodeDivId).html());
   
    /* $('.modal-content').append('<div id="' + barcodeDivId + '" class="barcode1 barcodeImage modal left" style="position: relative"></div>');
    $('#'+barcodeDivId).html("").show().barcode(value1, btype, settings); */
}

function lastValueValidation(){
	var endVal=$('#endValue').val();
	var startVal=$('#startValue').val();
	var incrementNo=$('#increament').val();
	if(incrementNo=="")
		incrementNo=1;
	if(parseFloat(endVal)< parseFloat(startVal)){
		$('#endValue').val("");
		var $toastContent = $('<span>Last value must be greater then start value<span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent,2000);
	}else if(((parseFloat(endVal)-parseFloat(startVal))/incrementNo)>1000){
		$('#endValue').val("");
		var $toastContent = $('<span>Max 1000 barcode can generate at a time<span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent,2000);
	}
}



function barcodeDataset(){
	 $('.modal-content').empty();
      
      $('.modal-content').append('<table class="barcodeTable"><tbody></tbody></table>');
       
     
      var colCount=$('#columnCount').val();
      var l=0;
     $('.barcodeTable tbody').empty();
     /*  barCodeArray=$('.spanBarcode').toArray();
      console.log(barCodeArray); */
       //for loop for barcode.length for generting tr and td
       for(var i=0;i<barCodeArray.length;){
       	$('.barcodeTable tbody').append('<tr id='+l+' class="trow"></tr>');
       	
       	var trId=l;
       	for(var k=0;k<colCount;k++){
       		
       		if(i==barCodeArray.length){
       			break;
       		}
       		else{
       			$('#'+trId).append('<td>'+barCodeArray[i]+'</td>')
       			i++;
       		}
       		
       	}
       	l++;
       	
       }
}