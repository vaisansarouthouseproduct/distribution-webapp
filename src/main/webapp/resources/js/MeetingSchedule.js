var businessNamesList;
$(document).ready(function() {
	
	
	$('#departmentId').change(function(){
		
		var departmentId=$('#departmentId').val();
		
		// Get the raw DOM object for the select box
		var employeeIdselect = document.getElementById('employeeId');

		// Clear the old options
		employeeIdselect.options.length = 0;
		
		employeeIdselect.options.add(new Option("Employee", '0'));
		
		if(departmentId==0){
			return false;
		}
		
		$.ajax({
			type:'POST',
			url: myContextPath+"/fetchEmployeeDetailsByDepartmentId/"+departmentId,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async: false,
			success : function(response){
				if(response.status=="Success"){
					//setting employee list
					var employeeDetailsList=response.employeeDetailsList;
					if(employeeDetailsList!=null){
						var options, index, option;
						employeeIdselect = document.getElementById('employeeId');
						for (var i = 0, len = employeeDetailsList.length; i < len; ++i) {
							var employeeDetails = employeeDetailsList[i];
							employeeIdselect.options.add(new Option(employeeDetails.name, employeeDetails.employeeId));
						}
					}
				}else{
					Materialize.Toast.removeAll();
					Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
	});
	
	$('#employeeId').change(function(){
		
		var employeeId=$('#employeeId').val();
		
		// Get the raw DOM object for the select box
		var businessSelect = document.getElementById('businessId');

		// Clear the old options
		businessSelect.options.length = 0;
		
		businessSelect.options.add(new Option("Business", '0'));
		
		if(employeeId==0){
			return false;
		}
		
		$.ajax({
			type:'POST',
			url: myContextPath+"/fetchBusinessListByEmployeeId/"+employeeId,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async: false,
			success : function(response){
				if(response.status=="Success"){
					//setting employee list
					businessNamesList=response.businessNamesList;
					if(businessNamesList!=null){
						var options, index, option;
						businessSelect = document.getElementById('businessId');
						for (var i = 0, len = businessNamesList.length; i < len; ++i) {
							var businessNames = businessNamesList[i];
							businessSelect.options.add(new Option(businessNames.shopName, businessNames.businessNameId));
						}
					}
				}else{
					Materialize.Toast.removeAll();
					// Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
		businessSelect.options.add(new Option("Other", 'Other'));
	});
	
	$('#businessId').change(function(){
		
		var businessNameId=$('#businessId').val();
		setBusinessDetails(businessNameId);
		
	});
	
	$('#setMeetingSubmit').click(function(){
		var employeeId=$('#employeeId').val();
		var date=$('#meetingDate').val();
		var startTime=$('#meetingStartTime').val();
		var endTime=$('#meetingEndTime').val();
		
		startTime=moment(moment(startTime, 'hh:mma').toDate()).format('hh:mm a');
		endTime=moment(moment(endTime, 'hh:mma').toDate()).format('hh:mm a');
		
		if(employeeId!='0' && date!='' && date!=undefined && startTime!='' && startTime!=undefined && endTime!='' && endTime!=undefined){
			if(isUpdate){
				var meetingId=$('#meetingId').val();
				if(checkMeetingScheduleForUpdate(employeeId,meetingId,date,startTime,endTime)){
					return false;
				}
			}else{
				if(checkMeetingScheduleForSave(employeeId,date,startTime,endTime)){
					return false;
				}
			}
		}
		
	});
	
	$('#meetingStartTime').change(function(){
		var startTime=$('#meetingStartTime').val();
		if(startTime!='' && startTime!=undefined){
			startTime=moment(moment(startTime, 'hh:mma').toDate()).format('hh:mm a');
			$('#meetingStartTime').val(startTime);
		}
	});
	$('#meetingEndTime').change(function(){
		var endTime=$('#meetingEndTime').val();
		if(endTime!='' && endTime!=undefined){
			endTime=moment(moment(endTime, 'hh:mma').toDate()).format('hh:mm a');
			$('#meetingEndTime').val(endTime);
		}
	});
	if(isUpdate){
		$('#departmentId').change();
		
		$('.preloader-wrapper').show();
		$('.preloader-background').show();
		setTimeout(function(){
					  	var source=$('#employeeId');
						source.val(employeeId);
						source.change();
						// $('.preloader-wrapper').show();
						// $('.preloader-background').show();

						if(businessId!='' && businessId!=undefined){
							setTimeout(function(){
								  var source=$('#businessId');
								source.val(businessId);
								source.change();
								$('.preloader-wrapper').hide();
								$('.preloader-background').hide();
							}, 2000);
						}else{
							var source=$('#businessId');
							source.val("Other");
							source.change();
							
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
							
							$('#ownerName').val(ownerName);
							$('#mobileNumber').val(mobileNumber);
							$('#shopName').val(shopName);
							$('#address').val(address);
						}
				  }, 1000);
		
	}
	if(url=="reschedule_meeting"){
		$('#ownerName').prop('readonly', true);
		$('#mobileNumber').prop('readonly', true);
		$('#shopName').prop('readonly', true);
		$('#address').prop('readonly', true);
		$('#departmentId').prop('disabled', true);
		$('#employeeId').prop('disabled', true);
		$('#businessId').prop('disabled', true);
	}
	
	$('#ownerName').prop('readonly', true);
	$('#mobileNumber').prop('readonly', true);
	$('#shopName').prop('readonly', true);
	$('#address').prop('readonly', true);
	
	
});

function checkMeetingScheduleForSave(employeeId,date, startTime, endTime){
	var isMeetingScheduled=false;
	$.ajax({
		type:'POST',
		url: myContextPath+"/checkMeetingScheduleForSave", 
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify({'employeeId':employeeId,'date':date,'fromTime':startTime,'toTime':endTime}),
		async: false,
		success : function(response){
			if(response==""){
				isMeetingScheduled=false;
			}else{
				isMeetingScheduled=true;
				Materialize.Toast.removeAll();
				Materialize.toast(response, '2000', 'red lighten-2');
			}
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
	
	return isMeetingScheduled;
}

function checkMeetingScheduleForUpdate(employeeId,meetingId,date, startTime, endTime){
	var isMeetingScheduled=false;
	$.ajax({
		type:'POST',
		url: myContextPath+"/checkMeetingScheduleForUpdate", 
		headers : {
			'Content-Type' : 'application/json'
		},
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		data:JSON.stringify({'employeeId':employeeId,'date':date,'fromTime':startTime,'toTime':endTime,'meetingId':meetingId}),
		async: false,
		success : function(response){
			if(response==""){
				isMeetingScheduled=false;
			}else{
				isMeetingScheduled=true;
				Materialize.Toast.removeAll();
				Materialize.toast(response, '2000', 'red lighten-2');
			}
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error){
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
		}		
	});
	
	return isMeetingScheduled;
}

function setBusinessDetails(businessNameId){
	
	$('#ownerName').val('');
	$('#mobileNumber').val('');
	$('#shopName').val('');
	$('#address').val('');
	
	var businessName=null;
	if(businessNamesList!=undefined){
		for(var i=0; i<businessNamesList.length; i++){		
			if(businessNamesList[i].businessNameId==businessNameId){
				businessName=businessNamesList[i];
				break;
			}
		}
	}
	
	if(businessName!=null){
		$('#ownerName').val(businessName.ownerName);
		$('#mobileNumber').val(businessName.contact.mobileNumber);
		$('#shopName').val(businessName.shopName);
		$('#address').val(businessName.address);
		
		$('#ownerName').prop('readonly', true);
		$('#mobileNumber').prop('readonly', true);
		$('#shopName').prop('readonly', true);
		$('#address').prop('readonly', true);
	}else{
		$('#ownerName').prop('readonly', false);
		$('#mobileNumber').prop('readonly', false);
		$('#shopName').prop('readonly', false);
		$('#address').prop('readonly', false);
	}
	
	$('#ownerName').change();
	$('#mobileNumber').change();
	$('#shopName').change();
	$('#address').change();
}