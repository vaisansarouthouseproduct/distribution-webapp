$(document).ready(function() {

	/**
	 * quantity given restrict only on number
	 */
	$('#qtyGiven').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	/**
	 * if quantity given change
	 * compare it with damage quantity 
	 * 
	 */
	$('#qtyGiven').keyup(function(){
		
		var qtyDamage=$('#qtyDamage').val();
		var qtyGiven=$('#qtyGiven').val();
		
		if(qtyDamage=='' || qtyDamage==undefined){
			qtyDamage=0;
		}		
		if(qtyGiven=='' || qtyGiven==undefined){
			qtyGiven=0;
		}
		if(parseInt(qtyGiven)>parseInt(qtyDamage))
		{	
			Materialize.Toast.removeAll();
			Materialize.toast('Given Quantity must be same or less than Damage Quantity', '3000', 'teal lighten-3');
			$('#qtyGiven').val(0);
			$('#qtyGiven').change();
		}		
		
	
	});
	/**
	 * quantity receive restrict only on number
	 */
	$('#qtyReceived').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	/**
	 * if quantity receive change
	 * compare it with given quantity 
	 * 
	 */
	$('#qtyReceived').keyup(function(){
		
		var qtyReceived=$('#qtyReceived').val();
		var qtyGiven=$('#givenQuantityId').val();
		
		if(qtyDamage=='' || qtyDamage==undefined){
			qtyDamage=0;
		}		
		if(qtyGiven=='' || qtyGiven==undefined){
			qtyGiven=0;
		}
		if(parseInt(qtyReceived)>parseInt(qtyGiven))
		{
			Materialize.Toast.removeAll();
			Materialize.toast('Received Quantity must be same or less than Given Quantity is : '+qtyGiven, '3000', 'teal lighten-3');
			$('#qtyReceived').val(0);
			$('#qtyReceived').change();
		}	
		if(parseInt(qtyReceived)<parseInt(qtyGiven))
		{
			$('#rejectReason').prop('required', "required");
			$('#rejectReason').removeAttr('disabled');
		}
		else if(parseInt(qtyReceived)>=parseInt(qtyGiven))
		{
			$('#rejectReason').removeAttr('required');
			$('#rejectReason').prop('disabled', "disabled");
		}
	});
	
	/*$('#productId').change(function(){
		var productId=$('#productId').val();
		$('#supplierId').empty();
		$("#supplierId").append('<option value="">Choose Supplier</option>');	
		$('#qtyDamage').val(0);
		$('#qtyDamage').change();
		$.ajax({
			url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
			dataType : "json",
			success : function(data) {
				supplierProductLists=data;
				
				$('#qtyDamage').val(supplierProductLists[0].product.damageQuantity);
				$('#qtyDamage').change();
				for(var i=0; i<supplierProductLists.length; i++)
				{								
					$("#supplierId").append('<option value='+supplierProductLists[i].supplier.supplierId+'>'+supplierProductLists[i].supplier.name+'</option>');
				}	
				
				$('#supplierId').change();
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '3000', 'teal lighten-3');
				$('#addeditmsg').modal('open');
	   	     	$('#msgHead').text("Supplier Order : ");
	   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); 
			}
		});
		
	});*/
	
	/**
	 * when give some quantity to supplier for recovery
	 * validate given quantity
	 */
	$('#addDamageQty').click(function(){
		
		var qtyGiven=$('#qtyGiven').val();
		if(parseInt(qtyGiven)==0)
		{
			Materialize.Toast.removeAll();
			Materialize.toast('Given Quantity must be Non Zero', '3000', 'teal lighten-3');
			return false;
		}
	});
	
});
/**
 * showing damage quantity given/recovery to supplier list against damageRecoveryId
 * @param damageRecoveryId
 * @returns
 */
function showDamageDetails(damageRecoveryId)
{
	$("#damageProductDetailsId").empty();
	$("#damageProductDetailsFooter").empty();
	$.ajax({
		url : myContextPath+"/fetchDamageRecoveryDetailsListByDamageRecoveryId?damageRecoveryId="+damageRecoveryId,
		dataType : "json",
		success : function(data) {
			var damageRecoveryDetailsList=data;
			var srno=1;			
			var quantityGivenTotal=0,quantityReceivedTotal=0,quantityNotClaimedTotal=0;
			
			for(var i=0; i<damageRecoveryDetailsList.length; i++)
			{			
				var damageRecoveryDetails=damageRecoveryDetailsList[i];
				var givenDate=moment(damageRecoveryDetails.givenDate).format("DD-MM-YYYY");//DD MMM YYYY hh:mm a
				var receiveDate;
				var buttonProperty="";
				if(damageRecoveryDetails.receiveStatus==true)
				{
					receiveDate=moment(damageRecoveryDetails.receiveDate).format("DD-MM-YYYY");//DD MMM YYYY hh:mm a
					buttonProperty="disabled";
				}
				else
				{
					receiveDate="NA";
					buttonProperty='onclick="updateReceivedQuantity(\''+damageRecoveryDetails.damageRecoveryDetailsId+'\','+damageRecoveryDetails.quantityGiven+')"';					
				}
				
				$("#damageProductDetailsId").append(
					'<tr>'+
						'<td>'+damageRecoveryDetails.damageRecoveryDetailsId+'</td>'+
						'<td>'+damageRecoveryDetails.supplier.name+'</td>'+
						'<td>'+damageRecoveryDetails.quantityGiven+'</td>'+
						'<td>'+damageRecoveryDetails.quantityReceived+'</td>'+
						'<td>'+givenDate+'</td>'+
						'<td>'+receiveDate+'</td>'+
						'<td>'+damageRecoveryDetails.quantityNotClaimed+'</td>'+
						'<td>'+damageRecoveryDetails.rejectQuantityReason+'</td>'+
						'<td><button '+buttonProperty+' class="btn-flat tooltipped actionBtn" data-position="left" data-delay="50" data-tooltip="Add Received Qty"><i class="material-icons">add</i></button></td>'+                   							
					'</tr>');
				
				quantityGivenTotal+=damageRecoveryDetails.quantityGiven;
				quantityReceivedTotal+=damageRecoveryDetails.quantityReceived;
				quantityNotClaimedTotal+=damageRecoveryDetails.quantityNotClaimed;
				
				srno++;
			}	
			
			$("#damageProductDetailsFooter").append(
					'<tr>'+
						'<th colspan="2">Total</th>'+
						'<th>'+quantityGivenTotal+'</th>'+
						'<th>'+quantityReceivedTotal+'</th>'+
						'<th></th>'+
						'<th></th>'+
						'<th>'+quantityNotClaimedTotal+'</th>'+
						'<th></th>'+
						'<th></th>'+
					'</tr>');
			
			$('#damageDetails').modal('open');
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong..with : fetchDamageRecoveryByProductId', '3000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});
}
/**+
 * modal plus button click and set damage recoveryId and damageQtyGiven set on ui or updateDamageForm show
 * @param damageRecoveryDetailsId
 * @param dmgGiven
 * @returns
 */
function updateReceivedQuantity(damageRecoveryDetailsId,dmgGiven){
	
	$('#damageRecoveryDetailsId').val(damageRecoveryDetailsId);
	$('#givenQuantityId').val(dmgGiven);
	
	$("#addDamageForm").hide();
	$("#updateDamageForm").show();
	$("#qtyReceived").focus();
	$("#damageDetails").modal('close');	
}
/**
 * get Damage recovery by damageRecoveryId and set
 * product name,quantity damage which available to give supplier,supplier list by product
 * @param damageRecoveryId
 * @returns
 */
function giveProductRecovery(damageRecoveryId){

	$.ajax({
		url : myContextPath+"/fetchDamageRecoveryByDamageRecoveryId?damageRecoveryId="+damageRecoveryId,
		dataType : "json",
		success : function(data) {
			damageRecoveryDayWise=data;
			
			$('#productName').val(damageRecoveryDayWise.product.productName);
			$('#productName').change();
			
			$('#damageRecoveryId').val(damageRecoveryDayWise.damageRecoveryId);
			$('#damageRecoveryId').change();
			
			$('#qtyDamage').val(parseInt(damageRecoveryDayWise.quantityDamage)-parseInt(damageRecoveryDayWise.quantityGiven));
			$('#qtyDamage').change();
			
			$('#qtyGiven').val(0);
			$('#qtyGiven').change();
			
			getSupplierListByProductId(damageRecoveryDayWise.product.productId);
			
			$('#damageRecoveryDetailsId').val('');
			
			$("#addDamageForm").show();
			$("#updateDamageForm").hide();
			
			$('#qtyGiven').val('');
			$('#qtyGiven').focus(); 
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '3000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});	
}
/**
 * fetch supplier list by product
 * and set on supplier drop down
 * @param productId
 * @returns
 */
function getSupplierListByProductId(productId)
{
	$('#supplierId').empty();
	$("#supplierId").append('<option value="">Choose Supplier</option>');	
	$('#supplierId').change();
	$.ajax({
		url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
		dataType : "json",
		success : function(data) {
			supplierProductLists=data;
			
			for(var i=0; i<supplierProductLists.length; i++)
			{								
				$("#supplierId").append('<option value='+supplierProductLists[i].supplierId+'>'+supplierProductLists[i].supplierName+'</option>');
			}	
			
			$('#supplierId').change();
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  Materialize.Toast.removeAll();
			Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '3000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});	
}