 var userIdValid=true;
$(document).ready(function() {
	$.validator.setDefaults({
	       ignore: []
	}); 
	//validation of form
	//$('select').change(function(){ $('select').valid(); });
	jQuery.validator.addMethod("userIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkEmployeeDuplication(value,"userId");
	}, "User Id already in use"); 
	jQuery.validator.addMethod("emailIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkEmployeeDuplication(value,"emailId");
	}, "Email Id Is already in use"); 
	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkEmployeeDuplication(value,"mobileNumber");
	}, "Mobile Number Is already in use");  

	$('#updateEmployeeForm').validate({
		
		rules: {
			password: "required",
		    confirmpass: {
		      equalTo: "#password"
		    },		    
            userId:{
            	userIdCheck:true,
            	required:true
            },
            mobileNumber:{
            	mobileNumberCheck:true,
            	required:true
            },
            emailId:{
            	emailIdCheck:true
            }
		  },	
		
	
	   errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	
						  /*$('.preloader-background').hide();
						
						  $('#Cnfrmpassword').keyup(function() {

				               var pass = $('#password').val();
				               var cpass = $('#Cnfrmpassword').val();

				               if (pass !== cpass) {
				                   // alert(cpass);
				                   $('#CnfrmpasswordLabel').text("Password not matched").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#CnfrmpasswordLabel').text("Password  matched").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
				           });
						  
						$('#mobileNo').keyup(function() {
							var mobileNo = $('#mobileNo').val();
							//alert(mobileNo);
							if (mobileNo.length !== 10) {
				                   // alert(cpass);
				                   $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#mobileNumberlabel').text("Right").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
						});*/
						
				
						
						//mobile number only allowed numbers
						$('#mobileNo').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						
						$("#userId").on('input', function() {
						    this.value = this.value.replace(/[^a-zA-Z0-9]/g, ''); //<-- replace all other than given set of values
						});
						
						/*$('#userId').on('keypress blur',function(){
							//alert('1');
							var userId=$('#userId').val().trim();
							var employeeId=$('#employeeId').val();
							//alert(userId);
							$.ajax({
								url : myContextPath+"/checkUserIdForUpdate?userId="+userId+"&employeeId="+employeeId,
								dataType : "json",
								success : function(data) {
									
									input:not([type]).valid+label:after, 
									input:not([type]):focus.valid+label:after, 
									input[type=text].valid+label:after, 
									input[type=text]:focus.valid+label:after,
									
									if(data==true)
									{
										$("#useridvalid").attr("data-success", "UserId Available");
										$('#userId').css({"border-bottom":"1px solid green","box-shadow":"0 1px 0 0 green"});
										userIdValid=true;
									}
									else
									{
										$("#useridvalid").attr("data-success", "UserId Not Available");
										$("#useridvalid.active::after").attr("class","red-text");
										$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"});
										userIdValid=false;
										
									}
								}
							});
							
						});*/
						
						$('#saveEmployeeSubmit1').click(function(){
							
							var userId=$('#userId').val().trim();
							  // Our regex
							  // a-z => allow all lowercase alphabets
							  // A-Z => allow all uppercase alphabets
							  // 0-9 => allow all numbers
							  var regex = /[^a-zA-Z0-9]/g;
							  // This is will test the value against the regex
							  // Will return True if regex satisfied
							  if (regex.test(this.value) !== true){
								  //alert if not true
								  //alert("Invalid Input");

								  // You can replace the invalid characters by:
								   //this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '');
								   Materialize.Toast.removeAll();
								  Materialize.toast('UserId Allowed only A-Z or a-z or 0-9', '4000', 'teal lighten-2');
							  }
							//mobile number validation for 10 digit required
							var mobileNo = $('#mobileNo').val();							
							if (mobileNo.length !== 10) {				                   
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Mobile Number Warning");
			           	     	$('#msg').text("Check Your Mobile Number");
			           	     	$('#mobileNo').focus();
			           	        $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
			           	     	return false;   
				              }
							
							
							//atlist one area assign to employee validation
							var areaListIds=$('#areaListIds').val();
							if(areaListIds=='' || areaListIds==undefined)
							 {
							     $('#addeditmsg').modal('open');
							     $('#msgHead').text("Area Select Message");
							     $('#msg').text("Atleast one Area need to select");
							     return false;
							 }
							
							//department validation
							var departmentid=$('#departmentid').val();
							if(departmentid==0)
							{
								$('#addeditmsg').modal('open');
							     $('#msgHead').text("Deparment Select Message");
							     $('#msg').text("Select department for Employee");
							     return false;
							}
							
							
						});
						
						//on area change event
						$("#areaList").on("change", function() {

			                var val = $("#areaList").val(); 
			                
			                /* $.each(productidlist, function(key, value) {
			                    //alert("product : "+key+"-"+value);
			                    if(if(jQuery.inArray(val, myarray) !== -1))
			                    {	return false;
			                    	$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Product Select Warning");
			               	     	$('#msg').text("This Product is already added");               	     	
			                    }
			              	}); */
			              	
			              	if(val==0)
			              	{
			              		return false;
			              	}
			              	//already exist or not check
			              	//if found exist them stop for adding one more time
			              	if(jQuery.inArray(val, areaidlist) !== -1)
			                {	
								Materialize.Toast.removeAll();
			              		Materialize.toast('This Area is already added!', '4000', 'teal lighten-2');
			              		/*$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Area Select Warning");
			           	     	$('#msg').text("This Area is already added");*/               	     	
			                }
			              	else
			           		{
			              		//area id push on array
				                areaidlist.push(val);
				             	//alert(productidlist);
				                
				                //set area list in form
				             	$('#areaListIds').val(areaidlist);
				                
				             	//selected area name
				             	var text=$("#areaList option:selected").text();
				                
				             	//selected area id
				             	var vl=$("#areaList option:selected").val();
								//alert(value+"-"+text); 
				                
				             	//find table of area list row count
				             	var rowCount = $('#areatable tr').length;
				                
				             	//add area to area list table
				             	$("#t1").append("<tr id='rowdel_" + count + "' >"+
					               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
					               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
					               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
					               				"</tr>");
				                count++;
			           		}
			            });
						
						
						/*$('#countryList').change();
						
						$('#countryList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('stateList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose State", 0));
							$.ajax({
								url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryList').val(),
								dataType : "json",
								success : function(data) {

									 alert(data); 
									var options, index, option;
									select = document.getElementById('stateList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var state = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(state.name, state.stateId));
									}
									
									if(editClicked){
										 var source1 = $("#stateListForArea");
											var v=area.region.city.state.stateId;
											source1.val(v);
											source1.change();
									}
									
									 for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} 
								}
							});		
							
						});*/
						
						

						/*$('#stateList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('cityList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose City", ''));
							$.ajax({
								url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									 alert(data); 
									var options, index, option;
									select = document.getElementById('cityList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var city = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(city.name, city.cityId));
									}
									
									 for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} 
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									Materialize.toast('City List Not Found!', '2000', 'teal lighten-2');
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("City List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);
									}
							});		
							
						});*/
						
						/**
						 * on city change fetch region list and set to region drop down
						 */
						$('#cityList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('regionList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose Region", ''));
							$.ajax({
								url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									/* alert(data); */
									var options, index, option;
									select = document.getElementById('regionList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var region = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(region.name, region.regionId));
									}
									
									/* for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} */
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									Materialize.Toast.removeAll();
									Materialize.toast('Region List Not Found!', '4000', 'teal lighten-2');
									  //alert(error +"---"+ xhr+"---"+status);
									/*$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("Region List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});		
							
						});
						/**
						 * on region select change fetch area list and set on area drop down
						 */
						$('#regionList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('areaList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose Area", ''));
							$.ajax({
								url : myContextPath+"/fetchAreaListByRegionId?regionId="+$('#regionList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									/* alert(data); */
									var options, index, option;
									select = document.getElementById('areaList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var area = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(area.name, area.areaId));
									}
									
									/* for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} */
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Area List Not Found!', '2000', 'teal lighten-2');
									/*$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("Area List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});		
							
						});
						
						
					});
/**
 * delete area id from array and also area table viewing
 * @param id
 * @returns
 */
function deleterow(id) {
	
    //alert('#rowproductkey_'+id);
    var removeItem=$('#rowproductkey_' + id).val();
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
    
    //remove given area id from array list
    areaidlist = jQuery.grep(areaidlist, function(value) {
    	  return value != removeItem;
    	});
    
   // alert(productidlist);
    
    //set area ids from array list to form
    $('#areaListIds').val(areaidlist);
   // alert(productidlist);
    
    //find no of rows on area table
    var rowCount = $('#t1 tr').length;
    
	//alert(rowCount);
	var trData="";
	count=1;
	
	//reset table with balance area
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!==i)
		{
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
       				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);

}

/**
 * check fields data when employee adding
 * user id,mobile number,email id
 * user id check in employee and company
 * @param checkText
 * @param type
 * @returns
 */
function checkEmployeeDuplication(checkText,type){
 	var status=false;
 	$.ajax({
 		url : myContextPath+"/checkEmployeeDuplicationForUpdate?checkText="+checkText+"&type="+type+"&employeeDetailsId="+employeeDetailsId,
 		async:false,
 		success : function(data) {
 			if(data==="Success"){
 				status=true;
 			}else{
 				status=false;
 			}
 		},
 		error: function(xhr, status, error) {
 			alert("Error");
 		}
 	});
 	
 	return status;
 }