
var addUpdate=true;
$(document).ready(function(){
	//reset form
	$("#resetExpenseSubmit").click(function(){
		resetForm();
	});
	
	/**
	 * name blank validation
	 * expense name already exist validation
	 * url change basis of  addUpdate boolean value
	 * save/update expense
	 */
	$('#saveExpenseTypeSubmit').click(function(){
		
		//expense name blank validation
		var expenseTypeName=$('#name').val();
		if(expenseTypeName=='' || expenseTypeName==undefined){
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Expense Type Name', '2000', 'teal lighten-2');
			return false;
		}
		var expenseTypeId=$('#expenseTypeId').val();
			
		//check expense name already exist or not
		if(checkExpenseTypeAlreadyExistForAddUpdate(expenseTypeName,expenseTypeId)!=true){
			return false;
		}
		
		//url change basis of addUpdate boolean value
		var url;
		if(addUpdate){
			url=baseUrl+"/save_expense_type";
		}else{
			url=baseUrl+"/update_expense_type";
		}
		
		//save / update expense type
		$.ajax({
			type:'POST',
			url: url,
			async: false,
			data:{
				name:expenseTypeName,
				expenseTypeId:expenseTypeId
			},
			success : function(data){
					if(data=="Success"){
						if(addUpdate){
							Materialize.Toast.removeAll();
							Materialize.toast('ExpenseType Added SuccessFully', '2000', 'teal lighten-2');
						}else{
							Materialize.Toast.removeAll();
							Materialize.toast('ExpenseType Updated SuccessFully', '2000', 'teal lighten-2');
						}	
						//reset form
						resetForm();
						
						//expense type list set on table
						refreshExpenseTypeList();
					}else{
						if(addUpdate){
							Materialize.Toast.removeAll();
							Materialize.toast('ExpenseType Added Failed', '2000', 'red lighten-2');
						}else{
							Materialize.Toast.removeAll();
							Materialize.toast('ExpenseType Updated Failed', '2000', 'red lighten-2');
						}
					}
			},
			error: function(xhr, status, error){
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
	});
	
	//expense type list set on table
	refreshExpenseTypeList();
});

//check expense type already exist or not
function checkExpenseTypeAlreadyExistForAddUpdate(expenseTypeName,expenseTypeId){
	
	var url;
	//change url basis of update or save by addUpdate boolean value 
	if(addUpdate){
		url=baseUrl+"/expense_type_check_for_add";
	}else{
		url=baseUrl+"/expense_type_check_for_update";
	}
	
	var checkExpenseType=true;
	
	$.ajax({
		type:'POST',
		url: url,
		async: false,
		data:{
			name:expenseTypeName,
			expenseTypeId:expenseTypeId
		},
		success : function(data){
				if(data=="Success"){
					checkExpenseType=true;
				}else{
					checkExpenseType=false;
					Materialize.Toast.removeAll();
					Materialize.toast('ExpenseType Already Exist', '2000', 'teal lighten-2');
				}
		},
		error: function(xhr, status, error){
			Materialize.Toast.removeAll();
			Materialize.toast('Checking ExpenseType Failed', '2000', 'red lighten-2');
		}		
	});
	
	return checkExpenseType;
}
//reset form
function resetForm(){
	$('#name').val('');
	$('#expenseTypeId').val('0');
	$("#saveExpenseTypeSubmit").html('<i class="material-icons left">add</i>Add');
	addUpdate=true;
}

//expense type list set on table
function refreshExpenseTypeList(){
	$('#deleteModals').empty();
	var t = $('#tblData').DataTable();
	    t.clear().draw();
	    
	 $.ajax({
			type:'GET',
			url: baseUrl+"/fetchExpenseTypeList",
			async: false,
			success : function(data){
				if(data==''){
					Materialize.Toast.removeAll();
					Materialize.toast('Expense Type List Not Found', '2000', 'teal lighten-2');
					return false;
				}
				expenseTypeList=data;
				
				for(var i=0; i<expenseTypeList.length; i++){
					expenseType=expenseTypeList[i];
					
					var dateAddedString = moment(expenseType.addedDate).format("DD-MM-YYYY")
					var timeAddedString = moment(expenseType.addedDate).format("hh:mm:ss")
					
					var dateUpdatedString;
					var timeUpdatedString;
					if(expenseType.updatedDate!='' && expenseType.updatedDate!=undefined){
						dateUpdatedString = moment(expenseType.updatedDate).format("DD-MM-YYYY")
						timeUpdatedString = moment(expenseType.updatedDate).format("hh:mm:ss")
					}else{
						dateUpdatedString='NA';
						timeUpdatedString="NA";
					}
					
					t.row.add([
					           (i+1),
					           expenseType.name,
					           dateAddedString+"<br>"+timeAddedString,
					           dateUpdatedString+"<br>"+timeUpdatedString,
					           
					           '<button onclick="editExpenseType('+expenseType.expenseTypeId+')" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50"'+ 
						       'data-tooltip="Edit" >edit</i></button>'+ 
						       '<a href="#delete'+expenseType.expenseTypeId+'" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" '+
						       'data-delay="50" data-tooltip="Delete">delete</i></a>'
					           
					           ]).draw(false);;
					
					$('#deleteModals').append('<div id="delete'+expenseType.expenseTypeId+'" class="modal deleteModal row">'+
									                '<div class="modal-content  col s12 m12 l12">'+
									                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
									                    '<h5 class="center-align">'+
									                	'Do you  want to Delete'+
														'</h5>'+
									                    '<br/>'+
									                 '</div>'+
									                '<div class="modal-footer">'+
									 				    '<div class="col s12 center">'+
									         				'<a  class="modal-action modal-close cursorPointer waves-effect btn red">Close</a>'+
										    			
										 				   '<a  onclick="deleteExpenseType('+expenseType.expenseTypeId+')"'+
									                       'class="modal-action modal-close waves-effect cursorPointer  btn marginLeft5px">'+
									                       'Delete'+
									                       '</a>'+
										   				'</div>'+    
									    			'</div>'+
									            '</div>');
				}
				$('.modal').modal();
				
			},
			error: function(xhr, status, error){
				Materialize.Toast.removeAll();
				Materialize.toast('Expense Type List Not Found', '2000', 'red lighten-2');
			}
			
		});
}
/**
 * set expense type saved data to form for update
 * @param expenseTypeId
 * @returns
 */
function editExpenseType(expenseTypeId){
	addUpdate=false;
	$.ajax({
		type:'POST',
		url: baseUrl+"/fetchExpenseType",
		async: false,
		data:{
			expenseTypeId:expenseTypeId
		},
		success : function(data){
			$('#name').val(data.name);
			$('#name').change();
			$('#expenseTypeId').val(data.expenseTypeId);
			$("#saveExpenseTypeSubmit").html('<i class="material-icons left">send</i>Update');
		},
		error: function(xhr, status, error){
			Materialize.Toast.removeAll();
			Materialize.toast('Fetching ExpenseType Failed', '2000', 'red lighten-2');
		}		
	});
}
/**
 * delete expense type by expense type id
 * @param expenseTypeId
 * @returns
 */
function deleteExpenseType(expenseTypeId){
	resetForm();
	$.ajax({
		type:'POST',
		url: baseUrl+"/delete_expense_type",
		async: false,
		data:{
			expenseTypeId:expenseTypeId
		},
		success : function(data){
			if(data=="Success"){
				Materialize.Toast.removeAll();
				Materialize.toast('Expense Type Deleted Successfully', '2000', 'teal lighten-2');
				refreshExpenseTypeList();
			}else{
				Materialize.Toast.removeAll();
				Materialize.toast('ExpenseType Deleting Failed', '2000', 'red lighten-2');
			}
		},
		error: function(xhr, status, error){
			Materialize.Toast.removeAll();
			Materialize.toast('ExpenseType Deleting Failed', '2000', 'red lighten-2');
		}		
	});
	
}