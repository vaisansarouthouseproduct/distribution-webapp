var add = "";
var employeeLocationDetails;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var infowindow;
var routeList = [];
$(document).ready(function () {

	$('ul.tabs').tabs();

	$('#oneDateButton').click(function(){
		$('#employeeId').change();
	});
	
	//google.maps.event.addDomListener(window, 'load', initialize);
	$('#employeeId').change(function () {
		$('.preloader-background').show();
		$('.preloader-wrapper').show();
		$('#routeListWithTime').html('');
		var employeeDetailsId = parseFloat($('#employeeId').val());
		var oneDate=$('#oneDate').val();

		var lat, lng;
		if (employeeDetailsId != 0 && employeeDetailsId != undefined) {
			$.ajax({
				type: "GET",
				url: myContextPath + "/fetchSingleEmployeeLatLng?employeeDetailsId=" + employeeDetailsId + "&pickDate="+oneDate,
				beforeSend: function () {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
				},
				success: function (data) {
					employeeLocationDetails = data;
					//console.log(data);
					var empName = $('#employeeId option:selected').text();
					var empMobileNo = employeeLocationDetails[0].employeeDetails.contact.mobileNumber;

					var subsrno = 1;

					var startEnd = [];
					//making routes
					for (var j = 0; j < employeeLocationDetails[0].employeeRouteListModelList.length; j++) {
						var wayPointList = [];
						var startRoute = [];
						var endRoute = [];

						var times = [];
						var addressList = [];

						var employeeRouteListLength = employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList.length;
						//set start route
						startRoute.push([employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[0].lat, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[0].lng, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[0].info]);

						//set way points
						for (var k = 1; k < employeeRouteListLength - 1; k++) {
							if (employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[k].info !== null) {
								wayPointList.push([employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[k].lat, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[k].lng, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[k].info]);
							}
						}
						//set end route
						endRoute.push([employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].lat, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].lng, employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].info]);

						//set route list
						routeList.push([employeeLocationDetails[0].employeeDetails.employeeDetailsId, startRoute, endRoute, wayPointList, j]);

						//ending current route
						times.push(moment(new Date(employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[0].datetime).toString()).format("hh:mm:ss"));
						times.push(moment(new Date(employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].datetime).toString()).format("hh:mm:ss"));

						addressList.push(employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[0].address);
						addressList.push(employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].address);

						//task list 
						var taksList = "";
						for (var d = 0; d < employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList.length; d++) {
							var info = employeeLocationDetails[0].employeeRouteListModelList[j].employeeRouteList[d].info;
							if (info !== '' && info !== undefined && info != null) {
								taksList = taksList + info + ", ";
							}
						}
						taksList = taksList.substring(0, taksList.length - 2);
						//set start time , address, end time ,address and task list  
						startEnd.push([times[0],
						addressList[0],
						times[times.length - 1],
						addressList[addressList.length - 1],
							taksList
						]);


						addressList = [];
						times = [];
						wayPointList = [];
						//current route end here												
					}
					//set route list employee wise
					var tbData = "";
					var charIncre = 'a';
					if (startEnd.length > 0) {
						for (var k = 0; k < startEnd.length; k++) {
							if (k != 0) {
								empName = "";
							}
							var current = startEnd[k];
							tbData = tbData + '<tr>' +
								'<td>'+subsrno+'</td>' +
								'<td>' + empName +'<br>'+empMobileNo+ '</td>' +
								'<td>' + '<b>Start(' + current[0] + ') :- </b><br>' + current[1] + '</td>' +
								'<td>' + '<b>End(' + current[2] + ') :- </b><br>' + current[3] + '</td>' +
								'<td>'+
								'<p class="truncateToThreeLine readableData">' + current[4] + '</p>'+
								'<span class="readMore blue-text right" data-type="MORE">Read More</span>'+
								'</td>' +
								'<td>    <button class="btn btn-flat" onclick="calcRoute(' + employeeLocationDetails[0].employeeDetails.employeeDetailsId + ',' + k + ')"> <i class="material-icons">visibility</i></button>' +
								'</td>' +
								'</tr>';
							//charIncre=nextChar(charIncre);	
							
						}
						subsrno ++;
					}
					$('#routeListWithTime').html(tbData);

					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function (xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					//alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
					$('#msgHead').text("Message : ");
					$('#msg').text("Something Went Wrong");
					setTimeout(function () {
						$('#addeditmsg').modal('close');
					}, 1000);
				}
			});
		}
		else{
			initializeMultipleDynamic();
		}



	});
	/**
	 * on department change
	 * get employee last location and set current location tab
	 * and route wise info show route table
	 **/
	$('#departmentId').change(function () {
		initializeMultipleDynamic();
	});
	var currentInput=$('#oneDate').pickadate(),
		current_picker=currentInput.pickadate('picker');
		console.log(current_picker.get('value'));
		 // An integer (positive/negative) sets it relative to today.
		current_picker.set('min', -6);
		//max `true` sets it to today. `false` removes any limits.
		current_picker.set('max', true);

	
});


function initializeMultipleDynamic() {
	var oneDate=$('#oneDate').val();
	
	routeList = [];
	// Get the raw DOM object for the select box
	var select = document.getElementById('employeeId');

	// Clear the old options
	select.options.length = 0;

	//Load the new options

	select.options.add(new Option("All", 0));
	// alert($('#departmentId').val());
	$.ajax({
		type: "GET",
		url: myContextPath + "/fetchEmployeeLatLng?departmentId=" + $('#departmentId').val()+ "&pickDate="+oneDate,
		beforeSend: function () {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
		},
		async: false,
		success: function (data) {
			employeeLocationDetails = data;
			//console.log(data);
			var options, index, option;
			select = document.getElementById('employeeId');

			var srno = 1;
			$("#empCurrentLocation").empty();
			var tbData = "";
			$("#routeListWithTime").empty();
			var subsrno = 1;
			for (i = 0; i < employeeLocationDetails.length; i++) {
				var empName = employeeLocationDetails[i].employeeDetails.name;
				var empMobileNo = employeeLocationDetails[i].employeeDetails.contact.mobileNumber;
				/* var latlng = new google.maps.LatLng(employeeLocationDetails[i].lat,employeeLocationDetails[i].lng);
					var geocoder = new google.maps.Geocoder();
					geocoder.geocode({ 'latLng': latlng }, function (results, status) {
						add="";
							if (status == google.maps.GeocoderStatus.OK) {
									if (results[1]) {
										add=results[0].formatted_address;
									}
							}
							$("#empCurrentLocation").append("<tr>"+
									"<td>"+srno+"</td>"+
									"<td>"+empName+"</td>"+
									"<td>"+add+"</td>"+
								"</tr>");
								srno=srno+1;
					});
					*/
				add = employeeLocationDetails[i].address;
				//if latitide and longitute found then set lat,lng on  view button clicked initializeDynamic(lat,lng);
				if (employeeLocationDetails[i].lat != "" && employeeLocationDetails[i].lng != "") {
					$("#empCurrentLocation").append("<tr>" +
						"<td>" + srno + "</td>" +
						"<td>" + empName + "</td>" +
						"<td>" + add + "</td>" +
						"<td>" + empMobileNo + "</td>" +
						"<td>" + '<button class="btn btn-flat" onclick="initializeDynamic(' +
						employeeLocationDetails[i].lat + ',' +
						employeeLocationDetails[i].lng + ',\'' +
						empName
						+ '\')" id="currentView"><i class="material-icons">visibility</i></button>' + "</td>" +
						"</tr>");
				}
				else {
					$("#empCurrentLocation").append("<tr>" +
						"<td>" + srno + "</td>" +
						"<td>" + empName + "</td>" +
						"<td>" + add + "</td>" +
						"<td>" + empMobileNo + "</td>" +
						"<td>" + '<button disabled class="btn btn-flat" id="currentView"><i class="material-icons">visibility</i></button>' + "</td>" +
						"</tr>");
				}
				srno = srno + 1;

				// set employee list on employee drop down
				select.options.add(new Option(employeeLocationDetails[i].employeeDetails.name, employeeLocationDetails[i].employeeDetails.employeeDetailsId));

				//making routes
				var startEnd = [];
				for (var j = 0; j < employeeLocationDetails[i].employeeRouteListModelList.length; j++) {
					var wayPointList = [];
					var startRoute = [];
					var endRoute = [];

					var times = [];
					var addressList = [];


					var employeeRouteListLength = employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList.length;

					//set start route
					startRoute.push([employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[0].lat, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[0].lng, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[0].info]);

					//set way points
					for (var k = 1; k < employeeRouteListLength - 1; k++) {
						if (employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[k].info !== null) {
							wayPointList.push([employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[k].lat, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[k].lng, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[k].info]);
						}
					}

					//set end route
					endRoute.push([employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].lat, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].lng, employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].info]);

					//set route list
					routeList.push([employeeLocationDetails[i].employeeDetails.employeeDetailsId, startRoute, endRoute, wayPointList, j]);

					//ending current route
					times.push(moment(new Date(employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[0].datetime).toString()).format("hh:mm:ss"));
					times.push(moment(new Date(employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].datetime).toString()).format("hh:mm:ss"));

					addressList.push(employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[0].address);
					addressList.push(employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[employeeRouteListLength - 1].address);

					//task list 
					var taksList = "<ul>";
					for (var d = 0; d < employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList.length; d++) {
						var info = employeeLocationDetails[i].employeeRouteListModelList[j].employeeRouteList[d].info;
						if (info !== '' && info !== undefined && info != null) {
							taksList = taksList + '<li type="square">' + info + '<b>,</b></li>';
						}
					}
					taksList = taksList.substring(0, taksList.length - 13);
					taksList = taksList + '</li></ul>';

					//set start time , address, end time ,address and task list  
					startEnd.push([times[0],
					addressList[0],
					times[times.length - 1],
					addressList[addressList.length - 1],
						taksList
					]);

					addressList = [];
					times = [];
					wayPointList = [];
					//current route end here												
				}
				//set route list employee wise
				var charIncre = 'a';
				if (startEnd.length > 0) {
					for (var k = 0; k < startEnd.length; k++) {
						if (k != 0) {
							empName = "";
						}
						var current = startEnd[k];
						tbData = tbData + '<tr>' +
							'<td>' + subsrno + '</td>' +
							'<td>' + empName +'<br>'+empMobileNo+ '</td>' +
							'<td>' + '<b>Start(' + current[0] + ') :- </b><br>' + current[1] + '</td>' +
							'<td>' + '<b>End(' + current[2] + ') :- </b><br>' + current[3] + '</td>' +
							'<td>'+
							'<p class="truncateToThreeLine readableData">' + current[4] + '</p>'+
							'<span class="readMore blue-text right" data-type="MORE">Read More</span>'+
							'</td>' +
							'<td>    <button class="btn btn-flat" onclick="calcRoute(' + employeeLocationDetails[i].employeeDetails.employeeDetailsId + ',' + k + ')"> <i class="material-icons">visibility</i></button>' +
							'</td>' +
							'</tr>';
						//charIncre=nextChar(charIncre);							
					}
					subsrno++;
				}
			}

			$('#routeListWithTime').html(tbData);

			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			
		},
		error: function (xhr, status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			//alert(error +"---"+ xhr+"---"+status);
			$('#addeditmsg').modal('open');
			$('#msgHead').text("Message : ");
			$('#msg').text("Something Went Wrong");
			setTimeout(function () {
				$('#addeditmsg').modal('close');
			}, 1000);
		}
	});
}
function nextChar(c) {
	return String.fromCharCode(c.charCodeAt(0) + 1);
}


/**
 * get address by lat,lng
 * @param lat
 * @param lng
 * @returns
 */
function getAddress(lat, lng) {
	//add="";
	var latlng = new google.maps.LatLng(lat, lng);
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({ 'latLng': latlng }, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[1]) {
				add = results[0].formatted_address;
			}
		}
	});
	return add;
}
/**
 * way point color
 * @param color
 * @returns
 */
function pinSymbol(color) {
	return {
		path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
		fillColor: color,
		fillOpacity: 1,
		strokeColor: '#000',
		strokeWeight: 2,
		scale: 1,
	};
}
/**
 * set location point by lat,lng,name
 * @param lat
 * @param lng
 * @param name
 * @returns
 */
function initializeSingleLocationDynamic(lat, lng, name) {
	directionsDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers: true
	});
	var chicago = new google.maps.LatLng(lat, lng);
	var mapOptions = {
		zoom: 0,
		center: chicago,
		fullscreenControlOptions: {
			position: google.maps.ControlPosition.LEFT_TOP
		},
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	infowindow = new google.maps.InfoWindow();
	directionsDisplay.setMap(map);

	var latlng = new google.maps.LatLng(19.231652, 72.861622);
	map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: latlng,
		zoom: 0,
		fullscreenControlOptions: {
			position: google.maps.ControlPosition.LEFT_TOP
		},
	});
	var marker = new google.maps.Marker({
		map: map,
		label: 'Co.',
		//icon: pinSymbol("blue"),
		title: CompanyName,
		position: latlng,
		draggable: false,
		anchorPoint: new google.maps.Point(0, -29)
	});
	infowindow = new google.maps.InfoWindow();
	google.maps.event.addListener(marker, 'click', function () {
		var iwContent = '<div id="iw_container">' +
			'<div class="iw_title"> ' + name + ' </div></div>';
		// including content to the infowindow
		infowindow.setContent(iwContent);
		// opening the infowindow in the current map and at the current marker location
		infowindow.open(map, marker);
	});
	//google.maps.event.addListener(marker, 'click', calcRoute); 

	//setTimeout(calcRoute, 1000);
	//open modal
	$('#mapModal').modal('open');
}
var i = 1;
/**
 * create way point and set to google map route wise
 * @param employeeDetailsId
 * @param id
 * @returns
 */
function calcRoute(employeeDetailsId, id) {
	//set selected company location
	initializeSingleLocationDynamic(19.231652, 72.861622, CompanyName);

	i = 1;
	var waypointList = [];
	var waypointNameList = [];
	var end;
	var start;
	var endName;
	var startName;

	//create way point list
	for (var r = 0; r < routeList.length; r++) {
		if (routeList[r][0] === employeeDetailsId && routeList[r][4] === id) {
			for (var t = 0; t < routeList[r][3].length; t++) {
				var waypoint = { location: new google.maps.LatLng(routeList[r][3][t][0], routeList[r][3][t][1]) };
				waypointList.push(waypoint);
				waypointNameList.push(routeList[r][3][t][2]);
			}
			start = new google.maps.LatLng(routeList[r][1][0][0], routeList[r][1][0][1]);
			end = new google.maps.LatLng(routeList[r][2][0][0], routeList[r][2][0][1]);
			if (routeList[r][1][0][2] == null) {
				startName = "Start";
			}
			else {
				startName = routeList[r][1][0][2];
			}

			if (routeList[r][2][0][2] == null) {
				endName = "End";
			}
			else {
				endName = routeList[r][2][0][2];
			}
		}

	}

  /**
   * if way points greater than 20 then its divided by 20 and show them
   */
	var waypointListParts = [];
	if (waypointList.length > 20) {

		var parts = Math.ceil(waypointList.length / 20);
		var uptoCopy = 20, w = 0;
		for (var c = 0; c < parts; c++) {
			var waypointListPart = [];
			for (; w < waypointList.length, uptoCopy != 0; w++) {
				waypointListPart.push(waypointList[w]);
				uptoCopy -= 1;
			}
			uptoCopy = 20;
			waypointListParts.push(waypointListPart);
			waypointListPart = [];
			w++;
		}
		//set way points on goole map
		for (var shw = 0; shw < waypointListParts.length; shw++) {
			var request = {
				origin: start,
				destination: end,
				waypoints: waypointListParts[shw],
				travelMode: 'WALKING'
			};
			directionsService.route(request, function (response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					directionsDisplay.setMap(map);

					var startLocation = new Object();
					var endLocation = new Object();
					var waypointLocations = [];

					// Display start and end markers for the route.
					var legs = response.routes[0].legs;
					for (i = 0; i < legs.length; i++) {
						if (i == 0) {
							startLocation.latlng = legs[i].start_location;
							startLocation.address = legs[i].start_address;
							//createMarker(legs[i].start_location, "start", legs[i].start_address, "",false);
						}
						if (i != 0) {
							var waypoint = {};
							waypoint.latlng = legs[i].start_location;
							waypoint.address = legs[i].start_address;
							waypointLocations.push(waypoint);
						}
						if (i == legs.length - 1) {
							endLocation.latlng = legs[i].end_location;
							endLocation.address = legs[i].end_address;
						}
						var steps = legs[i].steps;
					}
					createMarker(endLocation.latlng, "End", endName, "http://www.google.com/mapfiles/markerB.png", true)
					createMarker(startLocation.latlng, "Start", startName, "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png", true);
					for (var i = 0; i < waypointLocations.length; i++) {
						createMarker(waypointLocations[i].latlng, "Work", waypointNameList[i], "", false);
					}
				} else {
					alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
				}
			});
		}
	} else {

		var request = {
			origin: start,
			destination: end,
			waypoints: waypointList,
			travelMode: 'WALKING'
		};
		//set way points on goole map
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				directionsDisplay.setMap(map);

				var startLocation = new Object();
				var endLocation = new Object();
				var waypointLocations = [];

				// Display start and end markers for the route.
				var legs = response.routes[0].legs;
				for (i = 0; i < legs.length; i++) {
					if (i == 0) {
						startLocation.latlng = legs[i].start_location;
						startLocation.address = legs[i].start_address;
						//createMarker(legs[i].start_location, "start", legs[i].start_address, "",false);
					}
					if (i != 0) {
						var waypoint = {};
						waypoint.latlng = legs[i].start_location;
						waypoint.address = legs[i].start_address;
						waypointLocations.push(waypoint);
					}
					if (i == legs.length - 1) {
						endLocation.latlng = legs[i].end_location;
						endLocation.address = legs[i].end_address;
					}
					var steps = legs[i].steps;
				}
				createMarker(endLocation.latlng, "End", endName, "http://www.google.com/mapfiles/markerB.png", true)
				createMarker(startLocation.latlng, "Start", startName, "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png", true);
				for (var i = 0; i < waypointLocations.length; i++) {
					createMarker(waypointLocations[i].latlng, "Work", waypointNameList[i], "", false);
				}
			} else {
				alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
			}
		});
	}
	//open modal
	$('#mapModal').modal('open');
}
/**
 * generate location point and its click event
 * @param latlng
 * @param label
 * @param html
 * @param url
 * @param isMain
 * @returns
 */
function createMarker(latlng, label, html, url, isMain) {
	var contentString = '<b>' + label + '</b><br>' + html;

	if (isMain) {
		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			icon: url,
			title: label,
			zIndex: Math.round(latlng.lat() * -100000) << 5
		});
	}
	else {
		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			label: '' + i++,
			title: label,
			zIndex: Math.round(latlng.lat() * -100000) << 5
		});
	}
	google.maps.event.addListener(marker, 'click', function () {
		infowindow.setContent(contentString);
		infowindow.open(map, marker);
	});
}

/**
 * on page load set company on google map by lat,lng,name
 * @returns
 */
function initialize() {
	var latlng = new google.maps.LatLng(19.231652, 72.861622);
	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: latlng,
		zoom: 0,
		fullscreenControlOptions: {
			position: google.maps.ControlPosition.LEFT_TOP
		},
	});
	var marker = new google.maps.Marker({
		map: map,
		label: 'Co.',
		//icon: pinSymbol("blue"),
		title: CompanyName,
		position: latlng,
		draggable: false,
		anchorPoint: new google.maps.Point(0, -29)
	});
	var infowindow = new google.maps.InfoWindow();
	google.maps.event.addListener(marker, 'click', function () {
		var iwContent = '<div id="iw_container">' +
			'<div class="iw_title"><b>Location</b> : ' + CompanyName + '</div></div>';
		// including content to the infowindow
		infowindow.setContent(iwContent);
		// opening the infowindow in the current map and at the current marker location
		infowindow.open(map, marker);
	});
}
/**
 * set location point on google map by lat, lng, name
 * @param lat
 * @param lng
 * @param name
 * @returns 
 */
function initializeDynamic(lat, lng, name) {
	var latlng = new google.maps.LatLng(lat, lng);
	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: latlng,
		zoom: 12,
		fullscreenControlOptions: {
			position: google.maps.ControlPosition.LEFT_TOP
		},
	});
	var marker = new google.maps.Marker({
		map: map,
		label: 'Co.',
		//icon: pinSymbol("blue"),
		title: CompanyName,
		position: latlng,
		draggable: false,
		anchorPoint: new google.maps.Point(0, -29)
	});


	var infowindow = new google.maps.InfoWindow();
	google.maps.event.addListener(marker, 'click', function () {
		var iwContent = '<div id="iw_container">' +
			'<div class="iw_title"><b>Location</b> : ' + name + '</div></div>';
		// including content to the infowindow
		infowindow.setContent(iwContent);
		// opening the infowindow in the current map and at the current marker location
		infowindow.open(map, marker);
	});
	//open modal
	$('#mapModal').modal('open');
}
//on load initialize company location 
google.maps.event.addDomListener(window, 'load', initialize);

