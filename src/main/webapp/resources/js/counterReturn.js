var productCart=[];
var freeNonFreeRdo="NonFree";
var payTypeRdo="Cash";
var paymentRdo="InstantPay";
var isPrintBill=false;
var counterOrderOld
var refAmount=0;
var paymentSituation;
var balAmt;
let newTotalAmount=0;
let newTotalAmountWithoutTax=0;
let gateKeeperName="";
let paymentCounterList=[];
let refundAmt=0;
let returnCounterRequest;
let mainPercentage=0; 
let discountMain=0;
$(document).ready(function() {
	//fetch counter details and gatekeeper name by counter id
	fetchCounterOrderByCounterOrderId(counterOrderId);
	
	//payment list
	fetchPaymentCounterOrder(counterOrderId);
	
	//set information
	$('#counterOrderId').text(counterOrderOld.counterOrderId);
	if(counterOrderOld.businessName==null || counterOrderOld.businessName==""){
		$('#customerNameId').text(counterOrderOld.customerName);
		if(counterOrderOld.customerMobileNumber==null || counterOrderOld.customerMobileNumber==""){
			$('#tableCounterInfo').append('<tr>'+
						                    '<th>Mobile Number</th>'+
						                    '<th>'+counterOrderOld.customerMobileNumber+'</th>'+
						                  '</tr>');
		}
	}else{
		$('#customerNameId').text(counterOrderOld.businessName.shopName);
		$('#tableCounterInfo').append('<tr>'+
                '<th>Mobile Number</th>'+
                '<th>'+counterOrderOld.businessName.contact.mobileNumber+'</th>'+
              '</tr>');
	}
	$('#gatekeeperNameId').text(gateKeeperName);		
	$('#dateOfOrder').text(moment(counterOrderOld.dateOfOrderTaken).format("DD-MMM-YYYY"));
	
	//fetch counter product details list and add its on productCart array
	var counterOrderProductDetails=fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId);
	let totalMain=0;
	for(var i=0; i<counterOrderProductDetails.length; i++){		
		productCart.push([
							counterOrderProductDetails[i].product.product,
							counterOrderProductDetails[i].purchaseQuantity,
							counterOrderProductDetails[i].sellingRate,
							0, //total amount
							counterOrderProductDetails[i].type,							
							counterOrderProductDetails[i].discountPer,
							counterOrderProductDetails[i].discount,
							(counterOrderProductDetails[i].discountType=="Percentage"),
							0,//return qty
							""//reason
						]);	
		
		let qty = parseInt(counterOrderProductDetails[i].purchaseQuantity)
		+ parseInt(counterOrderProductDetails[i].returnQuantity);
		let total = qty * parseFloat(counterOrderProductDetails[i].sellingRate);
		let discAmt = (total * parseFloat(counterOrderProductDetails[i].discountPer)) / 100;
		total = total - discAmt;
		totalMain= parseFloat(totalMain)+parseFloat(total);
	}
	
	totalMain=parseFloat(totalMain.toFixedVSS(2));
	
	let discountType=counterOrderOld.discountType;
	let discount=counterOrderOld.discount;

	if(discountType=="Amount"){
		mainPercentage=(
			parseFloat(discount)/parseFloat(totalMain)	
		)*100;
		mainPercentage=parseFloat(mainPercentage)//.toFixedVSS(2));
	}else{
		mainPercentage=parseFloat(discount);
	}

	//appends table row using productCart[] 
	refreshTable();

	$('#returnSubmitId').click(function(){
		verifySubmit();
		
		return false;
	});	
});



/**
 * verify when click on pay,payAndPrint,CustInfoSubmit
 */
function verifySubmit(){

		var returnCounterOrderProductsList=[];
		var totalReturnQty=0;
		var productListData="";
		var returnFound=false;
		
		//qty verify with current invt start
		for(var i=0; i<productCart.length; i++){

			if(productCart[i][8]!=0 && productCart[i][9]==""){
				
				var freeNonFreeText="";
				if(productCart[i][4]=="Free"){
					freeNonFreeText="-Free";
				}else{
					freeNonFreeText="";
				}
				//Materialize.Toast.removeAll();
				Materialize.toast('Enter '+productCart[i][0].productName+freeNonFreeText+' Reason For Return ', '3000', 'teal lighten-2');
				
				return false;
			}
			
			totalReturnQty+=parseInt(productCart[i][8]);
		/* For External use */
/*
 * 		productCart.push([
							counterOrderProductDetails[i].product.product,
							counterOrderProductDetails[i].purchaseQuantity,
							counterOrderProductDetails[i].sellingRate,
							0, //total amount
							counterOrderProductDetails[i].type,
							
							counterOrderProductDetails[i].discountPer,
							counterOrderProductDetails[i].discount,
							(counterOrderProductDetails[i].discountType=="Percentage"),
							0,//return qty
							""//reason
						]);	
 * */
			
		var percentageCheck=productCart[i][7];
		var percentageCheckRes;
		if(percentageCheck){
			percentageCheckRes="Percentage";
		}else{
			percentageCheckRes="Amount";
		}
		
		if(productCart[i][8]!=0){
				var returnCounterOrderProduct= {
					issuedQuantity 				: productCart[i][1],
					returnQuantity				: productCart[i][8],
					sellingRate    				: productCart[i][2],
					returnTotalAmountWithTax 	: productCart[i][3],
					reason 						: productCart[i][9],
					product 					: {
													product : productCart[i][0]
												},
					type 						: productCart[i][4],
					discountType 				: percentageCheckRes,
					discount 					: productCart[i][6],
					discountPer 				: productCart[i][5]
				}
				returnCounterOrderProductsList.push(returnCounterOrderProduct);
		}
		
		
		
		/* End */



		}
		
		var mainComment=$('#mainCommentId').val();
		if(mainComment=="" || mainComment==undefined){
			//Materialize.Toast.removeAll();
			Materialize.toast(' Enter Comment ', '3000', 'teal lighten-2');
			return false;
		}

		if(totalReturnQty==0){
			//Materialize.Toast.removeAll();
			Materialize.toast(' At least Need to Return One Product ', '3000', 'teal lighten-2');
			return false;
		}
		
		returnCounterOrder = {

				employee :  {
					employeeId : counterOrderOld.employeeGk.employeeId
				},

				counterOrder : {
					counterOrderPKId : counterOrderOld.counterOrderPKId, 
					counterOrderId : counterOrderOld.counterOrderId
				},
				
				totalAmount : newTotalAmountWithoutTax,
				
				totalAmountWithTax : newTotalAmount,

				totalQuantity : totalReturnQty,

				discountType : counterOrderOld.discountType,

				discount : discountMain,//counterOrderOld.discount,
				
				comment : mainComment
			}
		
		returnCounterRequest={
				returnCounterOrder : returnCounterOrder,
				returnCounterOrderProductsList : returnCounterOrderProductsList,
				refAmount : refundAmt,
				payType : "Cash"
		}
		$('#returnSubmitId').prop('disabled',true);
		//submitReturnCounterOrder(returnCounterRequest);
		confirmToast();
		return true;
}

function confirmToast(){
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Submit?</span>').add($('<button class="btn red white-text toast-action" onclick="cancelSubmit();">Cancel</button><button class="btn white-text toast-action" onclick="submit()">Pay</button>  '));
	  Materialize.toast($toastContent, "abc");
}
function submit(){
	Materialize.Toast.removeAll();
	submitReturnCounterOrder(returnCounterRequest);
}
function cancelSubmit(){
	Materialize.Toast.removeAll();
	$('#returnSubmitId').prop('disabled',false);
}

/**
 * find available qty
 * @param {*} productId 
 */
function qtyAvailable(productId){
	var productDB=fetchProductByProductId(productId);
	var counterOrderProductDetails=fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId);
	var oldQty=0;
	for(var i=0; i<counterOrderProductDetails.length; i++){
		var product=counterOrderProductDetails[i].product.product;

		if(product.productId==productId){
			oldQty+=counterOrderProductDetails[i].purchaseQuantity;
		}	
	}

	var qty=0;
	for(var i=0; i<productCart.length; i++){
		var product=productCart[i][0];
		if(product.productId==productId){
			qty+=parseInt(productCart[i][1]);
		}		 
	}
	
	var availableQty=parseInt(productDB.currentQuantity)+parseInt(oldQty)-parseInt(qty);
	return availableQty;
}

/**
 * appends table rows using productCart[] 
 * @returns
 */
function refreshTable(){
	//productCart.push([product,qty,mrp,total,freeNonFreeRdo,disc,discAmt,percentageCheck]);
	$('#cartTbl').empty();
	for(var i=0; i<productCart.length; i++){
		var product=productCart[i][0];
		var qty=productCart[i][1];	
		var mrp=productCart[i][2];
		var total=parseFloat(productCart[i][3]).toFixedVSS(2);
		var type=productCart[i][4];
		var disc=parseFloat(productCart[i][5]).toFixedVSS(2);
		var discAmt=parseFloat(productCart[i][6]).toFixedVSS(2);
		var percentageCheck =productCart[i][7];
		
		var productName='<td>'+product.productName+'</td>';
		var mrpCell='<td><span id="productMrp_'+type+'_'+product.productId+'" >'+mrp+'</span></td>';
		
		var discountField='<td><span id="productDisc_'+type+'_'+product.productId+'">'+disc+'</span></td>'+
			              '<td><span id="productDiscAmt_'+type+'_'+product.productId+'">0</span></td>';		
		
		if(type=="Free"){
			productName='<td>'+product.productName+'<font color="green">-(Free)</font></td>';
			mrpCell='<td>0</td>';
			discountField='<td><span id="productDisc_'+type+'_'+product.productId+'">0</span></td>'+
						  '<td><span id="productDiscAmt_'+type+'_'+product.productId+'">0</span></td>';
		}

		$('#cartTbl').append('<tr>'+
				//'<td>'+(i+1)+'</td>'+
				  productName+
				//'<td><span id="productCurrQty_'+product.productId+'"'+product.currentQuantity+'</span></td>'+
				  mrpCell+
				'<td><span id="productQty_'+type+'_'+product.productId+'" >'+qty+'</span></td>'+
				'<td><input type="text" id="productRetQty_'+type+'_'+product.productId+'" value="0" class="editable center qty"  style="width: 4em;" /></td>'+
				 discountField+
				'<td><span id="productTotal_'+type+'_'+product.productId+'">0</span></td>'+
				'<td><input type="text" id="productRetReason_'+type+'_'+product.productId+'" value="" style="width: 10em;" /></td>'+
				'</tr>');
				 
		/**
		 * only number allowed without decimal 
		 */
		$('#productRetQty_'+type+'_'+product.productId).off("keypress");
		$('#productRetQty_'+type+'_'+product.productId).keypress(function( event ){
		    var key = event.which;						    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});
		
		/**
		 * qty change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productRetQty_'+type+'_'+product.productId).off("keyup");
		$('#productRetQty_'+type+'_'+product.productId).keyup(function(){
			var id=$(this).attr('id');
			var type=id.split('_')[1];
			var productId=id.split('_')[2];
			var productData;
			
			for(var i=0; i<productCart.length; i++){
				if(productId==productCart[i][0].productId && type==productCart[i][4]){
					productData=productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}
			
			var product=productData[0];
			var qty=productData[1];
			var returnQty=$('#productRetQty_'+type+'_'+productId).val();
			var mrp=productData[2];
			if(returnQty==undefined || returnQty==''){
				returnQty=0;
			}
			
			if(parseInt(returnQty)>parseInt(qty)){
				productCart.push(productData);
				$('#productRetQty_'+type+'_'+productId).val(0);
				$('#productRetQty_'+type+'_'+productId).keyup();
				//Materialize.Toast.removeAll();
				Materialize.toast('Return Quantity can not be greater than Issued Quantity', '3000', 'red lighten-2');
				return false;
			}
			
			var total=parseInt(returnQty)*parseFloat(mrp);
			
			var percentageCheck=productData[7];
			var disc=productData[5];
			var discAmt=productData[6];
			var reason=productData[9];
									
			if(returnQty==0){
				discAmt=0;
				total=0;
			}else{
				discAmt=(total*disc)/100;
				total=total-discAmt;
			}
			
			if(type=="Free"){
				total=0;
				mrp=0;
				disc=0;
				discAmt=0;
				
				percentageCheck=false;
			}
			
			$('#productDisc_'+type+'_'+productId).text(parseFloat(disc).toFixedVSS(2));	
			$('#productDiscAmt_'+type+'_'+productId).text(parseFloat(discAmt).toFixedVSS(2));
			$('#productTotal_'+type+'_'+productId).text(parseFloat(total).toFixedVSS(2));
			
			productCart.push([
			                  product,
			                  qty,
			                  mrp,
			                  parseFloat(total).toFixedVSS(2),
			                  type,
			                  parseFloat(disc).toFixedVSS(2),
			                  parseFloat(discAmt).toFixedVSS(2),
			                  percentageCheck,
			                  returnQty,
			                  reason]);
			
			//find final total Amount
			findTotal();
			
		});
		
		$('#productRetReason_'+type+'_'+product.productId).off("keyup");
		$('#productRetReason_'+type+'_'+product.productId).on("blur keyup",function(){
			var id=$(this).attr('id');
			var type=id.split('_')[1];
			var productId=id.split('_')[2];
			var productData;
			
			for(var i=0; i<productCart.length; i++){
				if(productId==productCart[i][0].productId && type==productCart[i][4]){
					productData=productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}
			
			productData[9]=$('#productRetReason_'+type+'_'+productId).val();
			
			productCart.push(productData);
			
		});
	}
	
	$('#cartTbl').append('<tr style="border-top: 2px solid black;">'+
			'<td align="center" >Total Amount</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td><span id="totalValue">0</span></td>'+
			'<td></td>'+
			'</tr>');
	$('#cartTbl').append('<tr>'+
			'<td align="center" >Whole Discount</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td><span id="totalDisc">0</span>%</td>'+
			'<td></td>'+
			'<td><span id="totalDiscAmt">0</span></td>'+
			'<td></td>'+
			'</tr>');
	$('#cartTbl').append('<tr style="border-top: 2px solid black;">'+
			'<td align="center" >Net Payable</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td><span id="totalValueAftDisc">0</span></td>'+
			'<td></td>'+
			'</tr>');
	
	//find final total Amount
	findTotal(); 
	
}


/**
 * find final totalAmount
 */
function findTotal(){
	var finalTotal=0,finalTotalTaxable=0;
	let returnQty = 0;
	for(var i=0; i<productCart.length; i++){
		finalTotal+=parseFloat(productCart[i][3]);
		
		var correctAmoutWithTaxObj=calculateProperTax(parseFloat(productCart[i][3]),parseFloat(productCart[i][0].categories.igst));
		finalTotalTaxable+=parseFloat(correctAmoutWithTaxObj.unitPrice);

		returnQty+=parseFloat(productCart[i][8]);
	}
	finalTotal=finalTotal.toFixedVSS(2);
	
	var discMain=counterOrderOld.discount;		
	if(discMain==""){
		discMain=0;
	}
	
	//with tax calc
	var discountAmount,disc;
	if(counterOrderOld.discountType=="Percentage"){
		disc=discMain;
		discountAmount=(finalTotal*discMain)/100;			
	}else if(counterOrderOld.discountType=="Amount"){
		/*discountAmount=discMain;
		disc=(discountAmount/finalTotal)*100;*/
		disc=mainPercentage;
		discountAmount=(finalTotal*mainPercentage)/100;	
	}else{
		disc=0;
		discountAmount=(finalTotal*discMain)/100;
	}
	disc=parseFloat(disc).toFixedVSS(2);
	discountAmount=parseFloat(discountAmount).toFixedVSS(2);
	
	newTotalAmount=(finalTotal-discountAmount);
	newTotalAmount=Math.round(newTotalAmount);

	discountMain=counterOrderOld.discount;
	if(counterOrderOld.discountType=="Amount"){
		discountMain=(parseFloat(finalTotal)*parseFloat(mainPercentage))/100;
	}
	
	$('#totalValue').html('<font color="blue" ><b>'+finalTotal+'</b></font>');
	$('#totalDisc').html('<font color="green" ><b>'+disc+'</b></font>');
	$('#totalDiscAmt').html('<font color="green" ><b>'+discountAmount+'</b></font>');
	$('#totalValueAftDisc').html('<font color="red" ><b>'+newTotalAmount+'</b></font>');
	
	//without tax calc
	if(counterOrderOld.discountType=="Percentage"){
		disc=discMain;
		discountAmount=(finalTotalTaxable*discMain)/100;			
	}else if(counterOrderOld.discountType=="Amount"){
		/*discountAmount=discMain;
		disc=(discountAmount/finalTotalTaxable)*100;*/
		disc=mainPercentage;
		discountAmount=(finalTotalTaxable*mainPercentage)/100;	
	}else{
		disc=0;
		discountAmount=(finalTotalTaxable*discMain)/100;
	}
	disc=parseFloat(disc).toFixedVSS(2);
	discountAmount=parseFloat(discountAmount).toFixedVSS(2);
	
	newTotalAmountWithoutTax=(finalTotalTaxable-discountAmount);
	newTotalAmountWithoutTax=Math.round(newTotalAmountWithoutTax);
	
	var paidAmt=0
	if(paymentCounterList.length>0){
		for(var i=0; i<paymentCounterList.length; i++)	
		{
			paidAmt+=paymentCounterList[i].currentAmountPaid.toFixedVSS(2)-paymentCounterList[i].currentAmountRefund.toFixedVSS(2)
		}
	}
	
	var totalAmountWithTax=counterOrderOld.totalAmountWithTax;
	var needToPaid=totalAmountWithTax-newTotalAmount;
	var balAmtForPaid=0,balAmt=0;
	
	if(paidAmt>needToPaid){
		refundAmt=parseFloat(paidAmt)-parseFloat(needToPaid);
		$('#balanceTotalValue').html('Refund &#8377; <font color="green" ><b>'+Math.round(refundAmt)+'</b></font>');
	}else{
		refundAmt=0;
		$('#balanceTotalValue').html('Refund &#8377; <font color="green" ><b>0</b></font>');
	}
}





/**
 * fetch product details by productId
 * @param {*} productId 
 */
function fetchProductByProductId(productId){
	
	var product;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByProductId?productId="+productId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			product=data;
		},
		error: function(xhr, status, error) {
			console.log("product not loaded");
		}
	});
	return product;
}

function fetchCounterOrderProductDetailsByCounterOrderId(counterOrderId){
	var counterOrderProductDetails;
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchCounterOrderProductDetailsByCounterOrderId?counterOrderId="+counterOrderId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			counterOrderProductDetails=data;
		},
		error: function(xhr, status, error) {
			console.log("counterOrderProductDetails not loaded");
		}
	});
	return counterOrderProductDetails;
}

function fetchCounterOrderByCounterOrderId(counterOrderId){
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchCounterOrderByCounterOrderId?counterOrderId="+counterOrderId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			counterOrderOld=data.counterOrder;
			gateKeeperName=data.gateKeeperName;
		},
		error: function(xhr, status, error) {
			console.log("counterOrder not loaded");
		}
	});
}

function fetchPaymentCounterOrder(counterOrderId){
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchPaymentCounterOrder?counterOrderId="+counterOrderId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			paymentCounterList=data;
		},
		error: function(xhr, status, error) {
			paymentCounterList='';
			console.log("paymentCounterList not loaded");
		}
	});
}

function submitReturnCounterOrder(formData){
	$('.disableButtons').attr('disabled','disabled');
	$.ajax({
		type:"POST",
		url : myContextPath+"/saveCounterReturnOrder",
		headers : {
			'Content-Type' : 'application/json'
		},
		data : JSON.stringify(formData),
		async : false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
		   },
		success:function(data){
			if(data.status=="Success"){
				//Materialize.Toast.removeAll();
				Materialize.toast('Return Counter Order SuccessFully' , '3000', 'teal lighten-2');

				setTimeout(function() {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();

					//url to redirect openCounter
					window.location.href=myContextPath+"/counterOrderReport?range=today";
				},5000);
					
			}else{
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				//Materialize.Toast.removeAll();
				Materialize.toast(data.errorMsg, '3000', 'red lighten-2');
			}
		},
		error: function(xhr, status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			//Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong' , '3000', 'teal lighten-2');
		}
	});
}
