var productList = [];

//var orderProductAndQuantityList = new Hashtable();
var orderSupplierList = [];
var orderSupplierListTemp = [];
var count=1;
var productListForDamage;
var submitBoolean=false;

/* for single inventory add */
let totalAmount=0;
let totalAmountWithTax=0;
let amountBeforeDiscount=0; // amount with Tax before Discount
let amountWithoutTaxBeforeDiscount=0; // amount without tax before discount
let totalAmountBeforeDisc=0;
let totalAmountWithTaxBeforeDisc=0;
let totalQuantity=0;
let discountAmount=0;
let discountPercentage=0;
let discountType=0;
let discountOnMRP="YES";
let isDiscountGiven=false;
/* for single inventory add */

$(document).ready(function() {
						
			/*if(productListFromController!=null && productListFromController!=undefined)
						{
							var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
							orderSupplierList.put(count,prodctlst);
						}*/
						$('#orderQuantity').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
						        event.preventDefault();
						});
						$('#orderQuantityOneOrderId').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
						        event.preventDefault();
						}); 
						$('#supplierMobileNumberOneOrderId').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
						        event.preventDefault();
						}); 
						$('#supplierMobileNumber').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
						        event.preventDefault();
						}); 
						//add order product-supplier to table/cart
						$('#addOrderProduct').click(function(){
							
							var productIdForOrder=$('#productIdForOrder').val();
							if(productIdForOrder==="0")
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Please Select Product!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
	                   	     	//$('#msgHead').text("Add Product in Cart Warning :");
	                   	     	$('#msg').text("Select Product"); */
	                   	     	return false;
							}
							
							var buttonStatus=$('#addOrderProduct').text();
			            	if(buttonStatus==="Update")
			            	{
			            		updaterow($('#currentUpdateRowId').val());
			            		return false;
			            	}
							
			            	var supplierRateForOrder=$('#supplierRateForOrder').val();
			            	var supplierMRPRateForOrder=$('#supplierMRPRateForOrder').val();
							
							var supplierIdForOrder=$('#supplierIdForOrder').val();
							var orderQuantity=$('#orderQuantity').val().trim();
							var supplierMobileNumber=$('#supplierMobileNumber').val().trim();
							
							
							if(supplierMobileNumber==="" || supplierMobileNumber===undefined || !(/^\d{10}$/.test(supplierMobileNumber)))
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Supplier mobile number not valid!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
	                   	     	$('#msgHead').text("Add Product in Cart Warning :");
	                   	     	$('#msg').text("Supplier mobile number not valid"); */
	                   	     	return false;
							}
							if(orderQuantity==="0" || orderQuantity==="" || orderQuantity===undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Order Quantity not valid!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
	                   	     	$('#msgHead').text("Add Product in Cart Warning :");
	                   	     	$('#msg').text("Order Quantity not valid");*/ 
	                   	     	return false;
							}
							//check product-supplier already exist or not
							for (var i=0; i<orderSupplierList.length; i++) {
		              			var value=orderSupplierList[i];
		              			if(value[0]===supplierIdForOrder && value[1] === productIdForOrder)
		              			{
									Materialize.Toast.removeAll();  
									Materialize.toast('This Product and Supplier is already added to Cart!', '4000', 'teal lighten-3');
		              				/*$('#addeditmsg').modal('open');
		                   	     	$('#msgHead').text("Product Select Warning");
		                   	     	$('#msg').text("This Product and Supplier is already added to Cart"); */
		                   	     	return false;
		              			}
		              		}
							
							//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
							
							//manage product-supplier details in orderSupplierList[]
							var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
							orderSupplierList.push(prodctlst);
							//alert(orderSupplierList.entries());
							var supplierName=$('#supplierIdForOrder option:selected').text();
							var productName=$('#productIdForOrder option:selected').text();
							var supplierRate=$('#supplierRateForOrder').val();
							
							$("#orderCartTb").append(	"<tr id='rowdel_" + count + "' >"+
							                            "<td id='rowcount_" + count + "'>"+count+"</td>"+
							                            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
							                            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
							                            "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+supplierRate+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+supplierMRPRateForOrder+">"+supplierMRPRateForOrder+"</td>"+
							                            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
							                            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
							                            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
							                        	"</tr>");
							
							count++;
							//alert(orderSupplierList.entries());
							//resetOrderSelectionData();
							
							/**
							 * reset quantity and product list after add 
							 */

							$('#orderQuantity').val('');
							var source = $("#productIdForOrder");
							source.val(0);
							source.change();
							$('#supplierRateForOrder').val('');
							$('#supplierMRPRateForOrder').val('');
						}); 
						
						//on supplier change set mrp and unit price
						$('#supplierIdForaddQuantity').change(function(){
							var productId=$('#productIdForAddQuantity').val();
							var supplierId=$('#supplierIdForaddQuantity').val();
							
							if(productId==0){
								return false;
							}
							if(supplierId==0){
								return false;
							}
							$.ajax({
								url : myContextPath+"/fetchSupplierRateByProductIdamdSupplierId?supplierId="+supplierId+"&productId="+productId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									if(data !=null){
										var supplierproduct=data;
										var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.igst))/100)).toFixedVSS(0);
				    					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.igst));
										
				    					$('#supplierMRPRate').val(parseFloat(correctAmoutWithTaxObj.mrp).toFixedVSS(2));
										$('#supplierMRPRate').change();
				    					
										$('#supplierRate').val(parseFloat(supplierproduct.supplierRate).toFixedVSS(2));
										$('#supplierRate').change();
										
										findTotalAmountAndTax();
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
									}
									
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
							/*		$('#addeditmsg').find("#modalType").addClass("warning");
									 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
									$('#addeditmsg').modal('open');
					       	     	//$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Supplier Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
						});
						//make order details string
						//save single product-supplier order 
						$('#orderProductsButtonOneOrderId').click(function(){
							
							var supplierId=$('#supplierIdOneOrderId').val();
							var productId=$('#productIdOneOrderId').val();
							var quantity=$('#orderQuantityOneOrderId').val().trim();
							var mobileNumber=$('#supplierMobileNumberOneOrderId').val().trim();
							
							if(supplierId==="0")
							{
								$('#add').modal('close');
								Materialize.Toast.removeAll();
								Materialize.toast('Select Supplier!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
								$('#msgHead').text("Supplier Order Warning :");
								$('#msg').html("Select Supplier");*/
								return false;
							}
							if(productId==="0")
							{
								$('#add').modal('close');
								Materialize.Toast.removeAll();
								Materialize.toast('Select Product!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
								$('#msgHead').text("Supplier Order Warning :");
								$('#msg').html("Select Product");*/
								return false;
							}
							if(mobileNumber==="" || mobileNumber===undefined || !(/^\d{10}$/.test(mobileNumber)))
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Supplier mobile number is not valid!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
	                   	     	$('#msgHead').text("Supplier Order Warning :");
	                   	     	$('#msg').text("Supplier mobile number not valid"); */
	                   	     	return false;
							}
							if(quantity==="0" || quantity==="" || quantity===undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Add Quantity for order!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
	                   	     	$('#msgHead').text("Supplier Order Warning :");
	                   	     	$('#msg').text("Order Quantity not valid"); */
	                   	     	return false;
							}
							
			            	var productIdList=supplierId+"-"+productId+"-"+quantity+"-"+mobileNumber;
			            	//alert(productIdList);
			            	$('#productWithSupplikerlistOneOrderId').val(productIdList);
			            	
			            	$("#orderProductsButtonOneOrderId").attr('disabled','disabled');
			            	
			            	var form = $('#orderBookFormOneOrderId');

							$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										data : $("#orderBookFormOneOrderId").serialize(),
										success : function(data) 
										{
											if(data=="Success")
											{													
													$('#singleOrder').modal('close');
													$('#addeditmsg').find("#modalType").addClass("success");
													 $('#addeditmsg').find(".modal-action").removeClass("teal lighten-2 teal").addClass("teal lighten-2");
													$('#addeditmsg').modal('open');
													//$('#msgHead').text("Success : ");
													$('#msg').text("Order Booked SuccessFully");
													productIdList=null;
											}
											else
											{
												   $("#orderProductsButtonOneOrderId").removeAttr("disabled");
													$('#singleOrder').modal('close');
													$('#addeditmsg').find("#modalType").addClass("warning");
													 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
													$('#addeditmsg').modal('open');
													//$('#msgHead').text("Failed : ");
													$('#msg').html("Order Booked Failed");
											}
										}
							});
							
						});
						
						/*$('#orderProductsButton').click(function(){
							//alert(orderSupplierList.entries());
							var productIdList="";
			            	for (var i=0; i<orderSupplierList.length; i++) {
			            		var value=orderSupplierList[i];
			            		productIdList=productIdList+value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]+",";
			          		}
			            	productIdList=productIdList.slice(0,-1)
			            	//alert(productIdList);
			            	$('#productWithSupplikerlist').val(productIdList);
			            	
			            	$("#orderProductsButton").attr('disabled','disabled');
			            	
			            	var form = $('#orderBookForm');

							$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										data : $("#orderBookForm").serialize(),
										success : function(data) 
										{
											if(data=="Success")
											{
													$('#order').modal('close');
													$('#addeditmsg').find("#modalType").addClass("success");
													 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
													$('#addeditmsg').modal('open');
													//$('#msgHead').text("Success : ");
													$('#msg').text("Order Booked SuccessFully");
													orderSupplierList=[];
													//fetchProductList();
											}
											else
											{
												$('#addeditmsg').find("#modalType").addClass("warning");
												 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
												$('#addeditmsg').modal('open');
												//$('#msgHead').text("Failed : ");
												$('#msg').text("Order Booked Failed");
												
												$("#orderProductsButton").removeAttr("disabled");
											}
										}
							});
							
						});*/
						/**
						 * validation of supplier selection, quantity entered, pay date , bill date, bill number
						 */
						//single product inventory saving and update product current quantity
						$('#addQuantityId').click(function(){
							
							var supplierIdd=$("#supplierIdForaddQuantity").val();
							if(supplierIdd==="0")
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Select Supplier!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	        $('#msgHead').text("Product Add Inventory Warning");
			           	     	$('#msg').text("Select Supplier");*/
								return false;
							}
							
							var val = $("#productIdForAddQuantity").val(); 
				             var qty = $('#addquantity').val();
			             	if(qty==="" || qty==undefined)
							{
								Materialize.Toast.removeAll();
			             		Materialize.toast('Enter Quantity!', '4000', 'teal lighten-3');
			             		/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Inventory Warning");
			           	     	$('#msg').text("Enter Quantity");*/
								return false;
							}
							var payDate=$('#paymentDate').val();
							if(payDate==="" || payDate==undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Select Payment date!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Inventory Warning");
			           	     	$('#msg').text("Select Payment date");*/
								return false;
							}
							var billDate=$('#selectedDate').val();
							if(billDate==="" || billDate==undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Select bill date!', '4000', 'teal lighten-3');
								return false;
							}
							var bill_number=$('#bill_number').val();
							if(bill_number==="" || bill_number==undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Enter Bill Number!', '4000', 'teal lighten-3');
								return false;
							}
							var paymentDate=new Date($('#paymentDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							if(paymentDate < today)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Payment date must be today or after todays date!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	        $('#msgHead').text("Product Add Inventory Warning");
			           	     	$('#msg').text("Payment date must be today or after todays date");*/
								return false;
							}
							var prdData=[val,qty];
							let productId=val;
							// productList.push(prdData);           	
			            	
			            	
			            	// //alert(productList.entries());
			            	// var productIdList="";
			            	// for (var i=0; i<productList.length; i++) {
			            	// 	var value=productList[i]; 
			            	// 	productIdList=productIdList+value[0]+"-"+value[1]+",";
			          		// }
							// productIdList=productIdList.slice(0,-1)
							
			            	//alert(productIdList);
			            	// $('#productlistinput').val(productIdList);
			            	
			            	$("#addQuantityId").attr('disabled','disabled');
			            	
							// var form = $('#addQuantityForm');
							
							/**
							 * Making Inventory Add Request
							 */
							let inventoryRequest = {};
							inventoryRequest.inventory={
								supplier:{
									supplierId : supplierIdd
								},
								totalAmount:totalAmount,
								totalAmountTax:totalAmountWithTax,
								totalQuantity:qty,
								billNumber:bill_number,
								billDate:new Date(billDate),
								inventoryPaymentDatetime:paymentDate,
								discountAmount:0,
								discountPercentage:0,
								discountType:'PERCENTAGE',
								discountOnMRP:'YES',
								totalAmountBeforeDiscount:totalAmountBeforeDisc,
								totalAmountTaxBeforeDiscount:totalAmountWithTaxBeforeDisc,
								isDiscountGiven:isDiscountGiven
							}

							inventoryRequest.inventoryDetailsList=[];

							inventoryRequest.inventoryDetailsList.push(
								{
									//inventoryDetailsId,
									product:{
										productId : productId,
									},
									rate:parseFloat($('#supplierMRPRate').val()),
									quantity:qty,
									amount:totalAmountWithTax,
									discountAmount:discountAmount,
									discountPercentage:discountPercentage,
									discountType:discountType,
									amountBeforeDiscount:amountBeforeDiscount,
									discountOnMRP:discountOnMRP
								}
							);
												
							let url= myContextPath+"/saveInventory";

							$.ajax({

										type : 'POST',
										url : url,
										data : JSON.stringify(inventoryRequest),
										headers: {
											'Content-Type': 'application/json'
										},
										success : function(data) {
											if(data.success)
											{
													$('#add').modal('close');
													$('#addeditmsg').find("#modalType").addClass("success");
													 $('#addeditmsg').find(".modal-action").removeClass("teal lighten-2 teal").addClass("teal lighten-2");
													$('#addeditmsg').modal('open');
													//$('#msgHead').text("Success : ");
													$('#msg').text("Quantity updated SuccessFully");

													setTimeout(function(){
														$('#addeditmsg').modal('close');
													},800);
													fetchProductList();
													productList=[];													
											}
											else
											{
												$('#addeditmsg').find("#modalType").addClass("warning");
												 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
													$('#addeditmsg').modal('open');
													//$('#msgHead').text("Failed : ");
													$('#msg').text("Quantity updatation Failed");
													$("#addQuantityId").removeAttr("disabled");
											}
										}
							});
			            	
						});
						
						$('#percentageCheck').click(function(){
							$('#discountId').keyup();
						});
						$('#discountOnMRPCheck').click(function(){
							$('#discountId').keyup();
						});
						
						$('#discountId').keyup(function(){
						
							var discMain=$('#discountId').val();		
							if(discMain==""){
								discMain=0;
							}
							discMain=parseFloat(discMain);

							var qty= $('#addquantity').val();
							if(qty==="" || qty===undefined)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Please Enter quantity', '3000', 'teal lighten-2');
								return false;
							}
					
					
							if($('#percentageMainCheck').prop("checked") == true){
								if(discMain>100 || discMain<0){
									$('#discountId').val(0);
									$('#discountId').keyup();
									Materialize.Toast.removeAll();
									Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
									return false;
								}
							}else{
								
								if($('#discountOnMRPCheck').prop("checked") == true){
									// Discount on Mrp
									if(discMain>amountBeforeDiscount){
										$('#discountId').val(0);
										$('#discountId').keyup();
										Materialize.Toast.removeAll();
										Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
										return false;
									}
								}else{
									
									// Discount on Unit Price
									if(discMain>amountWithoutTaxBeforeDiscount){
										$('#discountId').val(0);
										$('#discountId').keyup();
										Materialize.Toast.removeAll();
										Materialize.toast('Discount Amount Must be  Must be Less then Total  Taxable Amount', '3000', 'teal lighten-2');
										return false;
									}
								}
								//old code
								/*if(discMain>totalAmountWithTaxBeforeDisc){
									$('#discountId').val(0);
									$('#discountId').keyup();
									Materialize.Toast.removeAll();
									Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
									return false;
								}*/
							}
					
							findTotalAmountAndTax();
						});

						//on quantity change on single product inventory find taxable 
						$('#addquantity').keyup(function(){
							findTotalAmountAndTax();							
						});
						//set supplier mobile when supplier change
						//on change brand id change product list by supplierId,brandId,categoryId
						$('#supplierIdForOrder').change(function(){
							var supplierId=$('#supplierIdForOrder').val();
							
							$("#brandIdForOrder").change();
							
							/*$('#productIdForOrder').empty();
							$("#productIdForOrder").append('<option value="0">Choose Product</option>');
							
							$.ajax({
								url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									for(var i=0; i<data.length; i++)
									{								
										//alert(data[i].productId +"-"+ data[i].productName);
										$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
									}	
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									Materialize.toast('Product List Not Found!', '4000', 'teal lighten-3');
									$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product List Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);
									}
							});*/
							
							
							var supplierId=$('#supplierIdForOrder').val();
							$('#supplierMobileNumber').val('');
							$('#supplierMobileNumber').change();
							if(supplierId==="0")
							{
								return false;
							}
							$.ajax({
								url : myContextPath+"/fetchSupplierBySupplierId?supplierId="+supplierId,
								dataType : "json",
								success : function(data) {
									
									$('#supplierMobileNumber').val(data.contact.mobileNumber);
									$('#supplierMobileNumber').change();
									
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									  //alert("Error");
									}
							});
						});
						//when supplier change set supplier mobile number
						$('#supplierIdOneOrderId').change(function(){
							
							var supplierId=$('#supplierIdOneOrderId').val();
							
							if(supplierId==="0")
							{
								return false;
							}
							
							$('#supplierMobileNumberOneOrderId').val('');
							$('#supplierMobileNumberOneOrderId').change();
							$.ajax({
								url : myContextPath+"/fetchSupplierBySupplierId?supplierId="+supplierId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									$('#supplierMobileNumberOneOrderId').val(data.contact.mobileNumber);
									$('#supplierMobileNumberOneOrderId').change();
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Supplier Details Not Found!', '4000', 'teal lighten-3');
									/*$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Supplier Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
						});
						
						//on change category id change product list by supplierId,brandId,categoryId
						$('#categoryIdForOrder').change(function(){
							
							var categoryId=$('#categoryIdForOrder').val();
							var brandId=$('#brandIdForOrder').val();
							var supplierId=$('#supplierIdForOrder').val();
							
							$('#productIdForOrder').empty();
							$("#productIdForOrder").append('<option value="0">Choose Product</option>');
							$.ajax({ 
								url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
								dataType : "json",
								async :false,
								success : function(data) {
									
										for(var i=0; i<data.length; i++)
										{								
											//alert(data[i].productId +"-"+ data[i].productName);
											$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
										}	
										//alert("done");
										$("#productIdForOrder").change();
									
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										//alert("Error ");
									}
								});
							
						});
						
						//on change brand id change product list by supplierId,brandId,categoryId
						$('#brandIdForOrder').change(function(){
							
							var categoryId=$('#categoryIdForOrder').val();
							var brandId=$('#brandIdForOrder').val();
							var supplierId=$('#supplierIdForOrder').val();
							
							$('#productIdForOrder').empty();
							$("#productIdForOrder").append('<option value="0">Choose Product</option>');
							$.ajax({ 
								url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
								dataType : "json",
								async :false,
								success : function(data) {
									
									for(var i=0; i<data.length; i++)
									{								
										//alert(data[i].productId +"-"+ data[i].productName);
										$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
									}	
									//alert("done");
									$("#productIdForOrder").change();
									
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										//alert("Error ");
									}
								});
							
						});
						//after product change set mrp and unit rate 
						$('#productIdForOrder').change(function(){
							
							$('#supplierMRPRateForOrder').val('');
	    					$('#supplierMRPRateForOrder').change();
	    					
	    					$('#supplierRateForOrder').val('');
							$('#supplierRateForOrder').change();
							
							var productId=$('#productIdForOrder').val();
							if(productId==0){
								return false;
							}
							if(supplierId==0){
								return false;
							}
							
							var supplierId=$('#supplierIdForOrder').val();
							$.ajax({
								url : myContextPath+"/fetchSupplierRateByProductIdamdSupplierId?supplierId="+supplierId+"&productId="+productId,
								dataType : "json",
								success : function(data) {
									if(data!=null){
										var supplierproduct=data;
										var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.igst))/100)).toFixedVSS(0);
				    					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.igst));
										
				    					$('#supplierMRPRateForOrder').val(correctAmoutWithTaxObj.mrp);
				    					$('#supplierMRPRateForOrder').change();
				    					
				    					$('#supplierRateForOrder').val(parseFloat(data.supplierRate).toFixedVSS(2));
										$('#supplierRateForOrder').change();
									}
									
									
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									 // alert("Error ");
								}
							});
							
						});
						
						$("#OrderMobileNumberchecker").change(function() {
						    var ischecked= $(this).is(':checked');
						    if(ischecked)
						    {
						    	$('#supplierMobileNumber').removeAttr('readonly','readonly');
						    	$('#supplierMobileNumber').removeClass('grey lighten-3');
						    }
						    else
						    {
						    	$('#supplierMobileNumber').attr('readonly', 'readonly');
						    	$('#supplierMobileNumber').addClass('grey lighten-3');
						    	
						    }
						});
						$("#OrderMobileNumbercheckerOneOrderId").change(function() {
						    var ischecked= $(this).is(':checked');
						    if(ischecked)
						    {
						    	$('#supplierMobileNumberOneOrderId').removeAttr('readonly', 'readonly');
						    	$('#supplierMobileNumberOneOrderId').removeClass('grey lighten-3');
						    }
						    else
						    {
						    	$('#supplierMobileNumberOneOrderId').attr('readonly', 'readonly');
						    	$('#supplierMobileNumberOneOrderId').addClass('grey lighten-3');
						    	
						    }
						});
						
						/**
						 * Define Damage Product Start
						 */
						$('#openSetDamageModal').click(function(){
							
							submitBoolean=false;
							
							$('.modal').modal();
							$('#setDamageModal').modal('open');	
							
							var source=$('#damageProductBrandId');
							source.val('0');
							source.change();
							
							source=$('#damageProductCategoryId');
							source.val('0');
							source.change();
							
							$('#damageProductDamageQty').val('');
							$('#damageProductDamageQtyReason').val('');
						});
						//only number allowed
						$('#damageProductDamageQty').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
						        event.preventDefault();
						});
						//validation for damage quantity with current quantity
						$('#damageProductDamageQty').keyup(function(){
							var currrentQuantity=$('#damageProductCurrentQty').val();
							var damageQuantity=$('#damageProductDamageQty').val();
							
							if(currrentQuantity==undefined || currrentQuantity==''){
								currrentQuantity=0;
							}
							if(damageQuantity==undefined || damageQuantity==''){
								damageQuantity=0;
							}	
							if(parseInt(currrentQuantity)<parseInt(damageQuantity)){
								$('#damageProductDamageQty').val(0);
								Materialize.Toast.removeAll();
								Materialize.toast('Damage quantity can not be more than Currrent quantity', '4000', 'teal lighten-3');
							}
						});
						//refresh product list by brand and category 
						$('#damageProductBrandId').change(getProductListByBrandAndCategory);
						$('#damageProductCategoryId').change(getProductListByBrandAndCategory);
						
						//on change product set current quantity
						$('#damageProductId').change(function(){
							var productId=$('#damageProductId').val();
							
							$('#damageProductCurrentQty').val('');
							
							if(productId==0){
								return false;
							}
							
							for(var i=0; i<productListForDamage.length; i++){
								product=productListForDamage[i];
								if(product.productId==productId){
									$('#damageProductCurrentQty').val(product.currentQuantity);
									$('#damageProductCurrentQty').change();
									return false;
								}
							}
						});
						//validation of damage quantity non zero, product id and reason
						$('#damageProductSubmitButton').click(function(){
							$("#damageProductDamageOption").change();
							var damageQuantity=$('#damageProductDamageQty').val();
							var productId=$('#damageProductId').val();
							var reason=$('#damageProductDamageQtyReason').val();
							
							if(productId==0){
								Materialize.Toast.removeAll();
								Materialize.toast('Select Product', '4000', 'teal lighten-3');
								return false;
							}else if(damageQuantity=='' || damageQuantity==undefined){
								Materialize.Toast.removeAll();
								Materialize.toast('Enter Damage Quantity', '4000', 'teal lighten-3');
								return false;
							}else if(damageQuantity==0){
								Materialize.Toast.removeAll();
								Materialize.toast('Enter Damage Quantity', '4000', 'teal lighten-3');
								return false;
							}else if(reason=='' || reason==undefined){
								Materialize.Toast.removeAll();
								Materialize.toast('Enter Reason', '4000', 'teal lighten-3');
								return false;
							}
							//confirmation of set damage
							Materialize.Toast.removeAll();
							var $toastContent = $('<span>Making '+damageQuantity+' Quantity as Damage?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="defineDamage()">Yes</button>  '));
							Materialize.toast($toastContent, "abc");
						});
						
						
						
						/*Define damage product end*/
						
//						$('#OrderMobileNumberchecker').attr('checked', false);
						$('#supplierMobileNumber').attr('readonly', true);
						
//						$('#OrderMobileNumbercheckerOneOrderId').attr('checked', false);
						$('#supplierMobileNumberOneOrderId').attr('readonly', true);
						
					});
/**
 * submit define damage 
 */
function defineDamage(){
	$('#defineDamageForm').submit();
}
//refresh product list by brand and category 
function getProductListByBrandAndCategory()
{
	var brandId=$('#damageProductBrandId').val();
	var categoryId=$('#damageProductCategoryId').val();
	
	if(brandId==undefined || brandId==''){
		brandId=0;
	}
	if(categoryId==undefined || categoryId==''){
		categoryId=0;
	}	
	
	$('#damageProductId').empty();
	$("#damageProductId").append('<option value="0">Choose Product</option>');
	
	$.ajax({
		url:myContextPath+"/fetchProductListByBrandIdAndCategoryId?categoryId="+categoryId+"&brandId="+brandId,
		dataType:"json",
		async:false,
		success:function(data){
			productListForDamage=data;
			
			$('#damageProductId').empty();
			$("#damageProductId").append('<option value="0">Choose Product</option>');
			
			for(var i=0; i<productListForDamage.length; i++){
				product=productListForDamage[i];				
				$("#damageProductId").append('<option value='+product.productId+'>'+product.productName+'</option>');			
			}
			
			$('#damageProductId').change();
		},
		error:function(xhr, status, error) {
		  //var err = eval("(" + xhr.responseText + ")");
		  //alert("Error");
		  Materialize.Toast.removeAll();
		  Materialize.toast('Product List Not Found', '4000', 'teal lighten-3');
		}
		
	});
}

/*function showTaxCalculation(productId)
{

	$.ajax({
		url : myContextPath+"/fetchProductTaxcalculation?productId="+productId,
		dataType : "json",
		success : function(data) {
			
			$('#cgstPerId').text(data.cgstPercentage);
			$('#cgstAmtId').text(data.cgstRate);
			$('#sgstPerId').text(data.sgstPercentage);
			$('#sgstAmtId').text(data.sgstRate);
			$('#igstPerId').text(data.igstPercentage);
			$('#igstAmtId').text(data.igstRate);
			$('#taxAmountId').text(data.tax);
			
			$('.modal').modal();
			$('#tax').modal('open');
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  //alert("Error");
			}
	});
	
}*/
/**
 * open single add product inventory add
 * fetch and set supplier list ,product name by product Id
 * @param {*} productId 
 */
function showAddQuantity(productId)
{
	productList=[];
	$('#supplierRate').val('');
	$('#addquantity').val('');
	$('#supplierMRPRate').val('');
	$('#finalTotalAmount').val(0);
	$('#finalTotalAmountWithTax').val(0);
	$('#netfinalTotalAmount').val(0);
	$('#netfinalTotalAmountWithTax').val(0);
	$('#paymentDate').val('');
	$('#bill_number').val('');
	$('#selectedDate').val('');
	$("#addQuantityId").removeAttr("disabled");
	
	$('#supplierIdForaddQuantity').empty();
	$("#supplierIdForaddQuantity").append('<option value="0">Choose Supplier</option>');
	
	$('#discountId').val(0);
	$('#discountId').change();
	$('#discountCalculated').val(0);
	$('#discountCalculated').change();
	$('#percentageCheck').prop('checked', true);
	
	
	
	$.ajax({
		url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
		dataType : "json",
		success : function(data) {
			supplierProductLists=data;
			
			for(var i=0; i<supplierProductLists.length; i++)
			{								
				$('#productName').val(supplierProductLists[i].productName);			
				$('#productIdForAddQuantity').val(supplierProductLists[i].productId);
				$("#supplierIdForaddQuantity").append('<option value='+supplierProductLists[i].supplierId+'>'+supplierProductLists[i].supplierName+'</option>');
			}	
			$('#productName').change();
			$('#supplierIdForaddQuantity').change();
			$('.modal').modal();
			$('#add').modal('open');
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  Materialize.Toast.removeAll();
			  Materialize.Toast.removeAll();
			Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
			}
	});
	
}
//find taxable and total amount in single product inventory add
function findTotalAmountAndTax()
{
	var rate = $('#supplierRate').val();
	var qty= $('#addquantity').val();
	var productId = $("#productIdForAddQuantity").val();
	
	$('#finalTotalAmount').val(0);
	$('#finalTotalAmountWithTax').val(0);
	$('#netfinalTotalAmount').val(0);
	$('#netfinalTotalAmountWithTax').val(0);
	
	if(rate==="" || rate===undefined)
	{
		Materialize.Toast.removeAll();
		Materialize.toast('Please Select Supplier', '3000', 'teal lighten-2');
		rate=0;
		return false;
	}else if(qty==="" || qty===undefined){
		Materialize.Toast.removeAll();
		Materialize.toast('Please Enter quantity', '3000', 'teal lighten-2');
		return false;
	}else{
		
		
		$.ajax({
			url : myContextPath+"/fetchProductByProductId?productId="+productId,
			dataType : "json",
			success : function(data) {
				
				//var tempCgst=data.categories.cgst;
				var tempIgst=data.categories.igst;
				//var tempSgst=data.categories.sgst;
			
				var mrp=((parseFloat(rate))+((parseFloat(rate)*parseFloat(tempIgst))/100)).toFixedVSS(0);
				var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(tempIgst));
				
				
				var ttl=( parseInt(qty) * parseFloat(rate) );
				var amtwithtax=(parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(qty)).toFixedVSS(2);


				/* discount procedure */
				var discount=$('#discountId').val();
				var discountOn=$('#discountOn').val();
				if(discount==''){
					discount=0;
				}
				discount=parseFloat(discount);
				if($('#percentageCheck').prop("checked") == true){
					if(discount>100 || discount<0){
						Materialize.Toast.removeAll();
						Materialize.toast('Invalid Discount Percentage', '3000', 'teal lighten-2');
						return false;
					}
				}else{
					
					// Discount Given in Amount
					if($('#discountOnMRPCheck').prop("checked")==true){
						if(discount>parseFloat(amtwithtax)){
							Materialize.Toast.removeAll();
							Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
							return false;
						}
					}else{
						if(discount>parseFloat(ttl)){
							Materialize.Toast.removeAll();
							Materialize.toast('Discount Amount Must be  Must be Less then Total Taxable Amount', '3000', 'teal lighten-2');
							return false;
						}
					}
					
					//old code
					/*if(discount>parseFloat(amtwithtax)){
						Materialize.Toast.removeAll();
						Materialize.toast('Discount Amount Must be  Must be Less then Total Amount', '3000', 'teal lighten-2');
						return false;
					}*/
				}

				if(parseFloat(amtwithtax)>0){																									
					let discAmount,disc;
					
					//Discount On MRP New code // 11-10-2019
					if($('#discountOnMRPCheck').prop("checked") == true){
						
						discountOnMRP="YES";
						
						if($('#percentageCheck').prop("checked") == true){
							disc=discount;
							discAmount=(amtwithtax*discount)/100;
							
							discountType="PERCENTAGE"
							discountPercentage=parseFloat(disc);
							discountAmount=parseFloat(discAmount);
							
							$("#discountId").parent().find('label').text('Discount Percent');
							$("#discountCalculated").parent().find('label').text('Discount Amount');
							$("#discountCalculated").val(parseFloat(discountAmount).toFixedVSS(2)).change();


						}else{
							discAmount=discount;
							disc=(discAmount/amtwithtax)*100;
							
							discountType="AMOUNT"
							discountPercentage=parseFloat(disc);
							discountAmount=parseFloat(discAmount);
							
							$("#discountId").parent().find('label').text('Discount Amount');
							$("#discountCalculated").parent().find('label').text('Discount Percent');
							$("#discountCalculated").val(parseFloat(discountPercentage).toFixedVSS(2)).change();


						}
					}else{
						// Discount On Unit Price
						discountOnMRP="NO";
						if($('#percentageCheck').prop("checked") == true){
							disc=discount;
							discAmount=(ttl*discount)/100;
							
							discountType="PERCENTAGE"
							discountPercentage=parseFloat(disc);
							discountAmount=parseFloat(discAmount);
							
							$("#discountId").parent().find('label').text('Discount Percent');
							$("#discountCalculated").parent().find('label').text('Discount Amount');
							$("#discountCalculated").val(parseFloat(discountAmount).toFixedVSS(2)).change();


						}else{
							discAmount=discount;
							disc=(discAmount/ttl)*100;
							
							discountType="AMOUNT"
							discountPercentage=parseFloat(disc);
							discountAmount=parseFloat(discAmount);
							
							$("#discountId").parent().find('label').text('Discount Amount');
							$("#discountCalculated").parent().find('label').text('Discount Percent');
							$("#discountCalculated").val(parseFloat(discountPercentage).toFixedVSS(2)).change();


						}
					}
					
					// Old code for discount on MRP begin here
					/*if($('#percentageCheck').prop("checked") == true){
						disc=discount;
						discAmount=(amtwithtax*discount)/100;
						
						discountType="PERCENTAGE"
						discountPercentage=parseFloat(disc);
						discountAmount=parseFloat(discAmount);
						$("#discountId").parent().find('label').text('Discount Percent');
						$("#discountCalculated").parent().find('label').text('Discount Amount');
						$("#discountCalculated").val(parseFloat(discountAmount).toFixedVSS(2)).change();

					}else{
						discAmount=discount;
						disc=(discAmount/amtwithtax)*100;
						
						discountType="AMOUNT"
						discountPercentage=parseFloat(disc);
						discountAmount=parseFloat(discAmount);
						$("#discountId").parent().find('label').text('Discount Amount');
						$("#discountCalculated").parent().find('label').text('Discount Percent');
						$("#discountCalculated").val(parseFloat(discountPercentage).toFixedVSS(2)).change();

					}*/
					// Old code for discount on MRP end here

					amountWithoutTaxBeforeDiscount=parseFloat(ttl);
					amountBeforeDiscount=parseFloat(amtwithtax);

					$('#finalTotalAmount').val(parseFloat(ttl).toFixedVSS(2));
					$('#finalTotalAmountWithTax').val(parseFloat(amtwithtax).toFixedVSS(2));
									
					/*totalAmountBeforeDisc=parseFloat(ttl)-((parseFloat(ttl)*discountPercentage)/100);
					totalAmountWithTaxBeforeDisc=parseFloat(amtwithtax)-discountAmount;*/
					
					totalAmountBeforeDisc=parseFloat(ttl)-((parseFloat(ttl)*discountPercentage)/100); 
					//totalAmountWithTaxBeforeDisc=parseFloat(amtwithtax)-discountAmount;
					totalAmountWithTaxBeforeDisc=parseFloat(amtwithtax)-((parseFloat(amtwithtax)*discountPercentage)/100);

					totalAmount=totalAmountBeforeDisc;
					totalAmountWithTax=totalAmountWithTaxBeforeDisc;

						//parseFloat(ttl) + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 );/*+ ( (parseFloat(ttl)*parseFloat(tempCgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;*/
					
					$('#netfinalTotalAmount').val(parseFloat(totalAmount).toFixedVSS(2));
					$('#netfinalTotalAmountWithTax').val(parseFloat(totalAmountWithTax).toFixedVSS(2));
				}else{
					$('#finalTotalAmount').val(0);
					$('#finalTotalAmountWithTax').val(0);
									
					totalAmountBeforeDisc=0;
					totalAmountWithTaxBeforeDisc=0;

					totalAmount=totalAmountBeforeDisc;
					totalAmountWithTax=totalAmountWithTaxBeforeDisc;

						//parseFloat(ttl) + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 );/*+ ( (parseFloat(ttl)*parseFloat(tempCgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;*/
					
					$("#discountMainId").parent().find('label').text('Discount Amount');
					$("#discountCalculated").parent().find('label').text('Discount Percent');
					$("#discountCalculated").val(parseFloat(0).toFixedVSS(2)).change();
					
					$('#netfinalTotalAmount').val(parseFloat(totalAmount).toFixedVSS(2));
					$('#netfinalTotalAmountWithTax').val(parseFloat(totalAmountWithTax).toFixedVSS(2));
				}
			},
			error: function(xhr, status, error) {
					//var err = eval("(" + xhr.responseText + ")");
					//alert("Error");
				}
		});
	}
}

//refresh manage inventory table data 
function fetchProductList()
{

	var t = $('#tblData').DataTable();
	t.clear().draw();
	$.ajax({
		url : myContextPath+"/fetchProductListForInventoryAjax",
		dataType : "json",
		success : function(data) {
			var srno=1;		
				
			for (var i = 0, len = data.length; i < len; ++i) {
				var inventoryProductsList = data[i];
				
				t.row.add(
								[
										srno,
										inventoryProductsList.product.productName,
										inventoryProductsList.product.productCode,
										inventoryProductsList.product.categories.categoryName,
										inventoryProductsList.product.brand.name,
										inventoryProductsList.product.categories.hsnCode,	
										inventoryProductsList.product.rate.toFixedVSS(2),
										inventoryProductsList.rateWithTax,
										inventoryProductsList.product.currentQuantity,
										inventoryProductsList.product.threshold,
										inventoryProductsList.taxableAmount.toFixedVSS(2),
										+inventoryProductsList.tax.toFixedVSS(2),
										(inventoryProductsList.taxwithAmount.toFixedVSS(2)),
										"<button class='btn-flat tooltipped' data-position='right' data-delay='50' data-tooltip='Add Inventory' onclick='showAddQuantity("+inventoryProductsList.product.productId+")'><i class='material-icons'>add_shopping_cart</i></button>",
										"<button class='btn-flat tooltipped' data-position='right' data-delay='50' data-tooltip='Sinlge Order' onclick='showOrderOneProduct("+inventoryProductsList.product.productId+")'><i class='material-icons'>mail_outline</i></button>"
										
										/*,"<button class=' btn-flat '><i class='material-icons tooltipped blue-text ' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"*/

								]).draw(false); 

				srno = srno + 1;
			}
		}
	});

}
//reset multiple order form 
function resetOrderSelectionData()
{
	$('#supplierMobileNumber').val('');
	$('#supplierRateForOrder').val('');
	$('#supplierMRPRateForOrder').val('');
	$('#orderQuantity').val('');
	$('#OrderMobileNumberchecker').prop('checked', false);;
	$('#supplierMobileNumber').attr('readonly', 'readonly'); 
	$("#addOrderProduct").text("Add");	
	
	var source1 = $("#supplierIdForOrder");
	source1.val(0);
	source1.change();
	
	setTimeout(
			  function() 
			  {
				  var source = $("#productIdForOrder");
					source.val(0);
					source.change();
			  }, 1000);
	
	var source2 = $("#categoryIdForOrder");
	source2.val(0);
	source2.change();
	
	var source3 = $("#brandIdForOrder");
	source3.val(0);
	source3.change();
}
//open multiple product order modal
//set category,brand,supplier list
function showOrderProductQuantity(productId)
{
	$("#orderProductsButton").removeAttr("disabled");
	$('#OrderMobileNumberchecker').prop('checked', false);
	count=1;
	orderSupplierList=[];
	$("#orderCartTb").empty();
	
	resetOrderSelectionData();
	
	/*var selectProduct=false;
	if(parseInt(productId)>0)
	{
		selectProduct=true;
	}*/
	
	$.ajax({
		url : myContextPath+"/fetchBrandAndCategoryList",
		dataType : "json",
		async:false,
		success : function(data) {
			categoryList=data.categoryList;
			$('#categoryIdForOrder').empty();
			$("#categoryIdForOrder").append('<option value="0">Choose Category</option>');
			for(var i=0; i<categoryList.length; i++)
			{
				$("#categoryIdForOrder").append('<option value='+categoryList[i].categoryId+'>'+categoryList[i].categoryName+'</option>');
			}	
			$("#categoryIdForOrder").change();
			/*if(selectProduct==true){
				var source=$('#categoryIdForOrder');
				source.val(v);
				source.change();
			}*/
			
			brandList=data.brandList;
			$('#brandIdForOrder').empty();
			$("#brandIdForOrder").append('<option value="0">Choose Brand</option>');
			for(var j=0; j<brandList.length; j++)
			{
				$("#brandIdForOrder").append('<option value='+brandList[j].brandId+'>'+brandList[j].name+'</option>');
			}	
			$("#brandIdForOrder").change();
			
			
			supplierList=data.supplierList;
			$('#supplierIdForOrder').empty();
			$("#supplierIdForOrder").append('<option value="0">Choose Supplier</option>');
			for(var k=0; k<supplierList.length; k++)
			{
				$("#supplierIdForOrder").append('<option value='+supplierList[k].supplierId+'>'+supplierList[k].name+'</option>');
			}	
			$("#supplierIdForOrder").change();
			
			
		}
	});
	
	/*if(selectProduct==true){
		
		$.ajax({
				url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
				dataType : "json",
				async:false,
				success : function(data){
					
					supplierProductLists=data;
					$('#supplierIdForOrder').empty();
					$("#supplierIdForOrder").append('<option value="0">Choose Supplier</option>');
					for(var i=0; i<supplierProductLists.length; i++)
					{	
						$("#supplierIdForOrder").append('<option value='+supplierProductLists[i].supplier.supplierId+'>'+supplierProductLists[i].supplier.name+'</option>');
						
					}
					
					var source=$('#supplierIdForOrder');
					source.val(0);
					source.change();
					
					if(supplierProductLists.length>0)
					{
						$('#productIdForOrder').empty();
						$("#productIdForOrder").append('<option value='+supplierProductLists[0].product.productId+'>'+supplierProductLists[0].product.productName+'</option>');
					}
					
					$('#productIdForOrder').change();
					//$('#productIdForOrder').attr("disabled", true);

					$('.modal').modal();
					$('#order').modal('open');
				
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Supplier Order : ");
           	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); 
					}
			});	
		
	}
	else
	{*/
		$('.modal').modal();
		$('#order').modal('open');
	//}
	
	//selectProduct==false;
}
//open single product order modal
//set product ,category, brand name and supplier list
function showOrderOneProduct(id)
{
	$('#orderQuantityOneOrderId').val('');
	$('#orderQuantityOneOrderId').change();
	$('#supplierMobileNumberOneOrderId').val('');
	$('#supplierMobileNumberOneOrderId').change();
	$("#orderProductsButtonOneOrderId").removeAttr("disabled");
	$('#OrderMobileNumbercheckerOneOrderId').prop('checked', false);
	$.ajax({
		url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+id,
		dataType : "json",
		async:false,
		success : function(data){
			
			supplierProductLists=data;
			$('#supplierIdOneOrderId').empty();
			$("#supplierIdOneOrderId").append('<option value="0">Choose Supplier</option>');
			for(var i=0; i<supplierProductLists.length; i++)
			{	
				$("#supplierIdOneOrderId").append('<option value='+supplierProductLists[i].supplierId+'>'+supplierProductLists[i].supplierName+'</option>');
				
			}
			
			var source=$('#supplierIdOneOrderId');
			source.val(0);
			source.change();
			
			$('#productIdOneOrderId').val(supplierProductLists[0].productId);
			$("#productNameOneOrderId").val(supplierProductLists[0].productName);
			
			$('#brandIdOneOrderId').val(supplierProductLists[0].brandId);
			$('#brandOneOrderId').val(supplierProductLists[0].brandName);
			
			$('#categoryIdOneOrderId').val(supplierProductLists[0].categoryId);
			$('#categoryOneOrderId').val(supplierProductLists[0].categoryName);
			
			$("#productNameOneOrderId").change();
			$('#brandOneOrderId').change();
			$('#categoryOneOrderId').change();
			
			$('.modal').modal();
			$('#singleOrder').modal('open');
		
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			  Materialize.Toast.removeAll();
			Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
			 
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
			}
	});	
}
//view for edit supplier order -> product-supplier 
function editrow(id) {
	$('#currentUpdateRowId').val(id);
	var supplierId=$('#rowsupplierid_'+id).val();
	var productId=$('#rowproductid_'+id).val();
	//alert($('#rowproductrate_'+id).val());
			
	var source1 = $("#supplierIdForOrder");
	var v1=supplierId;
	source1.val(v1);
	source1.change();
	
	setTimeout(
			  function() 
			  {
				  var source = $("#productIdForOrder");
					var v=productId;
					source.val(v);
					source.change();
			  }, 1000);
	
	
	$('#supplierMobileNumber').val($('#rowsuppliermob_'+id).text());
	$('#orderQuantity').val($('#roworderquantity_'+id).text());
	$('#supplierMRPRateForOrder').val($('#rowsupplierMrp_'+id).val());
	$('#supplierRateForOrder').val($('#rowsupplierrate_'+id).val());
	
	$('#supplierMobileNumber').change();
	$('#orderQuantity').change();
	$('#supplierRateForOrder').change();
	$('#supplierMRPRateForOrder').change();
	
	$("#addOrderProduct").text("Update");	
}
//edit supplier order -> product-supplier 
function updaterow(id) {
	
	$("#addOrderProduct").text("Add");	
	
	var productIdForOrder=$('#productIdForOrder').val();
	var supplierIdForOrder=$('#supplierIdForOrder').val();
	var orderQuantity=$('#orderQuantity').val();
	var supplierMobileNumber=$('#supplierMobileNumber').val();
	
	/*for (let [key, value] of orderSupplierList.entries()) {
			//alert("supplierId : "+key+" - ProductId : "+value[0]+" - quantity : "+value[1]+" - mobile Number : "+value[2]);
			if(key===supplierIdForOrder && value[0] === productIdForOrder)
			{
				$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Product Select Warning");
   	     	$('#msg').text("This Product and Supplier is already added to Cart"); 
   	     	return false;
			}
		}*/
	
	//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
	/*for (let [key, value] of orderSupplierList.entries()) {
		alert(value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]);
	}*/
	//alert(id);
	
	var supplierIdTemp=$('#rowsupplierid_' + id).val();
	var productIdTemp=$('#rowproductid_' + id).val();
	//skip current updating row
	for (var i=0; i<orderSupplierList.length; i++) {
		var value=orderSupplierList[i];
		if(value[0]!==supplierIdTemp || value[1]!==productIdTemp)
		{
			orderSupplierListTemp.push(orderSupplierList[i]);
		}
	}
	orderSupplierList=[]
	for (var i=0; i<orderSupplierListTemp.length; i++) {
		orderSupplierList.push(orderSupplierListTemp[i]);
	}
	orderSupplierListTemp=[];
	
	//add updating row details
	var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
	orderSupplierList.push(prodctlst);
	
	var supplierName=$('#supplierIdForOrder option:selected').text();
	var productName=$('#productIdForOrder option:selected').text();
	var supplierRate=$('#supplierRateForOrder').val();
	var supplierMrpRate=$('#supplierMRPRateForOrder').val();
	
	var rowCount = $('#orderCartTb tr').length;
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!=i)
		{
			//alert("predata");
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
       				"</tr>"; */
			
					
			
       				trData=trData+"<tr id='rowdel_" + count + "' >"+
                            "<td id='rowcount_" + count + "'>"+count+"</td>"+
                            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
                            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
                            "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+$('#rowsupplierrate_' + i).val()+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+$('#rowsupplierMrp_' + i).val()+">"+$('#rowsupplierMrp_' + i).val()+"</td>"+
                            "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
                            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
                            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
                        	"</tr>";
    		 count++;
		}
		else
		{
			trData=trData+"<tr id='rowdel_" + count + "' >"+
				            "<td id='rowcount_" + count + "'>"+count+"</td>"+
				            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
				            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
				            "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+supplierRate+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+supplierMrpRate+">"+supplierMrpRate+"</td>"+
				            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
				            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
				            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
				        	"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#orderCartTb").html('');
	$("#orderCartTb").html(trData);
	
	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);

   //reset supplier order form
	resetOrderSelectionData();
}
//delete supplier-product from supplier order
function deleterow(id) {
	$("#addOrderProduct").text("Add");
	var supplierIdForOrder=$('#supplierIdForOrder').val();

	var supplierIdTemp=$('#rowsupplierid_' + id).val();
	var productIdTemp=$('#rowproductid_' + id).val();
	
	//skit deleting row
	//it will assumed as deleted from orderSupplierList[]
	for (var i=0; i<orderSupplierList.length; i++) {
		var value=orderSupplierList[i];
		if(value[0]!==supplierIdTemp || value[1]!==productIdTemp)
		{
			orderSupplierListTemp.push(orderSupplierList[i]);
		}
	}
	orderSupplierList=[]
	for (var i=0; i<orderSupplierListTemp.length; i++) {
		orderSupplierList.push(orderSupplierListTemp[i]);
	}
	orderSupplierListTemp=[];
	
    var rowCount = $('#orderCartTb tr').length;
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!==i)
		{			
       				trData=trData+"<tr id='rowdel_" + count + "' >"+
				                    "<td id='rowcount_" + count + "'>"+count+"</td>"+
				                    "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
				                    "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
				                    "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+$('#rowsupplierrate_' + i).val()+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+$('#rowsupplierMrp_' + i).val()+">"+$('#rowsupplierMrp_' + i).val()+"</td>"+
				                    "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
				                    "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
				                    "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
				                	"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#orderCartTb").html('');
	$("#orderCartTb").html(trData);
	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);

}
//supplier order  for multiple product submission
//minimum 1 product required validation
function supplierOrder(){
	//alert(orderSupplierList.entries());
	
	if(orderSupplierList.length==0){
		Materialize.Toast.removeAll();
		Materialize.toast('Add Minimum 1 product for Order', '4000', 'teal lighten-3');
		return  false;
	}
	

	var productIdList="";
	for (var i=0; i<orderSupplierList.length; i++) {
		var value=orderSupplierList[i];
		productIdList=productIdList+value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]+",";
		}
	productIdList=productIdList.slice(0,-1)
	//alert(productIdList);
	$('#productWithSupplikerlist').val(productIdList);
	
	$("#orderProductsButton").attr('disabled','disabled');
	
	var form = $('#orderBookForm');
	Materialize.Toast.removeAll();
	$.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : $("#orderBookForm").serialize(),
				success : function(data) 
				{
					if(data=="Success")
					{
							$('#order').modal('close');
							$('#addeditmsg').find("#modalType").addClass("success");
							 $('#addeditmsg').find(".modal-action").removeClass("teal lighten-2 teal").addClass("teal lighten-2");
							$('#addeditmsg').modal('open');
							//$('#msgHead').text("Success : ");
							$('#msg').text("Order Booked SuccessFully");
							orderSupplierList=[];
							//fetchProductList();
					}
					else
					{
						$('#addeditmsg').find("#modalType").addClass("warning");
						 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
						$('#addeditmsg').modal('open');
						//$('#msgHead').text("Failed : ");
						$('#msg').text("Order Booked Failed");
						
						$("#orderProductsButton").removeAttr("disabled");
					}
				}
	});
}
//order submit confirmation
function orderConfirmationSupplierOrder(){
	/* $('.modal').modal();
	$('#delete'+id).modal('open'); */
	Materialize.Toast.removeAll();
	var $toastContent = $('<span>Do you want to Order?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="supplierOrder()">Yes</button>  '));
	  Materialize.toast($toastContent, "abc");
}