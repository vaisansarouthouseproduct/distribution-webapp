var productListQtyMng=[];
var orderProductDetails;
var orderDetails;
var radioButton="NonFree";
var orderStatus;
var orderProductDetailsForIssueCheck=[];
var quantityZeroValidation=false;
$(document).ready(function() 
{
	//editing order status
	orderStatus=$('#orderStatusId').val();
	
	$('#updateId').click(function(){
		//atlist one product details need in order	
		if(orderProductDetails.length==0){
			Materialize.Toast.removeAll();
			Materialize.toast('Add some products', '3000', 'teal lighten-2');
			return false;
		}

		/*Sachin
		 *New validaion issue fixes begin if issueQuantity is zero then only toast is coming (if u first make the product qty zero then delete that product then quantityZeroValidation error is there )*/
		if(orderStatus==="Booked"){
			for(var k=0;k<orderProductDetails.length;k++){
				if(orderProductDetails[k].purchaseQuantity==0){
					quantityZeroValidation=true;
				}else{
					quantityZeroValidation=false;
				}
			}
		}
		if(orderStatus==="Packed" || orderStatus==="Issued"){
			for(var k=0;k<orderProductDetails.length;k++){
				if(orderProductDetails[k].issuedQuantity==0){
					quantityZeroValidation=true;
				}else{
					quantityZeroValidation=false;
				}
			}
		}
		/*validaion issue fixes end */
		
		
		//check zero quantity validation on order product
		if(quantityZeroValidation)
		{
			Materialize.Toast.removeAll();
			Materialize.toast('Quantity make as Non zero or delete this product', '3000', 'teal lighten-2');
			return false;
		}
		//atlist one non free product required in order products 
		var isNonFreeFound=false;
		for(var j=0; j<orderProductDetails.length; j++)
		{			
			var orderProduct=orderProductDetails[j];
			if(orderProduct.type=='NonFree'){
				isNonFreeFound=true;
			}
		}
		if(isNonFreeFound==false){
			Materialize.Toast.removeAll();
			Materialize.toast('Minimum One Non-Free Product Required For Order', '3000', 'teal lighten-2');
			return false;
		}

	if(orderStatus==="Packed"){
		/**cheking packed qty with invt */

		var productListWithQty=new HashMap();
		for(var j=0; j<orderProductDetails.length; j++){
			var orderProduct=orderProductDetails[j];
			var oldOrderProduct=getOldOrderProduct(orderProduct.product.product.productId,orderProduct.type);

			if(productListWithQty.containsKey(orderProduct.product.product.productId)){				
				var entry=productListWithQty.get(orderProduct.product.product.productId);
				var qty;
				if(oldOrderProduct==null){
					qty=parseInt(entry.value)+parseInt(orderProduct.issuedQuantity);
					
					if(qty>0){
						productListWithQty.remove(orderProduct.product.product.productId);
						var value=[parseInt(qty),orderProduct.type,0];
						productListWithQty.put(orderProduct.product.product.productId,value);
					}
				}else{
					qty=parseInt(entry.value)+parseInt(orderProduct.issuedQuantity-oldOrderProduct.issuedQuantity);
					
					if(qty>0){
						productListWithQty.remove(orderProduct.product.product.productId);
						var value=[parseInt(qty),orderProduct.type,oldOrderProduct.issuedQuantity];
						productListWithQty.put(orderProduct.product.product.productId,value);
					}
				}
				
			}else{				
				var qty;
				if(oldOrderProduct==null){
					qty=parseInt(orderProduct.issuedQuantity);
					
					if(qty>0){
						var value=[parseInt(qty),orderProduct.type,0];
						productListWithQty.put(orderProduct.product.product.productId,value);
					}
				}else{
					qty=parseInt(orderProduct.issuedQuantity-oldOrderProduct.issuedQuantity);
					
					if(qty>0){
						var value=[parseInt(qty),orderProduct.type,oldOrderProduct.issuedQuantity];
						productListWithQty.put(orderProduct.product.product.productId,value);
					}
				}
				
			}
		}

		var entrySet=productListWithQty.getAll()
		
		for(var i=0; i<entrySet.length; i++){
			var key=entrySet[i].key;
			var val=entrySet[i].value[0];
			var product=fetchProductByProductId(key);

			if(product.currentQuantity<val){
				Materialize.Toast.removeAll();
				Materialize.toast(product.productName+ '('+entrySet[i].value[1]+') qty exceeds Current Qty. Max Available :  '+ (parseInt(entrySet[i].value[2])+parseInt(product.currentQuantity)), '3000', 'teal lighten-2');
				console.log(product.productName+" qty exceeds Current Qty");
				return false;
			}

		} 
		/* cheking end */
	}
		/*[productId,purchaseQuantity,issuedQuantity,rate,sellingRate,igst,purchaseAmount,issuedAmount,type]
		 * orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
		 * */
		//make update order details to string
		var updateOrderProductList="";
		for(var j=0; j<orderProductDetails.length; j++)
		{			
			var orderProduct=orderProductDetails[j];

			var editMode="Non Edit";
			if(orderStatus==="Packed")
			{
				//find is Edited or not for update same purchase details
				for(var k=0; k<orderProductDetailsForIssueCheck.length; k++)
				{
					var orderProductOld=orderProductDetailsForIssueCheck[k];
					if(orderProductOld.product.productId==orderProduct.product.product.productId 
						&& orderProductOld.issuedQuantity!=orderProduct.issuedQuantity)
					{
						editMode="Edit";
					}
				}
				if(orderProduct.purchaseQuantity==0)
				{
					editMode="Edit";
				}
				updateOrderProductList =updateOrderProductList + 
										orderProduct.product.product.productId+","+
										orderProduct.purchaseQuantity+","+
										orderProduct.issuedQuantity+","+
										orderProduct.rate+","+
										orderProduct.sellingRate+","+
										orderProduct.purchaseAmount+","+
										orderProduct.issueAmount+","+
										orderProduct.type+","+
										orderProduct.igst+","+
										editMode+"-";
			}
			else
			{
				updateOrderProductList =updateOrderProductList + 
										orderProduct.product.product.productId+","+
										orderProduct.purchaseQuantity+","+
										orderProduct.issuedQuantity+","+
										orderProduct.rate+","+
										orderProduct.sellingRate+","+
										orderProduct.purchaseAmount+","+
										orderProduct.issueAmount+","+
										orderProduct.type+","+
										orderProduct.igst+"-";
			}
		}
		updateOrderProductList=updateOrderProductList.slice(0,-1);
		$('#updateOrderProductListId').val(updateOrderProductList);		
	});

	//get old product if have in orderProductDetailsForIssueCheck else null
	function getOldOrderProduct(productId,type){
		for(var k=0; k<orderProductDetailsForIssueCheck.length; k++){
			var orderProductOld=orderProductDetailsForIssueCheck[k];
			if(orderProductOld.product.product.productId==productId 
				&& orderProductOld.type==type) 
			{
				return orderProductOld;
			}
		}
		return null;
	}
	//filter product list by brandId and categoryId
	$('#brandid,#categoryid').change(function(){
		var brandId=$('#brandid').val();
		var categoryId=$('#categoryid').val();		
		filterProductList(brandId,categoryId);
	});
	/**
	 * if order status packed then 
	 * product change event
	 * qty change event
	 * check quantity available method
	 * productList Qty available manage  
	 */
	if(orderStatus==="Packed"){

		//on product change get its available quantity
		$('#productid').change( function(){
			var productId=$('#productid').val();
			if(productId==0){
				return false;
			}
			//var product= fetchProductByProductId(productId);
			var qtyAvail=qtyAvailable(productId);

			$('#curr_qty').val(qtyAvail);
			$('#curr_qty').change();
		});
		//check entered quantity with available quantity
		$('#qty').keyup(function(){

			var productId=$('#productid').val();
			var qty=$('#qty').val();
			var qtyAvail=qtyAvailable(productId);

			if(qtyAvail<qty){
				Materialize.Toast.removeAll();
				Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(qtyAvail), '3000', 'teal lighten-2');
				$('#qty').val(0);
			}

		});
		 //find available quantity using productListQtyMng
		 function qtyAvailable(productId){
			var product=fetchProductByProductId(productId);
			
			var qtyAvail;
			var isFoundIn_ProductListQtyMng=false;
			for(var i=0; i<productListQtyMng.length; i++){
				if(productListQtyMng[i].productId==productId){
					productQtyMng=productListQtyMng[i];	
					qtyAvail=parseInt(productQtyMng.qtyExtraAvail)+parseInt(product.currentQuantity);
					isFoundIn_ProductListQtyMng=true;			
					break;
				}		
			}
			if(isFoundIn_ProductListQtyMng==false){
				try{
					qtyAvail=parseInt(product.currentQuantity);
				}catch(err){
					console.log(err);
					console.log("Product Id :"+productId);
				}
				
			} 
			if(qtyAvail<0){
				return 0;
			}
			return qtyAvail;
		}


		//function getAllowedQuantity(product){
			
			//return qtyAvailable(product.productId);

			/* var allowedQty=0;
			var isOld=false;
			var orderProductOld;
			var oldQty=0;
			for(var j=0; j<orderProductDetailsForIssueCheck.length; j++){
				var orderProduct=orderProductDetailsForIssueCheck[j];
				if(parseInt(orderProduct.product.product.productId)===parseInt(product.productId)){ 
					orderProductOld=orderProductDetailsForIssueCheck[j];
					isOld=true;
					oldQty+=parseInt(orderProduct.issuedQuantity);
					if(isProductDeleted(product.productId,orderProduct.type)){
						allowedQty+=parseInt(orderProduct.issuedQuantity);	
					}
				}
			}
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==product.productId){
					if(isOld){
						if(orderProductOld.issuedQuantity!=orderProduct.issuedQuantity){
							allowedQty+=oldQty;
							if(orderProductOld.issuedQuantity>orderProduct.issuedQuantity){
								allowedQty-=parseInt(orderProductOld.issuedQuantity-orderProduct.issuedQuantity);
							}else{
								allowedQty+=parseInt(orderProduct.issuedQuantity-orderProductOld.issuedQuantity);
							}
						}
					}else{
						allowedQty-=parseInt(orderProduct.issuedQuantity);
					}
				}
			}	
			if(product.productId==36){
				var a="sdds";
			}	
			
			allowedQty+=product.currentQuantity;
			return allowedQty; */
		//}

		/* function isProductDeleted(productId,type){
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==productId && orderProduct.type==type){
					return false;
				}
			}
			return true;
		}
		function isProductAdded(productId,type){
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==productId && orderProduct.type==type){
					return true;
				}
			}
			return false;
		} */
		 // product change from anywhere selected product quantity refresh
		function refreshAllowedQuantityOfProducts(productId){	
			//var product=fetchProductByProductId(productId);		
			var qtyAvail=qtyAvailable(productId);
			$('.current_qty_'+productId).text(qtyAvail);
			$('#productid').change();
		}  

		 //manage quantity extra taken or less do 
		 function productListQtyMngFunc(id,qty,isAdd){
			var isProductQtyMngFound=false;
			
			//find product id related data and remove from productListQtyMng[]
			for(var i=0; i<productListQtyMng.length; i++){
				if(productListQtyMng[i].productId==id){
					isProductQtyMngFound=true;
					var productListQtyMngTemp=productListQtyMng[i];
					productListQtyMng.splice(i, 1);
					break;
				}
			}

			var oldQuantity=0;
			//find old quantity which already cut from current Quantity (Free,Non-Free Both)
			for(var k=0; k<orderProductDetailsForIssueCheck.length; k++){
				var orderProductOld=orderProductDetailsForIssueCheck[k];
				if(orderProductOld.product.product.productId==id) 
				{
					oldQuantity+=parseInt(orderProductOld.issuedQuantity);
				}
			}

			var updatedQuantity=0;			
			//find updated order quantity (Free,Non-Free Both)
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProductUpdated=orderProductDetails[j];	
				if(orderProductUpdated.product.product.productId==id) 
				{			
					updatedQuantity+=parseInt(orderProductUpdated.issuedQuantity);
				}
			}
			//old quantity reduce from updated quantity
			var qtyExtra=oldQuantity-updatedQuantity;

			//store current extra taken quantity 
			var productListQtyMngTemp={
						productId:	id,
						qtyExtraAvail:qtyExtra
					};
			productListQtyMng.push(productListQtyMngTemp);
			
			// product change from anywhere selected product quantity refresh
			refreshAllowedQuantityOfProducts(id);
		}
	} 
	//here privious orderProductDetails store in array and make event for delete,edit qty and mrp
	//delete , qty change, mrp change event make run time
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchOrderProductDetailsEditOrder?orderId="+orderId,
		dataType : "json",
		success:function(data)
		{
			orderProductDetails=data;
			orderDetails=orderProductDetails[0].orderDetails;
			var orderProductDetailsTemp=[];
			for(var j=0; j<orderProductDetails.length; j++)
			{
				var orderProduct=orderProductDetails[j];
				
				//its function defined in calculateProperAmount.js
				//calculate unit price,cgst ,igst ,sgst by mrp and igst percentage
				var calculateProperTaxObj=calculateProperTax(orderProduct.sellingRate,orderProduct.product.product.categories.igst);
				
				//changing according changes in order product details
				/*if(orderProduct.issuedQuantity!=0){
					
				}*/
					orderProductDetailsTemp.push
					({
						"purchaseQuantity" :orderProduct.purchaseQuantity,
						"issuedQuantity":orderProduct.issuedQuantity,
						"rate":calculateProperTaxObj.unitPrice,
						"sellingRate":orderProduct.sellingRate,
						"igst":orderProduct.product.product.categories.igst,
						"purchaseAmount":orderProduct.purchaseAmount,
						"issueAmount":orderProduct.issueAmount,
						//"orderDetails":orderProduct.orderDetails,
						"product":{"product":orderProduct.product.product},
						"type":orderProduct.type
					});
				
			
				//create for old status check of order product
				/*if(orderProduct.issuedQuantity!=0){
					
				}*/
					
					orderProductDetailsForIssueCheck.push
					({
						"purchaseQuantity" :orderProduct.purchaseQuantity,
						"issuedQuantity":orderProduct.issuedQuantity,
						"rate":calculateProperTaxObj.unitPrice,
						"sellingRate":orderProduct.sellingRate,
						"igst":orderProduct.product.product.categories.igst,
						"purchaseAmount":orderProduct.purchaseAmount,
						"issueAmount":orderProduct.issueAmount,
						//"orderDetails":orderProduct.orderDetails,
						"product":{"product":orderProduct.product.product},
						"type":orderProduct.type
					});
				
				
				var product=orderProduct.product.product;
			
				if(orderProduct.type==="Free")
				{
					$('#delPrdFree_'+product.productId).click(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];
						var igst;
						var deletingQty=0;

						//delete row and rearrange sr.no.
						var row=$(this).parents("tr");				
						var siblings = row.siblings();
					    row.remove();
					    siblings.each(function(index) {
					         $(this).children().first().text(index+1);
					    });
						
						//skipping current deleting order product from orderProductDetailsTemp
						//and take only its deleting order quantity and igst percentage
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}		
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}	
							else
							{
								deletingQty=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}			
						}
						orderProductDetails=orderProductDetailsTemp;
						//if order packed then update product available quantity in productListQtyMng
						if(orderStatus==="Packed"){
							//qty return add in inventory
							productListQtyMngFunc(productId);
							$('#productid').change();
						}
						//find totalQuantity and totalAmount
						findTotal();
					});
				}
				else
				{
					$('#delPrdNonFree_'+product.productId).click(function(){
						var id=$(this).attr('id');
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];
						var igst;
						var deletingQty=0;

						//delete row and rearrange sr.no.
						var row=$(this).parents("tr");				
						var siblings = row.siblings();
					    row.remove();
					    siblings.each(function(index) {
					         $(this).children().first().text(index+1);
					    });
						   
						//skipping current deleting order product from orderProductDetailsTemp
						//and take only its deleting order quantity and igst percentage
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}
							else
							{
								deletingQty=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}							
						}
						orderProductDetails=orderProductDetailsTemp;	
						//if order packed then update product available quantity in productListQtyMng
						if(orderStatus==="Packed"){
							//qty return add in inventory
							productListQtyMngFunc(productId);						
							$('#productid').change();	
						}		
						//find totalQuantity and totalAmount
						findTotal();
					});
				}
				if(orderProduct.type==="Free")
				{
					/**
					 * quantity only allowed number without decimal
					 */
					$('#qtyFree_'+product.productId).keypress(function(evt) {
					    evt = (evt) ? evt : window.event;
					    var charCode = (evt.which) ? evt.which : evt.keyCode;
					    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					        return false;
					    }
					    return true;
					});

					$('#qtyFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;
						var igst;
						var previousissuedQuantity;

						//skipping current change order product quantity from orderProductDetailsTemp
						//and take only its previousissuedQuantity order quantity and igst percentage
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								previousissuedQuantity=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}			
						}
						/**
						 * take old order product details
						 */
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="NonFree")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$('#qtyFree_'+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}

						//order status packed then 
						//available quantity with previousissuedQuantity compare with changed quantity
						if(orderStatus==="Packed"){
							// /parseInt(previousissuedQuantity)+
							var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

							//if change quantity exceeds allowed quantity then not allowed 
							//change entered quantity to old issuedQuantity
							if(allowedQty<qty){
								Materialize.Toast.removeAll();
								Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);

								//edit inventory 								
								productListQtyMngFunc(product.productId);

								//find totalQuantity and totalAmount
								findTotal();

								return false;
							}
						}
						//if order status Issued then changed quantity not exceeds issuedQuantity
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);

								//find totalQuantity and totalAmount
								findTotal();
								return false;
							}
						}
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"Free"
							};
							$('#purchaseAmtFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"Free"
							};
							$('#issuedAmtFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;

						if(orderStatus==="Packed"){
							//edit inventory 
							productListQtyMngFunc(product.productId);
						}
						//find totalQuantity and totalAmount
						findTotal();
					});
				}
				else
				{
					/**
					 * quantity only allowed number without decimal
					 */
					$('#qtyNonFree_'+product.productId).keypress(function(evt) {
					    evt = (evt) ? evt : window.event;
					    var charCode = (evt.which) ? evt.which : evt.keyCode;
					    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					        return false;
					    }
					    return true;
					});
					$('#qtyNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp,igst;
						var previousissuedQuantity;
						var srno=1;
						//skipping current change order product quantity from orderProductDetailsTemp
						//and take only its previousissuedQuantity order quantity and igst percentage
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else {
								previousissuedQuantity=orderProduct.issuedQuantity;
								mrp=orderProduct.sellingRate;
								igst=orderProduct.product.product.categories.igst;
							}
						}
						/**
						 * take old order product details
						 */
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						
						var qty=$('#qtyNonFree_'+product.productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						//order status packed then 
						//available quantity with previousissuedQuantity compare with changed quantity
						if(orderStatus==="Packed"){
							var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId); //parseInt(previousissuedQuantity)+

							//if change quantity exceeds allowed quantity then not allowed 
							//change entered quantity to old issuedQuantity
							if(allowedQty<qty){
								Materialize.Toast.removeAll();
								Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);

								 //edit inventory 								
								productListQtyMngFunc(product.productId);
								
								//find totalQuantity and totalAmount
								findTotal();

								return false;
							}
						}
						//if order status Issued then changed quantity not exceeds issuedQuantity
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixedVSS(2));
								//find totalQuantity and totalAmount
								findTotal();
								return false;
							}
						}
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						 if(orderStatus==="Packed"){
							//edit inventory 
							productListQtyMngFunc(product.productId);
						} 
						//find totalQuantity and totalAmount
						findTotal();
					});
				}
				if(orderProduct.type==="Free")
				{
					/*$('#mrpFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });*/
					/**
					 * mrp only allowed nunmber withtout decimal
					 */
					 $('#mrpFree_'+product.productId).keypress(function(evt) {
					    evt = (evt) ? evt : window.event;
					    var charCode = (evt.which) ? evt.which : evt.keyCode;
					    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					        return false;
					    }
					    return true;
					});
					$('#mrpFree_'+product.productId).keyup(function(){

						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;	
						var igst;
						var srno=1;
						//skipping current change order product mrp from orderProductDetailsTemp
						//and take only its and igst percentage
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						/**
						 * take old order product details
						 */
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/**
						 * if order status issued then changed quantity must be equal or less than issued quantity
						 */
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixedVSS(2));
								//find totalQuantity and totalAmount
								findTotal();
								return false;
							}
						}
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						//find totalQuantity and totalAmount
						findTotal();

					});
				}else{
					/*$('#mrpNonFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });*/
					/**
					 * only number allowed without decimal
					 */
					 $('#mrpNonFree_'+product.productId).keypress(function(evt) {
					    evt = (evt) ? evt : window.event;
					    var charCode = (evt.which) ? evt.which : evt.keyCode;
					    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					        return false;
					    }
					    return true;
					});
					$('#mrpNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp=$('#mrpNonFree_'+productId).val();
						var igst;
						if(mrp=='' || mrp==undefined)
						{
							mrp="0";
						}

						var srno=1;
						//skipping current change order product mrp from orderProductDetailsTemp
						//and take only its igst percentage
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}else{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						/**
						 * get old order product details 
						 */
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyNonFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/**
						 * if order status is issued then change quantity must be equal or less
						 */
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpNonFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixedVSS(2));
								
								//find total amount and total quantity
								findTotal();
								return false;
							}
						}
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);						
						
						//its function defined in calculateProperAmount.js 
						var calculateProperTaxObj=calculateProperTax(mrp,igst);
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						//find total amount and quantity
						findTotal();
					});
				}
			}
			
			orderProductDetails=orderProductDetailsTemp;
			//orderProductDetailsForIssueCheck=orderProductDetailsTemp;
		},
		error: function(xhr, status, error) {
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong', '3000', 'teal lighten-2');
		}
	});
	
/* 	
	//on ready productList Data fetch
	productListData();
	filterProductList("0","0");
	//fetch All ProductList and store array every 5 second
	window.setInterval(productListData, 5000); */
	
	$('#freeId').click(function(){
		radioButton="Free";
	});
	$('#nonFreeId').click(function(){
		radioButton="NonFree";
	});
	
	$('#orderProductAddId').click( function(){
		//get rows of product cart for next serial number to new adding row
		var length = document.getElementById('productcart').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
		length++;
		
		var productId=$('#productid').val();
		var qty=$('#qty').val();
		if(qty=='' || qty==undefined)
		{
			qty=0;
		}
		if(productId==="0")
		{
			Materialize.Toast.removeAll();
			Materialize.toast('Select Product', '3000', 'teal lighten-2');
   	     	return false;
		}
		else if(qty==0)
		{
			Materialize.Toast.removeAll();
			Materialize.toast('Enter Quantity', '3000', 'teal lighten-2');
   	     	return false;
		}
		//chack product already added or not
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			if(orderProduct.product.product.productId==productId && orderProduct.type===radioButton)
			{ 
				Materialize.Toast.removeAll();
				Materialize.toast('Product Already added', '3000', 'teal lighten-2');
       	     	return false;
			}
		}
		//if old product found then use old mrp and igst
		var mrp;
		var product;
		var isOldProduct=false;
		var igst;
		for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
		{
			var orderProduct=orderProductDetailsForIssueCheck[j];
			if(orderProduct.product.product.productId===parseInt(productId) && orderProduct.type===radioButton)
			{ 
				mrp=orderProduct.sellingRate;
				product=orderProduct.product.product;				
				product.productId=orderProduct.product.product.productId;
				igst=orderProduct.product.product.categories.igst;
				isOldProduct=true;
			}
		}
		//if its not found old product then use product mrp and igst
		if(isOldProduct==false)
		{
			var product= fetchProductByProductId(productId);	

			igst=product.categories.igst;
			mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
		}		
		
		

		var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
		var issuedAmt=parseFloat(mrp)*parseFloat(qty);
		
		if(orderStatus==="Booked")
		{
			if(radioButton==="NonFree")
			{
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'</td>'+
		        		'<td><input style="text-align:center;" class="qty" type="text" id="qtyNonFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td><input style="text-align:center;" class="qty" type="text" id="mrpNonFree_'+product.productId+'" value="'+parseFloat(mrp)+'"></td>'+
		        		'<td><span id="purchaseAmtNonFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdNonFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			else
			{
				mrp=0;
				purchaseAmt=0;
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'<font color="green">-(Free)</font></td>'+
		        		'<td><input style="text-align:center;" class="qty" type="text" id="qtyFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+parseFloat(mrp)+'</td>'+
		        		'<td><span id="purchaseAmtFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			
			orderProduct=
			{
				"purchaseQuantity" :qty,
				"issuedQuantity":0,
				"rate":product.rate,
				"sellingRate":mrp,
				"igst":igst,
				"purchaseAmount":purchaseAmt.toFixedVSS(2),
				"issueAmount":0,
				//"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":radioButton
			};

			orderProductDetails.push(orderProduct);
		}
		else
		{
			
			/*Sachin Sharma
			 * free qty totalManagement issue fixing begin*/
			/* free qty totalManagement issue fixing end*/
			if(radioButton==="NonFree"){
				orderProduct=
				{
					"purchaseQuantity" :0,
					"issuedQuantity":qty,
					"rate":product.rate,
					"sellingRate":mrp,
					"igst":igst,
					"purchaseAmount":0,
					"issueAmount":issuedAmt.toFixedVSS(2),
					//"orderDetails":orderProductDetails[0].orderDetails,
					"product":{"product":product},
					"type":radioButton
				};
			}else{
				orderProduct=
				{
					"purchaseQuantity" :0,
					"issuedQuantity":qty,
					"rate":0,
					"sellingRate":0,
					"igst":igst,
					"purchaseAmount":0,
					"issueAmount":0,
					//"orderDetails":orderProductDetails[0].orderDetails,
					"product":{"product":product},
					"type":radioButton
				};
			}
			
			//old code
			/*orderProduct=
			{
				"purchaseQuantity" :0,
				"issuedQuantity":qty,
				"rate":product.rate,
				"sellingRate":mrp,
				"igst":igst,
				"purchaseAmount":0,
				"issueAmount":issuedAmt.toFixedVSS(2),
				//"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":radioButton
			};*/

			orderProductDetails.push(orderProduct);

			//update qty from temp inventory
			productListQtyMngFunc(product.productId);

			if(radioButton==="NonFree")
			{
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
						'<td>'+product.productName+'</td>'+
						'<td><span class="current_qty_'+product.productId+'">'+qtyAvailable(product.productId)+'</span></td>'+
		        		'<td><input style="text-align:center;" class="qty" type="text" id="qtyNonFree_'+product.productId+'" value="'+qty+'"></td>'+
						'<td><input style="text-align:center;" class="qty" type="text" id="mrpNonFree_'+product.productId+'" value="'+(parseFloat(mrp)).toFixedVSS(0)+'"></td>'+
		        		'<td><span id="issuedAmtNonFree_'+product.productId+'">'+issuedAmt+'</span></td>'+
		        		'<td><button id="delPrdNonFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			else
			{
				mrp=0;
				issuedAmt=0;
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
						'<td>'+product.productName+'<font color="green">-(Free)</font></td>'+
						'<td><span class="current_qty_'+product.productId+'">'+qtyAvailable(product.productId)+'</span></td>'+
		        		'<td><input style="text-align:center;" class="qty" type="text" id="qtyFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+(parseFloat(mrp)).toFixedVSS(0)+'</td>'+
		        		'<td><span id="issuedAmtFree_'+product.productId+'">'+issuedAmt+'</span></td>'+
		        		'<td><button id="delPrdFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}			
		}
		//find  total amount and quantity
		findTotal();
		
		if(orderProduct.type==="Free")
		{
			//if previously exist same event then remove them
			$('#delPrdFree_'+product.productId).off("click");


			$('#delPrdFree_'+product.productId).click(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var addingQty;
				//delete row and rearrange sr.no.
				var row=$(this).parents("tr");				
				var siblings = row.siblings();
			    row.remove();
			    siblings.each(function(index) {
			         $(this).children().first().text(index+1);
			    });
				
				var srno=1;
				//skipping current deleting order product from orderProductDetailsTemp
				//and take only its deleting order quantity and igst percentage
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}		
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}else{
						addingQty=orderProduct.issuedQuantity;
					}				
				}
				orderProductDetails=orderProductDetailsTemp;
				
				

				 if(orderStatus=="Packed"){
					//qty return add in inventory
					productListQtyMngFunc(productId);
					$('#productid').change();
				} 
				findTotal();
			});
		}
		else
		{
			//if previously exist same event then remove them
			$('#delPrdNonFree_'+product.productId).off("click");
			$('#delPrdNonFree_'+product.productId).click(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var addingQty;
				//delete row and rearrange sr.no.
				var row=$(this).parents("tr");				
				var siblings = row.siblings();
			    row.remove();
			    siblings.each(function(index) {
			         $(this).children().first().text(index+1);
			    });
				
				var srno=1;
				//skipping current deleting order product from orderProductDetailsTemp
				//and take only its deleting order quantity and igst percentage
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}else{
						addingQty=orderProduct.issuedQuantity;
					}				
				}
				orderProductDetails=orderProductDetailsTemp;
				//if order packed then update product available quantity in productListQtyMng
				if(orderStatus=="Packed"){
					//qty return add in inventory
					productListQtyMngFunc(productId);
					$('#productid').change();
				}
				//find totalQuantity and totalAmount
				findTotal();
			});
		}
		if(orderProduct.type==="Free")
		{
			/**
			 * quantity only allowed number without decimal
			 */
			$('#qtyFree_'+product.productId).keypress(function(evt) {
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
			});
			//if previously exist same event then remove them
			$('#qtyFree_'+product.productId).off("keyup");
			$('#qtyFree_'+product.productId).keyup(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];				
				var previousissuedQuantity;
				var product=fetchProductByProductId(productId);
				var igst=product.categories.igst;
				var srno=1;
				//skipping current change order product quantity from orderProductDetailsTemp
				//and take only its previousissuedQuantity order quantity and igst percentage , order product details
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}else{
						orderProductEdit={
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						}
						product=orderProduct.product.product;
						igst=orderProduct.product.product.categories.igst;
						previousissuedQuantity=orderProduct.issuedQuantity;
					}	

				}
				
				var qty=$(this).val();
				if(qty=='' || qty==undefined)
				{
					qty="0";
				}
				if(qty==="0")
				{
					quantityZeroValidation=true;
				}
				else
				{
					quantityZeroValidation=false;
				}
				//order status packed then 
				//available quantity with previousissuedQuantity compare with changed quantity
				if(orderStatus==="Packed"){
					//parseInt(previousissuedQuantity)+
					var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

					//if change quantity exceeds allowed quantity then not allowed 
					//change entered quantity to old issuedQuantity
					if(allowedQty<qty){
						Materialize.Toast.removeAll();
						Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

						orderProductDetailsTemp.push(orderProductEdit);
						orderProductDetails=orderProductDetailsTemp;
						$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);

						 //edit inventory 								
						productListQtyMngFunc(product.productId);

						//find totalQuantity and totalAmount
						findTotal();

						return false;
					}
				}
				var mrp=0;
				var purchaseAmt=0;
				var issuedAmt=0;
				if(orderStatus==="Booked")
				{
					orderProduct=
					{
						"purchaseQuantity" :qty,
						"issuedQuantity":0,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":purchaseAmt.toFixedVSS(2),
						"issueAmount":0,
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"Free"
					};
					$('#purchaseAmtFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
				}
				else
				{
					orderProduct=
					{
						"purchaseQuantity" :0,
						"issuedQuantity":qty,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":0,
						"issueAmount":issuedAmt.toFixedVSS(2),
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"Free"
					};
					$('#issuedAmtFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
				}
				
				orderProductDetailsTemp.push(orderProduct);
				
				orderProductDetails=orderProductDetailsTemp;				
				
				 if(orderStatus==="Packed"){
					//edit inventory 
					productListQtyMngFunc(product.productId);
				} 
				//find totalQuantity and totalAmount
				findTotal();
			});
		}
		else
		{
			/**
			 * quantity only allowed number without decimal
			*/
			$('#qtyNonFree_'+product.productId).keypress(function(evt) {
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
			});
			//if previously exist same event then remove them
			$('#qtyNonFree_'+product.productId).off("keyup");
			$('#qtyNonFree_'+product.productId).keyup(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var previousissuedQuantity;
				var product=fetchProductByProductId(productId);
				var igst=product.categories.igst;
				var srno=1;
				//skipping current change order product quantity from orderProductDetailsTemp
				//and take only its previousissuedQuantity order quantity,order product details and igst percentage
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					//alert(orderProduct.product.product.productId+"!="+productId);
					if(orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else{
						orderProductEdit={
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						}
						mrp=orderProduct.sellingRate;
						igst=orderProduct.product.product.categories.igst;
						product=orderProduct.product.product;
						previousissuedQuantity=orderProduct.issuedQuantity;
					}			
				}
				
				var qty=$(this).val();		
				if(qty=='' || qty==undefined)
				{
					qty="0";
				}
				if(qty==="0")
				{
					quantityZeroValidation=true;
				}
				else
				{
					quantityZeroValidation=false;
				}
				//order status packed then 
				//available quantity with previousissuedQuantity compare with changed quantity
				if(orderStatus==="Packed"){
					//parseInt(previousissuedQuantity)+
					var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

					//if change quantity exceeds allowed quantity then not allowed 
					//change entered quantity to old issuedQuantity
					if(allowedQty<qty){
						Materialize.Toast.removeAll();
						Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

						orderProductDetailsTemp.push(orderProductEdit);
						orderProductDetails=orderProductDetailsTemp;
						$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);

						 //edit inventory 								
						productListQtyMngFunc(product.productId);

						//find totalQuantity and totalAmount
						findTotal();

						return false;
					}
				}
				var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
				var issuedAmt=parseFloat(mrp)*parseFloat(qty);
				
				if(orderStatus==="Booked")
				{
					orderProduct=
					{
						"purchaseQuantity" :qty,
						"issuedQuantity":0,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":purchaseAmt.toFixedVSS(2),
						"issueAmount":0,
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"NonFree"
					};
					$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
				}
				else
				{
					orderProduct=
					{
						"purchaseQuantity" :0,
						"issuedQuantity":qty,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":0,
						"issueAmount":issuedAmt.toFixedVSS(2),
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"NonFree"
					};
					$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
				}
				orderProductDetailsTemp.push(orderProduct);
				
				orderProductDetails=orderProductDetailsTemp;
				
				if(orderStatus==="Packed"){
					//edit inventory 
					productListQtyMngFunc(product.productId);
				}
				//find totalQuantity and totalAmount
				findTotal();
			});
		}
		if(orderProduct.type==="Free")
				{
					/*$('#mrpFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });*/
					/**
					 * mrp only allowed nunmber withtout decimal
					 */
					 $('#mrpFree_'+product.productId).keypress(function(evt) {
						    evt = (evt) ? evt : window.event;
						    var charCode = (evt.which) ? evt.which : evt.keyCode;
						    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
						        return false;
						    }
						    return true;
						});
					//if previously exist same event then remove them
					$('#mrpFree_'+product.productId).off("keyup");
					$('#mrpFree_'+product.productId).keyup(function(){

						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;
						var product=fetchProductByProductId(productId);	
						var igst=product.categories.igst;
						var srno=1;
						//skipping current change order product mrp from orderProductDetailsTemp
						//and take only its and igst percentage
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								orderProductEdit={
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								}
								product=orderProduct.product.product;
								igst=orderProduct.product.product.categories.igst;
							}
						}
						
						var qty=$("#qtyFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/* if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixedVSS(2));
								findTotal();
								return false;
							}
						} */
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						//find totalQuantity and totalAmount
						findTotal();

					});
				}else{
					/*$('#mrpNonFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });*/
					/**
					 * only number allowed without decimal
					 */
					$('#mrpNonFree_'+product.productId).keypress(function(evt) {
					    evt = (evt) ? evt : window.event;
					    var charCode = (evt.which) ? evt.which : evt.keyCode;
					    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					        return false;
					    }
					    return true;
					});
					//if previously exist same event then remove them
					$('#mrpNonFree_'+product.productId).off("keyup");
					$('#mrpNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp=$('#mrpNonFree_'+productId).val();
						var product=fetchProductByProductId(productId);	
						var igst=product.categories.igst;
						if(mrp=='' || mrp==undefined)
						{
							mrp="0";
						}

						var srno=1;
						//skipping current change order product mrp from orderProductDetailsTemp
						//and take only its igst percentage,product details, order product details
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							//alert(orderProduct.product.product.productId+"!="+productId);
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}else{
								orderProductEdit={
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								}
								product=orderProduct.product.product;
								igst=orderProduct.product.product.categories.igst;
							}
						}
						
						var qty=$("#qtyNonFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/*if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpNonFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixedVSS(2));
								findTotal();
								return false;
							}
						}*/
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);
						
						//its function defined in calculateProperAmount.js 
						var calculateProperTaxObj=calculateProperTax(mrp,igst);

						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixedVSS(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixedVSS(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixedVSS(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixedVSS(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						//find total amount and quantity
						findTotal();
					});
				}
		$('#qty').val('');
		$('#curr_qty').val('');
		var source=$('#productid');
		source.val(0);
		source.change();
		$('#nonFreeId').click();
	});	
});
//find total amount and quantity
function findTotal()
{
	var totalQuantity=0;
	var totalAmount=0;
	if(orderStatus==="Booked")
	{
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			totalQuantity+=parseInt(orderProduct.purchaseQuantity);
			totalAmount+=parseInt(orderProduct.purchaseAmount);
		}
	}
	else
	{
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			totalQuantity+=parseInt(orderProduct.issuedQuantity);
			totalAmount+=parseInt(orderProduct.issueAmount);
		}
	}
	
	$('#totalQuantityId').text(parseInt(totalQuantity));
	$('#totalAmountId').text(totalAmount.toFixedVSS(2));
}
//var firstTimeProductDataFetch=true
/* function productListData(){

	var productList;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductListAjax",
		dataType : "json",
		async : false,
		success:function(data)
		{
			productList=data;
			
			//$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;
} */
/**
 * fetch product by product Id
 * @param {*} productId 
 */
 function fetchProductByProductId(productId){
	
	var product;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByProductId?productId="+productId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			product=data;

			/* //for manage invt qty
			if(firstTimeProductDataFetch){
				productListQtyMng=data;
				firstTimeProductDataFetch=false;
			} */
			
			//$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return product;
}
//filter product by brand and category
function filterProductList(brandId,categoryId)
{
	var select = document.getElementById('productid');
	select.options.length = 0;
	select.options.add(new Option("Choose Product", 0));
	var options, index, option;

	var productList;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByBrandAndCategoryIdWeb?brandId="+brandId+"&categoryId="+categoryId,
		dataType : "json",
		async : false,
		success:function(data){

			productList=data;

			for(var i=0; i<productList.length; i++){				
				product=productList[i];
				if(product.brand.brandId==brandId)
				{
					select.options.add(new Option(product.productName, product.productId));
				}			
			}
			
			$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;

	/* if(brandId!=="0" && categoryId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(categoryId!=="0" && brandId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(brandId!=="0" && categoryId!=="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId && product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			select.options.add(new Option(product.productName, product.productId));
		}
	} */
}