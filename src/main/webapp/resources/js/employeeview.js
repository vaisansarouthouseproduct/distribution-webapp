var holidays;
var no=0;
var holidaycheckornot=false;
var checkedId=[];
var dateAdded;
$(document).ready(function() {
	$.validator.setDefaults({
	       ignore: []
		}); 
	//validate incentive given form
	$validator=$('#saveIncentive').validate({
 	    errorElement : "span",
 	    errorClass : "invalid error",
 	    errorPlacement : function(error, element) {
 	      var placement = $(element).data('error');
 	    
 	      if (placement) {
 	        $(placement).append(error)
 	      } else {
 	        error.insertAfter(element);
 	      }
 	      $('select').change(function(){ $('select').valid(); });
 	    }
 	  });
	
	
	
						var oneDate,fromDate,toDate;
						/*$('#oneday').click(function(){
							//alert("oneday");
							oneday=true;
							oneDate=$('#oneDate').val();
							fromDate=$('#fromDate').val();
							toDate=$('#toDate').val();
							alert(oneDate+" <> "+fromDate+" <> "+toDate);
						});
						$('#manydays').click(function(){
							//alert("manydays");
							oneday=false;
							
						});*/
						$('#oneDateDiv').hide();
						$('#fromDateDiv').hide();
						$('#toDateDiv').hide();
						/**
						 * if no of holiday entered or set 
						 * then
						 * if holiday days is 1 then show one date picker
						 * else from to to date picker show
						 */
						$("#holiodaydetailsnoofholidays").on("keyup change",function(){
							$('#oneDateDiv').hide();
							$('#fromDateDiv').hide();
							$('#toDateDiv').hide();
							holidays=0;
							no=$("#holiodaydetailsnoofholidays").val();
							if(no>1){
								$('#fromDateDiv').show();
								$('#toDateDiv').show();
								holidays=no;
							}
							else if(no==1){
								$('#oneDateDiv').show();
								holidays=1;
							}
							else{
								$('#oneDateDiv').hide();
								$('#fromDateDiv').hide();
								$('#toDateDiv').hide();
								holidays=0;
							}
						})
						//only number allowed
						$('#holiodaydetailsnoofholidays').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						//holiday saving or updating process
						$('#submitHoliday').click(function(){
														
							$('#msgholidaysave').html("");
							holidaycheckornot=false;
							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							var oneDate=new Date($('#oneDate').val()).setHours(0,0,0,0);
							
							//date of employee added
							var dateAddedInDate=new Date(parseInt(dateAdded)).setHours(0,0,0,0);
							
							//if no of days mentioned below 0 
							if(no<=0){
								$('#msgholidaysave').html("Select Holiday Dates");
							}
							
							var start=$('#fromDate').val();
							var end=$('#toDate').val();
							var one=$('#oneDate').val();
							var empId=$('#employeeIdForHolidaySave').val();
							var noOfDays=$('#holiodaydetailsnoofholidays').val();
							
							var employeeHolidaysId=$('#employeeHolidaysId').val();
							
							
							
							if(noOfDays==="")
							{
								$('#msgholidaysave').html("Enter No Of Days To Select Holiday Dates");
								
								return false;
							}
							else if(parseInt(noOfDays)==1)
							{
								start=one;
								end=one;
								
								//oneDate check
								if(oneDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select One Date must be after Employee Added Date");
									return false;
								}
								else if(oneDate == today)
								{
									$('#msgholidaysave').html("");									
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
								
							}
							else if(parseInt(noOfDays)>1)
							{
								start=$('#fromDate').val();
								end=$('#toDate').val();
								
								duration = moment(toDate).diff(moment(fromDate), 'days')+1; 

								if(parseInt(noOfDays)!=duration){
									$('#msgholidaysave').html("Number of holidays taken and date range choosen does not match, please check your dates");
									return false;
								}

								//fromDate check
								if(fromDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select From Date must be after Employee Added Date");
									return false;
								}
								else if(fromDate == today)
								{
									$('#msgholidaysave').html("");
									
								}
								else if(toDate <= fromDate)
								{
									$('#msgholidaysave').html("Select From Date in Before To Date");
									return false;
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
								
								//toDate check
								if(toDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select To Date must be after Employee Added Date");
									return false;
								}
								else if(toDate <= fromDate)
								{
									$('#msgholidaysave').html("Select To Date in After From Date");
									return false;
								}
								else if(toDate == today)
								{
									$('#msgholidaysave').html("");
									
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
							}	
							else
							{
								$('#msgholidaysave').html("Enter above Zero('0') No Of Days To Select Holiday Dates");
								return false;
							}
							//if employeeHolidayId not zero then its going for update any holiday 
							//else for save 
							if(employeeHolidaysId!=="0")
							{
								//check select from , to or single date have already holiday or not except same
								$.ajax({
									type : "GET",
									url : myContextPath+"/checkUpdatingHoliday?employeeDetailsId="+empId+"&startDate="+start+"&endDate="+end+"&employeeHolidayId="+employeeHolidaysId,
									dataType : "json",
									data : $("#searchForm").serialize(),
									async: false,
									success : function(data) {
										//alert(data.status);
										$('#msgholidaysave').html(data.status);
										if(data.status!=="")
										{
											//alert("not submit");
											holidaycheckornot=false;
											return false;
										}
										holidaycheckornot=true;
										//alert(holidaycheckornot);
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  //alert("Error");
										}
								});
								
								var paidStatus=$('#paidStatusId').val();
								if(paidStatus==="0")
								{
									$('#msgholidaysave').html("Select Paid Status");
									return false;
								}

								var reason=$('#reason').val();
								if(reason===""){
									$('#msgholidaysave').html("Enter Holiday Reason");
									return false;
								}

								//update holiday if holidaycheckornot is true
								if(holidaycheckornot===true && $('#msgholidaysave').html()==="")
								{
									var form = $('#saveHoliday');
									//alert($('#saveHoliday').attr('action'));
									//alert("going submit");
									//alert(form.serialize()+"&paidStatus="+$('#paidStatusId').val());
									$.ajax({
											type : form.attr('method'),
											url : myContextPath+"/updateHoildays",
											//dataType : "json",
											data : form.serialize()+"&paidStatus="+$('#paidStatusId').val(),
											//async: false,
											success : function(data) {
												$('.modal').modal();
												if(data==="Success")
												{
													/*$('#addeditmsg').find("#modalType").addClass("success");
							    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
													$('#addeditmsg').modal('open');
													$('#head').html("Holiday Booking Message ");
													$('#msg').text("Holiday Booked");
															 setTimeout(
															  function() 
															  {
																  location.reload();
															  }, 2000);*/
													
													//refresh hrm table data with db
													refreshHRM();
													Materialize.Toast.removeAll();
													Materialize.toast('Holiday Booked', '3000', 'teal lighten-2');
													
												}
												else
												{
													/*$('#addeditmsg').find("#modalType").addClass("warning");
							    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
													$('#addeditmsg').modal('open');
													//$('#head').html("Holiday Booking Message");
													$('#msg').text("Holiday Booking Failed");*/
													Materialize.Toast.removeAll();
													Materialize.toast('Holiday Booking Failed', '3000', 'teal lighten-2');
												}
										},
										error: function(xhr, status, error) {
											  //var err = eval("(" + xhr.responseText + ")");
											  //alert(error +"---"+ xhr.responseText+"---"+status);
											 //alert("Error");
											}
									});
								}
								else
								{
									//$('#msgholidaysave').html("Select Proper Holiday Dates");
								}
							
								return false;
							}
							//check select from , to or single date have already holiday or not
							$.ajax({
								type : "GET",
								url : myContextPath+"/checkHoliday?employeeDetailsId="+empId+"&startDate="+start+"&endDate="+end,
								dataType : "json",
								data : $("#searchForm").serialize(),
								async: false,
								success : function(data) {
									//alert(data.status);
									$('#msgholidaysave').html(data.status);
									if(data.status!=="")
									{
										//alert("not submit");
										holidaycheckornot=false;
										return false;
									}
									holidaycheckornot=true;
									//alert(holidaycheckornot);
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									  alert("Error");
									}
							});
							
							var paidStatus=$('#paidStatusId').val();
							if(paidStatus==="0")
							{
								$('#msgholidaysave').html("Select Paid Status");
								return false;
							}

							var reason=$('#reason').val();
							if(reason===""){
								$('#msgholidaysave').html("Enter Holiday Reason");
								return false;
							}
							
							/*alert("success : "+holidaycheckornot);
							return false;*/
							
							if(holidaycheckornot===true && $('#msgholidaysave').html()==="")
							{
								var form = $('#saveHoliday');
								//alert($('#saveHoliday').attr('action'));
								//alert("going submit");
								//alert(form.serialize()+"&paidStatus="+$('#paidStatusId').val());
								$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										//dataType : "json",
										data : form.serialize()+"&paidStatus="+$('#paidStatusId').val(),
										//async: false,
										success : function(data) {
											$('.modal').modal();
											if(data==="Success")
											{
												/*$('#addeditmsg').find("#modalType").addClass("success");
						    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
												$('#addeditmsg').modal('open');
												$('#head').html("Holiday Booking Message ");
												$('#msg').text("Holiday Booked");
														 setTimeout(
														  function() 
														  {
															  location.reload();
														  }, 2000);*/
											    //refresh hrm table data with db
												refreshHRM();
												Materialize.Toast.removeAll();
												Materialize.toast('Holiday Booked', '3000', 'teal lighten-2');
											}
											else
											{
												/*$('#addeditmsg').find("#modalType").addClass("warning");
						    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
												$('#addeditmsg').modal('open');
												$('#head').html("Holiday Booking Message ");
												$('#msg').html("Holiday Booking Failed");*/
												Materialize.Toast.removeAll();
												Materialize.toast('Holiday Booking Failed', '3000', 'teal lighten-2');
											}
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  //alert(error +"---"+ xhr.responseText+"---"+status);
										 alert("Error");
										}
								});
							}
							else
							{
								//$('#msgholidaysave').html("Select Proper Holiday Dates");
							}
						});
						//on fromDate change check 
						//blank and must before toDate
						$('#fromDate').change(function(){

							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							//$('#toDate').val($('#fromDate').val());
							//$('#toDate').pickadate();
							//$('#toDate').focus();
							$('#fromDate').focus();
							
							/*if(fromDate < today)
							{
								$('#msgholidaysave').html("Select From Date in Current Date or After that date");
								
							}
							else*/ if(fromDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else if(toDate <= fromDate)
							{
								$('#msgholidaysave').html("Select From Date in Before ToDate");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
							
						});
						
						
						//on toDate change check 
						//blank and must after fromDate
						$('#toDate').change(function(){
							//alert(new Date());		
							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							
							/*if(toDate < today)
							{
								$('#msgholidaysave').html("Select To Date in Current Date or After that date");
								
							}
							else*/ if(toDate <= fromDate)
							{
								$('#msgholidaysave').html("Select To Date in After From Date");
								
							}
							else if(toDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
							
						}); 
						
						$('#oneDate').change(function(){
							var today=new Date().setHours(0,0,0,0);
							var oneDate=new Date($('#oneDate').val()).setHours(0,0,0,0);
							
							/*if(oneDate < today)
							{
								$('#msgholidaysave').html("Select One Date in Current Date or After that date");
								
							}
							else*/ if(toDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
						});
						//save invcentives
						$('#saveIncentivesSubmit').click(function(){
							var form = $('#saveIncentive');
							
							/*var incentiveAmount=$('#incentiveAmount').val();
							var reason=$('#reasonIncentive').val();
							if(incentiveAmount==="")
							{
								$('#msgincentivesave').html("Enter Incentive Amount");
								return false;
							}
							else if(reason.trim()==="")
							{
								$('#msgincentivesave').html("Enter Incentive Reason");
								return false;
							}*/
							
							/*var incentiveId=$('#incentiveId').val();
							if(incentiveId==="0")
							{*/
							
								//$('#msgincentivesave').html("");
								
								//alert(form.serialize());
							if ($("#saveIncentive").valid()) {
						        // do something here when the form is valid
						       //alert();
						    
								$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										data : form.serialize(),
										//async: false,
										success : function(data) {
											
											if(data==="Success")
											{			
												refreshHRM();
												/*$('#msgincentivesave').html("<font color='green'>Incentive Assigned </green>");
												$('#msgincentivesave').focus();*/
												Materialize.Toast.removeAll();
												Materialize.toast('Incentive Assigned', '3000', 'teal lighten-2');
												//searchEmployeeIncentive(1);
											}
											else
											{
												Materialize.Toast.removeAll();
												Materialize.toast('Incentive Assigning Failed', '3000', 'teal lighten-2');
												//$('#msgincentivesave').html("<font color='red'>Incentive Assigning Failed</green>");
											}
											$('.modal').modal();
											$('#incentive').modal('close');
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  //alert(error +"---"+ xhr.responseText+"---"+status);
										 //salert("Error");
										}
								});
							}
							/*}
							else
							{
								$.ajax({
									type : form.attr('method'),
									url : myContextPath+"/editIncentivesAjax",
									data : form.serialize(),
									success : function(data) {
									
										if(data==="Success")
										{
											$('#msgincentivesave').html("<font color='green'>Incentive SuccessFully Updated</green>");
											$('#incentiveId').val("0");
											$('#incentiveAmount').val("");
											$('#saveIncentivesSubmit').text("Save");
											searchEmployeeIncentive(1);
										}
										else
										{
											$('#msgincentivesave').html("<font color='red'>Incentive Updating Failed</green>");
										}
									}
								});
							}*/
							
						});
						//reset incentive modal form
						$('#resetIncentive').click(function (){
							$('#incentiveId').val("0");
							$('#incentiveAmount').val("");
							$('#saveIncentivesSubmit').text("Save");
						});
						//payment amount take only number with decimal
						$('#paymentamount').keydown(function(e){            	
							-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
						 });
			            
						document.getElementById('paymentamount').onkeypress=function(e){
			            	
			            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
			              		 return false;
			          		 }

			            }
						
						//payment incentiveAmount take only number with decimal
						$('#incentiveAmount').keydown(function(e){            	
							-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
						 });
			            
						document.getElementById('incentiveAmount').onkeypress=function(e){
			            	
			            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
			              		 return false;
			          		 }

			            }
						//save employee payement 
						//not used
						$('#savePaymentId').click(function(){
							
							var payCash=$('#paymentCash').is(':checked');
							var payCheque=$('#paymentCheque').is(':checked');
							
							var amount=$('#paymentamount').val();
							var bankName=$('#bankName').val();
							var cheqNo=$('#cheqNo').val();
							var cheqDate=$('#cheqDate').val();
							var paymentcomment=$('#paymentcomment').val();
							
							if(amount==="")
							{
								$('#msgpaymentsave').html("Enter Amount");
								return false;
							}
							
							if(paymentcomment==="")
							{
								$('#msgpaymentsave').html("Enter Comment");
								return false;
							}
							
							if(payCheque==true)
							{
								if(bankName==="")
								{
									$('#msgpaymentsave').html("Enter Bank Name");
									return false;
								}
								
								if(cheqNo==="")
								{
									$('#msgpaymentsave').html("Enter Cheque Number");
									return false;
								}
								
								if(cheqDate==="")
								{
									$('#msgpaymentsave').html("Enter Cheque Date");
									return false;
								}
								
								var today=new Date().setHours(0,0,0,0);
								var chDate=new Date(cheqDate).setHours(0,0,0,0);
								
								if(chDate < today)
								{
									$('#msgpaymentsave').html("Select Cheque Date in Current Date or After that date");
									return false;
								}
								
							}
							$('#msgpaymentsave').html('');
							
							var form = $('#savePayment');
							
							$.ajax({
									type : form.attr('method'),
									url : form.attr('action'),
									data : form.serialize(),
									//async: false,
									success : function(data) {
										
										if(data==="Success")
										{
											$('#msgpaymentsave').html("<font color='green'>Payment Done </green>");
											id=$('#employeeDetailsIdId').val();
											$('#employeeDetailsIdId').val('');
											$('#paymentamount').val('');
											$('#bankName').val('');
											$('#cheqNo').val('');
											$('#cheqDate').val('');
											$('#paymentcomment').val('');
											$('#employeeDetailsIdId').val(id);
											
											setTimeout(
													  function() 
													  {
														  location.reload();
													  }, 2000);
											
											$.ajax({
												url : myContextPath+"/openPaymentModel?employeeDetailsId="+id,
												dataType : "json",
												async: false,
												success : function(data) {
													
													employeePaymentModel=data;
													$('#employeeDetailsIdId').val(id);
													$('#paymentName').html("<b>Name : </b>"+employeePaymentModel.name);
													$('#paymenttotalAmt').html("<b>Total Amount : </b>"+employeePaymentModel.totalsalary.toFixedVSS(2));
													$('#paymentamtPaid').html("<b>Amount Paid : </b>"+employeePaymentModel.amountPaid.toFixedVSS(2));
													$('#paymentamtRemaining').html("<b>Amount Pending : </b>"+employeePaymentModel.amountPending.toFixedVSS(2));
												}
											});
										}
										else
										{
											$('#msgpaymentsave').html("<font color='red'>Payment Failed</green>");
										}
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									  //alert(error +"---"+ xhr.responseText+"---"+status);
									 alert("Error");
									}
							});
							
						});
						//if select for check all employee for sms then show all employee records selected
						$("#checkAll").click(function () {
			            	var colcount=$('#tblData').DataTable().columns().header().length;
			                var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			                    state = this.checked;
								//alert(state);
			                for (var i = 0; i < cells.length; i += 1) {
			                    cells[i].querySelector("input[type='checkbox']").checked = state;
			                }		                
			                
			            });
						//at time of select employee 
						//all select if select all employee
			             $("input:checkbox").change(function(a){
			            	 
			            	 var colcount=$('#tblData').DataTable().columns().header().length;
			                 var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			                     
			                  state = false; 				
			                 var ch=false;
			                 
			                 for (var i = 0; i < cells.length; i += 1) {
			                	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
			                	
			                     if(cells[i].querySelector("input[type='checkbox']").checked == state)
			                     { 
			                    	 $("#checkAll").prop('checked', false);
			                    	 ch=true;
			                     }                      
			                 }
			                 if(ch==false)
			                 {
			                	 $("#checkAll").prop('checked', true);
			                 }
			                	
			                //alert($('#tblData').DataTable().rows().count());
			            });
						/**
						 * check one employee select or multiple
						 * if one then show mobile number for edit
						 */
						$('#sendsmsid').click(function(){
							
							$('#mobileEditSms').prop('checked', false);
						 	$("#mobileEditSms").change();
						 	mobileNumberCollection='';
							
							var count=parseInt('${count}')
							checkedId=[];
							//chb
							 var idarray = $("#employeeTblData")
				             .find("input[type=checkbox]") 
				             .map(function() { return this.id; }) 
				             .get();
							 var j=0;
							 var mobileNumberCollection="";
							 for(var i=0; i<idarray.length; i++)
							 {								  
								 idarray[i]=idarray[i].replace('chb','');
								 if($('#chb'+idarray[i]).is(':checked'))
								 {
									 checkedId[j]=idarray[i].split("_")[0];
									 mobileNumberCollection=mobileNumberCollection+idarray[i].split("_")[1]+",";
									 j++;
								 }
							 }
							 mobileNumberCollection=mobileNumberCollection.substring(0,mobileNumberCollection.length-1);
							 $('#mobileNoSms').val(mobileNumberCollection);
							 $('#mobileNoSms').change();
							 $('#smsText').val('');
							 $('#smsSendMesssage').html('');
							 
							 if(checkedId.length==0)
							 {
								 $('#addeditmsg').find("#modalType").addClass("warning");
		    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
								 $('#addeditmsg').modal('open');							    
							     $('#msg').text("Select Employee For Send SMS");
							 }
							 else
							 {
								 if(checkedId.length==1)
								 {
									 $('#smsText').val('');
									 $('#smsSendMesssage').html('');
									 $('.mobileDiv').show();
									 $('#sendMsg').modal('open');
								 }
								 else{
								 $('#smsText').val('');
								 $('#smsSendMesssage').html('');
								 $('.mobileDiv').hide();
								 $('#sendMsg').modal('open');
								 }
							 }
						});
						/**
						 * check sms text for blank
						 * send sms to selected employee
						 */
						$('#sendMessageId').click(function(){
							
							
							 $('#smsSendMesssage').html('');
							
							 var smsText=$("#smsText").val();
								if(checkedId.length==0)
								{
									$('#smsSendMesssage').html("<font color='red'>Select Employee For Send Message</font>");
									return false;
								}
								if(smsText==="")
								{
									$('#smsSendMesssage').html("<font color='red'>Enter Message Text</font>");
									return false;
								}
							
							var form = $('#sendSMSForm');
							//alert(form.serialize()+"&employeeDetailsId="+checkedId);
							
							$.ajax({
									type : form.attr('method'),
									url : form.attr('action'),
									data : form.serialize()+"&employeeDetailsId="+checkedId+"&mobileNumber="+$('#mobileNoSms').val(),
									//async: false,
									success : function(data) {
										if(data==="Success")
										{
											$('#smsSendMesssage').html("<font color='green'>Message send SuccessFully</font>");
											$('#smsText').val('');
										}
										else
										{
											$('#smsSendMesssage').html("<font color='red'>Message sending Failed</font>");
										}
									}
							});
						});
						
						refreshHRM();
					});

/* function showSalaryDetail(id)
{
	
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetail?employeeDetailsId="+id,
		dataType : "json",
		success : function(data) {
			employeeSalaryStatus=data[0];
			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary+" &#8377;");
			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction+" &#8377;");
			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive+" &#8377;");
			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount+" &#8377;");
			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth+" &#8377;");
			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth+" &#8377;");
			//alert(employeeSalaryStatus.employeeDetailsId);
			$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
			
			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
			$("#empdetailtable").empty();
			if(employeeSalarytable!==null)
			{
				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
				{
					employeeSalary=employeeSalarytable[i];
					var currentTime=new Date(parseInt(employeeSalary.date));
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					var date = day + "/" + month + "/" + year;
					
						$("#empdetailtable").append("<tr>"+
											        "<td>"+employeeSalary.srno+"</td>"+
											        "<td>"+employeeSalary.amount+" &#8377;"+"</td>"+
											        "<td>"+employeeSalary.incentives+" &#8377;"+"</td>"+
											        "<td>"+date+"</td>"+
											        "<td>"+employeeSalary.modeOfPayment+"</td>"+
											        "<td>"+employeeSalary.comment+"</td>"+
											    	"</tr>"); 
						
				}
			}
			$('.modal').modal();
			$('#EmpDetails').modal('open');
		}
	});
} */
//filter employee salary by range,startdate,enddate,employeeDetailsId
function searchEmployeeSalary(choice)
{
	
	var filter="";
	var url="";
	var employeeDetailsId=$('#employeeDetailsId').val();
	var startDate=$('#startDate1').val();
	var endDate=$('#endDate1').val();
	//alert(startDate +"-"+ endDate);
	//return false;
	if(choice==1)
	{
		filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==2)
	{
		filter="Last6Months";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		
	}
	else if(choice==3)
	{
		filter="Last1Year";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==4)
	{
		filter="Range";
		
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
	}
	else if(choice==5)
	{
		filter="ViewAll";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
		
	
	$.ajax({
		type : "GET",
		url : url,
		/* data: "id=" + id + "&name=" + name, */
		success : function(data) {
			
			employeeSalaryStatus=data[0];
			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary+" &#8377;");
			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction+" &#8377;");
			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive+" &#8377;");
			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount+" &#8377;");
			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth+" &#8377;");
			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth+" &#8377;");
			//alert(employeeSalaryStatus.employeeDetailsId);
			$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
			
			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
			$("#empdetailtable").empty();
			if(employeeSalarytable!==null)
			{
				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
				{
					employeeSalary=employeeSalarytable[i];
					//long date to dd/MM/yyyy format
					var currentTime=new Date(parseInt(employeeSalary.date));
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					var date = day + "/" + month + "/" + year;
					
						$("#empdetailtable").append("<tr>"+
											        "<td>"+employeeSalary.srno+"</td>"+
											        "<td>"+employeeSalary.amount+" &#8377;"+"</td>"+
											        "<td>"+employeeSalary.incentives+" &#8377;"+"</td>"+
											        "<td>"+date+"</td>"+
											        "<td>"+employeeSalary.modeOfPayment+"</td>"+
											        "<td>"+employeeSalary.comment+"</td>"+
											    	"</tr>"); 
						
				}
			}
			/*$('.modal').modal();
			$('#EmpDetails').modal('open');*/
		}
	});
}
	//show employee holiday details list from current month
	function showHolidayDetail(id)
	{
		//alert("showHolidayDetail "+id);
		$.ajax({
			url : myContextPath+"/fetchEmployeeHolidayDetail?employeeDetailsId="+id,
			dataType : "json",
			success : function(data) {
				employeeHolidayStatus=data;
				//alert(employeeHolidayStatus);
				$('#employeeDetailsId1').val(employeeHolidayStatus.employeeDetailPkId);
				$('#empholidayname').html("<b>Name : </b>"+employeeHolidayStatus.name);
				$('#empholidaydepartment').html("<b>Department : </b>"+employeeHolidayStatus.departmentname);
				$('#empholidaymobileno').html("<b>Mobile Number : </b>"+employeeHolidayStatus.mobileNumber);
				$('#empholidaynoofholiday').html("<b>No Of Holidays : </b>"+employeeHolidayStatus.noOfHolidays);
										
				employeeHolidaytable=employeeHolidayStatus.employeeHolidayList;
				$("#holidaytable").empty();
				if(employeeHolidaytable!==null)
				{
					for (var i = 0, len = employeeHolidaytable.length; i < len; ++i)
					{
						employeeHoliday=employeeHolidaytable[i];

						//long date to dd/MM/yyyy format
						var currentTime=new Date(parseInt(employeeHoliday.date));
						var month = currentTime.getMonth() + 1;
						var day = currentTime.getDate();
						var year = currentTime.getFullYear();
						var date = day + "/" + month + "/" + year;
						
						//long date to dd/MM/yyyy format
						var currentTime=new Date(parseInt(employeeHoliday.dateFrom));
						 month = currentTime.getMonth() + 1;
						 day = currentTime.getDate();
						 year = currentTime.getFullYear();
						var dateFrom = day + "/" + month + "/" + year;
						
						//long date to dd/MM/yyyy format
						var currentTime=new Date(parseInt(employeeHoliday.dateTo));
						 month = currentTime.getMonth() + 1;
						 day = currentTime.getDate();
						 year = currentTime.getFullYear();
						var dateTo = day + "/" + month + "/" + year;
						//alert(employeeHoliday.employeeHolidayId);
						
						//if holiday disabled then disable its edit,delete button
						var editDeleteButton;
						if(employeeHolidayStatus.status==true)
						{
							editDeleteButton="<td><a href='#' class='btn btn-flat blue-text' disabled='disabled' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></a>"+
							"<a href='#' class='btn btn-flat blue-text' disabled='disabled' onclick='deleteHolidayConfirmation("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>delete</i></a></td>";
						}	
						else
						{
							editDeleteButton="<td><a class='btn btn-flat blue-text' href='#' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></a>"+
							"<a class='btn btn-flat blue-text' href='#' onclick='deleteHolidayConfirmation("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>delete</i></a></td>";
						}
						//change Color basis Paid/UnPaid holiday
							var leaveTypeCell="";
							if(employeeHoliday.typeOfLeave.trim()==="Paid")
							{
								leaveTypeCell="<td><font color='Green'><b>"+employeeHoliday.typeOfLeave+"</b></font></td>";
							}
							else
							{
								leaveTypeCell="<td><font color='Red'><b>"+employeeHoliday.typeOfLeave+"</b></font></td>";
							}
						
							$("#holidaytable").append("<tr>"+
												        "<td>"+employeeHoliday.srno+"</td>"+
												        "<td>"+employeeHoliday.noOfHolidays+"</td>"+	
												        "<td>"+dateFrom+"</td>"+
												        "<td>"+dateTo+"</td>"+
												        leaveTypeCell+
												        "<td>"+(employeeHoliday.amountDeduct).toFixedVSS(2)+" &#8377;"+"</td>"+
												        "<td>"+employeeHoliday.reason+"</td>"+
												        "<td>"+date+"</td>"+
														editDeleteButton+
												    	"</tr>"); 
							
					}
				}
				$('.modal').modal();
				$('#viewHoliday').modal('open');
			}
		});
	}
	//filter employee holiday list by startDate,endDate,employeeDetailsId,range
	function searchEmployeeHolidays(choice)
	{  $(".showDates").hide();
		var filter="";
		var url="";
		var employeeDetailsId=$('#employeeDetailsId1').val();
		var startDate=$('#startDate2').val();
		var endDate=$('#endDate2').val();
		//alert(startDate +"-"+ endDate);
		//return false;
		if(choice==1)
		{
			filter="CurrentMonth";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
		else if(choice==2)
		{
			filter="Last6Months";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
			
		}
		else if(choice==3)
		{
			filter="Last1Year";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
		else if(choice==4)
		{
			filter="Range";
			
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
		}
		else if(choice==5)
		{
			filter="ViewAll";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
			
		
		$.ajax({
			type : "GET",
			url : url,
			/* data: "id=" + id + "&name=" + name, */
			success : function(data) {
				
				employeeHolidayStatus=data;
				
				$('#empholidayname').html("<b>Name : </b>"+employeeHolidayStatus.name);
				$('#empholidaydepartment').html("<b>Department : </b>"+employeeHolidayStatus.departmentname);
				$('#empholidaymobileno').html("<b>Mobile Number : </b>"+employeeHolidayStatus.mobileNumber);
				$('#empholidaynoofholiday').html("<b>No Of Holidays : </b>"+employeeHolidayStatus.noOfHolidays);
				
				
						
				employeeHolidaytable=employeeHolidayStatus.employeeHolidayList;
				$("#holidaytable").empty();
				if(employeeHolidaytable!==null)
				{
					for (var i = 0, len = employeeHolidaytable.length; i < len; ++i)
					{
						//long date to dd/MM/yyyy format
						employeeHoliday=employeeHolidaytable[i];
						var holydayTakenDate=new Date(parseInt(employeeHoliday.date));
						var month = holydayTakenDate.getMonth() + 1;
						var day = holydayTakenDate.getDate();
						var year = holydayTakenDate.getFullYear();
						var holydayTakenDateString = day + "/" + month + "/" + year;
						
						//long date to dd/MM/yyyy format
						var dateFrom=new Date(parseInt(employeeHoliday.dateFrom));
						month = dateFrom.getMonth() + 1;
						day = dateFrom.getDate();
						year = dateFrom.getFullYear();
						var dateFromString = day + "/" + month + "/" + year;
						
						//long date to dd/MM/yyyy format
						var dateTo=new Date(parseInt(employeeHoliday.dateTo));
						month = dateTo.getMonth() + 1;
						day = dateTo.getDate();
						year = dateTo.getFullYear();
						var dateToString = day + "/" + month + "/" + year;
						
						/*<th>Sr.No</th>
                        <th>No.of holiday</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Type Of Leave</th>
                        <th>Amount Deduct</th>
                        <th>Reason</th>
                        <th>Holiday Given Date</th>
                        
                        *private long srno;
						private long noOfHolidays;
						private String typeOfLeave;
						private double amountDeduct;
						private Date date;
						private Date dateFrom;
						private Date dateTo;	
						private String reason;
                        *
                        *disabled="disabled"
                        */
						//alert(employeeHolidayStatus.status);

						//if holiday disabled then disable its edit,delete button
						var editDeleteButton;
						if(employeeHolidayStatus.status==true)
						{
							editDeleteButton="<td><a href='#' class='btn btn-flat blue-text' disabled='disabled' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></a>"+
							"<a href='#'  class='btn btn-flat blue-text' disabled='disabled' onclick='deleteHolidayConfirmation("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>delete</i></a></td>";
						}	
						else
						{
							editDeleteButton="<td><a href='#'  class='btn btn-flat blue-text' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></a>"+
							 "<a href='#' class='btn btn-flat blue-text' onclick='deleteHolidayConfirmation("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>delete</i></a></td>";
						}
						//change Color basis Paid/UnPaid holiday
						var leaveTypeCell="";
						if(employeeHoliday.typeOfLeave.trim()==="Paid")
						{
							leaveTypeCell="<td><font color='Green'><b>"+employeeHoliday.typeOfLeave+"</b></font></td>";
						}
						else
						{
							leaveTypeCell="<td><font color='Red'><b>"+employeeHoliday.typeOfLeave+"</b></font></td>";
						}
						$("#holidaytable").append("<tr>"+
											        "<td>"+employeeHoliday.srno+"</td>"+
											        "<td>"+employeeHoliday.noOfHolidays+"</td>"+
											        "<td>"+dateFromString+"</td>"+
											        "<td>"+dateToString+"</td>"+
											        leaveTypeCell+												        
											        "<td>"+employeeHoliday.amountDeduct.toFixedVSS(2)+" &#8377;"+"</td>"+
											        "<td>"+employeeHoliday.reason+"</td>"+
											        "<td>"+holydayTakenDateString+"</td>"+
													editDeleteButton+
											    	"</tr>"); 
							
					}
				}
				/*$('.modal').modal();
				$('#EmpDetails').modal('open');*/
			}
		});

	}
	//delete holiday confirmation before delete any holiday
	function deleteHolidayConfirmation(id){
	
		var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deleteHoliday('+id+')">Delete</button>  '));
		Materialize.Toast.removeAll();
    	  Materialize.toast($toastContent, "4000");
	}
	//delete holiday and refresh hrm table
	function deleteHoliday(id){
	
    	$.ajax({
    		type : "GET",
    		url : myContextPath+"/deleteHoliday?employeeHolidaysId="+id,
    		success : function(data) {
    			if(data=="Success"){ 
					Materialize.Toast.removeAll();
					Materialize.toast('Holiday Delete SuccessFully', '3000', 'teal lighten-2'); 	
					refreshHRM();			
    			}else{
					Materialize.Toast.removeAll();
					Materialize.toast('Holiday Deleting Failed', '3000', 'red lighten-2');
    			}
    		},
    		error: function(xhr, status, error) {
				Materialize.Toast.removeAll();
    			Materialize.toast('Something Went Wrong', '3000', 'red lighten-2');
			}
    	});
    }
	//open holiday given modal and set holiday given old data on modal

	function editHoliday(id)
	{
		$('#holiodaydetailsname').focus();
		$('#holiodaydetailsmobilenumber').focus();
		$.ajax({
			url : myContextPath+"/fetchEmployeeHolidaysByEmployeeHolidaysId?employeeHolidaysId="+id,
			dataType : "json",
			success : function(data) {
				
				$('#holiodaydetailsname').val(data.employeeDetails.name);
				
				
				$('#holiodaydetailsmobilenumber').val(data.employeeDetails.contact.mobileNumber);
				
				dateAdded=data.employeeDetails.employeeDetailsAddedDatetime;
				$('#employeeIdForHolidaySave').val(data.employeeDetails.employeeDetailsId);
				$('#employeeHolidaysId').val(data.employeeHolidaysId);
				
				//long date to dd/MM/yyyy
				var dateFrom=new Date(parseInt(data.fromDate));
				month = dateFrom.getMonth() + 1;
				day = dateFrom.getDate();
				year = dateFrom.getFullYear();
				var dateFromString = year + "-" + month + "-" + day;
				
				//long date to dd/MM/yyyy
				var dateTo=new Date(parseInt(data.toDate));
				month = dateTo.getMonth() + 1;
				day = dateTo.getDate();
				year = dateTo.getFullYear();
				var dateToString = year + "-" + month + "-" + day;

				//alert(dateToString);
				
				var fromDate=new Date(dateFromString).setHours(0,0,0,0);
				var toDate=new Date(dateToString).setHours(0,0,0,0);
				
				if(fromDate==toDate)
				{				
					$('#holiodaydetailsnoofholidays').val("1");
					
					$('#oneDate').val(dateFromString);
					$('#oneDate').change();
					$('#holiodaydetailsnoofholidays').change();
				}
				else
				{
					duration = moment(dateTo).diff(moment(dateFrom), 'days')+1; 
					
					//var diffDays = daydiff(parseDate(fromDate), parseDate(toDate));
					$('#holiodaydetailsnoofholidays').val(duration);
					
					$('#fromDate').val(dateFromString);
					$('#fromDate').change();
					
					$('#toDate').val(dateToString);
					$('#toDate').change();
					$('#holiodaydetailsnoofholidays').change();
				}
				
				
				var source3 = $("#paidStatusId");
				if(data.paidHoliday==1)
				{
					source3.val("paid");
				}
				else
				{
					source3.val("unpaid");
				}
				source3.change();
				
				$('#reason').val(data.reason);
				$('#reason').change();
				
				$('#submitHoliday').text("Update");
								
				//holiday
				$('.modal').modal();
				$('#viewHoliday').modal('close');
				$('#holiday').modal('open');
			}
		});
	}
	//show area list according employee details
	function showAreaDetail(id)
	{
		var table = $('#modalTable').DataTable();
		table.destroy();
		$.ajax({
			url : myContextPath+"/fetchEmployeeAreas?employeeDetailsId="+id,
			dataType : "json",
			success : function(data) {
				
				employeeAreaDetails=data;

				$("#employeeareaId").html("<b>Employee Id: </b>"+employeeAreaDetails.employeeGenId);
				$('#employeeareaname').html("<b>Name: </b>"+employeeAreaDetails.name);
				$('#employeeareadeptname').html("<b>Department: </b>"+employeeAreaDetails.departmentName);
				$('#employeeareamobilenumber').html("<b>Mobile No: </b>"+employeeAreaDetails.mobileNumber);
				
				employeeAreaList=employeeAreaDetails.areaList;
				var srno=1;
				var empGenId = employeeAreaDetails.employeeGenId;
				$("#areaList").empty();
				for(var i=0; i<employeeAreaList.length; i++)
				{					
					area=employeeAreaList[i];
					$('#areaList').append("<tr>"+
					        "<td>"+srno+"</td>"+
					        "<td>"+area.name+"</td>"+
					        "</tr>"); 
					srno++;
				}
				$('#modalTable').DataTable({
					 "bSort" : false,
					 "bPaginate": false,
					 "bDestroy": true,
					 "autoWidth":false,					 
				      dom: '<"modal-dt-buttons"B>',
				      buttons: {
				             buttons: [
				              
				                 {
				                     extend: 'pdf',
				                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
				                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
				                     //title of the page
				                     title: 'Area Details',
				                     //file name 
				                     filename: function() {
				                         var d = new Date();
				                         var date = d.getDate();
				                         var month = d.getMonth();
				                         var year = d.getFullYear();
				                         var name = 'Area Details(HRM)';
				                         return name + date + '-' + month + '-' + year;
				                     },
				                    
				                     //  exports only dataColumn
				                     exportOptions: {
				                         columns: ':visible.print-col',
				                         
				                     },
				                     customize: function(doc, config) {
				                    	 doc.content.forEach(function(item) {
				                    		  if (item.table) {
				                    		  item.table.widths = ['*','*'] 
				                    		 } 
				                    		    });
				                    	 doc.styles.tableHeader.alignment = 'center';
				                         doc.styles.tableBodyEven.alignment = 'center';
				                         doc.styles.tableBodyOdd.alignment = 'center';
				                     },
				                     messageTop: function () {                 
				                    	 return 'Employee Id :'+empGenId+'     '+'Name :'+employeeAreaDetails.name+'      '+'Department :'+employeeAreaDetails.departmentName+'      '+'Mobile No :'+employeeAreaDetails.mobileNumber;
			                         },
			                         
				                 },
				                 {
				                     extend: 'excel',
				                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
				                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
				                     //title of the page
				                     title: 'Area Details',
				                     //file name 
				                     filename: function() {
				                         var d = new Date();
				                         var date = d.getDate();
				                         var month = d.getMonth();
				                         var year = d.getFullYear();
				                         var name = 'Area Details(HRM)';
				                         return name + date + '-' + month + '-' + year;
				                     },
				                      messageTop: function () {                 
			                             return 'Employee Id :'+empGenId+'     '+'Name :'+employeeAreaDetails.name+'      '+'Department :'+employeeAreaDetails.departmentName+'      '+'Mobile No :'+employeeAreaDetails.mobileNumber;
			                         }, 
			                         
				                    
				                     //  exports only dataColumn
				                     exportOptions: {
				                         columns: ':visible.print-col'
				                     },
				                 },
				                 {
				                     extend: 'print',
				                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
				                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
				                     //title of the page
				                    title: '',
				                     //file name 
				                     filename: function() {
				                         var d = new Date();
				                         var date = d.getDate();
				                         var month = d.getMonth();
				                         var year = d.getFullYear();
				                         var name = 'Area Details(HRM)';
				                         return name + date + '-' + month + '-' + year;
				                     },
				                    
				                     customize: function (win) {
				                    	 console.log(win);
				                    	
				                         $(win.document.body)
				                             .css( 'font-size', '10pt' )
				                             .prepend(
				                                 '<h6 style="position:absolute; top:0; left:0;">Employee Id :'+empGenId+'</h6>'+
				                                 '<h6 style="position:absolute; top:0; right:10%;">Name: '+employeeAreaDetails.name+'</h6>'+
				                                 '<br/>'+
				                                 '<h6 style="position:absolute; top:5%; left:0;">Department: '+employeeAreaDetails.departmentName+'</h6>'+
				                                 '<h6 style="position:absolute; top:5%; right:10%;">Mobile No: '+employeeAreaDetails.mobileNumber+'</h6>'+
												 '<br/>'
				                             );
				     				                     } ,
			                       
				                     
				                     //  exports only dataColumn
				                     exportOptions: {
				                         columns: ':visible.print-col'
				                     },
				                 }
				                 ]
				         }
				  });
				$('.modal').modal();
				$('#area').modal('open');
			}
		});
	}
//open holiday given modal with employee name and mobile number
function openHolidayBookModel(id)
{
	$('#submitHoliday').text("Submit");
	$('#employeeIdForHolidaySave').val(id);
	$('#msgholidaysave').html('');
	$('#oneday').click();
	//$('#onedays').prop('checked', true);
	$('#holiodaydetailsnoofholidays').val('');
	$('#oneDate').val('');
	$('#fromDate').val('');
	$('#toDate').val('');
	$('#fromDate').change();
	$('#toDate').change();
	$('#oneDate').change();
	$('#reason').val('');
	$('#oneDateDiv').hide();
	$('#fromDateDiv').hide();
	$('#toDateDiv').hide();
	$('#holiodaydetailsname').focus();
	$('#holiodaydetailsmobilenumber').focus();
	 var source=$('#paidStatusId');
		source.val('0');
		source.change();
		holidays=0;
	
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetailsByEmployeeDetailsId?employeeDetailsId="+id,
		dataType : "json",
		success : function(employeeDetails) {
			
			$('#holiodaydetailsname').val(employeeDetails.name);
			
			
			$('#holiodaydetailsmobilenumber').val(employeeDetails.contact.mobileNumber);
			
			dateAdded=employeeDetails.employeeDetailsAddedDatetime;
			$('.modal').modal();
			$('#holiday').modal('open');
		}
	});
	
	
	
}
//set incentive id and amount and button name to Update
function editIncentives(id,amt){
	$('#incentiveId').val(id);
	$('#incentiveAmount').val(amt);
	$('#saveIncentivesSubmit').text("Update");
}


//open incentive modal with employee name
function openIncentivesModel(id)
{

	$('#incentiveName').val('');
	$('#incentiveAmount').val('');
	$('#incentiveEmployeeDetailsId').val(id);
	$('#msgincentivesave').html('');
	$('#reasonIncentive').val('');
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetailsByEmployeeDetailsId?employeeDetailsId="+id,
		dataType : "json",
		success : function(employeeDetails) {
			
			$('#incentiveName').val(employeeDetails.name);
			
			$('.modal').modal();
			$('#incentive').modal('open');
		}
	});
	
}

/*function searchEmployeeIncentive(choice)
{
	var filter="";
	var url="";
	var employeeDetailsId=$('#incentiveEmployeeDetailsId').val();
	var startDate=$('#startDateIncentive').val();
	var endDate=$('#endDateIncentive').val();
	//alert(startDate +"-"+ endDate);
	//return false;
	if(choice==1)
	{
		filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==2)
	{
		filter="Last6Months";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";		
	}
	else if(choice==3)
	{
		filter="Last1Year";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==4)
	{
		filter="Range";
		
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
	}
	else if(choice==5)
	{
		filter="ViewAll";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
		
	
	$.ajax({
		type : "GET",
		url : url,
		 data: "id=" + id + "&name=" + name, 
		success : function(data) {
			
			employeeDetails=data.employeeDetails; 
			employeeIncentiveList=data.employeeIncentiveList;
			
			$('#incentiveName').val(employeeDetails.name);
			
			$("#incentiveTable").empty();
			if(employeeIncentiveList!==null)
			{
				for (var i = 0, len = employeeIncentiveList.length; i < len; ++i)
				{
					employeeIncentive=employeeIncentiveList[i];
					var incentiveGivenDate=new Date(parseInt(employeeIncentive.incentiveGivenDate));
					var month = incentiveGivenDate.getMonth() + 1;
					var day = incentiveGivenDate.getDate();
					var year = incentiveGivenDate.getFullYear();
					var incentiveGivenDateString = day + "/" + month + "/" + year;
					
					
					$("#incentiveTable").append("<tr>"+
										        "<td>"+(i+1)+"</td>"+
										        "<td>"+employeeIncentive.incentiveAmount+"</td>"+
										        "<td>"+incentiveGivenDateString+"</td>"+
										        "<td><button type='button' onclick='editIncentives("+employeeIncentive.employeeIncentiveId+","+employeeIncentive.incentiveAmount+")'>Edit</button></td>"+
										    	"</tr>"); 
						
				}
			}
		}
	});

}*/


/* 	function openPaymentModel(id)
	{

		$('#employeeDetailsIdId').val('');
		$('#paymentamount').val('');
		$('#bankName').val('');
		$('#cheqNo').val('');
		$('#cheqDate').val('');
		$('#paymentcomment').val('');
		$('#employeeDetailsIdId').val(id);
		$('#msgpaymentsave').html('');
		$.ajax({
			url : myContextPath+"/openPaymentModel?employeeDetailsId="+id,
			dataType : "json",
			async: false,
			success : function(data) {
				
				employeePaymentModel=data;
				$('#employeeDetailsIdId').val(id);
				$('#paymentName').html("<b>Name : </b>"+employeePaymentModel.name);
				$('#paymenttotalAmt').html("<b>Total Amount : </b>"+employeePaymentModel.totalsalary.toFixedVSS(2));
				$('#paymentamtPaid').html("<b>Amount Paid : </b>"+employeePaymentModel.amountPaid.toFixedVSS(2));
				$('#paymentamtRemaining').html("<b>Amount Pending : </b>"+employeePaymentModel.amountPending.toFixedVSS(2));
				
				$('.modal').modal();
				$('#pay').modal('open');
			}
		});	
} */
	
//refresh hrm table data 
function refreshHRM(){
	$('#deleteModals').empty();
	var t = $('#tblData').DataTable();
	    t.clear().draw();
	    
    var column = t.columns('.toggle');
    column.visible(true);
	
	$.ajax({
		type : "POST",
		url : myContextPath+"/fetchEmployeeListAjax",
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		async: false,
		success : function(data) {
			var modalData="";
			for(var i=0; i<data.length; i++){
				var empData=data[i];
				
				var actionCol='';
				if(empData.status == true){  
					actionCol='<a  class="btn-flat cursorPointer"><i class="material-icons tooltipped" area-hidden="true" data-position="left" data-delay="50"'+ 
			       'data-tooltip="Edit" >edit</i></a>'+ 
			       '<a href="#delete'+empData.employeePkId+'" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="left" '+
			       'data-delay="50" data-tooltip="Delete">delete</i></a>';
				}else{
					actionCol='<a href="'+myContextPath+'/fetchEmployee?employeeDetailsId='+empData.employeePkId+'" class="btn-flat">'+
				   '<i class="material-icons tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Edit" >edit</i></a> '+
				   '<a href="#delete'+empData.employeePkId+'" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="left" '+
				   'data-delay="50" data-tooltip="Delete">delete</i></a>';
				}
				
				var state=empData.status == true ?' disabled ':' ';
				
				//for display disable row in red colour (css is given in Style)
				var empName=empData.name;
				if(empData.status){
					empName+=" (Disabled)"
				}
				var employeeName='<span class="state" data-status="'+empData.status+'">'+empName+'</span>';
				
				t.row.add([				           
				           empData.srno,
				           empData.employeeId,
				           employeeName,
				           empData.mobileNumber,
				           empData.email,
				           empData.departmentName,
				           
						 /*'<a class="tooltipped" area-hidden="true"'+ 
						 'data-position="right" data-delay="50" data-tooltip="View Details"'+ 
						 'href="'+myContextPath+'/viewEmployeeSalary?employeeDetailsId='+empData.employeePkId+'">'+
						  empData.totalAmount.toFixedVSS(2)+
						 '</a>',
						 
						  empData.paidAmount,*/
			             '<a class="tooltipped" area-hidden="true"'+ 
						 'data-position="right" data-delay="50" data-tooltip="View Details"'+ 
						 'href="'+myContextPath+'/viewEmployeeSalary?employeeDetailsId='+empData.employeePkId+'">'+
						 empData.unPaidAmount.toFixedVSS(2)+
						 '</a>',
						 
						  empData.incentives,

						  '<button type="button" class="btn-flat"'+
						  state+
						 'onclick="openIncentivesModel('+empData.employeePkId+')"  id="incentiveDetails'+empData.srno+'">'+
						 '<i class="fa fa-inr tooltipped blue-text " data-position="left" data-delay="50" data-tooltip="Incentive">'+
						 '</i></button>',
						 
						 '<button type="button" class="btn-floating waves-effect waves-light tooltipped" area-hidden="true"'+ 
						 'data-position="left" data-delay="50" data-tooltip="Show Holidays"'+
					     'onclick="showHolidayDetail('+empData.employeePkId+')"  id="showholidayDetails'+empData.srno+'">'+
						  empData.noOfHolidays+
						 '</button>',
						 
						 '<button type="button" class="btn-flat tooltipped" area-hidden="true" data-position="left" '+
						 'data-delay="50" data-tooltip="Area details" onclick="showAreaDetail('+empData.employeePkId+')" '+
						 'style="font-size:10px;font-weight:800;" id="areaDetails'+empData.srno+'">'+
                         'Show'+
                         '</button>',
                         
                         '<button type="button" class="btn-flat" '+
                         state+
						 'onclick="openHolidayBookModel('+empData.employeePkId+')"  id="bookholidayDetails'+empData.srno+'">'+
                         '<i class="fa fa-asterisk tooltipped" aria-hidden="true"  data-position="right" data-delay="50" '+
                         'data-tooltip="Holiday" ></i>'+
                         '</button>',
                         
                         '<a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="right" data-delay="50" '+
					     'data-tooltip="Make payment" '+
					     'href="'+myContextPath+'/paymentEmployee?employeeDetailsId='+empData.employeePkId+'" > '+
	                     'Pay'+
	                     '</a>',
											 
						actionCol,
						
						'<input type="checkbox" class="filled-in chbclass" id="chb'+empData.employeePkId+'_'+empData.mobileNumber+'" />'+
						'<label for="chb'+empData.employeePkId+'_'+empData.mobileNumber+'"></label> '
						
						]).draw(false); 
				
				var stateDelAsk=empData.status == true ?'Enable' : 'Disable';
				var status = empData.status == true ? 'Do you  want to Enable?' : 'Do you want to Disable?';
				
				$('#deleteModals').append('<div id="delete'+empData.employeePkId+'" class="modal deleteModal row">'+
								                '<div class="modal-content  col s12 m12 l12">'+
								                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
								                    '<h5 class="center-align">'+
								                	status+
													'</h5>'+
								                    '<br/>'+
								                 '</div>'+
								                '<div class="modal-footer">'+
								 				    '<div class="col s6 m6 l3 offset-l3">'+
								         				'<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>'+
									    			'</div>'+
													'<div class="col s6 m6 l3">'+
									 				   '<a href="'+myContextPath+'/disableEmployee?employeeDetailsId='+empData.employeePkId+'"'+
								                       'class="modal-action modal-close waves-effect  btn">'+
								                       stateDelAsk+
								                       '</a>'+
									   				'</div>'+    
								    			'</div>'+
								            '</div>');
			}
			$('.modal').modal();
			
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			
		    column.visible(false);
		},
		error: function(xhr, status, error) {
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
		}
	});
}
