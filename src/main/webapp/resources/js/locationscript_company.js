var options, index, option;	
$(document).ready(function() {
						
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();
	
			            $('#addpincode').keypress(function(event){
			            	var key = event.which;
			        	    
			        	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
			        	        event.preventDefault();
			            });
						
						//  for no of entries and global search
						$('#tblDataArea').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},	
											{ "width": "17%", "targets": 1},	
							                { "width": "12%", "targets": 2},
							                { "width": "17%", "targets": 3},
							                { "width": "17%", "targets": 4},
							                { "width": "17%", "targets": 5},
							                { "width": "17%", "targets": 6},
							                { "width": "4%", "targets": 7}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
							 dom:'<lBfr<"scrollDivTable"t>ip>',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                    
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = [50,'*',50,70,60,60,50] 
					                    		 } 
					                    		    })
					                        
					                         /*for customize the pdf content*/ 
					                         doc.pageMargins = [5,20,10,5];
					                         
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						$('#tblDataRegion').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},	
											{ "width": "25%", "targets": 1},	
							                { "width": "25%", "targets": 2},
							                { "width": "25%", "targets": 3},
							                { "width": "25%", "targets": 4},	
							                { "width": "2%", "targets": 5}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					        
							 dom:'<lBfr<"scrollDivTable"t>ip>',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename: 'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = [50,'*','*','*','*'] 
					                    		 } 
					                    		    })
					                    	 
					                         /*for customize the pdf content*/ 
					                         doc.pageMargins = [5,20,10,5];
					                        
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});

						
						 $("select").change(function() {
			                    var t = this;
			                    var content = $(this).siblings('ul').detach();
			                    setTimeout(function() {
			                        $(t).parent().append(content);
			                        $("select").material_select();
			                    }, 200);
			                });
			            $('select').material_select();
			            $('.dataTables_filter input').attr("placeholder", "  Search");
						if ($.data.isMobile) {
							var table = $('#tblDataArea').DataTable(); // note the capital D to get the API instance
							var column = table.columns('.hideOnSmall');
							column.visible(false);

							var table1 = $('#tblDataRegion').DataTable(); // note the capital D to get the API instance
							var column1 = table1.columns('.hideOnSmall');
							column1.visible(false);

						
						}
												
												
												//saveCity
												$("#saveRegionSubmit").click(
														function() {
															//alert("go to save");
															//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
															if($("#cityListForRegion").val()==0)
															{
																Materialize.Toast.removeAll();
																Materialize.toast('Select City First For Save City!', '4000', 'teal lighten-2');
																/*$('#addeditmsg').modal('open');
																$('#msgHead').text("Error is : ");
																$('#msg').text("Select City First For Save City")*/;
																return false;
															}
															if($('#addRegion').val()==="")
															{
																Materialize.Toast.removeAll();
																Materialize.toast('Enter Region First For Save Region!', '4000', 'teal lighten-2');
																/*$('#addeditmsg').modal('open');
																$('#msgHead').text("Error is : ");
																$('#msg').text("Enter Area Name First For Save Area");*/
																return false;
															}
															
															var form = $('#saveRegionForm');

															$.ajax({

																		type : form.attr('method'),
																		url : form.attr('action'),
																		beforeSend: function() {
																			$('.preloader-background').show();
																			$('.preloader-wrapper').show();
																           },
																		/* dataType : "json", */
																		data : $("#saveRegionForm").serialize(),
																		success : function(data) {
																			
																				//alert("Country Saved ");
																				
																				//alert(data);
																				if(data=="Success")
																				{		
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																						$('#addeditmsg').modal('open');
																						$('#msgHead').text("Success : ");
																						
																						var regionId=$('#addRegionId').val();
																						if(regionId==0){
																							$('#msg').text("Region Saved SuccessFully");
																						}else{
																							$('#msg').text("Region Updated SuccessFully");
																						}	
																						
																						//$('#resetRegionSubmit').click();
																						
																						//reset region tab
																						$('#regionwork').click();
																				}
																				else
																				{
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																						$('#addeditmsg').modal('open');
																						$('#msgHead').text("Error : ");
																						$('#msg').text("Region "+data);
																				}
																				
																				
																				$('.preloader-background').hide();
																				$('.preloader-wrapper').hide();
																			
																		},
																	error: function(xhr, status, error) {
																		$('.preloader-wrapper').hide();
																		$('.preloader-background').hide();
																		  //alert(error +"---"+ xhr+"---"+status);
																		  Materialize.Toast.removeAll();
																		Materialize.toast('Something went wrong!', '4000', 'teal lighten-2');
																		/*$('#addeditmsg').modal('open');
												               	     	$('#msgHead').text("Message : ");
												               	     	$('#msg').text("Something Went Wrong"); 
												               	     		setTimeout(function() 
																			  {
												      	     					$('#addeditmsg').modal('close');
																			  }, 1000);*/
																		}
																	});
															return false;
														});
												
												$('#resetRegionSubmit').click(function() {
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
													
													var source1 = $("#cityListForRegion");
													var v1=0;
													source1.val(v1);
													source1.change();
													
													$('#addRegionId').val('0');
													$('#addRegion').val('');
													$("#saveRegionSubmit").html('<i class="material-icons left">add</i> Add');

													fetchallregion();
													
													$('.preloader-background').hide();
													$('.preloader-wrapper').hide();
												});
												
												$('#areawork').click(function() {
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
													
													$('#resetAreaSubmit').click();
													fetchCityList('cityListForArea');	
													
													$('.preloader-background').hide();
													$('.preloader-wrapper').hide();
												});
												
												
													/*$('#countryListForArea').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('stateListForArea');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose State", 0));
													$.ajax({
														url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryListForArea').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('stateListForArea');

															for (var i = 0, len = data.length; i < len; ++i) {
																var state = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(state.name, state.stateId));
															}
															
															if(editClicked){
																 var source1 = $("#stateListForArea");
																	var v=area.region.city.state.stateId;
																	source1.val(v);
																	source1.change();
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});
												
													$('#stateListForArea').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('cityListForArea');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose City", 0));
													$.ajax({
														url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateListForArea').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('cityListForArea');

															for (var i = 0, len = data.length; i < len; ++i) {
																var city = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(city.name, city.cityId));
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});*/
													
													
													$('#cityListForArea').change(function() {
														
														// Get the raw DOM object for the select box
														var select = document.getElementById('regionListForArea');

														// Clear the old options
														select.options.length = 0;
														//$('#countryListForState').html('');

														//Load the new options

														select.options.add(new Option("Choose Region", 0));
														$.ajax({
															url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityListForArea').val(),
															dataType : "json",
															beforeSend: function() {
																$('.preloader-background').show();
																$('.preloader-wrapper').show();
													           },
															success : function(data) {

																/* alert(data); */
																var options, index, option;
																select = document.getElementById('regionListForArea');

																for (var i = 0, len = data.length; i < len; ++i) {
																	var region = data[i];
																	//alert(state.name+" "+ state.stateId);
																	select.options.add(new Option(region.name, region.regionId));
																}
																
																/* for (index = 0; index < options.length; ++index) {
																  option = options[index];
																  select.options.add(new Option(option.name, option.cityId));
																} */
																$('.preloader-wrapper').hide();
																$('.preloader-background').hide();
															
															},
															error: function(xhr, status, error) {
																$('.preloader-wrapper').hide();
																$('.preloader-background').hide();
															}
														});		
														
													});

													//saveCity
													$("#saveAreaSubmit").click(function() {
														
																if($("#cityListForArea").val()==0)
																{
																	Materialize.Toast.removeAll();
																	Materialize.toast('Select City First For Save Area!', '4000', 'teal lighten-2');
																	/*$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select City First For Save Area");*/
																	return false;
																}
																//alert("go to save");
																//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
																if($("#regionListForArea").val()==0)
																{
																	Materialize.Toast.removeAll();
																	Materialize.toast('Select Region First For Save Area!', '4000', 'teal lighten-2');
																	/*$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select Region First For Save Area");*/
																	return false;
																}
																
																if($('#addArea').val()==="")
																{
																	Materialize.Toast.removeAll();
																	Materialize.toast('Enter Area Name First For Save Area!', '4000', 'teal lighten-2');
																	/*$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Enter Area Name First For Save Area");*/
																	return false;
																}
																
																if($('#addpincode').val()==="")
																{
																	Materialize.Toast.removeAll();
																	Materialize.toast('Enter Area Pincode First For Save Area!', '4000', 'teal lighten-2');
																	/*$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Enter Area Pincode First For Save Area");*/
																	return false;
																}
																
																var form = $('#saveAreaForm');

																$.ajax({

																			type : form.attr('method'),
																			url : form.attr('action'),
																			beforeSend: function() {
																				$('.preloader-background').show();
																				$('.preloader-wrapper').show();
																	           },
																			/* dataType : "json", */
																			data : $("#saveAreaForm").serialize(),
																					
																			success : function(data) {
																				
																					//alert("Country Saved ");
																					
																					//alert(data);
																					if(data=="Success")
																					{
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																							$('#addeditmsg').modal('open');
																							$('#msgHead').text("Success : ");
																							
																							var areaId=$('#addAreaId').val();
																							if(areaId==0){
																								$('#msg').text("Area Saved SuccessFully");
																							}else{
																								$('#msg').text("Area Updated SuccessFully");
																							}																							
																							//$('#resetAreaSubmit').click();
																							
																							//reset area tab
																							$('#areawork').click();
																					}
																					else
																					{
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
																						$('#addeditmsg').modal('open');
																						$('#msgHead').text("Error : ");
																						$('#msg').text("Area "+data);
																					}
																					
																						
																					$('.preloader-wrapper').hide();
																					$('.preloader-background').hide();
																				
																			},
																		error: function(xhr, status, error) {
																			$('.preloader-wrapper').hide();
																			$('.preloader-background').hide();
																			  //alert(error +"---"+ xhr+"---"+status);
																			//Materialize.toast('Something went wrong!', '4000', 'teal lighten-2');
																			/*$('#addeditmsg').find("#modalType").addClass("warning");
																			$('#addeditmsg').find(".modal-action").addClass("red lighten-2");
																			$('#addeditmsg').modal('open');
													               	     	$('#msgHead').text("Message : ");
													               	     	$('#msg').text("Something Went Wrong"); 
													               	     		setTimeout(function() 
																				  {
													      	     					$('#addeditmsg').modal('close');
																				  }, 1000);*/
																			
																			}
																		});
																return false;
															});
													
													$('#resetAreaSubmit').click(function() {
														
														$('.preloader-background').show();
														$('.preloader-wrapper').show();
														
														var source1 = $("#cityListForArea");
														var v1=0;
														source1.val(v1);
														source1.change();
														
														var source2 = $("#regionListForArea");
														var v2=0;
														source2.val(v2);
														source1.change();
														
														$('#addAreaId').val('0');
														$('#addArea').val('');
														$('#addpincode').val('');
														$("#saveAreaSubmit").html('<i class="material-icons left">add</i> Add');
												
														fetchallarea();
														
														$('.preloader-background').hide();
														$('.preloader-wrapper').hide();
													});
													$('#areawork').click();
													
					});

/*	function editCountry(id) {
		//alert(id);
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchCountry?countryId="+id,
			 data: "id=" + id + "&name=" + name, 
			success : function(data) {
				var user = data;
				$('#addCountry').focus();
				$('#addCountryId').val(user.countryId);
				$('#addCountry').val(user.name);
				$("#saveCountrySubmit").html('<i class="material-icons left">send</i> Update');
			}

		});
	}
	
	function editState(id) {
		//alert(id);
		$("#saveStateSubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchState?stateId="+id,
			 data: "id=" + id + "&name=" + name, 
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var state = data;
				$('#addState').focus();
				$('#addStateId').val(state.stateId);
				$('#addState').val(state.name);
				$('#addStateCodeId').val(state.code);
				$('#addStateCodeId').change();
				$('#saveStateSubmit').html("<i class='material-icons left'>send</i>Update");
				
				$("#countryListForState option").each(function()
						{
						 
						    	//$('#countryListForState option[value='+state.country.countryId+']').attr("selected",true);
						    	
						    	var source = $("#countryListForState");
								var v=state.country.countryId;
								source.val(v);
								source.change();
						    	
						    	 //remove selected one
						    	$('option:selected', 'select[name="options"]').removeAttr('selected');
						    	//Using the value
						    	$('select[name="options"]').find('option[value="3"]').attr("selected",true);
						    	 
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
						    	return false;
						    	
						});
			
			
			}

		});
	}
	
	function editCity(id) {
		//alert(id);
		$("#saveCitySubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchCity?cityId="+id,
			 data: "id=" + id + "&name=" + name, 
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var city = data;
				$('#addCity').focus();
				$('#addCityId').val(city.cityId);
				$('#addCity').val(city.name);		
				//alert(city.state.country.countryId);
		    	var source = $("#countryListForCity");
				var v1=city.state.country.countryId;
				source.val(v1);
				source.change();		
				//alert($("#countryListForCity").val());
			
				//alert(document.getElementById('stateListForCity').options.length);
				setTimeout(
						  function() 
						  {
						    //do something special
							  var source1 = $("#stateListForCity");
								var v=city.state.stateId;
								source1.val(v);
								source1.change();
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								//alert(v);
								//alert($("#stateListForCity").val());
						  }, 1000);
				
				
			}

		});
	}*/
	
	function editRegion(id) {
		//alert(id);
	
		$("#saveRegionSubmit").html('<i class="material-icons left">send</i> update');
	
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchRegion?regionId="+id,
			/* data: "id=" + id + "&name=" + name, */
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var region = data;
				$('#addRegion').focus();
				$('#addRegionId').val(region.regionId);
				$('#addRegion').val(region.name);								 
				
				
				setTimeout(function(){
						    //do something special
							  //alert("city");
							  var source2 = $("#cityListForRegion");
								var v2=region.city.cityId;
								source2.val(v2);
								source2.change();
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								//alert(v2);
								//alert($("#stateListForCity").val());
						  }, 1000);
				
				/*$('.preloader-background').hide();
				$('.preloader-wrapper').hide();*/
				
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			}

		});
	}

	
	function editArea(id) {
		
		//alert(id);
		$("#saveAreaSubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchArea?areaId="+id,
			/* data: "id=" + id + "&name=" + name, */
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var area = data;
				$('#addArea').focus();
				$('#addAreaId').val(area.areaId);
				$('#addArea').val(area.name);								 
				$('#addpincode').val(area.pincode);
				$('#addpincode').focus();
				$('#addArea').focus();
				//alert($('#addRegionId').val());
				
				var source2 = $("#cityListForArea");
				var v2=area.region.city.cityId;
				source2.val(v2);
				source2.change();
				
				setTimeout(function(){
						    //do something special
							 // alert("city");
							  var source3 = $("#regionListForArea");
								var v3=area.region.regionId;
								source3.val(v3);
								source3.change();
								//alert(v2);
								//alert($("#stateListForCity").val());
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								
						  }, 1000);
			
				/*$('.preloader-background').hide();
				$('.preloader-wrapper').hide();*/
				
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			}

		});
	}
	
	/*function fetchallstate()
	{
		var t = $('#tblDataState').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchStateList",
					 data: "id=" + id + "&name=" + name, 
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataState")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var state = data[i];
							t.row
									.add(
											[
													srno,
													state.name,
													state.code,
													state.country.name,
													"<button onclick='editState("+state.stateId+")' id='editstate' class='btn btn-flat'><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}
	
	function fetchallcity()
	{
		var t = $('#tblDataCity').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchCityList",
					 data: "id=" + id + "&name=" + name, 
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataCity")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var city = data[i];
							t.row
									.add(
											[
													srno,
													city.name,
													city.state.name,
													city.state.country.name,
													"<button onclick='editCity("+city.cityId+")' class='editcity btn btn-flat' ><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}*/
	

	function fetchallregion()
	{
		var t = $('#tblDataRegion').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchRegionList",
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataRegion")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var region = data[i];
							t.row
									.add(
											[
													srno,
													region.name,
													region.city.name,
													region.city.state.name,
													region.city.state.country.name,
													"<button onclick='editRegion("+region.regionId+")' class='editregion btn btn-flat' ><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();
						
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					}
				});	
		
	}
	
	
	function fetchallarea()
	{
		var t = $('#tblDataArea').DataTable();
		t.clear().draw();
		$.ajax({
					type : "GET",
					url : myContextPath+"/fetchAreaList",
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataArea")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var area = data[i];
							t.row
									.add(
											[
													srno,
													area.name,
													area.pincode,
													area.region.name,
													area.region.city.name,
													area.region.city.state.name,
													area.region.city.state.country.name,
													"<button onclick='editArea("+area.areaId+")' class='editarea btn btn-flat'><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();
						
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					}
				});	
		
	}
	
	function fetchCityList(taskForId){
		// Get the raw DOM object for the select box
		var select = document.getElementById(taskForId);

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("Choose City", 0));
		$.ajax({
			url : myContextPath+"/fetchCityListByCompanyId",
			dataType : "json", 
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			async:false, 
			success : function(data) {

				 //alert(data); 
				var options, index, option;
				select = document.getElementById(taskForId);

				for (var i = 0, len = data.length; i < len; ++i) {
					var city = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(city.name, city.cityId));
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			}
		});	
	}