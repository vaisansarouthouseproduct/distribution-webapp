 var productidlist = new Array();
 var count = 1;
 var userIdValid=false;  
 
$(document).ready(function() {
	//validation of add employee
	$.validator.setDefaults({
	       ignore: []
	}); 
	
	//$('select').change(function(){ $('select').valid(); });
	
	//user id check already exist or not
	jQuery.validator.addMethod("userIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkEmployeeDuplication(value,"userId");
	}, "User Id already in use"); 
	//email id check already exist or not
	jQuery.validator.addMethod("emailIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkEmployeeDuplication(value,"emailId");
	}, "Email Id Is already in use"); 
	//mobile number check already exist or not
	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkEmployeeDuplication(value,"mobileNumber");
	}, "Mobile Number Is already in use"); 
	//set form submit validation in jquery validate
	$('#saveEmployeeForm').validate({
		
		rules: {
			password: "required",
		    confirmpass: {
		      equalTo: "#password"
		    },		    
            userId:{
            	userIdCheck:true,
            	required:true
            },
            mobileNumber:{
            	mobileNumberCheck:true,
            	required:true
            },
            emailId:{
            	emailIdCheck:true
            }
		  },		
	
	   errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	
	
						 /* $('.preloader-background').hide();
						$('#Cnfrmpassword').keyup(function() {

				               var pass = $('#password').val();
				               var cpass = $('#Cnfrmpassword').val();

				               if (pass !== cpass) {
				                   // alert(cpass);
				                   $('#CnfrmpasswordLabel').text("Password not matched").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#CnfrmpasswordLabel').text("Password  matched").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
				           });*/
						
						/*$('#mobileNo').keyup(function() {
							var mobileNo = $('#mobileNo').val();
							//alert(mobileNo);
							if (mobileNo.length !== 10) {
				                   // alert(cpass);
				                   $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#mobileNumberlabel').text("Right").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
						});*/
	
						// This will be triggered everytime a user types anything
						// in the input field with id as input-field
						$("#userId").on('input', function() {
						    this.value = this.value.replace(/[^a-zA-Z0-9]/g, ''); //<-- replace all other than given set of values
						});
						
						/*$('#userId').on('keypress blur',function(){
							//alert('1');
							var userId=$('#userId').val().trim();
							//alert(userId);
							$.ajax({
								url : myContextPath+"/checkUserId?userId="+userId,
								dataType : "json",
								success : function(data) {
									
									input:not([type]).valid+label:after, 
									input:not([type]):focus.valid+label:after, 
									input[type=text].valid+label:after, 
									input[type=text]:focus.valid+label:after,
									
									if(data==true)
									{
										Materialize.toast('user id Available!', '2000', 'teal lighten-2');
										$("#useridvalid").attr("data-success", "UserId Available");
										$('#user	Id').css({"border-bottom":"1px solid green","box-shadow":"0 1px 0 0 green"});
										userIdValid=true;
									}
									else
									{
										//Materialize.toast('userid already in use!', '2500', 'teal lighten-2');
										$("#useridvalid").attr("data-success", "UserId Not Available");
										$("#useridvalid.active:after").css("color","red");
										$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"});
										userIdValid=false;
										
									}
								}
							});
							
						});*/
						
						//only number allowed
						$('#mobileNo').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						
						
						//before submit form event
						$('#saveEmployeeSubmit1').click(function(){
							var userId=$('#userId').val().trim();
						  // Our regex
						  // a-z => allow all lowercase alphabets
						  // A-Z => allow all uppercase alphabets
						  // 0-9 => allow all numbers
						  var regex = /[^a-zA-Z0-9]/g;
						  // This is will test the value against the regex
						  // Will return True if regex satisfied
						  if (regex.test(this.value) !== true){
							  //alert if not true
							  //alert("Invalid Input");

							  // You can replace the invalid characters by:
							   //this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '');
							   Materialize.Toast.removeAll();
							  Materialize.toast('UserId Allowed only A-Z or a-z or 0-9', '4000', 'teal lighten-2');
						  }
							  
							if(userIdValid==false)
							{
								Materialize.Toast.removeAll();
								Materialize.toast('userid already in use!', '4000', 'teal lighten-2');
								 /*$('#addeditmsg').modal('open');
							     $('#msgHead').text("UserId Message");
							     $('#msg').text("UserId Already Used..Change It");*/
							     return false;
							}
						/*	var mobileNo = $('#mobileNo').val();							
							if (mobileNo.length !== 10) {				                   
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Mobile Number Warning");
			           	     	$('#msg').text("Check Your Mobile Number");
			           	     	$('#mobileNo').focus();
			           	        $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
			           	     	return false;   
				              }
							
							var pass=$('#password').val();
							var confpass=$('#Cnfrmpassword').val();
							if(pass!==confpass)
							{
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Password Warning");
			           	     	$('#msg').text("Confirm password didn't Match With Password");
			           	        $("#useridvalid.active::after").attr("class","red-text");
								$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"});
								$('#userId').focus();
			           	     	return false;
							}
							
							var areaListIds=$('#areaListIds').val();
							if(areaListIds=='' || areaListIds==undefined)
							 {
							     $('#addeditmsg').modal('open');
							     $('#msgHead').text("Area Select Message");
							     $('#msg').text("Atleast one Area need to select");
							     return false;
							 }
							
							var departmentid=$('#departmentid').val();
							if(departmentid==0)
							{
								$('#addeditmsg').modal('open');
							     $('#msgHead').text("Deparment Select Message");
							     $('#msg').text("Select department for Employee");
							     return false;
							}*/
							
							
						});
						
						// on area select
						$("#areaList").on("change", function() {

			                var val = $("#areaList").val(); 
			                
			                /* $.each(productidlist, function(key, value) {
			                    //alert("product : "+key+"-"+value);
			                    if(if(jQuery.inArray(val, myarray) !== -1))
			                    {	return false;
			                    	$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Product Select Warning");
			               	     	$('#msg').text("This Product is already added");               	     	
			                    }
			              	}); */
			              	
			              	if(val==0)
			              	{
			              		return false;
			              	}
			              	//check area already added or not
			              	if(jQuery.inArray(val, productidlist) !== -1)
			                {	
								Materialize.Toast.removeAll();
			              		Materialize.toast('This Area is already added!', '4000', 'teal lighten-2');
			              		/*$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Area Select Warning");
			           	     	$('#msg').text("This Area is already added");        */       	     	
			                }
			              	else
			           		{
			              		//add selected area id in array
				                productidlist.push(val);
				                
				                //set area ids to areaListIds input
				             	$('#areaListIds').val(productidlist);
				                
				             	//selected area name 
				             	var text=$("#areaList option:selected").text();
				             	
				             	//selected area id
				                var vl=$("#areaList option:selected").val();
								
				                //length of area on select
				                var rowCount = $('#areatable tr').length;
				                
				                //set selected area in table
				                $("#t1").append("<tr id='rowdel_" + count + "' >"+
					               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
					               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
					               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
					               				"</tr>");
				                count++;
			           		}
			            });
						
						
						/*$('#countryList').change();
						
						$('#countryList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('stateList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose State", 0));
							$.ajax({
								url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryList').val(),
								dataType : "json",
								success : function(data) {

									 alert(data); 
									var options, index, option;
									select = document.getElementById('stateList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var state = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(state.name, state.stateId));
									}
									
									if(editClicked){
										 var source1 = $("#stateListForArea");
											var v=area.region.city.state.stateId;
											source1.val(v);
											source1.change();
									}
									
									 for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} 
								}
							});		
							
						});*/
						
						

						/*$('#stateList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('cityList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose City", ''));
							$.ajax({
								url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									 alert(data); 
									var options, index, option;
									select = document.getElementById('cityList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var city = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(city.name, city.cityId));
									}
									
									 for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} 
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									Materialize.toast('City List Not Found!', '4000', 'teal lighten-2');
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("City List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);
									}
							});		
							
						});*/
						
						
						//if city change then fetch region by city and 
						//refresh region list
						$('#cityList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('regionList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose Region", ''));
							$.ajax({
								url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									/* alert(data); */
									var options, index, option;
									select = document.getElementById('regionList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var region = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(region.name, region.regionId));
									}
									
									/* for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} */
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Region List Not Found!', '4000', 'teal lighten-2');
								/*	$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("Region List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});		
							
						});
						//when change region fetch area list by region id and
						//set region drop down
						$('#regionList').change(function() {
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('areaList');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options

							select.options.add(new Option("Choose Area", ''));
							$.ajax({
								url : myContextPath+"/fetchAreaListByRegionId?regionId="+$('#regionList').val(),
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {

									/* alert(data); */
									var options, index, option;
									select = document.getElementById('areaList');

									for (var i = 0, len = data.length; i < len; ++i) {
										var area = data[i];
										//alert(state.name+" "+ state.stateId);
										select.options.add(new Option(area.name, area.areaId));
									}
									
									/* for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} */
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
			    				},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									  Materialize.Toast.removeAll();
									Materialize.toast('Area List Not Found!', '4000', 'teal lighten-2');
							/*		$('#addeditmsg').modal('open');
			               	     	$('#msgHead').text("Message : ");
			               	     	$('#msg').text("Area List Not Found"); 
			               	     		setTimeout(function() 
										  {
			      	     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});		
							
						});
						
						
					});
/**
 * delete area table row by given id
 * and reset table data according productidlist array
 * @param id
 * @returns
 */
 function deleterow(id) {
	
    //alert('#rowproductkey_'+id);
    var removeItem=$('#rowproductkey_' + id).val();
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
    productidlist = jQuery.grep(productidlist, function(value) {
    	  return value != removeItem;
    	});
   // alert(productidlist);
    $('#areaListIds').val(productidlist);
   // alert(productidlist);
    var rowCount = $('#t1 tr').length;
	//alert(rowCount);
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!==i)
		{
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
       				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);

}
 /**
  * check employee info duplication
  * @param checkText
  * @param type
  * @returns
  */
 function checkEmployeeDuplication(checkText,type){
 	var status=false;
 	$.ajax({
 		url : myContextPath+"/checkEmployeeDuplicationForSave?checkText="+checkText+"&type="+type,
 		async:false,
 		success : function(data) {
 			if(data==="Success"){
 				status=true;
 			}else{
 				status=false;
 			}
 		},
 		error: function(xhr, status, error) {
 			alert("Error");
 		}
 	});
 	
 	return status;
 }