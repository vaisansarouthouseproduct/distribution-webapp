<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/additional-method.js"></script>
<script type="text/javascript">

var imageSize=false;
$(document).ready(function() {
			
			$.validator.setDefaults({
			       ignore: []
			});
			
			//image file size validation
			$.validator.addMethod('filesize', function (value, element, param) {
			    return this.optional(element) || (element.files[0].size <= param)
			}, 'File size must not exceed 10kb');

			//$('select').change(function(){ $('select').valid(); });
			$('#saveChequeBookForm').validate({
				 rules:{
						file:{
							extension: "jpg,jpeg,png",
							  filesize: 10000,
						},					
	            
					},
				messages:{
					file:{
						extension:'Upload only png,jpg or jpeg format'
					}
				},
			    errorElement : "span",
			    errorClass : "invalid error",
			    errorPlacement : function(error, element) {
			      var placement = $(element).data('error');
			    
			      if (placement) {
			        $(placement).append(error)
			      } else {
			        error.insertAfter(element);
			      }
			      $('select').change(function(){ $('select').valid(); });
			    }
			  });
			
			
			$("#noOfLeaves").keyup(function(){
				
				var noOfLeaves=parseFloat($("#noOfLeaves").val());
				
				$("#noOfLeaves").focus();
			});
			var msg="${saveMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
				 $('#addeditmsg').find("#modalType").addClass("success");
				 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
			     $('#addeditmsg').modal('open');
			     /* $('#head').text("Product Adding Message"); */
			     $('#msg').text(msg);
			 }
				
		          //only number allowed without decimal
		          $('#noOfLeaves').keypress(function( event ){
					    var key = event.which;
					    
					    if( ! ( key >= 48 && key <= 57 || key === 13) )
					        event.preventDefault();
					});
		        //only number allowed without decimal
				  $('#startingChqNo').keypress(function( event ){
					    var key = event.which;
					    
					    if( ! ( key >= 48 && key <= 57 ) )
					        event.preventDefault();
					});
		      
				  
				 /*  $('#endChequeNo').keypress(function( event ){
					    var key = event.which;
					    
					    if( ! ( key >= 48 && key <= 57 ) )
					        event.preventDefault();
					});
				   */
			
				//on Bank Account  change fetch template design list
					$("#bankAccountList").change(function(){
						var bankId=$("#bankAccountList").val().split('~')[0];
						
						var select = document.getElementById('bankCBTemplateList');
						
						// Clear the old options
						select.options.length = 0;
						select.options.add(new Option("Choose Cheque Template Design", '0'));
							$.ajax({
								type : "GET",
								url :"${pageContext.request.contextPath}/fetchBankCBTemplateDesignByBankId?bankId="+bankId,
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
						           success : function(data) {
						        	   
						        	   for(var i=0; i<data.length; i++)
										{				
						        		   var bankCBTemplate = data[i];
											//alert(data[i].productId +"-"+ data[i].productName);
											//$("#bankCBTemplateId").append('<option value='+data[i].id+'>'+data[i].templateName+'</option>');
						        		   select.options.add(new Option(bankCBTemplate.templateName, bankCBTemplate.id));
										}	
						        	   
						        	  
						        	 
						        	   //console.log(CategoryIgst);
						        	   $('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										$("#productRate").keyup();
						           },
						           error: function(xhr, status, error) {
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										Materialize.Toast.removeAll();
										Materialize.toast('Something Went Wrong!', '2000', 'red lighten-2');
										/* $('#addeditmsg').modal('open');
					           	     	$('#head').text("Warning: ");
					           	     	$('#msg').text("Something Went Wrong"); 
					           	     		setTimeout(function() 
											  {
					  	     					$('#addeditmsg').modal('close');
											  }, 1000); */
										}						
						           
							});			
					});
				   
					  
				   $('#noOfLeaves').on('blur',function(){
					   $("#endChequeNo").val("");
					   var startChqNo=$("#startingChqNo").val();
					   var noOfLeaves=$("#noOfLeaves").val();
					   if(noOfLeaves==""){
						   noOfLeaves=0;
						   Materialize.Toast.removeAll();
						   Materialize.toast(" Please Enter No of Leaves", '2000', 'red lighten-2');
						   return false
					   }else{
						   noOfLeaves=parseFloat(noOfLeaves);
					   }
					   if(startChqNo!=""){
						   startChqNo=parseFloat(startChqNo);
						   var lastChequeNo=startChqNo+noOfLeaves-1;
						   if(lastChequeNo>999999){
							Materialize.Toast.removeAll();
							   Materialize.toast(" Inavalid Start Cheque Number", '2000', 'red lighten-2');
							   return false;
						   }
						   
						   
					        var lastchqNoString=lastChequeNo.toString();
						   
						   while(lastchqNoString.length!=6){
					        	lastchqNoString="0"+lastchqNoString;
					        }
						   
						  $("#endChequeNo").val(lastchqNoString);
						   
					   }else{
						Materialize.Toast.removeAll();
						   Materialize.toast('Enter Start Cheque Number', '2000', 'red lighten-2');
					   }
				   });
				   
				   $('#startingChqNo').on('blur',function(){
					   var startChqNo=parseFloat($("#noOfLeaves").val().trim());
					   if(startChqNo=""){
						   $('#msgEndChqValidation').html("Please Enter Start Cheque No)");
							return false;  
					   }
				   });
			        // validating end Cheque No
			        $('#startingChqNo').on('blur',function(){
			        	$("#endChequeNo").val("");
			        	var startChqNo=$("#startingChqNo").val();
						   var noOfLeaves=$("#noOfLeaves").val();
						   if(startChqNo==""){
							return false;
						   }
						   if(startChqNo.length!=6){
							Materialize.Toast.removeAll();
							   Materialize.toast(" Inavalid start cheque number", '2000', 'red lighten-2');
							   return false;
					       }
						   if(noOfLeaves!=""){
							   startChqNo=parseFloat(startChqNo);
							   noOfLeaves=parseFloat(noOfLeaves);
							   var lastChequeNo=startChqNo+noOfLeaves-1;

							  if(lastChequeNo>999999){
								Materialize.Toast.removeAll();
								  Materialize.toast(" Inavalid Data(Please Check Start cheque No or No of leaves)", '2000', 'red lighten-2');
							  }else{
								  
							        var lastchqNoString=lastChequeNo.toString();
							        while(lastchqNoString.toString().length!=6){
							        	lastchqNoString="0"+lastchqNoString;
							        }
								   
								  $("#endChequeNo").val(lastchqNoString);
							  }
							   
						   }else{
							Materialize.Toast.removeAll();
							   Materialize.toast('Enter no of leaves', '2000', 'red lighten-2');
						   }
						
						  
			        });
		});

</script>
<style>
	#upload-error{
		display:block;
	}
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/saveChequeBook" method="post" id="saveChequeBookForm"  enctype="multipart/form-data">
                <div class="row  z-depth-3">
                    <br>
                    <div class="row" style="margin-bottom:0">
				
					
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">

                        <i class="material-icons prefix">star<span class="red-text">*</span></i>
                      <!--   <label for="brandid" class="active"></label> -->
                        <select id="bankAccountList" name="bankAccountId" required="" aria-required="true">
                                 <option value="" selected>Choose Account No.</option>
                                <c:if test="${not empty bankAccountList}">
							<c:forEach var="listValue" items="${bankAccountList}">
								<option value="<c:out value="${listValue.bank.bankId}~${listValue.id}" />"><c:out
										value="${listValue.bank.bankName}-${listValue.accountNumber}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">
                        <i class="material-icons prefix">view_comfy<span class="red-text">*</span></i>
                        <select id="bankCBTemplateList" name="bankCBTemplateId" required="" aria-required="true">
                                 <option value="" selected disabled>Choose Cheque Design Template</option>
                               <%--  <c:if test="${not empty bankCBTemplateDesignList}">
							<c:forEach var="listValue" items="${bankCBTemplateDesignList}">
								<option value="<c:out value="${listValue.id}" />"><c:out
										value="${listValue.templateName}" /></option>
							</c:forEach>
						</c:if> --%>
                        </select>
                    </div>

                    
                     </div>
                   
                  
                    <div class="row">
                   
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">rate_review</i>
                        <input id="noOfLeaves" type="text" class="validate" name="noOfLeaves" maxlength="3" required>
                        <label for="noOfLeaves" class="active"><span class="red-text">*</span>Enter No of Leaves</label>
                    </div>
                   
                   <div class="input-field col s12 m5 15 push-l1 push-m1">
	                   <i class="material-icons prefix">date_range</i>
						<input type="date" class="datepicker chequeDate"
						placeholder="Due Date" name="chequeDate" id="dueDate">
						<label for="dueDate">Added Date <span class="red-text">*</span>
						</label>
					</div>
                 	
                      
                       <div class="input-field col s12 m5 l5 push-l1 push-m1">                    
                        <i class="material-icons prefix">rate_review</i>
                        <input id="startingChqNo" type="text" class="validate" name="startingChqNo"  minlength="6" maxlength="6" required>
                        <label for="startingChqNo" class="active"><span class="red-text">*</span>Enter Starting Cheque No.</label>
                    </div>       
                  
                   	<div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix"> rate_review</i>
                        <input id="endChequeNo" type="text" class="validate" name="endChequeNo" minlength="6" maxlength="6" readonly>
                        <label for="endChequeNo" class="active">End Cheque No.</label>
                    </div>
                    
                  </div>
                    <br>
					  
                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveChequeBookSubmit">Add Cheque Book<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>

                </div>
                
                			<!-- <div class="col s12 l12 m12">                                
                               <h6 class="center"><font color='red'><span id="msgEndChqValidation"></span></font></h6> 
                        	</div> -->


            </form>
        </div>
        
     
    	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>    
    </main>
    <!--content end-->
</body>

</html>