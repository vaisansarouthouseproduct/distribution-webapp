<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
	<script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resources/js/employeeview.js"></script>
	
	<!-- for find date different -->
	<script src="resources/js/moment.min.js"></script>
	<!-- 
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
	<script>
	    if (!window.moment) { 
	        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
	    }
	</script>
	 -->
	 
	 	


    <script>
        $(document).ready(function() {
            $("#BankDetails").css("display", "none");
            $(".cash").change(function() {
                $("#BankDetails").css("display", "none");
            });
            $(".cheque").change(function() {
                $("#BankDetails").css("display", "block");
            });
            $(".showDates1").hide();
            $(".rangeSelect1").click(function() {
                $(".showDates1").show();
            });
            var msg="${saveMsg}";
       	 //alert(msg);
       	 if(msg!='' && msg!=undefined)
       	 {
       		 $('#addeditmsg').find("#modalType").addClass("success");
 			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
       	     $('#addeditmsg').modal('open');
       	     /* $('#msgHead').text("HRM Message"); */
       	     $('#msg').text(msg);
       	 }
       	 
            var table = $('#tblData').DataTable();
			 table.destroy();
			 $('#tblData').DataTable({
		         "oLanguage": {
		             "sLengthMenu": "Show _MENU_",
		             "sSearch": " _INPUT_" //search
		         },		    
		         autoWidth: false,
		          columnDefs: [
		                      { 'width': '1%', 'targets': 0},
		                      { 'width': '2%', 'targets': 1},
		                      { 'width': '15%', 'targets': 2},
		                      { 'width': '2%', 'targets': 3},
		                      { 'width': '3%', 'targets': 4},
		                      { 'width': '2%', 'targets': 5},
		                      /* { 'width': '1%', 'targets': 6},
		                      { 'width': '1%', 'targets': 7}, */
		                      { 'width': '2%', 'targets': 6},
		                      { 'width': '1%', 'targets': 7 },
		                      { 'width': '1%', 'targets': 8 },
		                      { 'width': '1%', 'targets': 9},
		                      { 'width': '1%', 'targets': 10 },
		                      { 'width': '1%', 'targets': 11},
		                      { 'width': '2%', 'targets': 12},
		                      { 'width': '2%', 'targets': 13,"orderable": false},
		                      { 'width': '1%', 'targets': 14}
		                      ], 
		     
		         lengthMenu: [
		             [10, 25., 50, -1],
		             ['10 ', '25 ', '50 ', 'All']
		         ],
		         
		         
		         "createdRow": function ( nRow, data, index ) {
                	 var status=nRow.childNodes[2].childNodes[0].attributes[1].value;
                     status=JSON.parse(status);
                	 if ( status==true )
                     {
                         $(nRow).addClass('disable');
                     }
                     else if (status==false)
                     {
                         
                     } 
                 },
		         
		       
		         //dom: 'lBfrtip',
		         dom:'<lBfr<"scrollDivTable"t>ip>',
		         buttons: {
		             buttons: [
		                 //      {
		                 //      extend: 'pageLength',
		                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
		                 //  }, 
		                 {
		                     extend: 'pdf',
		                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: '.print-col'
		                     },
		                     customize: function(doc, config) {
		                    	 doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [50,70,100,80,80,72,72,72,72,80] 
		                    		 } 
		                    		    })
		                         var tableNode;
		                         for (i = 0; i < doc.content.length; ++i) {
		                           if(doc.content[i].table !== undefined){
		                             tableNode = doc.content[i];
		                             break;
		                           }
		                         }
		        
		                         var rowIndex = 0;
		                         var tableColumnCount = tableNode.table.body[rowIndex].length;
		                          
		                         if(tableColumnCount > 6){
		                           doc.pageOrientation = 'landscape';
		                         }
		                         console.log(doc);
		                         /*for customize the pdf content*/ 
		                         doc.pageMargins = [5,20,10,5];
		                        
		                         doc.defaultStyle.fontSize = 8	;
		                         doc.styles.title.fontSize = 12;
		                         doc.styles.tableHeader.fontSize = 11;
		                         doc.styles.tableFooter.fontSize = 11;
		                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
		                       },
		                 },
		                 {
		                     extend: 'excel',
		                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'print',
		                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'colvis',
		                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
		                     collectionLayout: 'fixed two-column',
		                     align: 'left'
		                 },
		             ]
		         }

		     });
			 $("select")
               .change(function() {
                   var t = this;
                   var content = $(this).siblings('ul').detach();
                   setTimeout(function() {
                       $(t).parent().append(content);
                       $("select").material_select();
                   }, 200);
               });
           $('select').material_select();
           $('.dataTables_filter input').attr("placeholder", "Search");
           if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
							}
           var table = $('#tblData').DataTable(); // note the capital D to get the API instance
           var column = table.columns('.toggle');
           column.visible(false);
           $('#showColumn').on('click', function () {
           	 //console.log(column);
           	 column.visible(!column.visible()[0] );
         
           });
         /* // Use "DataTable" with upper "D"
            oTableStaticFlow = $('#tblData').DataTable({
                "aoColumnDefs": [{
                    'bSortable': true,
                    'aTargets': [0]
                }],
            });

            $("#checkAll").click(function () {
            	var colcount=oTableStaticFlow.columns().header().length;
                var cells = oTableStaticFlow.column(colcount-1).nodes(), // Cells from 1st column
                    state = this.checked;
				
                for (var i = 0; i < cells.length; i += 1) {
                    cells[i].querySelector("input[type='checkbox']").checked = state;
                }
            }); */


            
             

        });
    </script>	
    <style>
	.noborder>td{
	border:none !important;
	border-bottom:none !important;
	border-left:none !important;
	/* empty-cells: hide; */
}

tr.noborder:first-child th:first-child, tr.noborder td:first-child{
border-left:none !important;
}
#modalTable_wrapper div.dt-buttons{
	margin-left: 20% !important;
}
	.btn-flat:focus,
	.btn-flat:hover {
    background-color: transparent !important;
    box-shadow: none;
}
	/* table.dataTable tbody td {
    padding: 3px 2px !important;
} */
	
	#incentive .input-field{
		margin-top:10px !important;
	}
	@media only screen and (min-width:992px){
        #incentive{
		width:35% !important;
		height:55%;
		border-radius:5px;
	}
	#area{
		width:37% !important;
	}
    }
    @media only screen and (min-width:600px){
        #incentive{
		width:40% !important;
        height: 30% !important;
		border-radius:5px;
	}
    }
     @media only screen and (min-width:601px) and (max-width:992px){
     
     	#incentive{
				width:40% !important;
		        height: 40% !important;
				border-radius:5px;
		}
     }
    @media only screen and (max-width:600px){
        #area{
	width:95% !important;
}
#modalTable_wrapper div.dt-buttons{
	margin-left: 0 !important;
}
    }
	textarea.materialize-textarea {
    overflow-y: hidden;
    resize: none;
    height:2.5rem;
}


span.error {
    color: red;
    display: "inline";
    margin-top: 0 !important;
    margin-left: 0;
    padding: 0;
    font-size: 0.9em;
}

	/* .datepicker table,
	.datepicker td,
	.datepicker th{
	border: none !important; 
     font-size: 13px; 
    }  */
    #reason{
        height: 3rem !important;
    }
    
    
    #tblData tr.disable{
		background-color:#ffcdd2;
	}
	
	</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       
        <div class="row noBottomMarginOnLarge">
          <br>
            <!--Add Employee-->
            <div class="col s6 l3 m6 left">
                <a class="btn waves-effect waves-light blue-gradient left" href="${pageContext.request.contextPath}/addEmployee"><i class="material-icons left" >add</i>Add Employee</a>
            </div>
            <!--Add Employee-->
            <div class="col s6 l3 m6 right right-align">
                <a class="btn waves-effect waves-light blue-gradient right-align" href="${pageContext.request.contextPath}/addRoute"><i class="material-icons left" >add</i>Add Routes</a>
            </div>
            <!--Add Business Type-->
            <!-- <div class="col s10 l4 m6 offset-s2 ">
                <a class="btn waves-effect waves-light blue darken-blue-gradient center" href="AddBusinessType.html"><i class="material-icons left" >add</i>Add Business Type</a>
            </div> -->
            <!--Add Department-->
           <%--  <div class="col s10 l4 m6  right">
                <a class="btn waves-effect waves-light blue-gradient right" href="${pageContext.request.contextPath}/fetchDepartmentList"><i class="material-icons left" >add</i>Add Department</a>
            </div> --%>
       </div>

  <div class="row">
        <div class="col s12 l12 m12">
        
        
      
            <table class="striped highlight centered" id="tblData"  width="100%">
                <thead>
                    <tr>
                        <th class="print-col hideOnSmall">Sr. No.</th>
                        <th class="print-col toggle">Employee Id</th>
                        <th class="print-col" style="min-width:200px;">Name</th>
                        <th class="print-col">Mobile No.</th>
                        <th class="print-col toggle">EmailId</th>
                        <th class="print-col">Department</th>
                      <!--   <th class="print-col" >Total Amt</th>
                        <th class="print-col">Paid Amt</th> -->
                        <th class="print-col">Balance Amt</th>
                        <th class="print-col">Incentives Amt</th>
                        <th class="toggle">Give Incentive</th> 
                        <th class="hideOnSmall">No Of Holidays</th>                       
                        <th>Area</th>
                        <th class="toggle">Marks as Holiday</th>
                        <th>Make Payment</th>
                        <th>Action <br/><a  id="showColumn" class="tooltipped cursorPointer" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Show more coloumns"><i class="material-icons black-text">swap_horiz</i></a></th>
                        <!-- <th>Disable</th> -->
                        <th class="hideOnSmall"> <a class="modal-trigger tooltipped cursorPointer" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Send SMS" id="sendsmsid" style="margin-left:-22%;"><i class="material-icons">mail</i></a>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
                            <label for="checkAll"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
                 <%-- <c:if test="${not empty employeeViewModelList}">
							<c:forEach var="listValue" items="${employeeViewModelList}">													
                  	  <tr ${listValue.status == true ? 'class="red lighten-4"' : ''}>
                        <td><c:out value="${listValue.srno}"/></td>
                        <td><c:out value="${listValue.employeeId}"/></td>
                        <td class="wrapok"><c:out value="${listValue.name}"/></td>
                        <td><c:out value="${listValue.mobileNumber}"/></td>
                        <td><c:out value="${listValue.email}"/></td> 
                        <td><c:out value="${listValue.departmentName}"/></td>
                        <td>                      
	                        <a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.request.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.employeePkId}">
	                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" />
	                        </a>	                       
                        </td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.paidAmount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.unPaidAmount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.incentives}" /></td>                      	
                        <td>
                            <button type="button" class="btn-flat"  ${listValue.status == true ? 'disabled="disabled"' : ''} onclick="openIncentivesModel('${listValue.employeePkId}')"  id="incentiveDetails${listValue.srno}"><i class="fa fa-inr tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Incentive"></i></button>
                        </td>
                          
                        <td>
                            <button type="button" class="btn-floating waves-effect waves-light tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Show Holidays" onclick="showHolidayDetail('${listValue.employeePkId}')"  id="showholidayDetails${listValue.srno}"><c:out value="${listValue.noOfHolidays}"/></button>
                        </td>
                       
                        <td>
                            <button type="button" class="btn-flat tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Area details" onclick="showAreaDetail('${listValue.employeePkId}')"  style="font-size:10px;font-weight:800;" id="areaDetails${listValue.srno}">
                            Show
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn-flat" ${listValue.status == true ? 'disabled="disabled"' : ''} onclick="openHolidayBookModel('${listValue.employeePkId}')"  id="bookholidayDetails${listValue.srno}">
                            	<i class="fa fa-asterisk tooltipped" aria-hidden="true"  data-position="right" data-delay="50" data-tooltip="Holiday" ></i>
                            </button>
                        </td>

                        <td>
                        <button type="button" class=" btn-flat blue-text" onclick="openPaymentModel('${listValue.employeeId}')"  id="paymentDetails${listValue.srno}">
                        Pay
                        </button>
                        <a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Make payment" href="${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId=${listValue.employeePkId}" >
                        Pay
                        </a>
                        </td>

					<c:choose>  
					    <c:when test="${listValue.status == true}">  
					       <td><a  class="btn-flat cursorPointer"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a> 
					       <a href="#delete${listValue.employeePkId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
					    </c:when>  
					    <c:otherwise>  
					       <td><a href="${pageContext.request.contextPath}/fetchEmployee?employeeDetailsId=${listValue.employeePkId}" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a> 
					       <a href="#delete${listValue.employeePkId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
					    </c:otherwise>  
					</c:choose>  

                        
                        <!-- Modal Trigger -->
                        <td><a href="#delete${listValue.employeePkId}" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
                        <td>
                            <input type="checkbox" class="filled-in chbclass" id="chb${listValue.employeePkId}_${listValue.mobileNumber}" />
                             <label for="chb${listValue.employeePkId}_${listValue.mobileNumber}"></label> 
                        </td>
                    </tr>
                    <!-- Modal Structure for delete -->

                    <div id="delete${listValue.employeePkId}" class="modal deleteModal row">
                        <div class="modal-content  col s12 m12 l12">
                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
                            <h5 class="center-align">${listValue.status == true ? 'Do you  want to Enable?' : 'Do you want to Disable?'}</h5>
                            <br/>
                            </div>
                        <div class="modal-footer">
            
         				   <div class="col s6 m6 l3 offset-l3">
                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                			</div>
            				<div class="col s6 m6 l3">
             				   <a href="${pageContext.request.contextPath}/disableEmployee?employeeDetailsId=${listValue.employeePkId}" class="modal-action modal-close waves-effect  btn">${listValue.status == true ? 'Enable' : 'Disable'}</a>
               				 </div>
                
            			</div>
                    </div>
                 </c:forEach>
                 </c:if> --%>
                 </tbody>
                 </table>
                 <div id="deleteModals">
                 </div>
                     <!-- Modal Structure for Holiday  booked-->
       			 <div id="holiday" class="modal row">
         	       <div class="modal-content">  
         	             
                    <h5 class="center"><u>Mark As Holiday</u><i class="modal-close material-icons right">clear</i></h5>
          
                    
                    <form action="${pageContext.request.contextPath}/bookHoildays" method="post" class="col s12 l12 m12" id="saveHoliday">
                     <input type="hidden" name="employeeDetailsId" id="employeeIdForHolidaySave" value="">
                     <input type="hidden" name="employeeHolidaysId" id="employeeHolidaysId" value="0">
                        <div class="row" style="margin-bottom:0">
                            <div class="input-field col s12 l4 m4">
                              <input id="holiodaydetailsname" type="text" value="." readonly class="grey lighten-3">
                                <label for="holiodaydetailsname" class="active black-text">Name</label>
                                <!-- <h6 id="holiodaydetailsname"></h6> -->
                            </div>
                            <div class="input-field col s12 l4 m4">
                             <input id="holiodaydetailsmobilenumber" type="text" value="." readonly class="grey lighten-3">
                                <label for="holiodaydetailsmobilenumber" class="active black-text">Mobile No</label>
                                <!--<input id="image_title" type="text" class="validate">-->
                                <!-- <h6 id="holiodaydetailsmobilenumber">Mobile No:</h6> -->
                            </div>
                             <div class="input-field col s12 l3 m3">
                                <input id="holiodaydetailsnoofholidays" type="text" name="noOfDays">
                                <label for="holiodaydetailsnoofholidays" class="black-text"><span class="red-text">*</span>No of holidays</label>
                            </div>                             
                        </div>
                        <div class="row" style="margin-bottom:10px">                                                    
                            <div class="input-field col s12 l4  m4 offset-l4 offset-m3" id="oneDateDiv">                            	
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="oneDate" class="datepicker disableDate" name="oneDate">
                                <label for="oneDate" class="black-text"><span class="red-text">	*</span>One Date</label>
                            </div>
                           
                            <div class="input-field col s12 l4  m4" id="fromDateDiv">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="fromDate" class="datepicker " name="fromDate">
                                <label for="fromDate" class="black-text"><span class="red-text">*</span>From Date</label>
                            </div>
                            <div class="input-field col s12 l4 m4" id="toDateDiv">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="toDate" class="datepicker " name="toDate">
                                <label for="toDate" class="black-text"><span class="red-text">*</span>To Date</label>
                            </div>
                        </div>
                        <div class="row">
									<div class="input-field col s12 l4  m4">
										<select id="paidStatusId" name="paidStatus" >
											<option value="0" selected>Select  Type</option>
											<option value="paid">Paid</option>
											<option value="unpaid">Unpaid</option>
										</select> 
										<label><span class="red-text">*</span>Type Of Holiday</label>

									</div>
									<div class="input-field col s12 l4  m5">
										<textarea id="reason" class="materialize-textarea validate" name="reason"></textarea>
										<label for="reason" class="active black-text"><span class="red-text">*</span>Reason</label>
									</div>
								</div>
								<div class="col s12 l12 m12">                                
                               <h6 class="center"><font color='red'><span id="msgholidaysave"></span></font></h6> 
                        		</div>
                    </form>
               
          	  </div>
            <div class="modal-footer">
            <div class="col s12 m12 l12 divider"></div>
            <div class="col s12 m12 l12 center">
                <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                <button id="submitHoliday" type="button" class="modal-action waves-effect  btn">Submit</button>
                </div>
            	
            </div>

        </div>
        
        
                  
                    
                    
                    <!-- Modal Structure for sendMsg -->
                   
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMS" method="post">
                         <div id="sendMsg" class="modal">
                    
                        <div class="modal-content">                                   	
                        
                         
                           <div class="fixedDiv">
                              <div class="center-align" style="padding:0">
                         		<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
                         	  </div>
                         	  </div>
                         	  
                         	  <div class="scrollDiv">
                         	  
                         	 <div class="row mobileDiv" style="margin-bottom:0">
                         	  	<div class="col s12 l9 m9 input-field" style="padding-left:0">
                                    <label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
                                    <input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" readonly>
                                    	
                               </div>  
                                 <div class="col s12 l2 m2 right-align" style="margin-top:10%">
                                    <input type="checkbox" id="mobileEditSms"/>
                                    <label for="mobileEditSms" class="black-text">Edit</label>
                                    
                                </div>
                               </div>
                                <div class="input-field">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
                                </div> 
                                <div class="input-field center">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>
            					<div class="fixedFooter">
            					 <div class="col s6 m4 l3 right-align" style="margin-left:3%">
                            		 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
                                 </div>
		                          	<div class="col s6 m6 l2">	
		                           		<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
		                           			 
		                           			
		                             </div>
		                         </div>
		                           </div>  	
		                          <br><br><br>
                         	  
                         	                             
                            </div>
                            
                               				
                        </div>
                        </form>
           
                    
                    
                    
                    

                   <%--  <div id="sendMsg" class="modal modal-fixed-footer" style="width:40%;height:60%;">
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMS" method="post">
                        <div class="modal-content">
                            <h4 class="center">Message</h4>
                            <hr>	
                            <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                    <label for="smsText" class="black-text"><span class="red-text">*</span>Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" name="smsText"></textarea>
                                </div> 
                                <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>                            
                            </div>
                               				
                        </div>
                        <div class="modal-footer center-align row">
                            <div class="col s6 m6 l6">
                            		 <button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                        </form>
                    </div> --%>
                    
                </tbody>
            </table>
        </div>
        
       </div>
        
        <!--Modal Structure for payment-->
        <div id="pay" class="modal ">
            <div class="modal-content">
                <div class="row  ">
                    <form  action="${pageContext.request.contextPath}/givePayment" method="post" class="col s12 l12 m12" id="savePayment" class="col s12 l10 m8 push-l1">
                        <input name="employeeDetailsId" type="hidden" id="employeeDetailsIdId" class="cash" checked/>
                        <h5 class="center"><u>Payment Details</u></h5>
                        
                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <h6 id="paymentName">Name:</h6>
                            </div>
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <!--<input id="image_title" type="text" class="validate">-->
                                <h6 id="paymenttotalAmt">Total Salary:</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <h6 id="paymentamtPaid">Amount Paid: </h6>
                            </div>
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <h6 class="left" id="paymentamtRemaining">Amount Remaining: </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <div class="col s12 l6 push-l1">

                                    <input name="group1" type="radio" id="paymentCash" class="cash" checked/>
                                    <label class="left" for="paymentCash">Cash</label>
                                </div>
                                <div class="col s12 l6 push-l6">

                                    <input name="group1" type="radio" id="paymentCheque" class="cheque" />
                                    <label class="right" for="paymentCheque">Cheque</label>

                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 l5 push-l2 m5 push-m1">
                            <!--<i class="material-icons prefix">mode_edit</i>-->
                            <input id="paymentamount" type="text" name="amount">
                            <label for="paymentamount" class="active">Enter Amount:</label>
                        </div>
                        <div id="BankDetails">

                            <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                <input id="bankName" type="text" name="bankName">
                                <label for="bankName" class="active">Enter Bank Name:</label>
                            </div>
                            <br>
                            <div class="col s12 m6 l12 ">
                                <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                    <input id="cheqNo" type="text" name="cheqNo">
                                    <label for="cheqNo" class="active">Enter Cheque No.:</label>
                                </div>
                                <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                    <input id="cheqDate" type="text" class="datepicker" name="cheqDate">
                                    <label for="cheqDate">Enter Cheque Date:</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 l8 push-l2 m5 push-m1">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="paymentcomment" class="materialize-textarea" name="comment"></textarea>
                            <label for="paymentcomment">Comment</label>
                        </div>
							<div class="input-field col s12 l5 push-l4 m5 push-m4 ">                                
                   			  <font color='red'><span id="msgpaymentsave"></span></font>
            				</div>
                    </form>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="savePaymentId" class="modal-action waves-effect  btn">Pay</button>
                <a class="modal-action modal-close waves-effect btn">Close</a>
            </div>
        </div>
        
        
        
        <!-- Modal Structure for Employee Details -->
                <div id="EmpDetails" class="modal" style="width:80%; height:100%;">
            <div class="modal-content">
                <h5 class="center"><u>Employee Details</u></h5>
              
                <div class="row">
                    <div class="col s12 l6 m6">
                        <div class="row z-depth-3 green lighten-4">
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailname" class="black-text">Name : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetaildept" class="black-text">Department : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailmobile" class="black-text">Mobile No : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <!-- <h6 id="Add" class="black-text">Address : </h6> --><p class="black-text" id="empdetailaddress" ></p>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailareas" class="black-text">Area : </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m6">
                        <div class="row z-depth-3 red lighten-3">

                            <div class="col s12 l12 m12">
                                <h6 id="empdetailnoofholidays" class="black-text">No.of Holiday : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailbasicsalary" class="black-text">Basic Salary : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailincentive" class="black-text">Incentives : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetaildeduction" class="black-text">Basic Salary : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailtotalsalary" class="black-text">Total Amount : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailamtpaid" class="black-text">Amount Paid : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailamtpending" class="black-text">Amount Pending : </h6>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="row">
                   <div class="col s12 m12 l12">
                       <div class="col s6 m4 l4 offset-l1">
                           <!-- Dropdown Trigger -->
                           <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filter1'>Action<i
              				 class="material-icons right">arrow_drop_down</i></a>
                           <!-- Dropdown Structure -->
                           <ul id='filter1' class='dropdown-content'>
                               <li><button type="button" onclick="searchEmployeeSalary(1)">Current Month</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(2)">Last 6 Months</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(3)">Last 1 Year</button></li>
                               <li><button type="button" class="rangeSelect1">Range</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(5)">View All</button></li>
                           </ul>
                       </div>
                       <form action="ledger">
                           <input type="hidden" name="range" value="dateRange"> 
                           <input type="hidden" name="employeeDetailsId" id="employeeDetailsId">
                           <span class="showDates1">
                             <div class="input-field col s6 m6 l2">
                               <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate1" required>
                               <label for="startDate1">From</label>
                             </div>

                             <div class="input-field col s6 m6 l2">
                                   <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate1">
                                    <label for="endDate1">To</label>
                              </div>
                           <button type="button"  onclick="searchEmployeeSalary(4)">View</button>
                         </span>
                       </form>
                   </div>
               </div>

                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount Paid</th>
                            <th>Incentive</th>
                            <th>Date</th>
                            <th>Mode of Payment</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody id="empdetailtable">
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
            </div>
        </div>
        
        
        
        <!--modal structure for incentive-->
        <form class="col s12 l12 m12" action="${pageContext.request.contextPath}/giveIncentives" method="post" class="col s12 l12 m12" id="saveIncentive">
        <div id="incentive" class="modal row modal-fixed-footer" style="width:40%;">
          
                  <div class="modal-content" style="padding:10px 24px 10px 24px;">
                    <h5 class="center" style="margin:0"><u>Incentive Details</u></h5>
                   
                   
                    
                    <input id="incentiveEmployeeDetailsId" name="employeeDetailsId" type="hidden"/>
                       
                            <div class="input-field col s12 l8  m10 offset-l2 offset-m1">
                                Name <input id="incentiveName" type="text" class="grey lighten-3" readonly>
                                <!--<h6 id="name">Name:</h6>-->
                            </div>
                    
                     
                            <div class="input-field col s12 l8  m10 offset-l2 offset-m1">
                            	<!-- <input id="incentiveId" type="hidden" name="incentiveId" value="0"> -->
                                <label><span class="red-text">*</span>Incentive Amount</label><input id="incentiveAmount" type="text" name="incentives" required>
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                            <div class="input-field col s12 l8  m10 offset-l2 offset-m1">
                            	
                               <label><span class="red-text">*</span>Reason</label><textarea id="reasonIncentive" class="materialize-textarea" name="reason" required></textarea>
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                            <div class="col s12 l7  m10 offset-l3 offset-m1">                                
                   			  <font color='red'><span id="msgincentivesave"></span></font>
                   			
            				</div>
                      			    <br> 
                  
                        
                      </div>
                  
                       <div class="modal-footer">  
                             		<div class="col s12 m12 l12 center">
			              			  <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
			              			   	<button type="button"  id="saveIncentivesSubmit" class="modal-action waves-effect  btn">Save</button>
			               	    </div>
			             <!--     <button type="button" id="resetIncentive" class="modal-action waves-effect  btn ">Rest</button>
			                 </div> -->
			               
		                		  <br>
		               </div>      
		        </div> 
                   </form>
            
            <!-- incentive modal filter and table start-->
                        <!-- <div class="row">
		                    <div class="col s12 m12 l12">
		                        <div class="col s6 m4 l6 ">
		                            Dropdown Trigger
		                            <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filterIncentive'>Filter &nbsp &nbsp &nbsp &nbsp   <i
		                class="material-icons right">arrow_drop_down</i></a>
		                            Dropdown Structure
		                            <ul id='filterIncentive' class='dropdown-content'>
		                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeIncentive(1)">This Month</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(2)">Last 6 Months</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(3)">Last 1 Year</button></li>
		                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(5)">View All</button></li>
		                            </ul>
		                        </div>
		                        <form action="ledger">
		                            <input type="hidden" name="range" value="dateRange"> 
		                            <span class="showDates">
		                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsIdIncentive">
		                              <div class="input-field col s6 m6 l2">
		                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDateIncentive" required> 
		                                <label for="startDateIncentive">From</label>
		                              </div>
		                              <div class="input-field col s6 m6 l2">
		                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDateIncentive">
		                                     <label for="endDateIncentive">To</label>
		                               </div>
		                            <button type="button" onclick="searchEmployeeIncentive(4)">View</button>
		                          </span>
		                        </form>
		                    </div>
		                </div> -->
                <!-- <table border="2" class="centered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="incentiveTable">
                    
                    </tbody>
                </table> -->
               
             <!-- incentive modal filter and table end-->
           
            
       
        <!-- Modal Structure for Area Details -->
        	<div class="row">
			<div class="col s12 m12 l2">
        <div id="area" class="modal row">
            <div class="modal-content">
                <h5 class="center"><u>Area Details</u><i class="material-icons right modal-close">clear</i></h5>
            
                	 <!-- <div class="col s12 l6 m6">
                        <h6 id="employeeareaId" class="black-text">Employee Id:GK1000000001</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareaname" class="black-text">Name:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareadeptname" class="black-text">Department:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareamobilenumber" class="black-text">Mobile No:</h6>
                    </div> -->
                   
                
                <table id="modalTable" border="2" class="centered tblborder">
                	
                    <thead>
                    	<tr class="noborder">
                    		<td width="230px"><h6 id="employeeareaId" class="black-text">Employee Id:GK1000000001</h6></td>
                    		<td><h6 id="employeeareaname" class="black-text">Name:</h6></td>
                    	</tr>
                    	<tr class="noborder">
                    		<td><h6 id="employeeareadeptname" class="black-text">Department:</h6></td>
                    		<td><h6 id="employeeareamobilenumber" class="black-text">Mobile No:</h6></td>
                    	</tr>
                    	
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Area Name</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            
				<div class="col s12 m12 l12 center">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                </div>
            </div>
        </div>
	</div>
        </div>

        <!-- Modal Structure for View Holiday details -->
        <div id="viewHoliday" class="modal row modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Holiday Details</u><i class="material-icons modal-close right">clear</i></h5>
               

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="empholidayname" class="black-text">Name:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaydepartment" class="black-text">Department:</h6>
                    </div>
                
                
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaymobileno" class="black-text">Mobile No:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaynoofholiday" class="black-text">Total No Of Holiday:</h6>
                    </div>
                </div>
                <div class="row">
                    
                        <div class="col s12 m4 l4 left left-align">
                            <!-- Dropdown Trigger -->
                            <a class='dropdown-button btn left left-align' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                            <!-- Dropdown Structure -->
                            <ul id='filter' class='dropdown-content'>
                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeHolidays(1)">This Month</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(2)">Last 6 Months</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(3)">Last 1 Year</button></li>
                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(5)">View All</button></li>
                            </ul>
                        </div>
                        <div class="col s12 m6 l6">
                        <form action="ledger">
                            <input type="hidden" name="range" value="dateRange"> 
                            <span class="showDates">
                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsId1">
                              <div class="input-field col s6 m6 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate2" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m6 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate2">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s6 m2 l2">
                            		<button type="button" onclick="searchEmployeeHolidays(4)">View</button>
                            </div>
                          </span>
                        </form>
                    </div>
                <div class="col s12 m12 l12">
                <br>
                <div class="scrollDivTable">
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>No.of holiday</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Type Of Leave</th>
                            <th>Amount Deduct</th>
                            <th>Reason</th>
                            <th>Holiday Given Date</th>
                            <th width="80px">Action</th>
                        </tr>
                    </thead>
                    <tbody id="holidaytable">
                       
                    </tbody>
                </table>
            </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
             <div class="col s12 m12 l12 center center-align">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                 </div>	
            </div>
        </div>




         <!-- Modal Structure for View Product Details -->
      <!--  <div id="viewDetails" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Product Details</h4>
                <table border="2">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Category</th>
                            <th>Brand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
            </div>
        </div> -->
        
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
        
		
    </main>
    <!--content end-->
</body>

</html>