<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!-- <!DOCTYPE html> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">var myContextPath = "${pageContext.request.contextPath}"</script>
  <!-- <script src="https://code.highcharts.com/highcharts.js"></script>  -->
  <script src="resources/js/highcharts.js"></script>
  <script type="text/javascript" src="resources/js/charts.js"></script> 
   
   
    <style>
        /*.highcharts-container {
            display: block;
            height: 300px !important;
        }*/  
        
  #notificationbarPendingPayment{
    border-radius: 2px;
    background-color: #0073B7;
    padding: 20px;
    position: fixed;
    bottom: 5px !important;
    right: 10px;
    cursor: pointer;
    text-align: center;
    box-shadow : 5px 5px 5px grey;
    z-index : 999;
    color: #FFF;
     }
      
    #notificationbarThresold{
      border-radius: 2px;
      background-color: #0073B7;
      padding: 20px;
      position: fixed;
      bottom: 150px !important;
      right: 10px;
      cursor: pointer;
      text-align: center;
      box-shadow : 5px 5px 5px grey;
      z-index : 999;
      color: #FFF;
     }
      
        
        .highcharts-legend {
            transform: translate(33, 3000);
        }
        
        .card-content {
            height: 115px !important;
        }   
        /* for cards color */     
        .redcard{
        	background-color:#f56954 !important;
        }
        .greencard{
        	background-color:#00a65a !important;
        }
        .tealLightenone{
        	background-color:#26a69a !important;
        }
        .bluecard{
        	background-color:#0073b7 !important;
        }
        /* for icon color */
        .grrenIcon{
        	color:#009551 !important;
        }
        .blueIcon{
        	color:#0067a4 !important;
        }
        .redIcon{	
        
        	color:#dc5e4b !important;
        }
        
        .countParent{
        	font-size:25px;
        	font-weight:400;
        }
      .card .card-content {
    	padding: 20px;
    	padding-right:2px;
    	border-radius: 0 0 2px 2px;
} 
	.preloader-background{
		display:none!important;
	}
    @media only screen and (max-width: 600px) {
    .pieDiv{
        padding-bottom: 20px !important;
    }
    }
    </style>
    <script>
    function removeNotification(ele){
    	var id=$(ele).attr('data-id');
    	$('#'+id).fadeOut('slow');
    }
         $(document).ready(function() {
        	 
        	$('#notificationbarPendingPayment').animate({width: '400px'},"slow");
 	    	$("#notificationbarPendingPayment").html( "<i class='material-icons right' data-id='notificationbarPendingPayment' onclick='removeNotification(this)'>clear</i><span id='notificationtext1'></span>");
 	    	$("#notificationtext1").html("Hii VSS!!!<br/> Payment of total <b> " +<c:out value="${sessionScope.pendingCounterOrderPaymentCount}" /> +"</b> order are Pending. </br> Please contact them.</br>");
 	    	$("#notificationtext1").click(function(){
 	    		window.location=("${pageContext.request.contextPath}/getPendingPaymentListForAlert");
 	    	});
 	    	$('#notificationbarPendingPayment').show();
 	    	
 	    	
 	    	
 	
 	    	$('#notificationbarThresold').animate({width: '400px'},"slow");
 	    	$("#notificationbarThresold").html( "<i class='material-icons right' data-id='notificationbarThresold' onclick='removeNotification(this)'>clear</i><span id='notificationtext2'></span>");
 	    	$("#notificationtext2").html("Hii VSS!!!<br/> Total <b> "+<c:out value="${sessionScope.productUnderThesoldCount}" />+"</b> Product are under thresold. </br> Please add inventory for them.</br>");
 	    	$("#notificationtext2").click(function(){
 	    		window.location=("${pageContext.request.contextPath}/fetchProductListForUnderThresholdValue");
 	    	});
 	    	$('#notificationbarThresold').show();
        	 
        	 $('.datepicker').pickadate({
            	 container: 'main',
                 selectMonths: true, // Creates a dropdown to control month
                 selectYears: 15, // Creates a dropdown of 15 years to control year,
                 today: 'Today',
                 clear: 'Clear',
                 close: 'Ok',
                 format: 'yyyy-mm-dd',
                 closeOnSelect: 'true', // Close upon selecting a date,
                 
                 // this function closes after date selection Close upon
                 // selecting a date,
                 onSet: function(ele) {
                     if (ele.select) {
                         this.close();
                     }
                 },
                 onStart: function() {        	 
             	    var date = new Date();
             	    this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
             	   
             	}
                 
             	});
        	 
        	 const msg="${msg}";
        	 //alert(msg);
        	 if(msg!='' && msg!=undefined)
        	 {
        		 Materialize.Toast.removeAll();
        		 Materialize.toast(msg, '2000', 'teal lighten-2');
        	 }
        });
    </script>
    
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">

      <%--   <h4 class="center">
            Hello ${employeeDetails.name}. Welcome to Home page.
        </h4> --%>
        <br/>
        <div class="row">
            <div class="col s12 m6 l3">
                <a id="salesReportId" href="${pageContext.request.contextPath}/salesReport?range=today">
                    <div class="card  redcard">
                        <div class="card-content white-text">
                        	<div class="right">
                            <!-- <span><i class="medium material-icons redIcon">shopping_cart</i></span> -->
                            <span><img src="resources/img/rupee.png" alt=""></span>
                            </div>
                            <span class="countParent">&#8377;&nbsp;<span id="totalSalesId" class="count"></span></span>
                            <br/>
                            
                            <span class="center">Total Sales</span>
                            
                        </div>
                       <%--  <div class="card-action white center">
                            <a href="${pageContext.request.contextPath}/fetchProductListForReport?range=ViewAll" class="black-text" id="totalSalesId">345678</a>
                        </div> --%>
                    </div>
                </a>
            </div>

            <div class="col s12 m6 l3">
                <a id="collReportId" href="${pageContext.request.contextPath}/getCollectionReportDetails?range=today">
                    <div class="card greencard">
                        <div class="card-content white-text">
                        	<div class="right">
                            <!-- <span><i class="medium material-icons grrenIcon">play_for_work</i></span> -->
                            <span><img src="resources/img/get-money.png" alt=""></span>
                            </div>
                         <span class="countParent">&#8377;&nbsp;<span id="totalAmountInvestInMarketId" class="count"></span></span><br/>
                            <span class="center" >Total Outstanding</span>
                            
                            <!--<span class="card-title center">Last 7 days</span>-->
                        </div>
                      <!--   <div class="card-action white center">
                            <a href="#" class="black-text" id="totalAmountInvestInMarketId">123</a>
                        </div> -->
                    </div>
                </a>
            </div>

            <div class="col s12 m6 l3">
                <a href="${pageContext.request.contextPath}/fetchProductListForInventory">
                    <div class="card tealLightenone">
                        <div class="card-content white-text">
                        <div class="right">
                            <!-- <span><i class="medium material-icons teal-text">widgets</i></span> -->
                            <span style="margin-right: 3px;"><img src="resources/img/inventory.png" alt=""></span>
                            </div>
                        	<span class="countParent">&#8377;&nbsp;<span id="totalValueOfCurrentInventoryId" class="count"></span></span><br/>
                            <span class="center">Current Inventory</span>
                            
                            <!--<span class="card-title center">Last 7 days</span>-->
                        </div>
                     <%--    <div class="card-action white center">
                            <a href="${pageContext.request.contextPath}/fetchProductListForInventory" class="black-text" id="totalValueOfCurrentInventoryId">243589</a>
                        </div> --%>
                    </div>
                </a>
            </div>

            <div class="col s12 m6 l3">
                <a href="${pageContext.request.contextPath}/fetchProductListForUnderThresholdValue">
                    <div class="card bluecard">
                        <div class="card-content white-text">
                        <div class="right">
                            <!-- <span><i class="medium material-icons blueIcon">add_shopping_cart</i></span> -->
                            <span><img src="resources/img/outstanding.png" alt="" height="64px" width="64px"></span>
                            </div>
                        <span id="productUnderThresholdCountId" class="count countParent"></span></br>
                            <span class="center">Product Under Threshold</span>
                            
                            <!--<span class="card-title center">Last 7 days</span>-->
                        </div>
                     <%--    <div class="card-action white center">
                            <a href="${pageContext.request.contextPath}/fetchProductListForUnderThresholdValue" class="black-text" id="productUnderThresholdCountId">5</a>
                        </div> --%>
                    </div>
                </a>
            </div>

        </div>
        <div class="row">
            <div class="col s12 l6 m6 pieDiv">
                <a id="collReportId2" href="${pageContext.request.contextPath}/getCollectionReportDetails?range=today">
                    <div id="container1" class="center-align  z-depth-2" style="height:30vh"></div>
                </a>
            </div>
            <div class="col s12 l6 m6">
                <a id="topFiveProductsId" href="${pageContext.request.contextPath}/fetchProductListForReport?range=today&topProductNo=5">
                    <div id="container2" class="center-align  z-depth-2" style="height:30vh" ></div>
                </a>
            </div> 
        </div>
        <div class="row">
            <div class="col s12 l6 m6 pieDiv">
                <a id="salesManReportId" href="${pageContext.request.contextPath}/fetchSalesManReport?range=today">
                    <div id="container3" class="center-align  z-depth-2" style="height:30vh"></div>
                </a>
            </div>
            <div class="col s12 l6 m6">
                <a id="returnOrderId" href="${pageContext.request.contextPath}/returnOrderReport?range=currentMonth">
                    <div id="container4" class="center-align  z-depth-2" style="height:30vh"></div>
                </a>
            </div>
        </div>
	<div class="fixed-action-btn">
    <a class="btn-floating btn-large red">
      <i class="material-icons">eject</i>
    </a>
    <ul>
      <li><button id="currentMonthId" class="btn-floating tealLightenone tooltipped" onclick="filterChart('currentMonth','','');" data-position="left" data-delay="50" data-tooltip="Current Month"><i class="material-icons">event_available</i></button></li>
      <li><button id="lastMonthId" class="btn-floating greencard tooltipped" data-position="left" data-delay="50" data-tooltip="Last Month"><i class="material-icons">event</i></button></li>
      <li><button id="last3MonthId" class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Last 3 Month"><i class="material-icons">looks_3</i></button></li>
      <li><button id="last6MonthId" class="btn-floating green tooltipped" data-position="left" data-delay="50" data-tooltip="Last 6 Month"><i class="material-icons">looks_6</i></button></li>
      <li><a href="#rangeModal" class="btn-floating blue tooltipped modal-trigger" data-position="left" data-delay="50" data-tooltip="Range"><i class="material-icons">date_range</i></a></li>
       
    </ul>
  </div>
  
        <div id="rangeModal" class="modal row" style="width:30%;"> 
        <form action="#">
        <div class="modal-content">
        <h5 class="col s12 m12 l12 center"><u>Range</u></h5>
        <div class="col s12 m12 l12">
        	<div class="input-field col s12 m6 l6">
        		<input type="text" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                <label for="startDate">From</label>
        	</div>
        	<div class="input-field col s12 m6 l6">
        		  <input type="text" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate"> 
                     <label for="endDate">To</label>
        	</div>
        	</div>
        </div>
        <div class="modal-footer">
     		<div class="col s12 m12 l12 center center-align">
     			<button type="button" id="rangeId" class="modal-action modal-close btn waves-effect">View</button>
     		</div>   	
        </div>
        </form>
	</div>


		<div id="notificationbarPendingPayment" >
		</div>
		
		<div id="notificationbarThresold">
		</div>
    </main>
    <!--content end-->
        
</body>

</html>