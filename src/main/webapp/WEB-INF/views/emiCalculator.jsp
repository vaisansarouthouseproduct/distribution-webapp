<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"

	
	function clearFunction(){
		$('#loanAmt').val("");
		$('#timePeriod').val("");
		$('#interestRate').val("");
		$('#emiAmt').text("")
		$('#totalInterestAmt').text("");
		$('#totalPaymentAmt').text("");
		
		$('#emiCalculatedDiv').hide();
	
	}
	
	function calculateEMI(){
		
		var p=$('#loanAmt').val();
		var noOfMonths=$('#timePeriod').val();
		var r=$('#interestRate').val();
		if(p==""){
			var $toastContent = $('<span>Please Enetr Loan Amount<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else if(noOfMonths==""){
			var $toastContent = $('<span>Please Enetr Time Period<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else if(r==""){
			var $toastContent = $('<span>Please Enetr Rate Of Interest<span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
		}else{
			var monthlyInterestRate=r/(12*100);
			var top=Math.pow((1 + monthlyInterestRate), noOfMonths);
			var emiAmt=(p*monthlyInterestRate*top)/(top-1);
			emiAmt=emiAmt.toFixedVSS(2);
			
			var totalPaymentAmt=(emiAmt)*noOfMonths;
			var totalInterestAmt=totalPaymentAmt-p;
			$('#emiCalculatedDiv').show();
			
			$('#emiAmt').text(emiAmt);
			$('#totalInterestAmt').text(totalInterestAmt.toFixedVSS(2));
			$('#totalPaymentAmt').text(totalPaymentAmt.toFixedVSS(2));
		}
		
		
		
		
	}
$(document).ready(function() {
	
	$('#emiCalculatedDiv').hide();
	// only number allowed
	$('#loanAmt').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	
	// only number allowed
	$('#timePeriod').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	
	// only number allowed with decimal point
	$('#interestRate').keypress(function( event ){
	    var key = event.which;
	    
	    if($(this).val().indexOf('.') !== -1 && event.keyCode == 46)
            event.preventDefault(); 
	    
	    if(key!=46){
	    	 if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	 	        event.preventDefault();
	    }
	   
	});
	
	
	
	
});


</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	

i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        	 <!-- <button class="btn waves-effect waves-light blue-gradient" id="test">Text</button> -->
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> EMI Calculator </h4>
                    </div>
                    <div class="row">
	                      <div class="input-field col s12 m5 14 push-l1 push-m1 centered content">
	                       
	                        <i class="fa fa-inr prefix"></i>
	                        <input id="loanAmt" type="text" class="validate" name="loanAmt" required maxlength="10">
	                        <label for="loanAmt" class="active"><span class="red-text">*&nbsp;</span>Enter Loan Amount</label>
	                     </div>
	                     
	                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                       <i class="fa fa-percent prefix"></i>
	                        <input id="interestRate" type="text" class="validate" name="interestRate" required maxlength="3">
	                        <label for="interestRate" class="active"><span class="red-text">*&nbsp;</span>Enter Rate of Interest(per annum)</label>
	                     </div>
	                    
                    </div>
                    
                    
                     <div class="row">
	                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        	<i class="material-icons prefix">event</i>
	                        <input id="timePeriod" type="text" class="validate" name="timePeriod" required maxlength="4">
	                        <label for="timePeriod" class="active"><span class="red-text">*&nbsp;</span>Enter TIme Period (In Months only)</label>
	                     </div>
	                     
	                    <!--  <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">person</i>
	                        <input id="emiAmt" type="text" class="validate" name="emiAmt" readonly>
	                        <label for="emiAmt" class="active">EMI Amount:</label>
	                     </div> -->
	                    
                    </div>
                      <div class="row" >
	                      <div class="input-field col s12 m6 6 push-l1 push-m1">
	                     	<button class="btn" type="button" onclick="calculateEMI();">Calculate</button>
	                     </div>
	                     <div class="input-field col s12 m6 6 push-l1 push-m1">
	                     	<button  class="btn" type="button" onclick="clearFunction();">Clear</button>
	                     </div>
	                    
	                    
                    </div>
                    
                    
                </div >
                
                <div class="row  z-depth-3" id="emiCalculatedDiv">
                 
                 <!--  Monthly EMI-->
                 <div class="col s12 m4 l4">
					<div class="card grey lighten-3">
						<div class="card-content teal-text center">
							<p class="card-title">Monthly Emi</p>

							<p class="card-title">
							&#8377;<span id="emiAmt"></span>
							</p>
						</div>
					</div>
				</div>
				
				<!--Total Interest  -->
				<div class="col s12 m4 l4">
					<div class="card grey lighten-3">
						<div class="card-content teal-text center">
							<p class="card-title">Total Interest</p>

							<p class="card-title">
							&#8377;<span id="totalInterestAmt"></span>
							</p>
						</div>
					</div>
				</div>
				
				<!-- Total Payment  -->
				<div class="col s12 m4 l4">
					<div class="card grey lighten-3">
						<div class="card-content teal-text center">
							<p class="card-title">Total Payment</p>

							<p class="card-title">
							&#8377;<span id="totalPaymentAmt"></span>
							</p>
						</div>
					</div>
				</div>
				
				
	             </div>
                </div>
            

    </main>
    <!--content end-->
</body>

</html>
