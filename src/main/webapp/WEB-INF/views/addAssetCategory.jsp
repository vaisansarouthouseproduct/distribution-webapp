<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
    

<script type="text/javascript">

 $(document).ready(function(){
	 var table = $('#tblData').DataTable();
	 table.destroy();
	 $('#tblData').DataTable({
         "oLanguage": {
             "sLengthMenu": "Show _MENU_",
             "sSearch": " _INPUT_" //search
         },
         columnDefs: [
                      { 'width': '1%', 'targets': 0 },
                      { 'width': '30%', 'targets': 1 },
                      { 'width': '20%', 'targets': 2 },
                      { 'width': '20%', 'targets': 3 },
                      { 'width': '20%', 'targets': 4 },
                     
                      { 'width': '1%', 'targets': 5}
                      ],
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'All']
         ],
         autoWidth: false,        
         dom:'<lBfr<"scrollDivTable"t>ip>',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: '.print-col'
                     },
                     customize: function(doc, config) {
                         var tableNode;
                         for (i = 0; i < doc.content.length; ++i) {
                           if(doc.content[i].table !== undefined){
                             tableNode = doc.content[i];
                             break;
                           }
                         }
        
                         var rowIndex = 0;
                         var tableColumnCount = tableNode.table.body[rowIndex].length;
                          
                         if(tableColumnCount > 6){
                           doc.pageOrientation = 'landscape';
                         }
                         /*for customize the pdf content*/ 
                         doc.pageMargins = [5,20,10,5];
                         
                         doc.defaultStyle.fontSize = 8	;
                         doc.styles.title.fontSize = 12;
                         doc.styles.tableHeader.fontSize = 11;
                         doc.styles.tableFooter.fontSize = 11;
                         doc.styles.tableHeader.alignment = 'center';
                         doc.styles.tableBodyEven.alignment = 'center';
                         doc.styles.tableBodyOdd.alignment = 'center';
                       },
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }

     });
	 $("select")
      .change(function() {
          var t = this;
          var content = $(this).siblings('ul').detach();
          setTimeout(function() {
              $(t).parent().append(content);
              $("select").material_select();
          }, 200);
      });
  $('select').material_select();
  $('.dataTables_filter input').attr("placeholder", "Search");
	 var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	    // $('#msgHead').text("BusinessType Message");
	     $('#msg').text(msg);
	 }
	 
	$("#resetBankSubmit").click(function(){
		
		$('#payeeName').val('');	
		$('#address').val('');
		$('#payeeId').val('0');
		$("#saveBankPayeeSubmit").html('<i class="material-icons left">add</i> Add');
		
	});
	 var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	     //$('#msgHead').text("Brand Message");
	     $('#msg').text(msg);
	 }
});
 //when click on edit button 
 //given Bank Payee details fill on form for update
 function editBankPayee(id) {
		
		$.ajax({
					type : "GET",
					url : "${pageContext.request.contextPath}/fetchBankPayeeById?payeeId="+id,
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var bankPayee = data;
						//alert(brand.name +" "+ brand.brandId);
						$('#payeeName').val(bankPayee.payeeName);	
						$('#payeeId').val(bankPayee.payeeId);
						$('#address').val(bankPayee.payeeAddress);
						$('#payeeName').focus();
						$("#saveBankPayeeSubmit").html('<i class="material-icons left">send</i> update');
						
						
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
	    				
	    			},error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						 // alert(error +"---"+ xhr+"---"+status);
						 $('#addeditmsg').find("#modalType").addClass("warning");
				        $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
						$('#addeditmsg').modal('open');
	           	     	//$('#msgHead').text("Message : ");
	           	     	$('#msg').text("Something Went Wrong "); 
	           	     		 setTimeout(function() 
									  {
	           	     					$('#addeditmsg').modal('close');
									  },1000);
						}					
				
				});
	}

 </script>

</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
  <main class="paddingBody">
        <br>
            <div class="row">	
             <div class="col s12 m12 l12  formBg">
        <form  action="${pageContext.servletContext.contextPath}/saveBankPayee" method="post" id="saveBankPayeeForm">
            <input id="payeeId" type="hidden" class="validate" name="payeeId" value="0">
           
                <div class="input-field col s12 l3 m4">
                
                  <label><span class="red-text">*</span>Enter Asset Category Name:</label><input id="payeeName" type="text" class="validate" name="payeeName" required>
                </div>
               <!--   <div class="input-field col s12 l3 m4">
                
                  <label><span class="red-text">*</span>Enter Payee Address:</label><input id="address" type="text" class="validate" name="address" required>
                    <label for="businessTypename" class="active">Enter Business Type</label>
                </div> -->
                <div class=" col  s12 l3 m4 left" style="padding-top:32px;width:14%;">
                    <button class="btn waves-effect waves-light blue-gradient " id="saveBankPayeeSubmit" type="submit" name="action"><i class="left material-icons " id="ic">add</i>Add</button>
                </div>
                  <div class="col s10 l3 m6" style="padding-top:32px;" >
                    <button id="resetBankSubmit" class="btn waves-effect waves-light blue-gradient" type="button" name="action" ><i class="left material-icons">refresh</i>Reset</button>
                </div>
           
        </form>
        </div>
		    </div>    
        
                <div class="col s12 l12 m12">
                    <table class="striped highlight centered mdl-data-table display  select" id="tblData" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="print-col">Sr. No</th>
                                <th class="print-col">Asset Category</th>
                                <th class="print-col">Total Qty</th>
                                 <th class="print-col">Assign Qty</th>
                                <th class="print-col">Available Qty</th>
                                <th>Edit</th>
                                <!-- <th>Delete</th> -->
                            </tr>
                        </thead>

                        <tbody>
                         <% int rowincrement=0; %>
                     	<c:if test="${not empty assetsCategoryList}">
						<c:forEach var="listValue" items="${assetsCategoryList}">						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                            <tr>
                                <td><c:out value="${rowincrement}" /></td>
                                <td><c:out value="${listValue.assetCategoryName}" /></td>
                                <td><c:out value="0" /></td>
                                <td><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.payeeAddedDatetime}" /><c:out value="${dt}" /> & <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.payeeAddedDatetime}" /><c:out value="${time}" />
                                </td>
                              <%--   <td>
	                                <c:choose>
		                        	<c:when test="${empty  listValue.payeeUpdatedDatetime}">
		                        		NA
			                        </c:when>
			                        <c:otherwise>
				                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.payeeUpdatedDatetime}" /><c:out value="${dt}" />
				                      	&
				                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.payeeUpdatedDatetime}" /><c:out value="${time}" />
			                        </c:otherwise>
			                        </c:choose>
                                </td> --%>
                                <td><button class=" btn-flat"  onclick='editBankPayee(${listValue.assetCategoryId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
                                <!-- Modal Trigger -->
                                <!-- <td><a href="#delete"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                            </tr>
                            </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
             

			 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
                            <div class="col s12 center">
                                    <a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
                            </div>
                            
                        </div>
				</div>
			</div>
		</div>
		
          
       
  </main>
    <!--content end-->
</body>

</html>