<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html lang="en">
<head>
<%@include file="components/header_imports.jsp" %>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript" src="resources/js/targetView.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	
	var table = $('#tblDataTarget').DataTable();
	 table.destroy();
	$('#tblDataTarget').DataTable({
		//'rowsGroup': [0,1,3], 
		"oLanguage": {
             "sLengthMenu": "Show: _MENU_",
             "sSearch": " _INPUT_" //search
         },
         //"order": [],
         "ordering": false,
         "autoWidth": false,
         "span":true,
         "columnDefs": [
						{ "width": "5%", "targets": 0},	
						{ "width": "20%", "targets": 1},	
						{ "width": "10%", "targets": 2},
						{ "width": "20%", "targets": 3},			
						{ "width": "5%", "targets": 4},
						{ "width": "5%", "targets": 5},
						{ "width": "5%", "targets": 6}
		              ],
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'all']
         ],
       
         dom:'<lBfr<"scrollDivTable"t>ip>',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                     title: 'Target Details',
                     //file name 
                     filename: 'TargetDetails',
                    
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                     customize: function(doc, config) {
                    	 doc.content.forEach(function(item) {
                    		  if (item.table) {
                    		  item.table.widths = [50,50,100,20] 
                    		 } 
                    		    })
                        
                         //for customize the pdf content 
                         doc.pageMargins = [5,20,10,5];
                         
                         doc.defaultStyle.fontSize = 8	;
                         doc.styles.title.fontSize = 12;
                         doc.styles.tableHeader.fontSize = 11;
                         doc.styles.tableFooter.fontSize = 11;
                         doc.styles.tableHeader.alignment = 'center';
                         doc.styles.tableBodyEven.alignment = 'center';
                         doc.styles.tableBodyOdd.alignment = 'center';
                       },
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: 'Target Details',
                     //file name 
                     filename: 'TargetDetails',
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title:'Target Details',
                     //file name 
                     filename: 'TargetDetails',
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }
			});
	
	 $("select").change(function() {
            var t = this;
            var content = $(this).siblings('ul').detach();
            setTimeout(function() {
                $(t).parent().append(content);
                $("select").material_select();
            }, 200);
        });
    $('select').material_select();
    $('.dataTables_filter input').attr("placeholder", "  Search");
    if ($.data.isMobile) {
					var table = $('#tblDataTarget').DataTable(); // note the capital D to get the API instance
					var column = table.columns('.hideOnSmall');
					column.visible(false);
		}
    
   
    $('#departmentId').change(function(){
		var departmentId=$('#departmentId').val();
		
		window.location.href="${pageContext.servletContext.contextPath}/open_target_assign_list?departmentId="+departmentId;
	});
});
</script>
<style>
/* div.dataTables_filter {
     margin: 0 0 !important; 
} */
label{
	color:black;
}
.row .col.l12 {
    width: 100%;
    margin-left: auto;
    left: auto;
    right: auto;
    padding-bottom:50px;
}
.btnmarginAlign{
    margin-top:2%;
}
@media only screen and (max-width:992px){
    .btnmarginAlign{
    margin-top:4%;
}
}

@media only screen and (max-width:600px){
    .btnmarginAlign{
    margin-top:8%;
}
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->


    
    <main class="paddingBody">
        <br>

<div id="showTable">
		<div class="row">
		<div class="col s12 m12 l12" style="padding-bottom:15px;">
       		   <form method="post" action="${pageContext.request.contextPath}/employeeCommission">
       		   
       		   <div class="col s9 m6 l4 offset-l4 offset-m3">	
       		   
            		<fieldset>
 					 <legend>Filter</legend>
                      <div class="input-field col s6 m6 l6">
                      	<!-- <input type="text" id="startYear" class="datepicker"> -->
                        <select id="startMonthId" name="monthId" required>
                                 <option value="">Month</option>
                                 <option value="1">Jan</option>
                                 <option value="2">Feb</option>
                                 <option value="3">Mar</option>
                                 <option value="4">Apr</option>
                                 <option value="5">May</option>
                                 <option value="6">June</option>
                                 <option value="7">July</option>
                                 <option value="8">Aug</option>
                                 <option value="9">Sept</option>
                                 <option value="10">Oct</option>
                                 <option value="11">Nov</option>
                                 <option value="12">Dec</option>                                 
                        </select>
                      </div>
                      <div class="input-field col s6 m6 l6">
                      		<!-- <input type="text" id="endYear" class="datepicker"> -->
                            <select id="startYearId" class="yearSelect" name="yearId" required>
                                 <option value="">Start Year</option>
                                 <%
                                 for(int year=2018; year<2051; year++){%>
                                 	<option value="<%=year%>"><%=year%></option>
                                 <%} %>
                            </select>   
                       </div>
                       </fieldset>
                       </div>
                 		
					   <div class="input-field col s3 m2 l1 btnmarginAlign">
						<button type="submit" class="btn">View</button>
				  </div>
                  </form>
                
                </div>
                </div>
		<br>
		
		<div class="row">
		
		<div class="col s12 l12 m12">
			<table class="striped highlight centered tblborder" id="tblDataTarget" width="100%">
				<thead>
					<tr>
						<th  rowspan="2" class="print-col hideOnSmall">Sr.No.</th>
						<th  rowspan="2" class="print-col">Employee Name</th>
						<th  rowspan="2" class="print-col">Department</th>
						<th  colspan="3" class="print-col">Details</th>
						<th  rowspan="2" >Total Commission Value</th>
					</tr>
					<tr>
						<th>Name</th>
						<th>Sale Value</th>
						<th>Commission Value</th>
					</tr>
				</thead>					
				<tbody>
				<!-- <tr>
					<td>1</td>
					<td>Sachin</td>
					<td>SalesMan</td>
					
					<td>
						<button class="btn-flat" type="button" onclick="editrow('+i+')"><i class="material-icons">edit</i></button>&nbsp;&nbsp;
					    <button class="btn-flat" type="button" onclick="deleteRowConfirmation('+i+')"><i class="material-icons">clear</i></button>
					</td>
				</tr> -->
				
				<c:if test="${not empty employeeCommissionModelsList}">
				<c:forEach var="listValue" items="${employeeCommissionModelsList}">
				<c:set value="1" var="index"/>
				<c:set var="length" scope="page" value="${listValue.employeeCommissionDetailsModelList.size()}"/> 
				<c:forEach var="listValueChild" items="${listValue.employeeCommissionDetailsModelList}">
				<tr>
					<c:if test="${index==1}"><td rowspan="${length}"><c:out value="${listValue.srno}"/></td></c:if>
					<c:if test="${index==1}"><td rowspan="${length}"><c:out value="${listValue.name}"/></td></c:if>
					<c:if test="${index==1}"><td rowspan="${length}"><c:out value="${listValue.departmentName}"/></td></c:if>
					<td><c:out value="${listValueChild.commissionName}"/></td>
					<td><c:out value="${listValueChild.saleValue}"/>
						<c:choose>
						<c:when test="${listValueChild.saleValueType='amount'}">
						&#8377;
						</c:when>
						<c:otherwise>
						qty.
						</c:otherwise>
						</c:choose>
					</td>
					<td><c:out value="${listValueChild.commissionValue}"/></td>
					<c:if test="${index==1}">
					<td rowspan="${length}">
						<c:out value="${listValue.totalCommissionValue}"/>
					</td>
					</c:if>
				</tr>
				<c:set value="${index+1}" var="index"/>
				</c:forEach>
				</c:forEach>
				</c:if> 
				</tbody>
			</table>			
		</div> 
		</div>
</div>
 <!-- Modal Structure for View Commission Slabs -->

        <div id="viewDetails" class="modal">
            <div class="modal-content">
                <h5><u>Commission Assign Details</u></h5>
                <div class="row productNameHide" >
                	<div class="col s12 m12 l12">
          				<div class="col s2 m3 l3" style="width:18%">
          					<h6><b>Product Names : </b></h6>	
          				</div>
          				<div class="col s6 m6 l6 left-align">
          					<h6 ><span id="productNameId"></span></h6>	
          				</div>
          			</div>
                </div> 
                <div class="row">
	                <table border="2" class="tblborder centered">
	                    <thead>
	                        <tr>
	                            <th>Sr.No</th>
	                            <th>From</th>
	                            <th>To</th>
	                            <th>Commission Type</th>
	                            <th>Target Period</th>
	                            <th>Commission</th>
	                        </tr>
	                    </thead>
	                    <tbody id="commissionAssignSlabId">
	                    </tbody>
	                </table>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
            </div>
        </div> 
        <!-- End -->
	  </main>
	<!--content end-->
	
</body>

</html>