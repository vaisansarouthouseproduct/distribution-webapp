<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">

<head>
	<%@include file="components/header_imports.jsp" %>

	<script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script type="text/javascript" src="resources/js/targetView.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			$("#btnprint").click(function () {
				$('.tblExport tr').find('th.print-col:last-child, td.print-col:last-child').hide();
				window.print();
				$('.tblExport tr').find('th.print-col:last-child, td.print-col:last-child').show();
			});
			$("#btnExcel").click(function (e) {
				e.preventDefault();
				$('.tblExport tr').find('th:last-child, td:last-child').remove();
				//getting data from our table
				var data_type = 'data:application/vnd.ms-excel';
				var table_div = document.getElementsByClassName('tblExport');
				//var table_div = document.getElementById('tblSales');
				var table_html = table_div[0].outerHTML;
				var a = document.createElement('a');
				a.href = data_type + ', ' + escape(table_html);
				a.download = 'Target Assign View' + '.xls';
				a.click();
				window.location.reload();
			});



			$('#departmentId').change(function () {
				var departmentId = $('#departmentId').val();

				window.location.href = "${pageContext.servletContext.contextPath}/open_target_assign_list?departmentId=" + departmentId;
			});
		});
	</script>
	<style>
		@media only screen and (max-width:600px) {

			.excelBtnDiv,
			.printBtnDiv {
				float: left !important;
				text-align: left !important;
				margin: 10px 0;
			}
			.modal{
				width:95%;
			}	
			.modal .modal-content{
				padding: 5px !important;
			}
			.modal .modal-content h5>u{
				width: 90%;
			}
		}

		/* div.dataTables_filter {
     margin: 0 0 !important; 
} */
		label {
			color: black;
		}

		.noPadding {
			padding: 0 !important;
		}

		@media print {
			body * {
				visibility: hidden;
			}

			#section-to-print,
			#section-to-print * {
				visibility: visible;
				margin: auto;
			}

			#section-to-print {
				position: absolute;
				left: 0;
				top: 0;
			}
		}

	</style>

</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->



	<main class="paddingBody">
		<br>

		<div class="row">

			<div id="showTable">

				<div class="col s12 m12 l12 noPadding">
					<div class="col s6 l2 m3 left">
						<a class="btn waves-effect waves-light blue-gradient"
							href="${pageContext.servletContext.contextPath}/add_new_target">
							<i class="material-icons left hide-on-small-only">add</i>Assign Targets
						</a>
					</div>
					<!--<div class="col s10 l2 m3 left">
				<a class="btn waves-effect waves-light blue-gradient"
					href="${pageContext.servletContext.contextPath}/commissionAssign?commissionAssignId=0" >
					<i class="material-icons left">add</i>Commission slab
				</a>
			</div>-->
					<div class="col s6 l2 m3 right right-align">
						<select id="departmentId" class="btnSelect">
							<option value="0" selected>Choose Department</option>
							<c:if test="${not empty departmentList}">
								<c:forEach var="listValue" items="${departmentList}">
									<option value="${listValue.departmentId}"><c:out value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
						</select>
					</div>
					<div class="col s6 l2 m2 right right-align excelBtnDiv">
						<button class="btn waves-effect waves-light blue-gradient" id="btnprint">Print<i
								class="material-icons left">local_printshop</i> </button>
					</div>
					<div class="col s6 l2 m2 right right-align printBtnDiv">
						<button class="btn waves-effect waves-light blue-gradient" id="btnExcel">Excel<i
								class="material-icons left">insert_drive_file</i> </button>
					</div>
				</div>
				<br>
				<br>
				<div id="section-to-print">
					<div class="col s12 l12 m12">
						<table class="highlight centered tblborder tblExport" border="1" id="tblDataTarget" width="100%"
							style="width:100%">
							<thead>
								<tr>
									<th rowspan="2" class="print-col">Sr.No.</th>
									<th rowspan="2" class="print-col">Employee Name</th>
									<th rowspan="2" class="print-col">Department</th>
									<th colspan="2" class="print-col">Commission Assigned</th>
									<th rowspan="2" class="print-col">Action</th>
								</tr>
								<tr>
									<th>Name</th>
									<th>Target Value</th>
								</tr>
							</thead>
							<tbody>
								<!-- <tr>
					<td>1</td>
					<td>Sachin</td>
					<td>SalesMan</td>
					
					<td>
						<button class="btn-flat" type="button" onclick="editrow('+i+')"><i class="material-icons">edit</i></button>&nbsp;&nbsp;
					    <button class="btn-flat" type="button" onclick="deleteRowConfirmation('+i+')"><i class="material-icons">clear</i></button>
					</td>
				</tr> -->

								<c:if test="${not empty targetLists}">
									<c:forEach var="listValue" items="${targetLists}">
										<c:set value="1" var="index" />
										<c:set var="length" scope="page"
											value="${listValue.employeeCommissionDetails.size()}" />
										<c:forEach var="listValueChild" items="${listValue.employeeCommissionDetails}">
											<tr>
												<c:if test="${index==1}">
													<td rowspan="${length}">
														<c:out value="${listValue.srno}" />
													</td>
												</c:if>
												<c:if test="${index==1}">
													<td rowspan="${length}">
														<c:out value="${listValue.employeeName}" />
													</td>
												</c:if>
												<c:if test="${index==1}">
													<td rowspan="${length}">
														<c:out value="${listValue.departmentName}" />
													</td>
												</c:if>
												<td>
													<a class="btn-flat" type="button"
														onclick="showCommissionAssign(${listValueChild.commissionAssignId})">
														<c:out value="${listValueChild.commissionAssignName}" />
													</a>
												</td>
												<td>
													<c:out value="${listValueChild.targetMinimumValue}" />
												</td>
												<c:if test="${index==1}">
													<td rowspan="${length}" class="print-col">
														<a class="btn-flat"
															href="${pageContext.servletContext.contextPath}/edit_target?targetAssignId=${listValue.targetAssignId}"><i
																class="material-icons">edit</i></a>&nbsp;&nbsp;
														<button class="btn-flat" type="button"
															onclick="deleteConfirmation(${listValue.targetAssignId})"><i
																class="material-icons">clear</i></button>
													</td>
												</c:if>
											</tr>
											<c:set value="${index+1}" var="index" />
										</c:forEach>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Modal Structure for View Commission Slabs -->
		</div>
		<div id="viewDetails" class="modal">
			<div class="modal-content">
				<h5 class="center"><u>Commission Assign Details</u><i class="material-icons modal-close right">clear</i>
				</h5>
				<div class="row productNameHide">
					<div class="col s12">
						<ul class="tabs filterTab">
							<li class="tab col s3"><a href="#productTable">Product list</a></li>
							<li class="tab col s3"><a class="active" href="#commissionTable"
									id="commissionTab">Commission List</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col s12" id="productTable">
							<div class="scrollDivTable">
						<table>
							<thead>
								<tr>
									<th colspan="3">Products</th>
								</tr>
							</thead>
							<tbody id="productsNamesBody">

							</tbody>
						</table>
					</div>
					</div>
					<div class="col s12" id="commissionTable">
						<div class="scrollDivTable">
						<table border="2" class="tblborder centered">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>From</th>
									<th>To</th>
									<th>Commission Type</th>
									<th>Target Period</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody id="commissionAssignSlabId">
							</tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">
					<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
				</div>

			</div>
		</div>
		<!-- End -->
	</main>
	<!--content end-->

</body>

</html>