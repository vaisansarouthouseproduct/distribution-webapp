<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
      <script>
      $(document).ready(function(){
    	  var table = $('#tblData').DataTable();
 		 table.destroy();
 		 $('#tblData').DataTable({
 	         "oLanguage": {
 	             "sLengthMenu": "Show _MENU_",
 	             "sSearch": "_INPUT_" //search
 	         },
 	     	      	autoWidth: false,
 	         columnDefs: [
 	                      { 'width': '4%', 'targets': 0 },
 	                      { 'width': '15%', 'targets': 1},
 	                      { 'width': '5%', 'targets': 2},
 	                  	  { 'width': '5%', 'targets': 3},
 	                  	  { 'width': '5%', 'targets': 4},
 	                	  { 'width': '5%', 'targets': 5}
 	                	
 	                      ],
 	         lengthMenu: [
 	             [10, 25., 50, -1],
 	             ['10 ', '25 ', '50 ', 'All']
 	         ],
 	         
 	         
 	         dom:'<lBfr<"scrollDivTable"t>ip>',
 	         buttons: {
 	             buttons: [
 	                 //      {
 	                 //      extend: 'pageLength',
 	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
 	                 //  }, 
 	                 {
 	                     extend: 'pdf',
 	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                     customize: function(doc, config) {
 	                    	doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [50,'*',80,80,80] 
	                    		 } 
	                    		    })
 	                          
 	                        /*  if(tableColumnCount > 6){
 	                           doc.pageOrientation = 'landscape';
 	                         } */
 	                         /*for customize the pdf content*/ 
 	                         doc.pageMargins = [5,20,10,5];   	                         
 	                         doc.defaultStyle.fontSize = 8	;
 	                         doc.styles.title.fontSize = 12;
 	                         doc.styles.tableHeader.fontSize = 11;
 	                         doc.styles.tableFooter.fontSize = 11;
 	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
 	                       },
 	                 },
 	                 {
 	                     extend: 'excel',
 	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'print',
 	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'colvis',
 	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
 	                     collectionLayout: 'fixed two-column',
 	                     align: 'left'
 	                 },
 	             ]
 	         }

 	     });
 		 $("select").change(function() {
           var t = this;
           var content = $(this).siblings('ul').detach();
           setTimeout(function() {
               $(t).parent().append(content);
               $("select").material_select();
           }, 200);
       });
     $('select').material_select();
	 $('.dataTables_filter input').attr("placeholder", "  Search");
	 if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);
				$(".btnReIssue").removeClass('right right-align');
			}
    	 
      });
      </script>
      <style>
       .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    tfoot th,
    tfoot td    {
    	text-align:center;
    }
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
	}
	.modal .modal-footer{
		text-align: center;
	}
      	/*  .card-panel p{
        	font-size:15px !important;
        } */
        /* .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:35%;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
    .row{
    margin-bottom:0;
    } */
      </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    <!-- <script>alert("${orderProductIssueListForIssueReportResponse.deliveryPersonName}");</script> -->
        <br>
         <div class="row">
			 <div class="col s12">
        	<div class="col s12 l9 m9 card-panel hoverable blue-grey lighten-4">
      	
       			
                <div class="col s12 l6 m6">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${gKReturnOrderReportResponse.fetchOrderDetailsModel.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                 <div class="col s12 l6 m6">
                   <p id="department"><span class="leftHeader">Shop Name: </span>
                   <b><c:out value="${gKReturnOrderReportResponse.fetchOrderDetailsModel.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${gKReturnOrderReportResponse.fetchOrderDetailsModel.mobileNo}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                     <div class="col s12 l6 m6">
                       <p id="Add"><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${gKReturnOrderReportResponse.fetchOrderDetailsModel.orderDate}"  /><c:out value="${date}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
             
                <div class="col s12 l6 m6">
                	 <div class="col s12 m3 l3" style="padding:0;width:105px"><p id="area"><span>Address:</span></p></div>
                 <div class="col s12 m8 l8 left-align" style="word-wrap:break-word;padding:0"><p><b><c:out value="${gKReturnOrderReportResponse.fetchOrderDetailsModel.businessAddress}" /></b></p></div>
                         
                   <%-- <p id="area"><span class="leftHeader">Address:</span> <b><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.address}" /></b></p> --%>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
               
                
           
               <%--  <div class="col s12 l6 m6 ">
                       <p id="Add" class=" blue-text">Date of Order: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div> --%>
                
           
           </div> 
          <div class="col l3 m3 s12 center center-align" style="margin-top:0.6%;">
           <c:if test="${sessionScope.loginType=='GateKeeper'}">
        	<a href="${pageContext.request.contextPath}/fetchOrderDetailsByOrderIdForReIssueForWeb?orderId=${gKReturnOrderReportResponse.fetchOrderDetailsModel.orderId}" class="btn blue-gradient right right-align btnReIssue">ReIssue</a>
	       	<a href="${pageContext.request.contextPath}/returnOrderDetailsByReturnOrderIdForPReturn?returnOrderId=${gKReturnOrderReportResponse.fetchOrderDetailsModel.returnOrderId}" class="btn blue-gradient">Return</a>
       	    </c:if>
       		 	
			   </div>
		</div>
   <%--      <div class="row">
        <div class="col s12 l5 m5 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" id="name">Order ID:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.orderId}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 l5 m5 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" id="name">Shop Name:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.shopName}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
             <div class="col l2 m6 s6 right right-align" style="margin-top:0.6%">
        	<a href="${pageContext.request.contextPath}/fetchOrderDetailsByOrderIdForReIssueForWeb?orderId=${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.orderId}" class="btn blue-gradient">ReIssue</a>
       		 </div>
       		  </div> --%>
<%--        	<div class="row">
           <div class="col s12 l5 m5 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" id="name">Mobile No:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.contact.mobileNumber}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 l5 m5 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" id="name">Date of Order:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.orderDetailsAddedDatetime}"  /><c:out value="${date}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           </div>
           <div class="row">
           <div class="col s12 l10 m10 left">
      		  <div class="card horizontal">
      		  	<div class="card-image" style="width:17%">
       				  <h6 class="white-text" id="name">Address:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
      			 
       			<h6 class=""><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.address}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  --%> 
            <%-- <div class="col s12 l6 m6 card-panel hoverable">
                        <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.orderId}" /></b> </p>
                      
                        <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.shopName}" /></b></p>
                      
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.contact.mobileNumber}" /></b></p>
                        
                        <p id="Add" class="center-align blue-text">Address <b><c:out value="${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.businessName.address}" /></b></p>
                       
                        <p id="Add" class="center-align blue-text">Date of Order: <b>
                        <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${gKReturnOrderReportResponse.returnOrderProduct.orderDetails.orderDetailsAddedDatetime}"  />
				       	<c:out value="${date}" />
                        </b></p>
           </div> --%>
                     
               
            
     
      	  <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">Return Quantity</th>
                            <th class="print-col">Issue Quantity</th>
                            <th class="print-col">Amount</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty gKReturnOrderReportResponse.returnOrderProductDetailsReportList}">
						<c:forEach var="listValue" items="${gKReturnOrderReportResponse.returnOrderProductDetailsReportList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.productName}" /><font color='green'><b>${listValue.returnTotalAmountWithTax==0?'-Free':''}</b></font></td>
                            <td><c:out value="${listValue.returnQuantity}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            <td><c:out value="${listValue.returnTotalAmountWithTax}" /></td>
                            <td><a area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Reason" href="#reason_${rowincrement}" class="modal-trigger tooltipped">Reason</a></td>
                        </tr>
                        	 <div class="row">
								<div class="col s8 m2 l2">
									<div id="reason_${rowincrement}" class="modal deleteModal row">
										<div class="modal-content">
										<h5 class="center-align" style="margin-bottom:30px"><u>Reason</u></h5>
										
											<%-- <h5 id="msgHead">Reason of ${listValue.id} is :</h5>
											<hr> --%>	
											
											<h6 id="msg" class="center-align"><b>Reason: </b><c:out value="${rowincrement}"/></h6>
											  <br/>
										</div>
										<div class="col s12 center center-align m2 l2 offset-l5 modal-footer">
											<a href="#!"
												class="modal-close waves-effect  btn">OK</a>
										</div>
									</div>
								</div>
							</div>
                        
                        
	                        <%-- <div class="row">
								<div class="col s8 m2 l2">
									<div id="addeditmsg_${listValue.id}" class="modal" style="width:40%;">
										<div class="modal-content">
											<h5 id="msgHead">Reason of ${listValue.id} is : </h5>
											<hr>
											<p id="msg"><c:out value="${listValue.reason}"/></p>
										</div>
										<div class="modal-footer">
											<a href="#!"
												class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
										</div>
									</div>
								</div>
							</div> --%>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
        
    </main>
    <!--content end-->
</body>

</html>