
<!doctype html>
<html>
<head>
  <title>html2canvas - Examples</title>
  <script type="text/javascript"  src="resources/js/prTestJsp.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

  <base href="" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Screenshot creation with JavaScript">
  <meta name="author" content="Niklas von Hertzen">

  <link href="resources/css/bootstrap.min.css" rel="stylesheet">
  <style>
    header {
      height: 50px;
    }
    footer {
      background-color: #333;
      color: white;
      padding: 20px 0;
    }
    .share {
      padding-top:5px;
      margin-bottom:-5px;
      margin-right: -50px;
    }

    .page-header, .lead {
      color: #5A5A5A;
    }
    #doc-nav {
      width: 220px;
    }

    #doc-nav .active a {
      background-color: #08C;
      color: #fff;
      border-color: #08C;
    }

    html {
      overflow-y: scroll;
    }

    .com { color: #93a1a1; }
    .lit { color: #195f91; }
    .pun, .opn, .clo { color: #93a1a1; }
    .fun { color: #dc322f; }
    .str, .atv { color: #D14; }
    .kwd, .prettyprint .tag { color: #1e347b; }
    .typ, .atn, .dec, .var { color: teal; }
    .pln { color: #48484c; }

    .prettyprint {
      padding: 8px;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
    .prettyprint.linenums {
      -webkit-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
      -moz-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
      box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
    }

    /* Specify class=linenums on a pre to get line numbering */
    ol.linenums {
      margin: 0 0 0 33px; /* IE indents via margin-left */
    }
    ol.linenums li {
      padding-left: 12px;
      color: #bebec5;
      line-height: 20px;
      text-shadow: 0 1px 0 #fff;
    }

  </style>
  <style>
    #carbonads {
      display: block;
      overflow: hidden;
      padding: 1em;
      border: solid 1px #dddddd;
      border-radius: 4px;
      background-color: #fafafa;
      text-align: center;
      font-size: 14px;
      line-height: 1.5;
    }

    #carbonads span {
      display: block;
      overflow: hidden;
    }

    .carbon-img {
      display: block;
      margin: 0 auto .5em;
    }

    .carbon-text {
      display: block;
      margin-bottom: .5em;
    }

    .carbon-poweredby {
      display: block;
      color: #222;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-size: 9px;
      line-height: 1;
    }
  </style>

</head>
<body>
<header>
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="index.html">html2canvas</a>
        <div class="nav-collapse">
          <ul class="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/documentation.html">Documentation</a></li>
            <li><a href="/faq.html">FAQ</a></li>
            <li class="active"><a href="/examples.html">Examples</a></li>
            <li><a href="https://github.com/niklasvh/html2canvas/releases" target="_blank">Download</a></li>
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <a href="http://github.com/niklasvh/html2canvas"><img style="position: fixed; top: 0; right: 0; border: 0;z-index:9999;" src="https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png" alt="Fork me on GitHub"></a>
</header>
<div class="container">
  <div class="row-fluid">
    <div class="span3">
      <ul class="nav nav-tabs nav-stacked" id="doc-nav"></ul>
      <script async type="text/javascript" src="//cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=html2canvashertzencom" id="_carbonads_js"></script>

    </div>
    <div class="span9">
      <div class="row-fluid" id="docs">
        <div class="page-header">
          <h1>Examples</h1>
        </div>

        <p class="lead">
          Rendering the body element and appending the canvas to the body
        </p>

	       <div id="tableData1" style="background-color:white;">
	       <table  id="tableData" border ="1" style="border-collapse: collapse;">
        <tr>
            <td colspan="2" rowspan="1">
		            <b>BlueSquare</b><br>
		            You can easily change the formatting <br>
		            of selected text in the document text <br>
		            by choosing a look for the selected text <br>
		            from the Quick Styles gallery on the Home tab. 
		    </td>            
            <td colspan="6">
            	<table border ="1" frame="void" style="width: 100%;border-collapse: collapse;">
	            	<tr>
		                <td style="width: 50%;" colspan="2"><b>Invoice Number</b><br>A1234</td>
		                <td style="width: 50%;"  colspan="2"><b>Dated</b><br>21/12/2014</td>
		            </tr>
	            	<tr>
		                <td style="width: 50%;"  colspan="2"><b>Delivery Note</b><br>ABC</td>
		                <td style="width: 50%;"  colspan="2"></td>
		            </tr>
		            <tr>
		                <td style="width: 50%;"  colspan="2"><b>Supplier's Ref.</b><br>SUB1001</td>
		                <td style="width: 50%;"  colspan="2"><b>Other Reference(s)</b><br>Mr. Deshmukh</td>
		            </tr>
	            </table>	
            </td>
        </tr>


        <tr>
            <td colspan="2" rowspan="1">Buyer<br><b>Hari om Enterprise</b></td>
            <td  colspan="6">
            	<table  border ="1" frame="void" style="width: 100%;border-collapse: collapse;">
            		<tr>
		                <td style="width: 50%;"  colspan="2"><b>Buyer's Order No.</b><br>A1234</td>
		                <td style="width: 50%;"  colspan="2"><b>Dated</b><br>21/12/2014</td>
		            </tr>
	            	<tr>
		                <td style="width: 50%;"  colspan="2"><b>Despatch Document No.</b><br>ABC</td>
		                <td style="width: 50%;"  colspan="2"><b>Delivery Note Date</b><br>ABC</td>
		            </tr>
		            <tr>
		                <td style="width: 50%;"  colspan="2"><b>Despatched through</b><br>SUB1001</td>
		                <td style="width: 50%;"  colspan="2"><b>Destination</b><br>Mr. Deshmukh</td>
		            </tr>
	            </table>	
            </td>
        </tr>

        <tr >
            <th>Sr.No</th>
            <th>Description of Goods</th>
            <th>HSN/SAC</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>per</th>
            <th>Disc. %</th>
            <th>Amount</th>
        </tr>
        <tr style='border-bottom-color:white;'>
            <td>1</td>
            <td>Blue HDMI Cable 1.5 Mtr</td>
            <td>8471</td>
            <td><b>3</b><b>nos.</b></td>
            <td>90.00</td>
            <td>nos</td>
            <td>21.875%</td>
            <td>210.94</td>
        </tr>
        <tr style='border-bottom-color:white;'>
            <td>1</td>
            <td>Blue HDMI Cable 1.5 Mtr</td>
            <td>8471</td>
            <td><b>3</b><b> nos.</b></td>
            <td>90.00</td>
            <td>nos</td>
            <td>21.875%</td>
            <td>210.94</td>
        </tr>
        <tr style='border-bottom-color:white;'>
            <td>1</td>
            <td>Blue HDMI Cable 1.5 Mtr</td>
            <td>8471</td>
            <td><b>3</b><b> nos.</b></td>
            <td>90.00</td>
            <td>nos</td>
            <td>21.875%</td>
            <td>210.94</td>
        </tr>
        <tr style='border-bottom-color:white;'>
            <td>1</td>
            <td>Blue HDMI Cable 1.5 Mtr</td>
            <td>8471</td>
            <td><b>3</b><b> nos.</b></td>
            <td>90.00</td>
            <td>nos</td>
            <td>21.875%</td>
            <td>210.94</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Blue HDMI Cable 1.5 Mtr</td>
            <td>8471</td>
            <td><b>3</b><b> nos.</b></td>
            <td>90.00</td>
            <td>nos</td>
            <td>21.875%</td>
            <td>210.94</td>
        </tr>
        <tr>
            <td></td>
            <td>Less: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCGST</b><br><b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SGST</b><br><b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Round Off</b><br></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> <b>191.61</b><br> <b>191.61</b><br> <b>(-)0.07</b><br></td>
        </tr><!-- 
        <tr>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
        </tr> -->
        <tr>
            <td></td>
            <td>Total</td>
            <td></td>
            <td>15 nos</td>
            <td></td>
            <td></td>
            <td></td>
            <td> <b>₹</b> <b>2115.00</b></td>
        </tr>
        <tr>
            <td colspan="8">Amount Chargeable(in words) &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                E.&O.E <br><b>INR Two Thousand One Hundred Fifteen Only</b> </td>
        </tr>
        <tr>
            <th colspan="3" rowspan="2">HSN/SAC</th>
            <th rowspan="2">Taxable Value</th>
            <th colspan="2">Central Tax</th>
            <th colspan="2">State Tax</th>
            <tr>
                <td>Rate</td>
                <td>Amount</td>
                <td>Rate</td>
                <td>Amount</td>
            </tr>
        </tr>

        <tr>
            <td colspan="3">8471</td>
            <td>210.94</td>
            <td>14%</td>
            <td>29.53</td>
            <td>14%</td>
            <td>29.53</td>
        </tr>
        <tr>
            <td colspan="3"> <b> Total</b></td>
            <td><b> 1731.85</b></td>
            <td> </td>
            <td><b> 191.61 </b></td>
            <td> </td>
            <td><b>191.61</b></td>
        </tr>
        <tr>
            <td colspan="8">Tax Amount(in words):<b>INR Three Hundred Eighty Three and Twenty Two paise only</b> </td>
        </tr>
        <tr>
            <td colspan="4">Company's PAN : <b>AGUO010152</b> <br>
                <u> Declaration</u> <br> We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</td>
            <td colspan="4" align="right"><b>for Ramdev Enterprises</b><br><br><br>Authorised Signatory</td>
        </tr>
        <tr>
            <td colspan="8" align="center">This is Computer Generated Invoice</td>
        </tr>
    </table>
	       </div>
	       <div id="imageDivId"></div>
        <p><button class="btn example1">Test on this page</button></p>

        <p class="lead">
          Rendering the body element and restrict canvas size to 300x300px
        </p>

        <pre class="prettyprint linenums">html2canvas(document.body, {
  onrendered: function(canvas) {
    document.body.appendChild(canvas);
  },
  width: 300,
  height: 300
});</pre>
        <p><button class="btn example2">Test on this page</button></p>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
        <script type="text/javascript" src="resources/js/html2canvas.js"></script>
        <script type="text/javascript" src="resources/js/prettify.js"></script>
        <script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
       <!--  <script type="text/javascript" src="resources/js/canvas2image.js"></script> -->
        <script>
            window.prettyPrint();
            $(".example1").on("click", function(event) {
                event.preventDefault();
                html2canvas($('#tableData1'), {
                    allowTaint: true,
                    taintTest: false,
                    onrendered: function(canvas) {
                        document.body.appendChild(canvas);
                        
                        var imgData = canvas.toDataURL("image/jpeg", 1.0);
                        var pdf = new jsPDF();

                        pdf.addImage(imgData, 'JPEG', 10, 10);

                        pdf.save('Invoice.pdf');
                        
                        /* var d=canvas.toDataURL("image/png");
                        
                        $('#imageDivId').html("<img src='"+d+"' alt='Invoice'/>");
                        var img = document.createElement("img");
                        img.src=d;
                        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
            			
            			mywindow.document.write('<html><head><title> Print </title>');
            			mywindow.document.write('</head><body >');
            			mywindow.document.write('<h1> Print  </h1>'); 
            			mywindow.document.write(img);
            			mywindow.document.write('</body></html>');

            			mywindow.document.close(); // necessary for IE >= 10
            			mywindow.focus(); // necessary for IE >= 10

            			mywindow.print();
            			mywindow.close(); */
                        
                        
                    }
                }); 
                
                /* var scaleBy = 5;
                var w = 1000;
                var h = 1000;
                var div = document.querySelector('#tableData1');
                var canvas = document.createElement('canvas');
                canvas.width = w * scaleBy;
                canvas.height = h * scaleBy;
                canvas.style.width = w + 'px';
                canvas.style.height = h + 'px';
                var context = canvas.getContext('2d');
                context.scale(scaleBy, scaleBy);

                html2canvas(div, {
                    canvas:canvas,
                    onrendered: function (canvas) {
                        theCanvas = canvas;
                        document.body.appendChild(canvas);

                        Canvas2Image.saveAsPNG(canvas);
                        $(body).append(canvas);
                    }
                }); */
            });

            $(".example2").on("click", function(event) {
                event.preventDefault();
                html2canvas(document.body, {
                    allowTaint: true,
                    taintTest: false,
                    onrendered: function(canvas) {
                        document.body.appendChild(canvas);
                        
                        var imgData = canvas.toDataURL("image/PNG",0.3);
                        var pdf = new jsPDF("p", "mm", "a4");                       
                        
                        pdf.setProperties({
                            title: 'Order Invoice',
                            subject: 'Issued Product Invoice',
                            author: 'BlueSquare',
                            keywords: 'generated, javascript, web 2.0, ajax',
                            creator: 'BlueSquare'
                        });
                        
                        var width = pdf.internal.pageSize.width;    
                        var height = pdf.internal.pageSize.height;
                        
                        //pdf.addImage(imgData, 'PNG', 6,1,(width*2)-(width/1),height);
                        pdf.addImage(imgData, 'PNG', 6,1,width,height);
                        var currentDate = new Date();
                        var day = currentDate.getDate();
                        var month = currentDate.getMonth() + 1;
                        var year = currentDate.getFullYear();

                        var d = day + "-" + month + "-" + year;
                        
                        pdf.save('abc'+d+'.pdf');  
                        
                    },
                    width: 300,
                    height: 300
                });
            });

        </script>

      </div>

    </div>
  </div>
  <script src="resources/js/prettify.js"></script>

</div>
<footer>
  <div class="container">
    <p>
      Created by <a href="http://hertzen.com">Niklas von Hertzen</a> <br />
      Licensed under the <a href="LICENSE">MIT License</a>. <br />
      <a href="https://twitter.com/Niklasvh" class="twitter-follow-button" data-show-count="false">Follow @Niklasvh</a>
    </p>

    <script>
        var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-188600-10']);_gaq.push(['_trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();

        (function(d, s, id) {
            var po = d.createElement(s); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var e = d.getElementsByTagName(s)[0]; e.parentNode.insertBefore(po, e);

            var js;
            if(!d.getElementById(id)){
                js=d.createElement(s);
                js.id=id;
                js.src="//platform.twitter.com/widgets.js";
                e.parentNode.insertBefore(js,e);
            }

        })(document, "script", "twitter-wjs");
    </script>
    <script src="//hertzen.com/js/heatmap.min.js"></script>
  </div>
</footer>
</body>
</html>