<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

			<%@ page import="java.util.*"%>
				<%@ page import="java.text.*"%>
					<html>

					<head>
						<%@include file="components/header_imports.jsp"%>

							<style>
								.containerOfBanking {
									margin: 0 auto;
									width: 85% !important;
								}

								.addMarginRight {
									margin-right: 5%;
								}

								#cnfrmModal {
									margin-top: 1% !important;
									height: 80% !important;
								}

								#cnfrmModal th,
								#cnfrmModal td {
									padding: 7px 10px !important;
								}
								table ,th ,td{
								border : 1px solid black;
								}
							</style>
					</head>

					<body>
						<%@include file="components/navbar.jsp" %>
						<main class="paddingBody">
							<br>
							<div class="container z-depth-3">
							
							<div class="row">
							
								<br>
								<div class="input-field col l4 m5  offset-l2 offset-m1 s12">
									<input id="partyName" type="text" class="validate">
									 <label for="partyName">Party Name <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 m5 s12">
									<input id="amount" type="text" class="validate num"> <label for="amount">Amount <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 offset-l2 m5 offset-m1 s12">
									<select id="paymentMode" onchange="chequeFields();">
										<option value="CASH">CASH</option>
										<option value="CHEQUE">CHEQUE</option>
										<option value="NEFT">NEFT</option>
										<option value="IMPS">IMPS</option>
										<option value="UPI">UPI</option>
										<option value="RTGS">RTGS</option>
										<option value="Wallet">Wallet</option>
									</select> 
									<label class="active">Payment Mode <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 m5 s12 referenceField">
									<input id="refChqNo" type="text" class="validate"> <label for="refChqNo">Ref No. / Cheque No. <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 offset-l2 m5 offset-m1 s12 chequeFields">
									<input id="bankName" type="text" class="validate"> <label for="bankName">Bank Name <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 m5 s12 chequeFields">
									<input id="depositDate" type="text" class="datepicker validate customerChequeDate"> <label for="depositDate">Cheque Deposit / Issue Date <span class="red-text">*</span></label>
								</div>
								<div class="input-field col l4 m5 s12 paymentDropDown">
									<select id="paymentType">
									<option value="Credit">Credit</option>
									<option value="Debit">Debit</option>
									
								</select>
								 <label class="active">Payment Type <span class="red-text">*</span></label>
									<br>
								</div>
								<div class="row">
								<div class="input-field col l12 m12 s12 center">
									<a id="submit" class="waves-effect waves-light btn blue-gradient" onclick="submitAllData();" style="margin-bottom:2%;">Submit</a>
									<br>									
								</div>
								</div>
								
							</div>
						</div>
							<!-- Modal Structure -->
							<div id="cnfrmModal" class="modal modal-fixed-footer endCurve">
								<div class="modal-content">
									<h5 class="center-align" style="margin-top: 0px !important;">
										<u>Are you sure for below transaction?</u><i class="material-icons notranslate right modal-close  waves-effect waves-red">close</i>
									</h5>
									<div class="row noMarginBottom">
										<div class="col s12 m12 l12 center">
											<table>
												<tr>
													<th>Party Name</th>
													<td></td>
												</tr>
												<tr>
													<th>Amount</th>
													<td></td>
												</tr>
												<tr>
													<th>Payment Mode</th>
													<td></td>
												</tr>
												<tr>
													<th>Ref No. / Cheque No.</th>
													<td></td>
												</tr>
												<tr>
													<th>Bank Name</th>
													<td></td>
												</tr>
												<tr>
													<th>Cheque Date</th>
													<td></td>
												</tr>
												<tr>
													<th>Payment Type</th>
													<td></td>
												</tr>
												<tr>
													<th>Credit / Debit</th>
													<td></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<a href="#!" class="waves-effect waves-green btn red">Cancel</a> <a href="#!" class="modal-action modal-close waves-effect waves-green btn">Confirm</a>
								</div>
							</div>
							<!--modal ends-->
						</main>
						<%@include file="components/footer.jsp"%>

							<script>
								var partyName, amount, paymentMode, refChqNo, bankName, depositDate, paymentType, creditAmount, debitAmount;
								$(document).ready(function () {
									chequeFields();
									
									 //allowed only numbers 
									$('#amount').keypress(function( event ){
									    var key = event.which;
									    
									    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
									        event.preventDefault();
									});  
								    
								});

								function chequeFields() {
									if ($("#paymentMode option:selected").val() == "CHEQUE") {
										$(".chequeFields,.referenceField").show();
										$(".paymentDropDown").addClass("offset-l2 offset-m1");
									} else if($("#paymentMode option:selected").val() == "CASH") {
										$(".chequeFields,.referenceField").hide();
										$(".paymentDropDown").removeClass("offset-l2 offset-m1");
										depositDate = null;
										bankName = null;
									}
									else {
										$(".chequeFields").hide();
										$(".referenceField").show();
										$(".paymentDropDown").addClass("offset-l2 offset-m1");
										depositDate = null;
										bankName = null;
									}
								}

								function submitAllData() {
									$("#submit").hide();
									partyName = $("#partyName").val();
									amount = $("#amount").val();
									paymentMode = $("#paymentMode option:selected").val();
									refChqNo = $("#refChqNo").val();
									bankName = $("#bankName").val();
									depositDate = $("#depositDate").val();
									paymentType = $("#paymentType option:selected").val();
										
									if(partyName=="" || amount == ""){
										Materialize.Toast.removeAll();
										Materialize.toast('All fields are mandatory','3000', 'toastError');	
										$("#submit").show();
										return false;
									}
								
									if (paymentType == "Credit") {
										creditAmount = amount;
										debitAmount = 0;
									} else if (paymentType == "Debit") {
										creditAmount = 0;
										debitAmount = amount;
									}

									var bankAccountTransaction = {
										partyName: partyName,
										credit: creditAmount,
										debit: debitAmount,
										payMode: paymentMode,
										referenceNo: refChqNo,
										bankName: bankName,
										chequeDate: depositDate
									}

									$.ajax({
										type: 'POST',
										url: "${pageContext.request.contextPath}/insertTransaction",
										headers: {
											'Content-Type': 'application/json'
										},
										data: JSON.stringify(bankAccountTransaction),
										success: function (resultant) {
											if (resultant.success == true) {
												console.log(resultant);
												Materialize.Toast.removeAll();
												Materialize.toast('Transaction added successfully','3000', 'toastAlign');	
												setTimeout(
														function() {
															window.location.href = "${pageContext.request.contextPath}/banking"
														}, 500);
											}
											else{
												Materialize.Toast.removeAll();
												Materialize.toast(resultant.msg,'3000', 'toastError');	
												$("#submit").show();
											}
										},
										error: function (err) {
											/*Show Dialog for Server Error*/
											Materialize.Toast.removeAll();
											Materialize.toast('Internal Server Error',
												'3000', 'toastError');
										}
									});
								}
							</script>
					</body>

					</html>