<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bluesquare.rc.dao.ProductDAO"%>
<%@page import="com.bluesquare.rc.models.CalculateProperTaxModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
        <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
    <script>
    var productList = [];
    var nameValid=true;   
    var gstinValid=true;
    var count = 1;
    var productListForCal={};
        $(document).ready(function() {
        	//validation of form
        	$('#gstin').keypress(function( event ) {  
        		if(!/[0-9a-zA-Z-]/.test(String.fromCharCode(event.which)))
        			return false;
        	});
        	$.validator.setDefaults({
      	       ignore: []
      		}); 
        	jQuery.validator.addMethod("nameCheck", function(value, element){
        		if(value=='' || value==undefined)
        		{
        			return false;
        		}		
        	    return checkSupplierDuplication(value,"name");
        	}, "Name Is already in use"); 
        	jQuery.validator.addMethod("gstCheck", function(value, element){
        		if(value=='' || value==undefined)
        		{
        			return true;
        		}		
        	    return checkSupplierDuplication(value,"gstNo");
        	}, "gst no. Is already in use"); 
        	jQuery.validator.addMethod("emailIdCheck", function(value, element){
        		if(value=='' || value==undefined)
        		{
        			return true;
        		}		
        	    return checkSupplierDuplication(value,"emailId");
        	}, "Email Id Is already in use"); 
        	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
        		if(value=='' || value==undefined)
        		{
        			return false;
        		}		
        	    return checkSupplierDuplication(value,"mobileNumber");
        	}, "Mobile Number Is already in use"); 
      	//$('select').change(function(){ $('select').valid(); });
      	$('#updateSupplierForm').validate({
      		rules: {    		    
      			name:{
     				nameCheck:true, 
     				required:true
                },
                emailId:{
                	emailIdCheck:true
                },
                mobileNumber:{
                	mobileNumberCheck:true,
                	required:true
                },
                gstinNo:{
                	gstCheck:true
                }
    		  },
    			
      	
      	    errorElement : "span",
      	    errorClass : "invalid error",
      	    errorPlacement : function(error, element) {
      	      var placement = $(element).data('error');
      	    
      	      if (placement) {
      	        $(placement).append(error)
      	      } else {
      	        error.insertAfter(element);
      	      }
      	      $('select').change(function(){ $('select').valid(); });
      	    }
      	  });
            //mobile number allowed only number without decimal
            $('#mobileNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
            //rate allowed only number with decimal
            $('#rate').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
          //rate allowed only number with decimal upto 2 digit
            $('#rate').keyup(function(e){			            	
	           	var value=this.value.split('.');
            	if(value.length>1){
            		if(value[1].length>2){
            			value[1]=value[1].substring(0,2);
            			this.value=value[0]+"."+value[1];
            		}
            	}
			});
          //rate allowed only number with decimal
            $('#rate').keypress(function(e){
            	if (e.keyCode === 46 && this.value.split('.').length == 2) {
              		 return false;
          		 }
            });
            //set added product details list in productList[]
            var prdctlst="${productidlist}";  
            if(prdctlst!='' && prdctlst!=undefined)
			 {
            	var tempPrdctAndRateList=prdctlst.split(',');
            	
            	for(var t=0; t<tempPrdctAndRateList.length; t++)
            	{
            		var tempPrdctAndRate=tempPrdctAndRateList[t].split('-');
            		var prdData=[tempPrdctAndRate[0],tempPrdctAndRate[1]];
            		productList.push(prdData);
            	}
            	//alert(productList.entries());
			 }
            var cout="${count}";
            if(cout!='' && cout!=undefined)
			 {
            	count=cout;
	           	//alert(count);
			 }
          //on product add button click
 			$("#productAddId").click( function() {
 			// if button name found update then go for update product details
            	var buttonStatus=$('#productAddButton').text();
            	if(buttonStatus==="Update")
            	{
            		updaterow($('#currentUpdateProductId').val());
            		return false;
            	}
            	$("#productAddId").html('<i class="material-icons left">add</i><span id="productAddButton">Add</span>');
                var val = $("#productid").val(); 
                var rate = $('#rate').val();
                var mrp = $('#mrp').val();
              	
              	//alert(buttonStatus);
              	if(val==0)
              	{
					Materialize.Toast.removeAll();
              		Materialize.toast('Select product', '4000', 'teal lighten-2');
              		return false;
              	}
              	if(parseFloat(rate)==0)
              	{
					Materialize.Toast.removeAll();
              		Materialize.toast('Enter MRP', '4000', 'teal lighten-2');
              		return false;
              	}
              	/* if(jQuery.inArray(val, productidlist) !== -1)
                {	
              		$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Product Select Warning");
           	     	$('#msg').text("This Product is already added");               	     	
                } */
              	else
           		{
	                
              		
              		//alert(productList.size());
              		/* for (let key of productList.keys()) {
              		    alert(key+"-"+productList[key]);
              		}*/
              		//alert(productList.entries());
              		/* for (let entry of productList.entries()) {
              			alert(entry[0]+"-"+ entry[1]);
              		} */
              		/* for (let [key, value] of productList.entries()) {
              			alert(key+"-"+value);
              			productList.remove(key);
              		} */
	             	//alert(productList.key +"-"+ productList.value);
	             	 
              	    //product already exist or not validation
              		for (var i=0; i<productList.length; i++) {  
              			var value=productList[i];  
              			if(value[0]===val && buttonStatus==="Add")
              			{  
							Materialize.Toast.removeAll();
              				Materialize.toast('This Product is already added!', '4000', 'teal lighten-2');
              				/* $('#addeditmsg').modal('open');
                   	     	$('#msgHead').text("Product Select Warning");
                   	     	$('#msg').text("This Product is already added");  */
                   	     	return false;
              			}
              		}
              	   //add product details in productList[]
              		var prdData=[val,rate];
              		productList.push(prdData);
              		
	             	/* $('#productratelistinput').val(productRateidlist);
	             	$('#productlistinput').val(productidlist); */
	                var text=$("#productid option:selected").text();
	                var vl=$("#productid option:selected").val();
					//alert(value+"-"+text);
					var rowCount = $('#productcart tr').length;
					
					//add product details on table
	                $("#t1").append("<tr id='rowdel_" + count + "' >"+
		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
		               				"<td >" + mrp + "</td>"+
		               				"<td id='rowproductrate_" + count + "'><input type='hidden' id='rowproductmrp_" + count + "' value='"+mrp+"'>" + rate + "</td>"+
		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
		               				"</tr>");
	                count++;
           		}
              	
                //add product details in productlistinput(form field)
              	var productIdList="";
            	for (var i=0; i<productList.length; i++) {
            		var value=productList[i];
            		productIdList=productIdList+value[0]+"-"+value[1]+",";
          		}
            	productIdList=productIdList.slice(0,-1)
            	//alert(productIdList);
            	$('#productlistinput').val(productIdList);
            	$('#saveSupplierForm').validate();
             	
              	//alert(productList.entries());
              	
              	//reset product details adding field
            	resetFields();
				
            });

 			//update row of given id
		 function updaterow(id) {
		 	$("#productAddId").html('<i class="material-icons left">add</i><span id="productAddButton">Add</span>');
		     var val = $("#productid").val(); 
		     var rate = $('#rate').val();
		     var mrp = $('#mrp').val();
		   	var buttonStatus=$('#productAddButton').text();
		   	var text=$("#productid option:selected").text();
		     var vl=$("#productid option:selected").val();
		   	
		   	//alert(buttonStatus);
		   	if(val==0)
		   	{
		   		return false;
		   	}
		   	if(parseFloat(rate)==0)
		   	{
		   		return false;
		   	}
		 	
		   	//alert($('#rowproductkey_' + id).val());
		   	var removeItem=$('#rowproductkey_' + id).val();
		  //skip current updating object
		   	var productListTemp=[];
		   	for(var i=0; i<productList.length; i++)
		   	{
		   		var value=productList[i];
		   		if(removeItem!==value[0])
		   		{
		   			productListTemp.push(productList[i]);
		   		}
		   	}
		   	
		  //add updated product details
		   	productList=[];
		   	for(var i=0; i<productListTemp.length; i++)
		   	{
		   		productList.push(productListTemp[i]);
		   	}
		   	var prdData=[val,rate];
		   	productList.push(prdData);
		   	
		     //alert('#rowproductkey_'+id);
		     
		     //alert('removeItem '+$('#rowproductkey_' + id).val());
		     //alert('productidlist '+productidlist);
		     
		     var rowCount = $('#t1 tr').length;
		 	//alert(rowCount);
		 	var trData="";
		 	count=1;
		 	
		 	//reset product details on table
		 	for(var i=1; i<=rowCount; i++)
		 	{
		 		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		 		
		 		if(id!=i)
		 		{
		 			//alert("predata");
		 			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
		     		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
		        				"<td id='rowcount_" + count + "'>" + count + "</td>"+
		        				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
		        				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
		        				"</tr>"; */
		        				trData=trData+"<tr id='rowdel_" + count + "' >"+
			               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
			               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
			               				"<td>"+$('#rowproductmrp_' + i).val()+ "</td>"+
			               				"<td id='rowproductrate_" + count + "'><input type='hidden' id='rowproductmrp_" + count + "' value='"+$('#rowproductmrp_' + i).val()+"'>" + $('#rowproductrate_'+i).text() + "</td>"+
			               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
			               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
			               				"</tr>";
		     		 count++;
		 		}
		 		else
		 		{
		 			//alert("newdata");
		 			trData=trData+"<tr id='rowdel_" + count + "' >"+
			               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
			               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
			               				"<td>"+mrp+"</td>"+
			               				"<td id='rowproductrate_" + count + "'><input type='hidden' id='rowproductmrp_" + count + "' value='"+mrp+"'>" + rate + "</td>"+
			               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
			               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
			               				"</tr>";
		     		 count++;
		 		}
		 		//alert(trData);
		 	} 
		 	//add product details in productlistinput(form field)
		 	var productIdList="";
		 	for (var i=0; i<productList.length; i++) {
		 		var value=productList[i];
		 		productIdList=productIdList+value[0]+"-"+value[1]+",";
				}
		 	productIdList=productIdList.slice(0,-1)
		 	//alert(productIdList);
		 	$('#productlistinput').val(productIdList);
		 	
		 	$("#t1").html('');
		 	$("#t1").html(trData);
		 	//alert(productList.entries());
		     //$('#rowdel_' + id).remove();
		    // alert('productidlist '+productidlist);
		    
		    //reset product details adding field
			resetFields();
		 }
		// on category change fetch product list by brand id and category id
            $('#categoryid').change(function() {
    			var brandid = $('#brandid').val();
    			var categoryid = $('#categoryid').val();

    			/* if(brandid==0)
    			{
    				return false;
    			}
    			
    			if(categoryid==0)
    			{
    				return false;
    			} */
    			// Get the raw DOM object for the select box
    			var select = document.getElementById('productid');

    			// Clear the old options
    			select.options.length = 0;

    			//Load the new options

    			select.options.add(new Option("Choose Product", 0));
    			$.ajax({
    				url : "${pageContext.request.contextPath}/fetchProductListByBrandIdAndCategoryId?brandId=" + brandid+"&categoryId="+categoryid,
    				dataType : "json",
    				async:false,
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    				success : function(data) {
    					
    					productListForCal=data;
    					
    					/* alert(data); */
    					var options, index, option;
    					select = document.getElementById('productid');
    					
    					// Clear the old options
    	    			select.options.length = 0;
    	    			select.options.add(new Option("Choose Product", '0'));
    	    			
    					for (var i = 0, len = data.length; i < len; ++i) {
    						var product = data[i];
    						select.options.add(new Option(product.productName, product.productId));
    					}

    					/* for (index = 0; index < options.length; ++index) {
    					  option = options[index];
    					  select.options.add(new Option(option.name, option.cityId));
    					} */
    					$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
    				},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						  Materialize.Toast.removeAll();
						  Materialize.toast('Product List Not Found!', '2000', 'teal lighten-2');
						/* $('#addeditmsg').modal('open');
               	     	$('#msgHead').text("Message : ");
               	     	$('#msg').text("Product List Not Found");  */
               	     		/* setTimeout(function() 
							  {
      	     					$('#addeditmsg').modal('close');
							  }, 1000); */
						}
    			});

    		});
         // on brand change fetch product list by brand id and category id
            $('#brandid').change(function() {
    			var brandid = $('#brandid').val();
    			var categoryid = $('#categoryid').val();

    			/* if(brandid==0)
    			{
    				return false;
    			}
    			
    			if(categoryid==0)
    			{
    				return false;
    			} */
    			// Get the raw DOM object for the select box
    			var select = document.getElementById('productid');

    			// Clear the old options
    			select.options.length = 0;

    			//Load the new options

    			select.options.add(new Option("Choose Product", '0'));
    			$.ajax({
    				url : "${pageContext.request.contextPath}/fetchProductListByBrandIdAndCategoryId?brandId=" + brandid+"&categoryId="+categoryid,
    				dataType : "json",
    				async:false,
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    				success : function(data) {

    					productListForCal=data;
    					
    					/* alert(data); */
    					var options, index, option;
    					select = document.getElementById('productid');
    					
    					// Clear the old options
    	    			select.options.length = 0;
    	    			select.options.add(new Option("Choose Product", '0'));
    	    			
    					for (var i = 0, len = data.length; i < len; ++i) {
    						var product = data[i];
    						select.options.add(new Option(product.productName, product.productId));
    					}

    					/* for (index = 0; index < options.length; ++index) {
    					  option = options[index];
    					  select.options.add(new Option(option.name, option.cityId));
    					} */
    					$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
    				},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						 // alert(error +"---"+ xhr+"---"+status);
						 Materialize.Toast.removeAll();
						  Materialize.toast('Product List Not Found!', '2000', 'teal lighten-2');
						/* $('#addeditmsg').modal('open');
               	     	$('#msgHead').text("Message : ");
               	     	$('#msg').text("Product List Not Found"); 
               	     		 setTimeout(function() 
									  {
               	     					$('#addeditmsg').modal('close');
									  },1000);*/
						} 
    			});

    		});
            

            //on submiting product details set on form 
            $('#saveSupplierSubmit').click(function(){
            	
            	var productlistinput=productList.entries();
            	//alert(productlistinput);
            	if(productlistinput=='' || productlistinput==undefined)
           	 	{
					Materialize.Toast.removeAll();
            		Materialize.toast('Select Atlist 1 Product!', '2000', 'teal lighten-2');
           	    /*  $('#addeditmsg').modal('open');
           	     $('#msgHead').text("Supplier Adding Message");
           	     $('#msg').text("Select Atlist 1 Product"); */
           	     return false;
           	 	}
            	//alert(productList.entries());
            	var productIdList="";
            	for (var i=0; i<productList.length; i++) {
            		var value=productList[i];
            		productIdList=productIdList+value[0]+"-"+value[1]+",";
          		}
            	productIdList=productIdList.slice(0,-1)
            	//alert(productIdList);
            	$('#productlistinput').val(productIdList);
            	
            });
          //only number allowed without decimal
            $('#mrp').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
			        event.preventDefault();
			});
          //on product change execute mrp key up event
			  $("#productid").change(function(){
				  $("#mrp").keyup();
			  });
            
			  /* for calculating amount of product */
				$("#mrp").keyup(function(){
									
					var productId=$("#productid").val();
					if(productId=='0' || productId==undefined)
					{
						$('#igstPer').val('');
						$('#igstPer').change();
						$("#rate").val('');
						$("#rate").change();
						$("#mrp").val('');
						$("#mrp").change();
						return false;
					}				
					
					var product;
					for(var i=0; i<productListForCal.length; i++){
						if(productId==productListForCal[i].productId){
							product=productListForCal[i];
							$('#igstPer').val(product.categories.igst);
							$('#igstPer').change();
						}
					}
					
					var mrp=$("#mrp").val();
					if(mrp==undefined || mrp==''){
						mrp=0;
					}
					mrp=parseInt(mrp);
					
					var correctAmoutWithTaxObj=calculateProperTax(mrp,product.categories.igst);
					
					$("#rate").val(correctAmoutWithTaxObj.unitPrice);
					$("#rate").change();
					$("#cgst").val(correctAmoutWithTaxObj.cgst);
					$("#cgst").change();
					$("#sgst").val(correctAmoutWithTaxObj.sgst);
					$("#sgst").change();
					$("#igst").val(correctAmoutWithTaxObj.igst);
					$("#igst").change();
				});   
	            
				/* for calculating amount of product */
				$("#rate").keyup(function(){
									
					var productId=$("#productid").val();
					if(productId=='0' || productId==undefined)
					{
						$('#igstPer').val('');
						$('#igstPer').change();
						$("#rate").val('');
						$("#rate").change();
						$("#mrp").val('');
						$("#mrp").change();
						return false;
					}				
					
					var product;
					for(var i=0; i<productListForCal.length; i++){
						if(productId==productListForCal[i].productId){
							product=productListForCal[i];
							$('#igstPer').val(product.categories.igst);
							$('#igstPer').change();
						}
					}
					
					var rate=$("#rate").val();
					if(rate==undefined || rate==''){
						rate=0;
					}
					var mrp=(parseFloat(rate)+((parseFloat(rate)*parseFloat(product.categories.igst))/100)).toFixedVSS(0);
					
					var correctAmoutWithTaxObj=calculateProperTax(mrp,product.categories.igst);
					
					$("#mrp").val(mrp);
					$("#mrp").change();
					$("#cgst").val(correctAmoutWithTaxObj.cgst);
					$("#cgst").change();
					$("#sgst").val(correctAmoutWithTaxObj.sgst);
					$("#sgst").change();
					$("#igst").val(correctAmoutWithTaxObj.igst);
					$("#igst").change();
				});
            
            //onload fetch product list
			/* $('#brandid').change(); */
			$('#categoryid').change();
			
			$('#productResetId').click(function(){
				resetFields();
			});
        });
        //on edit product details button click
        //fill product with mrp,tax and unit price
        function editrow(id) {
        	$(".btn-flat").removeAttr('disabled','disabled');
        	$("#rowdelbutton_"+id).find('button').attr('disabled','disabled');
        	$('#currentUpdateProductId').val(id);
        	var productId=$('#rowproductkey_'+id).val();
        	//alert($('#rowproductrate_'+id).val());
        	
        	$('#mrp').val($('#rowproductmrp_'+id).val());
        	$('#mrp').change();
        	
        	/* $('#rate').val($('#rowproductrate_'+id).text());
        	$('#rate').focus();
        	$('#rate').trigger('blur'); */ 
        	$("#productAddId").html('<i class="material-icons left">send</i><span id="productAddButton">Update</span>');
    		$.ajax({
    			type : "GET",
    			url : "${pageContext.request.contextPath}/fetchProductByProductId?productId="+productId,
    			/* data: "id=" + id + "&name=" + name, */
    			beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    			success : function(data) {
    				product=data;

    				var source2 = $("#categoryid");
    				var v2=product.categories.categoryId;
    				source2.val(v2);
    				source2.change();
    				
    		    	var source = $("#brandid");
    				var v1=product.brand.brandId;
    				source.val(v1);
    				source.change();		

    			
    				setTimeout(
    						  function() 
    						  {
    						    //do something special
    							  var source3 = $("#productid");
    								var v3=product.productId;
    								source3.val(v3);
    								source3.change();
    								//alert(v);
    								//alert($("#stateListForCity").val());
    						  }, 1000);
    				$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
    				
    			},error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					 // alert(error +"---"+ xhr+"---"+status);
					 $('#addeditmsg').find("#modalType").addClass("warning");
					$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
					$('#addeditmsg').modal('open');
           	     	/* $('#msgHead').text("Message : "); */
           	     	$('#msg').text("Something Went Wrong "); 
           	     		 setTimeout(function() 
								  {
           	     					$('#addeditmsg').modal('close');
								  },1000);
					}

    		});
        	
        }
     // delete row of given id
        function deleterow(id) {
        	
            //alert('#rowproductkey_'+id);
            var removeItem=$('#rowproductkey_' + id).val();
            //alert('removeItem '+$('#rowproductkey_' + id).val());
            //alert('productidlist '+productidlist);
           
            //skip current updating product details
            var productListTemp=[];
           	for(var i=0; i<productList.length; i++)
           	{
           		var value=productList[i];
           		if(removeItem!==value[0])
           		{
           			productListTemp.push(productList[i]);
           		}
           	}
          //add updated product details
           	productList=[];
           	for(var i=0; i<productListTemp.length; i++)
           	{
           		productList.push(productListTemp[i]);
           	}
            var rowCount = $('#t1 tr').length;
        	//alert(rowCount);
        	var trData="";
        	count=1;
        	//update product details list on table
        	for(var i=1; i<=rowCount; i++)
        	{
        		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
        		
        		if(id!==i)
        		{
        			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
	        		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
	           				"<td id='rowcount_" + count + "'>" + count + "</td>"+
	           				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
	           				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
	           				"</tr>"; */
	           				trData=trData+"<tr id='rowdel_" + count + "' >"+
		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
		               				"<td>"+$('#rowproductmrp_' + i).val()+"</td>"+
		               				"<td id='rowproductrate_" + count + "'><input type='hidden' id='rowproductmrp_" + count + "' value='"+$('#rowproductmrp_' + i).val()+"'>" + $('#rowproductrate_'+i).text() + "</td>"+
		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
		               				"</tr>";
	        		 count++;
        		}
        		//alert(trData);
        	} 
        	//add product details in productlistinput(form field)
        	var productIdList="";
        	for (var i=0; i<productList.length; i++) {
        		var value=productList[i];
        		productIdList=productIdList+value[0]+"-"+value[1]+",";
      		}
        	productIdList=productIdList.slice(0,-1)
        	//alert(productIdList);
        	$('#productlistinput').val(productIdList);
        	$("#t1").html('');
        	$("#t1").html(trData);
        	//alert(productList.entries());
            //$('#rowdel_' + id).remove();
          // alert('productidlist '+productidlist);
           
            //reset product details fields
			resetFields();
        }
        
        function resetFields(){
        	$('#mrp').val(0);
          	$('#mrp').change();
          	$('#rate').val(0);
          	$('#rate').change();
          	$('#igst').val(0);
          	$('#igst').change();
          	$('#cgst').val(0);
          	$('#cgst').change();
          	$('#sgst').val(0);
          	$('#sgst').change();
          	var source3 = $("#productid");
			source3.val(0);
			source3.change();
			
			var source2 = $("#categoryid");
			source2.val(0);
			source2.change();
			
	    	var source = $("#brandid");
			source.val(0);
			source.change();	
			
			$(".btn-flat").removeAttr('disabled','disabled');
			
			$("#productAddId").html('<i class="material-icons left">add</i><span id="productAddButton">Add</span>');
        }
      //check for duplication
        /**
         * name
         * gst number
         * mobile number
         * email id
         */
        function checkSupplierDuplication(checkText,type){
        	var status=false;
        	$.ajax({
        		url : "${pageContext.servletContext.contextPath}/checkSupplierDuplicationForUpdate?checkText="+checkText+"&type="+type+"&supplierId=${supplier.supplierId}",
        		async:false,
        		success : function(data) {
        			if(data==="Success"){
        				status=true;
        			}else{
        				status=false;
        			}
        		},
        		error: function(xhr, status, error) {
        			alert("Error");
        		}
        	});
        	
        	return status;
        }
    </script>
	<style>
		td, th {
			padding: 8px 5px !important;
		}
	</style>
</head>

<body>
    <!--navbar start-->
     	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
     <main class="paddingBody">
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateSupplier" method="post" id="updateSupplierForm">
            
            <!-- <script type="text/javascript">
            alert("${supplier}");
            </script> -->
            
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Personal Details</h4>
                    </div>
                    <div class="row" style="margin-bottom:0">
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="name" value="<c:out value="${supplier.name}" />" required>
                        <label for="name" class="active"><span class="red-text">*</span>Name</label>
                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" class="validate" name="mobileNumber" minlength="10" maxlength="10"  value="<c:out value="${supplier.contact.mobileNumber}" />" required>
                        <label for="mobileNo" class="active"><span class="red-text">*</span>Mobile No.</label>
                    </div>
                    </div>
                    <div class="row" style="margin-bottom:0">
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" class="validate" name="emailId" value="<c:out value="${supplier.contact.emailId}" />">
                        <label for="emailId"  class="active">Email Id</label>
                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">assistant</i>
                        <input id="gstin" type="text" class="validate" name="gstinNo" minlength="15" maxlength="15" value="<c:out value="${supplier.gstinNo}" />">
                        <label for="gstin" class="active"><!-- <span class="red-text">*</span> -->GST In</label>
                    </div>
</div>
<div class="row" style="margin-bottom:0">
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea" name="address"  required><c:out value="${supplier.address}" /></textarea>
                        <label for="textarea1"><span class="red-text">*</span>Address</label>
                    </div>
						<%-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
	               			 <i class="material-icons prefix">location_on<span class="red-text">*</span></i>
	               			
	               			<select name="stateId" id="stateId" class="validate" required="" aria-required="true" title="Please select Area">
	                                 <option value="" >Select Area</option>
	                                <c:if test="${not empty stateList}">
								<c:forEach var="listValue" items="${stateList}">
									<option value="<c:out value="${listValue.stateId}" />" ${supplier.stateModel.stateId==listValue.stateId?'selected':''}><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
								</c:if>
	                        </select>
             		  </div> --%>
             		  <div class="input-field col s12 m5 l5 push-l1 push-m1">
               			  <i class="fa fa-percent prefix" style="font-size:24px"><span class="red-text">*</span></i>
               			 <span class="selectLabel">Select Tax Type</span>
               			<select name="taxType" id="taxTypeId" class="validate" required="" aria-required="true" title="Please select tax type">
                                 <option value="Intra" ${supplier.taxType=="Intra"?'selected':''}>Intra </option>
                                 <option value="Inter" ${supplier.taxType=="Inter"?'selected':''}>Inter </option>
                        </select>
              		 </div>
				</div>				
                </div>
                             <div class="row z-depth-3">
                	<input type='hidden' id='currentUpdateProductId'>
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                     <div class="row noMargin">
                   
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                         <i class="material-icons prefix">star </i>
						 <span class="selectLabel">Select Brand</span>
                        <select id="brandid" class="select2" name="brandId">
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">filter_list</i>
						<span class="selectLabel">Select Category</span>
                        <select id="categoryid" class="select2" name="categoryId">
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                     </div>
                      <div class="row noMargin">
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
						<i class="material-icons prefix">shopping_cart </i>      
						<span class="selectLabel"> <span class="red-text">*</span> Select Product</span>                
                         <select id="productid" name="productId" class="select2">
                                 <option value="0">Choose Product</option>
                                  <!-- <c:if test="${not empty productlist}">
							<c:forEach var="listValue" items="${productlist}">
								<option value="<c:out value="${listValue.productId}" />"><c:out value="${listValue.productName}" /></option>
							</c:forEach>
							</c:if> -->
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="mrp" type="text" name="rate">
                        <label for="mrp" class="active">MRP</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1" id="igstDiv">
                      <i class="fa fa-inr prefix"></i> 
                        <input id="igstPer" type="text" class="grey lighten-2"  readonly>
                        <label for="igstPer" class="active">Tax Slab</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1" id="unitprice">
						 <i class="fa fa-inr prefix"></i>
                        <input id="rate" type="text" class="grey lighten-2" name="productRate" readonly>
                        <label for="rate" class="active">Unit Price</label>                       
                    </div>
                    </div>
                     <div class="row noMargin">
                     
                    <!--  <div class="input-field col s12 m5 l5 push-l1  push-m1" id="cgstDiv">
                        <i class="fa fa-inr prefix"></i>
                        <input id="cgst" type="text" class="grey lighten-2" readonly>
                        <label for="cgst" class="active">CGST Amount</label>
                    </div>  
                    </div>
                     <div class="row noMargin">
                      <div class="input-field col s12 m5 l5 push-l1  push-m1" id="sgstDiv">
                      <i class="fa fa-inr prefix"></i>
                        <input id="sgst" type="text" class="grey lighten-2" readonly>
                        <label for="sgst" class="active">SGST Amount</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1" id="igstDiv">
                      <i class="fa fa-inr prefix"></i> 
                        <input id="igst" type="text" class="grey lighten-2"  readonly>
                        <label for="igst" class="active">IGST Amount</label>
                    </div> -->
                    </div>
                    <div class="input-field col s12 m4 l4  offset-l4">
                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="productAddId" ><i class="material-icons left">add</i>Add</button>
                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="productResetId" ><i class="material-icons left">refresh</i>Reset</button>
                    </div>
                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
                        <table class="centered tblborder" id="productcart">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>MRP</th>
                                    <th>Unit Price</th>
                                    <th>Edit</th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                            <% int rowincrement=0; %>
                            <c:if test="${not empty supplierProductList}">
								<c:forEach var="supplierProduct" items="${supplierProductList}">
								<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
								<c:set var="reqVal" value="${supplierProduct.supplierRate}" scope="request"/>
								<c:set var="reqIgst" value="${supplierProduct.igst}" scope="request"/>
								<%
									double rate=(double)(request.getAttribute("reqVal"));
									float igst=(float)(request.getAttribute("reqIgst"));
								
									double mrp=rate+((rate*igst)/100);
									mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
									int mrp_int=(int)mrp;
									pageContext.setAttribute("mrp", mrp_int);
								%>
                                <tr id='rowdel_${rowincrement}'> 
		               				<td id='rowcount_${rowincrement}'> <c:out value="${rowincrement}" /> </td>
		               				<td id='rowproductname_${rowincrement}'><input type='hidden' id='rowproductkey_${rowincrement}' value='<c:out value="${supplierProduct.productId}" />'><center><span id='tbproductname_${rowincrement}'><c:out value="${supplierProduct.productName}" /></span></center></td>
		               				<td><c:out value="${mrp}" /></td>
		               				<td id='rowproductrate_${rowincrement}'><input type='hidden' id='rowproductmrp_${rowincrement}' value='${mrp}'><c:out value="${supplierProduct.supplierRate}" /></td>
		               				<td id='rowcountproductedit_${rowincrement}'><button class='btn-flat' type='button' onclick='editrow(${rowincrement})'><i class='material-icons '>edit</i></button></td>
		               				<td id='rowdelbutton_${rowincrement}'><button class='btn-flat' type='button' onclick='deleterow(${rowincrement})'><i class='material-icons '>clear</i></button></td>
		               			</tr>
		               			</c:forEach>
                            </c:if> 
                            </tbody>
                        </table>
                        <br><br>
                        <input id="productlistinput" type="hidden" class="validate" name="productIdList" title="Add atleast one product" required>
                        <input id="supplierId" type="hidden" class="validate" name="supplierId" value="<c:out value="${supplier.supplierId}" />">
                    </div>

                </div>
                <%-- <div class="row z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 pull-m1 ">

                        <i class="material-icons prefix">star</i>
                        <select id="brandid" name="brandId">
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">
                        <i class="material-icons prefix">filter_list</i>
                        <select id="categoryid" name="categoryId">
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
                        <i class="material-icons prefix">shopping_cart</i>
                         <select id="productid" name="productId">
                                 <option value="0">Choose Product</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <!--<div class="input-field col s12 m5 l5 push-l2">
                        <button class="btn waves-effect waves-light blue darken-8" type="button">Ok</button>
                    </div>-->
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
                        <table class="centered" id="productcart">
                            <thead>
                            
	                                <tr>
	                                    <th>Sr.No</th>
	                                    <th>Product</th>
	                                    <th>Cancel</th>
	                                </tr>
	                      
                            </thead>
                            <tbody id="t1">
                            <% int rowincrement=0; %>
						
						
                            <c:if test="${not empty supplierProductList}">
								<c:forEach var="supplierProduct" items="${supplierProductList}">
								<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                                <tr id='rowdel_${rowincrement}'> 
		               				<td id='rowcount_${rowincrement}'> <c:out value="${rowincrement}" /> </td>
		               				<td id='rowproductname_${rowincrement}'><input type='hidden' id='rowproductkey_${rowincrement}' value='<c:out value="${supplierProduct.product.productId}" />'><center><span id='tbproductname_${rowincrement}'><c:out value="${supplierProduct.product.productName}" /></span></center></td>
		               				<td id='rowdelbutton_${rowincrement}'><button class='btn-flat' type='button' onclick='deleterow(${rowincrement})'><i class='material-icons '>clear</i></button></td>
		               				</tr>
		               			</c:forEach>
                            </c:if> 
                            </tbody>
                        </table>
                        
                        <br><br>
                    </div>

                </div> --%>
                <div class="row z-depth-3">
                   <div class="col s12 l12 m12">
                       <div class="col s12 l5 m5 push-l1 push-m1">
                           <h6 style="padding-top:2%;padding-bottom:2%;">Added Date : 
							<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${supplier.supplierAddedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${supplier.supplierAddedDatetime}" /><c:out value="${time}" />                        
                           </h6>
                       </div>
                       <div class="col s12 l6 m6 right-align">
                           <h6 style="padding-top:2%;padding-bottom:2%;">Last Updated Date : 
							<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${supplier.supplierUpdatedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${supplier.supplierUpdatedDatetime}" /><c:out value="${time}" /> 
                           </h6>
                       </div>
                   </div>
               </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" id="saveSupplierSubmit" type="submit">Update Supplier<i class="material-icons right">send</i> </button>

                </div>
                <br>
            </form>

        </div>



  	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>