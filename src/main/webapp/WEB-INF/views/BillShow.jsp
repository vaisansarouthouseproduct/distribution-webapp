<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
	<script>
	  $(document).ready(function () {
		  
		//open print window
		$("#btnprint").click(function(){
	
			var w = window.open("${pageContext.request.contextPath}/Invoice.pdf?orderId=${orderId}");
		      w.print();
		});
			
		//send bill pdf on shop id
		$('#btnsendmail').click(function(){
			
			$.ajax({
	    		url : "${pageContext.request.contextPath}/sendMailIssuedBill?orderId=${orderId}",
	    		beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
	    		/* dataType : "json", */
	    		success : function(data) {
	    			if(data==="Success")
	    			{
	    				$('#addeditmsg').modal('open');
						$('#msgHead').html("Message :  ");
						$('#msg').html("<font color='green'>Mail Send SuccessFully</font>");
	    			}
	    			else
	    			{
	    				$('#addeditmsg').modal('open');
						$('#msgHead').html("Message :  ");
						$('#msg').html("<font color='red'>Mail Sending failed</font>");
	    			}
	    			$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					  //alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Message : ");
           	     	$('#msg').text("Something went Wrong"); 
           	     		setTimeout(function() 
						  {
  	     					$('#addeditmsg').modal('close');
						  }, 1000);
					}
			});
		});
		
		
	  });
	  
	 
	</script>
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    <div class="row">
    <br>
    <div class="col s12 m12 l10 offset-l2 center">
    <div class="col s4 l3 m4">
    	     <button class="btn waves-effect waves-light blue-gradient"  id="btnprint">Print<i class="material-icons left">local_printshop</i> </button>
    </div>
    <div class="col s4 l3 m4">
    	 <a class="btn waves-effect waves-light blue-gradient center" download href="${pageContext.request.contextPath}/Invoice.pdf?orderId=${orderId}" id="btndownload">Download<i class="material-icons left">file_download</i> </a>
    </div>
    <div class="col s4 l3 m4 ">
    	 <button class="btn waves-effect waves-light blue-gradient right"  id="btnsendmail">Send Mail<i class="material-icons left">mail</i> </button>
    </div>
    </div>
    <div class="col s12 m12 l12 center">
    <br><br> 
		<object id="pdfViewer" width="80%" height="600px" data="${pageContext.request.contextPath}/Invoice.pdf?orderId=${orderId}">
			<p>Your web browser doesn't have a PDF plugin.
					Instead you can <a target="_blank" href="${pageContext.request.contextPath}/Invoice.pdf?orderId=${orderId}">click here to
					download the PDF file.</a></p>
       </object> 
       </div>
      
       
      
       
       </div>


		 <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>

    </main>
    <!--content end-->
</body>

</html>