<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
    $(document).ready(function() {
    	var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	     /*  initComplete: function () {
	    	.on('change', function () {
         var val = $.fn.dataTable.util.escapeRegex(
         $(this).val());
	      } */
	      	autoWidth: false,
	         columnDefs: [
	                      { 'width': '1%', 'targets': 0 },
	                      { 'width': '2%', 'targets': 1},
	                      { 'width': '15%', 'targets': 2},
	                  	  { 'width': '10%', 'targets': 3},
	                  	  { 'width': '2%', 'targets': 4},
						  { 'width': '3%', 'targets': 5},
						  { 'width': '2%', 'targets': 6}		                     
	                      ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	         
	         //dom: 'lBfrtip',
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	 doc.content.forEach(function(item) {
  	                    		  if (item.table) {
  	                    		  item.table.widths = [50,90,130,110,80,80] 
  	                    		 } 
  	                    		    })
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];   	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select").change(function() {
          var t = this;
          var content = $(this).siblings('ul').detach();
          setTimeout(function() {
              $(t).parent().append(content);
              $("select").material_select();
          }, 200);
      });
  $('select').material_select();
  $('.dataTables_filter input').attr("placeholder", "Search");
  //if there is mobile device the element contain this class will be hidden
	if ($.data.isMobile) {
		var table = $('#tblData').DataTable(); // note the capital D to get the API instance
		var column = table.columns('.hideOnSmall');
		column.visible(false);
	}
    	 $(".showDates").hide();
         $(".rangeSelect").click(function() {
        	 $("#oneDateDiv").hide();
             $(".showDates").show();
         });
    	$("#oneDateDiv").hide();
    	$(".pickdate").click(function(){
    		 $(".showDates").hide();
    		$("#oneDateDiv").show();
    	});
    	$('#areaId').change(function(){
    		
    		var areaId=$('#areaId').val();	
    		if(areaId==="0")
    		{
    			return false;
    		}
    		
    		var url = "${pageContext.request.contextPath}/permanentReturnOrderReport?areaId="+areaId;
    		window.location.href=url;
    		
    	});
    	
    });
	</script>
	<style>
		.filerMargin{
			margin-top: 0.8rem;
				}
				.input-field {
								position: relative;
								margin-top: 0.5rem;
							}
	</style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
				<div class="col s6 m3 l2 right right-align actionDiv filerMargin">
               <!--  <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                       <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
				<div class="col s6 l2 m4 right right-align actionDiv filerMargin">
             <select id="areaId" name="areaId" class="btnSelect">
             	<option value="0">Area Filter</option>
                 <c:if test="${not empty returnOrderDateRangeResponse.areaList}">
					<c:forEach var="listValue" items="${returnOrderDateRangeResponse.areaList}">
						<option value="<c:out value="${listValue.areaId}" />"><c:out
								value="${listValue.name}" /></option>
					</c:forEach>
				</c:if>
				</select>
            </div>



            
			<div class="input-field col s12 l3 m3 offset-l4 offset-m3 center actionDiv" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/permanentReturnOrderReport" method="post">
	                    <div class="input-field col s5 m8 l7 offset-s3">
		                    <input type="text" id="oneDate" class="datepicker " name="startDate" placeholder="Choose Date">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s2 m2 l2">
	                     <button type="submit" class="btn">View</button>
                        <input type="hidden" value=pickDate name="range">
	                    </div>
	             
                       
                    </form>
               </div>
               <div class="input-field col s12 l4 m5 offset-l3 offset-m1 center actionDiv">
                <form action="${pageContext.request.contextPath}/permanentReturnOrderReport" method="post">
                     <span class="showDates">
							<div class="input-field col s5 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <input type="hidden" value="range" name="range">
                                <label for="startDate">From</label>
                                 </div>
                              

                              <div class="input-field col s5 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               
                             <div class="input-field col s2 m2 l2">
                            <button type="submit" class="btn">View</button>
                            </div>
                            
                          </span>
                </form>
            </div>
       <!--  </div>


        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No</th>
                            <th class="print-col">Return Order Id</th>
                            <th class="print-col">Shop Name</th>
                            <th class="print-col hideOnSmall">Area</th>
                            <th class="print-col">Qty</th>
							<th class="print-col">Date</th>
							<th class="print-col">Credit Note</th>
                        </tr>
                    </thead>
                    <tbody>
                  <%-- <% int rowincrement=0; %> --%>
                    <c:if test="${not empty returnItemReportMainList}">
					<c:forEach var="listValue" items="${returnItemReportMainList}">
					<%-- <c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/> --%>
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Return Order Details" href="${pageContext.request.contextPath}/permanentReturnOrderDetailsByReturnOrderId?returnOrderProductId=${listValue.returnOrderProductId}"><c:out value="${listValue.returnOrderProductId}" /></a></td>
                            <td class="wrapok"><c:out value="${listValue.shopName}" /></td>                            
                            <td><c:out value="${listValue.areaName}" /></td>
                            <td><c:out value="${listValue.totalQty}" /></td>
							<td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.dateOfReturn}"  /><c:out value="${date}" /></td>
							<td><a href="${pageContext.request.contextPath}/ReturnCreditNote.pdf?returnOrderProductId=${listValue.returnOrderProductId}" class="btn-flat tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Get Credit Note"><i class="material-icons">receipt</i></a></td>
                            
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>
    </main>
    <!--content end-->
</body>

</html>