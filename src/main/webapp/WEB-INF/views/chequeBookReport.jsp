<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="components/header_imports.jsp"%>
<script type="text/javascript">

		function tableInitialize(){
			var table = $('#tblData').DataTable();
			table.destroy();
	    	 $('#tblData').DataTable({
	             "oLanguage": {
	                 "sLengthMenu": "Show:  _MENU_",
	                 "sSearch": " _INPUT_" //search
	             },
	             columnDefs: [
	                          { 'width': '1%', 'targets': 0 },
	                          { 'width': '5%', 'targets': 1 },
	                          { 'width': '5%', 'targets': 2 },
	                          { 'width': '5%', 'targets': 3 },
	                          { 'width': '5%', 'targets': 4 },
	                          { 'width': '5%', 'targets': 5 }
	                          ],
	             lengthMenu: [
	                 [10, 25., 50, -1],
	                 ['10 ', '25 ', '50 ', 'All']
	             ],
	             autoWidth: false,
	             
	             //dom: 'lBfrtip',
	             dom:'<lBfr<"scrollDivTable"t>ip>',
	             buttons: {
	                 buttons: [
	                     //      {
	                     //      extend: 'pageLength',
	                     //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                     //  }, 
	                     {
	                         extend: 'pdf',
	                         className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                         text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                         //title of the page
	                         title: function() {
	                             var name = $(".heading").text();
	                             return name
	                         },
	                         //file name 
	                         filename: function() {
	                             var d = new Date();
	                             var date = d.getDate();
	                             var month = d.getMonth();
	                             var year = d.getFullYear();
	                             var name = $(".heading").text();
	                             return name + date + '-' + month + '-' + year;
	                         },
	                         //  exports only dataColumn
	                         exportOptions: {
	                             columns: ':visible.print-col'
	                         },
	                         customize: function(doc, config) {
	                        	  doc.content.forEach(function(item) {
	                          		  if (item.table) {
	                          		  item.table.widths = [20,65,43,50,35,50,40,30,40,40,40,45] 
	                          		 } 
	                          		    })
	                             /* var tableNode;
	                             for (i = 0; i < doc.content.length; ++i) {
	                               if(doc.content[i].table !== undefined){
	                                 tableNode = doc.content[i];
	                                 break;
	                               }
	                             }
	            
	                             var rowIndex = 0;
	                             var tableColumnCount = tableNode.table.body[rowIndex].length;
	                              
	                             if(tableColumnCount > 6){
	                               doc.pageOrientation = 'landscape';
	                             } */
	                             /*for customize the pdf content*/ 
	                             doc.pageMargins = [5,20,10,5];
	                             
	                             doc.defaultStyle.fontSize = 8;
	                             doc.styles.title.fontSize = 12;
	                             doc.styles.tableHeader.fontSize = 11;
	                             doc.styles.tableFooter.fontSize = 11;
	                             doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	                           },
	                          
	                   
	                     },
	                     {
	                         extend: 'excel',
	                         className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                         text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                         //title of the page
	                         title: function() {
	                             var name = $(".heading").text();
	                             return name
	                         },
	                         //file name 
	                         filename: function() {
	                             var d = new Date();
	                             var date = d.getDate();
	                             var month = d.getMonth();
	                             var year = d.getFullYear();
	                             var name = $(".heading").text();
	                             return name + date + '-' + month + '-' + year;
	                         },
	                         //  exports only dataColumn
	                         exportOptions: {
	                             columns: ':visible.print-col'
	                         },
	                     },
	                     {
	                         extend: 'print',
	                         className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                         text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                         //title of the page
	                         title: function() {
	                             var name = $(".heading").text();
	                             return name
	                         },
	                         //file name 
	                         filename: function() {
	                             var d = new Date();
	                             var date = d.getDate();
	                             var month = d.getMonth();
	                             var year = d.getFullYear();
	                             var name = $(".heading").text();
	                             return name + date + '-' + month + '-' + year;
	                         },
	                         //  exports only dataColumn
	                         exportOptions: {
	                             columns: ':visible.print-col'
	                         },
	                     },
	                     {
	                         extend: 'colvis',
	                         className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                         text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                         collectionLayout: 'fixed two-column',
	                         align: 'left'
	                     },
	                 ]
	             }

	         });
		}

		
		function showChequeBookDetails(id){
			 location.href = 'chequeBookDetailsEntryListReport';
		}

	$(document).ready(function() {
		tableInitialize();
		$('#bankAccountId').change(function() {

			
			//$('#chequeReportTableData').empty();
			var bankAccountId = $('#bankAccountId').val();
			if (bankAccountId === "0") {
				//changeTb="area";
				//table.draw();
				return false;
			}
			$.ajax({
				url : "${pageContext.request.contextPath}/chequeBookReport?bankAccountId="+ bankAccountId,
				dataType : "json",
				success : function(data) {
					var srno = 1;
					var chequePrintReportData=data;
					var table = $('#tblData').DataTable();
					table.destroy();
					$('#chequeReportTableData').empty();
					for (var i = 0; i < data.length; i++) {

						 
						showChqBookDetails="<td><a href='${pageContext.servletContext.contextPath}/chequeBookDetailsEntryListReport?chqBookId="+chequePrintReportData[i].chequeBookId+"' class='btn btn-flat blue-text'>"+chequePrintReportData[i].chqBookSeriesNo+"</a>"+
						"</td>";
						/* showChqBookDetails="<td><a href='#' class='btn btn-flat blue-text' onclick='showChequeBookDetails("+chequePrintReportData[i].chequeBookId+")'>"+chequePrintReportData[i].chqBookSeriesNo+"</a>"+
						"</td>"; */
						$('#chequeReportTableData').append(
										'<tr>'
												+ '<td>'+ srno+ '</td>'
												/* + '<td>'+ chequePrintReportData[i].chqBookSeriesNo+ */
												+showChqBookDetails+
												+'</td>'
												+ '<td>'+chequePrintReportData[i].totalChequeCount+'</td>'
												+ '<td>'+chequePrintReportData[i].unusedChequeCount+'</td>'
												+ '<td>'+chequePrintReportData[i].printedChequeCount+'</td>'
												+ '<td>'+chequePrintReportData[i].cancelChequeCount+'</td>'
												+ '</tr>');

						srno++; 
					}
					tableInitialize();
				},
				error: function(xhr, status, error) {
					console.log("List Not found");
				}

			});
			
		});
		
		

	});

</script>

<style type="text/css">
/*  our css code is here*/
</style>
</head>

<body>

 <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    
    <main class="paddingBody">  <br class="hide-on-small-only">
    <div class="row">
		<div class="col s6 l3 m3 left left-align actionDiv filerMargin">
			<select id="bankAccountId" name="bankAccountId" class="btnSelect">
				<option value="0">Select Account Number</option>
				<c:if test="${not empty bankAccountList}">
					<c:forEach var="listValue" items="${bankAccountList}">
						<option value="<c:out value="${listValue.id}" />"><c:out
								value="${listValue.bankShortName} - ${listValue.accountNumber}" /></option>
					</c:forEach>
				</c:if>
			</select>
		</div>
		<div class="col s6 l3 m3 center">
			<a class="btn waves-effect waves-light blue-gradient"
				href="${pageContext.servletContext.contextPath}/openPrintedChequeReport">Printed Cheque Report</a>
		</div>
		<div class="col s6 l3 m3 right right-align">
			<a class="btn waves-effect waves-light blue-gradient"
				href="${pageContext.servletContext.contextPath}/openCancelChequeReport">Cancel Cheque Report</a>
		</div>

		<br> </br>

		<!-- <div class="col s6 l3 m3  left-align">
						        <button id="previewBt">Preview</button>
						        </div> -->


		<div class="col s12 l12 m12 ">
			<table class="striped highlight bordered centered " id="tblData"
				cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr. No</th>
						<th class="print-col">Cheque Book Series No.</th>
						<th class="print-col">Total Leaves</th>
						<th class="print-col">Unused Leaves</th>
						<th class="print-col">Printed Leaves</th>
						<th class="print-col">Cancel Leaves</th>
					</tr>
				</thead>

				<tbody id="chequeReportTableData">
					
				</tbody>
			</table>
		</div>
		
	</div>
    
    </main>

</body>
</html>