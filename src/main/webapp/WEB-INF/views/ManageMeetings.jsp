<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
	<%@include file="components/header_imports.jsp" %>



	<script type="text/javascript">
		var pickDateMainOld = "${pickDate}", departmentIdOld = "${departmentId}";
		$(document).ready(function () {

			var msg = "${saveMsg}";
			//alert(msg);
			if (msg != '' && msg != undefined) {
				$('#addeditmsg').find("#modalType").addClass("success");
				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
				$('#addeditmsg').modal('open');
				//$('#msgHead').text("Brand Message");
				$('#msg').text(msg);
			}
		});

	</script>
	<script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resources/js/ManageMeetings.js"></script>
	<style>
	

		table.dataTable tbody td {
			padding: 0 2px !important;
		}

		.dataTables_wrapper {

			margin-left: 2px !important;
		}

		.commentSection {
			height: 200px;
			overflow-y: auto;
			border: 1px solid #e0e0e0;
		}

		.commentSection p {
			padding: 10px;
		}

		/* #sendMsg{
	width:35%;
	border-radius: 20px;
} */
		table.dataTable thead th,
		table.dataTable thead td {
			padding: 10px 10px;
			border-bottom: 1px solid #111;
		}

		.input-field {
			position: relative;
			margin-top: 0rem;
		}

		#cancelReasonModal {
			width: 40% !important;
		}
			@media only screen and (max-width:600px) {
			#oneDateDiv {
				padding: 0 !important;
				margin-bottom: 20px !important;
				margin-top: 20px !important;
			}
			.firstButton{
				/* margin-bottom: 20px !important; */
			}
		}
	</style>

</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">

		<!--Add New Suppliers-->
		<div class="row">
			<br />
			<div class="col s6 l3 m4 left firstButton" >
				<a class="btn waves-effect waves-light blue-gradient"
					href="${pageContext.servletContext.contextPath}/set_meeting"><i
						class="material-icons left hide-on-small-only">add</i>Meeting Schedule</a>
			</div>
			
			<form action="${pageContext.request.contextPath}/fetch_scheduled_meeting" id="filterForm"  method="post">
				<div class="col s6 l2 offset-l4 m4">
					<select id="departmentId" class="btnSelect" name="departmentId">
						<option value="0" selected>Choose Department</option>
						<c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
								<option value="<c:out value="${listValue.departmentId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
					</select>
				</div>
				<div class="col s6 l3 m4" id="oneDateDiv">
					<div class="input-field col s7 m6 l6">
						<input type="text" id="pickDate" class="datepicker" placeholder="Choose date" name="pickDate">
						<label for="pickDate" class="black-text">Pick Date</label>
					</div>
					<div class="input-field col s5 m2 l2">
						<button class="btn waves-effect waves-light blue-gradient" type="submit">View</button>
					</div>
				</div>
				
			</form>
			<div class="col s12 l12 m12 left">
				<table class="striped highlight centered  display " id="tblData" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="print-col">Sr. No.</th>
							<th class="print-col">Employee</th>
							<th class="print-col">Department</th>
							<th class="print-col">Schedule</th>
						</tr>
					</thead>
					<tbody id="employeeTblData">
						<c:if test="${not empty meetingList }">
							<c:forEach var="meeting" items="${meetingList}">

								<tr>
									<td>
										<c:out value="${meeting.srno}" />
									</td>
									<td>
										<c:out value="${meeting.employeeName}" />
									</td>
									<td>
										<c:out value="${meeting.departmentName}" />
									</td>
									<td>
										<b>
											<c:choose>
												<c:when test="${meeting.meetingScheduled==true}">
													<button class=" btn-flat"
														onclick="viewMeetings('${meeting.employeeDetailsId}','${pickDate}')"
														type="button">Yes</button>
													<!-- <a href="#viewDetails" class="modal-trigger">viewModal</a> -->
												</c:when>
												<c:otherwise>
													<button class="btn-flat" type="button">No</button>
												</c:otherwise>
											</c:choose>
										</b>
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>


			</div>
		</div>
		<!-- Modal Structure for View Product Details -->

		<div id="viewDetails" class="modal">
			<div class="modal-content">
				<h5 class="center"><u>Meeting Schedule Details</u> <i class="material-icons right modal-close">clear</i>
				</h5>
				<table border="2" class="tblborder centered">
					<thead>
						<tr>
							<th>Sr.No</th>
							<th>Time</th>
							<th>Shop Name</th>
							<th>Owner Name</th>
							<th>Mobile Number</th>
							<th>Address</th>
							<th>Venue</th>
							<th>Status</th>
							<th width="60px">Action</th>
						</tr>
					</thead>
					<tbody id="meetingList">
						<!--<tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>
								<button class="btn-flat" type="button" onclick="editrow('+i+')"><i class="material-icons">edit</i></button>&nbsp;&nbsp;
					    		<button class="btn-flat" type="button" onclick="deleteRowConfirmation('+i+')"><i class="material-icons">clear</i></button>
							</td>
                        </tr>-->
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">
					<a href="#!" class="modal-close waves-effect btn red">Close</a>
				</div>

			</div>
		</div>

		<!-- Cancel Reason Modal Start-->
		<div id="cancelReasonModal" class="modal row">
			<div class="modal-content">

				<h5 class="center"><u>Cancel Meeting Reason</u><i class="modal-close material-icons right">clear</i>
				</h5>
				<br>
				<div class="input-field col s12 m12 l12">
					<input type="hidden" value="" id="meetingIdCancelReaons">
					<i class="material-icons prefix">edit</i>
					<textarea id="cancelReason" class="materialize-textarea" name="address" required></textarea>
					<label for="cancelReason"><span class="red-text">*</span>Cancel Reason</label>
				</div>
				<br>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">
					<br>
					<button class="btn modal-action modal-close waves-effect red">Close</button>
					<button class="btn waves-effect waves-light" id="cancelReasonButtonId" type="button">Cancel
						Meeting</button>
				</div>

			</div>
		</div>
		<!-- Cancel Reason Modal End -->

		<!-- Cancel Reason Modal Show Start-->
		<div id="" class="modal">
			<div class="modal-content">
				<h5 class="center"><u>Cancel Meeting Reason</u><i class="modal-close material-icons right">clear</i>
				</h5>
				<div class="col s12 m12 l12">
					<div class="commentSection">
						<p id=""></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">
					<a class="btn  modal-close waves-effect">Close</a>
				</div>
			</div>
		</div>

		<div id="cancelReasonShowModal" class="modal row">
			<div class="modal-content">
				<h5 class="center"><u>Cancel Meeting Reason</u><i class="modal-close material-icons right">clear</i>
				</h5>
				<div class="col s12 m12 l12">
					<div class="commentSection">
						<p id="cancelReasonShowId"></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center center-align">
					<a class="btn modal-close waves-effect">Ok</a>
				</div>
			</div>
		</div>
		<div id="venueReasonShowModal" class="modal row">
			<div class="modal-content">
				<h5 class="center"><u>Meeting Venue</u> <i class="modal-close material-icons right">clear</i></h5>
				<div class="col s12 m12 l12">
					<div class="commentSection">
						<p id="venueReasonShowId"></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12  center center-align">
					<a class="btn  modal-close waves-effect">Ok</a>
				</div>
			</div>
		</div>
		<!-- Cancel Reason Modal Show End -->
</body>

</html>