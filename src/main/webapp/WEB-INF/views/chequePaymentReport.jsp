<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                   	  { 'width': '3%', 'targets': 1 },
	   	                	  { 'width': '5%', 'targets': 2 },
	   	             		  { 'width': '1%', 'targets': 3 },
	   	                      { 'width': '3%', 'targets': 4 },
	   	                   	  { 'width': '3%', 'targets': 5 },
	   	                	  { 'width': '1%', 'targets': 6 },
	   	                	  { 'width': '1%', 'targets': 7 },
	   	                	 { 'width': '1%', 'targets': 8 }
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,60,90,50,70,70,80,] 
		                    		 } 
		                    		    })
	   	        /*                  var tableNode;
	   	                         for (i = 0; i < doc.content.length; ++i) {
	   	                           if(doc.content[i].table !== undefined){
	   	                             tableNode = doc.content[i];
	   	                             break;
	   	                           }
	   	                         }
	   	        
	   	                         var rowIndex = 0;
	   	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	   	                          
	   	                         if(tableColumnCount > 6){
	   	                           doc.pageOrientation = 'landscape';
	   	                         } */
	   	                         /*for customize the pdf content*/ 
	   	                         doc.pageMargins = [10,20,10,10];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                      doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select").change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
			 if ($.data.isMobile) {
				 
					var table = $('#tblData').DataTable(); // note the capital D to get the API instance
					var column = table.columns('.hideOnSmall');
					column.visible(false);
			}
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            
            
          //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            console.log(column);
            if(${sessionScope.loginType=='CompanyAdmin'}){            
                column.visible(true);
            }else{
            	 column.visible(false);
            }
            //send message validation and send msg on mobile number
        	$('#sendMessageId').click(function(){
        		var mobNo=$('#mobileNoSms').val();
        		if(mobNo.length!=10 || mobNo==''){
					Materialize.Toast.removeAll();
        			Materialize.toast('Enter Valid Mobile Number', '3000', 'red lighten-2');
        			return false;
        		}
        		
        		var sms=$('#smsText').val();
        		if(sms==undefined || sms==''){
					Materialize.Toast.removeAll();
        			Materialize.toast('Enter Message', '3000', 'red lighten-2');
        			return false;
        		}
        		
        		sendSms(mobNo,sms);
        	});
        });
	    //set mobile on modal and open sms modal
	    function sendSmsModelOpen(mobNo){
	    	$('#mobileNoSms').val(mobNo);
	    	$('#mobileNoSms').change();
	    	
	    	$('#smsText').val('');
	    	$('#smsText').change();
	    	
	    	$('#mobileEditSms').prop('checked', false);
	    	$("#mobileNoSms").attr('readonly','readonly');
	        
	    	 
	    	$('#sendMsg').modal('open');	    	
	    }
	    //send sms on given mobile number
	    function sendSms(mobNo,sms){
	    	
	    	$.ajax({
				type:'POST',
	    		url : myContextPath+"/sendSmsChequeReport",
				data:{
					mobNo:mobNo,
					sms:sms
				},
				success : function(data) {
					
					if(data=="Success"){
						$('#sendMsg').modal('close');
						Materialize.Toast.removeAll();
						Materialize.toast('Sms Sent SuccessFully', '3000', 'teal lighten-2');
					}else{
						Materialize.Toast.removeAll();
						Materialize.toast('Sms Sending Failed', '3000', 'red lighten-2');
					}
				},
				error: function(xhr, status, error) {
					Materialize.Toast.removeAll();
					Materialize.toast('Something went wrong', '3000', 'red lighten-2');
				}
			});		
	    	
	    }
	    
	    

    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
	 margin-bottom: 0 !important;
    /*  text-align:center !important; */
     }
    .card-image{
    	width:65% !important;
        	max-width:65% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
@media only screen and (min-width: 601px) and (max-width: 992px) {
	.input-field {
    position: relative;
    margin-top: 0.3rem;
}
}
@media only screen and (max-width: 600px) {
	.input-field {
    position: relative;
    margin-top: 0.6rem;
}
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br>
        <div class="row">
                 
         <div class="col s12 l3 m3 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total No of Chq</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><span id="totalNoOfCheque"><c:out value="${totalNoOfCheques}" /></span></h6>
       			  </div>
        	    </div>
          	  </div>
           </div>  
           <div class="col s12 l3 m3 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">No. Of Cleared Chq</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="" id="paidBills"><c:out value="${totalNoCleared}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 l3 m3 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">No. Of Bounce Chq</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="" id="paidBills"><c:out value="${totalNoBounced}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
		   <div class="col s7 m4  hide-on-large-only">
				<h6 class=""><b>Total No of Chq : <span class="blueColor-text" id="totalNoOfCheque"><c:out value="${totalNoOfCheques}" /></span></b></h6>
				<h6 class=""><b>No. Of Cleared Chq : <span class="blueColor-text" id="paidBills"><c:out value="${totalNoCleared}" /></span></b></h6>
				<h6 class="" ><b>No. Of Bounce Chq : <span class="blueColor-text" id="paidBills"><c:out value="${totalNoBounced}" /></span></b></h6>
		   </div>	
           <div class="col s5 m3 l3 right right-align actionDiv actionBtn">
           
           
           
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                     
                     <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s12 l3 m3 right actionDiv" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/chequeReport" method="post">
	                    <div class="input-field col s4 m7 l7 offset-s3">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s4 m3 l2">
	                     <button type="submit" class="btn">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value="pickDate" name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l4 m5 right actionDiv rangeDiv">      
           		 <form action="${pageContext.servletContext.contextPath}/chequeReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s5 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s5 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s2 m2 l2">
                            <button type="submit" class="btn">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
          <%-- <div class="col s12 m5 l5 right">
       			 <h5 class="center red-text">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></h5>
		</div> --%>
<!-- </div>

        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Bank/Cheque No</th>                            
                            <th class="print-col">Amount</th>
                            <th class="print-col">Cheque Date</th>
                            <th class="print-col">Business Name</th>
                            <th class="print-col">Mobile No</th>                            
                            <th class="print-col">Status</th>
                            <th class="toggle hideOnSmall">SMS</th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:if test="${not empty chequePaymentReportModelList}">
						<c:forEach var="listValue" items="${chequePaymentReportModelList}">
						
		                    <tr ${listValue.disableStatus == false ? 'class="red lighten-4"' : ''}>
	                            <td><c:out value="${listValue.srno}" /></td>
	                            <td><c:out value="${listValue.orderId}" /></td>	                                                      
	                            <td><c:out value="${listValue.bankName}" />-<c:out value="${listValue.chequeNumber}" /></td>
	                            <td><c:out value="${listValue.amount}" /></td>
	                            <td>
	                            	<fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.chequeDate}"  />
	                            	<c:out value="${date}" />	                            	
								</td>	                            
	                             <td><c:out value="${listValue.customerBusinessName}" /></td> 
	                            
	                            <td><c:out value="${listValue.mobileNumber}" /></td>
	                            <td>
	                            	<%-- <select class="browser-default" id="status">
		                            		<option value="cleared" ${listValue.bouncedStatus==false?'selected':''}>Cleared</option>
		                            		<option value="bounced" ${listValue.bouncedStatus==true?'selected':''}>Bounced</option>
		                            	</select> --%>
									<c:choose>
									<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
										<c:choose>
										<c:when test="${listValue.checkClearStatus==true}">
											<a href="#clearBounce${listValue.orderId}_${listValue.paymentId}" class="modal-trigger tooltipped">Cleared</a>	
										</c:when>
										<c:otherwise>
											Bounced
										</c:otherwise>
										</c:choose>	
									</c:when>
									<c:otherwise>
										<c:choose>
										<c:when test="${listValue.checkClearStatus==true}">
											Cleared
										</c:when>
										<c:otherwise>
											Bounced
										</c:otherwise>
										</c:choose>	
									</c:otherwise>
									</c:choose>
									
	                            	
	                            </td>
	                            <td>
	                            <button onclick="sendSmsModelOpen(${listValue.mobileNumber})"   id="sendsmsid" class="tooltipped btn-flat" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Send SMS" ><i class="material-icons">mail</i></button>
						       </td>
		                   </tr>
		                   <c:if test="${listValue.checkClearStatus==true}">
			                   <!-- Modal Structure for Cleared/Bounced -->
			                   <div id="clearBounce${listValue.orderId}_${listValue.paymentId}" class="modal deleteModal row">
			                        <div class="modal-content  col s12 m12 l12">
			                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
			                            <h5 class="center-align">Do you want to define cheque as Bounced ?</h5>
			                            <br/>
			                        </div>
			                        <div class="modal-footer">			            
			         				   <div class="col s6 m6 l3 offset-l3">
			                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
			                			</div>
			            				<div class="col s6 m6 l3">
			             				   <a href="${pageContext.request.contextPath}/defineChequeBounced?orderId=${listValue.orderId}&paymentId=${listValue.paymentId}" class="modal-action modal-close waves-effect  btn">Bounced</a>
			               				 </div>
			                
			            			</div>
			                    </div>
		                    </c:if>
		                    <!-- Modal Structure for sendMsg -->
		                         <div id="sendMsg" class="modal row">
		                    
		                        <div class="modal-content">   
		                         
		                           <div class="fixedDiv">
		                              <div class="center-align" style="padding:0">
		                         		<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
		                         	  </div>
		                         	  </div>
		                         	  
		                         	  <div class="scrollDiv">
		                         	  
		                         	 <div class="row mobileDiv" style="margin-bottom:0">
		                         	  	<div class="col s12 l9 m9 input-field" style="padding-left:0">
		                                    <label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
		                                    <input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" value="">
		                                    	
		                                </div>  
		                                <div class="col s12 l2 m2 right-align" style="margin-top:10%">
		                                    <input type="checkbox" id="mobileEditSms"/>
		                                    <label for="mobileEditSms" class="black-text">Edit</label>		                                    
		                                </div>
		                               </div>
		                                <div class="input-field">
		                                    <label for="smsText" class="black-text">Type Message</label>
		                                    <textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
		                                </div> 
		                                <div class="input-field center">                                
		                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
		            					</div>
		            					<div class="fixedFooter">
			            					 <div class="col s6 m4 l3 right-align" style="margin-left:3%">
			                            		 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
			                                 </div>
				                          	<div class="col s6 m6 l2">	
				                           		<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
				                           	</div>
				                         </div>
				                           </div>  	
				                          <br><br><br>
		                         	  
		                         	                             
		                            </div>
		                            
		                               				
		                        </div>
                    
		                   
	                   </c:forEach>
                   </c:if>
                  </tbody>
                </table>
            </div>
        </div>

		                   
 

    </main>
    <!--content end-->
</body>

</html>