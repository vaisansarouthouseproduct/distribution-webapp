<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
	<%@include file="components/header_imports.jsp" %>

	<script type="text/javascript">
		var myContextPath = "${pageContext.request.contextPath}";
	</script>
	<script type="text/javascript" src="resources/js/moment.js"></script>
	<script type="text/javascript" src="resources/js/damageRecovery.js"></script>
	<script>
		$(document).ready(function () {
			var d = new Date();
			var currentYear = d.getFullYear();
			$(".yearSelect").val(currentYear);
			$(".yearSelect").change();
			var table = $('#tblData').DataTable();
			table.destroy();
			$('#tblData').DataTable({
				"oLanguage": {
					"sLengthMenu": "Show _MENU_",
					"sSearch": "_INPUT_" //search
				},

				autoWidth: false,
				columnDefs: [
					{ 'width': '1%', 'targets': 0 },
					{ 'width': '15%', 'targets': 1 },
					{ 'width': '2%', 'targets': 2 },
					{ 'width': '2%', 'targets': 3 },
					{ 'width': '2%', 'targets': 4 },
					{ 'width': '2%', 'targets': 5 },
					{ 'width': '2%', 'targets': 6 },
					{ 'width': '2%', 'targets': 7 }
				],
				lengthMenu: [
					[10, 25., 50, -1],
					['10 ', '25 ', '50 ', 'All']
				],


				// dom: 'lBfrtip',
				dom: '<lBfr<"scrollDivTable"t>ip>',
				buttons: {
					buttons: [
						//      {
						//      extend: 'pageLength',
						//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
						//  }, 
						{
							extend: 'pdf',
							className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
							customize: function (doc, config) {
								doc.content.forEach(function (item) {
									if (item.table) {
										item.table.widths = [30, 180, 75, 55, 65, 65, 50]
									}
								})
								/*for customize the pdf content*/
								doc.pageMargins = [5, 20, 10, 5];
								doc.defaultStyle.fontSize = 8;
								doc.styles.title.fontSize = 12;
								doc.styles.tableHeader.fontSize = 11;
								doc.styles.tableFooter.fontSize = 11;
								doc.styles.tableHeader.alignment = 'center';
								doc.styles.tableBodyEven.alignment = 'center';
								doc.styles.tableBodyOdd.alignment = 'center';
							},
						},
						{
							extend: 'excel',
							className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'print',
							className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'colvis',
							className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
							collectionLayout: 'fixed two-column',
							align: 'left'
						},
					]
				}

			});
			$("select").change(function () {
				var t = this;
				var content = $(this).siblings('ul').detach();
				setTimeout(function () {
					$(t).parent().append(content);
					$("select").material_select();
				}, 200);
			});
			/* $('select').material_select(); */
			$('.dataTables_filter input').attr("placeholder", "Search");
			if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);
				$("#oneDateDiv").removeClass('right right-align');
			}
			$(".num").keypress(function (event) {
				var key = event.which;

				if (!(key >= 48 && key <= 57))
					event.preventDefault();
			});
			$("#updateDamageForm").hide();
			/* $(".actionBtn").click(function(){
				
				  $("#addDamageForm").hide();
				  $("#updateDamageForm").show();
				  $("#damageDetails").modal('close');
			}); */
			$("#startYearId").change(function () {
				var start = $("#startYearId").val();
				var op = $("#endYearId option");

				for (var i = 0; i < op.length; i++) {
					op[i].disabled = false;
					// lowercase comparison for case-insensitivity
					if (op[i].value.toLowerCase() < start) {
						op[i].disabled = true;
					}
				}
			});
			$("#endYearId").change(function () {
				var start = $("#endYearId").val();
				var op = $("#startYearId option");

				for (var i = 0; i < op.length; i++) {
					op[i].disabled = false;
					// lowercase comparison for case-insensitivity
					if (op[i].value.toLowerCase() > start) {
						op[i].disabled = true;
					}
				}
			});
		});
	</script>
	<style>
		.card {
			height: 2.5rem;
			line-height: 2.5rem;
		}

		.card-image {
			width: 40%;
			background-color: #0073b7 !important;
		}

		.card-image h6 {
			padding: 5px;
		}

		.card-stacked .card-content {
			padding: 5px;
		}

		.input-field {
			position: relative;
			margin-top: 0.5rem;
		}

		tfoot td {
			border: 1px solid #9e9e9e;
			text-align: center !important;
		}

		/* #startYear_table,
#endYear_table{
	display:none !important;
}
 */
		.browser-default {
			height: 3rem !important;
			width: 100% !important;
		}
		@media only screen and (max-width:600px){
			#damageDetails{
				width: 90% !important;
			}
		}
		table .btn{
			overflow: hidden;
		}
	</style>


</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">
		<br>
		<div class="row">
			<div class="col s12 m12 l12" style="padding-bottom:15px;">
				<form method="post" action="${pageContext.request.contextPath}/damageRecoveryList">
					<div class="col s12 m4 l4 offset-l2">
						<fieldset>
							<legend>From:</legend>
							<div class=" col s6 m6 l6">
								<!-- <input type="text" id="startYear" class="datepicker"> -->
								<select id="startMonthId" class="browser-default" name="startMonth" required>
									<option value="">Start Month</option>
									<option value="1">Jan</option>
									<option value="2">Feb</option>
									<option value="3">Mar</option>
									<option value="4">Apr</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">Aug</option>
									<option value="9">Sept</option>
									<option value="10">Oct</option>
									<option value="11">Nov</option>
									<option value="12">Dec</option>
								</select>
							</div>
							<div class=" col s6 m6 l6">
								<!-- <input type="text" id="endYear" class="datepicker"> -->
								<select id="startYearId" class="yearSelect browser-default" name="startYear" required>
									<option value="">Start Year</option>
									<%int year=2017;
                                 for(year=2017; year<2051; year++){%>
									<option value="<%=year%>"><%=year%></option>
									<%} %>
								</select>
							</div>
						</fieldset>
					</div>
					<div class="col s12 m4 l4">
						<fieldset>
							<legend>To:</legend>
							<div class=" col s6 m6 l6">
								<select id="endMonthId" class="browser-default" name="endMonth" required>
									<option value="">End Month</option>
									<option value="1">Jan</option>
									<option value="2">Feb</option>
									<option value="3">Mar</option>
									<option value="4">Apr</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">Aug</option>
									<option value="9">Sept</option>
									<option value="10">Oct</option>
									<option value="11">Nov</option>
									<option value="12">Dec</option>
								</select>
							</div>
							<div class="col s6 m6 l6">
								<select id="endYearId" class="yearSelect browser-default" name="endYear" required>
									<option value="">End Year</option>
									<%for(year=2017; year<2051; year++){%>
									<option value="<%=year%>"><%=year%></option>
									<%} %>
								</select>
							</div>
						</fieldset>
					</div>
					<div class="input-field col s12 m2 l2 center" style="margin-top:3%">
						<button type="submit" class="btn">View</button>
					</div>
				</form>
			</div>
			<div class="col s12 m12 l12">


				<div class="col s12 m12 l12 formBg">

					<form action="${pageContext.request.contextPath}/saveDamageRecoveryDetails" method="post"
						id="addDamageForm">
						<div class="row">
							<br>
							<div class="input-field col s12 m3 l3">
								<input id="damageRecoveryId" type="hidden" name="damageRecoveryId">
								<input id="productName" type="text" name="productName" class="grey lighten-3" readonly>
								<label for="productName" class="active">Product</label>
								<%-- <select id="productId" name="productId" required>
                                 <option value="">Choose Product</option>
                                 <c:if test="${not empty productList}">
									<c:forEach var="listValue" items="${productList}">
										<option value="<c:out value="${listValue.productId}" />"  ><c:out value="${listValue.productName}" /></option>
									</c:forEach>
								</c:if>
                        </select> --%>
							</div>
							<div class="input-field col s12 m2 l2">
								<input id="qtyDamage" type="text" name="quantity" class="grey lighten-3" readonly>
								<label for="qtyDamage" class="active">Quantity Damage</label>
							</div>
							<div class="input-field col s12 m3 l3">
								<select id="supplierId" name="supplierId" required>
									<option value="">Choose Supplier</option>
								</select>
							</div>
							<div class="input-field col s12 m2 l2">
								<input id="qtyGiven" type="text" class="num" name="quantityGiven" required>
								<label for="qtyGiven" class="active">Quantity Given</label>
							</div>
							<div class="input-field col s12 m2 l2">
								<input id="givenDate" type="text" class="datepicker" name="givenDate" required>
								<label for="givenDate" class="active">Given Date</label>
							</div>
							<br>
							<div class="input-field col s12 m12 l12 center" style="margin-top:1%">
								<button class="btn waves-effect waves-light blue-gradient" type="submit"
									id="addDamageQty"><i class="material-icons left">add</i>Add</button>
							</div>
						</div>

					</form>
					<form action="${pageContext.request.contextPath}/updateDamageRecoveryDetails" method="post"
						id="updateDamageForm">
						<br>
						<input type="hidden" name="damageRecoveryDetailsId" id="damageRecoveryDetailsId">
						<input type="hidden" id="givenQuantityId">
						<div class="input-field col s12 m2 l2 offset-l3">
							<input id="qtyReceived" type="text" class="num" name="quantityReceived" required>
							<label for="qtyReceived" class="active">Quantity Received</label>
						</div>
						<div class="input-field col s12 m2 l2">
							<input id="rejectReason" type="text" class="" name="rejectReason" required>
							<label for="rejectReason" class="active">Reject Reason</label>
						</div>
						<div class="input-field col s12 m2 l2">
							<input id="receivingDate" type="text" class="datepicker" name="receivedDate" required>
							<label for="receivingDate" class="active">Receiving Date</label>
						</div>
						<div class="input-field col s12 m2 l2 center" style="margin-top:2%">
							<button class="btn waves-effect waves-light blue-gradient" type="submit"
								id="updateDamageQty"><i class="material-icons left">add</i>Add</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col s12 m12 l12">
				<br>
				<table class="striped highlight centered" id="tblData">
					<thead>
						<tr>
							<th class="print-col hideOnSmall">Sr.No</th>
							<th class="print-col">Product</th>
							<th class="print-col">Qty Damage</th>
							<th class="print-col">Qty Given</th>
							<th class="print-col">Qty Recieve</th>
							<th class="print-col">Qty Rejected</th>
							<th class="print-col">Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<% int rowincrement=0; %>
						<c:if test="${not empty damageRecoveryDayWiseList}">
							<c:forEach var="listValue" items="${damageRecoveryDayWiseList}">
								<c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
								<tr>
									<td>
										<c:out value="${rowincrement}" />
									</td>
									<td>
										<c:out value="${listValue.product.productName}" />
									</td>
									<td>
										<button onclick="showDamageDetails(${listValue.damageRecoveryId})"
											class="btn-floating tooltipped modal-trigger" data-position="right"
											data-delay="50" data-tooltip="View Details">
											<c:out value="${listValue.quantityDamage}" />
										</button>
									</td>
									<td>
										<c:out value="${listValue.quantityGiven}" />
									</td>
									<td>
										<c:out value="${listValue.quantityReceived}" />
									</td>
									<td>
										<c:out value="${listValue.quantityNotClaimed}" />
									</td>
									<td>
										<fmt:formatDate pattern="MMM-yyyy" var="date" value="${listValue.datetime}" />
										<c:out value="${date}" />
									</td>
									<td>

										<c:choose>
											<c:when test="${listValue.quantityDamage!=listValue.quantityGiven}">
												<button onclick="giveProductRecovery(${listValue.damageRecoveryId})"
													class="btn tooltipped" data-position="left" data-delay="50"
													data-tooltip="Give Recovery">Give Recovery</button>
											</c:when>
											<c:otherwise>
												NA
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>

				</table>
				<br><br>
			</div>
		</div>
		<div id="damageDetails" class="modal row">
			<div class="modal-content">
				<h5 class="center"><u>Damage Details</u> <i class="material-icons right modal-close">clear</i></h5>
				<div class="scrollDivTable">
					<table class="tblborder centered">
						<thead>
							<tr>
								<th>Sr.No</th>
								<th>Supplier Name</th>
								<th>Qty Given</th>
								<th>Qty Recieve</th>
								<th>Given Date</th>
								<th>Recieved Date</th>
								<th>Rejected Qty</th>
								<th>Reject Reason</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="damageProductDetailsId">
							<!-- <tr>
                    							<td>1</td>
                    							<td>Sachin</td>
                    							<td>50</td>
                    							<td>30</td>
                    							<td>02/2/2018</td>
                    							<td>10/2/2018</td>
                    							<td>20</td>
                    							<td><button class="btn-flat tooltipped actionBtn" data-position="right" data-delay="50" data-tooltip="Add Received Qty"><i class="material-icons">add</i></button></td>                   							
                    						</tr> -->
						</tbody>
						<tfoot id="damageProductDetailsFooter">
							<!-- <tr>
                    							<th colspan="2">Total</th>
                    							<th>70</th>
                    							<th>48</th>
                    							<th></th>
                    							<th></th>
                    							<th>22</th>
                    							<th></th>
                    						</tr> -->
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 center center-align">
					<a class="btn modal-action modal-close waves-effect">Ok</a>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
						<div class="center white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!-- <h5 id="msgHead"></h5> -->

						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
							<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>

					</div>
				</div>
			</div>
		</div>

	</main>
	<!--content end-->
</body>

</html>