<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
      <script>
      $(document).ready(function(){
    	  var table = $('#tblData').DataTable();
 		 table.destroy();
 		 $('#tblData').DataTable({
 	         "oLanguage": {
 	             "sLengthMenu": "Show _MENU_",
 	             "sSearch": "_INPUT_" //search
 	         },
 	     /*  initComplete: function () {
 	    	.on('change', function () {
          var val = $.fn.dataTable.util.escapeRegex(
          $(this).val());
 	      } */
 	      	autoWidth: false,
 	         columnDefs: [
 	                      { 'width': '1%', 'targets': 0 },
 	                      { 'width': '2%', 'targets': 1},
 	                      { 'width': '15%', 'targets': 2},
 	                  	  { 'width': '2%', 'targets': 3},
 	                  	  { 'width': '10%', 'targets': 4},
 	                	  { 'width': '3%', 'targets': 5},
 	                	 { 'width': '3%', 'targets': 6},
 	                	{ 'width': '1%', 'targets': 7}
 	                      ],
 	         lengthMenu: [
 	             [10, 25., 50, -1],
 	             ['10 ', '25 ', '50 ', 'All']
 	         ],
 	         
 	         
 	         //dom: 'lBfrtip',
 	         dom:'<lBfr<"scrollDivTable"t>ip>',
 	         buttons: {
 	             buttons: [
 	                 //      {
 	                 //      extend: 'pageLength',
 	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
 	                 //  }, 
 	                 {
 	                     extend: 'pdf',
 	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                     customize: function(doc, config) {
 	                    	doc.content.forEach(function(item) {
 	                    		  if (item.table) {
 	                    		  item.table.widths = [35,70,130,90,70,70,65] 
 	                    		 } 
 	                    		    })
 	                     
 	                    	 /*   var tableNode;
 	                         for (i = 0; i < doc.content.length; ++i) {
 	                           if(doc.content[i].table !== undefined){
 	                             tableNode = doc.content[i];
 	                             break;
 	                           }
 	                         }
 	        
 	                         var rowIndex = 0;
 	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
 	                          
 	                        if(tableColumnCount > 6){
 	                           doc.pageOrientation = 'landscape';
 	                         } */
 	                         /*for customize the pdf content*/ 
 	                         doc.pageMargins = [5,20,10,5];   	                         
 	                         doc.defaultStyle.fontSize = 8	;
 	                         doc.styles.title.fontSize = 12;
 	                         doc.styles.tableHeader.fontSize = 11;
 	                         doc.styles.tableFooter.fontSize = 11;
 	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
 	                       },
 	                 },
 	                 {
 	                     extend: 'excel',
 	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'print',
 	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'colvis',
 	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
 	                     collectionLayout: 'fixed two-column',
 	                     align: 'left'
 	                 },
 	             ]
 	         }

 	     });
 		 $("select").change(function() {
           var t = this;
           var content = $(this).siblings('ul').detach();
           setTimeout(function() {
               $(t).parent().append(content);
               $("select").material_select();
           }, 200);
       });
   $('select').material_select();
   $('.dataTables_filter input').attr("placeholder", "Search");
   //if there is mobile device the element contain this class will be hidden
   if ($.data.isMobile) {
	     		var table = $('#tblData').DataTable(); // note the capital D to get the API instance
	     		var column = table.columns('.hideOnSmall');
	     		column.visible(false);
	     	}
    	  $(".showDates").hide();
          $(".rangeSelect").click(function() {
         	 $("#oneDateDiv").hide();
              $(".showDates").show();
          });
     	$("#oneDateDiv").hide();
     	$(".pickdate").click(function(){
     		 $(".showDates").hide();
     		$("#oneDateDiv").show();
     	}); 
      });
      //show replacement order product details
      function showProductDetails(orderId)
	   {
		   $.ajax({
				url : "${pageContext.request.contextPath}/fetchReIssueOrderProductDetailsForReplacementReport?reIssueOrderId="+orderId,
				dataType : "json",
				success : function(data) {
					
				    $("#tbproductlist").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var reIssueOrderProductDetails = data[i];
						var productName;
						if(reIssueOrderProductDetails.type=='Free'){
							productName="<td>"+reIssueOrderProductDetails.productName+"<font color='green'>-Free</font></td>";
						}else{
							productName="<td>"+reIssueOrderProductDetails.productName+"</td>"
						}
						
						$("#tbproductlist").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                          	productName+
	                           "<td>"+reIssueOrderProductDetails.returnQuantity+"</td>"+
	                           "<td>"+reIssueOrderProductDetails.replaceQuantity+"</td>"+
	                       "</tr>"); 
						srno++;
					}
						    	
					
					$('.modal').modal();
					$('#viewDetails').modal('open');
					
					return false;
				},
				error: function(xhr, status, error) {
					  	 $('#addeditmsg').modal('open');
					     $('#msgHead').text("ReIssue Report Message");
					     $('#msg').text("Not Have Product List");
				}
			});
	   }
      
      </script>
      <style>
      	#viewDetails{
      	width:40%;
      	}
		  .input-field {
    position: relative;
    margin-top: 0.5rem;
}
@media only screen and (min-width: 601px) and (max-width: 992px) {
	.input-field {
    position: relative;
    margin-top: 0.3rem;
}
.dataTables_wrapper {
	position: relative;
	clear: both;
	zoom: 1;
	margin: 3% auto;
}
#viewDetails{
      	width:70%;
      	}
}
@media only screen and (max-width: 600px) {
	.input-field {
    position: relative;
    margin-top: 0.6rem;
} 
#viewDetails{
      	width:90%;
      	}
						}
      </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br class="hide-on-small-only">


        <div class="row">
				<div class="col s6 m3 l3 right right-align actionDiv actionBtn">
                
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                       <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=viewAll">View All</a></li>
                    </ul>
                </div>
				<div class="input-field col s6 l4 m4 offset-l5 offset-m5 center actionDiv" id="oneDateDiv">                             	
                    <form action="${pageContext.request.contextPath}/fetchFilteredReplacementOrderReportForWeb" method="post">
	                    <div class=" input-field col s8 m6 l4">
		                    <input type="text" id="oneDate" class="datepicker " name="startDate" placeholder="Choose date">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s3 l2 m2">
                        	<input type="hidden" value=pickDate name="range">
	                    	<button type="submit" class="btn">View</button>
	                    </div>
                    </form>
               </div>
			   <div class="input-field col s12 l4 m5 offset-m4 offset-l4 actionDiv rangeDiv">   
                <form action="${pageContext.request.contextPath}/fetchFilteredReplacementOrderReportForWeb" method="post">
                     <span class="showDates">
							<div class="input-field col s5 m5 l5">
									<input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
									<input type="hidden" value="range" name="range">
									<label for="startDate">From</label>
									 </div>
                              <div class="input-field col s5 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                              
								 <div class="input-field col s2 m2 l2">
										<button type="submit" class="btn">View</button>
										</div>
                          </span>
                </form>
            </div>
         <!-- </div>

		 <div class="row"> -->
		 
            <div class="col s12 l12 m12 ">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                             <th class="print-col hideOnSmall">Sr.No</th>
                             <th class="print-col">Order Id</th>
                             <th class="print-col">Shop Name</th>
                             <th>Product Details</th>
                             <th  class="print-col hideOnSmall">Delivery Department</th>
                             <th  class="print-col">Date of Return</th>
                             <th  class="print-col">Date of Delivery</th>
                             <th class="print-col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <% int rowincrement=0; %>
                    <c:if test="${not empty replacementReIssueOrderDetailsReportResponse.reIssueOrderDetailsList}">
					<c:forEach var="listValue" items="${replacementReIssueOrderDetailsReportResponse.reIssueOrderDetailsList}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/orderProductDetailsListForWebApp?orderDetailsId=${listValue.orderId}"><c:out value="${listValue.orderId}" /></a></td>
                            <td class="wrapok"><c:out value="${listValue.shopName}" /></td>                            
                            <td><button class="btn tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" onclick="showProductDetails('${listValue.reIssueOrderId}')">View</button></td>
                            <td><c:out value="${listValue.employeeDepartmentName}" /> - <c:out value="${listValue.employeeName}" /></td>
                            <td class="wrapok"><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.returnOrderDateTime}" /><c:out value="${dt}" />                            
                            </td>
                            <td class="wrapok"><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.reIssueDeliveryDate}" /><c:out value="${dt}" /> 
                            </td>
                            <td><c:out value="${listValue.reIssueStatus}" /></td>
                        </tr>
                     </c:forEach>
                     </c:if>
                                          
                    </tbody>
                </table>
            </div>
        </div>
        
         <!-- Modal Structure for View Product Details -->

        <div id="viewDetails" class="row modal">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u> <i class="modal-close material-icons right">clear</i> </h5>
               			
               
                	
                   <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th width="5%">Sr.No</th>
                            <th>Product Name</th>
                            <th width="5%">Return Quantity</th>
                            <th width="5%">ReIssue Quantity</th>
                        </tr>
                    </thead>
                    <tbody id="tbproductlist">
                       <!--  <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
          
            <div class="modal-footer">
			
				<div class="col s12 m12 l12 center">
						 <a href="#!" class="modal-action modal-close waves-effect center btn">Close</a>
				</div>
				
				
			</div>			
               
            </div>
        
        
    </main>
    <!--content end-->
</body>

</html>