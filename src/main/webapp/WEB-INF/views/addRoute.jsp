<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@include file="components/header_imports.jsp"%>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0&v=3.exp&sensor=false&libraries=places"></script>
	<script>
		var myContextPath = "${pageContext.request.contextPath}"
	</script>
	<script type="text/javascript" src="resources/js/routeEmployee.js"></script>
	<!-- <script type="text/javascript">
	var inputCount = 0;
	function initialize() {
		var input1 = document.getElementById('startPoint');
		var input2 = document.getElementById('endPoint');
		var autocomplete = new google.maps.places.Autocomplete(input1);
		var autocomplete1 = new google.maps.places.Autocomplete(input2);

		google.maps.event
				.addListener(
						autocomplete,
						'place_changed',
						function() {
							var place = autocomplete.getPlace();
							if (!place.geometry) {
								document.getElementById('startPointName').value = "";
								document.getElementById('startPointLat').value = "";
								document.getElementById('startPointLng').value = "";
								alert("No details availabel for start point please select proper route");
							}

							else {
								//for start route
								document.getElementById('startPointName').value = place.name;
								document.getElementById('startPointLat').value = place.geometry.location
										.lat();
								document.getElementById('startPointLng').value = place.geometry.location
										.lng();
								//for end route
								/* document.getElementById('endPointName').value = place1.name;
								document.getElementById('endPointLat').value = place1.geometry.location.lat();
								document.getElementById('endPointLng').value = place1.geometry.location.lng(); */
							}
						});
					google.maps.event.addListener(autocomplete1,'place_changed',function() {
							var place1 = autocomplete1.getPlace();
							if (!place1.geometry) {
								alert("No details availabel for end point please select proper route");
							}
							else {
								
								//for end route
								 document.getElementById('endPointName').value = place1.name;
								document.getElementById('endPointLat').value = place1.geometry.location.lat();
								document.getElementById('endPointLng').value = place1.geometry.location.lng(); 
							}
						});

	}

	google.maps.event.addDomListener(window, 'load', initialize);
	
	function initializeNewInput(){
		var input = document.getElementById("middleRoute_"+inputCount);
		var autocomplete = new google.maps.places.Autocomplete(input);
		google.maps.event.addListener(autocomplete,'place_changed',function() {
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				alert("No details availabel for middle Route "+inputCount+" please select proper route");
			}
			else {
				
				//for end route
				 document.getElementById("middlePlaceName_"+inputCount).value = place.name;
				document.getElementById("middlePlaceLat_"+inputCount).value = place.geometry.location.lat();
				document.getElementById("middlePlaceLng_"+inputCount).value = place.geometry.location.lng(); 
			}
		});
	}
	function addInput() {
		var btn=document.getElementById('addMoreRoute');
		$(".appendinput")
				.append(
						'<div class="input-field col s12 m6 l6">'
								+ '<input id="middleRoute_'+inputCount+'" type="text" class="validate" required>'
								+ '<label for="middleRoute_'+inputCount+'" class="active"><span class="red-text">*</span>Middle Route-'
								+ inputCount
								+ '</label>'
								+ '<input id="middlePlaceName_'+inputCount+'" type="hidden" class="validate">'
								+ '<input id="middlePlaceLat_'+inputCount+'" type="hidden" class="validate">'
								+ '<input id="middlePlaceLng_'+inputCount+'" type="hidden" class="validate">'
								+ '</div>');
		initializeNewInput();
		google.maps.event.addDomListener(btn, 'click', initializeNewInput);
		inputCount++;
	}
	$(document).ready(function() {
		$("#addMoreRoute").click(function() {
			addInput();
		});
		$("#clearRoute").click(function(){
			inputCount=0;
			$(".appendinput").empty();
		});
	});
</script> -->
	<script>
		$(document).ready(function () {
			if ($.data.isMobile) {
				$(".mainRow").removeClass('z-depth-3');
			}
		});
	</script>
	<style>
		div.vertical-line {
			width: 5px;
			margin-left: 50%;
			background-color: gray;
			height: 20px;
			/* Override in-line if you want specific height. */
			position: relative;
		}

		div.vertical-line:after {
			content: "";
			width: 0px;
			height: 0px;
			border-left: 8px solid transparent;
			border-right: 8px solid transparent;
			border-top: 8px solid gray;
			position: absolute;
			bottom: -10%;
			left: 50%;
			margin-left: -0.52rem;
		}

		/* ul#routUl li:nth-child(even){
	text-indent: .5em;
	border-left: .25em solid gray;
}
ul li:nth-child(odd){
	text-indent: .5em;
	border-left: .25em solid red;
} */
		/* ul {
    text-indent: .5em;
    border-left: .25em solid gray;
}

ul ul {
    margin-top: -1.25em;
    margin-left: 1em;

}

li {
    position: relative;
    bottom: -1.25em;
}

li:before {
    content: "";
    display: inline-block;
    width: 2em;
    height: 0;
    position: relative;
    left: -.75em;
    border-top: .25em solid gray;
}

ul ul:before, h3:before, li:after {
    content: '';
    display: block;
    width: 1em;
    height: 1em;
    position: absolute;
    background: salmon;
    border: .25em solid white;
    top: 1em;
    left: .4em;
}

h3 {
    position: absolute;
    top: -1em;
    left: 1.75em;
}

h3:before {
    left: -2.25em;
    top: .5em;
} */
		#upload-error {
			display: block;
		}

		#companyNameId-error,
		#address-error {
			margin-left: 7%;
		}
		@media only screen and (max-width:600px){
			#routeModalView{
				width:90%;
			}
		}
	</style>
</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody"> <br>
		<div class="container">
			<div class="row mainRow z-depth-3">
				<br>
				<div id="routeAddUpdateForm">
					<div class="row ">
						<div class="input-field col s12 m4 l3 push-l1 push-m1">
							<button class="btn waves-effect waves-light blue-gradient" type="button" id="routeListId">
								<i class="material-icons left">arrow_back</i>Route List
							</button>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m9 l9 push-l1 push-m1" id="">
							<i class="material-icons prefix">account_balance</i>
							<input id="routeName" type="text" name="routeName">
							<input id="routeId" type="hidden" value="0">
							<label for="routeName"><span class="red-text">*</span>Enter Route Name</label>

						</div>

					</div>
					<div class="col s12 m10 l10 push-l1 push-m1" style="padding:0">
						<div class="row">
							<div class="input-field col s12 m6 l6">
								<i class="material-icons prefix">swap_calls</i>
								<input id="routePoint" type="text" name="startPoint" placeholder="Enter a location">
								<label for="routePoint">Route Point</label>
							</div>
							<div class="input-field col s6 m3 l2">
								<button class="btn waves-effect waves-light blue-gradient" type="submit"
									id="addRoutePoint">
									Add <i class="material-icons right">add</i>
								</button>
							</div>
							<div class="input-field col s6 m3 l3 right-align">
								<button class="btn waves-effect waves-light blue-gradient" type="button"
									id="clearRoute">
									Reset All<i class="material-icons right">refresh</i>
								</button>
							</div>
						</div>
						<div class="input-field col s12 m12 l8 push-l2">
							<table class="centered tblborder" id="routePointCart">
								<thead>
									<tr>
										<th>Sr.No</th>
										<th>Address</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="routePointTable">

								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m4 l3 push-l5 push-m1">
							<button class="btn waves-effect waves-light blue-gradient" type="button" id="saveRoute">
								Save Route
							</button>
						</div>
					</div>
				</div>
				<div class="row" id="routeDataTable">
					<div class="row ">
						<div class="input-field col s12 m4 l3 push-l1 push-m1">
							<button class="btn waves-effect waves-light blue-gradient" type="button" id="addNewRoute">
								<i class="material-icons left">add</i>Add Route
							</button>
						</div>
					</div>
					<div class="input-field col s12 m10 l10 push-l1 push-m1" style="padding:0">
						<table class="centered tblborder" id="routeCart">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>Route Name</th>
									<th>Route Number</th>
									<th>Route</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="routeTableData">
								<!-- <tr>
									<td>1</td>
									<td>Kandivali</td>
									<td>0101</td>
									<td><a class="modal-trigger" href="#routeModalView">View</a></td>
								</tr> -->
							</tbody>
						</table>
						<br>
						<br>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12">
				<div id="routeModalView" class="modal">
					<div class="modal-content">

						<h5 class="center"> <u> Route View </u><i class="material-icons modal-close right">clear</i>
						</h5>
						<div id="routePointList">
							<ul class="center" id="routUl">
								<li>Start Route</li>
								<div class="vertical-line"></div>
								<li>Middle Route1</li>
								<div class="vertical-line"></div>
								<li>Middle Route2</li>
								<div class="vertical-line"></div>
								<li>Middle Route3</li>
								<div class="vertical-line"></div>
								<li>End Route</li>
							</ul>
						</div>
					</div>
					<div class="modal-footer">
						<div class="col s12 m12 l12 center">
							<a href="#!" class="modal-close waves-effect  btn">OK</a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding: 0">
						<div class="center success  white-text" id="modalType" style="padding: 3% 0 3% 0"></div>
						<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
							<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</main>
	<!--content end-->
</body>

</html>