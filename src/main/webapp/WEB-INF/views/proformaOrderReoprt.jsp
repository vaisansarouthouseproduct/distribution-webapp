<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
  <script type="text/javascript">
  var myContextPath="${pageContext.servletContext.contextPath}";
  </script>
  <script type="text/javascript" src="resources/js/counterReport.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script> -->
    <script type="text/javascript">
    
    var downloadhrefList=[];
    var counterOrderIdList=[];
	    
   
	    $(document).ready(function() {
	    	//$('#notificationbarPendingPayment').text('Hii,Candylove I am sachin');
	    	
	    	
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   		    "oLanguage": {	 
	   		 
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '2%', 'targets': 1},
	   	                  	  { 'width': '5%', 'targets': 2},
	   	                	  { 'width': '5%', 'targets': 3},
	   	             		  { 'width': '3%', 'targets': 4}, 
	   	             		  { 'width': '3%', 'targets': 5},
	   	                      { 'width': '1%', 'targets': 6},
	   	                  	  { 'width': '3%', 'targets': 7},
	   	                  	  { 'width': '3%', 'targets': 8},
	   	                	  { 'width': '5%', 'targets': 9,"orderable": false},				
	   	                      ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,40,50,70,40,40,30,40,50,60,50] 
		                    		 } 
		                    		    })
	   	       
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select").change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
			 if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);
			}
	    	/* CHECK ALL CHECKBOX */
	    	 $("#checkAll").click(function () {
	    		 
	    		 var colcount=$('#tblData').DataTable().columns().header().length;
	    	    var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column 
	    	    state = this.checked;
	    			//alert(state);
	    	   for (var i = 0; i < cells.length; i += 1) {
	    	        cells[i].querySelector("input[type='checkbox']").checked = state;
	    	    } 
	    	    $("input[type='checkbox']",allPages).prop('checked',state);
	    	    $("input[type='checkbox']").change();
	    	    
	    	}); 
	    	/* SELECT CHECK BOX ONE BY ONE */
	    	  $("input:checkbox").change(function(a){
	    		 
	    		 var colcount=$('#tblData').DataTable().columns().header().length;
	    	     var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
	    	         
	    	      state = false; 				
	    	     var ch=false;
	    	     
	    	     for (var i = 0; i < cells.length; i += 1) {
	    	    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
	    	    	
	    	         if(cells[i].querySelector("input[type='checkbox']").checked == state)
	    	         { 
	    	        	 $("#checkAll").prop('checked', false);
	    	        	 ch=true;
	    	         }                      
	    	     }
	    	     if(ch==false)
	    	     {
	    	    	 $("#checkAll").prop('checked', true);
	    	     }
	    	    	
	    	    //alert($('#tblData').DataTable().rows().count());
	    	}); 
	    	
	    	
	    	//on checkBox change function
	   	  $(".chbRow").change(function() {
			var id = $(this).attr('data-id');
			console.log(id);
			if ($(this).is(':checked')) {
				
				
					
					var isExist = jQuery.inArray(id,counterOrderIdList);
					if (isExist == -1) {
						counterOrderIdList.push(id);
					}

				
			} else {
				//checked element is present in array if not then it returns -1 else position of ele.
				var isExist = jQuery.inArray(id,counterOrderIdList);
				if (isExist != -1) {
					counterOrderIdList.splice(isExist,1);
				}
				
			}
		});
	   	 
	   	
	   	
	    	$('#showPaymentCounterStatus').hide();
	    	var msg="${saveMsg}";
	       	 if(msg!='' && msg!=undefined)
	       	 {
	       		 $('#addeditmsg').find("#modalType").addClass("success");
	 			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	       	     $('#addeditmsg').modal('open');
	       	     /* $('#msgHead').text("HRM Message"); */
	       	     $('#msg').text(msg);
	       	 }
	       	 
	       	 //allowed only number without decimal
	   		$('.num').keypress(function( event ){
	 		    var key = event.which;
	 		    var id=this.id;
	 		    var value=$('#'+id).val();
	 		    var val=value.split('\.');
	 		    if(val.length>1 && key === 46){
	 		    	event.preventDefault();
	 		    }
	 		    if( ! ( key >= 48 && key <= 57 || key === 13 || key === 46) )
	 		    	event.preventDefault();
	 		});
	    	
	    	           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            
           
            
          //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle1');
            console.log(column);
            if(${sessionScope.loginType=='GateKeeper'})
            {            
                column.visible(true);
            }else{
            	 column.visible(false);
            	 
            }
            
            
        	  var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            column.visible(false);
            $('#showColumn').on('click', function () {
            	var table = $('#tblData').DataTable(); // note the capital D to get the API instance
                var column = table.columns('.toggle');
            	 //console.log(column);
          
            	 column.visible( ! column.visible()[0] );
          
            });
            
        });  
	 // payament details list by order id (counter order id)
	    function fetchPaymentDetails(orderId){
					$.ajax({
						type:'GET',
						url: "${pageContext.servletContext.contextPath}/fetchPaymentCounterOrder?counterOrderId="+orderId,
						async: false,
						success : function(data)
						{
							
							
							$('#paymentDetail').empty();
							
							var totalAmountPaid=0;
							for(var i=0; i<data.length; i++)	
							{
								totalAmountPaid+=data[i].currentAmountPaid.toFixedVSS(2)-data[i].currentAmountRefund.toFixedVSS(2)
							}
							
							if(data[0].counterOrder.businessName=="" || data[0].counterOrder.businessName==undefined){
								$('#boothNo').html("<b>Customer Name : </b>"+data[0].counterOrder.customerName);
							}else{
								$('#boothNo').html("<b>Booth No. : </b>"+data[0].counterOrder.businessName.businessNameId);
							}
							
							$('#orderId').html("<b>Order Id. : </b>"+data[0].counterOrder.counterOrderId);
							$('#totalAmt').html("<b>Total Amount : </b>"+data[0].counterOrder.totalAmountWithTax);
							$('#balanceAmt').html("<b>Balance Amount : </b>"+(Math.round(parseFloat(data[0].counterOrder.totalAmountWithTax))-parseFloat(totalAmountPaid)));
							
							var totalAmountPaid=0;
							var srno=1;
							$('#showPaymentCounterStatus').show();
							for(var i=0; i<data.length; i++)	
							{
								var mode="";
								if(data[i].payType==="Cash")
								{
									mode="Cash";
								}
								else if(data[i].payType==="Cheque")
								{
									mode=data[i].bankName+"-"+data[i].chequeNumber;
								}else if(data[i].payType==="Wallet")
								{
									mode="Wallet";
								}else{
									mode=data[i].paymentMethodName+"-"+data[i].transactionRefNo;
								}
								//long to dd/MM/yyyy format of date
								var dateFrom=new Date(parseInt(data[i].paidDate));
								month = dateFrom.getMonth() + 1;
								day = dateFrom.getDate();
								year = dateFrom.getFullYear();
								var dateFromString = day + "-" + month + "-" + year;
								
								
								if(data[i].currentAmountRefund=='' || data[i].currentAmountRefund==undefined){
									//paid amount
									$('#paymentDetail').append('<tr>'+
																'<td>'+srno+'</td>'+
																'<td>'+data[i].currentAmountPaid.toFixedVSS(2)+'</td>'+
																'<td><font color="blue"><b>Paid</b></font></td>'+
																'<td>'+mode+'</td>'+
																'<td>'+data[i].employeeName+'</td>'+
																'<td>'+dateFromString+'</td></tr>');
									totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].currentAmountPaid.toFixedVSS(2));
								}else{
									//refund amount
									$('#paymentDetail').append('<tr>'+
											'<td>'+srno+'</td>'+
											'<td>'+data[i].currentAmountRefund.toFixedVSS(2)+'</td>'+
											'<td><font color="green"><b>Refund</b></font></td>'+
											'<td>'+mode+'</td>'+
											'<td>'+data[i].employeeName+'</td>'+
											'<td>'+dateFromString+'</td></tr>');
									totalAmountPaid=parseFloat(totalAmountPaid)-parseFloat(data[i].currentAmountRefund.toFixedVSS(2));
								}
								
								srno++;
							}
							$('#totalAmountPaid').text(totalAmountPaid);
							$('#viewPaymentDetail').modal('open');
						},
						error: function(xhr, status, error) 
						{
							Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
							/* $('#addeditmsg').modal('open');
			       	     	$('#msgHead').text("Message : ");
			       	     	$('#msg').text("Product List Not Found"); 
			       	     		setTimeout(function() 
								  {
				     					$('#addeditmsg').modal('close');
								  }, 1000); */
						}
						
					});
		}

    </script>
     <style>
     /* notification css */
     #notificationbarPendingPayment{
      border-radius: 2px;
     background-color: #0073B7;
    padding: 20px;
    position: fixed;
    bottom: 5px !important;
    right: 10px;
    cursor: pointer;
    text-align: center;
    box-shadow : 5px 5px 5px grey;
	z-index : 999;
	color: #FFF;
     }
      
       #notificationbarThresold{
      border-radius: 2px;
     background-color: #0073B7;
    padding: 20px;
    position: fixed;
    bottom: 150px !important;
    right: 10px;
    cursor: pointer;
    text-align: center;
    box-shadow : 5px 5px 5px grey;
	z-index : 999;
	color: #FFF;
     }
      
     
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#openAgentCommission{
	width:50% !important;
	max-height:80% !important;
	}

td .btn-flat{
	color:unset !important;
	font-size:0.8rem !important;
}
@media only screen and (min-width:992px){
	.actionBtnDiv{
	margin-top:0.6%;
	width:15%;
}
#oneDateDiv{
	margin-top: 0.5rem;
	/* margin-left:4% */
}
}
@media only screen and (max-width:600px){
	.input-field{
		margin-top:0;
	}
}


    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    
   
	
    <main  class="paddingBody">
        <br>
       
	
        <div class="row noBottomMarginOnLarge">
        	<!-- <div class="col s12 m3 l3 center" style="margin-top:0.6%;width:30%">
        		<Button class="btn" id="printBulk">Save Invoice<i class="material-icons tooltipped right" data-delay='50' data-tooltip='Save selected Invoice'>arrow_downward</i></Button>
        	</div> -->
           <div class="col s5 m4 l3 right right-align actionBtnDiv">
           
           		
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>                    
                     <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s6 l4 m5 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/proformaOrderReport" method="post">
	                    <div class="input-field col s8 m4 l3">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s4 m3 l2">
	                     <button type="submit" class="btn">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l6 m4 offset-l1 offset-m2 right" style="margin-top:0.5rem;">      
           		 <form action="${pageContext.servletContext.contextPath}/proformaOrderReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s4 m4 l4">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s4 m4 l4">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s4 m4 l3">
                            <button type="submit" class="btn">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
          <%-- <div class="col s12 m5 l5 right">
       			 <h5 class="center red-text">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></h5>
		</div> --%>
 </div>

        <div class="row"> 
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Pro-forma Order Id</th>
                            <th class="print-col">Shop Name</th>
                             <th class="print-col toggle">Taxable Amt</th> 
                            <th class="print-col">Total Amt</th>
                            <th class="print-col">Total Qty</th>
                            <th class="print-col toggle">Order Taken Date/Time</th>
                            <th class="print-col toggle">Comment</th>
                            <th >Get Bill</th>
                            <!-- <th class="toggle">Action</th> -->
                          
                           <th class="toggle1">Action <br/><a class="tooltipped cursorPointer" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Show more columns" id="showColumn"><i class="material-icons black-text">swap_horiz</i></a>
                          <%-- 	<c:if test="${sessionScope.loginType=='GateKeeper'}"> --%>
                           <%-- </c:if> --%>
                        </tr>
                    </thead>

                    <tbody>
                    <c:if test="${not empty proformaOrderReports}">
						<c:forEach var="listValue" items="${proformaOrderReports}">
		                    <tr class="">
		                    	<td><c:out value="${listValue.srno}" /></td>
		                    	<td>
		                    		<a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/fetchProformaOrderProductDetailsByProformaOrderId?proformaOrderId=${listValue.id}"><c:out value="${listValue.proformaOrderId}" /></a>
		                    	</td>
		                    	<td><c:out value="${listValue.shopName}" /></td>
		                    	<td><c:out value="${listValue.totalAmount}" /></td>
		                    	<td><c:out value="${listValue.totalAmountWithTax}" /></td>
		                    	<td><c:out value="${listValue.totalQuantity}" /></td>
		                    	<td>
		                    	<c:out value="${listValue.orderTakenDate}" /><br>  <c:out value="${listValue.orderTakenTime}" />
		                    	</td>
		                    	<td><c:out value="${listValue.orderComment}" /></td>
		                    	<td><a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/proformaOrderInvoice.pdf?proformaOrderId=${listValue.proformaOrderId}"><i class="material-icons">receipt</i></a></td>
		                    	<%-- <c:if test="${sessionScope.loginType=='GateKeeper'}"> --%>
		                    	<td>
		                    
		                    
			                    		<a href="${pageContext.request.contextPath}/openProFormaForEdit?id=${listValue.id}" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Edit order" >edit</i></a> 
			                    		<a href="#delete_${listValue.id}"
												class="modal-trigger btn-flat"><i
												class="material-icons tooltipped" data-position="left"
												data-delay="50" data-tooltip="Delete">delete</i></a>
							
		                    	</td>
		                    	<%-- </c:if> --%>
		                    	
		                    	
		                    </tr>
		                    
		                    <!-- Modal Structure for delete start-->
		                    <div id="delete_${listValue.id}" class="modal deleteModal row">
		                        <div class="modal-content  col s12 m12 l12">
		                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
		                            <h5 class="center-align">Do You Want to Delete?</h5>
		                            <br/>
		                        </div>
		                        <div class="modal-footer">            
		         				   <div class="col s6 m6 l3 offset-l3">
		                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
		                			</div>
		            				<div class="col s6 m6 l3">
		             				   <a href="${pageContext.request.contextPath}/deleteProFormaOrder?id=${listValue.id}" class="modal-action modal-close waves-effect  btn">Delete</a>
		               				 </div>                
		            			</div>
		                    </div>
		                 </c:forEach>
		             </c:if>
                     	

                    
                    </tbody>
                </table>
            </div>
        </div>
		
		
		
    </main>
    <!--content end-->
</body>

</html>