<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">

<head>
<%@include file="components/header_imports.jsp"%>
<script>
	var myContextPath = "${pageContext.request.contextPath}"
</script>
<script type="text/javascript"
	src="resources/js/locationscript_admin.js"></script>
<script>
	/* document.addEventListener("DOMContentLoaded", function(){
	
	 alert();
	 $('.preloader-background').delay(1700).fadeOut('slow');
	
	 $('.preloader-wrapper')
	 .delay(1700)
	 .fadeOut();
	 }); */
</script>
<style>
label {
	color: black;
}

#addArea, #addRegion, #addCity, #addState, #addCountry {
	text-transform: uppercase;
}

.width14 {
	width: 14% !important;
	text-align: center !important;
}

.noPadding {
	padding: 0 !important;
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
	.width14 {
		width: 20% !important;
		text-align: center !important;
	}
	.paddingBody {
		padding: 0 5px !important;
	}
}

@media only screen and (max-width: 600px) {
	.width14 {
		width: 50% !important;
		text-align: center !important;
	}
	.paddingBody {
		padding: 0 5px !important;
	}
}
</style>

</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->

	<!-- <div class="preloader-background">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
         
        </div>
      </div>
     <div class="red-text"><br><h6>Wait.....</h6></div> 
    </div>
     
      </div>  -->
	<!-- <div class="preloader-wrapper big active">
		<div class="spinner-layer spinner-blue-only">
			<div class="circle-clipper left">
				<div class="circle"></div>
			</div>
			<div class="gap-patch">
				<div class="circle"></div>
			</div>
			<div class="circle-clipper right">
				<div class="circle"></div>
			</div>
		</div>
	</div> -->


	<main class="paddingBody"> <br>
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="row">
				<ul class="tabs tabs-fixed-width">

					<li class="tab col s3"><a href="#city" id="citywork">City</a>
					</li>
					<li class="tab col s3"><a href="#state" id="statework">State</a>
					</li>
					<li class="tab col s3" style="padding-right: 0"><a
						href="#country" id="fetchCountry">Country</a></li>
				</ul>
			</div>

			<!--CITY START-->
			<div id="city">
				<form action="${pageContext.servletContext.contextPath}/saveCity"
					method="post" id="saveCityForm">
					<!--Add New City-->
					<div class="row">
						<div class="col s12 m12 l12 formBg">
							<div class="input-field col s6 l2 m4">
								<span class="selectLabel_100"> <span class="red-text">*</span>
									Select Country
								</span> <select id="countryListForCity" name="coutryId">
									<option value="0" selected>Choose Country</option>
								</select>
							</div>
							<div class="input-field col s6 l2 m4">
								<span class="selectLabel_100"> <span class="red-text">*</span>
									Select State
								</span> <select id="stateListForCity" name="stateId">
									<option value="0" selected>Choose State</option>
								</select>

							</div>
							<div class="input-field col s12 l2 m4">
								<input value="0" id="addCityId" type="hidden" class="validate"
									name="cityId"> <input value="" id="addCity" type="text"
									class="validate" name="name"> <label for="addArea">
									<span class="red-text">*</span> Enter City Name
								</label>
							</div>
							<div class="col s6 l2 m4 offset-m4 width14"
								style="margin-top: 2%;">
								<button class="btn waves-effect waves-light blue-gradient"
									type="button" name="action" id="saveCitySubmit">
									<i class="material-icons left">add</i>Add
								</button>
							</div>
							<div class="col s6 l3 m3" style="margin-top: 2%;">
								<button class="btn waves-effect waves-light blue-gradient"
									type="button" name="action" id="resetCitySubmit">
									<i class="material-icons left">refresh</i> Reset
								</button>
							</div>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="col s12 l12 m12 noPadding">
						<table class="striped highlight centered " id="tblDataCity"
							width="100%">
							<thead>
								<tr>
									<th class="print-col hideOnSmall">Sr. No.</th>
									<th class="print-col">City Name</th>
									<th class="print-col">State Name</th>
									<th class="print-col">Country Name</th>
									<th>Edit</th>
									<!-- <th>Delete</th> -->
								</tr>
							</thead>

							<tbody>



							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--CITY END-->
			<!--STATE START-->
			<div id="state">

				<form action="${pageContext.servletContext.contextPath}/saveState"
					method="post" id="saveStateForm">
					<!--Add New State-->
					<div class="row">
						<div class="col s12 m12 l12 formBg">
							<div class="input-field col s12 l2 m4">
								<span class="selectLabel_100"> <span class="red-text">*</span>
									Select Country
								</span> <select id="countryListForState" name="countryId">
									<option value="0" selected>Choose Country</option>
								</select>

							</div>


							<div class="input-field col s6 l2 m4">
								<input value="0" id="addStateId" type="hidden" class="validate"
									name="stateId"> <input value="" id="addState"
									type="text" class="validate" name="name"> <label
									for="addArea"> <span class="red-text">*</span> Enter
									State Name
								</label>
							</div>
							<div class="input-field col s6 l2 m4">
								<input value="" id="addStateCodeId" type="text" class="validate"
									name="code"> <label for="addArea"> <span
									class="red-text">*</span> Enter State Code
								</label>
							</div>
							<div class="col s6 l2 m3 width14 offset-m4"
								style="margin-top: 2%;">
								<button class="btn waves-effect waves-light blue-gradient"
									id="saveStateSubmit" type="button" name="action">
									<i class="material-icons left">add</i>Add
								</button>
							</div>
							<div class="col s6 l2 m3" style="margin-top: 2%;">
								<button class="btn waves-effect waves-light blue-gradient"
									type="button" name="action" id="resetStateSubmit">
									<i class="material-icons left">refresh</i> Reset
								</button>
							</div>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="col s12 l12 m12 noPadding">
						<table class="striped highlight centered " id="tblDataState"
							width="100%">
							<thead>
								<tr>
									<th class="print-col hideOnSmall">Sr. No.</th>
									<th class="print-col">State Name</th>
									<th class="print-col">State Code</th>
									<th class="print-col">Country Name</th>
									<th>Edit</th>
									<!-- <th>Delete</th> -->

								</tr>
							</thead>

							<tbody>


							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--STATE END-->
			<!--COUNTRY START-->
			<div id="country">
				<form action="${pageContext.servletContext.contextPath}/saveCountry"
					method="post" id="saveCountryForm">
					<!--Add New Contry-->
					<div class="row">
						<div class="col s12 m12 l12 formBg">
							<div class="input-field col s12 l3 m4  offset-l3 offset-m2">
								<input value="0" id="addCountryId" type="hidden"
									class="validate" name="countryId"> <input value=""
									id="addCountry" type="text" name="name" required> <label
									for="addArea" class="active"> <span class="red-text">*</span>
									Enter Country Name
								</label>
							</div>
							<div class="col s6 l2 m2 width14"
								style="margin-top: 2%; width: 14%;">
								<button class="btn waves-effect waves-light blue-gradient"
									type="button" id="saveCountrySubmit" name="action">
									<i class="material-icons left">add</i>Add
								</button>
							</div>
							<div class="col s6 l2 m3" style="margin-top: 2%;">
								<button class="btn waves-effect waves-light blue-gradient"
									type="button" name="action" id="resetCountrySubmit">
									<i class="material-icons left">refresh</i> Reset
								</button>
							</div>
						</div>
					</div>
				</form>

				<div class="row">
					<div class="col s12 l12 m12 noPadding" id="dataAll">
						<table class="striped highlight centered " id="tblDataCountry"
							width="100%">
							<thead>
								<tr>
									<th class="print-col">Sr. No.</th>
									<th class="print-col">Country Name</th>
									<th>Edit</th>
									<!-- <th>Delete</th> -->

								</tr>
							</thead>

							<tbody id="countryList">


							</tbody>
						</table>

					</div>
				</div>
			</div>
			<!--COUNTRY END-->

		</div>


	</div>


	</main>
	<!--content end-->
	<div class="row">
		<div class="col s12 m12 l8">
			<div id="addeditmsg" class="modal">
				<div class="modal-content" style="padding: 0">
					<div class="center   white-text" id="modalType"
						style="padding: 3% 0 3% 0"></div>
					<!--  <h5 id="msgHead"></h5> -->

					<h6 id="msg" class="center"></h6>
				</div>
				<div class="modal-footer">
					<div class="col s12 center">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>

</html>