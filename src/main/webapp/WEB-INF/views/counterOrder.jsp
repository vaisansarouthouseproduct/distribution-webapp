<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<%@include file="components/header_imports.jsp"%>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript" src="resources/js/hash-map-collection.js"></script>
<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>

<script>
var syncBtnStatus="false";
  	$(document).ready(function(){
  		//allowed only number without decimal
  		$('.qty').keypress(function( event ){
		    var key = event.which;
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});
  	    //allowed only number without decimal
  		$('.num').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});

			//allowed only numbers with decimal 
	/* $('.numWithDecimal').keydown(function(e){            	
		-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
	 }); */
	
		//allowed only numbers with decimal 
		$('#discountId').keydown(function(e){            	
			-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
		 });
		//allowed only numbers with decimal 
		document.getElementById('discountId').onkeypress=function(e){
	    	
	    	if (e.keyCode === 46 && this.value.split('.').length === 2) {
	      		 return false;
	  		 }
	
	    }
	
		//allowed only numbers with decimal 
		$('#discountMainId').keydown(function(e){            	
			-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
		 });
		//allowed only numbers with decimal 
		document.getElementById('discountMainId').onkeypress=function(e){
	    	
	    	if (e.keyCode === 46 && this.value.split('.').length === 2) {
	      		 return false;
	  		 }
	
	    }
  		
  		$(".chequeForm").hide();
  		$(".otherForm").hide();
	    $(".partialForm").hide(); 

  		$('.creditDueDate').hide();
		$(".chequeForm").hide();
		$(".otherForm").hide();
	   	$(".partialForm").hide(); 
	   	$(".dueDateExtraPayDiv").hide();
	   
	    	 
	    	 $('#cash').click(function() {
	    		 $(".chequeForm").hide();
	    		 $(".otherForm").hide();
	               
	            });
	            $('#cheque').click(function() {
	            	
	                $('.chequeForm').show();
	                $(".otherForm").hide();
	               
	            });
	            
  				$('#other').click(function() {
	            	
	                $('.chequeForm').hide();
	                $(".otherForm").show();
	               
	            });
	            
	            $('#partialPay').click(function() {
	            	 $(".partialForm").show(); 
	            	 $('.paymentTypeSection').show();
	            	 $('.creditDueDate').hide();
	            });
	            $('#instantPay').click(function() {
	            	
	                $('.partialForm').hide();
	                $('.creditDueDate').hide();
	                $('.paymentTypeSection').show();
	            });
 				 $('#credit').click(function() { 
		            	
		                $('.partialForm').hide();
		                $('.creditDueDate').show();
		                $('.paymentTypeSection').hide();
		            });
 				$('.select2').material_select('destroy');
				$('.select2').select2({
					placeholder : "Choose your Product",
					allowClear : true
				});
				$(".select2").select2({
					width : 'resolve'
				});
				/* $(".btnPay").click(function(){
					var businessList=$("#businessList option:selected").val();
					
					if(businessList=="" ||businessList==undefined){
						$("#custInfo").modal('open');
					}
				}); */
				
				 $('#logo-collapse').toggle();
		         $('#logo-collapse-full').toggle();
		         $('.element-sideNav').toggle();
					$("#slide").toggleClass("reveal-open");
					$('.side-nav').toggleClass("reveal-open");
					$('main').toggleClass("paddingBody");
					$('main').toggleClass("intro");
								
					
					
					<c:if test="${isEdit==true}">
					$('#transport_details').change(function(){						
							$('#transport_details_info').toggle();
					});
					</c:if>
					
					<c:if test="${isEdit==false}">
					$('#transport_details_info').hide();
					$('#transport_details').change(function(){
						var tbody = $("#tblProduct tbody");
						if(tbody[0].children.length==3){
							if(this.checked){
								$('#transportationModal').modal('open');								
								$('#transport_details').prop("checked",false);
							}		
							else
								$('#transportationModal').modal('close');
						}else{
							$('#transport_details_info').toggle();
							
						}
					});
					</c:if>
					
					//Sync The data
					$('#syncDataBtn').click(function(){
						
						$('#productId').empty();
						$("#productId").append('<option value="0">Choose Product</option>');
						syncBtnStatus="true";
						$.ajax({
							type:'GET',
							url : myContextPath+"/syncProductListForCounterOrder",
							dataType : "json",
							success : function(data) {
								
								for(var i=0; i<data.length; i++)
								{								
									//alert(data[i].productId +"-"+ data[i].productName);
									$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
								}	
								Materialize.Toast.removeAll();
								Materialize.toast('Data is Successfully sync', '4000', 'teal lighten-2');
								//alert("done");
								$("#productId").change();
								
							},
							error: function(xhr, status, error) {
								Materialize.Toast.removeAll();
								Materialize.toast('Product List  Not Found', '4000', 'teal lighten-2');
							}
						});		
						
						$('#businessListId').empty();
						$("#businessListId").append('<option value="0">Business List</option>');
						
						$.ajax({
							type:'GET',
							url : myContextPath+"/fetchSyncBusinessList",
							dataType : "json",
							success : function(data) {
								
								for(var i=0; i<data.length; i++)
								{								
									//alert(data[i].productId +"-"+ data[i].productName);
									$("#businessListId").append('<option value='+data[i].businessNameId+'>'+data[i].shopName+'</option>');										
								}	
								//alert("done");
								$("#businessListId").change();
								
							},
							error: function(xhr, status, error) {
								Materialize.Toast.removeAll();
								Materialize.toast('Business List  Not Found', '4000', 'teal lighten-2');
							}
						});		
						
					});
					
					
		$("#barcode").on('blur keydown',function(e){
				if(e.keyCode ==13){
					var btnBarcodeValue=$('#barcodeSubmit').val();
					if ($('#barcode').val() === "") {
						$("#messageWarning").text("Please enter Barcode First");
						$("#validationModal").modal('open');
						$('#barcode').focus();
					}
					var barcode = $('#barcode').val();
					$.ajax({
								type : 'POST',
								async:'false',
								url :myContextPath+"/fetchProductByBarcode?barcode="+barcode,
								success : function(resultData) {
									//addProductInCartUsingBarcodeScanning(resultData.productId);
									console.log(resultData);
									//displayTsales(resultData);
									if(resultData==""){
										Materialize.Toast.removeAll();
										$('.preloader-background').hide();
										$('.preloader-wrapper').hide();
										Materialize.toast('Product Not Found for enter Barcode', '2000', 'teal lighten-2');
									}else{
										addProductInCartUsingBarcodeScanning(resultData.productId);
									}
									
									$('#barcode').val("");
									$(".testButton").attr("disabled",false);
								},
								error : function(err) {
									/*Show Dialog for Server Error*/
									/* alert("Internal Server Error"); */
									$("#validationModal").modal('open');
									$("#messageWarning").text("Internal Server Error");
								}
							});
				}
				
			});
				
  	});
 
  </script>

<c:if test="${isEdit==false}">
	<script type="text/javascript">
		let proformaOrderId = '${proformaOrderId}';
	</script>
	<script type="text/javascript" src="resources/js/counter.js"></script>
</c:if>

<c:if test="${isEdit==true}">
	<script type="text/javascript">
  		const counterOrderId="${counterOrder.counterOrderId}";
  		$(".paymentSection").hide();
  	</script>
	<script type="text/javascript" src="resources/js/counterEdit.js"></script>
</c:if>


<style>
#custInfo {
	width: 35% !important;
}

#tblProduct input {
	margin-bottom: 0px;
	height: 1rem;
}

.select2 {
	width: 100% !important;
	float: right;
	height: 3rem !important;
}

.select-wrapper span.caret {
	color: initial;
	position: absolute;
	right: 0;
	top: 0;
	bottom: 0;
	height: 10px;
	margin: 10px 0;
	font-size: 10px;
	line-height: 10px;
}

.select.select-wrapper span.caret {
	color: initial;
	position: absolute;
	right: 0;
	top: 0;
	bottom: 0;
	height: 10px;
	margin: 18px 0 !important;
	font-size: 10px;
	line-height: 10px;
}
[type="checkbox"]+label{
	font-size:0.9rem !important;
}
</style>
</head>
<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody"> <input type="hidden"
		id="memberIdentifier" value="nonMember">

	<div class="row">
		<div class="col l6 m6 s12">
			<div class="card z-depth-4">
				<div class="card-content">
					<span
						class="card-title sidenav-color white-text center grey-text text-lighten-2"
						style="padding: 5px; font-size: 30px; font-family: Serif;">Product</span>
					<div class="row bottomPadding">
						<!-- <form action="sales_buying_product" method="post"> -->
						<div class="input-field col s12 m12 l9" style="margin-top: 3%">
							<select id="productId" class="select2" required>
								<option value="0" selected>Choose your Product <span class="red-text">*</span>
								</option>
								<c:if test="${not empty productList}">
									<c:forEach var="product" items="${productList}"
										varStatus="status">
										<option value="${product.productId}" id="">${product.productName}</option>
									</c:forEach>
								</c:if>
							</select>
							<!--  <label>Select Product</label>  -->
						</div>
						<!-- pattern="^[\d.]{0,3}$" -->
						<div class="input-field col l3 m4 s6 right">
							<input id="quantityId" type="text" class="num"
								title="only number allowed" required> <label
								for="quantityId">Quantity <span class="red-text">*</span></label><br>
						</div>
						
						<div class="col s12 m4 l4" style="margin-top: 4%;">
							<input name="group1" type="radio" id="freeId" /> <label
								for="freeId" style="padding-left: 25px;">Free</label> <input
								name="group1" type="radio" id="nonFreeId" checked /> <label
								for="nonFreeId" style="padding-left: 25px;">Non-Free</label>
						</div>
						<div class="col s12 m4 l2 discountClass" style="margin-top: 4%;">
							<input id="percentageCheck" type="checkbox" checked /> <label
								for="percentageCheck" style="padding-left: 25px;">Percentage</label>
						</div>
						<div class="input-field col l2 m4 s12 offset-l1 discountClass">
							<input id="discountId" type="text" class=" numWithDecimal"
								title="only number allowed" required> <label
								for="discountId">Discount</label><br>
						</div>
						<div class="input-field col l2 m2 s4" style="margin-top: 4%">
							<button
								class=" editable waves-effect waves-light btn button-submit btnEffect"
								name="CalcHF" id="addCartButtonId" value="submit"
								style="width: 115px;">
								Submit <i class="material-icons tiny right btnHover">navigate_next</i>
							</button>
						</div>
						<!-- </form> -->
					</div>
					
					
					
					 <div class="row bottomPadding">
						<!-- <form action="sales_buying_barcode" method="post"> -->
						<div class="input-field col l11 m12 s12">
							<input id="barcode" name="barcode" type="text"
								placeholder="Enter Barcode Number here" autofocus="autofocus"
								required> <label for="rfidno">Bar Code</label>
						</div>

						<div class="input-field col l1 m1 s1">
							<button id="barcodeSubmit"
								class=" waves-effect waves-light btn button-submit btnEffect"
								name="CalcHF" value="submit" type="button"
								style="width: 115px; visibility: hidden;">
								Submit <i class="material-icons tiny right btnHover">navigate_next</i>
							</button>
						</div>
						<!-- </form> -->
					</div> 
						<div class="row">
						<div class="input-field col l3 m3 s6 center">
							<button id="syncDataBtn" class ="btn">Sync Data<i class="material-icons tiny right btnHover">sync</i></button>
						</div>
						<div class="col s6 m3 l3 invoiceRadioBtnClass" style="margin-top: 4%;">
								<input name="groupInvoice" type="radio" id="invoiceId" checked/> 
								<label for="invoiceId" style="padding-left: 25px;">Invoice</label> 
								
								<input name="groupInvoice" type="radio" id="proformaInvoiceId"/> 
								<label for="proformaInvoiceId" style="padding-left: 25px;">Pro-forma Invoice</label>
								
							
						</div>
					</div>
						<!-- </form> -->
					
				</div>
			</div>
			<div class="row z-depth-4" style="padding: 10px;">
				<!-- <div class="col l12"> -->

				<h4
					class="center-align sidenav-color white-text center grey-text text-lighten-2"
					style="margin: 5px; font-size: 30px; font-family: serif; padding: 5px;">Purchase
					Section</h4>
				<form action="prepaid" method="post">
					<div class="input-field col l7 m6 s12 center">

						<div class="input-field col l12 m12 s12">

							<select class="select2" id="businessListId" name="businessList"
								required style="padding-top: 20px;">
								<option value="0" selected>Business List<span
										class="red-text">*</span>
								</option>
								<c:if test="${not empty businessNameList}">
									<c:forEach var="businessName" items="${businessNameList}"
										varStatus="status">
										<option value="${businessName.businessNameId}">${businessName.shopName}</option>
									</c:forEach>
								</c:if>
							</select>
							<button type="submit" class="btn"
								style="float: right; visibility: hidden;">Check</button>
						</div>
					</div>
				</form>
				<div class="col l5 m6 s12 right" style="padding-top: 8%;">
					<input type="checkbox" value="1" name="transport"
						id="transport_details" class="issue_check" /> <label
						for="transport_details" class="black-text">Transportation
						Details</label>
				</div>
				<!-- </div> -->


				<div class="col s12 m12 l12" style="padding-bottom: 5px;"
					id="transport_details_info">
					<div class="input-field col l3 m12 s12">
						<select id="transport_select" class="select" required>
							<option selected value="0">Select Transport</option>
							<c:forEach var="transportation" items="${transportations}"
								varStatus="status">
								<option
									value="${transportation.transportName}~${transportation.mobNo}~${transportation.gstNo}~${transportation.vehicleNo}~${transportation.id}">${transportation.transportName}</option>
							</c:forEach>
						</select>
					</div>

					<div class="input-field col l3 m12 s12">
						<label for="transGstNo">GST No.</label> <input
							placeholder="GST No" type="text" id="transGstNo" name="gstNo"
							value="" class="grey lighten-3" readonly>
					</div>

					<div class="input-field col l3 m12 s12">
						<label for="transVehicleNo">Vehicle No.</label> <input
							placeholder="Vehicle No" type="text" id="transVehicleNo"
							name="vehicleNo" value="">
					</div>

					<div class="input-field col l3 m12 s12">
						<label for="transDocketNo">Docket No.</label> <input
							placeholder="Docket No" type="text" id="transDocketNo"
							name="docketNo">
					</div>
					<div class="input-field col l3 m12 s12">
						<label for="transportationCharges">Transportation Charges</label> 
						<input placeholder="Transportation Charges" type="text" class="num" id="transportationChargesId"  value="0" name="transportationCharges">
					</div>
					<input type="hidden" id="transMobNo" name="mobNo"> <input
						type="hidden" id="transportName" name="transportName">

				</div>

			</div>

		</div>
		<!--div 2 for bill and main products-->
		<div class="col l6 m6 s12">
			<div class="card z-depth-4">
				<div class="card-content" style="height: 54vh; overflow-y: auto;">
					<span
						class="card-title grey-text text-lighten-2 sidenav-color white-text center"
						style="padding: 5px; font-size: 30px; font-family: Serif;">Bill</span>
					<table id="tblProduct" class="highlight centered">
						<thead>
							<tr>
								<!-- <th>Sr No.</th> -->
								<th width="40%">Product</th>
								<!-- <th>Curr Qty</th> -->
								<th>MRP</th>
								<th>Qty</th>
								<th>Disc.(%)</th>
								<th>Disc. Amt.</th>
								<th>Total(&#8377;)</th>
								<th><i class="material-icons">delete</i></th>
							</tr>
						</thead>
						<tbody id="cartTbl">
							<!-- <tr>
							<td>1</td>
							<td>mouse</td>
							<td>24</td>
							<td><input type="text" value='1'
										class="editable center"  style="width: 4em;" required /></td>
							<td><input type="text" value='2'
										class="editable center"  style="width: 4em;" required /></td>
							<td>2</td>
							<td><a class="deleteIcon cursorPointer"><i
											class="material-icons">delete</i></a></td>
						</tr> -->
							<%-- <c:forEach var="tsales" items="${tsalesList}" varStatus="status">
								<tr>
									<td>1</td>	
									<td>${tsales.pname}</td>
									<fmt:formatNumber var="unitCost" value='${tsales.unitCost}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td>${unitCost}</td>
									<fmt:formatNumber var="pquantity" value='${tsales.pquantity}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td><input type="text" value='${tsales.pquantity}'
										class="editable" readonly style="width: 4em;" required /></td>
									<td>${pquantity}</td>
									<fmt:formatNumber var="pamt" value='${tsales.pamt}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td><input type="text" value='${tsales.pamt}' readonly
										class="editable" style="width: 4em;" required /></td>
									<td><a class="editFields cursorPointer"><i
											class="material-icons editIcon">edit</i> <i
											class="material-icons saveIcon">save</i></a></td>
									<td><a class="deleteIcon"
										href="<c:url value="/deletets/${tsales.tsalesid}"/>"><i
											class="material-icons">delete</i></a></td>
								</tr>
							</c:forEach> --%>
						</tbody>
					</table>
				</div>

				<div class="card-action" style="padding-bottom: 0.1%;">
					<div class="row">
						<!-- <div class="col l3 m6 s6 left "  style="margin-top:4%;">
							<strong>Total : <span id="totalValue">&#8377; <font color="blue"><b>0</b></font></span></strong>
						</div>-->
						<c:if test="${isEdit==true}">
							<div class="col l3 m6 s6 " style="margin-top: 4%;">
								<strong><span id="balanceTotalValue">Refund
										&#8377; <font color="blue"><b>0</b></font>
								</span></strong>
							</div>
						</c:if>
						<%-- <c:if test="${isEdit==true}">
							<div class="dueDateExtraPayDiv"> 
									<div class="row">								
										<div class="input-field col l4 m4 s12 dueDateExtraPayDiv">
											<input type="date" class="datepicker" placeholder="Due Date" name="chequeDate" id="dueDateExtraPay"> 
												<label for="dueDateExtraPay">Due Date <span class="red-text">*</span> </label>
										</div>
									</div>
							 </div>
						</c:if>	 --%>
						<div class="col s12 m4 l2" style="margin-top: 4%;">
							<input id="discountMainCheck" type="checkbox" checked /> <label
								for="discountMainCheck" style="padding-left: 25px;">Discount</label>
						</div>
						<div class="col s12 m4 l3  discountMainClass"
							style="margin-top: 4%;">
							<input id="percentageMainCheck" type="checkbox" checked /> <label
								for="percentageMainCheck" style="padding-left: 25px;">Percentage</label>
						</div>
						<div class="input-field col l2 m4 s12 discountMainClass left-align">
							<input id="discountMainId" type="text" class="center"
								title="only number allowed" required> <label
								for="discountMainId">Discount</label><br>
						</div>
						<div class="col l2 m6 s6 right right-align"
							style="margin-top: 4%;">
							<button type="button" id="clearButtonId"
								class="waves-effect waves-light btn red btnEffect right">
								Clear
							</button>
						</div>

						
					</div>

					<%-- <c:if test="${isEdit==false}"> --%>
					<div class="paymentSection">
						<div class="row">
							<div class="col s12 m12 l12 divider"></div>
							<br>
							<div class="col s12 m4 l4">
								<input name="group2" type="radio" id="instantPay"
									class="payTypesCls" checked /> <label for="instantPay">Instant
									Pay</label>
							</div>
							<div class="col s12 m4 l4">
								<input name="group2" type="radio" id="credit"
									class="payTypesCls" /> <label for="credit">Credit</label>
							</div>

							<div class="col s12 m4 l4">
								<input name="group2" type="radio" id="partialPay"
									class="payTypesCls" /> <label for="partialPay">Partial
									Credit</label>
							</div>
							<div class="partialForm">
								<div class="input-field col l4 m4 s12">
									<input id="amountPartial" name="amountPartial" type="text"
										class="num" title="only number allowed" required> <label
										for="amountPartial">Amount Paid<span class="red-text">*</span></label><br>
								</div>
								<div class="input-field col l4 m4 s12">
									<input id="balAmountPartial" name="balAmountPartial"
										type="text" class="grey lighten-3 num" readonly
										title="only number allowed" required> <label
										for="balAmountPartial">Balance Amount<span
										class="red-text">*</span></label><br>
								</div>
								<div class="input-field col l4 m4 s12">
									<input type="date" class="datepicker chequeDate disableDate"
										placeholder="Due Date" name="chequeDate" id="dueDate">
									<label for="dueDate">Due Date <span class="red-text">*</span>
									</label>
								</div>
							</div>
							<div class="creditDueDate">
								<div class="input-field col l4 m4 s12">
									<input type="date" class="datepicker chequeDate disableDate"
										placeholder="Due Date" name="chequeDate" id="dueDateCredit">
									<label for="dueDateCredit">Due Date <span
										class="red-text">*</span>
									</label>
								</div>
							</div>
						</div>

						<div class="paymentTypeSection">
							<div class="row">
								<div class="col s12 m12 l12 divider"></div>
								<br>
								<div class="col s12 m4 l4">
									<input name="payment" type="radio" id="cash" class="paymentCls"
										checked /> <label for="cash">Cash</label>
								</div>
								<div class="col s12 m4 l4">
									<input name="payment" type="radio" id="cheque"
										class="paymentCls" /> <label for="cheque">Cheque</label>
								</div>
								<div class="col s12 m4 l4">
									<input name="payment" type="radio" id="other"
										class="paymentCls" /> <label for="other">Other</label>
								</div>

								<!--  cheque payment form-->
								<div class="chequeForm">
									<div class="input-field col l4 m4 s12">
										<input id="bankName" name="bankName" type="text" class=""
											title="only number allowed" required> <label
											for="bankName">Bank Name<span class="red-text">*</span></label><br>
									</div>
									<div class="input-field col l4 m4 s12">
										<input id="chqNo" name="chqNo" type="text" class="num"
											title="only number allowed" maxlength="6" minlength="6"
											required> <label for="chqNo">Cheque No.<span
											class="red-text">*</span></label><br>
									</div>
									<div class="input-field col l4 m4 s12">
										<input type="date" class="datepicker chequeDate disableDate"
											placeholder="Cheque Date" name="chequeDate" id="chequeDate">
										<label for="chequeDate">Cheque Date <span
											class="red-text">*</span>
										</label>
									</div>
								</div>


								<!--  <label>Select Product</label>  -->
								<!--Other payment form  -->

								<div class="otherForm">

									<!--  Payment Method List Dropdown-->
									<!-- <form action="selecting payment method" method="post"> -->
									<div class="input-field col l4 m4 s12" style="margin-top: 3%">
										<select id="paymentMethodId" class="select2" required>
											<option value="0" selected>Choose Payment Method <span
													class="red-text">*</span>
											</option>
											<c:if test="${not empty paymentMethodList}">
												<c:forEach var="paymentMethod" items="${paymentMethodList}"
													varStatus="status">
													<option value="${paymentMethod.paymentMethodId}" id="">${paymentMethod.paymentMethodName}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
									<!-- <div class="input-field col l4 m4 s12">
									<input id="paymentMethod" name="paymentMethod" type="text" class="" title="only number allowed" required> 
									<label	for="paymnetMethod">Payment Method<span class="red-text">*</span></label><br>
								</div> -->
									<div class="input-field col l4 m4 s12">
										<input id="transactionRefNo" name="transactionRefNo"
											type="text" class="" title="" required> <label
											for="transactionRefNo">Transaction Ref. No.<span
											class="red-text">*</span></label><br>
									</div>
									<div class="input-field col l4 m4 s12">
										<input id="comment" name="comment" type="text" class=""
											title="" required> <label for="comment">Comment</label><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<%-- </c:if> --%>
					<%-- <c:if test="${isEdit==true}">
						<div class="dueDateExtraPayDiv">
								<div class="row">								
									<div class="input-field col l4 m4 s12">
										<input type="date" class="datepicker" placeholder="Due Date" name="chequeDate" id="dueDateExtraPay"> 
											<label for="dueDateExtraPay">Due Date <span class="red-text">*</span> </label>
									</div>
								</div>
						</div>
					</c:if>	 --%>
					<form method="post"
						action="${pageContext.request.contextPath}/saveCounterOrder"
						id="saveOrderWithBusines">

						<!-- send data -->
						<input type="hidden" class="transportationChargesH" name="checkValue">
						<input type="hidden" class="counterOrderId" name="counterOrderId"
							value="${counterOrder.counterOrderId}"> <input
							type="hidden" class="productListCls" name="productList">
						<input type="hidden" class="businessNameIdCls"
							name="businessNameId"> <input type="hidden"
							class="dicountTypeIdCls" name="discountType"> <input
							type="hidden" class="discountAmountCls" name="discountAmount">

						<input type="hidden" class="paidAmountCls" name="paidAmount">
						<input type="hidden" class="balAmountCls" name="balAmount">
						<input type="hidden" class="refAmountCls" name="refAmount">
						<input type="hidden" class="paymentSituationCls"
							name="paymentSituation"> <input type="hidden"
							class="dueDateCls" name="dueDate"> <input type="hidden"
							class="payTypeCls" name="payType"> <input type="hidden"
							class="paymentCls" name="paymentType"> <input
							type="hidden" class="bankNameCls" name="bankName"> <input
							type="hidden" class="chequeNumberCls" name="chequeNumber">
						<input type="hidden" class="chequeDateCls" name="chequeDate">

						<input type="hidden" class="isTransportationHaveCls"
							name="isTransportationHave"> <input type="hidden"
							class="transportationIdCls" name="transportationId"> <input
							type="hidden" class="vehicalNumberCls" name="vehicalNumber">
						<input type="hidden" class="docketNumberCls" name="docketNumber">

						<input type="hidden" class="paymentMethodIdCls"
							name="paymentMethodId"> <input type="hidden"
							class="transactionRefNoCls" name="transactionRefNo"> <input
							type="hidden" class="commentCls" name="comment">

						<input type="hidden" class="proFormaIdClass"	name="proFormaId">

						<input type="hidden" class="transportationChargesH"	name="transportationChargesH">
						<!-- send Date End -->


						<div class="row">
							<div class="col l6 m6 s6 center">
								<button type="button" value="PAY" id="payButtonId"
									class="waves-effect waves-light btn btnPay btnEffect"
									style="width: 115px;">
									Pay <i class="material-icons tiny right btnHover">navigate_next</i>
								</button>
							</div>
							<div class="col l6 m6 s6 center">
								<button type="button" value="PAY" id="payAndPrintButtonId"
									class="waves-effect waves-light btn btnPay btnEffect"
									style="width: 120px; padding: 0px !important;">
									&nbsp; Pay & Print<i class="material-icons tiny right btnHover">navigate_next</i>
								</button>
							</div>
						</div>
					</form>
					<!-- <a class="modal-trigger" href="#custInfo">Modal</a> -->
				</div>

			</div>
		</div>
	</div>
	<form method="post"
		action="${pageContext.request.contextPath}/saveCounterOrder"
		class="col s12 l12 m12" id="saveCustomerInfoWithOrder">
		<div id="custInfo" class="modal row">
			<div class="modal-content">

				<h5 class="center">
					<u>Customer Info </u>
					<i class="modal-close material-icons right">clear</i>
				</h5>

				<!-- send data -->

				<input type="hidden" class="counterOrderId" name="counterOrderId"
					value="${counterOrder.counterOrderId}"> <input
					type="hidden" class="productListCls" name="productList"> <input
					type="hidden" class="businessNameIdCls" name="businessNameId">

				<input type="hidden" class="dicountTypeIdCls" name="discountType">
				<input type="hidden" class="discountAmountCls" name="discountAmount">

				<input type="hidden" class="paidAmountCls" name="paidAmount">
				<input type="hidden" class="balAmountCls" name="balAmount">
				<input type="hidden" class="refAmountCls" name="refAmount">
				<input type="hidden" class="paymentSituationCls"
					name="paymentSituation"> <input type="hidden"
					class="dueDateCls" name="dueDate"> <input type="hidden"
					class="payTypeCls" name="payType"> <input type="hidden"
					class="paymentCls" name="paymentType"> <input type="hidden"
					class="bankNameCls" name="bankName"> <input type="hidden"
					class="chequeNumberCls" name="chequeNumber"> <input
					type="hidden" class="chequeDateCls" name="chequeDate"> <input
					type="hidden" class="isTransportationHaveCls"
					name="isTransportationHave"> <input type="hidden"
					class="transportationIdCls" name="transportationId"> <input
					type="hidden" class="vehicalNumberCls" name="vehicalNumber">
				<input type="hidden" class="docketNumberCls" name="docketNumber">

				<input type="hidden" class="paymentMethodIdCls"
					name="paymentMethodId"> <input type="hidden"
					class="transactionRefNoCls" name="transactionRefNo"> <input
					type="hidden" class="commentCls" name="comment">
					
				<input type="hidden" class="proFormaIdClass"	name="proFormaId">
				<input type="hidden" class="transportationChargesH"	name="transportationChargesH">
				<!-- send Date End -->


				<div class="row">
					<div class="input-field col s12 l8 m8 offset-l2">
						<input id="custName" type="text" name="custName"
							value="${counterOrder.customerName}"> <label
							for="custName" class="active black-text"><span
							class="red-text">*</span>Enter Name</label>

					</div>
					<div class="input-field col s12 l8 m8 offset-l2">
						<input id="mobileNo" type="text"
							value="${counterOrder.customerMobileNumber}" name="mobileNo"
							maxlength="10" minlength="10"> <label for="mobileNo"
							class="active black-text"><!-- <span class="red-text">*</span> -->Mobile
							No</label>
					</div>
					<div class="input-field col s12 l8 m8 offset-l2">
						<input id="gstNo" type="text" name="gstNo"
							value="${counterOrder.customerGstNumber}" maxlength="15"
							minlength="15"> <label for="gstNo" class="black-text">GST
							No.</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 divider"></div>
				<div class="col s6 m6 l3 offset-l3">
					<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
				</div>
				<div class="col s6 m6 l3">
					<button id="submitCustomerInfo" type="button"
						class="modal-action waves-effect  btn">Submit</button>
				</div>

			</div>

		</div>
	</form>
	<!--end of div 2 for bill and main products-->

	<div id="transportationModal" class="modal" style="width: 35%">
		<div class="modal-content">
			<h4 class="center-align"> Transportation Details <i class="modal-close material-icons right">clear</i></h4>
			<div class="divider"></div>
			<!-- <form name="updateTransport" method="post"> -->
			<div class="row">
			<div class="input-field col l4 m12 s12">
			
				<select id="invoiceSelect" required>
					<option value="-1" disabled selected>Select Invoice</option>
					<c:forEach var="invoice" items="${invoiceDetails}"
						varStatus="status">
						<option
							value="${invoice.orderId}~${invoice.transportation.id}~${invoice.vehicalNumber}~${invoice.docketNumber}~${invoice.transportationCharges}">${invoice.invoiceNumber}</option>
					</c:forEach>
				</select>
			</div>
			<input type="hidden" id="invoiceNum" name="invoiceNumber" />
			<div class="input-field col l8 m12 s12">
			
				<select id="transport_select1" required>
					<option value="-1" disabled selected>Select Transport</option>

					<c:forEach var="transportation" items="${transportations}"
						varStatus="status">
						<option value="${transportation.id}">${transportation.transportName}</option>
					</c:forEach>
				</select>
			</div>
			</div>
			
			<div class="row">
			<div class="input-field col l12 m12 s12">
				<label for="transGstNo1">GST No.</label> 
				<input placeholder="GST No"
					type="text" id="transGstNo1" name="gstNo" value=""
					class="grey lighten-3" readonly>
			</div>
		</div>
		<div class="row">
			<div class="input-field col l6 m12 s12">
				<label for="transVehicleNo1">Vehicle No.</label> <input
					placeholder="Vehicle No" type="text" id="transVehicleNo1"
					name="vehicleNo" value="">
			</div>
				
			<div class="input-field col l6 m12 s12">
				<label for="transDocketNo1">Docket No.</label> <input
					placeholder="Docket No" type="text" id="transDocketNo1"
					name="docketNo">
			</div>
			</div>
			<div class="row">
			<div class="input-field col l12 m12 s12">
				<label for="transportationCharges1">Transportation Charges</label> <input
					placeholder="Transportation Charges" type="text" id="transportationCharges1"
					name="transportationCharges1" value="">
			</div>
			
			</div>
			<input type="hidden" id="transMobNo1" name="mobNo"> <input
				type="hidden" id="transportName1" name="transportName">
			<div class="input-field col l3 m3 s3 center">
				<a href="#!"
					class="modal-action waves-effect waves-green modal-close btn red"
					style="margin-left: 5%;">close</a>
				<button type="button" value="SUBMIT"
					class="waves-effect waves-light btn testButton btnEffect"
					id="updateTransportId" style="width: 115px;">
					Submit <i class="material-icons tiny right btnHover">navigate_next</i>
				</button>
			</div>

			<!-- </form> -->
		</div>
		<!--/*<div class="divider"></div>
		<div class="modal-footer">
			<a href="#!"
				class="modal-action waves-effect waves-green modal-close btn red">close</a>
		</div>*/-->
	</div>
	</main>
</body>
</html>