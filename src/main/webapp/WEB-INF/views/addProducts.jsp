<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@include file="components/header_imports.jsp" %>
	<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resources/js/additional-method.js"></script>
	<script type="text/javascript">
		var igst = 0;
		var cgst = 0;
		var sgst = 0;
		var imageSize = false;
		function checkBarcodeDuplication(checkText, type) {
			var status = false;

			$.ajax({
				url: "${pageContext.servletContext.contextPath}/checkProductBarcodeDuplicationForSave?checkText=" + checkText + "&type=" + type,
				async: false,
				success: function (data) {
					if (data === "Success") {
						status = true;
					} else {
						status = false;
					}
				},
				error: function (xhr, status, error) {
					alert("Error");
				}
			});

			return status;
		}
		$(document).ready(function () {
			if ($.data.isMobile) {
				$(".removeRight").removeClass('right-align');
			}
			$.validator.setDefaults({
				ignore: []
			});

			jQuery.validator.addMethod("barcodeCheck", function (value, element) {
				if (value == '' || value == undefined) {
					return true;
				}
				return checkBarcodeDuplication(value, "productBarcode");
			}, "Barcode Is already in use");


			$('#saveProductForm').validate({

				rules: {
					productBarcode: {
						barcodeCheck: true
					}
				},

				errorElement: "span",
				errorClass: "invalid error",
				errorPlacement: function (error, element) {
					var placement = $(element).data('error');

					if (placement) {
						$(placement).append(error)
					} else {
						error.insertAfter(element);
					}
					$('select').change(function () { $('select').valid(); });
				}
			});



			//image file size validation
			$.validator.addMethod('filesize', function (value, element, param) {
				return this.optional(element) || (element.files[0].size <= param)
			}, 'File size must not exceed 10kb');

			//$('select').change(function(){ $('select').valid(); });
			$('#saveProductForm').validate({
				rules: {
					file: {
						extension: "jpg,jpeg,png",
						filesize: 10000,
					},

				},
				messages: {
					file: {
						extension: 'Upload only png,jpg or jpeg format'
					}
				},
				errorElement: "span",
				errorClass: "invalid error",
				errorPlacement: function (error, element) {
					var placement = $(element).data('error');

					if (placement) {
						$(placement).append(error)
					} else {
						error.insertAfter(element);
					}
					$('select').change(function () { $('select').valid(); });
				}
			});
			/* $("#categoryid").change(function(){
				$.ajax({
					url :"${pageContext.request.contextPath}/fetchCategories?categoriesId="+$("#categoryid").val(),
					dataType : "json",
					success : function(data) {
						igst=data.igst;
						cgst=data.cgst;
						sgst=data.sgst;		
						 $('#productRate').keyup();
						
					}
				});
			}); */
			/* for taking igst value of category selected */
			var categoryIgst;
			var categorySgst;
			var categoryCgst;

			//on category change fetch category details and set categoryIgst,categorySgst,categoryCgst
			$("#categoryid").change(function () {
				var categoryid = $("#categoryid").val();
				if (categoryid == '' || categoryid == undefined) {
					$("#unitRate").val('');
					$("#unitRate").focus();
					$("#unitRate").blur();
					$("#cgst").val('');
					$("#cgst").focus();
					$("#cgst").blur();
					$("#sgst").val('');
					$("#sgst").focus();
					$("#sgst").blur();
					$("#igst").val('');
					$("#igst").focus();
					$("#igst").blur();
					return false;
				}
				$.ajax({
					type: "GET",
					url: "${pageContext.request.contextPath}/fetchCategories?categoriesId=" + categoryid,
					beforeSend: function () {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
					},
					success: function (data) {
						categoryIgst = data.igst;
						categorySgst = data.sgst;
						categoryCgst = data.cgst;
						//console.log(CategoryIgst);
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						$("#productRate").keyup();
					},
					error: function (xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						Materialize.Toast.removeAll();
						Materialize.toast('Something Went Wrong!', '2000', 'red lighten-2');
						/* $('#addeditmsg').modal('open');
								  $('#head').text("Warning: ");
								  $('#msg').text("Something Went Wrong"); 
										setTimeout(function() 
							  {
										$('#addeditmsg').modal('close');
							  }, 1000); */
					}

				});
			});
			/* for calculating unit price of product */
			$("#productRate").keyup(function () {
				var categoryid = $("#categoryid").val();
				if (categoryid == '' || categoryid == undefined) {
					return false;
				}
				var rate = parseFloat($("#productRate").val());
				if (rate > 0) {
					/*var unitprice;
					var divisor;
					var multiplier;
					var igstAmt;
					var cgstAmt;
					var sgstAmt;
					
					 // for calculating divisor means
					divisor=(categoryIgst/100)+1;
					//console.log(divisor);
					unitprice=(rate/divisor);
					//console.log(unitprice);
					cgstAmt=(unitprice*categoryCgst)/100;
					//console.log(igstAmt);
					//for calculating cgst percent from igst percent
					//var cgstPer=(categoryIgst/2);					
					igstAmt=parseFloat(cgstAmt.toFixedVSS(2))+parseFloat(cgstAmt.toFixedVSS(2));
					//console.log(cgstAmt); */

					var correctAmoutWithTaxObj = calculateProperTax(rate, categoryIgst);

					$("#unitRate").val(correctAmoutWithTaxObj.unitPrice);
					$("#unitRate").focus();
					$("#unitRate").blur();
					$("#cgst").val(correctAmoutWithTaxObj.cgst);
					$("#cgst").focus();
					$("#cgst").blur();
					$("#sgst").val(correctAmoutWithTaxObj.sgst);
					$("#sgst").focus();
					$("#sgst").blur();
					$("#igst").val(correctAmoutWithTaxObj.igst);
					$("#igst").focus();
					$("#igst").blur();
				}

				$("#productRate").focus();
			});
			var msg = "${saveMsg}";
			//alert(msg);
			if (msg != '' && msg != undefined) {
				$('#addeditmsg').find("#modalType").addClass("success");
				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
				$('#addeditmsg').modal('open');
				/* $('#head').text("Product Adding Message"); */
				$('#msg').text(msg);
			}

			/* $('#productRate').keydown(function(e){            	
				  -1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			   });
			document.getElementById('productRate').onkeypress=function(e)
			{		if (e.keyCode === 46 && this.value.split('.').length === 2) {
						   return false;
					   }
			} */
			//only number allowed without decimal
			$('#productRate').keypress(function (event) {
				var key = event.which;

				if (!(key >= 48 && key <= 57 || key === 13))
					event.preventDefault();
			});
			//only number allowed without decimal
			$('#threshold').keypress(function (event) {
				var key = event.which;

				if (!(key >= 48 && key <= 57))
					event.preventDefault();
			});

			/* $('#productRate').on('keyup',function(){
				  var productrate=$('#productRate').val();
				  //alert(productrate);
				  if(productrate===undefined || productrate==="")
				  {
					  $('#productRateWithTax').val('0');
					  $('#productTax').val('0');
				  }
				  else
				  {
					  var tax=( (parseFloat(productrate)*parseFloat(igst))/100 )+
							   ( (parseFloat(productrate)*parseFloat(cgst))/100 )+
							   ( (parseFloat(productrate)*parseFloat(sgst))/100 );
					  var totalProduct=parseFloat(productrate)+tax;
					  $('#productRateWithTax').val(totalProduct);
					  $('#productTax').val(tax);
				  }
				  $('#productRateWithTax').change();
				  $('#productTax').change();
			}); */
			/* $("#saveProductSubmit").click(function() {

				   var brandid = $("#brandid").val();
				  //alert(brandid);
				  if (brandid === "0") {
					  $('#addeditmsg').modal('open');
					   $('#head').text("Product Adding Message");
					   $('#msg').text("Select Brand");
					  return false;
				  }
			   
				  var categoryid = $("#categoryid").val();
				  //alert(categoryid);
				  if (categoryid === "0") {
					  $('#addeditmsg').modal('open');
					   $('#head').text("Product Adding Message");
					   $('#msg').text("Select Category");
					  return false;
				  }
			  	
			  	
			  });  */
			//on click in add image button upload click event execute
			$("#addImage").click(function () {
				$("#upload").click();
			});
			/**
			 * on image file upload 
			 * extension validation only png/jpeg allowed
			 * 10 kb file size validation
			 */
			$('#upload').change(function (evt) {

				var filename = $("#upload").val();

				// Use a regular expression to trim everything before final dot
				var extension = filename.replace(/^.*\./, '');

				// Iff there is no dot anywhere in filename, we would have extension == filename,
				// so we account for this possibility now
				if (extension == filename) {
					extension = '';
				} else {
					// if there is an extension, we convert to lower case
					// (N.B. this conversion will not effect the value of the extension
					// on the file upload.)
					extension = extension.toLowerCase();
				}

				switch (extension) {
					case 'jpg':
					case 'jpeg':
					case 'png':
						{
							//alert("it's got an extension which suggests it's a PNG or JPG image (but N.B. that's only its name, so let's be sure that we, say, check the mime-type server-side!)");
							var tgt = evt.target || window.event.srcElement,
								files = tgt.files;

							var fileSize = files[0].size;
							var kb = fileSize / 1024;
							if (kb > 100) {
								Materialize.Toast.removeAll();
								Materialize.toast('Please upload less than 10kb sizes image!', '2000', 'red lighten-2');
								$("#upload").val('');
								/* $('#addeditmsg').modal('open');
								$('#head').text("Image Upload Error : ");
								$('#msg').text("Please upload less than 10kb sizes image"); */
								return false;
							}

							$('#imgContentType').val(files[0].type);
							// FileReader support
							if (FileReader && files && files.length) {
								var fr = new FileReader();
								fr.onload = function () {
									document.getElementById('addImage').src = fr.result;
								}
								fr.readAsDataURL(files[0]);
							}

							// Not supported
							else {
								// fallback -- perhaps submit the input to an iframe and temporarily store
								// them on the server until the user's session ends.
							}
							return false;
						}
					// uncomment the next line to allow the form to submitted in this case:
					//			          break;

					default:
						Materialize.Toast.removeAll();
						Materialize.toast('Only png and jpeg image is allowed!', '2000', 'red lighten-2');
						$("#upload").val('');
						/* $('#addeditmsg').modal('open');
						 $('#head').text("Image Upload Error : ");
						 $('#msg').text("Only png and jpeg image allowed"); */
						// Cancel the form submission
						return false;
				}


			});

		});
		function showData(data) {
			console.log(data);
		}

	</script>
	<style>
		#upload-error {
			display: block;
		}
	</style>
</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">
		<br>
		<div class="container">

			<form action="${pageContext.servletContext.contextPath}/saveProduct" method="post" id="saveProductForm"
				enctype="multipart/form-data">
				<!-- <input id="productId" type="text" class="validate" name="productId" value="0">-->
				<div class="row  z-depth-3">
					<br>
					<div class="row" style="margin-bottom:0">
						<div class="input-field col s12 m4 l4 center-align" style="height:145px">

							<!-- here addImage Id is commented because right now we don't want to upload the image.
							The whole upload code is remain same but only id is commented due to which code is not working until the id is not given.
							 In future if we want to upload a image then just give the id to img section-->
							
 							<!-- id="addImage" -->
							<img style="border:1px solid;height:120px;width:120px;"
								src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAHCCAYAAAB8GMlFAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAFKESURBVHhe7d0FsytNcrXt72+ZXttjO8zMzMzMzMzMzMzMzMzM9pgZ9xeXJnKmpt2SWlI1SL3uiIxznrPV1YW5KrNK+/n/nkIIIYQdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQyhE//7v/+7moUQridCGMKVEKD/+Z//efrv//7vp//8z/98+o//+I9n27//+78//du//dvTv/7rvz6X/fM///PFNixDuax9n/erh/pEGEO4jAhhCEeoaKvE7r/+678OgsNK6Ni//Mu/HATrH//xH59t//AP//D093//909/93d/91z2t3/7t09/8zd/M9l8fliGcpXfvu+f/umfDvWoOqlf1VW91b8VyohlCM8hQhhCQwkf8WgjO0JHbI4JHCNaU2xM8I7Z2PPHbFifVjArstSWiiATPYbwLCKEYbcQgTatSfSIhciK6FVkx0pcCM6YYA1tKFJz2Nh7h1afHQpjK47aXRGk/og4hr0RIQy7oCK9NsVZwldpzYr0WpEpIXkka4WyFcg2amxTqokcw6MTIQwPTRv1cfCivRK9Er4Sv6FYDP/t0WzYxuqL6hviqL+Io4g50WJ4VCKE4eGoM7424qvzvRK9Nirag+hNsWGfsBLGYSpV/+rnEB6BCGG4e9q0Z6U8OW3CN+bwY7dZpVH1c0WKSZ+GeyZCGO6aEj+RyvBWJ6fdRjdDhx6bbsN+rEhRf9dXN5I+DfdKhDDcFe2Zn4ikbnhyymMOOzaPDfu50qd1pmh8IorhXogQhrug0p+iDo62LrwMHXRsfRMtGh/jlCgx3AMRwrBpOFAXM4Y3Poepz6Ezji1vbZTYpk6dJxo/4xhBDFskQhg2B2dZ6U8OtC6+tOI3dMKx7Vkriq0gJm0atkaEMGwGjlH6sy6/tOIXewwrUaxzxNw2DVsgQhg2QZ3/telPjrOiiqFDjd2ftWNZl2tEiXWOGMJaRAjDalQKtC7AuP0pYoj4Pb61Y5yLNWFtIoRhFSoFKgJMCjRWKVPzoc4QQ1iKCGFYlEdOgVYb1rCx+tyLtW1ob5qaJ+ZLCHMTIQyLUGlQX4JvU6BDp7hlK4fdWv1MexhHXs58aIR/zPRH2djP2Vh59a5695R63oNVfbXbfDFvIohhTiKEYXYqCrTLL8c9dH73aCV8HDYR0z6RrvMuDrxM25mUX/2vjVrTP2XDn/m856qMtlzv8T7vLRElkPfev61w61/t87UL/RHCHEQIw2yUAFYalIPecoRSdWutxI7AlNgRII65RI5QDUWuFTfR8K0XQKqMttyhWJZgqpf6qWeJZEWQ7Ri0NtYfWzB1U2f1N4+0T9tD6EmEMHSHw+acOWMCOObgtmBDIWhFj6l7RSOsxO5eHLF6lkhWG2pTUm0scRzrjy2aumvHPY1D2D4RwtAVzsmuXRTSOtktW0UcFe2pP0fbRnY9orq1GEaTFUVqZ0WN2n8PY8XMK4Io8tWeEG4lQhi6UFEgx1pR4FYii2Gkw+GrI7GuNCdREDkRiXsVvEvRTu2tlKp+KGHUPwRnrP/WtqoH8VZf824vYxbmIUIYbsaunCPlQMt5rm2t0yZ86sVxEj/pQdFEHOj/pcSxLuMMzxfH+ndNUy/zzvxT7xCuIUIYbmIYBbKtOMlKeXLmop2K+PYU9V2L/qk0ap0xbjmFql42OOZjCJcSIQxXwUlykG0UuJYAem9ZK34EWmSjnjlLuo0a72GkqL/b/h8bn7mtHfuKDjPe4RIihOFiOBkRQhsFrmkcYCuAFf0l6psH/VpRYiuIbGx8ljb1UbdE/mEqEcIwGU5F6kkKagtRoD/VoxU/9SPUcYDzon/1s/6uSLFNnW5hXqhPNkRhChHCMAlOry7ErOXo6p1j0V9SYetSmySCaKNUgtiO25JW7zRHkioN54gQhrNwIhycVOia6a+hAHK82e1vi4oUa9PUCuJaZt5WqjSEMSKE4SicmmjL5QgOjVNZendf7/N+kQYHW+nPsF3MHcJjvCpCrPFccg4N509SpWGMCGEYpURwjVRovasiQHUQkRLAcH8QxOEZYjvOS5h3eW+lSiOGoSVCGP4PrQjWpZilrS471PlOHNd9Y/wqZdpetlraIoZhjAhheC7qPJCzqJ37EmbHXrv2NgKMs3osjGddqqmNVo392LzobTXHcm4YWiKE4dlwCpxDe54zdCS9rX1HneMkBboPjHOdP9ema0lBJMLeb95nw7VvIoThAGdQTomTWMohsTYNmh36vjDexn3pNHzN8RLDbL72TYQwPFsEyxHNLYJVvvflqxCh0qXmgfmwxjzMjdJ9EyHcMeWAlr68IA1WadBcWgiFeVBf12m/szq3ILI6m44Y7pMI4Y4hgu2FhTEH0ds4HLt+lyVyGzQMMR/qdqm5WWI4t5n/rRiGfREh3CGcTUWCFj8nMKcQlpOxy7fbz647nKPm6DA6nMtqDbRimDm6HyKEO6McTEWCY06ht3Eu9d0tu/0QpmK+EqX27HBOKzH0vojhfogQ7gyLu40Ex5xBLyun4n3EN04lXENt3upC15zztspuI8Pw+EQId0IbCZYIzuVQSgBdiOG8vDeEW3G72c3SSpXOOYeZd4gMs4l7fCKEO2EogmMLv5eVA6kLMSH0giBJsZtfc8/l2tAlMnx8IoQ7wE56qYsx9bUIjiMiGOaAGJpfsg3m29g87GG1TloxTGT4mEQIH5wlvixfZVYq1DtDmJtKlc4phmUlhkmTPiYRwgemRJCjmDMKZN7BKUUEw5KYb1Lwc4uh9UMM6+JXeCwihA+KtCRhqlt2c0aCLi9wRhHBsAbmeonhXBu+Ktd6Stbj8YgQPiAlgrVLnss52CG7tJBfkxbWpC7QLJH5YCWGOQN/HCKED0ZdJJg7VcQZ5AJB2ALmYN0iHZurvY3Ymv/ZAD4OEcIHokSQQNWCHS7iW0x5rL1FF8Ja1HxfUgRZrSvvzUbwMYgQPhAlgnM5haEIxgGEtVhLBFvz3ojhYxAhfBAc3s8pgkw6KL8uLWyBtUWwrDaGuUl630QIH4C6HGNRzpEO9WddEMiCD2vTimDv+X6pVZYkN0nvmwjhA9B+j2oOIaxIMAs9rMkW0qFDq/Vm/dmMJlNyn0QI7xwRGscwXKC9LCIYtsLWRHBo6pbzwvskQnjHECci1TtFVGUpNyIY1qaNBIdzdStmzVgvOS+8TyKEd8rc54K1qDmgENZk6yJYVusw54X3R4TwTpnzXLAVwaR5wpqUCM6x4ett6sdyXnh/RAjvDItrznPBiGDYAm061Jwcm6u3WgnXHALr9+9mDd0PEcI7gwjOcS7IlFkLOIQ1KREcm6c9bQ6RtS5rQ5nzwvsgQnhH2F1KubjJOcdOVkpHyjW72LAmbSQ4R7TGlO0dzvNs/sY+c63V2rROkyK9DyKEd0S7S55DBC3ae/+N+pyOiwr6Kna7iWiWmhPGzjvnjARr3RA/vzRb2+q8vWd02L5HmyKG2yZCeCe0X5UYLrpbrUTw3m+6lSOVkrIb11f+jF1n+m/JDVKJ4BxzvDXiRPyqTf7Uxt6RYVlSpNsnQngHtLvWnpFgpW8e5bq3ftIWjlTbnvnMZx7+jF1n1X8VPc0Z1dQGpsZubL72sKEIFiWG1kPP9ytLmY+QbXlkIoR3ACdRu9XeTqJ2q/eeulF/7ZgzrbZXI06yEXM48qXGTRuOiWBRWZex56+1Wq+1mQjbJEK4cTiKNsoZLrRr7BEXJyemn3rv6GPPmi/myhzpvSXSoVNEsGjr03O9KW+uzUS4nQjhhiGChEpKtBbUcJFdY8p5tHSNfuLsqn3DNseut5ovNho958sS6dBLRBDWXO9jiCpHmVPrEZYlQrhh5koZcWp2p49wLgiOpaLBsfbGbjeCwpH3yCAQm61Fgi1zzqe5IutwGxHCjcJZiNh6X5Bhj3SLrXWqY22N9bGagwRCn9/CEiLIiA7hvqa+dV7Ys476UHm5OLM9IoQbpaJBC6eHEFYZnANH9ChwcokG57eaP+ak+XONuCy5abkmEhzS1rXnGqw+DNshQrhB5kjNWITKsxu9dUe/JeZKH8fGzRy65tLH0iIoErw16lJnYqrNPYSwzObW+n6Uo4lHIEK4MSy+9uJHL7P4pEQfafHlpug6dk1WoUSwV4bjmFUk2GuzN0eKlPWIWEM/IoQbY64Ip3bJjxQNcq61YYgQLmOVWbABmeLEl4gE1YlQzSEuc9ZffS/dUIR5iBBuCItujlSM8h4tJaot2qRtY22OzWMlOoRhihNfKnVdIjjHHG/X5di7r7Hqx0dbl/dKhHBDcBqVhukhhK3TUvajwHHMHWXExq3m5RQh9POlvifY40zwFFKkPdtSZTzSDe57JkK4ESrC6fnleWXUl3gfadfJ4dkwJBpcxyr6Onbe3G5UCMdYGbeauV0iqC5zn7VpE7Ht9XWmKqOyNXPXP5wmQrgR2hRSj4XGLDKC8UgXZKCvOMCxNsfmsZqTU4RnbhEsW0oEC++Z4za3vkpUuC4Rwg1gt2lB127zViGsMqakr+4NzqjOBm/tp9g0q/lkfp6LXloRnHN8lhbBotpXfTJWt0tMGeaytjxS1ubeiBBugIoGe+2gLS5l3cNBvPpNsfqs9NQSjjb2HNPPJYJT0qFjZfQy476WCEI79UPv+affEhWuR4RwZSwsi7pXhFMiuMVDeG3lvBiHqn6c5xTzWSKYs8HlTX9fEgmOldHD1hbBwlzsfaltSh+H+YgQrkzdRqsFMVwk1xhnwTERnjWxqEvAOC/nKxwI02aO8xLTLg5jTmcbe267NBLsNYfHzDvWFkFos3ltPvYSQn9qn3LD8kQIV6SiQc6mXRC3GKEgNGuJoPeWAGobwYuA3aedi1JaEVwqElxrXg9Rj94XZ6q/t9LGPREhXBEOpqLBW63dVXJOa6A9lb4cEz91vNWqrNg8Vn18LhLEEiLISgTXjgSHaL/1q/295qb+3Fo790CEcCVqN83h9FhEyrAgp/7qq554n7Z4N6dVjqFsrL6x7ZoNjLE8JYIi/hKBsTJ6mLK3KoJQJ5sF/dVrDfMH1lKiwmWJEK4EJ1OplV5iwWmIyJbCYtUOjsq7x+oUuy/jiE+JoDEngnuOBFsqKh6r/6XGD0zZhIT+RAhXog7bxxbEpbbGAqqIVhrUu6sevUQ9tpzVmJUInhKecvxri6D5t4WoqXdUyLRdP4fliBCugAVskVcKcWwxXGLKsHiI6xLOweKv7/N5/zOf+cz/U6fY/Zj5c+5MsDY+vaKfUzZFBNVFff15SriXoCLkXmuZX9C2tdu1JyKEK1DnKzXxh4vhUrNwlrop6h1EsKLZHvWPrWuVTTjleJcWwWNzuRVk897nlzwOGEOd9F+PKLnWk/Zp5xJrOkQIF8fEttDtwIeL4FJrF80SzkDd20jQ+yOE92k1buciQXDIvW9HDk256jIlEhxuIv332qJR4tzW6xarcYkQLkOEcGE4nF637Sw4u/kl0ijlhHou9th6VsJz6ly5HfMe83XM1GPKPB4TwTJ18+8yLWuh3urf66yw2rTUmf/eiRAujMXMAfVYLExqSJlzU+ncOaOC2HI2NRKsjc9cVkI2VQTH5l+V4XhgTeGwRurI4FbTJmO0dtp3L0QIF8RCl/rpsWv0vHKWWPzq7T3lhG6te2wdq3G7RATn2vgo0yWrSyLBY/Ov/ltZ584656TWiXq09bvGtOlc34R+RAgXxI6xFZSxBTDVPF8XBTiLuVB2L/GOrWvGr0TwnPDMGQmqB7tGBMfKK6v2zb0mjlH17REVaos2a7syw7xECBfEAu21SJhFMnc0SLznPCOKLWdTIowSwTnH29ydKoKX1qXEY60UqfZ4f63RsTpeYoTdRjTMS4RwISxsC99CvXWBeF450kBz73yrzmP1iG3faq5xqFMvxrTP9TRlsktEsJ4blnXKlL+meGibetzah55fap3vnQjhQnBA0qI1wYeT/lJbIi2qzna33tejzrFnmb5sbewzvUz5JTxTRHCuTU+1tepybN4O63Jp//i855Y4Oz+G+vfK/PjTGlzzRuweiBAuQC3uWhyXLu7WaqHPvTjs1jkskcRYPWLbtppjFQneWzp0rIxLTLtPvWtOau1o7y1rvWyJTe/eiRAuQLswTOxbhVA5Uj9zLgy76R4OaU9mbMr8N2Fhxotj5tDK9G1Z++8+5/P17Fi5U63E4FRkdEv0NcWq3to0RQR7ZiCUtYYQQlv0f4926Ls1b8PugQjhArQpxh7GYc6dKlF+r4X8yNb2T4keM0bGXIqOEyMCNi/M7p6jLKt/Zz7n857zvHKqzBLG4XvHzNgpZyvp0FOOvBXBXoKsjCXWyTF6bySVdWpDE24jQrgAFrlF2WOBcxRzn39wTJwyB9ajzo9q+oYZE+NrXErwjLkxYgSA6ddzVp+tZ0solat87ymxGI5N/bdx8/lTc2RuEWTqU3U5J4I9RYPVu/WddyyN9hJ/dRir3yWmLTY2SY/OR4RwZkxcjqCHqHieI7S4jzmWHnCgnG6POj+S6YvqD31jLEQxHB4npd+Md29npbwSR+/xvjZabOt2SSTYtq2nVV3UbaoI9ooEy+r9p/piTnr2s7boH22Zc93vmQjhzEjNEJVbF7pnGQc498Keyzndu+kPRoA4Jf3EMfUWvnOUMHq/ehA/9fLn1EhwrH29zJwpETrVN+piPtczw3JuMeXpE+WvlR41RtZ+1WdYx6m2hbY8OhHCmenpeCyGc86lByJOTnWsDnuzcmD6gyPSN5wRsVlaAId4v3qYY+olWjwVMdRcNI96C0+Zcq+JBOeqj02LflkL/aCNY3W71NZuyyMTIZwZDqrSV7caZ6y8uelZ53s3/cBhc2j3uhsfCs9YO2+1ErOKBKeIYD03LKunWTNriod399pUGjvzMPQnQjgznIJJfMuC96xFwHlwInMTIXyW06kUKAFcO/q7hVZ45rISwamRYD0zLKe3rS2E2iuTYD7d6gOYVOs9z8WtEiGckZ5nBOd22j2pdM4tdb5Hq/bqa+PGia1x0aInJTxzjacya36eEkGUKLTPDcvracrX7iWyKMfQH/pF/9zS3uov/Xfvc3KLRAhnRCTRLvzh5J5qnrWQltrZ9ohi78nKyfh7GwXeMxV91fybw6rfapN2LFJpI8ElN1j1njXTidV2kekt7a6+Nj+VF/oSIZwJC8BO1MQdm9iXmAVgIS3lnPcohP6sywhLRN1zU5swIjVsby/Tb+ciQetAXZYWQVbvWlMIIYIzt3q0nR84F3mHy4kQzoSJSlB6OSKOZKmUyN6EkIMmGkTwWFRzT5QIatdYe28184KdE0FUXdrnhuXNZfWutYVQ//SKzPW5tH3So32JEM6EiWrC9nBGJj9xihD2t3IsS0Xbc1LRV695N2YlZlNEsF0Da8ylLQmhOuizYR0vtdq0RQj7EiGcCRO1bsfdatIh0qxLRSt7EcJHEkGUCPZwuMfsUhEcK2Mp24oQWreyDXVMcuu6Us6jzNmtECGciR4H5GV2gEtO/D0IobFZMsouOEUC0pp/u2WT49m5RdBcYFNEsOqyViRYVu9eWwihT9oUcVvPS6zGYM2vhDwiEcIZ4Jjqu3i3Tnp/ciqnHE9vHl0IjYs2ztmnreARW47Q5si84Jhb49SYn/ucz3tuijhGBI9bvV+d10af9VhXntW32jRlfoRpRAhngCMz6W91BvWsspbkUYWw2jPnGUsJE8HTj94lRS6dJQplBKW1+nfmcz7vWcJ4SnTgXd6hnGF7e5l+U/45EdSnJYL13LCsJa3evwUhhHq09brGPKt/l94cPzoRwhloHcKtk74c0JI8ohBWW4hM7+9hjYkfQTN2NQem2jOf+czDn1XPU7v+JSNB7bqXSLCs6rEVIaws0bCel5g2lRDq89CHCOEM1C69h1PgUJc+D3hEIWRTxOUSlGPTY3xqvMfee6mJDDnvU1FrzbG5RJAZf+Wry7lIUF3qmWE5a1nVZStCaO6Zg7fME23yvP7uvaHbMxHCGeAYCdjYRJ5qNeHniGDO8UhCWG0gLnbkvUSQMCjP+BCL2vS0NqzLKWvrWcJzrK5t9DUsp5epzxQRHEaCl7Z7Tqu6bEUIe2+Qe87nvRMhnAETlEMbm8BTzUKxYNZIgTyaEOpHzvBUhDUVjsd46KPa7EhnDt97qalnK4LHKGdKpOYYH2VWn91jOrS1qtNWhLCOTHqMXW1SIoR9iBB2xsQ0QU3UsQk81SyUKTvyOXi01CjB6rGZMA6i/VvTW2NmrM8JT6Uge7+7tZp36nJq41BOfasiyKpeWxFCY1ub5Fv7rDYqEcI+RAg7Y2L2EBLPVjpvaR5BCNW97cNbHQYnxqFWpF/lD997idXzU4Snjb6G5fQy9ZlalzbFd2s/zGVVr60IoTlYxya39Fk9az5ECPsQIeyMiclJ3OogPMvprnEg/ihC6E/O4paI2niWCBKJnn1SY3xKeLx/bhGsNlWUcaq/qi7tc1u1qt9WhBD6r4cQMpmJW+Z2eA4Rws5waCZoTdaxiTzFPGvBnNqZz8W9C6F6c+r679YbtxyX/igR7Nkn50QQJTzeP1ZGD9OmigRPOVb1LEG+h7lRddySEOrf8g/D+l5inu+V8g8Rwu6I4G7d8ZWtteN7FCE859jP4dkSwSp3+K5Lrcogghz0miKoLm1fTalLiWCPvpjbqo5bE0IZo2FdLzVt42fyFYo+RAg7Io3lPKqHEHI4FkyE8HJTb0IjGrz2DEW/97rY0FrV7ZwIVvQ1twhWJHisLpWabc8Ex8rbolVdtyaEtaEY1vcS07bKeFw7x8NziBB2xCS36Di6scl7iU1JVc3FvQshJ8PZnBKaU5QIcjTK69kPxtUcOTWuc4sg0yb9dC4SrLrUM8NytmxV3y0JIdFSn1vHVtumzKUwjQhhRzgNjqWHA6uoYY3d3r0KYdX3lp2yZ6Sb5viKxJRIcKl0qPLPiSD8XH/Wr34bK3OrVvXdmhBWpqGt4zU2dQzDeSKEHYkQrmtVX0Jy7S7ZGHq+twiaE+d27+053FgZt5r+YeVAp/SR/uhx+WsNq/puUQh7ZBuMo/kSIbydCGFHeghhLYw18//3KoSMiFzr+Nrdes+2T3FYdQ53y9w5Z9pUIjjVeeoTn59LnOe0GsOtCWFlHNo6XmMRwn5ECDtiQpqYPYTQQrFgIoSXWW0grmGOSyHlrJR9jC1Ggi36syKYe7Iaw60JYc2zto7XmPFUToTwdiKEHTEhy5GOTdwpVgtDORZMhHCaqat+vyTaadHPHOYtm5ihTRHBmjM93zu0W/vGMyXU9zYn/LklIUQvITQeNsyn5leYRoSwIybkrTvnWhgczzVOqwdrC6EFXjb28zFTV2JyTRTdc5depi7nhMc7p0aCl/ZHWfXLtSII/SNlrJxr+6bqf207Ttmw7LL6mbpviZ5zTRo/3yW8nQhhR/YkhByMxczJMH8v53OpWcye915pOAu7zH/b0esPfXvsHerq59f0mVRhGw227fZv3q0erNp5rG/8u/acqkcJr3JPiUv7bp/XH+o5ZY5VmcrQr9deHiq8f+rZqf7xWfWs+rdjWuOqXvrTZy+dOz6vfHNvWHZZvXdrmBv6RTum9Ocp03dbbOO9ESHsSE8h7OG8zmFBeo86l1OBP9u6jJmf/cVf/MXTH//xHx/M3y/93xFx0hayv//VX/3V05/92Z8dyvqjP/qjg/3hH/7h4c8//dM/ffrLv/zLQ/n1zJjj5FSvEULPeHZYHvPOP//zP392nbRT2485bvWTruLo/KmuTPkcszEtR3jO+VcfD/tCX419vjXPjolgRXfGW/3UaUoErc4+P/au1ryT+bu+qzlS9a+2+Df9+td//dfPrms9N8U8ox/+5E/+5FBeW377Hn/6XI1Da+2YLIn+Ni7GXzvG2jfVtCNCeDsRwo7ckxBajITnO77jO54+6ZM+6enjP/7jn773e7/3UH/vPbZI/bvFx8F83/d939OnfuqnPn3Kp3zK0/d8z/ccHM9Uh6YcbSQIv/u7v3t4/iu+4iuePvmTP/npwz/8ww/2oR/6oU8f8zEf8/SFX/iFT9/5nd/59PM///MHxykqUoe2PO+8ts84EuW17VWe//793//9p2/+5m8+1OPjPu7jDvXgwDnRoZB5Rln68Ed+5EeePuuzPuvw3Ed8xEc8fe7nfu7h3wh6CVH7bGv1bv3yrd/6rU8f+7Ef+/SBH/iBTx/1UR/19AVf8AVPP/dzP3eo77EyPHusP/y7vv6ET/iEp8/4jM841Ek7zqGcc+LtZxXp/M7v/M5hvPSdufXBH/zBzx7XD/mQDzn05Vd91Vc9/fAP//DTr//6rx/qXOW34zA0P2cE9id/8iefPvuzP/tQ9od92Icd+nnMPvqjP/rwvjL/rS+/6Iu+6NCX+nFJIoTbI0LYkUofjU3YqVYLY24hVDYHxEm94iu+4tOLvdiLPX3AB3zA08/8zM8cdv6cA6c5rJ9/8/Pf/u3fPgjUG7zBGzy97uu+7tPnfd7nPf3Wb/3Wof2n+sDiVwYjKD/90z99cEjv8z7v8/Smb/qmTy/3ci/39EIv9EJPL/iCL/j0Ai/wAod6Kf/d3/3dnz7xEz/xINycrHJaIfB3AnNpn/m854ZOSRv896/92q8dROOVXumVnl71VV/1IG42AfVMfd7fjRm0Td+83uu93tNLvMRLPL3wC7/w02u8xmsc+vqXfumXzqY3iSzB/7Ef+7HDZuDlX/7lD33iz3d6p3d6+u7v/u7D88MoVn2Z96vLMDrmgH/jN37jIKr61bgTBG08h34a24CUqYt5YTP0Az/wA4cx1d53eId3OPTB8z3f8x3awIzri7/4iz+9+Zu/+dMHfdAHPX3O53zO4Rn9qvxTfVNzRyRIZN/yLd/y6RnPeMbTi77oiz69yIu8yKF8/d2af/t//+//Pdue93mf9+l5nud5nt7ojd7o6au/+qsPm5Ml6SmE+iJCeDsRwo6YkCbmLZO7nl1CCDlF0dyrvdqrHZwIx/Bpn/ZpT7/4i794aMeYQ/LvJYRf8iVf8vQmb/Imh+c4/ilC6GfK4PSIGqf8hm/4hgehePVXf/XnMvXyZ/3sdV7ndQ6O1bs4b+Upi0NRJ/3PyVyCzctYJKKe/tt7RKnq8pqv+ZqHyE5ajyjUZ9VBGTVenv3iL/7iQ78Q9pd6qZc6COm7vMu7HKLoEo16fmjG3ibha7/2aw/tfYVXeIWDmLzyK7/y07u+67seIrpjQliCPJYi9nkiSjyMN2F9q7d6q6dv+7ZvG/18i37Vv1Xvtq+8U/uJ09d//dcfNi02L/rrtV/7tZ9rLNu/+7k/bRje+73f++lbvuVbDu2uqHLM9DWTIvb5t33bt316yZd8yUMf2ajUO06ZfjQe+lLbpXCXpJcQ1nhL74bbiBB2pKcQcrSXOvVL4LR/8zd/85AeIzAcLWdil07gpOUsshKaqp//5gxFZV/2ZV/29BZv8RZPb/Zmb3Zw/MTxnBByxtJanNi7vdu7HZwX45BFpCLLb/zGbzzs9n3ma77maw6p23d8x3c8OFVRjHeKzH7lV37lUJcSBaJ2Ka1zb62EUNRsc+DdHPbnf/7nP5cQ6o+h8Pg3faNfOP3Xeq3XOog5YfzyL//yQ/uV3/Zra8q2Ufj0T//0p9d//dc/lMFxK+M93/M9D2LaCqF6sqrLsQ2UNK9+M96v8iqvcijb3wm9953rPz9v3+lP7fBvIsFv+IZveHqP93iPp5d+6Zc+CK3+EnFq8zd90zcdxpVJ90qL+pk+sVEwrlKmP/qjP3qI0I71jzYyQqict3/7tz+Im36xQTJnvEtdyoidDUCZ9LZN2I//+I8f6q2/l8Y7ta/68RrzrDIihLcTIexIbyGckxJCztbunQgyUQKB4ig47Nq5Vv207xoh9Bznzcn90A/90NP7vu/7HqIlIsEBEjz/zjF5p9SjBe65X/3VXz04M3V9m7d5m4PTFIl+6Zd+6dMf/MEfHD5zTVoU3jEW+WqDsTglhD4jehlGU+qjb9SReIla9JMynFFJj4pCxt6rr/27z0gbEioRnDHi8AlNK4TqWA7xlAj69x/8wR98drQmAnNeJ7Vtk/Fd3/VdhzJPoYxWCL2z+snztbHRZtEWYXIGV+e66sesE89Kw5s36uIZbZVO/YVf+IVDXY7NI9ZGhMbF3JCRINbmgnFRV39ukbF0/KVWYxAhvJ0IYUfuUQhddhEZMILGkREnDvuXf/mXD85Em6p+/n6NEJbj5hhdVqgoyUWH7//+7z9c3OEwLWzOq8y79AnhUF9nTxVpceqiAs9eI4LgkNr2lZWDPyaEnA+nPpZSVJ6+kfL1HEET7dZZZ1240T7trXd6zns5+W//9m8/pEVF6C58iHyMDbEpIay+8dwpEYRy9Z1xJqzOxmx2vENfSpFr1ylkKNS56qsOxsU8EVUaU2L9kR/5kYdLMKJMP1fPdkyZuWC8pVMJtDNiG5y3e7u3O0Rt2qT8tn+qj9iYEJofhNDaUb73+nPOzMq1RAi3RYSwI/cohByYqECk4VKInTyB4ixL3DgUTkm9tM9/XyKEFqvnOD6pKpdiXuZlXubZaT7OsoTF59pnmfL0LQeqzpXOrcsexOpavNM7hmPmnf5tTAg5bwJ47FzNs9LLhI/wfOZnfubhduMbv/EbH8TRzU8iUc6wfae+dS4phenz+kg/v9/7vd8hnWl89JnPGhP9dU4EzSViZdPhXM65rChb+ttlHGdszi9tUs5R/cWMh02IVCQBI9Q1pvrOmGnP2Jhqt58RLmW4MaxtNgyiS1958FzbP8y/sVYI9TMhlyonfPrGZ5daS9dgvmvbcN5dYp5VRoTwdiKEHblHIXQTk3PmEEUqIgUOyYUCToaTk87ktCw67btUCP23dkldEQWRAyEjCByahTzm9FrzM46f83XlXx2dLUnr+W+icikV4ajbcMyqzq0QcrjSfeduGXre5zxjk6FvnI2J5giRiEm5RKVts/bpWxGS1LHUqvERHUof12UZX3NRP3U/J4IgNF/5lV95GCdjrW71Tn8XxTmrM9aE5BStEJqj5oHNFBFUtnQ1kTeew3kwZtprg0SERffaKjUqYvbz4ZxQLjuWGrU5qU1V9c8WiRBuiwhhR+5VCEUtooSf/dmfPZzREShOV8TFaTvL4aw4as7tUiEsZ6ccaT4i6FyIwxcNts71lClXv7TiJHXopqJyLoXT5PiVPRwz7/Jv7bvUWzuN8Sk4cZGjZ/QtsfH1AFELsXjnd37nQ9uVX469hF4/uVwiahalfd3Xfd3hlqjxMSb+jRDq01NRaUHs3QIWDTr/JRxS0cafA3U5Rbq2zufMiVPCWmOlvvrOnHmv93qvwwUZX+1wzkuAtGUoYvVca9V2ESBxY86mjefwWVb9NYwIpfiNlQ1RRYU+589zl4DWIEK4LSKEHblnIRSBcJjaYHfOObo8Y7ftKwPSaJwMB8zJXSKEHBPnxqFLwSpTGla6jsNTXvv5Y6Zvvf/3fu/3DrcO3TSVPiQ6opBLuVQI1Vub1WGMEiZ10WfEhZN2E9b3JQk2x03k1N+mo5x19ZkxcT7rWWepvjROuIxPfQWDqJ5zfiV0+taZm74iVs4rtQlEUrRIoKVdbSpEZObAMcxLdWZE25hKi6qbyzcu+ZgnwzHVl54hpMonTqz+3p7j+bd6fjgu9W5CWLdGnXGKlGUb9Ks0q82EsfKni1jtLVJfS/Fv5vk1mYQeRAi3RYSwIxawRXrr5Pbn0kLoDMru3qLyfo6CyDk/4mw4U6JFOOz4pwqhherz7VkScSAuHLL2tp8/ZfqWg3SpgxOUFhVhcYDE8VIuFUJpTk5WHYYoi5PXfwSuFUJfGHfu57q+fvZv+t3Gw3sIq3pIuYoURcvSopy4domy/BuxkV71RftTqAsHr7+MkwjfGDNnlwSsRWRF5G18pBjV49gFkxI5ZRsHKd+3fuu3PvSNc03fTdWW4ZiaB97rlq95p1+Zz7fmZ77mIarW/55ry/FeRgjdJDYHnDe3t57Pma8K+UK/MVDmGujHCOF2iBB25N6FUNRiUfmZnb2IxG7buZaU5k/8xE8cHJzPXCOEnKavP1wrhMryWdGlqEjUygG7PCNivZQpQkjAzgmhiEYbPWPcWiFkBIJzZ6IvZ3LSo/Ur7TxLRAmLDYiNR0V+6uAM1CUUqVERtSjxGMZOeYzwEFFRoHNZzxJabW4hyCJOUSHBVb7nx2iFUOQryiWEoknzwbwg7H5efVlzgKjrP2la75FS9acbo8x4vv/7v/9BuKXRpUu9qx2XMSEU6TLi5jfMML85p6x+60yZ33LzvM/7vIeLVvp3DSKE2yJC2JHeQnhsV96DMSH8qZ/6qcMCBSckJeffnRVypCIbO3p149QuFUJpqR5CKFpwM1F0pO6imDmE0Pu0qb6EPiaEytC+cvxjQihiJRoiamd+oj1l6T/9rA6eExFJi+pPv1/ULUjl+ipCCaE/jdMYFZWqtz9tGNRXeTY0RMPtVRuS+nI5EyUSSbeF1Y0Q6eMx1FPfqFcrhN7hosyYEPq7eeWmKlEWkT3/8z//QZD8ujVWvwbtZV/2ZQ+iLOUqMq3xactibWpUv0g526zV7751gccvYvCnuWYMynyG2WgcE/y50Y/G6VZfoS8ihLcTIexITyHkyIjVXBwTQgvUzzgzjlSE4kYhByVKcbOU8+YEpe4uEUK/LYbjIoQckWhLe9vPnzJ9S4S8W5pVWQRljtSof1dvUZozSHXWT4RF39TzPqONnvHnMSFUP++QHhX1iDCdk7rpqF3KdHFFpCi6qjNEz0wRwmFd1J8oGV8iKCUotervTFRa5r/9TGrRO0RnosSxjZh5qb7eQ4yMabu5Mabq0Y6pz3tOW0XvziuNm/6U2vZ3z9twqQuxtAk7J4R1WUYfa6df8q2/zE9zl5j70+cJ8dCGZ5NL4Z21YRnOu0tNP/M74TYihB2xsKY69WNWC2NNIeRUOWYmRcphiwg5Kr/l3yUXjsiV/HNCyDizSmdyss6jRDyuyV96WUZZor8SYRGML4qf+zL4GPpAefq8dUjeY5dtPDlTERJnq5+0uYSwnFk9d0oIiZLPEAObAH1AEKWj9YGNgk2H9rh9KYXJoauXM0H9dkwIWxFk6q+/67zRb/CROnTeKzUo+vJLzeuXUPvF1NKKztp8Rh1EW23kW2hztVXdjGn9PlQCRoyITDumPmtO6EsCq14ieqlhqduKSl140V++S6jcqUKoL/VxZQX0h/FTjy1GSxHC7REh7AjHOdWpH7NaGGsLIWdjoTpnEpH4OUdpB0+E3LgTtXBE54RQnyiHk/S/JSKqIh9fnNZeDqv9/DFTrs9KsUkh+u4aMRSV+PmllEPybPW7dvu3ihQIioiQg67UqH7Rfz5bdWP+/ZgQEkA/J3p1wUQkJV3Mqfu5iEr0bbPRRlbEUv+PCaGxKqeqbJ/339LOUrrKk/KUFvVdROdixsBGRB+WOQ9WtvNg4yOlaDzbiMnf1ana6u82M8RWutNcqHNP5jPVN/rXfxM2Y1gipUz/phx1Exlq6yVC6Jl7+kJ9zTv9UXW81rQ1Qng7EcKOPJIQVn04O6lI0YobnxylnbuvBHD2/u2cENZ/Sw96TlpQCszZnn/zTp8ZCksZh+HnIjGfEUF4r+jFOaFLPBxr67Sn4rl6bzl37S9EMZUaJYTOwYgjJz5Wz2NC6PzPezgtF0FEPX5W390jdr4rKG0oAhUNlsO06TA+0pdDIVTf1qGaf/7N5kXfiAalkKWSia3I0+dqnMpsVFzKUQdjLMqzUWlFRL+0wqTvRGHmkLrpI+fI3qOtxkvd6vM1jupYGQf94d/cWHbGpwxnhBHCaaatEcLbiRB2pKcQDh1yb6YIobqUY+HcODkRhnMlQlS3NkVlBOKYEJYD9HffUxMNOXPkbEVHfmUZp6rNJUrDZy12jpMoiF44a5GOyEXd1JldKobeq3zvHfa5skoIiZYUnC/Hq69+autZdT0nhOaI+toE+Blhk/qsSyfGQeqQowTBLSEcRoT6pJwpK5EgdlK4xlVK1Pf7jLV3n+ofYy3Kr69S6FuiX+ibVpjUkSCJagmSW6fGVIRem4XavLT9xNpx9aeNgN9uVEJ4yRnhvQmhfjTX2jpea9WH4TYihB15RCH0Z+3eOWwRg2iOoySIzg05cDcgjwkh82/aJK3pwgQBFUUQU2IosuA8fY44caKMs63LOZwlh8fxqYO6VOTgs+cc/Rie4zzVzfMtylKnOiP0XqLoPFI/ceZtG88JYf28Lvtw4i7GiJKlLfWJW51+XmOvfkMhdOGG81Pn1pGae9KjPi/9aaNgbEXrhEO/GsdT88pXJ5xRVrQtei1Ha860wmSsvM+YEnb9Y0ylSrVPetdGQr202/trbNXduGmrtKh0u1SxdLfz0EvPCM0p57DGzDh6T71za6hftStCuA0ihB3hYEzwoYO8xGphKOcaxz6VS4TQYuNAfXVCipTDcl7oJqLU2xQh5Lz8uzI5ar+6TQRB0KRaCYdUnDpxrvWM6EbqU8Tp7ExEShBEpASUo1Wu/uJgL+0vfew5/TFEWW1EeKsQ+ox6ap9UIPGTJtafFVnrg9Z5D4XQ1xxcMKnNSftu4qK+LroQM1GzzYKUp9Snn7OxthZS1SJB9XJpxrgqE+bFcKOn771bKtZlGc/ZHHlW2teZoQiYqGk3KyFTL2euzivNIfNJnUX8xtwGqG0jGxNC46LOLnbV3NNez/tTn6v7mOmLudbYMXoJoX7XP8oLtxEh7IiFxdFYqGMTd4rVwjDB7fS2IIQWXDkg6SdOxy6c4/KLr6cIYRkHwDlJ/xGCuo3qEo4IhFN0yUMalpi4QMKZO4cULYg4OHmpPxGA8jhn71T3S/tLPxwTBmURNULGwWvzLUKoP9XV2Eqv1mcInE2BlKD+bWmFUNv1kbSh9g6F0LtFWKJK0aD6GheiYex8/lwfKddXZJwrivpFZy45gcMdzm2fNwbEUr2c8xmnZzzjGYdNDuE2piI248q0281Zt2b9X07U1TwyF2yQfIfQLx9Q/vB9NQ+1qb5HWClZl37MG+33Hv3uTxG3lPbQ/LvIVVlLoe97CaF+UI71Gm4jQtgRE1Iqb7h4L7FaGBzmNRHOVDh/Nwvrt8dIUUq5cZRDISwj8kRMRCI685yr937fp68w2PlzjKx9rrVyZFKOUq1ElTMjxlKtIsz6FVhMio4AiBT8jk6XTESColPvIQBl56KdS7ER0SZfHyFW2lupy1NCqG6cMAfNpHPbXx5QFyU4YalRIkBoOXERcIs5ICVsfHxOBOSykPlRzlRZ+tSfUpvSp84GbRhEXd5HBKdsrvSfDY3zOr+VxZg4N1S+co6Nrb4X7YkMba4Ik+iduNWYlhlXbdGnFXn6jirREgkSVe0bW0c1f4iXs0iXi/zmmJor7Xva97VWv3HG10j0v/5dCn1vDGoTc6sQ6vcI4e1ECDvSQwjLOJxzu/db4PBEKdJXohI7f070lBCqEwdFxKSlpCc5XM7E98CmRISsHDfHSWhcoCEW9R1DTrKcmqhE1MAx28VzlMTC88P39Nwd63fl6SN1I8T6SRQxJSIkakSAiYYqIlRn5ao/kSLsxIZoiYT0b4vyjItzNxG4z9VXFEoIqy8IsF8orb/q3Mx7fc7npzpNn5WKJlKiOxsB533e00ahrXm/56RAfVYdnU2K5tVFKt24MuMqCyAl7CKPz9kUqb+NVrVn2L/Mz1gbESpT+cx8HLN6NzO3GAElpM5Bl8K8kvrWV9oTIdwGEcKO3JMQKpcQiQI5T45Luq7OzNRhbJFqG8cqLWlHLs0lYuCsPV+Oavjc0Dg5i1gbRQD1RWtnkNKPBEfZnCTRVU+p3BIb/dOWp66cdI/zEmV4h3eJ/tRLPYg9h20j4F1DR+2//bv2+C0xnmGiJOKpjm3/OI+rNut/zn1Yf07TBkNf6xM3NF1OUUb1QZXpHd4rHWpMOHh1US+fVdaU+WSTZIMiYnWGJ+VZqcphv5dpm/eYG4ygmSPqo30uHBlXVuPqd4Xa2NQGSqTk2WG/tuZnjOCaM35lnU2HvvGO1rzLn8ZNO8pczGHE3iZsGIXPSQlh9eOtQsjfRAhvJ0LYEQ5kzElfYzXJe6b6puK96nBskZYzOuWwpliVoa2nymx/duwz6nqJsz8GISLQ6jR8Ry8r0fCeU8LNwfmMz17qMNt+8ieBuXaTUPNan5yrR7333Ji2n2Xt58c+e41VXdV9K5ib1ZfD+l5i2qYMZa3hIx6NCGFHarcn7XGp4xoah8B5bVEIGdGpiM7ntflaJ2ZBe157laXM1rxHpOWdx96hrtVn1zr8VniqXH/3bvViFQke65uqx9gz9XN/es+psW3ron+0q/ri2EbLZ/VjvbfSb7VBuGUuefepdg/NZ723+qEdT+bfpozrMavylTEsu6zeuyWMQY3rWLummnEwvnPeI9gTEcKOmJDSOyboVIdxyjiJrQrh1kxdyzlyDpdCPLVbGXO0W5lVR47wXCTYsy5E9Jb0mf40p+fqm7ms6rolMbSejcewrpeathmTc5efwjQihJ3hcAiYiXqL06iJfosDu5Z7FMIy0ZPo5xIq+ro1XXXK9OVUEayIoUf/V39c6yw9R0jm7Ju5rPpva0JY/mFY30vM8/zDtdmP8NxECDvDcdjxmai3THbPim7s+JbmXoWw6ktIpkbSHEkJz7C8XqZe14jgLf3f9sUtmynPVgRzS33WsKrvloTQ+N+aMfIsWytj9IhECDtDCHsIiWcJ4aXRTQ/uXQgrZXSONhKco61VZiuCxyKznpHgpf1wDHWVFjUPlTlHH81pVd+tCGH1Zw8h9Kf5cmw+hcuIEHbGxOyRSjLZlaGspXd995waZRzNuUsEFeksFQmeisp6iiBTRp2V3uIol+ijOa36citCaB3b2NbGYljfS8yYWKcRwj5ECGegJvvYBJ5qrRNd+hzgnoXQ5kH9TwnPI6dD67364NYNFCGdK1pewqreWxHCGusefVqb5AhhHyKEM1Dpj7EJPNXKodmRL31OeK9CeE54OI25RbD67FxdMEckyEGee+8Uqm5V7vBd92BV760IoTGpCPvWPuVfbrkEFZ6bCOEM9J7whHVJ7lEISwCmRII+O1ZGD9Nna4gg6yWCnCvxmLOflrDq160IoQ2tCy7GfFjXqVbza40N8iMTIZyBXk7Os5zR0gv53oRwqvBol/6co13KZFPr0s6PW+pTz2qX9p3aCExF3Xtc8V/bqv5bEUIR3K2bC22aMsfCZUQIZ6Ccbg8h9KeyluSehJBj4RROCUCNR53b9m6X8lg5qHN1aUVwrLwpVu/0d+3qJYLOFZU114ZhSav6b0UI1aOt1zXm2Zpn+epEPyKEMyC1VLu/Wye9P5ee9PcihCWCp3bGrWMfK6OHtc7pXF18pp4ZlnOJ1fN1VtRDBCHdduv59las+mgLQljzsK3XNVZzTZtyPtiPCOFMcCg9rkkz5wFLpkHuQQiniOAwEuxt+qcc0xQR5Lxqc3RN37bPKce8cH7ca5Ok/nW2PXz3PVr11RaEsPq2rdc15lljv/S9gUcnQjgTnLBzlrHJfKktfUNs60JYIngqCpo7EtQ3rBXBY+NTIniNINd7/N27tMd8UJ7295oT1V+PIoKs+m1tITRG7U3yqte1ppxTm65wORHCmeCkOMgejoXz46ROOf6ebFkIp4ign2lDr4h8zJQ7RQTV5VoRLKt32VjZEHlfrygQylLunP21hlVb1hbC2gj12JSZByLLpXzBXogQzkTtsHtFJJzg3oWwRPDUbngoPL3boDzWiuAYhNHPfEa967kp5j3K1wbjrgxtkm7vFQEWvaOVLVm1ZQtC2CvlPGUjGC4nQjgTHIxddjmYW8yC5hRPCUBPtiiEU0WwIsGxMnqYPjknguD8CAwhU3fmuTGrn5eZMxznXOJXKFf5lcJ/JBFkWxFC89KY9uhfc1t7zK/QjwjhjHCU93hAvjUh1PZzu+BWBOeqt3IJ1zkRBJHhrAgNM3YcmDoOzb8zGyefVbb2eH4uEUSv+blVqzatKYS12bh1XnqWEVTlhb5ECGeEI+M0ayIPJ/dU8ywx4DSX2Al6T713WJelrUTwlPCUCPqsZ3rXu8prRfAagSpxVN/W/HvZEngPZ1rpui2M8xxW7VpTCI2395ubt/SzZ5kxM2dCXyKEM9NDVDzLYUlhLbEb3IoQarOFf4kIzmH6YYog3wMlgo+aDm2t2ramEPbacHiWmYNLbZj2RIRwZqS7ejlp6RXlzc0WhLAVnmMLnwhyckumQ+/ZCal7nVtW29Yc47mt2ramEOpv83NYt2vMPFyzLY9MhHBm2t33rWYhEKm5nfHaQjgl+qqUUzmZ3nWt8ioqvfd0lP6yiTIXb41O7sW2IITerb+HdbvGnA8udU9gb0QIZ4Yz59RvdT6eZUs45TWFcIoItpHgWBk9TNuN2bm63ANtf/VyyvdgNX/XEkKbD/Onrcs1VnPx3DFBuJ4I4cyI3ixEDv5WYfG8XaGdvUU2F2sJ4VQRVL8l06H3irlXZ1RzRc5btmrrGkJYfd8jG1Tz0byfc93vmQjhAlgQvb5HVA56zqhwDSG8JBL0Wc/0rl+VN6UuW4YT1lc2TObdsJ17sRKQNYSQYFlHNVdvMe2wkZEWnftYZK9ECBeAU7IrH5vk1xjnNqeT7nnBZ4q1wnNsobeR4FgZPawc5z2LIAfMYZpvS47hVq0EZGnM1153A5iylBnmIUK4AJxTr0jGs8ohVnPtDnvedDtnU0VQ/81VJ306FMF723mrszmh/jZK2lLtGmvzHkwf6AsZmaXxTvO1R/9bI0mLzkuEcAE41UqPmti3LI5y2HMenBPCJVJqrQgeYxgJ9nbsyqs+PSeCHJGfM/Xy32sJpvd6v3qYW+peG63Ys2wtIWw3vj3mqzYkLTovEcKF4LA4KxN764uD45j7mv0UMS8R7OVQxmwogsco56bfibL+UTd9tbSDUhfv9X71UB9tqPbM1Vf3ZDWuc24Yj2FsemwkaxzXaMPeiBAuBGfJkfYQl1rkHOEcTrhEu0ddh9Y6qFM79VYE67lhWbeY8qou50RQXSo1W88pQ91KEKUlbUyU0zNSVI73K1f56qG+3tv2TdUp9izTH/rH2Oi/JTFGVYdhvS4xz8+5zsNziBAuSK+UYzk+YjLHIrfo1HWOSKxE8JTwEJJWBOewcjJERR8eczQVCVZq9pgpy2e0zedLFJWtjEutxI/A6gvlnqtD7DlmfPXX0hG7sTNWtUbH6naJaYM5EOYlQrggHFuvSMvzc6ZH1VXUMfbuW4yTOOWcCEAbfY2Vcaspt0TwnCCfE0Fllflv5RJwzxgfps3exYjaMavP+Hw9qxzlKXfsfbHjph/n2Cgew5zumRY15rVewrxECBeEY7W76xFpeV45HOcci11dlV0O+BarRU1Yp6RDS3h6O3vlVV3O9VsryGNlHbN6R2v+3TvLjNvQ2p+fKic2zfSjubR0NGheGc+xOl1ixls55qByw7xECBeGEHCuvRyb3eccO8YS7UuFYMw4pSkieI3wXGL6XF04q1OR4BJ1ic1rxm6ubMkxzKke0SAzV6sNYX4ihAvDyUp31M7/Fptz18iBKFNdveda4dbOSu+cSoe2keAcViKoLhzWqbpEBO/Xap7a7CwZSXmXeWM9XrtWWqu5aj6G+YkQLgwH3CvSqgUn2ppr51ip3OG7z1krPKciwXIg1R89nEhrVZ66TIkE5xbk2Pxm/JaOpMzxOlPvMYe1wbpYMqLdMxHCFeCMCYQJ32PRlJOfY9GUOFyz0z2XDiWCS0WC50RwKMix+zNjbZ6aU0tGUtadd5pnY/W6xGqN1dqJEC5DhHAFKiq0cHoIoTKcTZxK+V2L8pRbu92pNjUd2iuVNGbKvUQEe53vxNYz83SOdXCKWh+91rI5az6al2EZIoQrYfH0PFifcyfMqViYlSI9teAt4qmR4JTyrrEqb4oI6q9EgvdtNd7G0AZzSWoT1XNDN9cFuHCcCOFKzBERWUBznY2oL1E5FcWWCJ7akVe7t5AOjQg+htV4m1dLR1EE69JsyTHTjjk3tOE4EcKVIBQWESfcQwiXcAZEZUwM690lgsco4alIsLepR9WlRPCUIEcE79/a8V5aPHpHg8owH08dKYR5iBCuiIVUl2ZutVqI59KSt0Jc2jqXIzr3Xk6qjQR7OI7WlMc4pUSCj281f9YSQZjv1oI69JrP1tHSUW2IEK6KXV/7VYoei6mEYM4dZYkhB+CdUy7G9Nw5j5lyyymeEsHaxUcE79va8V5DBM11G7ue2Q1lmZuJBpcnQrgyFnFFWL1Eog7b5xZDToidE8ElIsEpIliCHBG8X6v5YwzXOksz180z66zHfK4yzh0thPmIEK6MRVVfWu+1qIgCcZ1zUam36IodE8GKvnrumoc2bO+5ukQE78+Mca0NY02AjKUxXQPzzKZLXXqt2YoG12rT3okQbgALy27QwhpbKJdaiYOFdUwY5qaNvno4izGrdlYkeKytiQTv24wzqyjQWK8lGOaYudRLBMsSDa5LhHADWFx1VliLfmyxTLUq49wFlrkYCk9Ph8Gqfa0IHiOR4H1ZjW2ZfxMtmcvGcY1UaIv1pC5t/W4xZWif9b/WpjVECDdDRYW1OIYL5hqzwAjFks7Du9ozwTlM/5wTQU6lBFk/lOOKbcvGxtfYMuMmDUokjPPaQmFT1fuCjD5INLg+EcKNUCmXnlGUMpS31G6zFcEe9R8z5U6JBKsuEcFtmzFtRY8577UW/HIIY7yFczPrR316ze0qQ7u1NWeD6xIh3BAWPQfPMfRabMpaYsfZCk+9e1ifW6zKa0XwlLhXNMipxrZrxtK8MVY2bFsSvxbzSX17rk1/KnPutRnOEyHcEBw7Z1BRzHDxXGu165wrKpwjZTQ0/VEiyClNQXtj92dbQ51qXY7NzWus5vOc6zJMJ0K4MewO66ywp0k52W33XnQVeS2RDs3uOSyN9VIXZMbm5i1mTa5xmS38XyKEG8PCI1gWydjiudZKSKZGU1OoSLDnuWZrbXnqfuqL+yHMgfVSxxXt3LzVrG9RZs4Gt0GEcIPMkWokKj1TpBxEK4JzWEWCduMRwbA05tscRxXmtLXTc1MabiNCuFEqRWrR9FiEVUaPdEyJYG8H0VqVm0gwrEV7TNFzDdbGLmyHCOFG4fjnOnu75axtGAn2rluVl0gwrMkcKVFzW3nWdVKi2yJCuGHmujgjkrvk9mWx5O3QRIJhLeac57kgs00ihBuGCNSXeC2iXtGXcizyS3amw0iwt6lTRDCsjTnnXLBnJqbKUWYuyGyTCOHGsTAJEIHovTDtTgntORIJhr1g7vU8m2c1t2VhIoLbJEJ4B1ic9XWKXouzrM4LjwlPGwn2fndZOYqcCYY1qXPBsTl6rdWambrpDOsQIbwD7CJ7p2uYskR5hG7svNB72ws7Pd/NqsyKBOf4wn8IU6i5bj30nOfKuvQYIixPhPBOqN0q0RhbcLcYobNQWzFsxddneosga0UwkWBYixLByrr0tsq6hO0SIbwj6vzC4uotTCWGnMJQBOewEsGkQ8OamHc113tuMmt91i3RzO9tEyG8IyymNn3TWwwtWk6h3R33fkdb70SCYU3Muzl+nWHN8UqJZn5vnwjhnSHFUinS3iKlTDtjC3js5z2s6pxIMKxNfU+3ZyTIzHFlJiV6P0QI7wzCUQt4bBH2st4iy8pBRATD2lhDhKq3CJYlJXpfRAjvEItr7ossPa3qV7vkOIiwFrWRnOPimXnO2vP2cB9ECO+UuulmMd+DELYiGMJalAj2/poEq/KOfR0pbJcI4R3TfqViq2I4FMFEgmEtrJf6DUnmZc81087znAveHxHCO2eJ88JrrJwM55AzwbA2rQi287OnZZ7fLxHCB2CL54XtDjnOIazJMBIcm6/XWpVX54KZ5/dJhPAB2Np5YYlgdshhbUoE59oo1lzPueB9EyF8ECxC0ZdFObZgl7REgmELDEVwDqu5nnPB+yZC+EAQnjXFMJFg2ApzpkPLMtcfhwjhA2ExlhhaqHM5gKF5T70rkWBYG9FZiWDNz3a+3mpVXkTwcYgQPhglhnOmg8Ys6dCwNuZd+z3BsXnawwih8vO/DXscIoQPSF2eWeImqbIjgmFtzDvCtMTRABEUceY3xzwOEcIHpcRwrvRQWQmhXXguDIQ1MNd9hUiq0lycY65XmSWCuSH6WEQIH5glLgyU1feo4iDCkphvbfZjLsuG77GJED44HMUS6SLlc0bZLYelGIrgnJs989s6IoJJ/z8eEcId4OyuFcM5HQanZNfsnTlDCXNAiMwvm645I8FaJyWCOQN/XCKEO6EVw7nTpN7hvMa5TcQw9KQuxcx5Hlim7FYEw+MSIdwJHIi0TiuGczmRciCVKs2ZSuhBpUL9T2/nnsPMOwhu0qGPT4RwZ9jZLvW/blK+izp1wSDOJFxDbeKWuPhVZVsfiQT3Q4RwZ7SRYX21Ym6rnbWUVlKl4RLMV2Jk/iwxXwlhzVfvzeZtH0QId0g5lzYynHuX7T1SWnb1cTDhHLVhM18qFTo2t3pZrYGI4D6JEO6YigxLDMccRG8rR1MXaeJsQov5YF4s9VtiykoEkw7dJxHCHcPpWPRLOhzmXfU1i/y+xlDUfBxGgUts0loRzHzcHxHCnWPRiwyJUp3BzO14qnzvEx26CagOcUD7pOagedCeBS45D83/iOB+iRCGA66m24mL1DiIuZ1QaxyR3bjoUD3ijPaD8Tbu7UZsCas57p3mPSEO+yVCGJ7Nkr+yqqx9R6VLszN/fCoKrM3XkmlQ1opgNl8hQhieCxcV2t/kP+ZE5jCOiXlnXaZJuvTxKAE0vrIAxKjGfmxe9LaaY8TXpo8IhhAhDP8Hzqou0SyZrmrNeytdmtul94/xq9ugS6dBW6uNVi5phZYIYRilFUPOY6kdO6t31c5dHSpCDPeHqKsiwDXSoKwiwYhgGCNCGI5SYljnOOVQhk5mTqv3eb9IghNLynT7GJ/2Ikw7f5YWQH/W/Mn5cxgjQhjOUueGvts1dDRLmh19mzLNJYftYTzaL8S3EeBaZt7mPDCcIkIYJtE6N45t6ciQVTTh/RysunBwdvnqF9aDANYlmIoASwDXmiv+rFRo5kc4RYQwTKacXXvZYQ0nx+q96sHZlSCqH6eXSHFeKvLT3/p9eAa4hXmhPkmFhilECMPFcICEZ+1UaRnHW1FiK4pxgPOgX/WvftbfJX4VAa5t6qNuSZ2HqUQIw1UQQ87QrnsL0WFZK4gu+YhU1DOpsduo8ZZm1K+tALb9PzY+c1s79uZjUqHhUiKE4SakxjjGNjpcyyEOrRXFihJFCYkUzqN/iIm+quivTX2O9feapl5S9uZjCJcSIQw3w2HWRZqKDte2Vow5bvUqUeQwRYqcZgTxudEfxE//tJGf/msFcCubnToLNP/UO4RriBCGLnCgw+hwK85SPcr8N4eujpU+Fe1wpBUx7kUcS/Qq5akf9Adh0T9tyrvtv7Wt6kGg1TcbmnArEcLQlYoOicwwitiqqSOnSgA4VvXnXImE9jCO9l6dbdW92qJd2qedJXzafw9jxcwrQi1q1Z4QbiVCGLrD6XK0Ioz27HBrNox0CAEnSxSYuhMJ7WAiJ+26F+ernupbZ3xMWli7qo3tZmXYH1s0ddeOexqHsH0ihGE2OCpRR/sF6y072lYIylpxFOVW1MgZi0i0j9CUSIq22kiS9Ygmh1Edq3eV2DH1US/1q2hveM431s6x/tiCqZs6q795pH3aHkJPIoRhdjhrzplTbiOQe7cSSVFKiSRnTYC0t4zzLsFsxbJsTNzKSuSqjLZc7/G+EruK9O69f1th1r/aR9j1RwhzECEMi1BOngPntCsyaR3g1q2ip9bqZ9pTwsgI0tAI1Zjpj7Kxn7Ox8upd9e4p9bwHq/pqt/li3iQKDHMSIQyLwqGJbOqsiiMv53dvDnto1YY1bKw+92TVhtpEiALNkwhgWIIIYVgFDk7Krz0/HDrH2H7M+JsH5oN5kTRoWJIIYVgNlz84PDt/511tyvQRopzYcWvHuATQPDAfbr1YFMKlRAjDJnjklGnsWdaOpfE1zlKgdQ4YwlpECMNmqK8HSI2JEJMyfTyrFKjxNc7GOxFgWJsIYdgclTLlKF2bFzW0opgI8T6sIsASv/oaRJ0BRgDDVogQhk3DWfouHQfapk1bUYwwbsNqLEr82hugxs84RvzCFokQhruAA61zRGm19hwxti0jgsbHOOUCTLgHIoThruBQK23qkoUo0W3T4eUaNnTQsX427Oe6/GI86vwvAhjuhQhhuGtKFDlfgigVN5Y6LYcdu86G/dimPvV7vv4Q7pkIYbh7Km3aRop1wWbo0GO3W5376ecSv9z+DPdMhDA8HJyyixkctUiR065ocSxSrChn7zbsE1aRX33nT3/qV/2rn0N4BCKE4aFpzxSHN08rhVriOBSF4b89mg3bWH1RfWPjUGd+SXuGRyZCGHZBmz6t/7VRGzGWOJYoVkTUCsWjWLWNlfBVulN/1Hf99FPSnmEPRAjDbmmjRREPAWhTqcSRQAwjx1ZIjtlQfOawsfcOrT7bCp52talO7bYp0A+J+sIeiRCG0FCRo2iIOBIIQlHiOBTG1lqROmVjgnXMxp4/ZsP6tMLXip52Eb1EeiE8iwhhCEcoURymVFkJJHOORmgq0mLHBJNgjQneMTsncGX1FYaqU0V4rFKcbZozAhjCc4gQhnAlJZRterWshFIU1hrBvNSGZZTYte/z/lboQgjTiRCG0ImKtNayEMJ1RAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyaCGEIIYRdEyEMIYSwayKEIYQQdk2EMIQQwq6JEIYQQtg1EcIQQgi7JkIYQghh10QIQwgh7JoIYQghhF0TIQwhhLBrIoQhhBB2TYQwhBDCrokQhhBC2DURwhBCCLsmQhhCCGHXRAhDCCHsmghhCCGEXRMhDCGEsGsihCGEEHZNhDCEEMKuiRCGEELYNRHCEEIIuyZCGEIIYddECEMIIeyYp6f/Hyux08DKLA0VAAAAAElFTkSuQmCC"
								alt="click here to upload image">
							<input type="file" id="upload" style="display: none;" name="file" /><br />
							<input type="hidden" id="imgContentType" style="display: none;" name="imageType" />
							<span class="" style="position:absolute;font-size:12px;left:4rem">
								<!-- Click image to upload Product Img --></span>
							<br>
						</div>

						<%-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
               			 <i class="material-icons prefix">location_on<span class="red-text">*</span></i>
               			<select name="areaId" id="areaId" class="validate" required="" aria-required="true" title="Please select Area">
                                 <option value="" selected>Select Area</option>
                                <c:if test="${not empty areaList}">
							<c:forEach var="listValue" items="${areaList}">
								<option value="<c:out value="${listValue.areaId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
							</c:if>
                        </select>
              		 </div> --%>


						<div class="input-field col s12 m5 l5 push-l1 push-m1" style="margin-top:3%">

							<i class="material-icons prefix">star</i>
							<!--   <label for="brandid" class="active"></label> -->
							<span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Brand</span>
							<select id="brandid" name="brandId" required="" aria-required="true">
								<option value="" selected>Choose Brand</option>
								<c:if test="${not empty brandlist}">
									<c:forEach var="listValue" items="${brandlist}">
										<option value="<c:out value="${listValue.brandId}" />">
										<c:out value="${listValue.name}" />
										</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div class="input-field col s12 m5 l5 push-l1 push-m1">
							<i class="material-icons prefix">filter_list</i>
							<!-- <label for="categoryid" class="active"></label> -->
							<span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Category</span>
							<select id="categoryid" name="categoryId" required="" aria-required="true">
								<option value="" selected>Choose Category</option>
								<c:if test="${not empty categorylist}">
									<c:forEach var="listValue" items="${categorylist}">
										<option value="<c:out value="${listValue.categoryId}" />">
										<c:out value="${listValue.categoryName}" />
										</option>
									</c:forEach>
								</c:if>
							</select>
						</div>


					</div>

					<!--  <div class="input-field file-field  col s12 m4 l4 left-align ">

                        <div class="file-path-wrapper">
                            <i class="material-icons prefix" style="margin-left:-3%;">image</i>
                            <input id="upload" type="file" name="file">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>

                    </div> -->
					<div class="row">
						<div class="input-field col s12 m4 l4 right-align removeRight" id="unitprice">
							<i class="fa fa-inr prefix"></i>
							<input id="unitRate" type="text" class="grey lighten-2" name="productRate" readonly>
							<label for="productRate" class="active">Product Unit Price</label>
						</div>
						<div class="input-field col s12 m5 l5 push-l1 push-m1">
							<i class="material-icons prefix">shopping_cart</i>
							<input id="product" type="text" class="validate" name="productname" required>
							<label for="product" class="active"><span class="red-text">*</span>Enter Product
								Name</label>
						</div>
						<div class="input-field col s12 m4 l4 right-align removeRight" id="cgstDiv">
							<i class="fa fa-inr prefix"></i>
							<input id="cgst" type="text" class="grey lighten-2" readonly>
							<label for="cgst" class="active">CGST Amount</label>
						</div>

						<div class="input-field col s12 m5 l5 push-l1 push-m1" id="productrate">
							<i class="fa fa-inr prefix"></i>
							<input id="productRate" type="text" class="validate" required>
							<label for="productRate" class="active"><span class="red-text">*</span>Enter MRP</label>
						</div>
						<div class="input-field col s12 m4 l4 right-align removeRight" id="sgstDiv">
							<i class="fa fa-inr prefix"></i>
							<input id="sgst" type="text" class="grey lighten-2" readonly>
							<label for="sgst" class="active">SGST Amount</label>
						</div>
						<div class="input-field col s12 m5 l5 push-l1 push-m1" id="threshold">
							<i class="material-icons prefix">view_comfy</i>
							<input id="thresholdValue" type="text" class="validate" name="thresholvalue" required>
							<label for="thresholdValue" class="active"><span class="red-text">*</span>Enter Threshold
								Value</label>
						</div>

						<div class="input-field col s12 m4 l4 right-align removeRight" id="igstDiv">
							<i class="fa fa-inr prefix"></i>
							<input id="igst" type="text" class="grey lighten-2" readonly>
							<label for="igst" class="active">IGST Amount</label>
						</div>
						<div class="input-field col s12 m5 l5 push-l1 push-m1">
							<i class="material-icons prefix"> rate_review</i>
							<input id="code" type="text" class="validate" name="productcode">
							<label for="code" class="active">Enter Product Code</label>
						</div>

						<div class="input-field col s12 m5 l5 push-m5 push-l5">

							<i class="fa fa-barcode prefix"></i>
							<input id="productBarcode" type="text" class="validate" name="productBarcode"
								maxlength="15">
							<label for="productBarcode" class="active"> Enter Product Barcode Number</label>
						</div>








						<!--  <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productTax" type="text" name="productTax" readonly>
                        <label for="productTax" class="active">Product Tax</label>
                    </div>
                    <br>
                    <div class="input-field col s12 m5 l5 push-l5 push-m5" id="productrate">
                        <input id="productRateWithTax" type="text" name="productRateWithTax" readonly>
                        <label for="productRateWithTax" class="active">Product Rate With Tax</label>
                    </div> -->
					</div>
					<br>

					<div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
						<button class="btn waves-effect waves-light blue-gradient" type="submit"
							id="saveProductSubmit">Add Product<i class="material-icons right">send</i> </button>
						<br><br>
					</div>

				</div>


			</form>
		</div>


		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
						<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
							<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</main>
	<!--content end-->
</body>

</html>