<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
			<html>

			<head>
				<%@include file="components/header_imports.jsp" %>
					<script src="resources/js/moment.min.js"></script>
					<script type="text/javascript">

						var showEditButton = ${ sessionScope.loginType!= 'Admin'};
						/* 
						comming message show on modal
						*/
						var msg = "${saveMsg}";
						//alert(msg);
						if (msg != '' && msg != undefined) {
							$('#addeditmsg').find("#modalType").addClass("success");
							$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
							$('#addeditmsg').modal('open');
							//$('#msgHead').text("Brand Message");
							$('#msg').text(msg);
						}						// show inventory transaction details by transaction id and pay status
						function showTransactionDetails(id_and_paystatus) {
							var idAndPayStatus = id_and_paystatus.split("_");
							$.ajax({
								url: "${pageContext.servletContext.contextPath}/fetchTrasactionDetailsByInventoryId?inventoryId=" + idAndPayStatus[0],
								dataType: "json",
								success: function (data) {
									//alert(data);
									var totalAll = 0, totalAmtWithTaxAll = 0, totalQty = 0;
									var totalTaxableAmount=0;
									$("#productOrderedDATA").empty();
									var srno = 1;
									//set inventory product details list on modal 
									for (var i = 0, len = data.inventoryDetailsModelList.length; i < len; ++i) {
										var inventoryDetailList = data.inventoryDetailsModelList[i];

										var totalAmtWithTax = inventoryDetailList.rate.toFixedVSS(2) * inventoryDetailList.quantity;

										var unitPrice=(inventoryDetailList.rate.toFixedVSS(2)*100)/(100+inventoryDetailList.product.categories.igst)
										var taxableAmount=unitPrice.toFixedVSS(2)*inventoryDetailList.quantity;
										
										var correctAmoutWithTaxObj = calculateProperTax(inventoryDetailList.rate.toFixedVSS(2), inventoryDetailList.product.categories.igst);
										// $("#productOrderedDATA").append("<tr>" +
										// 	"<td class='hideColumnOnSmall'>" + srno + "</td>" +
										// 	"<td class='hideColumnOnSmall'>" + inventoryDetailList.product.categories.hsnCode + "</td>" +
										// 	"<td>" + inventoryDetailList.product.productName + "</td>" +
										// 	"<td class='hideColumnOnSmall'>" + inventoryDetailList.product.categories.categoryName + "</td>" +
										// 	"<td class='hideColumnOnSmall'>" + inventoryDetailList.product.brand.name + "</td>" +
										// 	"<td class='hideColumnOnSmall'>" + correctAmoutWithTaxObj.unitPrice + "</td>" +
										// 	"<td class='hideColumnOnSmall'>" + inventoryDetailList.product.categories.igst + " %</td>" +
										// 	"<td>" + inventoryDetailList.rate.toFixedVSS(2) + "</td>" +
										// 	"<td>" + inventoryDetailList.quantity + "</td>" +
										// 	"<td>" + totalAmtWithTax.toFixedVSS(2) + "</td>" +
										// 	"</tr>");

										$("#productOrderedDATA").append("<tr>" +
											"<td class='hideColumnOnSmall'>" + srno + "</td>" +
											"<td>" + inventoryDetailList.product.productName + "</td>" +
											"<td>" + unitPrice.toFixedVSS(2) + "</td>" +
											"<td>" + inventoryDetailList.rate.toFixedVSS(2) + "</td>" +
											"<td>" + inventoryDetailList.quantity + "</td>" +
											(	
													(inventoryDetailList.discountOnMRP=="YES")?
															("<td>" + taxableAmount.toFixedVSS(2)+ "</td>") 
															:
															("<td class='black-text'> <b>" + taxableAmount.toFixedVSS(2)+ "</b></td>")		
											)+
											(
														(inventoryDetailList.discountOnMRP=="YES")?
																("<td class='black-text'> <b>" + inventoryDetailList.amountBeforeDiscount.toFixedVSS(2)+ "</b></td>") 
																:
																("<td>" + inventoryDetailList.amountBeforeDiscount.toFixedVSS(2)+ "</td>")		
											)+
											/*
											("<td>" + taxableAmount.toFixedVSS(2)+ "</td>")+
											"<td>" + inventoryDetailList.amountBeforeDiscount.toFixedVSS(2) + "</td>" + */
											(
													(inventoryDetailList.discountType=="PERCENTAGE")?
													("<td><b>" + inventoryDetailList.discountPercentage.toFixedVSS(2) + "</b></td>"+"<td>"+inventoryDetailList.discountAmount.toFixedVSS(2)+"</td>")
													:
													("<td>" + inventoryDetailList.discountPercentage.toFixedVSS(2) + "</td>"+"<td><b>"+inventoryDetailList.discountAmount.toFixedVSS(2)+"</b></td>")
											) +
											"<td>" + inventoryDetailList.amount.toFixedVSS(2) + "</td>" +
											"</tr>");

										srno++;
										totalQty += inventoryDetailList.quantity;
										totalAmtWithTaxAll = parseFloat(totalAmtWithTaxAll) + totalAmtWithTax;
										totalTaxableAmount=parseFloat(totalTaxableAmount)+parseFloat(taxableAmount);
									}
									//last total row set
									// var colSpan;
									// if ($.data.isMobile){
									// 	colSpan="2";
									// }
									// else{
									// 	colSpan="8";
									// }
									$("#productOrderedDATA").append("<tr>" +
										"<td colspan=5><b>Total Amount</b></td>" +
										"<td><b>" + totalTaxableAmount.toFixedVSS(2) + "</b></td>" +
										"<td><b>" + data.totalBeforeAllDiscount.toFixedVSS(2) + "</b></td>" +
										"<td></td>" +
										"<td><b>" + data.totalProductsAllDiscount.toFixedVSS(2) + "</b></td>" +
										"<td><b>" + data.inventoryReportView.totalAmountTaxBeforeDiscount.toFixedVSS(2) + "</b></td>" +
										"</tr>");
									if(data.inventoryReportView.isDiscountGiven){										
										$("#productOrderedDATA").append("<tr>" +
											"<td colspan=7><b>Overall Discount</b></td>" +
											/* ((data.inventoryReportView.discountType=="PERCENTAGE")?("<td>" + data.inventoryReportView.discountPercentage.toFixedVSS(2) + "</td>"+"<td>-</td>"):("<td>-</td>"+"<td>"+data.inventoryReportView.discountAmount.toFixedVSS(2)+"</td>")) + */
											(
												(data.inventoryReportView.discountType=="PERCENTAGE")?
												(
													"<td><b>"+ data.inventoryReportView.discountPercentage.toFixedVSS(2) + "</b></td>"+
													"<td>"+data.inventoryReportView.discountAmount.toFixedVSS(2)+"</td>"
												):
												(
													"<td>"+ data.inventoryReportView.discountPercentage.toFixedVSS(2) + "</td>"+
													"<td><b>"+data.inventoryReportView.discountAmount.toFixedVSS(2)+"</b></td>"
												)
											) +
											/* "<td  class='red-text'><b>" + data.inventoryReportView.discountAmount.toFixedVSS(2) + "</b></td>" + */
											"<td  class='red-text'><b> ( - )</b></td>" +
											"</tr>");
									}else{
										$("#productOrderedDATA").append("<tr>" +
												"<td colspan=7><b>Overall Discount</b></td>" +
												"<td>-</td>"+
												"<td>-</td>"+
												"<td><b>0</b></td>" +
												"</tr>");	
									}
									$("#productOrderedDATA").append("<tr>" +
										"<td colspan=7><b>Net Total Amount</b></td>" +
										"<td></td>" +
										"<td></td>" +
										"<td  class='green-text'><b>" + data.inventoryReportView.totalAmountTax.toFixedVSS(2) + "</b></td>" +
										"</tr>");

									//inventory added date
									var dateAdded = data.inventoryReportView.addedDate;
									$('#supplierNameId').html(data.inventoryReportView.supplier.name);
									$('#mobNoId').html(data.inventoryReportView.supplier.contact.mobileNumber);

									var today = new Date();

									/* less three days from inventory date */
									var dateBeforeThreeDay = new Date();
									dateBeforeThreeDay.setDate(today.getDate() - 3);


									/* var isGreaterorEq = date1.getTime() >= date2.getTime();
									var lessThan = date2.getTime() < date1.getTime();
									console.log(isGreaterorEq ,lessThan );  */

									/* 
										showEditButton is true when company and gatekeeper login and false for admin login
									*/
									/* 
									IF ORDER BETWEEN LAST 3 DAYS
									IF ORDER PAY STATUS IS UNPAID
									IF ADMIN LOGIN
									THEN SHOW EDIT / DELETE BUTTON
									*/
									if (dateAdded <= today.getTime()
										&& dateAdded >= dateBeforeThreeDay.getTime()
										&& idAndPayStatus[1].trim() === "UnPaid"
										&& showEditButton == true) {
										$('#editButton').html('<a href="${pageContext.servletContext.contextPath}/openEditMultipleInventory?inventoryId=' + data.inventoryReportView.transactionId + '" class="modal-action modal-close waves-effect btn">Edit</a>');
										$('#deleteButton').html('<a onclick="deleteConfirmationForInventory(\'' + data.inventoryReportView.transactionId + '\')" class="waves-effect btn">Delete</a>');
									}
									else {
										$('#editButton').html('');
										$('#deleteButton').html('');
									}

									$('.modal').modal();
									$('#product').modal('open');

								},
								error: function (xhr, status, error) {
									alert("error");
								}
							});
						}
						/* 
						DELETE INVENTORY CONFIRMATION 
						*/
						function deleteConfirmationForInventory(id) {
							/* $('.modal').modal();
							$('#delete'+id).modal('open'); */
							Materialize.Toast.removeAll();
							var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><a class="btn white-text toast-action" href="${pageContext.servletContext.contextPath}/deleteInventory?inventoryId=' + id + '">Delete</a>'));
							Materialize.toast($toastContent, "abc");
						}

						$(document).ready(function () {
							var table = $('#tblData').DataTable();
							table.destroy();
							$('#tblData').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": "_INPUT_" //search
								},
								autoWidth: false,
								columnDefs: [
									{ 'width': '1%', 'targets': 0 },
									{ 'width': '3%', 'targets': 1 },
									{ 'width': '3%', 'targets': 2 },
									{ 'width': '1%', 'targets': 3 },
									{ 'width': '3%', 'targets': 4 },
									{ 'width': '5%', 'targets': 5 },
									{ 'width': '3%', 'targets': 6 },
									{ 'width': '5%', 'targets': 7 },
									{ 'width': '5%', 'targets': 8 },
									{ 'width': '5%', 'targets': 9 },
									{ 'width': '3%', 'targets': 10, "orderable": false },
									{ 'width': '5%', 'targets': 11 },
									{ 'width': '5%', 'targets': 12 },
									{ 'width': '5%', 'targets': 13 },
									{ 'width': '5%', 'targets': 14 },
									{ 'width': '1%', 'targets': 15 },
									{ 'width': '1%', 'targets': 16 },
									{ 'width': '1%', 'targets': 17 }

								],
								lengthMenu: [
									[10, 25., 50, -1],
									['10 ', '25 ', '50 ', 'All']
								],


								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = [30, 80, '*', 40, 40, 40, 40, 40, 50, 50, 50, 50, 50, 50, 50, 50]
													}
												})
												var tableNode;
												for (i = 0; i < doc.content.length; ++i) {
													if (doc.content[i].table !== undefined) {
														tableNode = doc.content[i];
														break;
													}
												}

												var rowIndex = 0;
												var tableColumnCount = tableNode.table.body[rowIndex].length;

												if (tableColumnCount > 6) {
													doc.pageOrientation = 'landscape';
												}
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];
												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										},
										{
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										},
										{
											extend: 'print',
											className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										},
										{
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}

							});
							$("select").change(function () {
								var t = this;
								var content = $(this).siblings('ul').detach();
								setTimeout(function () {
									$(t).parent().append(content);
									$("select").material_select();
								}, 200);
							});
							$('select').material_select();
							$('.dataTables_filter input').attr("placeholder", "Search");
							//if there is mobile device the element contain this class will be hidden
							if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
							}

							/*   $("#BankDetails").css("display", "none");
							$(".cash").change(function() {
								$("#BankDetails").css("display", "none");
							});
							$(".cheque").change(function() {
								$("#BankDetails").css("display", "block");
							}); */
							$(".showQuantity").hide();
							$(".showDates").hide();
							$("#oneDateDiv").hide();
							$(".topProduct").click(function () {
								$(".showQuantity").show();
								$(".showDates").hide();
								$("#oneDateDiv").hide();
							});

							$(".rangeSelect").click(function () {
								$("#oneDateDiv").hide();
								$(".showDates").show();
								$(".showQuantity").hide();
							});

							$(".pickdate").click(function () {
								$(".showQuantity").hide();
								$(".showDates").hide();
								$("#oneDateDiv").show();
							});

							//hide column depend on login
							var table = $('#tblData').DataTable(); // note the capital D to get the API instance
							var column = table.columns('.toggle');
							console.log(column);
						if (${ sessionScope.loginType != 'Admin' }){
							column.visible(true);
						}else {
							column.visible(false);
						}
						var columntoggle = table.columns('.toggle1');
						columntoggle.visible(false);
						$('#showColumn').on('click', function () {

							//console.log(column);

							columntoggle.visible(!columntoggle.visible()[0]);

						});
   });
						/* 
						 PAYMENT DETAILS OF TRANSACTION BY TRANSACTION ID
						*/
						function fetchPaymentDetails(invtId) {
							$.ajax({
								type: 'GET',
								url: "${pageContext.servletContext.contextPath}/fetchPaymentSupplier?inventoryId=" + invtId,
								async: false,
								success: function (paymentPaySupplierListResponse) {
									$('#paymentDetail').empty();

									var totalAmountPaid = 0;
									var srno = 1;
									for (var i = 0; i < paymentPaySupplierListResponse.paymentPaySupplierModuleList.length; i++) {
										paymentPaySupplier = paymentPaySupplierListResponse.paymentPaySupplierModuleList[i];

										var mode = paymentPaySupplier.payType;

										// if (paymentPaySupplier.payType === "Cash") {
										// 	mode = "Cash";
										// } else if (paymentPaySupplier.payType === "Other") {
										// 	mode = paymentPaySupplier.paymentMethod.paymentMethodName + "-" + paymentPaySupplier.transactionReferenceNumber;
										// } else {
										// 	mode = paymentPaySupplier.bankName + "-" + paymentPaySupplier.chequeNumber;
										// }

										var dateFrom = new Date(parseInt(paymentPaySupplier.paidDate));
										month = dateFrom.getMonth() + 1;
										day = dateFrom.getDate();
										year = dateFrom.getFullYear();
										var dateFromString = day + "-" + month + "-" + year;


										var editDeleteButton = "<a href='${pageContext.servletContext.contextPath}/editPaymentSupplier?paymentPayId=" + paymentPaySupplier.paymentPayId + "' class='btn-flat' ><i class='material-icons'>edit</i></a>" +
											"<a href='#' class='btn-flat' onclick='deleteConfirmToast(\"" + paymentPaySupplierListResponse.inventoryTransactionId + "\"," + paymentPaySupplier.paymentPayId + ")'><i class='material-icons'>delete</i></a>";

										if (showEditButton == false) {
											editDeleteButton = "NA";
										}

										//deletePaymentSupplier
										$('#paymentDetail').append('<tr>' +
											'<td>' + srno + '</td>' +
											'<td>' + paymentPaySupplier.paidAmount.toFixedVSS(2) + '</td>' +
											'<td>' + mode + '</td>' +
											'<td>' + dateFromString + '</td>' +
											'<td>' + editDeleteButton + '</td></tr>');

										totalAmountPaid = parseFloat(totalAmountPaid) + parseFloat(paymentPaySupplier.paidAmount.toFixedVSS(2));

										srno++;
									}

									$('#inventoryId').html("<b>Inventory Id : </b>" + paymentPaySupplierListResponse.inventoryTransactionId);
									$('#supplierDetailsId').html("<b>Supplier : </b>" + paymentPaySupplierListResponse.supplierName);
									$('#totalAmt').html("<b>Total Amount : </b>" + parseFloat(paymentPaySupplierListResponse.totalAmountTax).toFixedVSS(2));

									var totalAmountBal = (parseFloat(paymentPaySupplierListResponse.totalAmountTax) - parseFloat(totalAmountPaid)).toFixedVSS(2);
									$('#balAmt').html("<b>Balance Amount : </b>" + parseFloat(totalAmountBal).toFixedVSS(2));

									$('#totalAmountPaid').text(parseFloat(totalAmountPaid).toFixedVSS(2));
									$('#viewPaymentDetail').modal('open');
								},
								error: function (xhr, status, error) {
									Materialize.Toast.removeAll();
									Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
								}

							});
						}
						/* DELETE PAYEMENT CONFIRMATION */
						function deleteConfirmToast(invtId, paymentPayId) {
							Materialize.Toast.removeAll();
							var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deletePaymentSupplier(\'' + invtId + '\',' + paymentPayId + ')">Delete</button>  '));
							Materialize.toast($toastContent, "abc");
						}
						/* DELETE PAYMENT SUPPLIER REQUEST
						AND RESET PAYAMENT DETAILS
						*/
						function deletePaymentSupplier(invtId, paymentPayId) {

							$.ajax({
								type: 'GET',
								url: "${pageContext.servletContext.contextPath}/deletePaymentSupplier?inventoryId=" + invtId + "&paymentPayId=" + paymentPayId,
								async: false,
								success: function (paymentPaySupplierListResponse) {
									Materialize.Toast.removeAll();
									if (paymentPaySupplierListResponse == "" || paymentPaySupplierListResponse.paymentPaySupplierModuleList.length == 0) { 

										$('#viewPaymentDetail').modal('close');
										refreshInventoryReport();
										return false;
									}
									$('#paymentDetail').empty();

									var totalAmountPaid = 0;
									var srno = 1;
									for (var i = 0; i < paymentPaySupplierListResponse.paymentPaySupplierModuleList.length; i++) {
										paymentPaySupplier = paymentPaySupplierListResponse.paymentPaySupplierModuleList[i];
										var mode = "";
										if (paymentPaySupplier.payType === "Cash") {
											mode = "Cash";
										} else {
											mode = paymentPaySupplier.bankName + "-" + paymentPaySupplier.chequeNumber;
										}

										var dateFrom = new Date(parseInt(paymentPaySupplier.paidDate));
										month = dateFrom.getMonth() + 1;
										day = dateFrom.getDate();
										year = dateFrom.getFullYear();
										var dateFromString = day + "-" + month + "-" + year;


										var editDeleteButton = "<a href='${pageContext.servletContext.contextPath}/editPaymentSupplier?paymentPayId=" + paymentPaySupplier.paymentPayId + "' class='btn-flat' ><i class='material-icons'>edit</i></a>" +
											"<a href='#' class='btn-flat' onclick='deleteConfirmToast(\"" + paymentPaySupplierListResponse.inventoryTransactionId + "\"," + paymentPaySupplier.paymentPayId + ")'><i class='material-icons'>delete</i></a>";

										if (showEditButton == false) {
											editDeleteButton = "NA";
										}

										//deletePaymentSupplier
										$('#paymentDetail').append('<tr>' +
											'<td>' + srno + '</td>' +
											'<td>' + paymentPaySupplier.paidAmount.toFixedVSS(2) + '</td>' +
											'<td>' + mode + '</td>' +
											'<td>' + dateFromString + '</td>' +
											'<td>' + editDeleteButton + '</td></tr>');

										totalAmountPaid = parseFloat(totalAmountPaid) + parseFloat(paymentPaySupplier.paidAmount.toFixedVSS(2));

										srno++;
									}

									$('#inventoryId').html("<b>Inventory Id : </b>" + paymentPaySupplierListResponse.inventoryTransactionId);
									$('#supplierDetailsId').html("<b>Supplier : </b>" + paymentPaySupplierListResponse.supplierName);
									$('#totalAmt').html("<b>Total Amount : </b>" + parseFloat(paymentPaySupplierListResponse.totalAmountTax).toFixedVSS(2));

									var totalAmountBal = (parseFloat(paymentPaySupplierListResponse.totalAmountTax) - parseFloat(totalAmountPaid)).toFixedVSS(2);
									$('#balAmt').html("<b>Balance Amount : </b>" + parseFloat(totalAmountBal).toFixedVSS(2));

									$('#totalAmountPaid').text(parseFloat(totalAmountPaid).toFixedVSS(2));
									//$('#viewPaymentDetail').modal('open');

									refreshInventoryReport();
								},
								error: function (xhr, status, error) {
									Materialize.Toast.removeAll();
									Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
								}

							});

						}
						/* 
						RESET INVENTORY TABLE DATA 
						*/
						function refreshInventoryReport() {
							var tblData = $('#tblData').DataTable();
							tblData.clear().draw();

							var range = "${range}";
							if (range == "" || range == undefined) {
								range = "-";
							}

							var startDate = "${startDate}";
							if (startDate == "" || startDate == undefined) {
								startDate = "-";
							}

							var endDate = "${endDate}";
							if (endDate == "" || endDate == undefined) {
								endDate = "-";
							}
							var supplierId = "${supplierId}";
							var data = {
								range: range,
								startDate: startDate,
								endDate: endDate,
								supplierId: supplierId
							};


							if (supplierId == "" || supplierId == undefined) {
								data = {
									range: range,
									startDate: startDate,
									endDate: endDate
								};
							}



							$.ajax({
								type: 'GET',
								url: "${pageContext.servletContext.contextPath}/fetchInventoryReportViewAjax",
								data: data,
								beforeSend: function () {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
								},
								async: false,
								success: function (data) {
									for (var i = 0; i < data.length; i++) {
										listValue = data[i];

										var supplierAnchorVisibility;
										if ("${sessionScope.loginType!='GateKeeper'}") {

											supplierAnchorVisibility = '<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Supplier Details"' +
												'href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=' + listValue.supplier.supplierId + '">' +
												listValue.supplier.name + '</a>';

										} else {
											supplierAnchorVisibility = listValue.supplier.name;
										}
										/* 	
											   MODAL OPEN ALOOWED ONLY SOME PAYMENT DONE ACCORDING TRANSACTION
										*/
										var amountPaid;
										if (listValue.payStatus != 'UnPaid') {
											amountPaid = '<button class="btn-flat" onclick="fetchPaymentDetails(' + listValue.transactionId + ')">' +
												'<b>' +
												listValue.amountPaid.toFixedVSS(2) +
												'</b>' +
												'</button>';
										} else {
											amountPaid = listValue.amountPaid.toFixedVSS(2);
										}

										var addedDate = moment(listValue.addedDate).format("DD-MM-YYYY");
										var addedTime = moment(listValue.addedDate).format("HH:mm:ss");
										var paymentDate = moment(listValue.paymentDate).format("DD-MM-YYYY");

										var billDate = moment(listValue.billDate).format("DD-MM-YYYY");
										var billTime = moment(listValue.billDate).format("HH:mm:ss");

										var paidStatus;
										if (listValue.payStatus == 'Paid') {
											paidStatus = "<b><font color='green'>" + listValue.payStatus + "</font></b>";
										} else if (listValue.payStatus == 'UnPaid') {
											paidStatus = "<b><font color='Red'>" + listValue.payStatus + "</font></b>";
										} else if (listValue.payStatus == 'Partially Paid') {
											paidStatus = "<b><font color='Blue'>" + listValue.payStatus + "</font></b>";
										}

										tblData.row.add([
											listValue.srno,
											listValue.transactionId,
											listValue.billNumber,
											supplierAnchorVisibility,

											'<button class="btn blue-gradient"' +
											'onclick="showTransactionDetails(\'' + listValue.transactionId + '_' + listValue.payStatus + '\')">' +
											'View</button>',

											listValue.totalQuantity,
											listValue.totalAmount,
											(parseFloat(listValue.totalAmountTax) - parseFloat(listValue.totalAmount)).toFixedVSS(2),
											listValue.totalAmountTax.toFixedVSS(2),
											amountPaid,
											listValue.amountUnPaid.toFixedVSS(2),
											addedDate + "<br>" + addedTime,
											billDate + "<br>" + billTime,
											paymentDate,
											listValue.byUserName,
											paidStatus,

											'<a class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Make Payment"' +
											'href="${pageContext.servletContext.contextPath}/paymentSupplier?inventoryTransactionId=' + listValue.transactionId + '">' +
											'Pay</a>',
											'<a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/inventory-invoice.pdf?inventoryTsnId='+listValue.transactionId+'"><i class="material-icons">receipt</i></a>'
										]).draw(false);
									}

									$('.preloader-background').hide();
									$('.preloader-wrapper').hide();
								},
								error: function (xhr, status, error) {
									$('.preloader-background').hide();
									$('.preloader-wrapper').hide();
									Materialize.Toast.removeAll();
									Materialize.toast('Something Went Wrong', '2000', 'teal lighten-2');
								}
							});

						}
					</script>
					<style>
						tr [colspan="2"] {
							text-align: center;
						}

						tr,
						td,
						th {
							text-align: center;
						}

						.input-field {
							position: relative;
							margin-top: 0.5rem;
						}
						.productModal{
							width:70% !important;
						}
						.mobileNoDiv{
							text-align: right !important;		
						}
						.noBottomMargin{
							margin-bottom:0 !important;
						}
						@media only screen and (max-width: 600px) {
							.productModal{
							width:90% !important;
							border-radius: 10px !important;
						}
						.mobileNoDiv{
							text-align: left !important;		
						}
					
						}
						@media only screen and (min-width: 601px) and (max-width: 992px) {
							.productModal{
							width:90% !important;
						}
						}
					</style>


			</head>

			<body>
				<!--navbar start-->
				<%@include file="components/navbar.jsp" %>
					<!--navbar end-->
					<!--content start-->
					<main class="paddingBody">
						<br class="hide-on-small-only">
						<div class="row">
							<div class="col s6 m3 l3 right right-align actionDiv actionBtn">
								<!-- <div class="col s6 m4 l4 right"> -->
								<!-- Dropdown Trigger -->
								<a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action
									<i class="material-icons right">arrow_drop_down</i>
								</a>
								<!-- Dropdown Structure -->
								<ul id='filter' class='dropdown-content'>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=today&suppplierId=${supplierId}">Today</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=yesterday&suppplierId=${supplierId}">Yesterday</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=last7days&suppplierId=${supplierId}">Last 7 Days</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=currentMonth&suppplierId=${supplierId}">Current Month</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=lastMonth&suppplierId=${supplierId}">Last Month</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=last3Months&suppplierId=${supplierId}">Last 3 Month</a>
									</li>
									<li>
										<a class="rangeSelect">Range</a>
									</li>
									<li>
										<a class="pickdate">Pick date</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=viewAll&supplierId=${supplierId}">View All</a>
									</li>
								</ul>
							</div>
							<div class="input-field col s6 l5 m4 offset-l3 offset-m4 right actionDiv" id="oneDateDiv">
								<form action="${pageContext.request.contextPath}/fetchInventoryReportView" method="post">
									<input type="hidden" name="range" value="pickDate">
									<input type="hidden" name="supplierId" value="${supplierId}">
									<div class="input-field col s8 m6 l4">
										<input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
										<label for="oneDate" class="black-text">Pick Date</label>
									</div>
									<div class="input-field col s3 m2 l2">
										<button type="submit" class="btn">View</button>

									</div>
								</form>
							</div>
							<div class="input-field col s12 l5 m5 offset-m3 offset-l1 right actionDiv rangeDiv">
								<form action="${pageContext.request.contextPath}/fetchInventoryReportView" method="post">
									<input type="hidden" name="range" value="range">
									<input type="hidden" name="supplierId" value="${supplierId}">
									<span class="showDates">
										<div class="input-field col s5 m4 l3">
											<input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
											<label for="startDate">From</label>
										</div>
										<div class="input-field col s5 m4 l3">
											<input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required>
											<label for="endDate">To</label>
										</div>

										<div class="input-field col s2 m2 l2">
											<button type="submit" class="btn">View</button>
										</div>
									</span>
								</form>
							</div>





							<div class="col s12 l12 m12">
								<table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th class="print-col hideOnSmall">Sr. No</th>
											<th class="print-col">Transaction Id</th>
											<th class="print-col hideOnSmall">Bill Number</th>
											<th class="print-col">Supplier Name</th>
											<th>Product</th>
											<th class="print-col">Total Qty</th>
											<th class="print-col hideOnSmall">Taxable Amount</th>
											<th class="print-col hideOnSmall">Tax</th>
											<th class="print-col">Total Amount</th>
											<th class="print-col">Amount Paid</th>
											<th class="print-col">Balance Amount
												<a  id="showColumn" class="tooltipped hide-on-small-only cursorPointer" area-hidden="true" data-position="left" data-delay="50"
												 data-tooltip="Show more coloumns">
													<i class="material-icons black-text">swap_horiz</i>
												</a>
											</th>
											<th class="print-col toggle1 hideOnSmall">Added Date</th>
											<th class="print-col toggle1 hideOnSmall">Bill Date</th>
											<th class="print-col toggle1 hideOnSmall">Payment Date</th>
											<th class="print-col toggle1 hideOnSmall">By User</th>
											<th class="print-col">Payment Status</th>
											<th class="toggle hideOnSmall">Pay</th>
											<th class="toggle hideOnSmall">Invoice</th>
										</tr>
									</thead>

									<tbody>
										<c:if test="${not empty inventoryReportViews}">
											<c:forEach var="listValue" items="${inventoryReportViews}">
												<tr>
													<td>
														<c:out value="${listValue.srno}" />
													</td>
													<td>
														<c:out value="${listValue.transactionId}" />
													</td>
													<td>
														<c:out value="${listValue.billNumber}" />
													</td>
													<td class="wrapok">
														<c:choose>
															<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
																<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Supplier Details" href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=${listValue.supplier.supplierId}">
																	<c:out value="${listValue.supplier.name}" />
																</a>
															</c:when>
															<c:otherwise>
																<c:out value="${listValue.supplier.name}" />
															</c:otherwise>
														</c:choose>
													</td>
													<td>
														<button class="btn blue-gradient" onclick="showTransactionDetails('${listValue.transactionId}_${listValue.payStatus}')">View</button>
													</td>
													<td>
														<c:out value="${listValue.totalQuantity}" />
													</td>
													<td>
														<c:out value="${listValue.totalAmount}" />
													</td>
													<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountTax-listValue.totalAmount}"
														/>
													</td>
													<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountTax}" />
													</td>
													<td>
														<%-- <c:choose>
                            	<c:when test="${sessionScope.loginType!='Admin'}">     --%>
															<c:choose>
																<c:when test="${listValue.payStatus!='UnPaid'}">
																	<button class="btn-flat" onclick="fetchPaymentDetails('${listValue.transactionId}')">
																		<b>
																			<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
																		</b>
																	</button>
																</c:when>
																<c:otherwise>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
																</c:otherwise>
															</c:choose>
															<%-- 	</c:when>
	                    		<c:otherwise>
	                    			<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
	                    		</c:otherwise>
	                    		</c:choose> --%>
													</td>
													<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountUnPaid}" />
													</td>
													<td class="wrapok">
														<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.addedDate}" />
														<c:out value="${date}" />
														<br>
														<fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.addedDate}" />
														<c:out value="${time}" />
													</td>
													<td>
														<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.billDate}" />
														<c:out value="${date}" />
														<br>
														<fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.billDate}" />
														<c:out value="${time}" />
													</td>
													<td class="wrapok">
														<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.paymentDate}" />
														<c:out value="${date}" />
													</td>
													<td>
														<c:out value="${listValue.byUserName}" />
													</td>
													<td>
														<font color="${listValue.payStatus=='Paid'?'Green':''}
                            				   ${listValue.payStatus=='UnPaid'?'Red':''}
                            				   ${listValue.payStatus=='Partially Paid'?'Blue':''}">
															<b>
																<c:out value="${listValue.payStatus}" />
															</b>
														</font>
													</td>
													<td>
														<c:choose>
															<c:when test="${listValue.payStatus=='Paid'}">
																NA
															</c:when>
															<c:otherwise>
																<a class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Make Payment" href="${pageContext.servletContext.contextPath}/paymentSupplier?inventoryTransactionId=${listValue.transactionId}">Pay</a>
															</c:otherwise>
														</c:choose>
													</td>
						                             <td><a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/inventory-invoice.pdf?inventoryTsnId=${listValue.transactionId}"><i class="material-icons">receipt</i></a></td>

												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<!-- Modal Structure for View Product Details -->
						<div id="product" class="modal modal-fixed-footer1 productModal">
							<div class="modal-content">
								<h5 class="center">
									<u>Product Details</u>
									<i class="modal-close material-icons right">clear</i>
								</h5>

								<div class="row">
								<div class="col s12 m6 l6">
										<h6 class=""><b>Supplier Name :</b>
									<span id="supplierNameId"></span>
								</h6>
							
									</div>
									<div class="col s12 m6 l6 mobileNoDiv">
											<h6 class=""><b>Mobile No. :</b>
									<span id="mobNoId"></span>
								</h6>
										</div>
								</div>
								<div class="scrollDivTable">
									<table border="2" class="centered tblborder">
										<thead>
											<!-- <tr>
												<th class="hideColumnOnSmall">Sr.No</th>
												<th class="hideColumnOnSmall">HSN Code</th>
												<th>Product Name</th>
												<th class="hideColumnOnSmall">Category</th>
												<th class="hideColumnOnSmall">Brand</th>
												<th class="hideColumnOnSmall">Rate</th>
												<th class="hideColumnOnSmall">Tax Slab</th>
												<th>MRP</th>
												<th>Quantity</th>
												<th>Total Amount</th>
											</tr> -->
											<tr>
												<th>Sr.No</th>
												<th>Product Name</th>
												<th>Unit Price</th>
												<th>MRP</th>
												<th>Quantity</th>
												<th>Taxable Amt</th>
												<th>Amount</th>
												<th>Disc.(%)</th>
												<th>Disc.Amt.</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody id="productOrderedDATA">

										</tbody>
									</table>
								</div>
							</div>
							<div class="modal-footer row hide-on-small-only1 noBottomMargin">
							
								<div class="col s12 m12 l12 center">
									<span  id="editButton">
									</span>
									<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
									<span  id="deleteButton">
									</span>
								</div>
								

							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l8">
								<div id="addeditmsg" class="modal">
									<div class="modal-content" style="padding:0">
										<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
										<!--  <h5 id="msgHead"></h5> -->

										<h6 id="msg" class="center"></h6>
									</div>
									<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
								</div>
							</div>
						</div>

						<!-- Payment list view model -->
						<div id="viewPaymentDetail" class="modal modal-fixed-footer">
							<div class="modal-content">
								<h5 class="center">
									<u>Payment Details</u><i class="modal-close material-icons right">clear</i>
								</h5>
								

								<div class="row">
									<div class="col s12 l6 m6">
										<h6 id="inventoryId" class="black-text">Inventory No:</h6>
									</div>
									<div class="col s12 l6 m6">
										<h6 id="totalAmt" class="black-text">Total Amt :</h6>
									</div>


									<div class="col s12 l6 m6">
										<h6 id="balAmt" class="black-text">Balance Amount:</h6>
									</div>
									<div class="col s12 l6 m6">
										<h6 id="supplierDetailsId" class="black-text">Supplier : </h6>
									</div>
								</div>

								<table border="2" class="centered tblborder">
									<thead>
										<tr>
											<th>Sr.No</th>
											<th>Amount</th>
											<th>Mode</th>
											<th> Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody id="paymentDetail">

									</tbody>
									<tfoot>
										<tr>
											<th>Total</th>
											<th>
												<span id="totalAmountPaid"></span>
											</th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="modal-footer row">
								<div class="col s6 m6 l3 offset-l4">
									<a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
								</div>
							</div>
						</div>
						<!-- payment list view model end -->

					</main>
					<!--content end-->
			</body>

			</html>