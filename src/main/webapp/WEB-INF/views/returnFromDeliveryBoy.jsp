<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
	<%@include file="components/header_imports.jsp" %>


	<script type="text/javascript">

		$(document).ready(function () {
			var table = $('#tblData').DataTable();
			table.destroy();
			$('#tblData').DataTable({
				"oLanguage": {
					"sLengthMenu": "Show _MENU_",
					"sSearch": "_INPUT_" //search
				},
				autoWidth: false,
				columnDefs: [
					{ 'width': '1%', 'targets': 0 },
					{ 'width': '3%', 'targets': 1 },
					{ 'width': '12%', 'targets': 2 },
					{ 'width': '15%', 'targets': 3 },
					{ 'width': '5%', 'targets': 4 },
					{ 'width': '3%', 'targets': 5 },
					{ 'width': '3%', 'targets': 6 },
					{ 'width': '3%', 'targets': 7 },
					{ 'width': '3%', 'targets': 8 },
					{ 'width': '6%', 'targets': 9 }
				],
				lengthMenu: [
					[10, 25, 50, -1],
					['10', '25 ', '50 ', 'All']
				],

				dom: '<lBfr<"scrollDivTable"t>ip>',
				//dom: 'lBfrtip',
				buttons: {
					buttons: [
						//      {
						//      extend: 'pageLength',
						//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
						//  }, 
						{
							extend: 'pdf',
							className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
							customize: function (doc, config) {
								doc.content.forEach(function (item) {
									if (item.table) {
										item.table.widths = [20, 30, 70, 70, 70, 45, 45, 45, 50, 60]
									}
								})

								/*for customize the pdf content*/
								doc.pageMargins = [5, 20, 10, 5];
								doc.defaultStyle.fontSize = 8;
								doc.styles.title.fontSize = 12;
								doc.styles.tableHeader.fontSize = 11;
								doc.styles.tableFooter.fontSize = 11;
								doc.styles.tableHeader.alignment = 'center';
								doc.styles.tableBodyEven.alignment = 'center';
								doc.styles.tableBodyOdd.alignment = 'center';
							},
						},
						{
							extend: 'excel',
							className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'print',
							className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'colvis',
							className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
							collectionLayout: 'fixed two-column',
							align: 'left'
						},
					]
				}

			});
			$("select").change(function () {
				var t = this;
				var content = $(this).siblings('ul').detach();
				setTimeout(function () {
					$(t).parent().append(content);
					$("select").material_select();
				}, 200);
			});
			$('select').material_select();
			$('.dataTables_filter input').attr("placeholder", "Search");
			if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);
				$("#oneDateDiv").removeClass('right right-align');
			}
			$(".showDates").hide();
			$(".rangeSelect").click(function () {
				$("#oneDateDiv").hide();
				$(".showDates").show();
			});
			$("#oneDateDiv").hide();
			$(".pickdate").click(function () {
				$(".showDates").hide();
				$("#oneDateDiv").show();
			});
		});

	</script>
	<style>
		#oneDateDiv {
			margin-top: 0.3rem;
			margin-left: 4%
		}

		.input-field {
			position: relative;
			margin-top: 0.5rem;
		}
	</style>
</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">
		<br>
		<div class="row noBottomMarginOnLarge">

			<div class="col s5 m3 l2 right right-align">
				<!-- <div class="col s6 m4 l4 right"> -->
				<!-- Dropdown Trigger -->
				<a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
						class="material-icons right">arrow_drop_down</i></a>
				<!-- Dropdown Structure -->
				<ul id='filter' class='dropdown-content'>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today">Today</a>
					</li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=yesterday">Yesterday</a>
					</li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=last7days">Last
							7 Days</a></li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=currentMonth">Current
							Month</a></li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=lastMonth">Last
							Month</a></li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=last3Months">Last
							3 Month</a></li>
					<li><a class="rangeSelect">Range</a></li>
					<li><a class="pickdate">Pick date</a></li>
					<li><a
							href="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=viewAll">View
							All</a></li>
				</ul>
			</div>
			<div class="input-field col s6 l3 m3 right right-align" id="oneDateDiv" style="margin-top:0">
				<form action="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport" method="post">
					<div class="input-field col s8 m7 l7">
						<input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
						<label for="oneDate" class="black-text">Pick Date</label>
					</div>
					<div class="input-field col s4 m2 l2" style="margin-top:1%;">
						<button type="submit" class="btn">View</button>
						<input type="hidden" value=pickDate name="range">
					</div>
				</form>
			</div>



			<div class="input-field col s12 l4 m4 right" style="margin-top:0">
				<form action="${pageContext.request.contextPath}/fetchReturnOrderFromDeliveryBoyReport" method="post">
					<span class="showDates">
						<div class="input-field col s4 m5 l5">
							<input type="date" class="datepicker" placeholder="Choose Date" name="startDate"
								id="startDate" required>
							<input type="hidden" value="range" name="range">
							<label for="startDate">From</label>
						</div>
						<div class="input-field col s4 m5 l5">
							<input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
							<label for="endDate">To</label>
						</div>
						<div class="input-field col s4 m1 l1" style="margin-top:1%">
							<button type="submit" class="btn">View</button>
						</div>
					</span>
				</form>
			</div>



		</div>
		<!--  </div> -->
		<div class="row">
			<div class="col s12 l12 m12">
				<table class="striped highlight centered" id="tblData" width="100%">

					<thead>
						<tr>
							<th class="print-col hideOnSmall">Sr. No</th>
							<th class="print-col">Order Id</th>
							<th class="print-col">Salesman Name</th>
							<th class="print-col">Shop Name</th>
							<th class="print-col">Area</th>
							<th class="print-col">Total issued Qty</th>
							<th class="print-col">Total Delivered Qty</th>
							<th class="print-col">Total Damage Qty</th>
							<th class="print-col">Total Recieving Qty</th>
							<th class="print-col">Status</th>
						</tr>

					</thead>
					<tbody>
						<c:if test="${not empty orderFromDeliveryBoyReportList}">
							<c:forEach var="listValue" items="${orderFromDeliveryBoyReportList}">
								<tr>
									<td>
										<c:out value="${listValue.srno}" />
									</td>
									<td><a title="View Order Details"
											href="${pageContext.request.contextPath}/fetchReturnFromDeliveryBoy?returnFromDeliveryBoyMainId=${listValue.returnFromDeliveryBoyMainId}">
											<c:out value="${listValue.returnFromDeliveryBoyMainId}" /></a></td>
									<td class="wrapok">
										<c:out value="${listValue.smName}" />
									</td>
									<td class="">
										<c:out value="${listValue.shopName}" />
									</td>
									<td class="wrapok">
										<c:out value="${listValue.areaName}" />
									</td>
									<td>
										<c:out value="${listValue.totalIssuedQuantity}" />
									</td>
									<td>
										<c:out value="${listValue.totalDeliveryQuantity}" />
									</td>
									<td>
										<c:out value="${listValue.totalDamageQuantity}" />
									</td>
									<td>
										<c:out value="${listValue.totalReceivedQuantity}" />
									</td>
									<td>
										<c:out value="${(listValue.status==true)?'Received':'Pending'}" />
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>


		<div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
						<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->

						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
							<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>

					</div>
				</div>
			</div>
		</div>


	</main>
	<!--content end-->
</body>

</html>