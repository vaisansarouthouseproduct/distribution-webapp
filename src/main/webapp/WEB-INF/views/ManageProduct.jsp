<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
   <script type="text/javascript">
   /* $(window).bind('resize', function () {
	    oTable.fnAdjustColumnSizing();
	  } ); */
	  var msg="${saveMsg}";
	  window.onhashchange = function() {
		  msg="";
		 } 
   $(document).ready(function() {
				
				 <c:set var="saveMsg" value=""/>
				$('#addeditmsg').modal({
				 complete:function(){
					 msg="";
					 <c:set var="saveMsg" value="" scope="page"/>
					 var m = "${saveMsg}";
					// console.log("m = "+m);
				 }
				});
				// alert(msg);
				 if(msg!='' && msg!=undefined)
				 {
					 $('#addeditmsg').find("#modalType").addClass("success");
						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
				     $('#addeditmsg').modal('open');
				     /* $('#msgHead').text("Product Message"); */
				     $('#msg').text(msg);
				     msg="";
				 }
				 var table = $('#tblData').DataTable();
				 table.destroy();
				 $('#tblData').DataTable({
			         "oLanguage": {
			             "sLengthMenu": "Show _MENU_",
			             "sSearch": " _INPUT_" //search
			         },
			        // "sScrollXInner": "100%",
			         autoWidth: false,
			         columnDefs: [
									{ "width": "1%", "targets": 0},	
									/* { "width": "1%", "targets": 1}, */	//Image is hide
									/* { "width": "3%", "targets": 2, "visible": false}, */
									{ "width": "15%", "targets": 1},	
									{ "width": "15%", "targets": 2},	
									{ "width": "15%", "targets": 3},	
									{ "width": "5%", "targets": 4},
									{ "width": "5%", "targets": 5},
									{ "width": "1%", "targets":6},
									{ "width": "1%", "targets": 7},
									{ "width": "1%", "targets": 8},
									{ "width": "1%", "targets": 9},
									{ "width": "1%", "targets": 10},
									{ "width": "1%", "targets": 11},
									{ "width": "1%", "targets": 12},
									{ "width": "1%", "targets": 13},
									{ "width": "1%", "targets": 14},
									{ "width": "1%", "targets": 15},
									{ "width": "1%", "targets": 16},
									{ "width": "1%", "targets": 17},
									{ "width": "1%", "targets": 18},
									{ "width": "1%", "targets": 19},
									{ "width": "1%", "targets": 20,"orderable": false}
			                      ],
			         
			                   lengthMenu: [
			                  	             [10, 25, 50, -1],
			                  	             ['10', '25', '50', 'All']
			                  	         ],
			         dom:'<lBfr<"scrollDivTable"t>ip>',
			         buttons: {
			             buttons: [
			                 //      {
			                 //      extend: 'pageLength',
			                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
			                 //  }, 
			                 {
			                     extend: 'pdf',
			                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: '.print-col'
			                     },
			                     customize: function(doc, config) {
			                    	 doc.content.forEach(function(item) {
			                    		  if (item.table) {
			                    		  item.table.widths = [15,80,70,70,30,30,30,20,35,20,20,35,20,35,20,35,35,45,45] 
			                    		 } 
			                    		    })
			                         var tableNode;
			                         for (i = 0; i < doc.content.length; ++i) {
			                           if(doc.content[i].table !== undefined){
			                             tableNode = doc.content[i];
			                             break;
			                           }
			                         }
			        
			                         var rowIndex = 0;
			                         var tableColumnCount = tableNode.table.body[rowIndex].length;
			                          
			                         if(tableColumnCount > 6){
			                           doc.pageOrientation = 'landscape';
			                         }
			                         /*for customize the pdf content*/ 
			                         doc.pageMargins = [5,20,10,5];
			                        // doc.styles.
			                         doc.defaultStyle.fontSize = 8	;
			                         doc.styles.title.fontSize = 12;
			                         doc.styles.tableHeader.fontSize = 11;
			                         doc.styles.tableFooter.fontSize = 11;
			                         doc.styles.tableHeader.alignment = 'center';
			                         doc.styles.tableBodyEven.alignment = 'center';
			                         doc.styles.tableBodyOdd.alignment = 'center';
			                       },
			                 },
			                 {
			                     extend: 'excel',
			                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: ':visible.print-col'
			                     },
			                 },
			                 {
			                     extend: 'print',
			                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: ':visible.print-col'
			                     },
			                 },
			                 {
			                     extend: 'colvis',
			                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
			                     collectionLayout: 'fixed two-column',
			                     align: 'left'
			                 },
			             ]
			         }

			     });
				 $("select")
	                .change(function() {
	                    var t = this;
	                    var content = $(this).siblings('ul').detach();
	                    setTimeout(function() {
	                        $(t).parent().append(content);
	                        $("select").material_select();
	                    }, 200);
	                });
	            $('select').material_select();
				$('.dataTables_filter input').attr("placeholder", "Search");
				$('.dataTables_filter input').attr("placeholder", "Search");
				if ($.data.isMobile) {
					var table = $('#tblData').DataTable(); // note the capital D to get the API instance
					var column = table.columns('.hideOnSmall');
					column.visible(false);
				}
				if($.data.mobileAndIpad){
					$(".removeRight").removeClass('right right-align');
				}
	            var table1 = $('#tblData').DataTable(); // note the capital D to get the API instance
	            var column1 = table1.columns('.toggle');
	            column1.visible(false);
	            $('#showColumn').on('click', function () {
	             
	            	 //console.log(column);
	          
	            	 column1.visible(!column1.visible()[0]);
	          
	            });
	            
	            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
	            var column = table.columns('.toggleAauth');
	            console.log(column);
	            if(${sessionScope.loginType=='CompanyAdmin'}){            
	                column.visible(true);
	            }else{
	            	 column.visible(false);
	            }
			});
   </script>
   
    <style>
     /*    table.dataTable tbody td {
        padding: 0 6px !important;
    } */
        table, td, th {
  
    font-size:13px;
}
	/* .dataTables_scrollHead{
		height:0px !important;
	}
	.dataTables_scrollBody thead tr,
	.dataTables_scrollBody thead tr th, 
	.dataTables_scrollBody thead tr th .dataTables_sizing{
		height:60px !important;
	} */
	@media only screen and (min-width:992px){
		#bulkUploadModal{
		width:40%;
	}
	}
	@media only screen and (max-width:600px){
		#bulkUploadModal{
		width:90%;
	}
	}
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <div class="row">      
        <c:if test="${sessionScope.loginType=='CompanyAdmin'}">  
	        <div class="col s6 l3 m3">
				<a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/fetchBrandList" style="margin-top:10%">
					<i class="material-icons left hide-on-small-only" >settings</i>Manage Brand</a>
	         
	            
	        </div> 
			 <div class="col s6 l3 m3  right-align">
	         <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/fetchCategoriesList" style="margin-top:10%"><i class="material-icons left hide-on-small-only" >settings</i>Manage Category</a>
	        </div>
	        <div class="col s6 l3 m3  right-align removeRight">
	           <a class="btn waves-effect waves-light blue-gradient modal-trigger" href="#bulkUploadModal" onclick="resetBulkForm(this)" style="margin-top:10%"><i class="material-icons left hide-on-small-only" >add</i>Bulk Upload</a>
	        </div>
	        <div class="col s6 l3 m3 right-align">
	           <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/addProduct" style="margin-top:10%"><i class="material-icons left hide-on-small-only" >add</i>Add Product</a>
	        </div> 
        </c:if>
        
      
       
		<div class="col s12 m12 l12">
		<br/>
            <table class="striped highlight centered" id="tblData" width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" class="print-col hideOnSmall">Sr. No.</th>
                        <!-- <th rowspan="2">Image</th> -->
                       <!--  <th rowspan="2" class="print-col">Product Code</th> -->
                        <th rowspan="2" class="print-col" style="min-width:200px">Product</th>
                        <th rowspan="2" class="print-col" style="min-width:100px">Category</th>
                        <th rowspan="2" class="print-col hideOnSmall" style="min-width:100px">Brand</th>                        
                        <th rowspan="2" class="print-col">HSNCode</th>
                         <th rowspan="2" class="print-col toggle">Barcode</th>
                        
                        <th rowspan="2" class="print-col">Rate</th>
                        <th rowspan="2" class="print-col">MRP</th>
                        <th rowspan="2" class="print-col">Current Qty</th>
                        <th rowspan="2" class="print-col">Taxable Amount</th>
                        <th rowspan="2" class="print-col toggle">Threshold</th>
                        <th colspan="2" class="print-col toggle">%CGST</th>
                        <th colspan="2" class="print-col toggle">%SGST</th>
                        <th colspan="2" class="print-col toggle">%IGST</th>
                        <th rowspan="2" class="print-col">Total</th>
                        <th rowspan="2" class="print-col toggle">Added</th>
                        <th rowspan="2" class="print-col toggle">Updated</th>
                        <th rowspan="2" class="toggleAauth">Edit<br/><a id="showColumn" class="cursorPointer"><i class="material-icons black-text tooltipped" area-hidden="true" data-position="left" data-delay="30" data-tooltip="Show more columns">swap_horiz</i></a></th>
                        <!-- <th rowspan="2">Delete</th> -->

                    </tr>
                    <tr>
                    	<th class="print-col toggle">%</th>
                        <th class="print-col toggle">Amt</th>
                        <th class="print-col toggle">%</th>
                        <th class="print-col toggle">Amt</th>
                        <th class="print-col toggle">%</th>
                        <th class="print-col toggle">Amt</th>
                    </tr>
                </thead>

                <tbody id="tbodyWidth">
                     <c:if test="${not empty productlist}">
							<c:forEach var="listValue" items="${productlist}">
                    <tr>
                        <td><c:out value="${listValue.srno}" /></td>
                        
                        <%-- <td><img src="${pageContext.request.contextPath}/downloadProductImage/${listValue.productId}" alt="" style="border:1px black solid;height:40px;width:40px;" /></td> --%>
                        
                        <%-- <td><c:out value="${listValue.productCode}" /></td> --%>
                        <td><c:out value="${listValue.productName}" /></td>
                        <td><c:out value="${listValue.categoryName}" /></td>
                        <td><c:out value="${listValue.brandName}" /></td>
                        <td class="wrapok"><c:out value="${listValue.hSNCode}" /></td>
                        <td class="wrapok"><c:out value="${listValue.productBarcode}" /></td>
                       
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.productRate}" /></td>
                        <td><c:out value="${listValue.productRateWithTax}" /></td>
                        <td><c:out value="${listValue.currentQuantity}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.taxableTotal}" /></td>
                         <td><c:out value="${listValue.thresholdValue}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="1" maxFractionDigits="1" value="${listValue.cgstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="3" maxFractionDigits="3" value="${listValue.cgstamount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="1" maxFractionDigits="1" value="${listValue.sgstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="3" maxFractionDigits="3" value="${listValue.sgstamount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="1" maxFractionDigits="1" value="${listValue.igstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.igstamount}" /></td>
                        <td><fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${listValue.total}" /></td>
                        <td class="wrapok">
                        	<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.productAddedDatetime}" /><c:out value="${dt}" />
		                    <br/>
	                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.productAddedDatetime}" /><c:out value="${time}" />
                        </td>
                        <td class="wrapok">
                        	<c:choose>
                        	<c:when test="${empty  listValue.productQuantityUpdatedDatetime}">
                        		NA
	                        </c:when>
	                        <c:otherwise>
		                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.productQuantityUpdatedDatetime}" /><c:out value="${dt}" />
		                        	<br/>
		                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.productQuantityUpdatedDatetime}" /><c:out value="${time}" />
	                        </c:otherwise>
	                        </c:choose>
                        </td>
                        
                        <td width="2%"><a class=" btn-flat " href="${pageContext.request.contextPath}/fetchProduct?productId=${listValue.productId}"><i class="material-icons tooltipped blue-text " data-position="left" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    </tr>
                    </c:forEach>
                    </c:if>
                    
                    <!-- Modal Structure for delete -->

                    <!-- <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->
                </tbody>
            </table>
</div>
        </form>
   </div>   
   			  <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>    
		<!-- bulk upload modal start -->
		<div id="bulkUploadModal" class="modal smsModal">

			<div class="modal-content">
				<h5 class="center-align">
					<u>Upload Excel Sheet</u><i
						class="material-icons notranslate right modal-close  waves-effect waves-red bulkUploadClose">close</i>
				</h5>
				<div class="file-field input-field">
					
					<div class="file-path-wrapper">
						<input id="fileSelectId" type="file">
						<input id="excelFile" onchange="checkExcelFile(this);" class="file-path validate" placeholder="Click to upload file" type="text">
					</div>
				</div>
				<h6 class="center-align">Download Sample File : <a class="textHover" href=${pageContext.request.contextPath}/downloadProductExcel>Bulk Sample</a></h6>
				<h6 class="center-align red-text" id="bulkModalMsg"></h6>
			</div>
			<!-- <div class="divider"></div> -->
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">

				
				<a class="btn" id="resetBtn" onclick="resetBulkForm(this)" >Reset</a> 
					<a id="bulkUploadClose" href="#!"
					class=" waves-effect waves-green modal-close btn red modalButtons bulkUploadClose"  onclick="resetBulkForm(this)">close</a>
				<button type="button" onclick="submitExcelData();"
					class="waves-effect btn teal waves-light blue-gradient">SEND</button>
				</div>
			</div>
		</div>
	
		<!-- bulk upload modal ends -->
		
       
    </main>
    <!--content end-->
    
    <!-- Excel Upload Start -->
    
    <script>
		var files;
		$(document).ready(function() {
				$(".bulkUploadClose").click(function(){
					 $("#excelFile").val("");
				});
				$('input[type=file]').on('change', prepareUpload);
				$('.loader').hide();
		});
		
		/* checks if file is excel file or not */
		function checkExcelFile(data) {
			var file = $(data).val();
			var ext = file.split(".");
		    ext = ext[ext.length-1].toLowerCase();      
		    var arrayExtensions = ["xlsx" , "xls"];//, "csv"];

		    if (arrayExtensions.lastIndexOf(ext) == -1) {
		    	Materialize.Toast.removeAll();
				Materialize.toast('Extension not supported.Only XLSX , XLS files can be uploaded!', '3000','red lighten-2');
		        $("#excelFile").val("");
		    }
		}
		
		/* resets bulk form */
		function resetBulkForm(){
			$("#excelFile").val("");
			$("#fileSelectId").val("");
			$('#bulkModalMsg').html("");
		}
		
		/* submit excel file */
		function submitExcelData(){
			var fileUrl=$("#excelFile").val();
			if(fileUrl=='' || fileUrl==undefined){
				Materialize.Toast.removeAll();
				Materialize.toast('Select Excel File For Upload!', '3000','red lighten-2');
				return false;
			}
			//get file object
			var data = new FormData();
		    $.each(files, function(key, value)
		    {
		        data.append("productFile", value);
		    });

		    $.ajax({
		    	url: "${pageContext.request.contextPath}/upload-product",
		    	type: 'POST',
		        enctype: 'multipart/form-data',
		        data: data,
		        cache: false,
		        dataType: 'json',
		        timeout: 600000,
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		        success: function(result){
		            if(result.success){
		            	Materialize.Toast.removeAll();
						Materialize.toast('Products addded successfully', '3000','toastMsg');
						resetBulkForm();
						$('#bulkUploadModal').modal('close');
						
					 	setTimeout(function() {
						 location.reload();
	                    }, 2000);
		            }else{
		            	$('#bulkModalMsg').html(result.msg);
		            }
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		            // Handle errors here
		            console.log('ERRORS: ' + textStatus);
		            // STOP LOADING SPINNER
		        }, beforeSend: function () {
                    $(".loader").show();
                },
                complete: function () {
                    $(".loader").hide();
                }
		        
		    });
		}
		
		// Grab the files and set them to our variable
		function prepareUpload(event)
		{
		  files = event.target.files;
		  $('#bulkModalMsg').html("");
		}
		
	</script>
    
    <!-- Excel Upload End -->
    
</body>

</html>