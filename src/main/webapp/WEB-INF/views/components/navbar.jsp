
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">

var run = function(){
	var online = navigator.onLine;
	if(!online)
	{
	document.getElementById('errorInternet').style.display="block";
		//alert("No internet connection.");
	}
	if(online)
	{ 
		document.getElementById('errorInternet').style.display="none";
		//alert("No internet connection.");
	}	
}
 
setInterval(run,1000);



</script>
<style>

	 .collapsible li.active .collapsible-header span>i {
  -ms-transform: rotate(180deg); /* IE 9 */
  -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
  transform: rotate(180deg);
}
	
        main{
        padding-right:0px;
        }
       /*css for side nav hide/show*/
        
        .reveal-open {
            max-width: 60px !important;
            /*max-height: 100px !important;*/
            /*margin-top: 62px;*/
        }
        
        .side-nav {
            /* overflow given to give effect of half hiding side nav */
            /*overflow: hidden;*/
            max-width: 230px;
            /* to give effect of sliding of side navbar */
            -webkit-transition: max-width .01s linear;
            -moz-transition: max-width .01s linear;
            transition: max-width .01s linear;
        }
        /* to give padding to body after side nav is half closed */
        
        .intro {
            padding-left: 70px;
        }
        /*for display button over sidebar*/
        
       /*  .side-nav.fixed {
            left: 0;
            top: 68px;
            -webkit-transform: translateX(0);
            transform: translateX(0);
            position: fixed;
            z-index: 100;
        } */
        
       /*  .sidebutton {
            transform: translateX(-190px) rotate(180deg);
            transform: rotate(180deg);
            transform-origin: right left;
        } */
        .greycolor {
            background-color: #eceff1;
        }
        
        #logo-collapse {
            display: none;
        }
        
        #logo-collapse-full {
            display: visible;
        }
        
        nav ul li a.btn-flat{
        	padding: 0 5px 4px 5px;
        	line-height:45px;
        	height:45px; 
        	margin-right: 0px;
        }
        .slidemargin{
        	
        }
     /*    nav a{
        	color:#303641 !important;
        } */
       
       	nav .btn-flat{
       		font-size:0.8rem !important;
       	}
       	/* url('resources/img/net.png') */
       	#errorInternet{
       	display:none;
       	 position:fixed; 
       	 background: grey  center no-repeat;
       	 background-size:cover;         	
       	 top:0;
       		right:0;
       		bottom:0;
       		left:0;
			height:100vh;
       	    width:100vw;
       	     opacity:0.8;  		
       		background-size:cover;
       		z-index:1000;
       		/* -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);	 */ 	
       	
       	}
   
		.spinner-blue, .spinner-blue-only {
    		border-color: black;
        }
 
        .smallHeighticon_35{
            height: 35px;

        }
        .iconImg{
            /* vertical-align: text-bottom;
            padding-right: 10px; */
            vertical-align: middle !important;
            margin: 0 4px 0 2px;
    
        }
    
 
        @media only screen and (max-width: 600px) {
        main{
        padding-right:0 !important;
        }
        .side-nav {
            /* overflow given to give effect of half hiding side nav */
            /*overflow: hidden;*/
            max-width: unset;
            
        }
        }
        @media only screen and (min-width: 601px) and (max-width: 992px) {
        main{
        padding-right:0 !important;
        }
        .side-nav {
            max-width: unset;
         
        }

} 

  span.num {
  position: relative;
  display: block;
  top: -3.5em;
  right: -1.4em;
  line-height: 1em;
  text-align: center;
  background:#f56954;
  color: #fff;
  border-radius: 50%;
  min-height: 20px;
  min-width: 20px;
  padding: 5px !important;
  font-size: 12px;
}
</style>


<!--top to scroll button-->
    <div onclick="topFunction()" id="myBtn"> <img src="resources/img/arrow-up.png" class="hide-on-small-only" alt="scroll up" />
        <img src="resources/img/chevron-arrow-up.png" class="hide-on-med-and-up" alt="scroll up" />
        </div>
    
  <div id="errorInternet" >    	
    	<!-- <img src="resources/img/molecule.png" class="pulse center-align" width="100%" height="100%"/> -->
    	<!-- <h2 class="pulse center-align" style="position:relative;top:30%">Oops</h2> -->
    	<h2 class="pulse center-align" style="position:relative;top:35%">No internet Connection....</h2>
    </div>
  <!-- style="display:none;" -->
    <!--navbar start-->
    <div class="navbar-fixed z-depth-3">
        <!--class=" blue darken-8 nav"-->
        <nav class="sidenav-color">

            <div class="nav-wrapper">
                <!--style="padding-left:50px; height:60px; "-->

                <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"><img class="responsive-img" src="resources/img/bslogo_66.png" style="margin:5% 2% 0 15%;height:50px;"></a> --%>
                
                <a href="#" data-activates="mobile-demo" class="button-collapse left" style="padding-left: 10px; margin-left: 0px;"><i class="material-icons">menu</i></a>
            
                <!--<ul class=" hide-on-med-and-down center" style="margin-left:50%">-->
                <!--<li><a id="slide"><i class="medium material-icons ">swap_horiz</i></a></li>-->
                <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                <!--</ul>-->
                <!--<ul class="center hide-on-med-and-down">
                    <li>
                        <a class="white-text  center" style="font-size:200%;">List of Supplier</a>
                    </li>
                </ul>-->
              <!--   <ul class="hide-on-med-and-down" style="margin-left:15%;">
                 <li><a id="slide" class=""><i class="medium material-icons">menu</i></a></li>
                </ul> -->
                
                <ul class="hide-on-large-only center-align center" style="margin:auto;">
                	 
                    <li style="float:unset;">
                        <a class="center heading1" style="font-size:120%;">${pageName}</a>
                    </li>
                  
                    <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                </ul>
                <ul class="hide-on-med-and-down" style="margin-left:45%;">
                	 
                    <li>
                        <a class="center heading" style="font-size:120%;">${pageName}</a>
                    </li>
                  
                    <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                </ul>
                
                <ul class="right hide-on-med-and-down">
                    <!--<li><a href="search" class="waves-effect waves-light btn-flat white-text" style="padding: 0px 5px 4px 5px; height: 64px; margin-right: 0px;"><i
						class="fa fa-search white-text"></i> Search</a></li>-->
					<li><a href="${pageContext.servletContext.contextPath}/getPendingPaymentListForAlert" class="notification">
                    			<i class="material-icons tooltipped"  data-position="left" data-delay="50" data-tooltip="Pending payment" aria-hidden="true">payment</i><span class="num">${sessionScope.pendingCounterOrderPaymentCount}</span></a></li>
                    					
                    			<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForUnderThresholdValue" class="">
                    			<i class="material-icons tooltipped" data-position="left" data-delay="50" data-tooltip="Product Under Thresold" aria-hidden="true">add_alert</i><span class="num">${sessionScope.productUnderThesoldCount}</span></a></li>
					<li><a href="#" class="dropdown-button waves-effect waves-light btn-flat tooltipped" data-activates="loginDetails" data-position="left" data-delay="50" data-tooltip="User Info"><i
					class="fa fa-user"></i></a>
					
						<ul class="dropdown-content loginDetails" id="loginDetails">
					<c:choose>
                    	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
                    	
		                    	
                    			
                    			
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="waves-effect waves-light truncate"><i
							class="material-icons left">call_split</i><c:out value="${sessionScope.selectedBranchName}"/> </a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordCompany" class="waves-effect waves-light truncate"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:when test="${sessionScope.loginType=='Admin'}">
                    		<%-- <li><a href="${pageContext.servletContext.contextPath}/getPendingPaymentListForAlert" class="notification">
                    			<i class="material-icons tooltipped"  data-position="left" data-delay="50" data-tooltip="Pending payment" aria-hidden="true">payment</i><span class="num">${sessionScope.pendingCounterOrderPaymentCount}</span></a></li>
                    			
                    		<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForUnderThresholdValue" class="">
                    			<i class="material-icons tooltipped" data-position="left" data-delay="50" data-tooltip="Product Under Thresold" aria-hidden="true">add_alert</i><span class="num">${sessionScope.productUnderThesoldCount}</span></a></li> --%>
                    			
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="waves-effect waves-light truncate">
	                    	<i class="fa fa-user left"></i>${sessionScope.sessionCopamnyName}( ${sessionScope.selectedBranchName} )</a></li>
                    		
                    		<li><a href="#" class="waves-effect waves-light truncate">
                    		<i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:otherwise>
                    		<%-- <li><a href="${pageContext.servletContext.contextPath}/getPendingPaymentListForAlert" class="notification">
                    			<i class="material-icons tooltipped"  data-position="left" data-delay="50" data-tooltip="Pending payment" aria-hidden="true">payment</i><span class="num">${sessionScope.pendingCounterOrderPaymentCount}</span></a></li>
                    			
                    		<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForUnderThresholdValue" class="">
                    			<i class="material-icons tooltipped" data-position="left" data-delay="50" data-tooltip="Product Under Thresold" aria-hidden="true">add_alert</i><span class="num">${sessionScope.productUnderThesoldCount}</span></a></li> --%>
                    		                    			
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="waves-effect waves-light truncate"><i
							class="material-icons left">call_split</i> ${sessionScope.selectedBranchName}</a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordGateKeeper" class="waves-effect waves-light truncate"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>                    	
                    	</c:otherwise>
                    </c:choose>
                    </ul>
					
					</li>
                    <li><a href="${pageContext.servletContext.contextPath}/logoutEmployee" class="waves-effect waves-light btn-flat"><i
					class="fa fa-sign-out left"></i> Logout</a></li>
                </ul>
                
            </div>
        </nav>
    </div>
    
    <!-- admin navbar for medium and small device start -->
    <% Calendar cal=Calendar.getInstance();%>
<c:if test="${sessionScope.loginType=='Admin'}">
    <div class="col s4 l4 m4">
         <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <!-- <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                    <li><a href="${pageContext.servletContext.contextPath}/fetchAllCompany" class="collapsible-header"><i class="icon icon-manage-company"></i>Manage Companies</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_admin" class="collapsible-header">Location<i class="material-icons">edit_location</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header">Employee Details<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><i class="icon icon-inventory-details"></i>Inventory Details</a></li>
                	<li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons">dvr</i>Counter Report</a></li>
                            	 <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="icon icon-payment-report"></i>Payment Report</a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="icon icon-bad-debts"></i>Bad Debts Report</a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons">subtitles</i>Cheque Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth&topProductNo=0"><i class="fa fa-cart-arrow-down"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="icon icon-business-report"></i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report" style="font-weight:bold"></i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons">assignment</i>Supplier Orders</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list"></i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="icon icon-return-item"></i>Return Item Report</a></li>
								<li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="icon icon-permanent-return"></i>Permanent Return</a></li>
								<li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="icon icon-counter-return"></i>Counter Return</a></li>
								<li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=today"><i class="icon icon-replacement-report" style="font-weight:bold"></i>Replacement Report</a></li>
								<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="icon icon-damage-report"></i>Damage Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="icon icon-collection-report"></i>Collection Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report"></i>Sales Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report"></i>Cancel Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr" data-position="right" style="text-align:center"></i></a></li>
		                	    <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><i class="icon icon-manage-expenses"></i>Expense Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss"><i class="material-icons" data-position="right" data-delay="50" data-tooltip="Profit & Loss" aria-hidden="true">show_chart</i>Profit & Loss</a></li>
                            </ul>
                        </div>
                    </li>                	
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">person_pin_circle</i></a></li>            
                    
                </ul>
            </li>
            	
					<c:choose>
                    	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="collapsible-header"><i
							class="material-icons left">call_split</i><c:out value="${sessionScope.selectedBranchName}"/> </a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordCompany" class="collapsible-header"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:when test="${sessionScope.loginType=='Admin'}">
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="collapsible-header">
	                    	<i class="material-icons left">call_split</i>${sessionScope.sessionCopamnyName}( ${sessionScope.selectedBranchName} )</a></li>
                    		
                    		<li><a href="#" class="collapsible-header">
                    		<i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:otherwise>
                    		<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="collapsible-header"><i
							class="material-icons left">call_split</i> ${sessionScope.selectedBranchName}</a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordGateKeeper" class="collapsible-header"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>                    	
                    	</c:otherwise>
                    </c:choose>
					
                    <li><a href="${pageContext.servletContext.contextPath}/logoutEmployee" class="collapsible-header"><i
					class="fa fa-sign-out left"></i> Logout</a></li>
        </ul>
    </div>
</c:if>
<!-- admin navbar for medium and small device stop -->
	<!-- Companyadmin navbar for medium and small device start -->
<c:if test="${sessionScope.loginType=='CompanyAdmin'}">
   <!-- fixed side nav on all page -->
    <div class="col s4 l4 m4">
         <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <!-- <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/" class="brand-logo left">Vaisansar<i class="material-icons black-text" aria-hidden="true">menu</i>
                        </a>
                	</li>  --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList" class="collapsible-header">Manage Supplier<i class="material-icons">settings</i>
                        </a>
                    </li> 
                    <li>
                        <a class="collapsible-header">Commission<i class="material-icons right chevron">expand_more</i><i class="material-icons">local_atm</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCommissionAssign"><i class="material-icons">playlist_add_check</i>Manage Commission</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/open_target_assign_list?departmentId=0"><i class="material-icons">playlist_add_check</i>Target Assign</a></li>
                            </ul>
                        </div>
                    </li>
                    	
                    <li><a href="${pageContext.servletContext.contextPath}/fetch_scheduled_meeting?departmentId=0&pickDate=<%=(new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime())%>" class="collapsible-header"><i class="material-icons">event</i>Meeting's</a>
                    </li>                                     
                              
                    <li>
                    	<%-- <a href="${pageContext.servletContext.contextPath}/banking" class="collapsible-header"><span class="element-sideNav">Manage Banking</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Banking" aria-hidden="true">account_balance</i>
                        </a> --%>
                        <a class="collapsible-header"><span class="element-sideNav">Manage Banking<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Banking">account_balance</i></a>
                        <div class="collapsible-body">
                            <ul>
                             <li><a href="${pageContext.servletContext.contextPath}/fetchBankList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Bank Name">home</i><span class="element-sideNav">Bank Details</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/banking"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Bank Account">star</i><span class="element-sideNav">Bank Account</span></a></li>
                                   <li><a href="${pageContext.servletContext.contextPath}/openEMICalculatorForm"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="EMI Calulator">iso</i><span class="element-sideNav">EMI Calculator</span></a></li>                                  
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/transportation" class="collapsible-header">Transportation<i class="material-icons">local_shipping</i>
                        </a>
                    </li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header">Location<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header">Location<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                    <li>
                        <a class="collapsible-header">Product<i class="material-icons right chevron">expand_more</i><i class="material-icons">local_grocery_store</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList"><i class="icon icon-manage-brand"></i>Manage Brand</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList"><i class="icon icon-manage-category"></i>Manage Category</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductList"><i class="icon icon-manage-product"></i>Manage Product</a></li>
                            </ul>
                        </div>
                    </li>
	                <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList" class="collapsible-header">HRM<i class="material-icons">people</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameList" class="collapsible-header">Business List<i class="material-icons">business_center</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Manage Inventory<i class="icon icon-inventory-details"></i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header">Manage Expense<i class="icon icon-inventory-details" data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true"></i></a></li>
                	<li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons" data-position="right" data-delay="50" data-tooltip="Counter Report">dvr</i>Counter Report</a></li>
                            	 <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                               	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="icon icon-payment-report"></i>Payment Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="icon icon-bad-debts"></i>Bad Debts Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons">subtitles</i>Cheque Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth&topProductNo=0"><i class="fa fa-cart-arrow-down" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="icon icon-business-report"></i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report" style="font-weight:bold"></i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons">assignment</i>Supplier Orders</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list"></i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="icon icon-return-item"></i>Return Item Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="icon icon-permanent-return"></i>Permanent return</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="icon icon-counter-return"></i>Counter Return</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=today"><i class="icon icon-replacement-report" style="font-weight:bold"></i>Replacement Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="icon icon-damage-report" data-position="right" data-delay="50" data-tooltip="Damage Report"></i>Damage Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="icon icon-collection-report"></i>Collection Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report"></i>Sales Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/employeeCommission?monthId=9&yearId=2018">Employee Commission<i class="icon icon-manage-expenses"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report"></i>Cancel Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr" style="text-align:center"></i></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/profit-loss">Profit & Loss<i class="material-icons">show_chart</i></a></li>
                                
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchPaymentPendingList" class="collapsible-header"><i class="icon icon-payment"></i>Payment</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth" class="collapsible-header ">Ledger<i class="material-icons">assessment</i></a></li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header">Complains<i class="material-icons">feedback</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header">Get Invoice<i class="material-icons">receipt</i></a></li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons">person_pin_circle</i></a></li>   
                    <li><a href="${pageContext.servletContext.contextPath}/logoutEmployee" class="collapsible-header">Logout <i class="fa fa-sign-out left"></i></a></li>         
                </ul>
            </li>
        </ul>
    </div>    
    <!--navbar end-->
</c:if>
	<!-- Companyadmin navbar for medium and small device stop -->
	<!-- gatekeeper navbar for medium and small device start -->   
<c:if test="${sessionScope.loginType=='GateKeeper'}">
<!-- fixed side nav on all page -->

    <div class="col s4 l4 m4">
        <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                <!--  <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                <li><a href="${pageContext.servletContext.contextPath}/openCounter" class="collapsible-header hide-on-med-and-down">Counter Page<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter" aria-hidden="true">dvr</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Manage Inventory<i class="icon icon-inventory-details"></i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header">Employee Details<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0">Booked Order List<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Booked Order List" aria-hidden="true">format_list_bulleted</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header">Get Invoice<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header">Manage Expense<i class="material-icons" data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>
                	<%-- <li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down " data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="icon icon-business-report"></i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report" style="font-weight:bold"></i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list"></i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="icon icon-return-item"></i>Return Item Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="icon icon-collection-report"></i>Collection Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report"></i>Sales Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report"></i>Cancel Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today">DeliveryBoy Return<i class="icon icon-delivery-boy-rreturn"></i></a></li>
		                		<% Calendar cal=Calendar.getInstance();%>	
		                		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>">Damage Recovery<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="text-align:center"></i></a></li>
                            </ul>
                        </div>
                    </li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today">DeliveryBoy Return<i class="icon icon-delivery-boy-rreturn"></i></a></li>
              
              		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>">Damage Recovery<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header">Complains<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li> --%>
                    <li>
                        <a class="collapsible-header">Reports <i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports">equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                 <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter Report">dvr</i>Counter Report</a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="icon icon-bad-debts"></i>Bad Debts Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report" style="font-weight:bold"></i>Inventory Report</a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons">assignment</i>Supplier Orders</a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list"></i>Order Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report"></i>Sales Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report"></i>Cancel Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderReportForWeb"><i class="icon icon-return-item"></i>Return Item Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="icon icon-permanent-return"></i>Permanent return</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="icon icon-counter-return"></i>Counter Return</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReplacementOrderReportForWeb"><i class="icon icon-replacement-report" style="font-weight:bold"></i>Replacement Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="icon icon-damage-report" data-position="right" data-delay="50" data-tooltip="Damage Report"></i>Damage Report</a></li>                                
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons  " >person_pin_circle</i></a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--navbar end-->
</c:if>
<!-- gatekeeper navbar for medium and small device stop -->   
	

<!-- navbar for large screen start -->
<!-- navbar for admin start -->
<c:if test="${sessionScope.loginType=='Admin'}">
    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                    <li><a href="${pageContext.servletContext.contextPath}/fetchAllCompany" class="collapsible-header"><i class="icon icon-manage-company tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Companies" aria-hidden="true"></i><span class="element-sideNav">Manage Companies</span></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_admin" class="collapsible-header"><span class="element-sideNav">Location</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header"><span class="element-sideNav">Employee Details</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><i class="tooltipped icon icon-inventory-details" data-position="right" data-delay="50" data-tooltip="Inventory Details" aria-hidden="true"></i><span class="element-sideNav">Inventory Details</span></a></li>
                	<li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">dvr</i><span class="element-sideNav">Counter Report</span></a></li>
                            	 <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="tooltipped icon icon-payment-report" data-position="right" data-delay="50" data-tooltip="Payment Report"></i><span class="element-sideNav">Payment Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="tooltipped icon icon-bad-debts"  data-position="right" data-delay="50" data-tooltip="Bad Debts Report"></i><span class="element-sideNav">Bad Debts Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i><span class="element-sideNav">Cheque Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth&topProductNo=0"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="tooltipped icon icon-business-report"  data-position="right" data-delay="50" data-tooltip="Business Report"></i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="tooltipped icon icon-inventory-report" data-position="right" data-delay="50" data-tooltip="Inventory Report" style="font-weight:bold"></i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="tooltipped icon icon-order-list" data-position="right" data-delay="50" data-tooltip="Order Report"></i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="tooltipped icon icon-return-item"  data-position="right" data-delay="50" data-tooltip="Return Item Report"></i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="tooltipped icon icon-permanent-return" data-position="right" data-delay="50" data-tooltip="Permanent return"></i><span class="element-sideNav">Permanent return</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="tooltipped icon icon-counter-return"  data-position="right" data-delay="50" data-tooltip="Counter Return"></i><span class="element-sideNav">Counter Return</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=today"><i class="tooltipped icon icon-replacement-report" data-position="right" data-delay="50" data-tooltip="Replacement Report" style="font-weight:bold;"></i><span class="element-sideNav">Replacement Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="tooltipped icon icon-damage-report" data-position="right" data-delay="50" data-tooltip="Damage Report"></i><span class="element-sideNav">Damage Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="tooltipped icon icon-collection-report"  data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true"></i><span class="element-sideNav">Collection Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="tooltipped  icon icon-sales-report"  data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true"></i><span class="element-sideNav">Sales Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="tooltipped icon icon-cancel-report" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true"></i><span class="element-sideNav">Cancel Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="text-align:center"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><i class="tooltipped icon icon-manage-expenses"  data-position="right" data-delay="50" data-tooltip="Expense Report" aria-hidden="true"></i><span class="element-sideNav">Expense Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Profit & Loss">show_chart</i><span class="element-sideNav">Profit & Loss</span></a></li>
                            </ul>
                        </div>
                    </li>                	
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">person_pin_circle</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>
</c:if>
<!-- navbar for admin end -->

<!-- navbar for company admin start -->
<c:if test="${sessionScope.loginType=='CompanyAdmin'}">
   <!-- fixed side nav on all page -->
    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left">Vaisansar<i class="material-icons black-text" aria-hidden="true">menu</i>
                        </a>
                    </li>  --%>
                    
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList" class="collapsible-header"><span class="element-sideNav">Manage Supplier</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Supplier" aria-hidden="true">settings</i>
                        </a>
                    </li>
                    <li>
                    
                        <a class="collapsible-header"><span class="element-sideNav">Commission<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Commission">local_atm</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCommissionAssign"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Commission">playlist_add_check</i><span class="element-sideNav">Manage Commission</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/open_target_assign_list?departmentId=0" class="collapsible-header"><span class="element-sideNav">Target Assign</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Target Assign" aria-hidden="true">assignment_turned_in</i>
                                </a></li>                                
                            </ul>
                        </div>
                    </li>
                    
                    
                    <li><a href="${pageContext.servletContext.contextPath}/fetch_scheduled_meeting?departmentId=0&pickDate=<%=(new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime())%>" class="collapsible-header"><span class="element-sideNav">Meeting's</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Meeting's" aria-hidden="true">event</i>
                        </a>
                    </li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/banking" class="collapsible-header"><span class="element-sideNav">Manage Banking</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Banking" aria-hidden="true">account_balance</i>
                        </a>
                    </li> --%>
                    
					<li>
                    	<%-- <a href="${pageContext.servletContext.contextPath}/banking" class="collapsible-header"><span class="element-sideNav">Manage Banking</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Banking" aria-hidden="true">account_balance</i>
                        </a> --%>
                        <a class="collapsible-header"><span class="element-sideNav">Manage Banking<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Banking">account_balance</i></a>
                        <div class="collapsible-body">
                            <ul>
                             <li><a href="${pageContext.servletContext.contextPath}/fetchBankList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Bank Name">home</i><span class="element-sideNav">Bank Details</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/banking"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Bank Account">star</i><span class="element-sideNav">Bank Account</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchPayeeList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Payee">person_pin</i><span class="element-sideNav">Manage Payee</span></a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/fetchCheckBookList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Book">library_books</i><span class="element-sideNav">Manage Cheque Book</span></a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/printChequeBook"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Print Cheque">print</i><span class="element-sideNav">Cheque Print</span></a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/openEMIChequePrintForm"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Print EMI Cheque">chrome_reader_mode</i><span class="element-sideNav">EMI Cheque Print</span></a></li>
                                   <li><a href="${pageContext.servletContext.contextPath}/openEMICalculatorForm"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="EMI Calulator">iso</i><span class="element-sideNav">EMI Calculator</span></a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/openChequeReport"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Book Reports">list_alt</i><span class="element-sideNav">Cheque Book Reports</span></a></li>
                            </ul>
                        </div>
                    </li>
                   
                    <li><a class="collapsible-header"><span class="element-sideNav">Manage Barcode<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Barcode">local_grocery_store</i></a>
                     <div class="collapsible-body">
                    	<ul>	
                    		<li><a href="${pageContext.servletContext.contextPath}/openBarcodeGeneratorForm" class="collapsible-header"><span class="element-sideNav">Barcode</span><i class="fa fa-barcode prefix"></i></a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/openBarcodeSeriesGeneratorForm" class="collapsible-header"><span class="element-sideNav">Barcode Series</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Barcode Series" aria-hidden="true">scanner</i></a></li>
                    	</ul>
                    </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/transportation" class="collapsible-header"><span class="element-sideNav">Transportation</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Transportation" aria-hidden="true">local_shipping</i>
                        </a>
                    </li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header"><span class="element-sideNav">Location</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li> --%>
                     <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header"><span class="element-sideNav">Location</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                    <li>
                        <a class="collapsible-header"><span class="element-sideNav">Product<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Product">local_grocery_store</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList"><i class="tooltipped icon icon-manage-brand" data-position="right" data-delay="50" data-tooltip="Manage Brand"></i><span class="element-sideNav">Manage Brand</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList"><i class="tooltipped icon icon-manage-category"  data-position="right" data-delay="50" data-tooltip="Manage Category"></i><span class="element-sideNav">Manage Category</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductList"><i class="tooltipped icon icon-manage-product"  data-position="right" data-delay="50" data-tooltip="Manage Product"></i><span class="element-sideNav">Manage Product</span></a></li>
                            </ul>
                        </div>
                    </li>
	                <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList" class="collapsible-header"><span class="element-sideNav">HRM</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="HRM" aria-hidden="true">work</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameList" class="collapsible-header"><span class="element-sideNav">Business List</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Business list" aria-hidden="true">business_center</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><span class="element-sideNav">Manage Inventory</span><i class="icon icon-inventory-details tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true"></i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><span class="element-sideNav">Manage Expense</span><i class="icon icon-manage-expenses tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true"></i></a></li>
                	<li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">dvr</i><span class="element-sideNav">Counter Report</span></a></li>
                            	 <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="icon icon-payment-report tooltipped" data-position="right" data-delay="50" data-tooltip="Payment Report"></i><span class="element-sideNav">Payment Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="icon icon-bad-debts tooltipped" data-position="right" data-delay="50" data-tooltip="Bad Debts Report"></i><span class="element-sideNav">Bad Debts Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i><span class="element-sideNav">Cheque Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth&topProductNo=0"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="icon icon-business-report tooltipped" data-position="right" data-delay="50" data-tooltip="Business Report"></i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report" style="font-weight:bold"></i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report"></i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="icon icon-return-item tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report"></i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="icon icon-permanent-return tooltipped" data-position="right" data-delay="50" data-tooltip="Permanent return"></i><span class="element-sideNav">Permanent return</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="icon icon-counter-return tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Return"></i><span class="element-sideNav">Counter Return</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/fetchFilteredReplacementOrderReportForWeb?range=today"><i class="icon icon-replacement-report tooltipped" data-position="right" data-delay="50" data-tooltip="Replacement Report" style="font-weight:bold;"></i><span class="element-sideNav">Replacement Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="icon icon-damage-report" data-position="right" data-delay="50" data-tooltip="Damage Report"></i><span class="element-sideNav">Damage Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="icon icon-collection-report tooltipped" data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true"></i><span class="element-sideNav">Collection Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true"></i><span class="element-sideNav">Sales Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/employeeCommission?monthId=9&yearId=2018"><i class="icon icon-manage-expenses tooltipped" data-position="right" data-delay="50" data-tooltip="Employee Commission" aria-hidden="true"></i><span class="element-sideNav">Employee Commission</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true"></i><span class="element-sideNav">Cancel Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="text-align:center"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Profit & Loss">show_chart</i><span class="element-sideNav">Profit & Loss</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchPaymentPendingList" class="collapsible-header"><i class="tooltipped icon icon-payment" data-position="right" data-delay="50" data-tooltip="Manage Payment" aria-hidden="true"></i><span class="element-sideNav">Payment</span></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth" class="collapsible-header "><span class="element-sideNav">Ledger</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Ledger" aria-hidden="true">assessment</i></a></li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header"><span class="element-sideNav">Complains</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header"><span class="element-sideNav">Get Invoice</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">person_pin_circle</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>    
    <!--navbar end-->
</c:if>
<!-- navbar for company admin end -->    	

<!-- navbar for gatekeeper start -->    
<c:if test="${sessionScope.loginType=='GateKeeper'}">
<!-- fixed side nav on all page -->

    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<li><a href="${pageContext.servletContext.contextPath}/openCounter" class="collapsible-header"><span class="element-sideNav">Open Counter</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Open Counter" aria-hidden="true">dvr</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><span class="element-sideNav">Manage Inventory</span><i class="icon icon-inventory-details tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true"></i></a></li>
                	
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header"><span class="element-sideNav">Employee Details</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0"><span class="element-sideNav">Booked Order List</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Booked Order List" aria-hidden="true">format_list_bulleted</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header"><span class="element-sideNav">Get Invoice</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><span class="element-sideNav">Manage Expense</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>   
                	<%-- <li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="icon icon-business-report tooltipped" data-position="right" data-delay="50" data-tooltip="Business Report"></i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report" style="font-weight:bold"></i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report"></i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="icon icon-return-item tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><i class="icon icon-collection-report tooltipped" data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true"></i><span class="element-sideNav">Collection Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><span class="element-sideNav"><i class="icon icon-sales-report  tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i>Sales Report</span></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><span class="element-sideNav">Cancel Report</span><i class="icon icon-cancel-report tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today"><span class="element-sideNav">DeliveryBoy Return</span><i class="icon icon-delivery-boy-rreturn tooltipped" data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true"></i></a></li>
		                		<% Calendar cal=Calendar.getInstance();%>	
		                		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>"><span class="element-sideNav">Damage Recovery</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="text-align:center"></i></a></li>
                            </ul>
                        </div>
                    </li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today"><span class="element-sideNav">DeliveryBoy Return</span><i class="icon icon-delivery-boy-rreturn tooltipped" data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true"></i></a></li>
              		
              		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>"><span class="element-sideNav">Damage Recovery</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header"><span class="element-sideNav">Complains</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li> --%>
                    <li>
                        <a class="collapsible-header"><span class="element-sideNav">Reports <i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports">equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>                            
                                 <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">dvr</i><span class="element-sideNav">Counter Report</span></a></li>
                                  <li><a href="${pageContext.servletContext.contextPath}/proformaOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Pro-forma Order Report">library_books</i><span class="element-sideNav">Pro-Forma Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessBadDebtsForReport?year=<%=(new SimpleDateFormat("yyyy")).format(cal.getTime())%>"><i class="icon icon-bad-debts tooltipped" data-position="right" data-delay="50" data-tooltip="Bad Debts Report"></i><span class="element-sideNav">Bad Debts Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="icon icon-inventory-report  tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report" style="font-weight:bold"></i><span class="element-sideNav">Inventory Report</span></a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="icon icon-order-list tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report"></i><span class="element-sideNav">Order Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="icon icon-sales-report  tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report"></i><span class="element-sideNav">Sales Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="icon icon-cancel-report tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report"></i><span class="element-sideNav">Cancel Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderReportForWeb"><i class="icon icon-return-item tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report"></i><span class="element-sideNav">Return Item Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/permanentReturnOrderReport?range=currentMonth"><i class="icon icon-permanent-return  tooltipped" data-position="right" data-delay="50" data-tooltip="Permanent return"></i><span class="element-sideNav">Permanent return</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth"><i class="icon icon-counter-return  tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Return"></i><span class="element-sideNav">Counter Return</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReplacementOrderReportForWeb"><i class="icon icon-replacement-report tooltipped" data-position="right" data-delay="50" data-tooltip="Replacement Report" style="font-weight:bold;"></i><span class="element-sideNav">Replacement Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="icon icon-damage-report" data-position="right" data-delay="50" data-tooltip="Damage Report"></i><span class="element-sideNav">Damage Report</span></a></li>                                
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">person_pin_circle</i></a></li>
                </ul>
            </li>
        </ul>
    </div>
    
    <!--navbar end-->
</c:if>
<!-- navbar for gatekeeper stop -->
<!-- navbar for large screen stop -->
    <!-- code for loader -->
   
	

    <div class="preloader-background">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
         
        </div>
      </div>
      
    </div>
    <!--  <div class="red-text"><br><h6>Wait.....</h6></div> -->
     </div>
     
     <style>
     .loginDetails{
     	right:5% !important;
     	width:300px !important;
     	top:54px !important;
     }
     .loginDetails li a{
     	line-height:45px;
     }
     .loginDetails a{
     	color:#000 !important;
     }
     </style>