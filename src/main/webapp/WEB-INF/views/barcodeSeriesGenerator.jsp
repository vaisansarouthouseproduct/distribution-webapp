<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="resources/js/jquery-barcode.js"></script>
    <script type="text/javascript" src="resources/js/barcode.js"></script>
    
    <script>
		var myContextPath = "${pageContext.request.contextPath}"
		
		
		function printBarcode(){
			$('.modal-content').empty();
			 var startValue = $("#startValue").val();
	            var lastValue = $("#endValue").val();
	            var increamentBy=$("#increament").val();
	            if(startValue==""){
	            	var $toastContent = $('<span>Please Enter the Start Value<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else if(lastValue==""){
	            	var $toastContent = $('<span>Please Enter the Last value<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else if(increamentBy==""){
	            	var $toastContent = $('<span>Please Enter Increment Number<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else{
	            	callGeneratebarCode();
	    			//location.reload(true);
	                //var modalData =document.getElementById("modal-barcode-id").outerHTML;
	                //$('#barcodeImageModal').modal('close');
	                
		 	      	var modalData =$('.modal-content').html();

	                var printWindow = window.open('', '', 'height=700,width=1000');
		            var content= '<html><head><title>Barcode Print</title>';
		            content += '</head><body><div>'
		            content += modalData;
		            content +="</br></br></br><button onclick='window.print();'>Print</button>"
		            content += '</div></body></html>';
		            printWindow.document.write(content);

		            printWindow.document.close();
		            location.reload(true)
		            setTimeout(function(){
			            //printWindow.print();
		            	//openPrintWindow();
		            	//location.reload(true);
		            },1000); 
	            }
			
		}
	
		function callGeneratebarCode() {
	             var startValue = $("#startValue").val();
	            var lastValue = $("#endValue").val();
	            var increamentBy=$("#increament").val();
	            /*
	            if(startValue==""){
	            	var $toastContent = $('<span>Please Enter the Start Value<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else if(lastValue==""){
	            	var $toastContent = $('<span>Please Enter the Last value<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else if(increamentBy==""){
	            	var $toastContent = $('<span>Please Enter Increament Number<span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent,2000);
	            }else{ */
	            	
	            	//$('#barcodeImageModal').modal('open');
	            	 var t = 0;
	            	 var colCount=4;
	 	            for (var j = parseInt(startValue); j <= parseInt(lastValue); j=parseInt(j)+parseInt(increamentBy)) {

	 	                var value = j;
	 	                var btype = 'code128';
	 	                var renderer = 'svg';
	 	                //value = value.toString();
	 	                generateSingleBarcode(value, btype, renderer, t);
	 	                t++;
	 	            }
	 	            
	 	            
	 	           barcodeDataset();
	 	            
	 	           //$('#barcodeImageModal').modal('open');
	            //}

	           
	            /* barcodeImageModelOpen(); */
	        }	


		
		$(document).ready(function() {
			
			$('#columnCount').change(function(){
				 barcodeDataset();
			});
			$('#maskDetails').click(function(){
				maskModelOpen();
			});
					
			//allowed only numbers 
			$('.onlyNumberAllowed').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
			        event.preventDefault();
			});
			
			
			
				});
		
	</script>
	
	 <style>
        .row{
                width: 100%;
            }

          .trow{
					width:100%;
					height:100%;
					}
            
            .barcodeImage {
			    display: block;
			    padding: 10px;
			    margin-left:20px;
			    margin-bottom: 40px; /* SIMPLY SET THIS PROPERTY AS MUCH AS YOU WANT. This changes the space below barcodeImage */
			    margin-top: 10px;
			    text-align: justify;
			}
            .barcode1
                {
                    display:inline-block;
                }
            @media print{
                body *{
                    visibility: hidden;
                }

                .barcode1 *{
                    visibility: visible;
                }
                #maskDetailsModal{
                 visibility: visible;
                }
            }
        </style>
    
</head>
<body>

<!--navbar start--> <%@include file="components/navbar.jsp" %>     <!--navbar end-->

<main class="paddingBody">
	<br>
        <div class="container">
        	 <div class="row  z-depth-3">
	        	 	<div class="col l12 m12 s12">
	                        <h4 class="center"> Barcode Series Generator </h4>
	                 </div>
                 
                  
	                 <div class="row">
	                 
	                 	  <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="startValue" type="text" class="validate onlyNumberAllowed" name="startValue" required>
		                        <label for="startValue" class="active"><span class="red-text">*&nbsp;</span>Enter Start Value</label>
                   		 </div>
                   		 
                        <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <input id="endValue" type="text" class="validate onlyNumberAllowed" name="endValue"  onblur="lastValueValidation()" required>
	                        <label for="endValue" class="active"><span class="red-text">*&nbsp;</span>Enter Last Value</label>
                    	</div>
                    	
	                 </div>
	                 
	                  <div class="row">
	                 
	                 	  <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="increament" type="text" class="validate onlyNumberAllowed" name="increament"  value="1" required>
		                        <label for="increament" class="active"><span class="red-text">*&nbsp;</span>Increment By</label>
                   		 </div>
                   		 
                        <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <input id="maskId" type="text" class="validate" name="maskId" value="$$" onkeypress="return maskAllowedCharacter(event);" required>
	                        <label for="maskId" class="active"><span class="red-text">*&nbsp;</span>Mask</label>
	                        <span  id="maskDetails">What is Mask?(click me!!)</span>
	                       
                    	</div>
                    	  
                    	
	                 </div>
	                  <div class="row">
	                 
	                 	  <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="columnCount" type="text" class="validate onlyNumberAllowed" name="columnCount"  value="5" maxlength="2" required>
		                        <label for="columnCount" class="active"><span class="red-text">*&nbsp;</span>Enter Number of Barcode(Column) per Row</label>
                   		 </div>
                   		
                     
                    	
	                 </div>
	                 
	                 
	                  <!-- <div class="row">
			                 <div id="submit" class="col s12 m6 l6 push-l1 push-m1">
					      		  <input type="button"  class="btn" onclick="barcodeImageModelOpen();" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generate the barcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
					    	</div>
			    	</div> -->
        	 </div>
        	 
        	 <div class="row  z-depth-3">
        	 
        	 		<div class="col l12 m12 s12">
	                        <h4 class="center"> Set the Barcode format </h4>
	                 </div>
	                 
	                   <div class="row">
	                 
	                 	   <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                 	  <input type="color" id="background-color-picker" value="#FFFFFF" />
		                 	  <span>Background Colour</span>
		                        <!-- <input id="backgroundColor" type="text" class="validate" name="backgroundColor"  value="#FFFFFF" maxlength="7" required> -->
		                        <!-- <label for="backgroundColor" class="active"><span class="red-text">*&nbsp;</span>Enter Background Colour</label> -->
                   		 </div>
                   		 
                         <div class="input-field col s12 m5 l5 push-l1 push-m1">
                         <input type="color" id="bar-color-picker" value="#000000"/>
                         <span>Bar Colour</span>
	                       <!--  <input id="barColour" type="text" class="validate" name="barColour" value="#000000" maxlength="7" required> -->
	                       <!--  <label for="barColour" class="active"><span class="red-text">*&nbsp;</span>Enter Bar colour</label> -->
	                       
                    	</div>
                    	
	                 </div>
	                 
	                  <div class="row">
	                 
	                 	  <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="barWidth" type="text" class="validate onlyNumberAllowed" name="barWidth"  value="1" maxlength="2" required>
		                        <label for="barWidth" class="active"><span class="red-text">*&nbsp;</span>Enter Bar width</label>
                   		 </div>
                   		 
                        <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <input id="barHeight" type="text" class="validate onlyNumberAllowed" name="barHeight" value="50" maxlength="3" required>
	                        <label for="barHeight" class="active"><span class="red-text">*&nbsp;</span>Enter Bar Height</label>
	                       
                    	</div>
                    	
	                 </div>
	                 
	                 
	                 <div class="row">
	                <!--   <div id="submit" class="col s5 m5 l5 push-l1 push-m1">
					      		  <input type="button"  class="btn" onclick="barcodeImageModelOpen();" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generate the barcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
					    	</div> -->
					     <div  class="col s11 m11 l11 center">
					      		  <input type="button"  class="btn" onclick="printBarcode();" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generate & Print&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
					    	</div>
	                 </div>
        	 </div>
        
        </div>
        
        
        <!-- Barcode Image modal start -->
        <!-- PreView Model Start  --> <!-- Modal Structure for Preview -->
	<%-- <form action="${pageContext.request.contextPath}/savePrintedChequeFromCB"
		method="post" class="col s12 l12 m12" id="savePrintModal1"> --%>
		<div id="barcodeImageModal" class="modal">
		<h5 class="center"><u>Barcode Images</u> <i class="material-icons modal-close right">clear</i></h5>
		 <div class="row">
	                 
	                 	  <!-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="columnCount" type="text" class="validate onlyNumberAllowed" name="columnCount"  value="5" maxlength="2" required>
		                        <label for="columnCount" class="active"><span class="red-text">*&nbsp;</span>Enter Number Column per Row</label>
                   		 </div> -->
                   		  <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <input id="columnAndRowSpaceAdjust" type="text" class="validate onlyNumberAllowed" name="columnAndRowSpaceAdjust"  value="100" maxlength="3" required>
		                        <label for="columnAndRowSpaceAdjust" class="active"><span class="red-text">*&nbsp;</span>Enter Column & Row Space in % only</label>
                   		 </div>
                   		 
                     
                    	
	                 </div>
			<div class="modal-content" id="modal-barcode-id">
				
			</div>

			<div class="modal-footer row">
				<div class="col s12 l12 m12  center">
					<button id="printBtn" type="button" class="btn" onclick="printBarcode()">Print</button>
				</div>

			</div>





		</div>
        
        <!-- Barcode Image modal End -->
        
        <!-- Mask Details modal start -->

<div id="maskDetailsModal" class="modal">

			
			<div class="center">
				<span class="underline "><b>Mask Details:</b></span>
			</div>

	                      <div class="input-field col s12">&nbsp; &nbsp; &nbsp; &nbsp;	The Mask field specify  how the generated serial numbers shall look like. Each character in a mask pattern
	                      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; corresponds to a character in the serial number.
	                      <br>
	                      <br>
	                      &nbsp; &nbsp; &nbsp;&nbsp;You can use 2 different place holders:
	                      <br>
	                      <br>
	                      <br>
	                      <b>&nbsp;&nbsp;&nbsp;&nbsp;$..-></b>The Serial number filled up with a <b>leading Zeros</b>.
	                      <br>
	                      <b>&nbsp; &nbsp;&nbsp;&nbsp;*..-></b>The Serial number filled up with a <b>leading asterisk(*)</b>.
	                      <br>
	                     </div>
	                     
	                   
	                 
</div>

<!-- Mask Details modal end -->
</main>
	
</body>
</html>