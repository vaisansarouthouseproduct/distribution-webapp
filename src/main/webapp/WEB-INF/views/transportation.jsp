<!DOCTYPE html>
<html lang="en">

<head>
	<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	--%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<head>
		<%@include file="components/header_imports.jsp"%>
		<script>
			$(document).ready(function () {
				$('.num').keypress(function (event) {
					var key = event.which;

					if (!(key >= 48 && key <= 57 || key === 13))
						event.preventDefault();
				});
				$("#editButton").hide();
				var table = $('#tblData').DataTable();
				table.destroy();
				$('#tblData').DataTable({
					"oLanguage": {
						"sLengthMenu": "Show:  _MENU_",
						"sSearch": "_INPUT_" //search
					},
					"autoWidth": false,
					"columnDefs": [
						{ "width": "1%", "targets": 0 },
						{ "width": "12%", "targets": 1 },
						{ "width": "5%", "targets": 2 },
						{ "width": "5%", "targets": 3 },
						{ "width": "4%", "targets": 4 },
						{ "width": "4%", "targets": 5 }

					],
					lengthMenu: [
						[10, 25., 50, -1],
						['10 ', '25 ', '50 ', 'All']
					],

					/* dom: 'lBfrtip', */
					dom: '<lBfr<"scrollDivTable"t>ip>',
					buttons: {
						buttons: [
							//      {
							//      extend: 'pageLength',
							//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
							//  }, 
							{
								extend: 'pdf',
								className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
								text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
								//title of the page
								title: function () {
									var name = $(".heading").text();
									return name
								},
								//file name 
								filename: function () {
									var d = new Date();
									var date = d.getDate();
									var month = d.getMonth();
									var year = d.getFullYear();
									var name = $(".heading").text();
									return name + date + '-' + month + '-' + year;
								},
								//  exports only dataColumn
								exportOptions: {
									columns: ':visible.print-col'
								},
								customize: function (doc, config) {
									doc.content.forEach(function (item) {
										if (item.table) {
											item.table.widths = [40, '*', '*', '*']
										}
									})
									var tableNode;
									for (i = 0; i < doc.content.length; ++i) {
										if (doc.content[i].table !== undefined) {
											tableNode = doc.content[i];
											break;
										}
									}

									var rowIndex = 0;
									/*  var tableColumnCount = tableNode.table.body[rowIndex].length;
									  
									 if(tableColumnCount > 6){
									   doc.pageOrientation = 'landscape';
									 } */
									/*for customize the pdf content*/
									doc.content[1].table.alignment = 'center';
									doc.pageMargins = [5, 20, 10, 5];

									doc.defaultStyle.fontSize = 8;
									doc.styles.title.fontSize = 12;
									doc.styles.tableHeader.fontSize = 11;
									doc.styles.tableFooter.fontSize = 11;
									doc.styles.tableHeader.alignment = 'center';
									doc.styles.tableBodyEven.alignment = 'center';
									doc.styles.tableBodyOdd.alignment = 'center';

								},


							},
							{
								extend: 'excel',
								className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
								text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
								//title of the page
								title: function () {
									var name = $(".heading").text();
									return name
								},
								//file name 
								filename: function () {
									var d = new Date();
									var date = d.getDate();
									var month = d.getMonth();
									var year = d.getFullYear();
									var name = $(".heading").text();
									return name + date + '-' + month + '-' + year;
								},
								//  exports only dataColumn
								exportOptions: {
									columns: ':visible.print-col'
								},
							},
							{
								extend: 'print',
								className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
								text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
								//title of the page
								title: function () {
									var name = $(".heading").text();
									return name
								},
								//file name 
								filename: function () {
									var d = new Date();
									var date = d.getDate();
									var month = d.getMonth();
									var year = d.getFullYear();
									var name = $(".heading").text();
									return name + date + '-' + month + '-' + year;
								},
								//  exports only dataColumn
								exportOptions: {
									columns: ':visible.print-col'
								},
							},
							{
								extend: 'colvis',
								className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
								text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
								collectionLayout: 'fixed two-column',
								align: 'left'
							},
						]
					}

				});
				$('select').material_select();
				$("select")
					.change(function () {
						var t = this;
						var content = $(this).siblings('ul').detach();
						setTimeout(function () {
							$(t).parent().append(content);
							$("select").material_select();
						}, 200);
					});
				$('.dataTables_filter input').attr("placeholder", "Search");


				if ($('#tblData tbody tr').children('td').length > 2) {
					$("#tblData tbody tr td:first-child").each(
						function (index) {
							$(this).text(index + 1);
						});
				}
				else {

				}
			});
			/* save transportation details */
			function addForm(data) {
				var form = $(data).closest('form');
				var datas = $(form).find('input');
				var gstNo = $(datas).eq(1).val().trim();
				var mobileNo = $(datas).eq(2).val().trim();
				if ($(datas).eq(0).val() == "" || $(datas).eq(1).val() == "" || $(datas).eq(2).val() == "" || $(datas).eq(3).val() == "") {
					Materialize.Toast.removeAll();
					Materialize.toast("All fields are mandatory",
						'3000', 'toastAlign');
					return false;
				}
				else if (gstNo.length < 15) {
					Materialize.Toast.removeAll();
					Materialize.toast("Please enter valid GST Number",
						'3000', 'toastAlign');
					return false;
				}
				else if (mobileNo.length < 10) {
					Materialize.Toast.removeAll();
					Materialize.toast("Please enter valid Mobile Number",
						'3000', 'toastAlign');
					return false;
				}

				transportation = {
					transportName: $(datas).eq(0).val(),
					gstNo: $(datas).eq(1).val(),
					mobNo: $(datas).eq(2).val(),
					vehicleNo: $(datas).eq(3).val()
				}

				console.log(transportation);

				$.ajax({
					type: 'POST',
					url: "${pageContext.request.contextPath}/addTransportation",
					headers: {
						'Content-Type': 'application/json'
					},
					data: JSON.stringify(transportation),
					success: function (resultData) {
						if (resultData.success == true) {
							console.log(resultData);
							Materialize.Toast.removeAll();
							Materialize.toast(resultData.msg,
								'3000', 'toastAlign');
							setTimeout(
								function () {
									window.location.href = "${pageContext.request.contextPath}/transportation"
								}, 500);
						} else {
							Materialize.Toast.removeAll();
							Materialize.toast(resultant.msg, '3000',
								'toastError');
						}
					},
					error: function (err) {
						/*Show Dialog for Server Error*/
						Materialize.Toast.removeAll();
						Materialize.toast('Internal Server Error', '3000',
							'toastError');
					}
				});
			}
			/* rest form */
			function emptyForm(data) {
				var form = $(data).closest('form');
				$(form).find('input').each(function () {
					$(this).val("");
				});
				$("#addButton").show();
				$("#editButton").hide();
			}
			/* on click of edit set transportation details to form */
			function showEditData(data) {
				var uid = $(data).attr("data-id");
				var form = $("#transportForm");
				emptyForm(form);
				$("#addButton").hide();
				$("#editButton").show();
				var formData = $("#transportForm").find('input');
				$(data).closest('tr').each(function (i) {
					var tableData = $(this).find('td');
					for (i = 0; i < 4; i++) {
						var value = $(tableData).eq(i + 1).text();
						value = value.trim();
						$(formData).eq(i).val(value);
					}
					$("#uniqueId").val(uid);
				});
			}
			/* update transportation details */
			function editForm(data) {
				var form = $(data).closest('form');
				var datas = $(form).find('input');
				var gstNo = $(datas).eq(1).val().trim();
				var mobileNo = $(datas).eq(2).val().trim();
				if ($(datas).eq(0).val() == "" || $(datas).eq(1).val() == "" || $(datas).eq(2).val() == "" || $(datas).eq(3).val() == "") {
					Materialize.Toast.removeAll();
					Materialize.toast("All fields are mandatory",
						'3000', 'toastAlign');
						return false;
				}
				else if (gstNo.length < 15) {
					Materialize.Toast.removeAll();
					Materialize.toast("Please enter valid GST Number",
						'3000', 'toastAlign');
						return false;
				}
				else if (mobileNo.length < 10) {
					Materialize.Toast.removeAll();
					Materialize.toast("Please enter valid Mobile Number",
						'3000', 'toastAlign');
						return false;
				}

				transportation = {
					id: $("#uniqueId").val(),
					transportName: $(datas).eq(0).val(),
					gstNo: $(datas).eq(1).val(),
					mobNo: $(datas).eq(2).val(),
					vehicleNo: $(datas).eq(3).val()
				}

				console.log(transportation);

				$.ajax({
					type: 'POST',
					url: "${pageContext.request.contextPath}/updateTransportation",
					headers: {
						'Content-Type': 'application/json'
					},
					data: JSON.stringify(transportation),
					success: function (resultData) {
						if (resultData.success == true) {
							console.log(resultData);
							Materialize.Toast.removeAll();
							Materialize.toast(resultData.msg,
								'3000', 'toastAlign');
							setTimeout(
								function () {
									window.location.href = "${pageContext.request.contextPath}/transportation"
								}, 500);
						} else {
							Materialize.Toast.removeAll();
							Materialize.toast(resultData.msg, '3000',
								'toastError');
						}
					},
					error: function (err) {
						/*Show Dialog for Server Error*/
						Materialize.Toast.removeAll();
						Materialize.toast('Internal Server Error', '3000',
							'toastError');
					}
				});

				$('#mobileNo').keypress(function (event) {
					var key = event.which;

					if (!(key >= 48 && key <= 57 || key === 13))
						event.preventDefault();
				});
			}
		</script>
		<style>
			.containerOfAccountDetails {
				margin: 0 auto;
				width: 95% !important;
			}

			.card-panel p {
				margin: 5px !important;
				font-size: 16px !important;
				color: black;
				/* border:1px solid #9e9e9e; */
			}

			.card-panel {
				padding: 8px !important;
				border-radius: 8px;
			}
		</style>

	</head>

<body>
	<%@include file="components/navbar.jsp" %>
	<main class="paddingBody">
		<br>



		<div class="row noMarginBottom " style="padding: 0 0.75rem">
			<div class="col s12 m12 l12 card-panel hoverable blue-grey lighten-4">
				<form id="transportForm">
					<div class="input-field col s12 m12 l3">
						<input id="name" type="text" class="validate" required placeholder="Transport Name"> <label
							for="name">Transport Name <span class="red-text">*</span></label>
					</div>
					<div class="input-field col s12 m12 l3">
						<input id="gstNo" type="text" class="validate" maxlength="15" minlength="15"
							placeholder="GST Number" required> <label for="gstNo">GST Number <span
								class="red-text">*</span></label>
					</div>
					<div class="input-field col s12 m12 l3">
						<input id="mobileNo" type="text" class="validate num" maxlength="10" minlength="10"
							placeholder="Mobile Number" required> <label for="mobileNo">Mobile Number
							<span class="red-text">*</span>
						</label>
					</div>
					<div class="input-field col s12 m12 l3">
						<input id="veicleNo" type="text" class="validate" placeholder="Vehicle Number" required> <label
							for="veicleNo">Vehicle Number <span class="red-text">*</span></label>
					</div>
					<input type="hidden" id="uniqueId">
					<div class="col s12 m12 l12 center">
						<button onclick="editForm(this);" type="button" id="editButton"
							class="waves-effect waves-light btn" style="margin-right: 3%;">
							<i class="material-icons left">create</i> Update
						</button>
						<button onclick="addForm(this);" type="button" id="addButton"
							class="waves-effect waves-light btn" style="margin-right: 3%;">
							<i class="material-icons left">send</i> Add
						</button>
						<button onclick="emptyForm(this);" type="button" class="waves-effect waves-light btn">
							<i class="material-icons left">refresh</i> Reset
						</button>
					</div>
				</form>
			</div>

		</div>

		<div class="row">
			<div class="col s12 m12 l12">
				<table id="tblData" class="striped bordered highlight centered">
					<thead>
						<tr>
							<th class="print-col">Sr No.</th>
							<th class="print-col">Transport Name</th>
							<th class="print-col">GST Number</th>
							<th class="print-col">Mobile No</th>
							<th class="print-col">Vehicle Number</th>
							<th>Operations</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transportation" items="${transportations}" varStatus="status">
							<tr>
								<td></td>
								<td>
									<c:out value="${transportation.transportName}" />
								</td>
								<td>
									<c:out value="${transportation.gstNo}" />
								</td>
								<td>
									<c:out value="${transportation.mobNo}" />
								</td>
								<td>
									<c:out value="${transportation.vehicleNo}" />
								</td>
								<td><a onclick="showEditData(this)"
										class="waves-effect waves-light iconPadding tooltipped iconHover"
										data-id="${transportation.id}" data-position="left" data-delay="50"
										data-tooltip="Edit"><i class="material-icons">create</i></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</main>
	<%@include file="components/footer.jsp"%>

</body>

</html>