<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

			<html>

			<head>
				<%@include file="components/header_imports.jsp" %>

					<script>
						var checkedId = [];
						var checkedIdPending = [];
						$(document).ready(function () {
							$('#showPaymentCounterStatus').hide();
							//  for no of entries and global search
							$('#tblFullPayment').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": " _INPUT_" //search
								},
								lengthMenu: [
									[10, 25., 50, -1],
									['10 ', '25 ', '50 ', 'all']
								],
								"autoWidth": false,
								"columnDefs": [
									{ 'width': '1%', 'targets': 0 },
									{ 'width': '1%', 'targets': 1 },
									{ 'width': '3%', 'targets': 2 },
									{ 'width': '3%', 'targets': 3 },
									{ 'width': '5%', 'targets': 4 },
									{ 'width': '5%', 'targets': 5 },
									{ 'width': '5%', 'targets': 6 },
									{ 'width': '3%', 'targets': 7 },
									{ 'width': '3%', 'targets': 8 },
									{ 'width': '3%', 'targets': 9 }

								],

								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF</span>',
											//title of the page
											title: 'Full Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = [20, 90, 65, 45, 40, 40, 30, 40, 40, 40, 45]
													}
												})
												/*  var tableNode;
												 for (i = 0; i < doc.content.length; ++i) {
												   if(doc.content[i].table !== undefined){
													 tableNode = doc.content[i];
													 break;
												   }
												 }
							    
												 var rowIndex = 0;
												 var tableColumnCount = tableNode.table.body[rowIndex].length;
												  
												 if(tableColumnCount > 6){
												   doc.pageOrientation = 'landscape';
												 } */
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];

												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										}, {
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL</span>',
											//title of the page
											title: 'Full Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'print',
											className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT</span>',
											//title of the page
											title: 'Full Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY</span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}
							});
							$('#tblPartialPayment').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": " _INPUT_" //search
								},
								lengthMenu: [
									[10, 25., 50, -1],
									['10 ', '25 ', '50 ', 'all']
								],
								autoWidth: false,
								columnDefs: [
									{ 'width': '1%', 'targets': 0 },
									{ 'width': '15%', 'targets': 1 },
									{ 'width': '3%', 'targets': 2 },
									{ 'width': '3%', 'targets': 3 },
									{ 'width': '5%', 'targets': 4 },
									{ 'width': '5%', 'targets': 5 },
									{ 'width': '5%', 'targets': 6 },
									{ 'width': '3%', 'targets': 7 },
									{ 'width': '3%', 'targets': 8 },
									{ 'width': '2%', 'targets': 9 },
									{ 'width': '2%', 'targets': 10 },
									{ 'width': '3%', 'targets': 11 },
									{ 'width': '1%', 'targets': 12 }

								],

								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF</span>',
											//title of the page
											title: 'Partial Payment Report',

											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = [20, 65, 65, 45, 40, 30, 30, 40, 40, 40, 45, 45]
													}
												})
												/*   var tableNode;
												  for (i = 0; i < doc.content.length; ++i) {
													if(doc.content[i].table !== undefined){
													  tableNode = doc.content[i];
													  break;
													}
												  }
								 
												  var rowIndex = 0;
												  var tableColumnCount = tableNode.table.body[rowIndex].length;
												   
												  if(tableColumnCount > 6){
													doc.pageOrientation = 'landscape';
												  } */
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];
												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										}, {
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL</span>',
											//title of the page
											title: 'Partial Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'print',
											className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT</span>',
											//title of the page
											title: 'Partial Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY</span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}

							});

							$('#tblPendingPayment').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": " _INPUT_" //search
								},
								lengthMenu: [
									[10, 25., 50, -1],
									['10 ', '25 ', '50 ', 'all']
								],
								autoWidth: false,
								columnDefs: [
									{ 'width': '1%', 'targets': 0 },
									{ 'width': '10%', 'targets': 1 },
									{ 'width': '2%', 'targets': 2 },
									{ 'width': '3%', 'targets': 3 },
									{ 'width': '5%', 'targets': 4 },
									{ 'width': '5%', 'targets': 5 },
									{ 'width': '5%', 'targets': 6 },
									{ 'width': '3%', 'targets': 7 },
									{ 'width': '3%', 'targets': 8 },
									{ 'width': '1%', 'targets': 9 }
								],

								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF</span>',
											//title of the page
											title: 'Pending Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = [30, 120, 65, 50, 60, 50, 45, 45, 50]
													}
												})
												/*  var tableNode;
												 for (i = 0; i < doc.content.length; ++i) {
												   if(doc.content[i].table !== undefined){
													 tableNode = doc.content[i];
													 break;
												   }
												 }
							    
												 var rowIndex = 0;
												 var tableColumnCount = tableNode.table.body[rowIndex].length;
												  
												 if(tableColumnCount > 6){
												   doc.pageOrientation = 'landscape';
												 } */
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];

												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										}, {
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL</span>',
											//title of the page
											title: 'Pending Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'print',
											className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT</span>',
											//title of the page
											title: 'Pending Payment Report',
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading h4").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										}, {
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY</span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}
							});
							$("select").change(function () {
								var t = this;
								var content = $(this).siblings('ul').detach();
								setTimeout(function () {
									$(t).parent().append(content);
									$("select").material_select();
								}, 200);
							});
							$('select').material_select();
							$('.dataTables_filter input').attr("placeholder", "Search");
							if ($.data.isMobile) {
								var table = $('#tblFullPayment').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
								var table1 = $('#tblPartialPayment').DataTable(); // note the capital D to get the API instance
								var column1 = table1.columns('.hideOnSmall');
								column1.visible(false);
								var table2 = $('#tblPendingPayment').DataTable(); // note the capital D to get the API instance
								var column2 = table2.columns('.hideOnSmall');
								column2.visible(false);
							}
							$('ul.tabs').tabs();
							$('.modal').modal();

							/* partial Amount SMS */

							$("#partialCheckAll").click(function () {
								var colcount = $('#tblPartialPayment').DataTable().columns().header().length;
								var cells = $('#tblPartialPayment').DataTable().column(colcount - 1).nodes(), // Cells from 1st column
									state = this.checked;
								//alert(state);
								for (var i = 0; i < cells.length; i += 1) {
									cells[i].querySelector("input[type='checkbox']").checked = state;
								}

							});

							$("input:checkbox").change(function (a) {

								var colcount = $('#tblPartialPayment').DataTable().columns().header().length;
								var cells = $('#tblPartialPayment').DataTable().column(colcount - 1).nodes(), // Cells from 1st column

									state = false;
								var ch = false;

								for (var i = 0; i < cells.length; i += 1) {
									//alert(cells[i].querySelector("input[type='checkbox']").checked);

									if (cells[i].querySelector("input[type='checkbox']").checked == state) {
										$("#partialCheckAll").prop('checked', false);
										ch = true;
									}
								}
								if (ch == false) {
									$("#partialCheckAll").prop('checked', true);
								}

								//alert($('#tblData').DataTable().rows().count());
							});
							$('.mobileDiv').hide();
							$('#sendPartialSmsid').click(function () {

								$('#mobileEditSms').prop('checked', false);
								$("#mobileEditSms").change();
								mobileNumberCollection = '';

								$('.mobileDiv').hide();
								var count = parseInt('${count}')
								checkedId = [];
								//chb
								var idarray = $("#tblDataPartial")
									.find("input[type=checkbox]")
									.map(function () { return this.id; })
									.get();
								var j = 0;
								var mobileNumberCollection = "";
								for (var i = 0; i < idarray.length; i++) {
									idarray[i] = idarray[i].replace('partialSms', '');
									if ($('#partialSms' + idarray[i]).is(':checked')) {
										checkedId[j] = idarray[i].split("_")[0];
										mobileNumberCollection = mobileNumberCollection + idarray[i].split("_")[1] + ",";
										j++;
									}
								}
								mobileNumberCollection = mobileNumberCollection.substring(0, mobileNumberCollection.length - 1);
								$('#mobileNoSmsPartial').val(mobileNumberCollection);
								$('#mobileNoSmsPartial').change();
								$('#smsPartialText').val('');
								$('#smsSendPartialMesssage').html('');
								//alert(checkedId);
								//return false;
								if (checkedId.length == 0) {
									$('#addeditmsg').find("#modalType").addClass("warning");
									$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
									$('#addeditmsg').modal('open');
									//$('#msgHead').text("Message : ");
									$('#msg').text("Select Orders For Send Message");
								}
								else {
									if (checkedId.length == 1) {
										$('#smsPartialText').val('');
										$('#smsSendPartialMesssage').html('');
										$('.mobileDiv').show();
										$('#sendMsgPartialAmt').modal('open');
									}
									else {

										$('#smsPartialText').val('');
										$('#smsSendPartialMesssage').html('');
										$('#sendMsgPartialAmt').modal('open');
									}
								}
							});

							$('#sendPartialMessageId').click(function () {

								$('#smsSendPartialMesssage').html('');

								var smsText = $("#smsText").val();
								if (checkedId.length == 0) {
									$('#smsSendPartialMesssage').html("<font color='red'>Select Employee For Send Message</font>");
									return false;
								}
								if (smsText === "") {
									$('#smsSendPartialMesssage').html("<font color='red'>Enter Message Text</font>");
									return false;
								}

								var form = $('#sendPartialSMSForm');
								//alert(form.serialize()+"&employeeDetailsId="+checkedId);

								$.ajax({
									type: form.attr('method'),
									url: form.attr('action'),
									data: form.serialize() + "&shopsIds=" + checkedId + "&mobileNumber=" + $('#mobileNoSmsPartial').val(),
									//async: false,
									success: function (data) {
										if (data === "Success") {
											$('#smsSendPartialMesssage').html("<font color='green'>Message sent SuccessFully</font>");
											$('#smsPartialText').val('');
										}
										else {
											$('#smsSendPartialMesssage').html("<font color='red'>Message sending Failed</font>");
										}
									}
								});
							});

							/* pending Amount SMS */

							$("#pendingCheckAll").click(function () {
								var colcount = $('#tblPendingPayment').DataTable().columns().header().length;
								var cells = $('#tblPendingPayment').DataTable().column(colcount - 1).nodes(), // Cells from 1st column
									state = this.checked;
								//alert(state);
								for (var i = 0; i < cells.length; i += 1) {
									cells[i].querySelector("input[type='checkbox']").checked = state;
								}

							});

							$("input:checkbox").change(function (a) {

								var colcount = $('#tblPendingPayment').DataTable().columns().header().length;
								var cells = $('#tblPendingPayment').DataTable().column(colcount - 1).nodes(), // Cells from 1st column

									state = false;
								var ch = false;

								for (var i = 0; i < cells.length; i += 1) {
									//alert(cells[i].querySelector("input[type='checkbox']").checked);

									if (cells[i].querySelector("input[type='checkbox']").checked == state) {
										$("#pendingCheckAll").prop('checked', false);
										ch = true;
									}
								}
								if (ch == false) {
									$("#pendingCheckAll").prop('checked', true);
								}

								//alert($('#tblData').DataTable().rows().count());
							});

							$('#sendPendingSmsid').click(function () {

								$('#mobileEditSmsPending').prop('checked', false);
								$("#mobileEditSmsPending").change();
								mobileNumberCollection = '';

								$('.mobileDiv').hide();
								var count = parseInt('${count}')
								checkedIdPending = [];
								//chb
								var idarray = $("#tblDataPending")
									.find("input[type=checkbox]")
									.map(function () { return this.id; })
									.get();
								var j = 0;
								var mobileNumberCollection = "";
								for (var i = 0; i < idarray.length; i++) {
									idarray[i] = idarray[i].replace('pendingSms', '');
									if ($('#pendingSms' + idarray[i]).is(':checked')) {
										checkedIdPending[j] = idarray[i].split("_")[0];
										mobileNumberCollection = mobileNumberCollection + idarray[i].split("_")[1] + ",";
										j++;
									}
								}
								mobileNumberCollection = mobileNumberCollection.substring(0, mobileNumberCollection.length - 1);
								$('#mobileNoSmsPending').val(mobileNumberCollection);
								$('#mobileNoSmsPending').change();
								$('#smsPendingText').val('');
								$('#smsSendPendingMesssage').html('');
								//alert(checkedId);
								//return false;
								if (checkedIdPending.length == 0) {
									$('#addeditmsg').find("#modalType").addClass("warning");
									$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
									$('#addeditmsg').modal('open');
									// $('#msgHead').text("Message : ");
									$('#msg').text("Select Orders For Send Message");
								}
								else {
									if (checkedId.length == 1) {
										$('#smsPendingText').val('');
										$('#smsSendPendingMesssage').html('');
										$('.mobileDiv').show();
										$('#sendMsgPendingAmt').modal('open');
									}

									else {
										/*  mobileNumberCollection=mobileNumberCollection.substring(0,mobileNumberCollection.length-1);
										 $('#mobileNoSmsPending').val(mobileNumberCollection);
										 $('#mobileNoSmsPending').change(); */
										$('#smsPendingText').val('');
										$('#smsSendPendingMesssage').html('');
										$('#sendMsgPendingAmt').modal('open');
									}
								}
							});

							$('#sendPendingMessageId').click(function () {


								$('#smsSendPendingMesssage').html('');

								var smsText = $("#smsText").val();
								if (checkedIdPending.length == 0) {
									$('#smsSendPendingMesssage').html("<font color='red'>Select Employee For Send Message</font>");
									return false;
								}
								if (smsText === "") {
									$('#smsSendPendingMesssage').html("<font color='red'>Enter Message Text</font>");
									return false;
								}

								var form = $('#sendPendingSMSForm');
								//alert(form.serialize()+"&employeeDetailsId="+checkedId);

								$.ajax({
									type: form.attr('method'),
									url: form.attr('action'),
									data: form.serialize() + "&shopsIds=" + checkedIdPending + "&mobileNumber=" + $('#mobileNoSmsPending').val(),
									//async: false,
									success: function (data) {
										if (data === "Success") {
											$('#smsSendPendingMesssage').html("<font color='green'>Message sent SuccessFully</font>");
											$('#smsPendingText').val('');
										}
										else {
											$('#smsSendPendingMesssage').html("<font color='red'>Message sending Failed</font>");
										}
									}
								});
							});

							$(".showDates").hide();
							$("#oneDateDiv").hide();
							$(".topProduct").click(function () {
								$(".showDates").hide();
								$("#oneDateDiv").hide();
							});

							$(".rangeSelect").click(function () {
								$("#oneDateDiv").hide();
								$(".showDates").show();
							});

							$(".pickdate").click(function () {
								$(".showDates").hide();
								$("#oneDateDiv").show();
							});
							$("#mobileEditSms").change(function () {
								if (this.checked) {
									$("#mobileNoSmsPartial").removeAttr('readonly', 'readonly');
								}
								else {
									$("#mobileNoSmsPartial").attr('readonly', 'readonly');

								}
							});
							$("#mobileEditSmsPending").change(function () {
								if (this.checked) {
									$("#mobileNoSmsPending").removeAttr('readonly', 'readonly');
								}
								else {
									$("#mobileNoSmsPending").attr('readonly', 'readonly');

								}
							});

						});

						// payament details list by order id (salesman or counter order id)
						function fetchPaymentDetails(orderId) {
							if (orderId.includes("CORD")) {
								$.ajax({
									type: 'GET',
									url: "${pageContext.servletContext.contextPath}/fetchPaymentCounterOrder?counterOrderId=" + orderId,
									async: false,
									success: function (data) {
										$('#paymentDetail').empty();

										var totalAmountPaid = 0;
										for (var i = 0; i < data.length; i++) {
											totalAmountPaid += data[i].currentAmountPaid.toFixedVSS(2) - data[i].currentAmountRefund.toFixedVSS(2)
										}

										if (data[0].isBusinessHave == false) {
											$('#boothNo').html("<b>Customer Name : </b>" + data[0].customerName);
										} else {
											$('#boothNo').html("<b>Booth No. : </b>" + data[0].businessNameId);
										}

										$('#orderId').html("<b>Order Id. : </b>" + data[0].counterOrderId);
										$('#totalAmt').html("<b>Total Amount : </b>" + data[0].totalAmountWithTax);
										$('#balanceAmt').html("<b>Balance Amount : </b>" + (parseFloat(data[0].totalAmountWithTax) - parseFloat(totalAmountPaid)));

										var totalAmountPaid = 0;
										var srno = 1;
										$('#showPaymentCounterStatus').show();
										for (var i = 0; i < data.length; i++) {
											var mode = "";
											if (data[i].payType === "Cash") {
												mode = "Cash";
											}
											else if (data[i].payType === "Cheque") {
												mode = data[i].bankName + "-" + data[i].chequeNumber;
											} else {
												mode = data[i].paymentMethodName + "-" + data[i].transactionRefNo;
											}

											var dateFrom = new Date(parseInt(data[i].paidDate));
											month = dateFrom.getMonth() + 1;
											day = dateFrom.getDate();
											year = dateFrom.getFullYear();
											var dateFromString = day + "-" + month + "-" + year;

											if (data[i].currentAmountRefund == '' || data[i].currentAmountRefund == undefined) {
												$('#paymentDetail').append('<tr>' +
													'<td>' + srno + '</td>' +
													'<td>' + data[i].currentAmountPaid.toFixedVSS(2) + '</td>' +
													'<td><font color="blue"><b>Paid</b></font></td>' +
													'<td>' + mode + '</td>' +
													'<td class="hideColumnOnSmall">' + data[i].employeeName + '</td>' +
													'<td>' + dateFromString + '</td></tr>');
												totalAmountPaid = parseFloat(totalAmountPaid) + parseFloat(data[i].currentAmountPaid.toFixedVSS(2));
											} else {
												$('#paymentDetail').append('<tr>' +
													'<td>' + srno + '</td>' +
													'<td>' + data[i].currentAmountRefund.toFixedVSS(2) + '</td>' +
													'<td><font color="green"><b>Refund</b></font></td>' +
													'<td>' + mode + '</td>' +
													'<td class="hideColumnOnSmall">' + data[i].employeeName + '</td>' +
													'<td>' + dateFromString + '</td></tr>');
												totalAmountPaid = parseFloat(totalAmountPaid) - parseFloat(data[i].currentAmountRefund.toFixedVSS(2));
											}

											srno++;
										}
										$('#totalAmountPaid').text(totalAmountPaid);
										$('#viewPaymentDetail').modal('open');
									},
									error: function (xhr, status, error) {
										Materialize.Toast.removeAll();
										Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
										/* $('#addeditmsg').modal('open');
												  $('#msgHead').text("Message : ");
												  $('#msg').text("Product List Not Found"); 
														setTimeout(function() 
											  {
														$('#addeditmsg').modal('close');
											  }, 1000); */
									}

								});
							} else {
								$('#showPaymentCounterStatus').hide();
								$.ajax({
									type: 'GET',
									url: "${pageContext.servletContext.contextPath}/fetchPaymentListByOrderId?orderId=" + orderId,
									async: false,
									success: function (data) {
										$('#paymentDetail').empty();

										$('#boothNo').html("<b>Booth No. : </b>" + data[0].businessNameId);
										$('#orderId').html("<b>Order Id. : </b>" + data[0].orderId);
										$('#totalAmt').html("<b>Total Amount : </b>" + data[0].totalAmountWithTax);
										$('#balanceAmt').html("<b>Balance Amount : </b>" + (parseFloat(data[0].dueAmount)));

										var totalAmountPaid = 0;
										var srno = 1;
										for (var i = 0; i < data.length; i++) {
											var mode = "";
											if (data[i].payType === "Cash") {
												mode = "Cash";
											}
											else if (data[i].payType === "Cheque") {
												mode = data[i].bankName + "-" + data[i].chequeNumber;
											} else {
												mode = data[i].paymentMethodName + "-" + data[i].transactionRefNo;
											}

											var dateFrom = new Date(parseInt(data[i].paidDate));
											month = dateFrom.getMonth() + 1;
											day = dateFrom.getDate();
											year = dateFrom.getFullYear();
											var dateFromString = day + "-" + month + "-" + year;

											$('#paymentDetail').append('<tr>' +
												'<td>' + srno + '</td>' +
												'<td>' + data[i].paidAmount.toFixedVSS(2) + '</td>' +
												'<td><font color="blue"><b>Paid</b></font></td>' +
												'<td>' + mode + '</td>' +
												'<td class="hideColumnOnSmall">' + data[i].employeeName + '</td>' +
												'<td>' + dateFromString + '</td></tr>');
											totalAmountPaid = parseFloat(totalAmountPaid) + parseFloat(data[i].paidAmount.toFixedVSS(2));
											srno++;
										}
										$('#totalAmountPaid').text(totalAmountPaid);
										$('#viewPaymentDetail').modal('open');
									},
									error: function (xhr, status, error) {
										Materialize.Toast.removeAll();
										Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
										/* $('#addeditmsg').modal('open');
												  $('#msgHead').text("Message : ");
												  $('#msg').text("Product List Not Found"); 
														setTimeout(function() 
											  {
														$('#addeditmsg').modal('close');
											  }, 1000); */
									}

								});
							}

						}

       /*  $("#viewDetails").click(function(){
        	$("#viewPaymentDetail").modal('open');
        }); */
					</script>
					<style>
						.card {
							height: 2.5rem;
							line-height: 2.5rem;
						}

						.card-image {
							width: 50% !important;
							background-color: #0073b7 !important;
						}

						.card-image h6 {
							padding: 5px;
							font-size: 0.9rem !important;
						}

						.card-stacked .card-content {
							padding: 5px;
						}

						/* .row .col{
		   padding: 0 .75rem 0 0;
	} */

						#viewPaymentDetail {
							width: 40%;
						}

						/*  .carousel .carousel-item {
            width: 100%;
        } */

						/* .right{
	padding-right:0 !important;
} */

						.sendMsg {
							width: 40%;
							/* border-radius: 10px; */
							max-height: unset !important;
							height: 350px !important;
							overflow-y: unset !important;

						}

						.sendMsg .fixedDiv {
							position: fixed;
							top: 0;
							right: 0;
							bottom: 0;
							left: 0;
							width: 40%;
							height: 350px;
							/* background: url('resources/img/smsModal1.jpg') center   !important; */
							background: linear-gradient( rgba(48, 54, 65, 0.5),
							rgba(48, 54, 65, 0.5)), url('resources/img/smsModal1.jpg') center !important;
							background-size: cover;
							-webkit-filter: grayscale(100%);
							filter: grayscale(100%);
						}

						.sendMsg .scrollDiv {
							left: 40%;
							position: relative;
							overflow-y: auto;
							height: 300px;
							width: 60%;
							padding: 20px;
						}

						.sendMsg .fixedFooter {
							position: fixed;
							bottom: 5px;
							width: 100%;
							height: 50px;
						}

						.sendMsg .modal-content {
							padding: 0;

						}

						.sendMsg textarea.materialize-textarea {

							padding: .8rem 0 1.6rem 0 !important;
							resize: none;
							min-height: 8rem !important;
						}

						.noPadding {
							padding: 0 !important;
						}

						/* .cardDiv{
	margin-bottom: 5px
} */

						.actionBtn {
							margin-bottom: 25px;
						}

						@media only screen and (max-width: 600px) {
							#viewPaymentDetail {
								width: 90% !important;								
							}
							
						}

						@media only screen and (min-width: 601px) and (max-width: 992px) {
							#viewPaymentDetail {
								width: 60% !important;
								
							}
							.sendMsg {
							width: 60%;
							/* border-radius: 10px; */
							max-height: unset !important;
							height: 350px !important;
							overflow-y: unset !important;

						}
						.actionBtn {
							margin-bottom: 15px;
						}
						}
					</style>


			</head>

			<body>
				<!--navbar start-->
				<%@include file="components/navbar.jsp" %>
					<!--navbar end-->
					<!--content start-->
					<main class="paddingBody">
						<br>




						<div class="row" style="margin-bottom:0">
							<div class="col s12 m12 hide-on-large-only noPadding">
								<div class="col s12 m6">
									<h6 class="">
										<b>Amount to be collected :
											<span class="blueColor-text"> &#8377;
												<span id="amtToBeCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.amountToBePaid}"
													/>
												</span>
											</span>
										</b>
									</h6>
								</div>
								<div class="col s12 m6">
									<h6 class="">
										<b>Pending Amount :
											<span class="blueColor-text"> &#8377;
												<span id="amtPending">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.pendingAmount}"
													/>
												</span>
											</span>
										</b>
									</h6>
								</div>
								<div class="col s12 m6">
									<h6 class="">
										<b>Full Amount collected :
											<span class="blueColor-text"> &#8377;
												<span id="fullAmtCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.fullAmountCollected}"
													/>
												</span>
											</span>
										</b>
									</h6>
								</div>
								<div class="col s12 m6">
									<h6 class="">
										<b>Partial Amount Collected :
											<span class="blueColor-text"> &#8377;
												<span id="partialAmtCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.partialAmountCollected}"
													/>
												</span>
											</span>
										</b>
									</h6>
								</div>
							</div>
							<div class="col s6 m3 l4 right right-align actionDiv actionBtn">
								<!-- Dropdown Trigger -->
								<a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action
									<i class="material-icons right">arrow_drop_down</i>
								</a>
								<!-- Dropdown Structure -->
								<ul id='filter' class='dropdown-content'>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today">Today</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=yesterday">Yesterday</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=last7days">Last 7 days</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=currentMonth">Current Month</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=lastMonth">Last Month</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=last3Months">Last 3 Months</a>
									</li>
									<li>
										<a class="rangeSelect">Range</a>
									</li>
									<li>
										<a class="pickdate">Pick date</a>
									</li>
									<li>
										<a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=viewAll">View All</a>
									</li>
								</ul>
							</div>
							<div class="col s12 l4 m4 left cardDiv hide-on-med-and-down">
								<div class="card horizontal">
									<div class="card-image">
										<h6 class="white-text center">Amount to be collected</h6>
									</div>
									<div class="card-stacked grey lighten-3">
										<div class="card-content">
											<h6 class="">&#8377;
												<span id="amtToBeCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.amountToBePaid}"
													/>
												</span>
											</h6>
										</div>
									</div>

								</div>
							</div>
							<div class="col s12 l4 m4 left cardDiv hide-on-med-and-down">
								<div class="card horizontal">
									<div class="card-image">
										<h6 class="white-text center">Pending Amount</h6>
									</div>
									<div class="card-stacked grey lighten-3">
										<div class="card-content">
											<h6 class="">&#8377;
												<span id="amtPending">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.pendingAmount}"
													/>
												</span>
											</h6>
										</div>
									</div>

								</div>
							</div>
							<div class="col s12 l4 m4 left cardDiv  hide-on-med-and-down">
								<div class="card horizontal">
									<div class="card-image">
										<h6 class="white-text center">Full Amount collected</h6>
									</div>
									<div class="card-stacked grey lighten-3">
										<div class="card-content">
											<h6 class="">&#8377;
												<span id="fullAmtCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.fullAmountCollected}"
													/>
												</span>
											</h6>
										</div>
									</div>

								</div>
							</div>
							<div class="col s12 l4 m4 left cardDiv hide-on-med-and-down">
								<div class="card horizontal">
									<div class="card-image">
										<h6 class="white-text center">Partial Amount Collected</h6>
									</div>
									<div class="card-stacked grey lighten-3">
										<div class="card-content">
											<h6 class="">&#8377;
												<span id="partialAmtCollected">
													<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.partialAmountCollected}"
													/>
												</span>
											</h6>
										</div>
									</div>

								</div>
							</div>
							<div class="col s6 l4 m4  offset-m4 center actionDiv" id="oneDateDiv">
								<form action="${pageContext.request.contextPath}/getCollectionReportDetails" method="post">
									<input type="hidden" name="range" value="pickDate">
									<div class="input-field col s8 m5 l5 offset-l2">
										<input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
										<label for="oneDate" class="black-text">Pick Date</label>
									</div>
									<div class="input-field col s2 m2 l2">
										<button type="submit" class="btn">View</button>

									</div>

								</form>
							</div>
							<div class="col s12 m4 l4 offset-m4 actionDiv">
								<form action="${pageContext.servletContext.contextPath}/getCollectionReportDetails" method="post">
									<input type="hidden" name="range" value="range">
									<span class="showDates">
										<div class="input-field col s5 m5 l5">
											<input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required>
											<label for="startDate">From</label>
										</div>

										<div class="input-field col s5 m5 l5">
											<input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
											<label for="endDate">To</label>
										</div>
										<div class="input-field col s2 m2 l2">
											<button type="submit" class="btn">View</button>
										</div>

									</span>
								</form>

							</div>

						</div>
						<div class="row ">


							<%--  <div class="col s12 l6 m6 card">
                <div class="col s12 l6 m6 ">
                    <h6 id="amtToBeCollected" class="light-blue-text text-darken-4">Amount to be collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.amountToBePaid}" /></h6>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="amtPending" class="light-blue-text text-darken-4">Pending Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.pendingAmount}" /></h6>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="fullAmtCollected" class="light-blue-text text-darken-4">Full Amount collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.fullAmountCollected}" /></h6>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="partialAmtCollected" class="light-blue-text text-darken-4">Partial Amount Collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.partialAmountCollected}" /></h6>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
            </div> --%>



								<div class="col s12 l12 m12">
									<ul id="tabs" class="tabs tabs-fixed-width">
										<li class="tab col s3 l4">
											<a href="#fullPayment">Full Payment</a>
										</li>
										<li class="tab col s3 l4">
											<a href="#partialPayment">Partial Payment</a>
										</li>
										<li class="tab col s3 l4" style="padding-right:0">
											<a href="#pendingPayment">Pending Payment</a>
										</li>

									</ul>
								</div>
						</div>



						<!--Full Payment START-->
						<div id="fullPayment" class="row">


							<div class="col s12 l12 m12 ">
								<table class="striped highlight centered" id="tblFullPayment" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th class="print-col hideOnSmall">Sr. No.</th>
											<th class="print-col">Shop Name /
												<br> Customer Name</th>
											<th class="print-col">Order Id</th>
											<th class="print-col">Mobile No</th>
											<th class="print-col hideOnSmall">Area</th>
											<th class="print-col hideOnSmall">Region</th>
											<th class="print-col hideOnSmall">City</th>
											<th class="print-col">Total Amount</th>
											<th class="print-col">Amount Paid</th>
											<!-- <th class="print-col">Balance Amount</th> -->
											<!-- <th class="print-col">Mode</th> -->
											<th class="print-col hideOnSmall">Date</th>
										</tr>
									</thead>

									<tbody>
										<% int rowincrement=0; %>
											<c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
												<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">
													<c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
													<c:choose>
														<c:when test="${listValue.payStatus == 'Full'}">
															<tr>
																<td>
																	<c:out value="${listValue.srno}" />
																</td>
																<td class="wrapok">
																	<c:out value="${listValue.shopName}" />
																</td>
																<td>
																	<c:out value="${listValue.orderId}" />
																</td>
																<td class="wrapok">
																	<c:out value="${listValue.mobileNumber}" />
																</td>
																<td>
																	<c:out value="${listValue.areaName}" />
																</td>
																<td>
																	<c:out value="${listValue.regionName}" />
																</td>
																<td>
																	<c:out value="${listValue.cityName}" />
																</td>
																<td>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}"
																	/>
																</td>
																<td>
																	<button class="btn-flat modal-trigger" data-target="viewPaymentDetail" id="viewDetails" onclick="fetchPaymentDetails('${listValue.orderId}')">
																		<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}"
																		/>
																	</button>
																</td>
																<!-- <td>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}"
																	/>
																</td> -->
																<%-- <td><c:out value="${listValue.payMode}" /></td> --%>
																	<td class="wrapok">
																		<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.paidDate}" />
																		<c:out value="${date}" /> </td>
															</tr>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:if>
									</tbody>
								</table>
							</div>
						</div>
						<!--Full Payment END-->


						<!--Partial Payment START-->
						<div id="partialPayment" class="row">

							<div class="col s12 l12 m12 ">
								<table class="striped highlight centered mdl-data-table display  select" id="tblPartialPayment" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th class="print-col hideOnSmall">Sr. No.</th>
											<th class="print-col">Shop Name /
												<br> Customer Name</th>
											<th class="print-col">Order Id</th>
											<th class="print-col">Mobile No</th>
											<th class="print-col hideOnSmall">Area</th>
											<th class="print-col hideOnSmall">Region</th>
											<th class="print-col hideOnSmall">City</th>
											<th class="print-col">Total Amount</th>
											<th class="print-col">Amount Paid</th>
											<th class="print-col">Balance Amount</th>
											<th class="print-col">Next Estimated payment Date</th>
											<!-- <th class="print-col">Mode</th> -->
											<th class="print-col hideOnSmall">Date</th>
											<th class="hideOnSmall">
												<a class="modal-trigger cursorPointer" id="sendPartialSmsid" style="margin-left: -30%;">
													<i class="material-icons">mail</i>
												</a>
												<br/>
												<input type="checkbox" class="filled-in" id="partialCheckAll" />
												<label for="partialCheckAll"></label>
											</th>
										</tr>
									</thead>

									<tbody id="tblDataPartial">
										<%rowincrement=0; %>
											<c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
												<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">
													<c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
													<c:choose>
														<c:when test="${listValue.payStatus == 'Partial'}">
															<tr>
																<td>
																	<c:out value="${listValue.srno}" />
																</td>
																<td class="wrapok">
																	<c:out value="${listValue.shopName}" />
																</td>
																<td>
																	<c:out value="${listValue.orderId}" />
																</td>
																<td>
																	<c:out value="${listValue.mobileNumber}" />
																</td>
																<td>
																	<c:out value="${listValue.areaName}" />
																</td>
																<td>
																	<c:out value="${listValue.regionName}" />
																</td>
																<td>
																	<c:out value="${listValue.cityName}" />
																</td>
																<td>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}"
																	/>
																</td>
																<td>
																	<button class="btn-flat modal-trigger" data-target="viewPaymentDetail" id="viewDetails" onclick="fetchPaymentDetails('${listValue.orderId}')">
																		<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}"
																		/>
																	</button>
																</td>
																<td>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}"
																	/>
																</td>
																<td class="wrapok">
																	<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.nextDueDate}" />
																	<c:out value="${date}" />
																</td>
																<%--  <td><c:out value="${listValue.payMode}" /></td> --%>
																	<td class="wrapok">
																		<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.paidDate}" />
																		<c:out value="${date}" />
																	</td>
																	<td>

																		<input type="checkbox" class="filled-in" id="partialSms${listValue.orderId}_${listValue.mobileNumber}" />
																		<label for="partialSms${listValue.orderId}_${listValue.mobileNumber}"></label>

																	</td>

															</tr>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:if>
									</tbody>
								</table>
								<!-- Modal Structure for sendMsg -->
								<form id="sendPartialSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
									<div id="sendMsgPartialAmt" class="modal sendMsg">

										<div class="modal-content">


											<div class="fixedDiv">
												<div class="center-align" style="padding:0">
													<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
												</div>
											</div>

											<div class="scrollDiv">

												<div class="row mobileDiv" style="margin-bottom:0">
													<div class="col s12 l9 m9 input-field" style="padding-left:0">
														<label for="mobileNoSmsPartial" class="black-text" style="left:0">Mobile No</label>
														<input type="text" id="mobileNoSmsPartial" class="grey lighten-3" minlength="10" maxlength="10" readonly>

													</div>
													<div class="col s12 l2 m2 right-align" style="margin-top:10%">
														<input type="checkbox" id="mobileEditSms" />
														<label for="mobileEditSms" class="black-text">Edit</label>

													</div>
												</div>
												<div class="input-field">
													<label for="smsPartialText" class="black-text">Type Message</label>
													<textarea id="smsPartialText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
												</div>
												<div class="input-field center">
													<font color='red'>
														<span id="smsSendMesssage"></span>
													</font>
												</div>
												<div class="fixedFooter row">
													<div class="col s12 m6 l6 center">
														<a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
														<button type="button" id="sendPartialMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
													</div>
													
												</div>
											</div>
											<br>
											<br>
											<br>


										</div>


									</div>


								</form>



								<!-- Modal Structure for sms -->

								<%--                      
                        <div id="sendMsgPartialAmt" class="modal"  style="width:40%;height:60%;">
                        <form id="sendPartialSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
                            <div class="modal-content">
                            
                                <h4 class="center">Message</h4>
                                <hr>
                                 <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                      <label for="smsPartialText">Type Message</label>
                                <textarea id="smsPartialText" class="materialize-textarea" name="smsText" required></textarea>
                                </div> 
                                 <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPartialMesssage"></span></font>
            					</div>                         
                           	 </div>
                                
                             

                            </div>
                          <div class="modal-footer center-align row">
                          <hr>
                            <div class="col s6 m6 l4 offset-l1">
                              <a href="#!" id="sendPartialMessageId" class="modal-action waves-effect btn">Send</a>
                            		
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                            
                        </form>
                        </div> --%>


							</div>
						</div>
						<!--Partial Payment END-->
						<!--Pending payment START-->
						<div id="pendingPayment" class="row">


							<div class="col s12 l12 m12">
								<table class="striped highlight centered mdl-data-table display  select" id="tblPendingPayment" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th class="print-col hideOnSmall">Sr. No.</th>
											<th class="print-col">Shop Name /
												<br> Customer Name</th>
											<th class="print-col">Order Id</th>
											<th class="print-col">Mobile No</th>
											<th class="print-col hideOnSmall">Area</th>
											<th class="print-col hideOnSmall">Region</th>
											<th class="print-col hideOnSmall">City</th>
											<th class="print-col">Amount Due</th>
											<th class="print-col">Estimated payment Date</th>
											<th class="hideOnSmall">
												<a  class="modal-trigger cursorPointer" id="sendPendingSmsid" style="margin-left: -31%;">
													<i class="material-icons">mail</i>
												</a>
												<br/>
												<input type="checkbox" class="filled-in" id="pendingCheckAll" />
												<label for="pendingCheckAll"></label>
											</th>
										</tr>
									</thead>

									<tbody id="tblDataPending">
										<%rowincrement=0; %>
											<c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
												<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">
													<c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
													<c:choose>
														<c:when test="${listValue.payStatus == 'UnPaid'}">
															<tr>
																<td>
																	<c:out value="${listValue.srno}" />
																</td>
																<td class="wrapok">
																	<c:out value="${listValue.shopName}" />
																</td>
																<td>
																	<c:out value="${listValue.orderId}" />
																</td>
																<td>
																	<c:out value="${listValue.mobileNumber}" />
																</td>
																<td>
																	<c:out value="${listValue.areaName}" />
																</td>
																<td>
																	<c:out value="${listValue.regionName}" />
																</td>
																<td>
																	<c:out value="${listValue.cityName}" />
																</td>
																<td>
																	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}"
																	/>
																</td>
																<td class="wrapok">
																	<fmt:formatDate pattern="dd-MM-yyyy" var="date" value="${listValue.nextDueDate}" />
																	<c:out value="${date}" />
																</td>
																<td>

																	<input type="checkbox" class="filled-in" id="pendingSms${listValue.orderId}_${listValue.mobileNumber}" />
																	<label for="pendingSms${listValue.orderId}_${listValue.mobileNumber}"></label>

																</td>

															</tr>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:if>
									</tbody>
								</table>
								<form id="sendPendingSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
									<div id="sendMsgPendingAmt" class="modal sendMsg">

										<div class="modal-content">
											<div class="fixedDiv">
												<div class="center-align" style="padding:0">
													<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
												</div>
											</div>

											<div class="scrollDiv">

												<div class="row mobileDiv" style="margin-bottom:0">
													<div class="col s12 l9 m9 input-field" style="padding-left:0">
														<label for="mobileNoSmsPending" class="black-text" style="left:0">Mobile No</label>
														<input type="text" id="mobileNoSmsPending" class="grey lighten-3" minlength="10" maxlength="10" readonly>

													</div>
													<div class="col s12 l2 m2 right-align" style="margin-top:10%">
														<input type="checkbox" id="mobileEditSmsPending" />
														<label for="mobileEditSmsPending" class="black-text">Edit</label>
													</div>
												</div>
												<div class="input-field">
													<label for="smsPendingText" class="black-text">Type Message</label>
													<textarea id="smsPendingText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
												</div>
												<div class="input-field center">
													<font color='red'>
														<span id="smsSendPendingMesssage"></span>
													</font>
												</div>
												<div class="fixedFooter row" style="margin-bottom:0">
													<div class="col s6 m6 l6 center">
														<a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
														<button type="button" id="sendPendingMessageId" class="modal-action waves-effect btn btn-waves">Send</button>
													</div>
								
												</div>
											</div>
											<br>
											<br>
											<br>


										</div>


									</div>

								</form>


								<!-- Modal Structure for sms -->
								<%-- <div id="sendMsgPendingAmt" class="modal">
                        <form id="sendPendingSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
                            
                            <div class="modal-content">
                               <h4 class="center">Message</h4>
                                <hr>
                                 <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                      <label for="smsPendingText">Type Message</label>
                                <textarea id="smsPendingText" class="materialize-textarea" name="smsText" required></textarea>
                                </div> 
                                 <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPendingMesssage"></span></font>
            					</div>                         
                           	 </div>
                            </div>
                            <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPendingMesssage"></span></font>
            					</div>  
            					
            		  <div class="modal-footer center-align row">
                          <hr>
                            <div class="col s6 m6 l4 offset-l1">
                              <a href="#!" id="sendPendingMessageId" class="modal-action waves-effect btn">Send</a>
                            		
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                          
                        </form>
                        </div> --%>


						</div>
						</div>
						<!--Pending payment END-->
						<div class="row">
							<div class="col s12 m12 l6">

								<div id="viewPaymentDetail" class="modal">
									<div class="modal-content">
										<h5 class="center">
											<u>Payment Detail</u>
											<i class="modal-close material-icons right">clear</i>
										</h5>


										<div class="row">
											<div class="col s12 l6 m6">
												<h6 id="boothNo" class="black-text">Booth No:</h6>
											</div>
											<div class="col s12 l6 m6">
												<h6 id="orderId" class="black-text">Order Id:</h6>
											</div>


											<div class="col s12 l6 m6">
												<h6 id="totalAmt" class="black-text">Total Amount:</h6>
											</div>
											<div class="col s12 l6 m6">
												<h6 id="balanceAmt" class="black-text">Balance Amount : </h6>
											</div>

										</div>

										<table border="2" class="centered tblborder">
											<thead>
												<tr>
													<th>Sr.No</th>
													<th>Amount</th>
													<th class="showPaymentCounterStatus">Status</th>
													<th>Mode</th>
													<th class="hideColumnOnSmall">Collecting Person</th>
													<th >Date</th>
												</tr>
											</thead>
											<tbody id="paymentDetail">

											</tbody>
											<tfoot>
												<tr>
													<th>Total</th>
													<th>
														<span id="totalAmountPaid"></span>
													</th>
													<th class="showPaymentCounterStatus"></th>
													<th class="hideColumnOnSmall"></th>
													<th></th>
													<th></th>
												</tr>
											</tfoot>
										</table>
									</div>
									<div class="modal-footer">
										<div class="col s12 m12 l12 center">
											<a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 m12 l8">
								<div id="addeditmsg" class="modal">
									<div class="modal-content" style="padding:0">
										<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
										<!--  <h5 id="msgHead"></h5> -->

										<h6 id="msg" class="center"></h6>
									</div>
									<div class="modal-footer">
											<div class="col s12 center">
													<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
											</div>
											
										</div>
								</div>
							</div>
						</div>

						

					</main>
					<!--content end-->
			</body>

			</html>