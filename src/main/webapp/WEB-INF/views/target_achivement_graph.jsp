<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">var myContextPath = "${pageContext.request.contextPath}"</script>
  <!-- <script src="https://code.highcharts.com/highcharts.js"></script>  -->
  <script src="resources/js/highcharts.js"></script>
  <script src="resources/js/exporting.js"></script>
<script src="resources/js/export-data.js"></script>
  <script type="text/javascript" src="resources/js/targetGraph.js"></script> 
   
   
    <style>
        /*.highcharts-container {
            display: block;
            height: 300px !important;
        }*/  
        
        .highcharts-legend {
            transform: translate(33, 3000);
        }
        
        .card-content {
            height: 115px !important;
        }   
        /* for cards color */     
        .redcard{
        	background-color:#f56954 !important;
        }
        .greencard{
        	background-color:#00a65a !important;
        }
        .tealLightenone{
        	background-color:#26a69a !important;
        }
        .bluecard{
        	background-color:#0073b7 !important;
        }
        /* for icon color */
        .grrenIcon{
        	color:#009551 !important;
        }
        .blueIcon{
        	color:#0067a4 !important;
        }
        .redIcon{	
        
        	color:#dc5e4b !important;
        }
        
        .countParent{
        	font-size:25px;
        	font-weight:400;
        }
      .card .card-content {
    	padding: 20px;
    	padding-right:2px;
    	border-radius: 0 0 2px 2px;
		} 
	.preloader-background{
		display:none!important;
	}
	 .btnmarginInform{
		margin-top:2%;
	} 
    </style>
    <script>
    </script>
    
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br/>
        <div class="col s12 m12 l12" style="padding:0">
         <div class="col s12 m12 l8 formBg row">
        	<input id="brandId" type="hidden" name="brandId" value="0" > 
          	<div class="input-field col s12 m3 l2 left">
          		<i class="material-icons prefix">view_stream </i>
               	<select name="departmentId" id="departmentId">
                            <option value="0" disabled selected>Department</option>                            
                            <c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
									<option value="<c:out value="${listValue.departmentId}"/>"><c:out value="${listValue.name}" /></option>
							</c:forEach>
							</c:if>
               </select>
          	</div>
          	
          	<div class="input-field col s12 m3 l3 left">
          		<i class="material-icons prefix">people </i>
               	<select name="employeeId" id="employeeId"  >
                     <option value="" disabled selected>Employee</option>
                </select>
          	</div>   
          	
          	<div id="yearShow">
	          	<div class="input-field col s12 m3 l2 left">
	          		<i class="material-icons prefix">filter_list </i>
	               	<select id="yearId1"  >
	                     <option value="" disabled selected>Year</option>
	                </select>
	          	</div>   
          	</div>
          	
          	<div id="monthShow">
	          	<div class="input-field col s12 m3 l2 left">
	          		<i class="material-icons prefix">filter_list </i>
	               	<select id="yearId2">
	                     <option value="" disabled selected>Year</option>
	                </select>
	          	</div>  
	          	<div class="input-field col s12 m3 l2 left"> 
	               	<select id="monthshow_monthId">
	                     <option value="" disabled selected>Month</option>
	                </select>
	          	</div>   
          	</div>
          	      
          	<div id="weekShow">
	          	<div class="input-field col s12 m3 l2 left">
	          		<i class="material-icons prefix">filter_list </i>
	               	<select id="yearId3"  >
	                     <option value="" disabled selected>Year</option>
	                </select>
	          	</div>  
	          	<div class="input-field col s12 m3 l3 left">
	               	<select id="weekshow_weekId"  >
	                     <option value="" disabled selected>Weeks</option>
	                </select>
	          	</div>   
          	</div>  
           <div id="dayShow">
           		<div class="input-field col s12 m3 l2 left">	
                    <input type="text" id="dayshow_pickdateId" class="datepicker" placeholder="Choose date" name="startDate">
                    <label for="dayshow_pickdateId" class="black-text">Pick Date</label>
                 </div>   
          	</div>
            <div id="buttonShow" class="col s10 l2 m2 btnmarginInform">
                <button type="button" id="showChartId" class="btn waves-effect waves-light blue-gradient" type="submit" name="action"><i class="material-icons left" >send</i>Show</button>
            </div>
               
        </div> 
        </div>
        <div class="row" style="height:100%" id="graphId">
            <div class="col s12 l12 m12" > 
                    <div id="dailyContainer" class="center-align  z-depth-2" style="height:100%;width:100%;"></div>
            </div>
       </div>
       <!-- <div class="row" style="height:100%" id="graphId">
            <div class="col s12 l12 m12" > 
                    <div id="container" class="center-align  z-depth-2" style="height:100%;width:100%;"></div>
            </div>
       </div> -->
       <div class="row" id="targetTable">
       	 <div class="input-field col s12 m10 l10 push-l1 push-m1">
               <table class="centered tblborder" id="targetTbl">
                   <thead>
                       <tr>
                           <th>Sr.No</th>
                           <th>Target Name</th>
                           <th>Assign</th>
                           <th>Completed</th>
                           <th>Completed Percentage</th> 
                       </tr>
                   </thead>
                   <tbody id="targetTblData">
                   
                   </tbody>
               </table>
           </div>
       </div>
    </main>
    <!--content end-->
        
</body>

</html>