<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>


<script type="text/javascript">
var checkedId=[];
var mobileNumberCollection="";
$(document).ready(function() {
	
	var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	     //$('#msgHead').text("Brand Message");
	     $('#msg').text(msg);
	 }
	
	
		var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         autoWidth: false,
	         columnDefs: [
	                      { 'width': '1%', 'targets': 0 },
	                      { 'width': '15%', 'targets': 1 },
	                      { 'width': '8%', 'targets': 2},
	                      { 'width': '8%', 'targets': 3},
	                      { 'width': '8%', 'targets': 4},
	                      { 'width': '3%', 'targets': 5},
	                      { 'width': '3%', 'targets': 6}
	                      ],
	         lengthMenu: [
	             [50, 75, 100, -1],
	             ['50 ', '75 ', '100', 'All']
	         ],
	         
	        
	         //dom: 'lBfrtip',
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [20,30,100,70,50,60,80,70,60,40,40,40,70] 
	                    		 } 
	                    		    })
	                         var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         }
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];
	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select")
          .change(function() {
              var t = this;
              var content = $(this).siblings('ul').detach();
              setTimeout(function() {
                  $(t).parent().append(content);
                  $("select").material_select();
              }, 200);
          });
      $('select').material_select();
      $('.dataTables_filter input').attr("placeholder", "Search");
      var table = $('#tblData').DataTable(); // note the capital D to get the API instance
      var column = table.columns('.toggle');
      column.visible(false);
      $('#showColumn').on('click', function () {
       
      	 //console.log(column);
    
      	 column.visible( ! column.visible()[0] );
    
      });
      
});

</script>
<style>
table.dataTable tbody td {
    padding: 0 2px !important;
}
	.dataTables_wrapper {
 
    margin-left: 2px !important;
}

/* #sendMsg{
	width:35%;
	border-radius: 20px;
} */
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 10px;
    border-bottom: 1px solid #111;
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
         
        <!--Add New Supplier-->
        <div class="row">
        <br/> 
        
         <div class="col s10 l3 m2 left">        
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/addChequeBook"><i class="material-icons left" >add</i>Add Cheque Book</a>
        </div>
        
   		<div class="col s10 l3 m2 offset-l2 right-align">
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/chequeCancelPage"><i class="material-icons left" >add</i>Cancel Cheque</a>
        </div>
        
         <div class="col s10 l3 m2 left">        
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/manageChequeTemplate"><i class="material-icons left" >add</i>Add Cheque Template Design</a>
        </div>
        
      <%--    <div class="col s10 l3 m2 ">
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/printChequeBook">Print Cheque</a>
        </div> --%>
        
        
         <div class="col s12 l12 m12 left">
      <br/>
            <table class="striped highlight centered  display " id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Account No.</th>
                        <th class="print-col" >Name</th>
                        <th class="print-col" >Starting Cheque No.</th>
                        <th class="print-col">End Cheque No.</th>
                        <th class="print-col">Total Leaves</th>
                        <th class="print-col">Unused Leaves</th>                       
                    </tr>
                </thead>

                <tbody id="employeeTblData">
              <% int rowincrement=0; %>
                   <c:if test="${not empty bankChequeBookDetailList}">
					<c:forEach var = "i" begin = "0" end = "${bankChequeBookDetailList.size()-1}" step="1">						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                       <tr>
                        <td><c:out value="${rowincrement}" /></td>
                        <td><c:out value="${bankChequeBookDetailList[i].bankAccount.bankName } - ${bankChequeBookDetailList[i].bankAccount.accountNumber }" /></td>
                         <td><c:out value="${bankChequeBookDetailList[i].bankAccount.accountHolderName}" /></td>
                        <td><c:out value="${bankChequeBookDetailList[i].srtChqNo}" /></td>
                         <td><c:out value="${bankChequeBookDetailList[i].endChqNo}" /></td>
                         <td><c:out value="${bankChequeBookDetailList[i].numberOfLeaves}" /></td>
                          <td><c:out value="${bankChequeBookDetailList[i].numberOfUnusedLeaves}" /></td>
                        <!-- Modal Trigger -->
                    </tr>
                    	 <!-- Modal Structure for delete -->
				</c:forEach>
				</c:if>
				</tbody>
            </table>
                
        </div>
        
   </div>
				
    </main>
    <!--content end-->
</body>

</html>