<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
<%@include file="components/header_imports.jsp"%>
<script type="text/javascript" src="resources/js/chequePrint.js"></script>
<script type="text/javascript">
	var msg = "";
	var templateDateFormate="";
	
	// Validation Function
	function feildValidation(id) {
		var payeeName = $('#payee_name' + id).val();
		var amt = $('#chq_Amt' + id).val();
		var amtInWord = convertNumberToWords(amt);
		var chqDate = $('#chq_date' + id).val();
		var chqNo = $('#chq_no' + id).text();
		var remark = $('#remark' + id).val();

		if (payeeName == "") {
			msg = "Please Enter Payee Name";
			//message.innerHTML = "Please Enter Payee Name";
			$('#msg').text(msg);
			//window.alert("Please Enter Payee Name for Cheque No:"+chqNo)

			var $toastContent = $('<span>Please Enter Payee Name for Cheque No.<span>').add('<span class="yellow-text">' + chqNo + '</span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
			return false
		} else if (amt <= 0) {
			msg = "Please Enter Cheque Amount";
			//message.innerHTML = "Please Enter Cheque Amount";
			$('#msg').text(msg);
			//window.alert("Please Enter Cheque Amount for Cheque No:"+chqNo)
			var $toastContent = $(
					'<span>Please Enter Cheque Amount for Cheque No.<span>')
					.add('<span class="yellow-text">' + chqNo + '</span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
			return false;
		} else if (chqDate == "") {
			msg = "Please Enter Cheque Date for Cheque No:" + chqNo;
			//message.innerHTML = "Please Enter Cheque Date";
			$('#msg').text(msg);
			//window.alert(msg)
			var $toastContent = $(
					'<span>Please Enter Cheque Date for Cheque No.<span>').add(
					'<span class="yellow-text">' + chqNo + '</span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
			return false;
		} else if (remark == "") {
			msg = "Please Enter Remark for Cheque No:" + chqNo;
			//message.innerHTML = "Please Enter Cheque Date";
			$('#msg').text(msg);
			//window.alert(msg)
			var $toastContent = $(
					'<span>Please Enter Remark for Cheque No.<span>').add(
					'<span class="yellow-text">' + chqNo + '</span>');
			Materialize.Toast.removeAll();
			Materialize.toast($toastContent, 3000);
			return false;
		} else {
			return true;
		}

	}
	function previewDataSet(id) {
		var payeeName = $('#payee_name' + id).val();
		var amt =$('#chq_Amt' + id).val();
		var amtInWord = convertNumberToWords($('#chq_Amt' + id).val()) + "Rupees Only";
		var chqDate = $('#chq_date' + id).val();
		var chqNo = $('#chq_no' + id).text();
		var remark = $('#remark' + id).val();
		var textData = "";
		if ($('#chb' + id).is(':checked')) {
			textData = "A/c Payee";
		}
		

		/* DATE sPLIIRING */
		var dateData = moment(chqDate);
		var date = dateData.format("DD");
		var month = dateData.format("MM");
		var year = dateData.format("YYYY");
		var lineDate="",d1="",d2="",m1="",m2="",y1="",y2="",y3="",y4="";
		if(templateDateFormate=="lineFormate"){
			 lineDate=date+"/"+month+"/"+year;	
		}else{
			 d1 = date.substring(0, 1);
			 d2 = date.substring(1, 2);
			 m1 = month.substring(0, 1);
			 m2 = month.substring(1, 2);
			 y1 = year.substring(0, 1);
			 y2 = year.substring(1, 2);
			 y3 = year.substring(2, 3);
			 y4 = year.substring(3, 4);
		}
		
		

		

		/* create aarray for sending data to Backend */
		var data={
				chequeNo:chqNo,
				payeeName:payeeName,
				chequeAmt:amt,
		        acPayee:textData,
				chequeDate:chqDate,
				remark:remark
		}
		dataList.push(data);
		$(".modal-content").append('<div class="container1" id="container_'+id+'" style="position: relative">'
								+ '<div class="center-align" style="height: 100%; width:fit-content;padding:0;">'
								+ '<div class="center-align" style="padding:0">'
								+ '<img style="border: 1px solid; height: 360px; width: 816px;" id="addImage_'+id+'" class="chqImg"  src="'+imgSrc+'"/>'
								+ '<div id="chqDatePreView_'
								+ id
								+ '" class="chqDate chqDatePreView" style="position: absolute;border: 0px solid blue; padding: 5px; background: solid white;">'
								+ '<div id="lineDate_'+id+'" class="lineDateFormat">'
								+ '<p id="lineDatePreview_'+id+'" class="lineDatePosition">'
								+lineDate
								+'</p>'
								+ '</div>'
								+ '<div id="boxDate_'+id+'" class="boxDateFormat" style="display: inline-block">'
								+ '<div id="d1_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="d1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ d1
								+ '</p>'
								+ '</div>'
								+ '<div id="d2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="d2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ d2
								+ '</p>'
								+ '</div>'
								+'<div id="m1_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="m1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ m1
								+ '</p>'
								+ '</div>'
								+'<div id="m2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p  id="m2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ m2
								+ '</p>'
								+ '</div>'
								+'<div id="y1_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y1
								+ '</p>'
								+ '</div>'
								+'<div id="y2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p  id="y2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y2
								+ '</p>'
								+ '</div>'
								+'<div id="y3_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y3Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y3
								+ '</p>'
								+ '</div>'
								+'<div id="y4_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y4Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y4
								+ '</p>'
								+ '</div>'
								+ '</div>'
								+ '</div>'
								+ '<div id="payeeNamePreView_'+id+'" class="draggable" style="position: absolute; left: 120px; top: 60px; width: 400px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">'
								+ '<p id="payeeNamePreViewText_'+id+'" name="payeeNamePreViewText" class="textPosition payeeNamePreView" style="margin:0">'
								+ payeeName
								+ '</p>'
								+ '</div>'
								+'<div id="amountPreView_'+id+'" class="draggable" style="position: absolute; right: 60px; top: 130px; width: 130px; height: 30px; min-width: 60px; border: 0px solid blue; padding: 5px;">'
								+ '<p id="amountPreViewText_'+id+'" class="textPosition amountPreView" style="margin:0">***'
								+ amt
								+ '/-</p>'
								+ '</div>'
								+'<div id="amountInWordPreView_'+id+'" class="draggable" style="position: absolute; left: 120px; top: 100px; width: 380px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">'
								+ '<p id="amountInWordPreViewText_'+id+'" class="textPosition amountInWordPreView"  style="margin:0" >'
								+ amtInWord
								+ '</p>'
								+ '</div>'
								+'<div id="acPayeePreView_'+id+'" class="draggable" style="position: absolute; left: 350px; top: 170px; width: 120px; height: 30px; border: 0px solid white; padding: 5px; min-width: 75px;">'
								+ '<p id="acPayeePreViewText_'+id+'" class="textPosition acPayeePreView" style="margin:0">'
								+ textData
								+ '</p>'
								+ '</div>'
								+ '</div>'
								+ '</div>' + '</div>');
	}

	function sendingBulkData(){
		Materialize.Toast.removeAll();
		$('.contentData').css('display','block');
		window.print();
		var dataListReq={
				bulkPrintSaveModelList:dataList,
				cbId:cbId,
				isEMICheque:"No"
				
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/saveBulkPrintCheque",
			type : "POST",
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend:function(){
				$('.contentData').css('display','none');
			},
			data : JSON.stringify(dataListReq),
			success : function(result) {

				
				window.location.reload();
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				
			},
			error : function(xhr,status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong!','2000','red lighten-2');
			}
		});
	}

	/* // preview of all Cheques
	function previewAllDataSet() {
		indexOfPreview = selectedPreviewListId
				.indexOf(selectedPreviewListId[0]);
		previewDataSet(selectedPreviewListId[0]);
	} */
	$(document).ready(function() {
						tableInitialize();
						$("select").change(function() {
							var t = this;
							var content = $(this).siblings('ul').detach();
							setTimeout(function() {
								$(t).parent().append(content);
								$("select").material_select();
							}, 200);
						});
						$('select').material_select();

						$('.dataTables_filter input').attr("placeholder",
								"Search");
						if ($.data.isMobile) {

							var table = $('#tblData').DataTable(); // note the capital D to get the API instance
							var column = table.columns('.hideOnSmall');
							column.visible(false);
						}
						$('.modal').modal({complete: function() { 
								$(".chbRow").prop('checked',false);
								$(".chbRow").change();
								selectedPreviewListId = [];
							}
						});
						$('.chqAmtClass').keypress(function(event) {
							var key = event.which;

							if (!(key >= 48 && key <= 57 || key === 13))
								event.preventDefault();
						});


						/* on Account number change Cheque Book List set on Cheque Book drop down */
						$('#bankAccountId').change(function() {

											var bankAccountId = $('#bankAccountId').val();
											if (bankAccountId === "0") {
												//changeTb="area";
												//table.draw();
												return false;
											}

											var select = document.getElementById('chequeBookId');

											// Clear the old options
											select.options.length = 0;
											select.options.add(new Option("Select Cheque Book ",'0'));
											$.ajax({
														url : "${pageContext.request.contextPath}/fetchBankCBListByBankAccountId?bankAccountId="
																+ bankAccountId,
																type : "get",
														dataType : "json",
														success : function(data) {
						
															/* alert(data); */
															var options, index, option;
															select = document
																	.getElementById('chequeBookId');

															for (var i = 0; i < data.length; i++) {
																var chequeBook = data[i];
																select.options.add(new Option(chequeBook.chqBookName+" ("+chequeBook.numberOfUnusedLeaves+" )",chequeBook.chequeBookId));
															}

															//console.log(CategoryIgst);
															$('.preloader-wrapper')
																	.hide();
															$('.preloader-background')
																	.hide();
															var table = $('#tblData').DataTable();
															table.clear().draw();
															
														},
														error : function(xhr,status, error) {
															$('.preloader-wrapper')
																	.hide();
															$('.preloader-background')
																	.hide();
															Materialize.Toast
																	.removeAll();
															Materialize
																	.toast(
																			'Please Add Cheque Template or Something went Wrong!',
																			'2000',
																			'red lighten-2');
														}
													});

										});
						
						// on Cheque Book Id Select fetch their unused cheque
						$('#chequeBookId').change(function() {
											var chequeBookId = $('#chequeBookId').val();
											cbId=chequeBookId;

											if (chequeBookId === "0") {
												//changeTb="area";
												//table.draw();
												return false;
											}

											/* var select = document.getElementById('chequeBookId'); */

											/* // Clear the old options
											select.options.length = 0;
											select.options.add(new Option("Select Cheque Book ", '0')); */
											$.ajax({
														url : "${pageContext.request.contextPath}/fetchChequeNoListByCBId?chequeBookId="
																+ chequeBookId,
														dataType : "json",
														type : "get",
														success : function(data) {

															/* alert(data); */
															/* var options, index, option;
															select = document.getElementById('chequeBookId');
															 */
															/* for (var i = 0; i < data.length; i++) {
																var chequeBook = data[i];
																select.options.add(new Option(chequeBook.chqBookName, chequeBook.chequeBookId));
															} */
															var srno = 1;
															var table = $('#tblData').DataTable();
															table.clear().draw();
															table.destroy();
														
															if(data.length==0){
																var $toastContent = $('<span>All Cheque Leaves are Printed for the Selected Cheque Book.<span>');
																Materialize.Toast.removeAll();
																Materialize.toast($toastContent, 3000);
															}
															for (var i = 0; i < data.length; i++) {

																$('#chequeNumberPrintData').append(
																				'<tr>'
																						+ '<td>'
																						+ srno
																						+ '</td>'
																						+ '<td><span id="chq_no'+srno+'">'
																						+ data[i]
																						+ '</span></td>'
																						+ '<td><input type="text" class="inputForChq" id="payee_name'+srno+'" /></td>'
																						+ '<td><input type="text" class="chqAmtClass inputForChq" id="chq_Amt'+srno+'" /></td>'
																						+ '<td><input type="text" class="datepicker chequeDate inputForChq"  name="chequeDate" id="chq_date'+srno+'"></td>'
																						+ '<td><input type="checkbox" class="filled-in checkAll" id="chb'+srno+'"/>'
																						+ '<label for="chb'+srno+'"></label></td>'
																						+ '<td><input type="text" class="inputForChq" id="remark'+srno+'" /></td>'
																						+
																						/*  '<td><button data-id="'+srno+'" class="previewBtn btn" onclick="previewDataSet('+srno+')">Preview</button></td>'+ */
																						/* '<td><input type="button" id="printbt"  value="Print"/></td>'+ */
																						'<td><input type="checkbox" class="filled-in chbRow selectPrintClass" data-id="'+srno+'" id="selectPrintchb'+srno+'"/>'
																						+ '<label for="selectPrintchb'+srno+'"></label></td>'
																						+ '</tr>');

																srno++;

															}

															/*payeeNameh">
																	<input type="hidden" name="chqAmt" id="chqAmth">
																	<input type="hidden" name="chqDate" id="chqDateh">
																	<input type="hidden" name="acpayee" id="acpayeeh">
																	<input type="hidden" name="chqNo" id="chqNoh">  */
															/*  hidden feild data set end */
															/*  class="validate datepicker" */
															$('.datepicker').pickadate({
																				container : 'body',
																				selectMonths : true, // Creates a dropdown to control month
																				selectYears : 15, // Creates a dropdown of 15 years to control year,
																				today : 'Today',
																				clear : 'Clear',
																				close : 'Ok',
																				format : 'yyyy-mm-dd',
																				closeOnSelect : 'true', // Close upon selecting a date,

																				// this function closes after date selection Close upon
																				// selecting a date,
																				onSet : function(
																						ele) {
																					if (ele.select) {
																						this
																								.close();
																					}
																				},
																				onStart : function() {
																					var date = new Date();
																					this.set('select',
																							[date.getFullYear(),
																							date.getMonth(),
																							date.getDate()]);
																					}

																			});
															/* $(".previewBtn").click(function(){
																var id=$(this).attr('data-id');
																previewDataSet();
																$('.modal').modal();
																$('#chqPreviewModal').modal('open');
															}); */

															// to get the checked box id
															//Sachin 21-09-2018
															/* $('input[type=checkbox]' ).change(function() {
																 if ($('input[type=checkbox]').is(':checked')) {
																 }
																   var bid = this.id; // button ID 
																   //var trid = $(this).closest('tr').attr('id'); // table row ID 
																   
																   var splitId=bid.substring(14);// to get the id of checked row
																   //alert(splitId)
																   selectedPreviewListId.push(splitId);
																   console.log(selectedPreviewListId);
																   
																 });
															 */

															$('.chqAmtClass').keypress(function(event) {
																				var key = event.which;

																				if (!(key >= 48&& key <= 57 || key === 13))
																					event.preventDefault();
																			});

															$(".chbRow").change(function() {
																var id = $(this).attr('data-id');
																				if ($(this).is(':checked')) {
																					
																					if (!feildValidation(id)) {
																						$(this).prop('checked',false);

																					} else {
																						$(this).closest('tr').find('input.inputForChq').attr('readonly',true);
																						id = parseFloat(id);
																						var isExist = jQuery.inArray(id,selectedPreviewListId);
																						if (isExist == -1) {
																							selectedPreviewListId.push(id);
																						}

																					}
																				} else {
																					//checked element is present in array if not then it returns -1 else position of ele.
																					var isExist = jQuery.inArray(id,selectedPreviewListId);
																					if (isExist != -1) {
																						selectedPreviewListId.splice(isExist,1);
																					}
																					$(this).closest('tr').find('input.inputForChq').removeAttr('readonly',false);
																				}
																			});
															tableInitialize();

															//console.log(CategoryIgst);
															$('.preloader-wrapper').hide();
															$('.preloader-background').hide();
															$("#productRate").keyup();
														},
														error : function(xhr,
																status, error) {
															$('.preloader-wrapper').hide();
															$('.preloader-background').hide();
															Materialize.Toast.removeAll();
															Materialize.toast('Something Went Wrong!','2000','red lighten-2');
														}
													});

											$.ajax({
														type : "get",
														url : "${pageContext.request.contextPath}/fetchChequeTempalteDesigntByCBId?id="
																+ chequeBookId,
														dataType : "json",
														success : function(data) {
															var chequeTemplateDesign = data;
															imgSrc = chequeTemplateDesign.templateImgBase64;

															console.log(data);
															$("#addImage").attr("src",imgSrc);
															
															templateDateFormate=data.dateFormate;
															if(templateDateFormate=="boxFormate"){
																$('.boxDateFormat').show();
																$('.lineDateFormat').hide();
															}else{
																$('.boxDateFormat').hide();
																$('.lineDateFormat').show();
															}
															
															 var css = '.payeeNamePreView{ left: '+ data.payeeNameLeft+'px;width:'+data.payeeNameWidth+'px;top:'+data.payeeNameTop+'px;}'+
															 '.amountPreView{ left: '+ data.amountLeft+'px;width:'+data.amountWidth+'px;top:'+data.amountTop+'px;}'+	
															 '.amountInWordPreView{ left: '+ data.amountInWordLeft+'px;width:'+data.amountInWordWidth+'px;top:'+data.amountInWordTop+'px;}'+
															 '.acPayeePreView{ left: '+ data.acPayeeLeft+'px;width:'+data.acPayeeWidth+'px;top:'+data.acPayeeTop+'px;}'+
															 '.chqDatePreView{ left: '+ data.chqDateLeft+'px;width:'+data.chqDateWidth+'px;top:'+data.chqDateTop+'px;}'+
															 '.dateDimension{ height: '+ data.chqDateHeight+'px;width:'+(data.chqDateWidth)/8.25+'px;}'
															 , 
														    head = document.head || document.getElementsByTagName('head')[0],
														    style = document.createElement('style');

																style.type = 'text/css';
																if (style.styleSheet){
																  // This is required for IE8 and below.
																  style.styleSheet.cssText = css;
																} else {
																  style.appendChild(document.createTextNode(css));
																}
		
																head.appendChild(style);
															
															/* $(".payeeNamePreView").css(
																			{
																				"left" : data.payeeNameLeft,
																				"width" : data.payeeNameWidth,
																				"top" : data.payeeNameTop

																			});
															$(".payeeName").css(
																			{
																				"left" : data.payeeNameLeft,
																				"width" : data.payeeNameWidth,
																				"top" : data.payeeNameTop

																			});

															$(".amountPreView")
																	.css(
																			{
																				"left" : data.amountLeft,
																				"width" : data.amountWidth,
																				"top" : data.amountTop

																			});
															$(".amountInWordPreView").css(
																			{
																				"left" : data.amountInWordLeft,
																				"width" : data.amountInWordWidth,
																				"top" : data.amountInWordTop

																			});

															$(".acPayeePreView").css(
																			{
																				"left" : data.acPayeeLeft,
																				"width" : data.acPayeeWidth,
																				"top" : data.acPayeeTop

																			});
															$(".chqDatePreView").css(
																			{
																				"left" : data.chqDateLeft,
																				"top" : data.chqDateTop,
																				
																				"width" : data.chqDateWidth

																			});

															var boxDivWidth = parseFloat(data.chqDateWidth) / 8.25;
															$('.dateDimension')
																	.css(
																			{
																				"height" : data.chqDateHeight,
																				"width" : boxDivWidth
																			});
															//console.log(CategoryIgst);
															$('.preloader-wrapper').hide();
															$('.preloader-background').hide(); */

														},
														error : function(xhr,
																status, error) {
															$(
																	'.preloader-wrapper')
																	.hide();
															$(
																	'.preloader-background')
																	.hide();
															Materialize.Toast
																	.removeAll();
															Materialize
																	.toast(
																			'Something Went Wrong!',
																			'2000',
																			'red lighten-2');
														}
													});

										});

					});
	function tableInitialize() {
		//var table = $('#tblData').DataTable();
		//table.destroy();
		$('#tblData').DataTable(
						{
							"oLanguage" : {
								"sLengthMenu" : "Show _MENU_",
								"sSearch" : "_INPUT_" //search
							},
							"bDestroy" : true,
							autoWidth : false,
							columnDefs : [ {'width' : '5%','targets' : 0},
								{'width' : '10%','targets' : 1},
								{'width' : '25%','targets' : 2},
								{'width' : '8%','targets' : 3},
								{'width' : '15%','targets' : 4},
								{'width' : '5%','targets' : 5},
								{'width' : '15%','targets' : 6},
								{'width' : '5%','targets' : 7},
							
							],
							lengthMenu : [ [ 10, 25., 50, -1 ],
									[ '10 ', '25 ', '50 ', 'All' ] ],

							//dom: 'lBfrtip',
							dom : '<lBfr<"scrollDivTable"t>ip>',
							buttons : {
								buttons : [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend : 'pdf',
											className : 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
											customize : function(doc, config) {
												doc.content.forEach(function(
														item) {
													if (item.table) {
														item.table.widths = [
																30, '*', '*',
																'*', '*', 50 ]
													}
												})
												/* var tableNode;
												for (i = 0; i < doc.content.length; ++i) {
												  if(doc.content[i].table !== undefined){
													tableNode = doc.content[i];
													break;
												  }
												}
												
												var rowIndex = 0;
												var tableColumnCount = tableNode.table.body[rowIndex].length;
												 
												if(tableColumnCount > 6){
												  doc.pageOrientation = 'landscape';
												} */
												/*for customize the pdf content*/
												doc.pageMargins = [ 5, 20, 10,
														5 ];
												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										},
										{
											extend : 'excel',
											className : 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
										},
										{
											extend : 'print',
											className : 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
										},
										{
											extend : 'colvis',
											className : 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout : 'fixed two-column',
											align : 'left'
										}, ]
							}

						});
	}
</script>
<style>
.lineDatePosition{
margin-top:0 !important;
}
.card {
	height: 2.5rem;
	line-height: 2.5rem;
	margin-top: 0 !important;
}

.card-image {
	width: 35% !important;
	background-color: #0073b7 !important;
}

.card-image h6 {
	padding: 5px;
}

.card-stacked .card-content {
	padding: 5px;
}

.input-field {
	position: relative;
	margin-top: 0.5rem;
}

/* #oneDateDiv {
							margin-top: 0.5rem;
							margin-left: 4%
						} */
#area {
	width: 35% !important;
}

.actionDiv, .salesmanFilter {
	margin-top: 10px;
}

.dateDimension {
	display: table-cell;
	vertical-align: middle;
}

.dateDimension p {
	margin: 0 !important;
}

.dateBox {
	display: table-cell;
	vertical-align: middle;
}
.sliderBtnsDiv{
	display:none;
}
.contentData{
display:none;
}
@media only screen and (max-width: 600px) {
	#area {
		width: 90% !important;
		border-radius: 10px !important;
	}
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
	#area {
		width: 70% !important;
	}
	.actionDiv, .salesmanFilter {
		margin-top: 0;
	}
}
@page {
  margin-top:0 !important;
}
@media print {

	body * {
		visibility: hidden;
	}
	 .contentData, .contentData * {
		visibility: visible;
	}
	
	
	
	.contentData div.container1{ 
		page-break-after: always;
		
		
	}
	
	.contentData div.container1:last-child{ 
		page-break-after: avoid;
	}
	

		#printBtn,.sliderBtnsDiv,#addImage,.chqImg {
		visibility: hidden;
	}
 	 .contentData {
   	position: relative !important;
   	margin:0 !important;
    left: 0 !important;
    top: 0 !important;
  }  
}
</style>

</head>

<body>

	
		<div class="contentData" id="contentData" >
			
		</div>
<!--  width:100% !important; --> 
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->
	
	
	<main class="paddingBody"> <br class="hide-on-small-only">
	
	<div class="row">
		<div class="col s6 l3 m3 left left-align actionDiv filerMargin">
			<select id="bankAccountId" name="bankAccountId" class="btnSelect">
				<option value="0">Select Account Number</option>
				<c:if test="${not empty bankAccountList}">
					<c:forEach var="listValue" items="${bankAccountList}">
						<option value="<c:out value="${listValue.id}" />"><c:out
								value="${listValue.bankShortName} - ${listValue.accountNumber}" /></option>
					</c:forEach>
				</c:if>
			</select>
		</div>
		<div class="col s6 l3 m3 left left-align actionDiv filerMargin">
			<select id="chequeBookId" name="chequeBookId" class="btnSelect">
				<option value="0">Select Cheque Book</option>
				<%-- <c:if test="${not empty areaList}">
											<c:forEach var="listValue" items="${areaList}">
												<option value="<c:out value="${listValue.areaId}" />"><c:out value="${listValue.name}" /></option>
											</c:forEach>
										</c:if> --%>
			</select>
		</div>

		<div class="col s6 l3 m3 right right-align">
			<a class="btn waves-effect waves-light blue-gradient"
				href="${pageContext.servletContext.contextPath}/manualChequePrint"><i
				class="material-icons left">add</i>Manual Cheque Print</a>
		</div>

		<br> </br>

		<!-- <div class="col s6 l3 m3  left-align">
						        <button id="previewBt">Preview</button>
						        </div> -->


		<div class="col s12 l12 m12 ">
			<table class="striped highlight bordered centered " id="tblData"
				cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="print-col hideOnSmall">Sr. No</th>
						<th class="print-col">Cheque No.</th>
						<th class="print-col">Payee Name</th>
						<th class="print-col">Cheque Amount</th>
						<th class="print-col hideOnSmall">ChequeDate</th>
						<!-- <th class="print-col">A/C Payee</th> -->
						<th class="print-col">A/C Payee <input type="checkbox"
							class="filled-in checkAll" id="checkAll" /> <label for="checkAll"></label>
						</th>
						<th class="print-col">Remark</th>
						<!-- <th class="print-col">Preview</th> -->
						<th class="print-col">Select For Print</th>
						<!-- <th class="print-col">Print</th> -->
					</tr>
				</thead>

				<tbody id="chequeNumberPrintData">
					<!-- <tr>
											<td></td>
											<td></td>
											<td><input type="text" id="payee_name" /></td>
											<td><input type="text" id="chq_Amt" /></td>
											<td><input type="date" id="chq_date" /></td>
											<td> <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
                           					 <label for="checkAll"></label></td>
											<td><input type="text" id="remark" /></td>
											<td><Input type="button"  value="Print" id="chk_all" /></td>
										</tr> -->
				</tbody>
			</table>
		</div>
		<div class="col s12 l12 m12 ">
			<button class="btn" id="printBtnMain" center onclick="printData()">Print</button>
			<button class="btn" onclick="showPreview()">Preview</button>
		</div>
	</div>
	<!-- print preview content data div -->
	
	
	
	
	
	<!-- PreView Model Start  --> <!-- Modal Structure for Preview -->
	<form
		action="${pageContext.request.contextPath}/savePrintedChequeFromCB"
		method="post" class="col s12 l12 m12" id="savePrintModal1">
		<div id="chqPreviewModal" class="modal">
			<div class="modal-content">
				
				
			</div>

			<div class="modal-footer row">
				<div class="col s12 l12 m12  center">
					<button id="printBtn" type="button" class="btn">Print</button>
				</div>

				<div class="row sliderBtnsDiv">
					<div class="col s12 l12 m12  center">
					<button id="previousPreviewtBtn" type="button" class="btn">
						<i class="material-icons right">chevron_left</i>
					</button>
				
					<button id="nextPreviewtBtn" type="button" class="btn">
						<i class="material-icons right">chevron_right</i>
					</button>
					</div>
				
				</div>

			</div>





		</div>
	</form>
	<!--PreView Model End  --> <!-- msg Model -->
	<div class="row">
		<div class="col s12 m12 l12">
			<div id="addeditmsg" class="modal">
				<div class="modal-content" style="padding: 0">
					<div class="center  white-text" id="modalType"
						style="padding: 3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
					<h6 id="msg" class="center"></h6>
				</div>
				<div class="modal-footer">
					<div class="col s12 center">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	</main>
	<!--content end-->
</body>
<!-- <div class="container1" id="container1" style="position: relative">


					<input type="hidden" name="payeeNameh" id="payeeNameh"> <input
						type="hidden" name="chqAmth" id="chqAmth"> <input
						type="hidden" name="chqDateh" id="chqDateh"> <input
						type="hidden" name="acpayeeh" id="acpayeeh"> <input
						type="hidden" name="chqNoh" id="chqNoh"> <input
						type="hidden" name="chqBookIdh" id="chqBookIdh"> <input
						type="hidden" name="remarkh" id="remarkh">

					<div class="center-align"
						style="height: 100%; width: fit-content; padding: 0;">



						<div class="center-align" style="padding: 0">

							<img style="border: 1px solid; height: 360px; width: 816px;"
								id="addImage" />

							<div id="chqDatePreView" class="chqDate"
								style="position: absolute; top: 5%; width: 180px; height: 30px; min-height: 15px; max-height: 50px; min-width: 90px; border: 0px solid blue; padding: 5px; background: solid white;">


								<div id="lineDate" style="display: none;">
									<p id="lineDatePreview" class="textPosition">DD/MM/YYYY</p>
								</div>


								<div id="boxDate" style="display: inline-block">
									<div id="d1" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="d1Preview" class="textPosition1"
											style="text-align: center">D</p>
									</div>

									<div id="d2" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="d2Preview" class="textPosition1"
											style="text-align: center">D</p>
									</div>

									<div id="m1" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="m1Preview" class="textPosition1"
											style="text-align: center">M</p>
									</div>

									<div id="m2" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="m2Preview" class="textPosition1"
											style="text-align: center">M</p>
									</div>

									<div id="y1" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="y1Preview" class="textPosition1"
											style="text-align: center">Y</p>
									</div>

									<div id="y2" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="y2Preview" class="textPosition1"
											style="text-align: center">Y</p>
									</div>

									<div id="y3" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="y3Preview" class="textPosition1"
											style="text-align: center">Y</p>
									</div>

									<div id="y4" class="dateBox dateDimension"
										style="display: table-cell; vertical-align: middle;">
										<p id="y4Preview" class="textPosition1"
											style="text-align: center">Y</p>
									</div>

								</div>


							</div>




							<div id="payeeNamePreView" class="draggable"
								style="position: absolute; left: 120px; top: 60px; width: 400px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">
								<p id="payeeNamePreViewText" name="payeeNamePreViewText" val=""
									class="textPosition" style="margin: 0">Payee
									Name=====================></p>
							</div>

							<div id="amountPreView" class="draggable"
								style="position: absolute; right: 60px; top: 130px; width: 130px; height: 30px; min-width: 60px; border: 0px solid blue; padding: 5px;">
								<p id="amountPreViewText" class="textPosition" style="margin: 0">***
									Amount</p>
							</div>

							<div id="amountInWordPreView" class="draggable"
								style="position: absolute; left: 120px; top: 100px; width: 380px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">
								<p id="amountInWordPreViewText" class="textPosition"
									style="margin: 0">Amount in word================></p>
							</div>

							<div id="acPayeePreView" class="draggable"
								style="position: absolute; left: 350px; top: 170px; width: 120px; height: 30px; border: 0px solid white; padding: 5px; min-width: 75px;">
								<p id="acPayeePreViewText" class="textPosition"
									style="margin: 0">A/c Payee</p>
							</div>
						</div>






					</div>
				</div> -->
</html>
