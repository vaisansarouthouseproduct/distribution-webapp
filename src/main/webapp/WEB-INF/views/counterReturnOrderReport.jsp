<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
	    	$('#showPaymentCounterStatus').hide();
	    	var msg="${saveMsg}";
	       	 if(msg!='' && msg!=undefined)
	       	 {
	       		 $('#addeditmsg').find("#modalType").addClass("success");
	 			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	       	     $('#addeditmsg').modal('open');
	       	     /* $('#msgHead').text("HRM Message"); */
	       	     $('#msg').text(msg);
	       	 }
	    	
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '2%', 'targets': 1},
	   	                  	  { 'width': '5%', 'targets': 2},
	   	                	  { 'width': '5%', 'targets': 3},
	   	             		  { 'width': '3%', 'targets': 4},
	   	             		  { 'width': '5%', 'targets': 5},
	   	                      { 'width': '5%', 'targets': 6},
	   	                  	  { 'width': '3%', 'targets': 7},
	   	                	  { 'width': '5%', 'targets': 8},
	   	             		  { 'width': '2%', 'targets': 9}
	   	                	  
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,40,50,70,40,40,30,40,50,60,50] 
		                    		 } 
		                    		    })
	   	       
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select")
	             .change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
	       //if there is mobile device the element contain this class will be hidden
	     	if ($.data.isMobile) {
	     		var table = $('#tblData').DataTable(); // note the capital D to get the API instance
	     		var column = table.columns('.hideOnSmall');
	     		column.visible(false);
	     	}
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            
           
            
          //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            console.log(column);
            if(${sessionScope.loginType=='GateKeeper'}){            
                column.visible(true);
            }else{
            	 column.visible(false);
            }
        });  


    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
/* #oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
} */
td .btn-flat{
	color:unset !important;
	font-size:0.8rem !important;
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br class="hide-on-small-only">
        <div class="row">
				<div class="col s6 m3 l3 right right-align actionDiv actionBtn">
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>                    
                     <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/returnCounterOrderReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s6 l5 m4 offset-l3 offset-m4 right actionDiv" id="oneDateDiv">                         	
                    <form action="${pageContext.request.contextPath}/returnCounterOrderReport" method="post">
	                    <div class="input-field col s8 m6 l4">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s2 m2 l2">
	                     <button type="submit" class="btn">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
			   <div class="input-field col s12 l6 m5 offset-m3 right actionDiv rangeDiv" style="margin-top: 0.5rem;">   
           		 <form action="${pageContext.servletContext.contextPath}/returnCounterOrderReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s5 m2 l3">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s5 m2 l3">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s2 m2 l2">
                            <button type="submit" class="btn">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Return Counter Order Id</th>
                            <th class="print-col">Counter Order Id</th>
                            <th class="print-col hideOnSmall">Gatekeeper name</th>
                            <th class="print-col">Shop Name/Customer Name</th>
                            <th class="print-col hideOnSmall">Taxable Amt</th>
                            <th class="print-col">Total Amt</th>
                            <th class="print-col">Total Qty</th>
                            <th class="print-col hideOnSmall">Return Date/Time</th>
                            <th>Credit Note</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:if test="${not empty returnCounterOrderReportList}">
						<c:forEach var="listValue" items="${returnCounterOrderReportList}">
		                    <tr>
		                    	<td><c:out value="${listValue.srno}" /></td>
		                    	<td>
		                    		<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/returnCounterOrderProductDetailsListForWebApp?returnCounterOrderId=${listValue.returnCounterOrderId}"><c:out value="${listValue.returnCounterOrderGenId}" /></a>
		                    	</td>
		                    	<td>
		                    		<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/counterOrderProductDetailsListForWebApp?counterOrderId=${listValue.counterOrderId}"><c:out value="${listValue.counterOrderId}" /></a>
		                    	</td>
		                    	<td><c:out value="${listValue.gateKeeperName}" /></td>
		                    	<td><c:out value="${listValue.customerName}" /></td>
		                    	<td><c:out value="${listValue.totalAmount}" /></td>
		                    	<td><c:out value="${listValue.totalAmountWithTax}" /></td>
		                    	<td><c:out value="${listValue.totalQuantity}" /></td>
		                        <td>
		                    		 <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.returnDate}" /><c:out value="${dt}" />
			                        	<br/>
			                         <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.returnDate}" /><c:out value="${time}" />
		                    	</td>
		                    	<td><a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/counterReturnOrderInvoice.pdf?returnCounterOrderId=${listValue.returnCounterOrderId}"><i class="material-icons">receipt</i></a></td>
		                    	
		                    </tr>
		                    
		                 </c:forEach>
		             </c:if>
                    </tbody>
                </table>
            </div>
        </div>
		
    </main>
    <!--content end-->
</body>

</html>