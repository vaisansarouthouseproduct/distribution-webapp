<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">
/* $(window).unload(function(){
    $("#addeditmsg").modal('close');
}); */
 $(document).ready(function(){
	 var table = $('#tblData').DataTable();
		table.destroy();
		$('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show:  _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         "autoWidth": false,
	          "columnDefs": [
							{ "width": "2%", "targets": 0},	
							{ "width": "12%", "targets": 1},	
			                { "width": "5%", "targets": 2},
			                { "width": "5%", "targets": 3},	
			                { "width": "4%", "targets": 4}	
			                
			              ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	  doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [40,'*','*','*'] 
	                    		 } 
	                    		    })
	                         var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                        /*  var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         } */
	                         /*for customize the pdf content*/
	                          doc.content[1].table.alignment = 'center';
	                         doc.pageMargins = [5,20,10,5];
	                         
	                         doc.defaultStyle.fontSize = 8;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                        
	                       },
	                      
	               
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		$('select').material_select();
	     $("select").change(function() {
	             var t = this;
	             var content = $(this).siblings('ul').detach();
	             setTimeout(function() {
	                 $(t).parent().append(content);
	                 $("select").material_select();
	             }, 200);
	         });
	     $('.dataTables_filter input').attr("placeholder", "Search");
		 if ($.data.isMobile) {
			var table = $('#tblData').DataTable(); // note the capital D to get the API instance
			var column = table.columns('.hideOnSmall');
			column.visible(false);

			

		
		}
		if($.data.mobileAndIpad){
			$(".catProductBtn").removeClass('right right-align');
		}
	 var msg="${saveMsg}";
	 //alert(msg);
	 $('#addeditmsg').modal({
	 complete:function(){
		 msg="";
		 <c:set var="saveMsg" value=""/>
		 var m = "${saveMsg}";
		// console.log("m = "+m);
	 }
	});

	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	     //$('#msgHead').text("Brand Message");
	     $('#msg').text(msg);
	     
	 }
	
	$("#resetBrandSubmit").click(function(){
		
		$('#brandname').val('');	
		$('#brandId').val('0');
		$("#saveBrandSubmit").html('<i class="material-icons left">add</i> Add');

	});
});
 /* ON CLICK ON EDIT BUTTON SET RECORD OF GIVEN BRAND ID */
function editBrand(id) {
	
	$.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/fetchBrand?brandId="+id,
				/* data: "id=" + id + "&name=" + name, */
				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
				success : function(data) {
					var brand = data;
					//alert(brand.name +" "+ brand.brandId);
					$('#brandname').val(brand.name);	
					$('#brandId').val(brand.brandId);
					$('#brandname').focus();
					$("#saveBrandSubmit").html('<i class="material-icons left">send</i> update');
					
					
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					  //alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Message : ");
           	     	$('#msg').text("Something Went Wrong"); 
           	     		setTimeout(function() 
						  {
  	     					$('#addeditmsg').modal('close');
						  }, 1000);
					}					
			
			});
}


</script>
<style>
/* .formBg{
	background-color:#cfd8dc !important;
	border:1px solid #b0bec5;
	border-radius:5px;
	padding:5px !important;
} */
label{
	color:black !important;
}

.btnmargin{
margin-top:2%;
}
.dataTables_wrapper th {
    padding: 5px 10px !important;
}
@media only screen and (min-width:992px){
	.btnmarginInform{
	margin-top:2%;
	width:21%;
} 
}
  @media only screen and (min-width:601px) and (max-width:992px){
  	.btnmarginInform{
  		margin-top:3%;
  	}
  }
@media only screen and (max-width:992px){
.catProductBtn{
margin:20px 0;
}
}

</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
        
        
        <div class="col s12 m12 l12">
         <div class="col s12 m12 l8 formBg">
        <form  action="${pageContext.servletContext.contextPath}/saveBrand" method="post" id="saveBrandForm">
        	<input id="brandId" type="hidden" name="brandId" value="0" > 
          	<%-- <div class="input-field col s12 m4 l4 left">
          		
               	<select name="areaId" id="areaId" class="validate" required="" aria-required="true" title="Please select Area">
                                 <option value="" selected><span id="redMark" class="red-text">*</span>Select Area</option>
                                <c:if test="${not empty areaList}">
							<c:forEach var="listValue" items="${areaList}">
								<option value="<c:out value="${listValue.areaId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
							</c:if>
                        </select>
          	</div> --%>
                <div class="input-field col s12 m4 l4 left" style="padding-bottom:15px;">                	
					<label for="brandname"><span class="red-text">*</span>Enter Brand Name</label>
					<input id="brandname" type="text" name="name" value="" required>
                    <!-- <label for="brandname" class="active">Enter Brand Name</label> -->
                </div>
                <div class="col s6 l2 m3 btnmarginInform">
                    <button id="saveBrandSubmit" class="btn waves-effect waves-light blue-gradient" type="submit" name="action"><i class="material-icons left" >add</i>Add</button>
                </div>
                 <div class="col s6 l2 m3 btnmarginInform">
                    <button id="resetBrandSubmit" class="btn waves-effect waves-light blue-gradient" type="button" name="action"><i class="left material-icons">refresh</i>Reset</button>
                </div>
              
        </form>
        </div>
        <div class="col s6 m3 l2 right right-align catProductBtn" style="padding:0;">
         <a  class="btn waves-effect waves-light blue-gradient right-align" href="${pageContext.request.contextPath}/fetchProductList"><i class="left material-icons">settings</i>Product</a>
        </div>
        <div class="col s6 m3 l2 right catProductBtn">
         <a  class="btn waves-effect waves-light blue-gradient right" href="${pageContext.request.contextPath}/fetchCategoriesList"><i class="left material-icons">settings</i>Category</a>
        </div>
        
         
        </div>

	<div class="col s12 m12 l12"> 
            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>

                        <th class="print-col">Sr. No.</th>
                        <th class="print-col wrapok">Name</th>
                        <th class="print-col">Added</th>
                        <th class="print-col">Updated</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->

                    </tr>
                </thead>

                <tbody>
                <% int rowincrement=0; %>
                     <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<tr>
	                        <td><c:out value="${rowincrement}" /></td>	
	                        <td><c:out value="${listValue.name}" /></td>
	                        <td>
		                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.brandAddedDatetime}" /><c:out value="${dt}" />
		                        	<br/>
		                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.brandAddedDatetime}" /><c:out value="${time}" />                        
	                        </td>
	                        <td>
	                        	<c:choose>
	                        	<c:when test="${empty  listValue.brandUpdateDatetime}">
	                        		NA
		                        </c:when>
		                        <c:otherwise>
			                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.brandUpdateDatetime}" /><c:out value="${dt}" />
			                        	<br/>
			                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.brandUpdateDatetime}" /><c:out value="${time}" />
		                        </c:otherwise>
		                        </c:choose>  
	                        </td>
	                        <td><button class="btn-flat" class="brandedit" onclick='editBrand(${listValue.brandId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
	                   
	                   <!-- Modal Trigger -->
                       <!--  <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    	</tr>
                    	</c:forEach>
                    </c:if>
                    <!-- Modal Structure for delete -->

                
                </tbody>
            </table>
          </div>
</div>          
	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
        
    </main>
    <!--content end-->
</body>

</html>