<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
#map {
	width: 100%;
	height: 600px;
}

.mapContainer {
	width: 50%;
	position: relative;
}

.mapContainer a.direction-link {
	position: absolute;
	top: 15px;
	right: 15px;
	z-index: 100010;
	color: #FFF;
	text-decoration: none;
	font-size: 15px;
	font-weight: bold;
	line-height: 25px;
	padding: 8px 20px 8px 50px;
	background: #0094de;
	background-image: url('direction-icon.png');
	background-position: left center;
	background-repeat: no-repeat;
}

.mapContainer a.direction-link:hover {
	text-decoration: none;
	background: #0072ab;
	color: #FFF;
	background-image: url('direction-icon.png');
	background-position: left center;
	background-repeat: no-repeat;
}
</style>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0"></script>
<script type="text/javascript">
	// <![CDATA[
	var markers = [
			{
				"lat" : "19.2316437",
				"lng" : "72.8621382"
			},
			{
				"title" : "shilparamam",
				"lat" : "17.452665",
				"lng" : "78.435608",
				"description" : "Mumbai formerly Bombay, is the capital city of the Indian state of Maharashtra."
			},
			{
				"title" : "image hospitals",
				"lat" : "17.452421",
				"lng" : "78.435715",
				"description" : "Pune is the seventh largest metropolis in India, the second largest in the state of Maharashtra after Mumbai."
			} ];
	window.onload = function() {
		var mapOptions = {
			center : new google.maps.LatLng(markers[0].lat, markers[0].lng),
			zoom : 10,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("dvMap"),
				mapOptions);
		var infoWindow = new google.maps.InfoWindow();
		var lat_lng = new Array();
		var latlngbounds = new google.maps.LatLngBounds();
		for (i = 0; i < markers.length; i++) {
			var data = markers[i]
			var myLatlng = new google.maps.LatLng(data.lat, data.lng);
			lat_lng.push(myLatlng);
			var marker = new google.maps.Marker({
				position : myLatlng,
				map : map,
				title : data.title
			});
			latlngbounds.extend(marker.position);
			(function(marker, data) {
				google.maps.event.addListener(marker, "click", function(e) {
					infoWindow.setContent(data.description);
					infoWindow.open(map, marker);
				});
			})(marker, data);
		}
		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);

	}

	// ]]>
</script>
</head>
<body id="map">
	</pre>
	<div id="dvMap" style="width: 500px; height: 500px;"></div>

</body>
</html>