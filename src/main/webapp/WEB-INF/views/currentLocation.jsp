<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
     <style>
.mapContainer{
    width:50%;
    position: relative;
}
.mapContainer a.direction-link {
    position: absolute;
    top: 15px;
    right: 15px;
    z-index: 100010;
    color: #FFF;
    text-decoration: none;
    font-size: 15px;
    font-weight: bold;
    line-height: 25px;
    padding: 8px 20px 8px 50px;
    background: #0094de;
    background-image: url('direction-icon.png');
    background-position: left center;
    background-repeat: no-repeat;
}
.mapContainer a.direction-link:hover {
    text-decoration: none;
    background: #0072ab;
    color: #FFF;
    background-image: url('resources/img/direction.png');
    background-position: left center;
    background-repeat: no-repeat;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0"></script>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>

   <script type="text/javascript">     
     
   function initialize() {
       var latlng = new google.maps.LatLng(19.231652, 72.861622);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 15
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : Vaisansar Technologies PVT. LTD.</div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
        GetAddress(19.231652, 72.861622);
    }
    function GetAddress(lat,lng) {
        var latlng = new google.maps.LatLng(lat,lng);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                	$('#currentLocation').val(results[0].formatted_address);     
                	$('#currentLocation').change();
                }
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
 
    
    function initializeDynamic(lat,lng,name) {
        var latlng = new google.maps.LatLng(lat,lng);
         var map = new google.maps.Map(document.getElementById('map'), {
           center: latlng,
           zoom: 15
         });
         var marker = new google.maps.Marker({
           map: map,
           position: latlng,
           draggable: false,
           anchorPoint: new google.maps.Point(0, -29)
        });
         var infowindow = new google.maps.InfoWindow();
         google.maps.event.addListener(marker, 'click', function() {
           var iwContent = '<div id="iw_container">' +
           '<div class="iw_title"><b>Location</b> : '+name+'</div></div>';
           // including content to the infowindow
           infowindow.setContent(iwContent);
           // opening the infowindow in the current map and at the current marker location
           infowindow.open(map, marker);
         });
     }
    
     function initializeMultipleDynamic() { 
    	 var markers=[];
    	 
    	// Get the raw DOM object for the select box
			var select = document.getElementById('employeeId');

			// Clear the old options
			select.options.length = 0;

			//Load the new options

			select.options.add(new Option("Choose Employee", 0));
    	// alert($('#departmentId').val());
    	 $.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/fetchEmployeeLatLng?departmentId="+$('#departmentId').val(),
				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
			    async :false,
				success : function(data) {
					employeeDetils=data;
					
					var options, index, option;
					select = document.getElementById('employeeId');
					
					for(i=0; i<employeeDetils.length; i++)
					{
						var results = new Object();
						results['title'] = employeeDetils[i].employee.department.name;
						results['lat'] = employeeDetils[i].latitude;
						results['lng'] = employeeDetils[i].longitude;
						results['description'] = employeeDetils[i].name;
						markers.push(results);
						
						select.options.add(new Option(employeeDetils[i].name, employeeDetils[i].employeeDetailsId));
					}  			
					/* alert(markers.length);
					for(i in markers) {
						db=markers[i];
						alert(db['title']);
					} */
					
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
    	     	$('#msgHead').text("Message : ");
    	     	$('#msg').text("Something Went Wrong"); 
    	     		setTimeout(function() 
					  {
    					$('#addeditmsg').modal('close');
					  }, 1000);
				}	
    	 });
        
    	 
    	   		var mapOptions = {
    	   			center : new google.maps.LatLng(19.231652, 72.861622),
    	   			zoom : 15,
    	   			mapTypeId : google.maps.MapTypeId.ROADMAP
    	   		};
    	   		var map = new google.maps.Map(document.getElementById("map"),mapOptions);
    	   		var infoWindow = new google.maps.InfoWindow();
    	   		var lat_lng = new Array();
    	   		var latlngbounds = new google.maps.LatLngBounds();
    	   		
    	   		for (i in markers) 
    	   		{
    	   			var data = markers[i];
    	   			//alert("abc");
    	   			//alert(data['lat'] +"-"+ data['lng']);
    	   			var myLatlng = new google.maps.LatLng(data['lat'], data['lng']);
    	   			lat_lng.push(myLatlng);
    	   			var marker = new google.maps.Marker({
    	   				position : myLatlng,
    	   				map : map,
    	   				title : data['title']
    	   			});
    	   			latlngbounds.extend(marker.position);
    	   			(function(marker, data) {
    	   				google.maps.event.addListener(marker, "click", function(e) {
    	   					infoWindow.setContent(data['description']);
    	   					infoWindow.open(map, marker);
    	   				});
    	   			})(marker, data);
    	   		}
    	   		map.setCenter(latlngbounds.getCenter());
    	   		map.fitBounds(latlngbounds);

     }


   
        $(document).ready(function() {
        	//google.maps.event.addDomListener(window, 'load', initialize);
        	$('#employeeId').change(function(){
        		
        		 var employeeDetailsId=$('#employeeId').val();
        		
        		 /*var latlngArray=latlng.split(",");
        		initializeDynamic(latlngArray[0],latlngArray[1]);
        		$('#map').change();
        		GetAddress(latlngArray[0],latlngArray[1]); */
        		$('#areaTblData').empty();
        		var lat,lng;
        		$.ajax({
    				type : "GET",
    				url : "${pageContext.request.contextPath}/fetchEmployeeDetails?employeeDetailsId="+employeeDetailsId,
    				beforeSend: function() {
    						$('.preloader-background').show();
    						$('.preloader-wrapper').show();
    			           },
    				success : function(data) {
    					employeeDetils=data.employeeDetails;
    					areaListt=data.areaList;
    					
    					lat=employeeDetils.latitude;
    					lng=employeeDetils.longitude;
    					var srno=1;
    					for(var i=0; i<areaListt.length; i++)
						{						
    						$("#areaTblData").append("<tr>"+
											        "<td>"+srno+"</td>"+
											        "<td>"+areaListt[i].name+"</td>"+
											    	"</tr>");	
    						srno=srno+1;
						}	
						//alert("done");
						$("#areaTblData").change();
    					
    					initializeDynamic(lat,lng,employeeDetils.name);
    	        		GetAddress(lat,lng);
    	        		
    				$('.preloader-wrapper').hide();
    				$('.preloader-background').hide();
    			},
    			error: function(xhr, status, error) {
    				$('.preloader-wrapper').hide();
    				$('.preloader-background').hide();
    				  //alert(error +"---"+ xhr+"---"+status);
    				$('#addeditmsg').modal('open');
        	     	$('#msgHead').text("Message : ");
        	     	$('#msg').text("Something Went Wrong"); 
        	     		setTimeout(function() 
    					  {
        					$('#addeditmsg').modal('close');
    					  }, 1000);
    				}	
        	 });
        		
        		
        		
        	});
        	$('#departmentId').change(function(){
        		$('#areaTblData').empty();
        		$('#currentLocation').val('');     
            	$('#currentLocation').change();
        		initializeMultipleDynamic();
        	});
        });
    </script>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> <br>
        <div class="row ">
              
            <div class="col s12 m6 l6">
            <div class="col l4 m4 s12">
                 <select id="departmentId">                 
                 <option value="0" selected>Choose Department</option>
                 <c:if test="${not empty departmentList}">
					<c:forEach var="listValue" items="${departmentList}">
                         <option value="${listValue.departmentId}"><c:out value="${listValue.name}" /></option>
                     </c:forEach>
                     </c:if>
                </select>
            </div>
            <div class="col l4 m4 s12">
                 <select id="employeeId">
                    	 <option value="0" selected>Choose Employee</option>
                </select>
            </div>
            <div class="col l12 m12 s12">
                <table class="tblborder centered highlight striped" width="100%">
                 <thead>
                <tr>
                	<th>Sr.No</th>
                	<th>Area</th>
                </tr>
                </thead>
                <tbody id="areaTblData">
                </tbody>
                </table>
                  <br>  <br>
            </div>
             <div class="input-field col s12 m12 l12"> 
          <i class="material-icons prefix">location_on</i>
          <textarea  id="currentLocation"  class="materialize-textarea" readonly></textarea>
          <label for="currentLocation" class="black-text">Current Location</label>
      
                           
            </div>
                
            </div>
            
            <div class="col s12 m6 l6 hoverable z-depth-3"> 
            <div id="map"   style="border:0;width: 100%;height:500px;">
            
            </div>
            
                 <!-- <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.1647819147424!2d72.85943331447078!3d19.231648986998827!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b0d1bc8cd01f%3A0x5fde5207e9aa0924!2sVaisansar+Technologies+PVT.+LTD.!5e0!3m2!1sen!2sin!4v1511759567678" width="100%" height="530" frameborder="0" style="border:0"></iframe> -->
                           
            </div>

        </div>









        <br>
     
     	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
    
   
    </main>





    <!--content end-->
</body>

</html>