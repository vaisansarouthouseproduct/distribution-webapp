<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
			<html>

			<head>
				<%@include file="components/header_imports.jsp" %>


					<script type="text/javascript">
						var checkedId = [];
						var mobileNumberCollection = "";
						$(document).ready(function () {

							var msg = "${saveMsg}";
							//alert(msg);
							if (msg != '' && msg != undefined) {
								$('#addeditmsg').find("#modalType").addClass("success");
								$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
								$('#addeditmsg').modal('open');
								//$('#msgHead').text("Brand Message");
								$('#msg').text(msg);
							}
							/* CHECK ALL CHECKBOX */
							$("#checkAll").click(function () {
								var colcount = $('#tblData').DataTable().columns().header().length;
								var cells = $('#tblData').DataTable().column(colcount - 1).nodes(), // Cells from 1st column
									state = this.checked;
								//alert(state);
								for (var i = 0; i < cells.length; i += 1) {
									cells[i].querySelector("input[type='checkbox']").checked = state;
								}

							});
							/* CHECK SINGLE SINGLE CHECKBOX */
							$("input:checkbox").change(function (a) {

								var colcount = $('#tblData').DataTable().columns().header().length;
								var cells = $('#tblData').DataTable().column(colcount - 1).nodes(), // Cells from 1st column

									state = false;
								var ch = false;

								for (var i = 0; i < cells.length; i += 1) {
									//alert(cells[i].querySelector("input[type='checkbox']").checked);

									if (cells[i].querySelector("input[type='checkbox']").checked == state) {
										$("#checkAll").prop('checked', false);
										ch = true;
									}
								}
								if (ch == false) {
									$("#checkAll").prop('checked', true);
								}

								//alert($('#tblData').DataTable().rows().count());
							});
							/* SET MOBILE NUMBERS AND OPEN SMS MODAL */
							$('#sendsmsid').click(function () {

								$('#mobileEditSms').removeAttr('checked');
								$("#mobileEditSms").change();
								mobileNumberCollection = '';

								var count = parseInt('${count}')
								checkedId = [];
								//chb
								var idarray = $("#employeeTblData")
									.find("input[type=checkbox]")
									.map(function () { return this.id; })
									.get();
								var j = 0;

								for (var i = 0; i < idarray.length; i++) {
									idarray[i] = idarray[i].replace('chb', '');
									if ($('#chb' + idarray[i]).is(':checked')) {
										checkedId[j] = idarray[i].split("_")[0];
										mobileNumberCollection = mobileNumberCollection + idarray[i].split("_")[1] + ",";
										j++;
									}
								}
								mobileNumberCollection = mobileNumberCollection.substring(0, mobileNumberCollection.length - 1);
								$('#mobileNoSms').val(mobileNumberCollection);
								$('#mobileNoSms').change();
								$('#smsText').html('');
								$('#smsSendMesssage').html('');

								if (checkedId.length == 0) {
									$('#addeditmsg').find("#modalType").addClass("warning");
									$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
									$('#addeditmsg').modal('open');
									// $('#msgHead').text("Message : ");
									$('#msg').text("Select Company For Send Message");
								}
								else {
									if (checkedId.length == 1) {
										$('#smsText').val('');
										$('#smsSendMesssage').html('');
										$('.mobileDiv').show();
										$('#sendMsg').modal('open');
									}
									else {
										$('#smsText').val('');
										$('#smsSendMesssage').html('');
										$('.mobileDiv').hide();
										$('#sendMsg').modal('open');
									}
								}
							});
							/* SEND MESSAGE VALIDATION AND SEND MESSAGE TO SELECTED COMPANY */
							$('#sendMessageId').click(function () {


								$('#smsSendMesssage').html('');
								var smsText = $("#smsText").val();
								if (checkedId.length == 0) {
									$('#smsSendMesssage').html("<font color='red'>Select Employee For Send Message</font>");
									return false;
								}
								if (smsText === "") {
									$('#smsSendMesssage').html("<font color='red'>Enter Message Text</font>");
									return false;
								}

								var checkedIds = "";
								for (var i = 0; i < checkedId.length; i++) {
									checkedIds += checkedId[i] + ",";
								}
								checkedIds = checkedIds.substring(0, checkedIds.length - 1);

								$.ajax({
									type: "post",
									url: "${pageContext.request.contextPath}/sendSMSTOCompany",
									data: {
										smsText: $('#smsText').val(),
										shopsIds: checkedIds,
										mobileNumber: $('#mobileNoSms').val()
									},
									//async: false,
									success: function (data) {
										if (data === "Success") {
											$('#smsSendMesssage').html("<font color='green'>Message send SuccessFully</font>");
											$('#smsText').val('');
										}
										else {
											$('#smsSendMesssage').html("<font color='red'>Message sending Failed</font>");
										}
									}
								});
							});
							var table = $('#tblData').DataTable();
							table.destroy();
							$('#tblData').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": "_INPUT_" //search
								},
								autoWidth: false,
								columnDefs: [
									{ 'width': '1%', 'targets': 0 },
									{ 'width': '1%', 'targets': 1 },
									{ 'width': '1%', 'targets': 2 },
									{ 'width': '2%', 'targets': 3 },
									{ 'width': '2%', 'targets': 4 },
									{ 'width': '1%', 'targets': 5 },
									{ 'width': '1%', 'targets': 6 },
									{ 'width': '1%', 'targets': 7 },
									{ 'width': '1%', 'targets': 8 }
								],
								lengthMenu: [
									[50, 75, 100, -1],
									['50 ', '75 ', '100', 'All']
								],


								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = [30, 80, 80, 70, 100, 180]
													}
												})
												var tableNode;
												for (i = 0; i < doc.content.length; ++i) {
													if (doc.content[i].table !== undefined) {
														tableNode = doc.content[i];
														break;
													}
												}

												var rowIndex = 0;
												var tableColumnCount = tableNode.table.body[rowIndex].length;

												if (tableColumnCount > 6) {
													doc.pageOrientation = 'landscape';
												}
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];

												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										},
										{
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
										},
										{
											extend: 'print',
											className: 'printButton waves-effect waves-light grey hide-on-med-and-down1 lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: '.print-col'
											},
										},
										{
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 hide-on-med-and-down light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}

							});
							
							$("select")
								.change(function () {
									var t = this;
									var content = $(this).siblings('ul').detach();
									setTimeout(function () {
										$(t).parent().append(content);
										$("select").material_select();
									}, 200);
								});
							$('select').material_select();
							$('.dataTables_filter input').attr("placeholder", "Search");
							 //if there is mobile device the element contain this class will be hidden
							if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
							}
							var table = $('#tblData').DataTable(); // note the capital D to get the API instance
							var column = table.columns('.toggle');
							column.visible(false);
							$('#showColumn').on('click', function () {
								//console.log(column);
								column.visible(!column.visible()[0]);

							});



						});
						/* SHOW CITY LIST ACCORDING COMPANY ID */
						function showCities(companyId) {
							$("#tbcitylist").empty();
							$.ajax({
								type: "post",
								url: "${pageContext.request.contextPath}/fetchCompanyCities?companyId=" + companyId,
								dataType: "json",
								success: function (data) {
									//console.log(data);
									for (var i = 0, len = data.length; i < len; ++i) {
										var city = data[i];
										$("#tbcitylist").append("<tr>" +
											"<td>" + (i + 1) + "</td>" +
											"<td>" + city.name + "</td>" +
											"</tr>");
									}

									$('.modal').modal();
									$('#cityList').modal('open');
								},
								error: function (xhr, status, error) {
									Materialize.Toast.removeAll();
									Materialize.toast('City List Not Found', '4000', 'teal lighten-2');
								}
							});
						}

					</script>
					<style>
						table.dataTable tbody td {
							padding: 0 2px !important;
						}

						.dataTables_wrapper {

							margin-left: 2px !important;
						}

						#cityList {
							width: 37% !important;
						
						}

						@media only screen and (max-width: 600px) {
							#cityList {
							width: 90% !important;
							overflow-y: auto;
						}
							/* .btnAlign{
								text-align: center !important;
							} */
						}

						@media only screen and (min-width: 601px) and (max-width: 992px) {
							#cityList {
							width:60% !important;
	
						}
						}

						table.dataTable thead th,
						table.dataTable thead td {
							padding: 10px 10px;
							border-bottom: 1px solid #111;
						}
					</style>

			</head>

			<body>
				<!--navbar start-->
				<%@include file="components/navbar.jsp" %>
					<!--navbar end-->
					<!--content start-->
					<main class="paddingBody">

						<!--Add New Supplier-->
						<div class="row">
							<br/>
							<div class="col s12 l3 m6 left btnAlign">
								<a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/addCompany">
									<i class="material-icons left">add</i>Add Company</a>
							</div>
							
							<div class="col s12 l12 m12 left">
								<br/>
								<table class="striped highlight centered  display " id="tblData" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th class="print-col hideOnSmall">Sr. No.</th>
											<th class="print-col">Company Name</th>
											<!-- <th class="print-col">User Name</th>
                        <th class="print-col">Password</th> -->
											<th class="print-col">Mobile No/Telephone No</th>
											<th class="print-col hideOnSmall">GST No</th>
											<th class="print-col hideOnSmall">Email Id</th>
											<th class="print-col hideOnSmall">Address</th>
											<th>City List</th>
											<th class="">Action</th>
											<th class="hideOnSmall">
												<a  class="modal-trigger cursorPointer" id="sendsmsid">
													<i class="material-icons tooltipped" data-position='right' data-delay='50' data-tooltip='Send Message' style="margin-left:-22%;">mail</i>
												</a>
												<br>
												<input type="checkbox" class="filled-in checkAll" id="checkAll" />
												<label for="checkAll"></label>
											</th>
										</tr>
									</thead>

									<tbody id="employeeTblData">
										<% int rowincrement=0; %>
											<c:if test="${not empty companyList}">
												<c:forEach var="listValue" items="${companyList}">
													<c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
													<tr>
														<td>
															<c:out value="${rowincrement}" />
														</td>
														<td>
															<c:out value="${listValue.companyName}" />
														</td>
														<%-- <td><c:out value="${listValue.userId}" /></td>
	                        <td><c:out value="${listValue.password}" /></td> --%>
															<td>
																<c:out value="${listValue.contact.mobileNumber}" />
																<c:if test="${listValue.contact.telephoneNumber!=''}">
																	/
																	<c:out value="${listValue.contact.telephoneNumber}" />
																</c:if>
															</td>
															<td>
																<c:out value="${listValue.gstinno==''?'NA':listValue.gstinno}" />
															</td>
															<td>
																<c:out value="${listValue.contact.emailId==''?'NA':listValue.contact.emailId}" />
															</td>
															<td>
																<c:out value="${listValue.address}" />
															</td>
															<td>
																<button class="btn" type="button" onclick="showCities(${listValue.companyId})">View</button>
															</td>
															<td>
																<a class="btn-flat" href="${pageContext.servletContext.contextPath}/fetchCompany?companyId=${listValue.companyId}">
																	<i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Edit">edit</i>
																</a>
																<a class="btn-flat modal-trigger" href="#crential_${listValue.companyId}">
																	<i class="material-icons tooltipped" data-position="left" data-delay="50" data-tooltip="Show Credential">person</i>
																</a>
															</td>
															<!-- Modal Trigger -->
															<%-- <td><a href="#delete${rowincrement}" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> --%>
																<td>

																	<input type="checkbox" class="filled-in" id="chb${listValue.companyId}_${listValue.contact.mobileNumber}" />
																	<label for="chb${listValue.companyId}_${listValue.contact.mobileNumber}"></label>

																</td>
													</tr>
													<div class="row modal deleteModal" id="crential_${listValue.companyId}">
														<div class="modal-content">
															<div class="col s12 m12 l12">
																<h5 class="center-align">
																	<u>Credentials</u>
																	<i class="material-icons modal-close right">clear</i>
																</h5>
															</div>
															<div class="col s12 m12 l12">
																<h5 class="center">
																	<b>User Id : </b>${listValue.userId}</h5>
															</div>
															<div class="col s12 m12 l12">
																<h5 class="center">
																	<b>Password : </b>${listValue.password}</h5>
															</div>
														</div>
														<div class="modal-footer">
															<div class="col s12 m12 l12 center-align">
																<a class="modal-action modal-close waves-effect  btn red">Close</a>
															</div>
														</div>
													</div>

												</c:forEach>
											</c:if>
									</tbody>
								</table>

								<!-- Modal Structure for sendMsg -->

								<div id="sendMsg" class="modal">

									<div class="modal-content">


										<div class="fixedDiv">
											<div class="center-align" style="padding:0">
												<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
											</div>
										</div>

										<div class="scrollDiv">

											<div class="row mobileDiv" style="margin-bottom:0">
												<div class="col s12 l9 m9 input-field" style="padding-left:0">
													<label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
													<input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" readonly>

												</div>
												<div class="col s12 l2 m2 right-align" style="margin-top:10%">
													<input type="checkbox" id="mobileEditSms" />
													<label for="mobileEditSms" class="black-text">Edit</label>

												</div>
											</div>
											<div class="input-field">
												<label for="smsText" class="black-text">Type Message</label>
												<textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
											</div>
											<div class="input-field center">
												<font color='red'>
													<span id="smsSendMesssage"></span>
												</font>
											</div>
											<div class="fixedFooter">
												<div class="col s6 m4 l3 right-align" style="margin-left:3%">
													<a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
												</div>
												<div class="col s6 m6 l2">
													<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>


												</div>
											</div>
										</div>
										<br>
										<br>
										<br>


									</div>


								</div>


								<!-- Modal Structure for View City List Details -->
								<div id="cityList" class="row modal">
									<div class="modal-content">
										<h5 class="center">
											<u>City List</u>
											<i class="material-icons right modal-close">clear</i>
										</h5>
										<table class="centered tblborder" id="modalTable">
											<thead>
												<tr>
													<th class="print-col">Sr.No</th>
													<th class="print-col ">City Name</th>
												</tr>
											</thead>
											<tbody id="tbcitylist">
											</tbody>
										</table>
									</div>
									<div class="modal-footer">
										<div class="col s12 m12 l12 center">
											<a href="#!" id="productDetailsClose" class="modal-action modal-close waves-effect center btn red">Close</a>
										</div>
									</div>
								</div>


							</div>
						</div>

						<div class="row">
							<div class="col s12 m12 l8">
								<div id="addeditmsg" class="modal">
									<div class="modal-content" style="padding:0">
										<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
										<!--  <h5 id="msgHead"></h5> -->

										<h6 id="msg" class="center"></h6>
									</div>
									<div class="modal-footer">
											<div class="col s12 center">
													<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
											</div>
											
										</div>
								</div>
							</div>
						</div>

					</main>
					<!--content end-->
			</body>

			</html>