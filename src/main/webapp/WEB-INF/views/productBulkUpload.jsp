<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<body>

<h1>Excel file upload example</h1>

				<c:choose>
					<c:when test="${not empty msg}">
						<br />
						<div class="alert alert-warning alert-dismissable"
							style="border-radius: 30px; width: 450px; margin: 40px auto;">

							<a class="close cursorPointer" data-dismiss="alert" aria-label="close">X</a>
							<center>
								<h3 style="color: red; margin: 0;">${msg}</h3>
							</center>
						</div>
					</c:when>
				</c:choose>
<div>
<h4>1. Product Excel Upload</h4>
<form method="POST" action=${pageContext.request.contextPath}/upload-product enctype="multipart/form-data">
    <input type="file" name="productFile" /><br/><br/>
    <input type="submit" value="Submit" />
</form>
</div>

<%-- <div>
<h4>2. User Excel Upload</h4>
<form method="POST" action=${pageContext.request.contextPath}/upload-user enctype="multipart/form-data">
    <input type="file" name="userProductFile" /><br/><br/>
    <input type="submit" value="Submit" />
</form>
</div> --%>

<%-- <div>
<h4>3. User Daily Bill Excel Upload</h4>
<form method="POST" action=${pageContext.request.contextPath}/upload-user_daily_bill enctype="multipart/form-data">
    <input type="file" name="userDailyBillFile" /><br/><br/>
    <input type="submit" value="Submit" />
</form>
</div> --%>


</body>
</html>