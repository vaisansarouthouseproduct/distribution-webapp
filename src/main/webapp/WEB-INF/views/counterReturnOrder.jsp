<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <html>

        <head>
            <%@include file="components/header_imports.jsp"%>
                <script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript">
 		const counterOrderId="${counterOrderId}";
 		$(".paymentSection").hide();
</script>
<script type="text/javascript" src="resources/js/counterReturn.js"></script>
                <script>
                    $(document).ready(function () {
                        //allowed only number without decimal
                        $('.qty').keypress(function (event) {
                            var key = event.which;
                            if (!(key >= 48 && key <= 57 || key === 13))
                                event.preventDefault();
                        });
                        //allowed only number without decimal
                        $('.num').keypress(function (event) {
                            var key = event.which;

                            if (!(key >= 48 && key <= 57 || key === 13))
                                event.preventDefault();
                        });


                        $(".chequeForm").hide();
                        $(".otherForm").hide();
                        $(".partialForm").hide();

                        $('.creditDueDate').hide();
                        $(".chequeForm").hide();
                        $(".otherForm").hide();
                        $(".partialForm").hide();
                        $(".dueDateExtraPayDiv").hide();


                        $('#cash').click(function () {
                            $(".chequeForm").hide();
                            $(".otherForm").hide();

                        });
                        $('#cheque').click(function () {

                            $('.chequeForm').show();
                            $(".otherForm").hide();

                        });

                        $('#other').click(function () {

                            $('.chequeForm').hide();
                            $(".otherForm").show();

                        });

                        $('#partialPay').click(function () {
                            $(".partialForm").show();
                            $('.paymentTypeSection').show();
                            $('.creditDueDate').hide();
                        });
                        $('#instantPay').click(function () {

                            $('.partialForm').hide();
                            $('.creditDueDate').hide();
                            $('.paymentTypeSection').show();
                        });
                        $('#credit').click(function () {

                            $('.partialForm').hide();
                            $('.creditDueDate').show();
                            $('.paymentTypeSection').hide();
                        });
                        $('.select2').material_select('destroy');
                        $('.select2').select2({
                            placeholder: "Choose your Product",
                            allowClear: true
                        });
                        $(".select2").select2({
                            width: 'resolve'
                        });
                        /* $(".btnPay").click(function(){
                            var businessList=$("#businessList option:selected").val();
                        	
                            if(businessList=="" ||businessList==undefined){
                                $("#custInfo").modal('open');
                            }
                        }); */

                        $('#logo-collapse').toggle();
                        $('#logo-collapse-full').toggle();
                        $('.element-sideNav').toggle();
                        $("#slide").toggleClass("reveal-open");
                        $('.side-nav').toggleClass("reveal-open");
                        $('main').toggleClass("paddingBody");
                        $('main').toggleClass("intro");

				
		  	});

                </script>


                <style>
                    #custInfo {
                        width: 35% !important;
                    }

                    #tblProduct input {
                        margin-bottom: 0px;
                        height: 1rem;
                    }

                    .select2 {
                        width: 100% !important;
                        float: right;
                        height: 3rem !important;
                    }

                    .select-wrapper span.caret {
                        color: initial;
                        position: absolute;
                        right: 0;
                        top: 0;
                        bottom: 0;
                        height: 10px;
                        margin: 10px 0;
                        font-size: 10px;
                        line-height: 10px;
                    }

                    .select.select-wrapper span.caret {
                        color: initial;
                        position: absolute;
                        right: 0;
                        top: 0;
                        bottom: 0;
                        height: 10px;
                        margin: 18px 0 !important;
                        font-size: 10px;
                        line-height: 10px;
                    }

                    [type="checkbox"]+label {
                        font-size: 0.9rem !important;
                    }
                    .cardPanel{
                        min-height: 500px !important; 
                    }
                </style>
        </head>

        <body>
            <!--navbar start-->
            <%@include file="components/navbar.jsp"%>
                <!--navbar end-->
                <!--content start-->
                <main class="paddingBody">
                    <input type="hidden" id="memberIdentifier" value="nonMember">

                    <div class="row">
                        <div class="col l4 m4 s12">
                            <div class="card z-depth-4 cardPanel">
                                <div class="card-content">
                                    <span class="card-title sidenav-color white-text center grey-text text-lighten-2" style="padding: 5px; font-size: 30px; font-family: Serif;">Information</span>
                                    <div class="row bottomPadding">
                                        <table class="highlight centered" id="tableCounterInfo">
                                            <tr>
                                                <th>Counter Order Id</th>
                                                <th><span id="counterOrderId"></span></th>
                                            </tr>
                                            <tr>
                                                <th>Customer Name/Shop Name</th>
                                                <th><span id="customerNameId"></span></th>
                                            </tr>
                                            <tr>
                                                <th>GateKeeper Name</th>
                                                <th><span id="gatekeeperNameId"></span></th>
                                            </tr>
                                            <tr>
                                                <th>Date of Order</th>
                                                <th><span id="dateOfOrder"></span></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div 2 for bill and main products-->
                        <div class="col l8 m8 s12">
                            <div class="card z-depth-4 cardPanel">
                                <div class="card-content" style="height: 54vh; overflow-y: auto;">
                                    <span class="card-title grey-text text-lighten-2 sidenav-color white-text center" style="padding: 5px; font-size: 30px; font-family: Serif;">Bill</span>
                                    <table id="tblProduct" class="highlight centered">
                                        <thead>
                                            <tr>
                                                <!-- <th>Sr No.</th> -->
                                                <th width="40%">Product</th>
                                                <!-- <th>Curr Qty</th> -->
                                                <th>MRP</th>
                                                <th>Qty</th>
                                                <th>Rtn. Qty</th>
                                                <th>Disc.(%)</th>
                                                <th>Disc. Amt.</th>
                                                <th>Total(&#8377;)</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cartTbl">


                                        </tbody>
                                    </table>
                                </div>  

                                <div class="card-action" style="padding-bottom: 0.1%;">
                                    <div class="row">
                                        <div class="col l3 m3 s6 " style="margin-top: 4%;">
                                            <strong>
                                                <span id="balanceTotalValue">Return Amount: &#8377;
                                                    <font color="blue">
                                                        <b>0</b>
                                                    </font>
                                                </span>
                                            </strong>
                                        </div>
                                        <div class="input-field col l9 m9 s6 " style="margin-top: 4%;">
                                            <label for="mainCommentId"><span class="red-text">*</span> Comment</label>
                                            <input id="mainCommentId" type="text" class="" required> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col l12 m12 s12 center">
                                            <button type="button" value="Return" id="returnSubmitId" class="disableButtons waves-effect waves-light btn btnPay btnEffect" style="width: 120px; padding: 0px !important;">
                                                &nbsp; Return
                                                <i class="material-icons tiny right btnHover">navigate_next</i>
                                            </button>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>

                    <!--end of div 2 for bill and main products-->


                </main>
        </body>

        </html>