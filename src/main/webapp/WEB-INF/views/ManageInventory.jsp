<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>

<head>
	<%@include file="components/header_imports.jsp"%>

	<script>var myContextPath = "${pageContext.request.contextPath}"</script>

	<!-- this two js file attach when login doing other than admin-->
	<c:if test="${sessionScope.loginType!='Admin'}">
		<script type="text/javascript" src="resources/js/hashtable.js"></script>
		<script type="text/javascript" src="resources/js/manageInventory.js"></script>
	</c:if>
	<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<style>
		tr [colspan="2"] {
			text-align: center;
		}

		tr,
		td,
		th {
			text-align: center;
		}

		/* .modal .modal-content {
    padding: 10px !important;
} */
		[type="checkbox"]+label {
			padding-left: 25px !important;
		}

		#setDamageModal {
			width: 45%;
		}

		.select2 {
			width: 100% !important;
			float: right;
			height: 3rem !important;
		}

		.select2-dropdown {
			z-index: 9001 !important;
		}

		#order {
			width: 80% !important;
			max-height: 80% !important;
		}


		@media only screen and (min-width:992px) {
			#singleOrder {
				width: 40%;
			}

			.singleOrderChbEdit {
				margin-top: 4%;
				width: 12% !important;
			}

			.singleOrderMobileNoField {
				padding-right: 0;
				width: 20% !important;
			}

			.modalBtnLeftMargin {
				margin-left: 19%;
			}
			#add{
				max-height: 80% !important;
			}
		}

		@media only screen and (max-width:992px) {
			#order {
				width: 95% !important;
				max-height: 90% !important;
			}

			.singleOrderChbEdit {
				margin-top: 4%;
				width: 10% !important;
			}

			#setDamageModal {
				width: 70%;
			}

			.singleOrderMobileNoField {
				padding-right: 0;
				width: 22% !important;
			}

			.modalBtnLeftMargin {
				margin-left: 15%;
			}
		}
		@media only screen and (max-width:600px){
			
			#setDamageModal,#add {
				width: 95%;
			}

		}
		.dropdown-content {
			max-height: 250px !important;
		}
		table.dataTable thead th,
		table.dataTable thead td {
		padding: 10px 10px;
		border-bottom: 1px solid #111;
		}

		label {
			color: black !important;
		}

		.noBottomMargin {
			margin: 0;
		}
	</style>
	<script>
		$(document).ready(function () {
			var table = $('#tblData').DataTable();
			table.destroy();
			$('#tblData').DataTable({
				"oLanguage": {
					"sLengthMenu": "Show _MENU_",
					"sSearch": "_INPUT_" //search
				},
				autoWidth: false,
				columnDefs: [
					{ 'width': '1%', 'targets': 0 },
					{ 'width': '15%', 'targets': 1 },
					{ 'visible': false, 'targets': 2 },
					{ 'width': '10%', 'targets': 3 },
					{ 'width': '12%', 'targets': 4 },
					{ 'width': '1%', 'targets': 5 },
					{ 'width': '1%', 'targets': 6 },
					{ 'width': '1%', 'targets': 7 },
					{ 'width': '1%', 'targets': 8 },
					{ 'width': '1%', 'targets': 9 },
					{ 'width': '1%', 'targets': 10 },
					{ 'width': '1%', 'targets': 11 },
					{ 'width': '1%', 'targets': 12 },
					{ 'width': '1%', 'targets': 13 },
					{ 'width': '1%', 'targets': 14 }

				],
				lengthMenu: [
					[10, 25., 50, -1],
					['10 ', '25 ', '50 ', 'All']
				],


				//dom: 'lBfrtip',
				dom: '<lBfr<"scrollDivTable"t>ip>',
				buttons: {
					buttons: [
						//      {
						//      extend: 'pageLength',
						//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
						//  }, 
						{
							extend: 'pdf',
							className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
							customize: function (doc, config) {
								doc.content.forEach(function (item) {
									if (item.table) {
										item.table.widths = [40, 100, 70, 50, 70, 40, 40, 50, 50, 70, 70, 70]
									}
								});
								var tableNode;
								for (i = 0; i < doc.content.length; ++i) {
									if (doc.content[i].table !== undefined) {
										tableNode = doc.content[i];
										break;
									}
								}

								var rowIndex = 0;
								var tableColumnCount = tableNode.table.body[rowIndex].length;

								if (tableColumnCount > 6) {
									doc.pageOrientation = 'landscape';
								}
								/*for customize the pdf content*/
								doc.pageMargins = [5, 20, 10, 5];
								doc.defaultStyle.fontSize = 8;
								doc.styles.title.fontSize = 12;
								doc.styles.tableHeader.fontSize = 11;
								doc.styles.tableFooter.fontSize = 11;
								doc.styles.tableHeader.alignment = 'center';
								doc.styles.tableBodyEven.alignment = 'center';
								doc.styles.tableBodyOdd.alignment = 'center';
							},
						},
						{
							extend: 'excel',
							className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'print',
							className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: ':visible.print-col'
							},
						},
						{
							extend: 'colvis',
							className: 'colvisButton  waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
							collectionLayout: 'fixed two-column',
							align: 'left'
						},
					]
				}

			});
			/* var table = $('#tblData').DataTable();
			table.columns('.hide').visible( false ); */
			$("select").change(function () {
				var t = this;
				var content = $(this).siblings('ul').detach();
				setTimeout(function () {
					$(t).parent().append(content);
					$("select").material_select();
				}, 200);
			});
			$('select').material_select();
			$('.dataTables_filter input').attr("placeholder", "Search");
			$("#BankDetails").css("display", "none");
			$(".cash").change(function () {
				$("#BankDetails").css("display", "none");
			});
			$(".cheque").change(function () {
				$("#BankDetails").css("display", "block");
			});
			//hide column depend on login
			var tableData = $('#tblData').DataTable(); // note the capital D to get the API instance
			var columnTbl = tableData.columns('.toggle');
			console.log(column);
			if (${ sessionScope.loginType != 'Admin' }){
				columnTbl.visible(true);
			}else {
				columnTbl.visible(false);
			}
			if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);

				$('.editOrderDiv').removeClass('right-align');
			}
			
        });



	</script>


</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">
		<br>
		<c:if test="${sessionScope.loginType!='Admin'}">
			<div class="row">
				<div class="col s6 l3 m3 right right-align">
					<a class="btn waves-effect waves-light blue-gradient"
						href="${pageContext.request.contextPath}/openAddMultipleInventory">
						<i class="material-icons left hide-on-small-only">add</i>Add Inventory
					</a>
				</div>
				<div class="col s10 l3 m3 left offset-s2 hide-on-small-only">
					<button class="btn waves-effect waves-light blue-gradient" onclick="showOrderProductQuantity(0)">
						<i class="material-icons left hide-on-med-and-down">add</i>Order Products
					</button>
				</div>
				<div class="col s10 l3 m3 left hide-on-small-only">
					<button id="openSetDamageModal" class="btn waves-effect waves-light blue-gradient modal-trigger">
						<i class="material-icons left">autorenew</i>Set Damage
					</button>
				</div>
				<div class="col s6 l3 m3 right-align editOrderDiv">
					<a class="btn waves-effect waves-light blue-gradient"
						href="${pageContext.request.contextPath}/fetchLast24HoursOrdersForEdit">
						<i class="material-icons left hide-on-small-only">edit</i>Edit Orders
					</a>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="col s12 l12 m12">

				<table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
					<thead>
						<%-- <c:choose>
                    <c:when test="${sessionScope.loginType!='Admin'}"> --%>
						<tr>
							<th rowspan="2" class="print-col hideOnSmall">Sr. No</th>
							<th rowspan="2" class="print-col ">Product</th>
							<th rowspan="2" class="print-col hide">Product Code</th>
							<th rowspan="2" class="print-col hideOnSmall">Category</th>
							<th rowspan="2" class="print-col hideOnSmall">Brand</th>
							<th rowspan="2" class="print-col hideOnSmall">HSNCode</th>
							<th rowspan="2" class="print-col hideOnSmall">Unit Price</th>
							<th rowspan="2" class="print-col">MRP</th>
							<th rowspan="2" class="print-col ">Qty</th>
							<th rowspan="2" class="print-col hideOnSmall">Threshold</th>
							<th rowspan="2" class="print-col hideOnSmall">Taxable Amount</th>
							<th rowspan="2" class="print-col hideOnSmall">Tax</th>
							<th rowspan="2" class="print-col">Total Amount</th>
							<th colspan="2" class="toggle hideOnSmall">Action</th>
						</tr>
						<tr>
							<th class="toggle hideOnSmall">Add</th>
							<th class="toggle hideOnSmall">Order</th>
						</tr>
						<%-- </c:when>
                     <c:otherwise>
                     		<tr>
	                            <th class="print-col hideOnSmall">Sr. No</th>
	                            <th class="print-col">Product</th>
	                            <th class="print-col hide">Product Code</th>
	                            <th class="print-col hideOnSmall" >Category</th>
	                            <th class="print-col hideOnSmall">Brand</th>                            
	                            <th class="print-col hideOnSmall">HSNCode</th>
	                            <th class="print-col hideOnSmall">Unit Price</th>
	                            <th class="print-col">MRP</th>
	                            <th class="print-col">Qty</th>
	                            <th class="print-col hideOnSmall">Threshold</th>
	                            <th class="print-col hideOnSmall">Taxable Amount</th>
	                            <th class="print-col hideOnSmall">Tax</th>
	                            <th class="print-col">Total Amount</th>
	                        </tr>
                     </c:otherwise>
                     </c:choose> --%>
					</thead>

					<tbody id="inventoryTB">
						<c:if test="${not empty inventoryProductsList}">
							<c:forEach var="listValue" items="${inventoryProductsList}">
								<tr>
									<td>
										<c:out value="${listValue.srno}" />
									</td>
									<td>
										<c:out value="${listValue.product.productName}" />
									</td>
									<td>
										<c:out value="${listValue.product.productCode}" />
									</td>
									<td>
										<c:out value="${listValue.product.categories.categoryName}" />
									</td>
									<td>
										<c:out value="${listValue.product.brand.name}" />
									</td>
									<td class="wrapok">
										<c:out value="${listValue.product.categories.hsnCode}" />
									</td>
									<td class="wrapok">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
											value="${listValue.product.rate}" />
									</td>
									<td class="wrapok">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
											value="${listValue.rateWithTax}" />
									</td>
									<td>
										<c:out value="${listValue.product.currentQuantity}" />
									</td>
									<td>
										<c:out value="${listValue.product.threshold}" />
									</td>
									<td class="wrapok">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
											value="${listValue.taxableAmount}" />
									</td>
									<td class="wrapok">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
											value="${listValue.tax}" />
									</td>
									<td class="wrapok">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
											value="${listValue.taxwithAmount}" />
									</td> <%-- <c:if test="${sessionScope.loginType!='Admin'}"> --%>
									<td>
										<button class="btn-flat tooltipped" data-position="left" data-delay="50"
											data-tooltip="Add Inventory"
											onclick="showAddQuantity(${listValue.product.productId})"><i
												class="material-icons">add_shopping_cart</i></button>
									</td>
									<td>
										<button class="btn-flat tooltipped" data-position="left" data-delay="50"
											data-tooltip="Single Order"
											onclick="showOrderOneProduct(${listValue.product.productId})">
											<i class="material-icons">mail_outline</i> 
											</button>
													</td><%--    </c:if>   --%> </tr> </c:forEach> </c:if> </tbody>
													</table> </div> </div> <div class="modal row" id="setDamageModal">
												<form action="${pageContext.request.contextPath}/defineDamage"
													id="defineDamageForm" method="post">
													<div class="modal-content">
														<div class="col s12 m12 l12 center">
															<h5 class="center">
																<u>Set Damage</u><i
																	class="material-icons modal-close right">clear</i>
															</h5>
														</div>
														<div class="row" style="margin-bottom: 0">
															<div class="input-field col s6 l6  m6">
																<label for="damageProductBrandId" class="active">Select
																	Brand</label> <select id="damageProductBrandId"
																	class="select2" name="damageProductBrandId">
																	<option value="0" selected>Choose Brand</option>
																	<c:if test="${not empty brandlist}">
																		<c:forEach var="listValue" items="${brandlist}">
																			<option value="${listValue.brandId}">
																				<c:out value="${listValue.name}" />
																			</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
															<div class="input-field col s6 l5  m6 offset-l1 ">
																<label for="damageProductCategoryId"
																	class="active">Select
																	Category</label> <select
																	id="damageProductCategoryId" class="select2"
																	name="damageProductCategoryId">
																	<option value="0" selected>Choose Category</option>
																	<c:if test="${not empty categorieslist}">
																		<c:forEach var="listValue"
																			items="${categorieslist}">
																			<option value="${listValue.categoryId}">
																				<c:out
																					value="${listValue.categoryName}" />
																			</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
															<div class="input-field col s6 l6  m6">
																<label for="damageProductId" class="active"> <span
																		class="red-text">*</span>Select Product
																</label> <select id="damageProductId" class="select2"
																	name="productId" required>
																	<option value="" selected>Choose Product</option>
																</select>
															</div>
															<div class="input-field col s6 l5  m6 offset-l1">
																<input id="damageProductCurrentQty" type="text"
																	class="grey lighten-3"
																	name="damageProductCurrentQty" readonly>
																<label for="damageProductCurrentQty"
																	class="active black-text">Current
																	Quantity</label>
															</div>
														</div>
														<div class="row">
															<div class="input-field col s6 l6  m6">
																<input id="damageProductDamageQty" type="text"
																	name="damageQuantity" required> <label
																	for="damageProductDamageQty"
																	class="active black-text">Damage
																	Quantity</label>
															</div>
															<div class="input-field col s6 l5  m6 offset-l1 ">
																<label for="damageProductDamageOption" class="active">
																	<span class="red-text">*</span>Select Damage Type
																</label> <select id="damageProductDamageOption"
																	name="damageType" required>
																	<option value="claimed" selected>Supplier Damage
																	</option>
																	<option value="notClaimed">Self Damage</option>
																</select>
															</div>
														</div>
														<div class="row">
															<div class="input-field col s12 l12  m12">
																<input id="damageProductDamageQtyReason" type="text"
																	name="damageQuantityReason" required> <label
																	for="damageProductDamageQtyReason"
																	class="active black-text">Damage
																	Quantity Reason</label>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s12 m12 l12 center">
															<button type="button" id="damageProductSubmitButton"
																class="waves-effect btn">Ok</button>
														</div>
													</div>
												</form>
			</div>
			<!-- Modal Structure for Tax Details -->
			<div id="tax" class="modal" style="width: 40%;">
				<div class="modal-content">
					<h5 class="center">
						<u>Tax Details</u><i class="modal-close material-icons right">clear</i>
					</h5>

					<br>

					<table class="centered highlight tblborder" border="2">
						<thead>
							<tr>
								<th colspan="2">%CGST</th>
								<th colspan="2">%SGST</th>
								<th colspan="2">%IGST</th>
								<th rowspan="2">Total tax Amount</th>
							</tr>
							<tr>
								<th>%</th>
								<th>Amount</th>
								<th>%</th>
								<th>Amount</th>
								<th>%</th>
								<th>Amount</th>
							</tr>	
						</thead>
						<tbody>
							<tr>
								<td id="cgstPerId"></td>
								<td id="cgstAmtId"></td>
								<td id="sgstPerId"></td>
								<td id="sgstAmtId"></td>
								<td id="igstPerId"></td>
								<td id="igstAmtId"></td>
								<td id="taxAmountId"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="divider"></div>

				<div class="modal-footer row">
					<div class="col s12 m12 l12 center">
						<a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
					</div>
				</div>
			</div>

			<!--Modal Structure for Add Inventory-->
			<div id="add" class="modal row">
				<div class="modal-content">

					<h5 class="center">
						<u>Add Inventory</u><i class="modal-close material-icons right">clear</i>
					</h5>

					<form action="${pageContext.request.contextPath}/saveOneInventory" method="post"
						id="addQuantityForm" class="col s12 l12 m12">
						<input id="productlistinput" type="hidden" class="validate" name="productIdList"> <input
							id="productIdForAddQuantity" type="hidden">
						<div class="row" style="margin-bottom: 5px">
							<div class="input-field col s6 l5  m4 offset-l1">
								<input id="productName" type="text" class="grey lighten-3" readonly> <label
									for="productName" class="active black-text">Product</label>
							</div>

							<div class="input-field col s6 l2  m4">
								<!--<i class="material-icons prefix">mode_edit</i>-->
								<input id="supplierMRPRate" type="text" class="grey lighten-3" readonly> <label
									for="supplierMRPRate" class="active black-text">MRP</label>
							</div>

							<div class="input-field col s6 l3  m4">
								<!--<i class="material-icons prefix">mode_edit</i>-->
								<input id="supplierRate" type="text" class="grey lighten-3" readonly> <label
									for="supplierRate" class="active black-text">Rate</label>
							</div>

						</div>
						<div class="row" style="margin-bottom: 5px">
							<div class="input-field col s6 l5  m4 offset-l1">
								<label for="supplierIdForaddQuantity" class="active">Select
									Supplier</label> <select id="supplierIdForaddQuantity" name="supplierId">
								</select>
							</div>

							<div class="input-field col s6 l2  m4">
								<!--<i class="material-icons prefix">mode_edit</i>-->
								<input id="addquantity" type="text"> <label for="addquantity" class="active black-text">
									<span class="red-text">*</span>Quantity
								</label>
							</div>
							<div class="input-field col s6 l3  m4">
								<input id="paymentDate" type="text" name="paymentDate" title="Enter Payment Date"
									class="datepicker disableDate">
								<label for="paymentDate" class="black-text"> <span class="red-text">*</span>Payment Date
								</label>
							</div>
						</div>
						<div class="row" style="margin-bottom: 5px">
							
							<div class="col s12 m4 l2 discountClass offset-l1 offset-m2" style="margin-top: 3%;">
								<input id="discountOnMRPCheck" type="checkbox" checked /> <label
									for="discountOnMRPCheck" style="padding-left: 25px;">Discount on MRP</label>
							</div>
							
							<div class="col s6 m4 l2 discountClass offset-l1" style="margin-top: 3%;">
								<input id="percentageCheck" type="checkbox" checked /> <label for="percentageCheck"
									style="padding-left: 25px;">Percentage</label>
							</div>
							<div class="input-field col l2 m4 s6 discountClass">
								<input id="discountId" type="text" class="num" title="only number allowed" required>
								<label for="discountId">Discount Percent</label><br>
							</div>
							<div class="input-field col l2 offset-l1 m4 s6 discountClass">
								<input id="discountCalculated" type="text" class="num" value=""
									title="only number allowed" readonly>
								<label for="discountCalculated">Discount Amount</label><br>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s6 l5  m4 offset-l1">
								<input id="finalTotalAmount" type="text" value="0" readonly class="grey lighten-3">
								<label for="finalTotalAmount" class="active black-text">Total Amount(Without
									Tax)</label>
								<!-- <h6 class="red-text"><b>Total Amount(Without Tax)</b> : <span id="finalTotalAmount">0</span></h6>-->
								<br>
							</div>
							<div class="input-field col s6 m4 l5">
								<input id="finalTotalAmountWithTax" type="text" value="0" readonly
									class="grey lighten-3"> <label for="finalTotalAmountWithTax"
									class="active black-text">Total
									Amount(With Tax)</label>
								<!-- <h6 class="red-text"><b>Total Amount(With Tax)</b> : <span id="finalTotalAmountWithTax">0</span></h6> -->

								<br>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s6 l5  m4 offset-l1">
								<input id="netfinalTotalAmount" type="text" value="0" readonly class="grey lighten-3">
								<label for="netfinalTotalAmount" class="active black-text">Total Net Amount(Without
									Tax)</label>
								<!-- <h6 class="red-text"><b>Total Net Amount(Without Tax)</b> : <span id="netfinalTotalAmount">0</span></h6>-->
								<br>
							</div>
							<div class="input-field col s6 m4 l5">
								<input id="netfinalTotalAmountWithTax" type="text" value="0" readonly
									class="grey lighten-3"> <label for="netfinalTotalAmountWithTax"
									class="active black-text">Total
									Net Amount(With Tax)</label>
								<!-- <h6 class="red-text"><b>Total Net Amount(With Tax)</b> : <span id="netfinalTotalAmountWithTax">0</span></h6> -->

								<br>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s6 m4 l5 offset-l1">
								<input id="selectedDate" type="text" name="billDate" title="Enter Select Date"
									class="datepicker previousDate">
								<label for="selectedDate" class="active black-text">Bill
									Date</label>
							</div>
							<div class="input-field col s6 m4 l5">
								<input id="bill_number" type="text" name="billNumber"> <label for="bill_number"
									class="active black-text">Bill Number</label>
							</div>
						</div>

					</form>

				</div>

				<div class="modal-footer">
					<div class="col s12 m12 l12">
						<div class="divider"></div>
					</div>
					<div class="col s12 l3 m4 offset-l2 offset-m1 right-align modalBtnLeftMargin hide-on-small-only">
						<a href="${pageContext.servletContext.contextPath}/addSupplier"
							class="btn waves-effect teal lighten-2" style="letter-spacing: 0.2px;">Add New Supplier</a>
					</div>
					<div class="col s6 m2 l2 center-align">
						<a href="#!" class="modal-action waves-effect modal-close btn red">close</a>
					</div>
					<div class="col s6 m4 l3 left-align">
						<a href="#!" class="modal-action waves-effect  btn" id="addQuantityId">ADD Inventory</a>
					</div>

				</div>

			</div>
			<!-- Modal Structure for single order -->
			<div id="singleOrder" class="modal">
				<div class="modal-content">
					<h5 class="center-align">
						<u>Order Information</u><i class="material-icons modal-close right">clear</i>
					</h5>



					<form action="${pageContext.servletContext.contextPath}/orderBook" method="post"
						id="orderBookFormOneOrderId" class="col s12 l12 m12">
						<input id="productWithSupplikerlistOneOrderId" type="hidden" class="validate"
							name="productIdList"> <input type='hidden' id='currentUpdateRowId'>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="input-field col s12 m4 l4">
									<input id="productIdOneOrderId" type="hidden" readonly> <input
										id="productNameOneOrderId" type="text" readonly class="grey lighten-3"> <label
										for="productNameOneOrderId" class="black-text">Product</label>
								</div>
								<div class="input-field col s12 m4 l4">
									<input id="brandIdOneOrderId" type="hidden" readonly> <input id="brandOneOrderId"
										type="text" readonly class="grey lighten-3">
									<label for="brandOneOrderId" class="black-text">Brand</label>
								</div>
								<div class="input-field col s12 m4 l4">
									<input id="categoryIdOneOrderId" type="hidden" readonly>
									<input id="categoryOneOrderId" type="text" readonly class="grey lighten-3"> <label
										for="categoryOneOrderId" class="black-text">Category</label>
								</div>

							</div>

							<div class="col s12 m12 l12">
								<br>
								<div class="input-field col s12 m4 l4">
									<label for="supplierIdOneOrderId" class="active"> <span class="red-text">*</span>
										Select Supplier
									</label> <select id="supplierIdOneOrderId" class="select2" name="supplierId">
										<option value="0" selected>Choose Supplier Name</option>
									</select>

								</div>
								<div class="input-field col s12 m4 l4">
									<input id="orderQuantityOneOrderId" type="text" class="validate" required> <label
										for="orderQuantityOneOrderId" class="black-text"> <span
											class="red-text">*</span>Quantity
									</label>
								</div>


								<div class="input-field col s12 m3 l3 singleOrderMobileNoField">
									<input id="supplierMobileNumberOneOrderId" type="text" class="grey lighten-3"
										minlength="10" maxlength="10"> <label for="supplierMobileNumberOneOrderId"
										class="black-text">Mobile
										Number</label>
								</div>
								<div class="col s12 m2 l2 right right-align singleOrderChbEdit">
									<input type="checkbox" id="OrderMobileNumbercheckerOneOrderId" required /> <label
										for="OrderMobileNumbercheckerOneOrderId" class="black-text">Edit</label>
									<!-- <a href="#!" class="modal-action waves-effect waves-green btn">Edit</a> -->
								</div>
							</div>

						</div>

					</form>

				</div>
				<div class="divider"></div>
				<div class="modal-footer row" style="margin-bottom: 0;">
					<div class="col s12 m4 l3 offset-l2 offset-m1">
						<c:choose>
							<c:when test="${sessionScope.loginType=='CompanyAdmin'}">

								<a href="${pageContext.servletContext.contextPath}/addSupplier"
									class="modal-action waves-effect teal lighten-2 btn"
									style="letter-spacing: 0.2px; margin-top: 3%">Add New
									Supplier</a>

							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>
					<div class="col s6 m2 l2 center-align">
						<a href="#!" class="modal-action waves-effect modal-close btn red">close</a>
					</div>

					<div class="col s6 m3 l2 left-align">
						<button type="button" id="orderProductsButtonOneOrderId"
							class="modal-action waves-effect  btn">Send sms</button>
					</div>


				</div>
			</div>




			<!-- Modal Structure for order multiple order -->

			<div id="order" class="modal">
				<div class="modal-content" style="padding: 10px;">
					<h5 class="center-align">
						<u>Order Information</u><i class="material-icons modal-close right">clear</i>
					</h5>

					<div class="row">
						<form action="${pageContext.servletContext.contextPath}/orderBook" method="post"
							id="orderBookForm" class="col s12 l12 m12">
							<input id="productWithSupplikerlist" type="hidden" class="validate" name="productIdList">
							<input type='hidden' id='currentUpdateRowId'>
							<div class="col s12 l6 m12 noPadding">
								<div class="col s12 m12 l12 noPadding">
									<div class="input-field col s12 m3 l6">
										<label for="supplierIdForOrder" class="active"> <span
												class="red-text">*</span>Select Supplier
										</label> <select id="supplierIdForOrder" class="select2" name="supplierId">
											<option value="0" selected>Choose Supplier Name</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
										</select>
										<!--<label>Supplier Name</label>-->
									</div>
									<div class="input-field col s12 m3 l6">
										<label for="brandIdForOrder" class="active"> <span
												class="red-text">*</span>Select Brand
										</label> <select id="brandIdForOrder" class="select2" name="brandId">
											<option value="0" selected>Choose Brand</option>

										</select>
									</div>


									<div class="input-field col s12 m3 l6">
										<label for="categoryIdForOrder" class="active"> <span
												class="red-text">*</span>Select Category
										</label> <select id="categoryIdForOrder" class="select2" name="categoryId">
											<option value="0" selected>Choose Category</option>

										</select>
									</div>
									<div class="input-field col s12 m3 l6">
										<label for="productIdForOrder" class="active"> <span
												class="red-text">*</span>Select Product
										</label> <select id="productIdForOrder" class="select2" name="productId">
											<option value="0" selected>Choose Product</option>

										</select>
									</div>
								</div>
								<div class="col s12 m12 l12 noPadding">
									<div class="input-field col s12 m3 l3">
										<input id="orderQuantity" type="text" class="validate" required>
										<label for="orderQuantity" class="black-text"> <span
												class="red-text">*</span>Quantity
										</label>
									</div>
									<div class="input-field col s12 m3 l3">
										<input id="supplierMRPRateForOrder" type="text" readonly class="grey lighten-3">
										<input id="supplierRateForOrder" type="hidden" class="grey lighten-3">
										<label for="supplierRateForOrder" class="black-text">MRP</label>
									</div>
									<div class="input-field col s12 m3 l4">
										<label for="supplierMobileNumber" class="black-text">Mobile
											Number</label> <input id="supplierMobileNumber" type="text"
											class="grey lighten-3" minlength="10" maxlength="10">
										<!-- <label for="mobile"></label> -->
									</div>
									<div class="col s12 m2 l2 left-align" style="margin-top: 4%">
										<input type="checkbox" id="OrderMobileNumberchecker" required />
										<label for="OrderMobileNumberchecker" class="black-text">Edit</label>
										<!-- <a href="#!" class="modal-action waves-effect waves-green btn">Edit</a> -->
									</div>


								</div>


								<div class="input-field col s12 m12 l12 center" style="margin:2% 0">
									<button id="addOrderProduct" type="button" name="addOrderCart"
										class="waves-effect  btn">Add</button>
								</div>

							</div>


							<div class="col s12 l6 m12">
								<table class="tblborder centered">
									<thead>
										<tr>
											<th>Sr No.</th>
											<th>Supplier Name</th>
											<th>Product</th>
											<th>MRP</th>
											<th>Qty</th>
											<th>Edit</th>
											<th>Cancel</th>
										</tr>
									</thead>
									<tbody id="orderCartTb">
										<!-- <tr>
                                            <td>1</td>
                                            <td>Ankit</td>
                                            <td>John</td>
                                            <td>300</td>
                                            <td>5</td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">edit</i></button></td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">cancel</i></button></td>
                                        </tr> -->
									</tbody>
								</table>
							</div>



						</form>
					</div>

				</div>

				<div class="modal-footer row" style="margin-bottom: 0;">
					<div class="divider col s12 m12 l12"></div>
					<div class="col s12 m3 l2 offset-l4 offset-m2 right-align">
						<c:choose>
							<c:when test="${sessionScope.loginType=='CompanyAdmin'}">

								<a href="${pageContext.request.contextPath}/addSupplier"
									class="modal-action waves-effect waves-green btn teal lighten-2"
									style="letter-spacing: 0.2px; margin-top: 3%">Add New
									Supplier</a>

							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>

					<div class="col s6 m2 l1">
						<a href="#!" class="modal-action waves-effect modal-close btn red">close</a>
					</div>
					<div class="col s6 m4 l2 left-align">
						<button type="button" id="orderProductsButton" onclick="orderConfirmationSupplierOrder()"
							class="modal-action waves-effect  btn">Order Products</button>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col s12 m12 l8">
					<div id="addeditmsg" class="modal">
						<div class="modal-content" style="padding: 0">
							<div class="center   white-text" id="modalType" style="padding: 3% 0 3% 0"></div>
							<!--  <h5 id="msgHead"></h5> -->

							<h6 id="msg" class="center"></h6>
						</div>
						<div class="modal-footer">
							<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>

						</div>
					</div>
				</div>
			</div>
	</main>


	<!--content end-->
</body>

</html>