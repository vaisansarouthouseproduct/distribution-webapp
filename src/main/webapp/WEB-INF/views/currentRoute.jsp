<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

			<head>
				<%@include file="components/header_imports.jsp" %>
					<script type="text/javascript" src="resources/js/moment.js"></script>
					<style>
						.mapContainer {
							width: 50%;
							position: relative;
						}

						.mapContainer a.direction-link {
							position: absolute;
							top: 15px;
							right: 15px;
							z-index: 100010;
							color: #FFF;
							text-decoration: none;
							font-size: 15px;
							font-weight: bold;
							line-height: 25px;
							padding: 8px 20px 8px 50px;
							background: #0094de;
							background-image: url('direction-icon.png');
							background-position: left center;
							background-repeat: no-repeat;
						}

						.mapContainer a.direction-link:hover {
							text-decoration: none;
							background: #0072ab;
							color: #FFF;
							background-image: url('resources/img/direction.png');
							background-position: left center;
							background-repeat: no-repeat;
						}

						span.selectLabel_100 {
							position: absolute;
							top: -1rem;
							left: 5px;
							font-size: 0.8rem;
						}
					</style>
					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0"></script>
					<script type="text/javascript">
						const CompanyName = "${sessionScope.sessionCopamnyName}";
						$(document).ready(function(){

							$('.readMore').click(function () {
                    var type=$(this).attr("data-type");
                    if(type=='MORE'){
                        //$(this).parent().find('p.truncateToThreeLine').css({ 'height': '300px' });
                        $(this).parent().find('p.readableData').removeClass('truncateToThreeLine');
                        $(this).attr('data-type','LESS');
                        $(this).text('Read Less');
                    }
                    else if(type=='LESS'){
                        //$(this).parent().find('p.truncateToThreeLine').css({ 'height': '80px' });
                        $(this).parent().find('p.readableData').addClass('truncateToThreeLine');
                        $(this).attr('data-type','MORE');
                        $(this).text('Read More');
                    }
                    else{
                        
                    }
                    
                });
							var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);
							
						    $('.datepicker.oneDate').pickadate({
						        container: 'body',
						        selectMonths: false, // Creates a dropdown to control month
						        selectYears: false, // Creates a dropdown of 15 years to control year,
						        today: 'Today',
						        clear: 'Clear',
						        close: 'Ok',
						        format: 'yyyy-mm-dd',
						        closeOnSelect: 'true', // Close upon selecting a date,

						        // this function closes after date selection Close upon
						        // selecting a date,
						        onSet: function (ele) {
						            if (ele.select) {
						                this.close();
						            }
						        },
						        onStart: function () {
						            var date = new Date();
						            this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);

						        },
						        disable: [
						            { from: [0, 0, 0], to: yesterday }


						        ]
						    });

						});
					</script>
					<script>var myContextPath = "${pageContext.request.contextPath}"</script>
					<script type="text/javascript" src="resources/js/currentRoute.js"></script>
					<style>
							.locationTable th,
					.locationTable td {
							border: 1px solid #9e9e9e;
							padding: 2px;
							background-color: white;
						}

						.sticky {
							position: sticky;
							top: -1px;
						}

							.locationTable .wrapper {
								height: 400px;
								overflow-y: auto;
							}

						.locationTable .wrapper table {
							position: relative;
							width: 100%;
						}


						.tabs .tab a {
							color: #303641;
							display: block;
							width: 100%;
							height: 30px;
							padding: 0 24px;
							font-size: 14px; // background-color: white !important;
							/* line-height: 0.2em; */
							text-overflow: ellipsis;
							overflow: hidden;
							transition: color .28s ease;
							/* border-radius: 10px; */
							border:2px solid transparent;
							border-image-slice: 1;
						}

						label {
							color: black;
						}
						#mapModal{
							height:500px !important;
							width:70% !important
						}
						#map-canvas{
							border:0;width: 100%;
							height:463px !important;
						}
						.actionDiv{
							margin-top: 3%;
						}
						.tblCurrentLocation .name{
							width:300px !important;
						}
						.tblCurrentLocation .srNo{
							width:60px !important;
						}
						.truncateToThreeLine {
							/* white-space: nowrap; */
							overflow: hidden;
							display: block;
							text-overflow: ellipsis;
							/* text-align: justify; */
							max-width: 100%;
							display: -webkit-box;
							height: 65px;
							-webkit-line-clamp: 3;
							-webkit-box-orient: vertical;
						}
						td p{
							margin:0 !important;
						}
						#tblRoute .name{
							
							width:300px !important;
						}
						#tblRoute .start{
							
							width:300px !important;
						}
						#tblRoute .task{

							width:300px !important;
						}
						#tblRoute .end{
							
							width:300px !important;
						}
						@media only screen and (max-width: 600px) {
							#mapModal{
							height:400px !important;
							width:90% !important
						}	
						#map-canvas{
							border:0;width: 100%;
							height:395px !important;
						}
						.actionDiv{
							margin-top: 5%;
						}
						.tblCurrentLocation .name{
							width:500px !important;
						}
						.tblCurrentLocation .srNo{
							width:20px !important;
						}
						#tblRoute .name{
							width:400px !important;
						}
						#tblRoute .start{
							width:400px !important;
						}
						#tblRoute .task{
							width:400px !important;
						}
						#tblRoute .end{
							width:400px !important;
						}
						}

						 @media only screen and (min-width: 601px) and (max-width: 992px) {
							#mapModal{
							height:500px !important;
							width:80% !important
						}	
						.actionDiv{
							margin-top: 4%;
						}
						 }

					</style>

				
			</head>

			<body>
				<!--navbar start-->
				<%@include file="components/navbar.jsp" %>
					<!--navbar end-->
					<!--content start-->
					<main class="paddingBody">
						<br>
						<div class="row ">

							<div class="col s12 m12 l12">

								<div class="col s12 m12 l12" style="padding:0">
									<div class="col s12 l12 m12" style="margin-bottom:20px;padding:0">
										<ul class="tabs">
											<li class="tab col s6 m6 l6 left-align" style="padding-right:10px">
												<a href="#tblCurrentLocation" class="active">Current Location</a>
											</li>
											<li class="tab col s6 m6 l6 right-align" style="padding-right:0">
												<a href="#tblRoute">Route</a>
											</li>
										</ul>
									</div>

									<div class="col s12 m12 l12 formBg">
										<div class="col l3 m4 s12 offset-l2  input-field actionDiv">
											<span class="active selectLabel_100" for="departmentId">Select Department</span>
											<select id="departmentId" class="select2_1001">
												<option value="0" selected>Choose Department</option>
												<c:if test="${not empty departmentList}">
													<c:forEach var="listValue" items="${departmentList}">
														<option value="${listValue.departmentId}">
															<c:out value="${listValue.name}" />
														</option>
													</c:forEach>
												</c:if>
											</select>
										</div>
										<div class="col l3 m4 s12 input-field actionDiv">
											<span class="active selectLabel_100" for="employeeId">Select Employee</span>
											<select id="employeeId" class="select2_1001">
												<option value="0" selected>All</option>
											</select>
										</div>
										<div class="input-field col s12 l3 m3" id="">
											<form action="" method="post">
												<div class="input-field col s6 m8 l7">
													<input type="text" id="oneDate" class="datepicker oneDate" placeholder="Choose date" name="startDate">
													<label for="oneDate" class="black-text">Pick Date</label>
												</div>
												<div class="input-field col s6 m2 l2"> 
													<button type="button" id="oneDateButton" class="btn">View</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div id="tblCurrentLocation" class="col l12 m12 s12 locationTable" style="padding:0">
									<br>
									<div class="wrapper">
										<table class="centered" width="100%">
											<thead>
												<tr>
													<th class="sticky srNo">Sr.No</th>
													<th class="sticky name">Name</th>
													<th class="sticky">Current Location</th>
													<th class="sticky">Mobile No.</th>
													<th width="25px" class="sticky">
														<i class="material-icons">location_on</i>
													</th>
												</tr>
											</thead>
											<tbody id="empCurrentLocation">
											<!--	 <tr>
									                <td>1</td>
									                <td>SAchin Sawant</td>
									                <td>Western Exp. Highwey , m.d road ,borwali west</td>
													<td>0123456789</td>
													<td>123</td>
									                </tr>  -->  
											</tbody>
										</table>
									</div>
								</div>
								<div id="tblRoute" class="col l12 m12 s12 locationTable" style="padding:0">
									<br>
									<div class="wrapper">

										<table class="centered routeTable">
											<thead>
												<tr>
													<th class="sticky" width="3%">Sr.No</th>
													<th class="sticky name" >Name</th>
													<!--<th width="8%" class="sticky">Mobile No.</th>-->
													<th  class="sticky start">Start</th>
													<th class="sticky end">End</th>
													<th  class="sticky task">Task Perform</th>
													<th width="5%" class="sticky">
														<i class="material-icons">location_on</i>
													</th>
												</tr>
											</thead>
											<tbody id="routeListWithTime">
													<tr>
									                <td>1</td>
									                <td>SAchin Sawant</td>
									                <td>Western Exp. Highwey , m.d road ,borwali west</td>
													<td>0123456789</td>
													<td><p class="truncateToThreeLine readableData">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurie</p>
														<span class="readMore blue-text right" data-type="MORE">Read More</span>
													</td>

													<td>123</td>
									                </tr>
											</tbody>
										</table>
									</div>


								</div>
							
							

							</div>
						</div>
						<br>
						<div class="row">
							<div class="col s12 m12 l12">
								<div id="addeditmsg" class="modal">
									<div class="modal-content" style="padding:0">
										<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
										<!-- <h5 id="msgHead" class="red-text"></h5> -->
										<h6 id="msg" class="center"></h6>
									</div>
									<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12">

								<div class="modal mapModal" id="mapModal">
									<span class="white" style="position: absolute;top:2%;right:1%;z-index:1005;display:block;">
										<i class="material-icons modal-close center">clear</i>
									</span>

									<div class="modal-content" style="padding:0">
										<div id="map-canvas" ></div>
									</div>
								</div>
							</div>
						</div>
					</main>

			</body>

	</html>