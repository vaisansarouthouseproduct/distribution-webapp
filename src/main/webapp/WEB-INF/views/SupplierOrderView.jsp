<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
        <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
            <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
                <html>

                <head>
                    <%@include file="components/header_imports.jsp" %>
                        <!-- <style>
        nav {
            /*height: 100px;*/
            background-color: red;
            background-image: linear-gradient( to right, #3498ce, #311b92);
        }
    </style> -->
                        <script type="text/javascript">
                            /* show supplier with hyper link when company login only */
                            var showSupplierNavigation = ${ sessionScope.loginType== 'CompanyAdmin'};
                            $(document).ready(function () {
                                $(".showDates").hide();
                                $("#oneDateDiv").hide();

                                $(".rangeSelect").click(function () {
                                    $("#oneDateDiv").hide();
                                    $(".showQuantity").hide();
                                });

                                $(".pickdate").click(function () {
                                    $(".showDates").hide();
                                    $("#oneDateDiv").show();
                                });
                                var table = $('#tblData').DataTable();
							table.destroy();
							$('#tblData').DataTable({
								"oLanguage": {
									"sLengthMenu": "Show _MENU_",
									"sSearch": "_INPUT_" //search
								},
								autoWidth: false,
								columnDefs: [
									{ 'width': '2%', 'targets': 0 },
									{ 'width': '3%', 'targets': 1 },
									{ 'width': '1%', 'targets': 2 },
									{ 'width': '2%', 'targets': 3 },
									{ 'width': '2%', 'targets': 4 },
									{ 'width': '2%', 'targets': 5 },
									{ 'width': '3%', 'targets': 6 },
									{ 'width': '3%', 'targets': 7 }
								],
								lengthMenu: [
									[10, 25., 50, -1],
									['10 ', '25 ', '50 ', 'All']
								],


								//dom: 'lBfrtip',
								dom: '<lBfr<"scrollDivTable"t>ip>',
								buttons: {
									buttons: [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend: 'pdf',
											className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
											customize: function (doc, config) {
												doc.content.forEach(function (item) {
													if (item.table) {
														item.table.widths = ['*','*','*','*','*','*','*']
													}
												})
												var tableNode;
												for (i = 0; i < doc.content.length; ++i) {
													if (doc.content[i].table !== undefined) {
														tableNode = doc.content[i];
														break;
													}
												}

												var rowIndex = 0;
												var tableColumnCount = tableNode.table.body[rowIndex].length;

												if (tableColumnCount > 7) {
													doc.pageOrientation = 'landscape';
												}
												/*for customize the pdf content*/
												doc.pageMargins = [5, 20, 10, 5];
												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										},
										{
											extend: 'excel',
											className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										},
										{
											extend: 'print',
											className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
											//title of the page
											title: function () {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename: function () {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-' + month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions: {
												columns: ':visible.print-col'
											},
										},
										{
											extend: 'colvis',
											className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout: 'fixed two-column',
											align: 'left'
										},
									]
								}

							});
							$("select").change(function () {
								var t = this;
								var content = $(this).siblings('ul').detach();
								setTimeout(function () {
									$(t).parent().append(content);
									$("select").material_select();
								}, 200);
							});
							$('select').material_select();
							$('.dataTables_filter input').attr("placeholder", "Search");
							//if there is mobile device the element contain this class will be hidden
							if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
							}

                            });
                            /* show supplier order product details  by supplierOrderId*/
                            function showProductDetails(supplierOrderId) {
                                var basePath = "${pageContext.request.contextPath}";
                                $.ajax({
                                    url: "${pageContext.request.contextPath}/fetchSupplierOrderDetails?supplierOrderId=" + supplierOrderId,
                                    dataType: "json",
                                    success: function (data) {
                                        supplierOrderDetailsLists = data;
                                        var srno = 1;
                                        $("#supplierOrderDetailsData").empty();
                                        var srno = 0;
                                        var totalAll = 0, totalAmtWithTaxAll = 0;
                                        for (var i = 0; i < supplierOrderDetailsLists.length; i++) {
                                            supplierOrderDetails = supplierOrderDetailsLists[i];
                                            srno++;

                                            if (showSupplierNavigation) {
                                                supplierName = "<td><a href='" + basePath + "/fetchSupplierListBySupplierId?supplierId=" + supplierOrderDetails.supplier.supplierId + "'>" + supplierOrderDetails.supplier.name + "</a></td>";
                                            } else {
                                                supplierName = "<td>" + supplierOrderDetails.supplier.name + "</td>";
                                            }

                                            var mrp = ((parseFloat(supplierOrderDetails.supplierRate)) + ((parseFloat(supplierOrderDetails.supplierRate) * parseFloat(supplierOrderDetails.product.categories.igst)) / 100)).toFixedVSS(0);
                                            var correctAmoutWithTaxObj = calculateProperTax(mrp, parseFloat(supplierOrderDetails.product.categories.igst));

                                            var total = parseFloat(supplierOrderDetails.totalAmount).toFixedVSS(2);
                                            var totalAmtWithTax = parseFloat(supplierOrderDetails.totalAmountWithTax).toFixedVSS(2);

                                            $('#supplierOrderDetailsData').append("<tr><td class='hideColumnOnSmall'>" + srno + "</td>" +
                                                supplierName +
                                                "<td>" + supplierOrderDetails.product.productName + "</td>" +
                                                "<td class='hideColumnOnSmall'>" + supplierOrderDetails.product.brand.name + "</td>" +
                                                "<td class='hideColumnOnSmall'>" + supplierOrderDetails.product.categories.categoryName + "</td>" +
                                                "<td>" + supplierOrderDetails.quantity + "</td>" +
                                                "<td>" + parseFloat(correctAmoutWithTaxObj.mrp).toFixedVSS(2) + "</td>" +
                                                "<td class='hideColumnOnSmall'>" + parseFloat(supplierOrderDetails.totalAmount).toFixedVSS(2) + "</td>" +
                                                "<td>" + parseFloat(supplierOrderDetails.totalAmountWithTax).toFixedVSS(2) + "</td><tr>");
                                            totalAmtWithTaxAll = parseFloat(totalAmtWithTaxAll) + parseFloat(totalAmtWithTax);
                                            totalAll = parseFloat(totalAll) + parseFloat(total);
                                        }
                                        var colSpan;
                                        if ($.data.isMobile) {
                                            colSpan = "4";
                                        }
                                        else {
                                            colSpan = "7";
                                        }
                                        $("#supplierOrderDetailsData").append("<tr>" +
                                            "<td colspan='"+colSpan+"'><b>All Total</b></td>" +
                                            "<td  class='red-text hideColumnOnSmall'><b>" + totalAll.toFixedVSS(2) + "</b></td>" +
                                            "<td  class='red-text'><b>" + totalAmtWithTaxAll.toFixedVSS(2) + "</b></td>" +
                                            "</tr>");
                                        $('.modal').modal();
                                        $('#product').modal('open');
                                    }
                                });

                            }
                        </script>
                        <style>
                            .input-field {
                                position: relative;
                                margin-top: 0.5rem;
                            }
                            .productModal{
							width:60% !important;
						}
						@media only screen and (max-width: 600px) {
							.productModal{
							width:90% !important;
							border-radius: 10px !important;
                            
						}
						}
						@media only screen and (min-width: 601px) and (max-width: 992px) {
							.productModal{
							width:70% !important;
						}
						}
                        </style>
                </head>

                <body>
                    <!--navbar start-->
                    <%@include file="components/navbar.jsp" %>
                        <!--navbar end-->
                        <!--content start-->
                        <main class="paddingBody">
                            <br class="hide-on-small-only">
                            <div class="row">

                                <div class="col s6 m3 l3 right right-align actionDiv actionBtn">
                                    <!-- <div class="col s6 m4 l4 right"> -->
                                    <!-- Dropdown Trigger -->
                                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action
                                        <i class="material-icons right">arrow_drop_down</i>
                                    </a>
                                    <!-- Dropdown Structure -->
                                    <ul id='filter' class='dropdown-content'>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=today&suppplierId=${supplierId}">Today</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=yesterday&suppplierId=${supplierId}">Yesterday</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=last7days&suppplierId=${supplierId}">Last 7 Days</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=currentMonth&suppplierId=${supplierId}">Current Month</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=lastMonth&suppplierId=${supplierId}">Last Month</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=last3Months&suppplierId=${supplierId}">Last 3 Month</a>
                                        </li>
                                        <li>
                                            <a class="rangeSelect">Range</a>
                                        </li>
                                        <li>
                                            <a class="pickdate">Pick date</a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=viewAll&supplierId=${supplierId}">View All</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="input-field col s6 l5 m4 offset-l3 offset-m4 right actionDiv" id="oneDateDiv">
                                    <form action="${pageContext.request.contextPath}/showSupplierOrderReport" method="post">
                                        <input type="hidden" name="range" value="pickDate">
                                        <input type="hidden" name="supplierId" value="${supplierId}">
                                        <div class="input-field col s8 m6 l4">
                                            <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
                                            <label for="oneDate" class="black-text">Pick Date</label>
                                        </div>
                                        <div class="input-field col s3 m2 l2">
                                            <button type="submit" class="btn">View</button>

                                        </div>
                                    </form>
                                </div>
                                <div class="input-field col s12 l5 m5 offset-m3 offset-l1 right actionDiv rangeDiv">
                                    <form action="${pageContext.request.contextPath}/showSupplierOrderReport" method="post">
                                        <input type="hidden" name="range" value="range">
                                        <input type="hidden" name="supplierId" value="${supplierId}">
                                        <span class="showDates">
                                            <div class="input-field col s5 m4 l3">
                                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                                <label for="startDate">From</label>
                                            </div>
                                            <div class="input-field col s5 m4 l3">
                                                <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required>
                                                <label for="endDate">To</label>
                                            </div>

                                            <div class="input-field col s2 m2 l2">
                                                <button type="submit" class="btn">View</button>
                                            </div>
                                        </span>
                                    </form>
                                </div>

                                <div class="col s12 l12 m12">
                                    <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="print-col hideOnSmall">Sr.No.</th>
                                                <th class="print-col">Supplier Order Id</th>
                                                <th class="print-col">Product Details</th>
                                                <th class="print-col">Total Quantity</th>
                                                <th class="print-col hideOnSmall">Taxable Amount</th>
                                                <th class="print-col">Total Amount</th>
                                                <th class="print-col">Order Date</th>
                                                <th class="print-col hideOnSmall">Order Updated Date</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <% int rowincrement=0; %>
                                                <c:if test="${not empty supplierOrderLists}">
                                                    <c:forEach var="listValue" items="${supplierOrderLists}">
                                                        <c:set var="rowincrement" value="${rowincrement + 1}" scope="page" />
                                                        <tr ${listValue.status==false ? 'class="red lighten-4"' : ''}>
                                                            <td>
                                                                <c:out value="${rowincrement}" />
                                                            </td>
                                                            <td>
                                                                <c:out value="${listValue.supplierOrderId}" />
                                                            </td>
                                                            <td>
                                                                <button class="btn" onclick="showProductDetails('${listValue.supplierOrderId}')">View</button>
                                                            </td>
                                                            <td>
                                                                <c:out value="${listValue.totalQuantity}" />
                                                            </td>
                                                            <td>
                                                                <c:out value="${listValue.totalAmount}" />
                                                            </td>
                                                            <td>
                                                                <c:out value="${listValue.totalAmountWithTax}" />
                                                            </td>
                                                            <td>
                                                                <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.supplierOrderDatetime}" />
                                                                <c:out value="${dt}" />
                                                                <br/>
                                                                <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.supplierOrderDatetime}" />
                                                                <c:out value="${time}" />
                                                            </td>
                                                            <td>
                                                                <c:choose>
                                                                    <c:when test="${empty  listValue.supplierOrderUpdateDatetime}">
                                                                        NA
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.supplierOrderUpdateDatetime}" />
                                                                        <c:out value="${dt}" />
                                                                        <br/>
                                                                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.supplierOrderUpdateDatetime}" />
                                                                        <c:out value="${time}" />
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Modal Structure for View Product Details -->
                            <div id="product" class="modal productModal">
                                <div class="modal-content">
                                    <h5 class="center">
                                        <u>Product Details</u>
                                        <i class="modal-close material-icons right">clear</i>
                                    </h5>

                                    <br>
                                    <div class="scrollDivTable">
                                    <table border="2" class="centered tblborder">
                                        <thead>
                                            <tr>
                                                <th class="hideColumnOnSmall">Sr.No.</th>
                                                <th>Supplier Name</th>
                                                <th>Product Name</th>
                                                <th class="hideColumnOnSmall">Category</th>
                                                <th class="hideColumnOnSmall">Brand</th>
                                                <th>Qty</th>
                                                <th>MRP</th>
                                                <th class="hideColumnOnSmall">Taxable Amount</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="supplierOrderDetailsData"></tbody>
                                    </table>
                                </div>
                            </div>
                                <div class="modal-footer row1">

                                    <div class="col s12 m12 l12 center">
                                        <a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
                                    </div>

                                </div>
                            </div>




                        </main>
                        <!--content end-->
                </body>

                </html>