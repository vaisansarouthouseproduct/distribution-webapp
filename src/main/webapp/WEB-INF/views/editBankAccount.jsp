<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<html>

<head>
<%@include file="components/header_imports.jsp"%>

<style>
.containerOfBanking {
	margin: 0 auto;
	width: 85% !important;
}

th, td {
	padding: 4px 15px;
}

.addMarginRight {
	margin-right: 5%;
}

input {
	/* height: 1.3rem !important; */
	margin: -5px 0 0px 0 !important;
	border-bottom: 1px solid white !important;
}
table ,th ,td{
			border : 1px solid black;
			}
</style>
</head>

<body>
	<%@include file="components/navbar.jsp" %>
	<main class="paddingBody">

	<div class="row">

		<div class="col s12 m4 l12 center">
			<h5 class="center-align pageTitle">Update Bank Account</h5>
		</div>
	</div>
	<div class="row noMarginBottom">
		<div class="col s12 m12 l8 offset-l2">
			<table>
				<tr>
					<th>Account Holder Name <span class="red-text">*</span></th>
					<td><input placeholder="Account Holder Name"
						id="accountHolderName" type="text" class="validate center"
						value="${bankAccount.accountHolderName}"></td>
				</tr>
				<input type="hidden" value="${bankAccount.accountType}">
				<tr>
					<th>Account Number <span class="red-text">*</span></th>
					<td class="center"><input placeholder="Account Number"
						pattern="[0-9]{9,18}" id="accountNumber" type="text"
						class="validate center num" value="${bankAccount.accountNumber}"></td>
				</tr>
				<tr>
					<th>Account Type <span class="red-text">*</span></th>
					<td><select id="accountType" class="browser-default">
							<option value="Current">Current</option>
							<option value="Savings">Savings</option>
					</select></td>
					<!--<td><input placeholder="Account Type" id="accountType" type="text" class="validate center"></td>-->
				</tr>
				<%-- <tr>
					<th>Bank Name <span class="red-text">*</span></th>
					<td><input placeholder="Bank Name" id="bankName" type="text"
						class="validate center" value="${bankAccount.bank.bankName}"></td>
				</tr> --%>
					<tr>
							<th>Bank Name <span class="red-text">*</span></th>
							<td>
								<select id="bankId" class="browser-default">
								
								<c:if test="${not empty bankList}">
									<c:forEach var="listValue" items="${bankList}">
										<option value="<c:out value="${listValue.bankId}" />"${listValue.bankId==bankAccount.bankId?'selected':''}><c:out
										value="${listValue.bankName}" /></option>
									</c:forEach>
								</c:if>
									<!-- <option value="Current">Current</option>
									<option value="Savings">Savings</option> -->
								</select>
							</td>
						<!--<td><input placeholder="Account Type" id="accountType" type="text" class="validate center"></td>-->
					</tr>
				<tr>
					<th>IFSC Code <span class="red-text">*</span></th>
					<td><input placeholder="IFSC Code" id="ifscCode" type="text"
						class="validate center" value="${bankAccount.ifscCode}"></td>
				</tr>
				<tr>
					<th>Branch Name / Code <span class="red-text">*</span></th>
					<td><input placeholder="Branch Name / Code" id="branchName"
						type="text" class="validate center"
						value="${bankAccount.branchCode}"></td>
				</tr>
				<tr>
					<th>Bank Address <span class="red-text">*</span></th>
					<td><input placeholder="Bank Address" id="bankAddress"
						type="text" class="validate center"
						value="${bankAccount.bankAddress}"></td>
				</tr>
				<tr>
					<th>Minimum Balance <span class="red-text">*</span></th>
					<td><input placeholder="Minimum Balance" id="minimumBalance"
						type="text" class="validate center num"
						value="${bankAccount.minimumBalance}"></td>
				</tr>
				<tr>
					<th>Make this account default</th>
					<td class="center"><input name="makeDefault"
								class="with-gap" type="radio" id="makeDefault" /> <label
								for="makeDefault"></label></td>
					<%-- <c:choose>
						<c:when test="${true}">
							<td class="center"><input name="makeDefault"
								class="with-gap" type="radio" id="makeDefault" checked /> <label
								for="makeDefault"></label></td>
						</c:when>
						<c:otherwise>
							<td class="center"><input name="makeDefault"
								class="with-gap" type="radio" id="makeDefault" /> <label
								for="makeDefault"></label></td>

						</c:otherwise>
					</c:choose> --%>

				</tr>
			</table>
		</div>
		<div class="col s12 m12 l12 center"
			style="margin-top: 1%; margin-bottom: 1%;">
			<button onclick="addAccount();" type="button"
				class="waves-effect waves-light btn"
				style="padding: 0 1.5rem !important;">
				<i class="material-icons left">send</i> Update Bank Account
			</button>
		</div>
	</div>
	</main>
	<%@include file="components/footer.jsp"%>

	<script>
		var accountHolderName, accountType, bankName, ifscCode, branchName, bankAddress, minimumBalance, makeDefault, accountNumberExists;
		var accountTypeSelected = "${bankAccount.accountType}";

		$(document).ready(function() {
			
			
			 //allowed only numbers 
			$('#minimumBalance').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
			        event.preventDefault();
			});  
			 
			 
			 
							$("#accountType option[value=" + accountTypeSelected + "]")
							.attr("selected", "selected");
							$("#accountType").change();

					var isCheckedValue = "${bankAccount.primary}";
					if (isCheckedValue == "true") {
						$("#makeDefault").attr("checked", true);
					}
							$('input#accountNumber').typeWatch(
											{
												callback : function() {
													var inputValue = $(
															'input#accountNumber')
															.val();
													if (inputValue == "") {
														return false;
													} else {
														var patternMatch = /[-,!,@,#,,%,^,&,*,(,),_,-,+,=,/,?,]/g;
														var result = inputValue
																.match(patternMatch);
														if (result != null) {
															Materialize.Toast
																	.removeAll();
															Materialize
																	.toast(
																			'Only alphabets and number allowed',
																			'3000',
																			'toastError');
															return false;
														} else {
															var CheckBankAccountNumberRequest = {
																accountNumber : inputValue
															}
															// var CheckBankAccountNumberRequest = {
															// 	accountNumber : inputValue
															// }
															// check if account number exists
															$.ajax({
																		type : 'POST',
																		url : "${pageContext.request.contextPath}/checkAccountNumber",
																		headers : {
																			'Content-Type' : 'application/json'
																		},
																		data : JSON
																				.stringify(CheckBankAccountNumberRequest),
																		success : function(
																				response) {
																			console
																					.log(response);
																			if (response.success != true) {
																				Materialize.Toast.removeAll();
																				Materialize.toast(response.msg,
																								'3000',
																								'toastError');
																				accountNumberExists = true;
																			} else {
																				accountNumberExists = false;
																			}
																		},
																		error : function(
																				err) {
																			/*Show Dialog for Server Error*/
																			Materialize.Toast
																					.removeAll();
																			Materialize
																					.toast(
																							'Internal Server Error',
																							'3000',
																							'toastError');
																		}
																	});
														}
													}
												},
												wait : 750,
												highlight : false,
												captureLength : 1
											});
						});

		function addAccount() {
			accountHolderName = $("#accountHolderName").val();
			accountNumber = $("#accountNumber").val();
			accountType = $("#accountType option:selected").val();
			bankId = $("#bankId").val();
			ifscCode = $("#ifscCode").val();
			branchName = $("#branchName").val();
			bankAddress = $("#bankAddress").val();
			minimumBalance = $("#minimumBalance").val();
			if ($('#makeDefault').is(':checked')) {
				makeDefault = true;
			} else {
				makeDefault = false;
			}
			// validation
			if (accountHolderName == "" || accountNumber == ""
					|| bankName == "" || ifscCode == "" || branchName == ""
					|| bankAddress == "" || minimumBalance == "") {
				Materialize.Toast.removeAll();
				Materialize.toast('Please fill all mandatory fields', '3000',
						'toastError');
			} else if (accountNumberExists == true) {
				Materialize.Toast.removeAll();
				Materialize.toast('Account number already used!', '3000',
						'toastError');
			}
			//  else if (accountNumber.length < 9) {
			// 	Materialize.toast('Account number should be atleast 9 digit long', '3000', 'toastError');
			// }
			else {
				// object to send
				var bankAccount = {
					accountHolderName : accountHolderName,
					accountNumber : accountNumber,
					accountType : accountType,
					bank:{
						bankId: bankId
					},
					ifscCode : ifscCode,
					branchCode : branchName,
					bankAddress : bankAddress,
					minimumBalance : minimumBalance,
					primary : makeDefault
				}
				console.log(bankAccount);
				$.ajax({
							type : 'POST',
							url : "${pageContext.request.contextPath}/updateAccount",
							headers : {
								'Content-Type' : 'application/json'
							},
							data : JSON.stringify(bankAccount),
							success : function(resultant) {
								if (resultant.success == true) {
									Materialize.Toast.removeAll();
									Materialize.toast(
											'Account added successfully',
											'3000', 'toastAlign');

									setTimeout(
											function() {
												window.location.href = "${pageContext.request.contextPath}/banking"
											}, 500)

								} else if (resultant.success != true) {
									Materialize.Toast.removeAll();
									Materialize.toast(resultant.msg, '3000',
											'toastError');
								} else {
									Materialize.Toast.removeAll();
									Materialize.toast('Internal Server Error',
											'3000', 'toastError');
								}
							},
							error : function(err) {
								/*Show Dialog for Server Error*/
								Materialize.Toast.removeAll();
								Materialize.toast('Internal Server Error',
										'3000', 'toastError');
							}
						});
			}
		}
	/* 	$(window).on('load',
				function() {
					$("#accountType option[value=" + accountTypeSelected + "]")
							.attr("selected", "selected");

					var isCheckedValue = "${bankAccount.primary}";
					if (isCheckedValue == true) {
						$("#makeDefault").prop("checked", true);
					}

				}); */
	</script>
</body>

</html>