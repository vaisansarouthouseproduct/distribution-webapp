<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>
    	<script type="text/javascript">
	 $(document).ready(function() {
		//hide column depend on login
	/*     var table = $('#productcart').DataTable(); // note the capital D to get the API instance
	    var column = table.columns('.toggle');
	    console.log(column);
	    if(${orderDetails.orderStatus.status!='Booked'}){            
	        column.visible(true);
	    }else{
	    	 column.visible(false);
	    } */
		 $('.qty').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
	}); 
	</script>
     <script type="text/javascript">
     var myContextPath = "${pageContext.request.contextPath}";
     var orderId="${orderDetails.orderId}";     
     </script>
	<script type="text/javascript" src="resources/js/hash-map-collection.js"></script>
	<script type="text/javascript" src="resources/js/editOrderDetails.js"></script>
	

	
<style>
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40%;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
}
tfoot td{
	border:1px solid #9e9e9e;
	text-align:center !important;
}

.select2 {
	width: 100% !important;
	float:right;
	height:3rem !important;
	
}
</style>


</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
              
              <div class="col s12 l6 m6 left" style="padding-left:0">
      		  <div class="card horizontal">
      		  	<div class="card-image" style="width:25%">
       				  <h6 class="white-text center">Shop Name</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><span id="name"><c:out value="${orderDetails.businessName.shopName}"/></span></h6>
       			  </div>
        	    </div>
          	  </div>
           </div>  
           <div class="col s12 l3 m3 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Area</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><span id="shopname"><c:out value="${orderDetails.businessName.area.name}"/></span></h6>
       			  </div>
        	    </div>
          	  </div>
           </div>  
           <div class="col s12 l3 m3 left" style="padding-right:0">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Account No</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><span id="AccountNo"><c:out value="${orderDetails.businessName.businessNameId}"/></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
                    <div class="col s12 m12 l12 formBg"  style="display:${orderDetails.orderStatus.status=='Issued'?'none':'block'}">
                    	
                    
                    <div class="input-field col s12 m3 l3">
                         
                        
                        <select id="brandid" name="brandId">
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandList}">
									<c:forEach var="listValue" items="${brandList}">							
										<option value="<c:out value="${listValue.brandId}" />"><c:out value="${listValue.name}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <select id="categoryid" name="categoryId">
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categoryList}">
									<c:forEach var="listValue" items="${categoryList}">
										<option value="<c:out value="${listValue.categoryId}" />"  ><c:out value="${listValue.categoryName}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3">                                             
                         <select id="productid" class="select2" name="productId">
                                 <option value="0">Choose Product</option>
                                 <c:if test="${not empty productList}">
									<c:forEach var="listValue" items="${productList}">
										<option value="<c:out value="${listValue.productId}" />"  ><c:out value="${listValue.productName}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m1 l1" style="display:${orderDetails.orderStatus.status!='Packed'?'none':'block'}">                        
                        <input id="curr_qty" type="text" name="quantity" readonly>
                        <label for="curr_qty" class="active">Curr. Qty</label>
                    </div>
                    <div class="input-field col s12 m1 l1">                        
                        <input id="qty" type="text" name="quantity">
                        <label for="qty" class="active">Quantity</label>
                    </div>
                    
                   <div class="col s12 m4 l2" style="margin-top:2%;">
	              	  <input name="group1" type="radio" id="freeId" required/>
	     				 <label for="freeId" style="padding-left:25px;">Free</label>
	     				 <input name="group1" type="radio" id="nonFreeId" checked="checked"  required/>
	     				 <label for="nonFreeId" style="padding-left:25px;">Non-Free</label>
	     		  </div>
	     		  <div class="input-field col s12 m12 l12 center">	     				 
                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="orderProductAddId" ><i class="material-icons left">add</i>Add</button>
                    </div>
	                </div>
                    
                    <div class="col s12 m12 l12" style="padding:0">
                    <br>
                        <table class="centered tblborder" id="productcart">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    
                                    <c:if test="${orderDetails.orderStatus.status=='Packed'}"><th>Current Qty.</th></c:if>
                                    
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="producctListtb">
                            	<% int rowincrement=0; %>
                            	<c:if test="${not empty orderProductDetailsList}">
									<c:forEach var="listValue" items="${orderProductDetailsList}">
									<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
		                            <c:choose>
		                            	<c:when test="${orderDetails.orderStatus.status=='Booked'}">
		                            		<c:choose>
				                            	<c:when test="${listValue.type=='NonFree'}"> 
					                            	<tr>
					                            		<td><c:out value="${rowincrement}" /></td>
					                            		<td><c:out value="${listValue.product.product.productName}" /></td>
					                            		<td><input style="text-align:center;" class="qty" type="text" id="qtyNonFree_${listValue.product.product.productId}" value="<c:out value="${listValue.purchaseQuantity}" />"></td>
					                            		<td><input style="text-align:center;" class="qty" type="text" id="mrpNonFree_${listValue.product.product.productId}" value="<fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" />"></td>
					                            		<td><span id="purchaseAmtNonFree_${listValue.product.product.productId}"><c:out value="${listValue.purchaseAmount}" /></span></td>
					                            		<td><button id="delPrdNonFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
					                            	</tr>
				                            	</c:when>
				                            	<c:otherwise>
				                            		<tr>
					                            		<td><c:out value="${rowincrement}" /></td>
					                            		<td><c:out value="${listValue.product.product.productName}" /><font color="green">-(Free)</font></td>
					                            		<td><input style="text-align:center;" type="text" id="qtyFree_${listValue.product.product.productId}" value="<c:out value="${listValue.purchaseQuantity}" />"></td>
					                            		<td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" /></td>
					                            		<td><span id="purchaseAmtFree_${listValue.product.product.productId}"><c:out value="${listValue.purchaseAmount}" /></span></td>
					                            		<td><button id="delPrdFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
					                            	</tr>
				                            	</c:otherwise>
		                            		</c:choose>
		                            	</c:when>
		                            	<c:when test="${orderDetails.orderStatus.status=='Issued'}">
		                            	
		                            		<c:choose>
		                            		
		                            		
												
						                         	<c:when test="${listValue.type=='NonFree'}">
						                         	 				                            	
								               		<%--  Sachin  not adding the product in a row with Zero  issue Quantity  --%>
													<%-- <c:if test="${listValue.issuedQuantity}!=='0'">
													</c:if>  --%>
									                         <tr>
							                            		<td><c:out value="${rowincrement}" /></td>
							                            		<td><c:out value="${listValue.product.product.productName}" /></td>
							                            		<td><input style="text-align:center;" type="text" id="qtyNonFree_${listValue.product.product.productId}" value="<c:out value="${listValue.issuedQuantity}" />"></td>
							                            		<td><input style="text-align:center;" type="text" id="mrpNonFree_${listValue.product.product.productId}" value="<fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" />"></td>
							                            		<td><span id="issuedAmtNonFree_${listValue.product.product.productId}"><c:out value="${listValue.issueAmount}" /></span></td>
							                            		<td><button id="delPrdNonFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
							                            	</tr>
							                         
						                            	</c:when>
						                            	<c:otherwise>
						                            		<tr>
							                            		<td><c:out value="${rowincrement}" /></td>
							                            		<td><c:out value="${listValue.product.product.productName}" /><font color="green">-(Free)</font></td>
							                            		<td><input style="text-align:center;" type="text" id="qtyFree_${listValue.product.product.productId}" value="<c:out value="${listValue.issuedQuantity}" />"></td>
							                            		<td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" /></td>
							                            		<td><span id="issuedAmtFree_${listValue.product.product.productId}"><c:out value="${listValue.issueAmount}" /></span></td>
							                            		<td><button id="delPrdFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
							                            	</tr>
						                            	</c:otherwise>
				                            	
				                            	
		                            		</c:choose>
		                            	</c:when>
		                            	<c:otherwise>
		                            		<c:choose>
				                            	<c:when test="${listValue.type=='NonFree'}"> 
				                            	
					                            	<tr>
					                            		<td><c:out value="${rowincrement}" /></td>
					                            		<td><c:out value="${listValue.product.product.productName}" /></td>
					                            		<td><span class="current_qty_${listValue.product.product.productId}"><c:out value="${listValue.product.product.currentQuantity}" /></span></td>
					                            		<td><input style="text-align:center;" class="qty" type="text" id="qtyNonFree_${listValue.product.product.productId}" value="<c:out value="${listValue.issuedQuantity}" />"></td>
					                            		<td><input style="text-align:center;" class="qty" type="text" id="mrpNonFree_${listValue.product.product.productId}" value="<fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" />"></td>
					                            		<td><span id="issuedAmtNonFree_${listValue.product.product.productId}"><c:out value="${listValue.issueAmount}" /></span></td>
					                            		<td><button id="delPrdNonFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
					                            	</tr>
					                            	
				                            	</c:when>
				                            	<c:otherwise>
				                            	  
				                            		<tr>
					                            		<td><c:out value="${rowincrement}" /></td>
					                            		<td><c:out value="${listValue.product.product.productName}" /><font color="green">-(Free)</font></td>
					                            		<td><span class="current_qty_${listValue.product.product.productId}"><c:out value="${listValue.product.product.currentQuantity}" /></span></td >
					                            		<td><input style="text-align:center;" type="text" class="qty" id="qtyFree_${listValue.product.product.productId}" value="<c:out value="${listValue.issuedQuantity}" />"></td>
					                            		<td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${listValue.sellingRate}" /></td>
					                            		<td><span id="issuedAmtFree_${listValue.product.product.productId}"><c:out value="${listValue.issueAmount}" /></span></td>
					                            		<td><button id="delPrdFree_${listValue.product.product.productId}" class="btn-flat"><i class="material-icons">clear</i></button></td>
					                            	</tr>
					                            	
				                            	</c:otherwise>
		                            		</c:choose>
		                            	</c:otherwise>
		                            </c:choose>
		                            </c:forEach>
		                         </c:if>
                            </tbody>
                            <tfoot>
                            <c:choose>
		                      <c:when test="${orderDetails.orderStatus.status=='Booked'}">
                            	<tr>
                            		<td colspan="2">Total</td>
                            		<td><span id="totalQuantityId"><c:out value="${orderDetails.totalQuantity}" /></span></td>
                            		<td> </td>
                            		<td><span id="totalAmountId"><c:out value="${orderDetails.totalAmountWithTax}" /></span></td>
                            		<td></td>
                            	</tr>
                              </c:when>
                              <c:when test="${orderDetails.orderStatus.status=='Issued'}">
                              	<tr>
                            		<td colspan="2">Total</td>
                            		<td><span id="totalQuantityId"><c:out value="${orderDetails.issuedTotalQuantity}" /></span></td>
                            		<td> </td>
                            		<td><span id="totalAmountId"><c:out value="${orderDetails.issuedTotalAmountWithTax}" /></span></td>
                            		<td></td>
                            	</tr>
                              </c:when>
                              <c:otherwise>
                                 <tr>
                            		<td colspan="3">Total</td>
                            		<td><span id="totalQuantityId"><c:out value="${orderDetails.issuedTotalQuantity}" /></span></td>
                            		<td> </td>
                            		<td><span id="totalAmountId"><c:out value="${orderDetails.issuedTotalAmountWithTax}" /></span></td>
                            		<td></td>
                            	</tr>
                              </c:otherwise>
                            </c:choose>
                            </tfoot>
                        </table>
                        <br><br>
                    </div>
                     <div class="input-field col s12 m12 l12 center">
                    <form action="${pageContext.request.contextPath}/updateEditedOrder" method="post">
                      <input type='hidden' name='updateOrderProductListId' id='updateOrderProductListId'>
		              <input type='hidden' name='orderId' id='orderId' value='${orderDetails.orderId}'>
		              <input type='hidden' name='orderStatusId' id='orderStatusId' value='${orderDetails.orderStatus.status}'>
		              
		              <!-- its hidden button for generate dynamically events : don't remove it-->
		              <button type="button" class="btn waves-effect waves-light blue-gradient" id="forUpdateTableId" disabled="disabled" style="display:none;">ForUpdateTable</button>
		              
					  <button id="updateId" type="submit" class="btn waves-effect waves-light blue-gradient">Update</button>
					</form>
					</div>
                </div>
	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!-- <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
       
    </main>
    <!--content end-->
</body>

</html>