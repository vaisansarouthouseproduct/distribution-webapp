<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
<script type="text/javascript">
 $(document).ready(function() {
 	var table = $('#tblData').DataTable();
	 table.destroy();
	 $('#tblData').DataTable({
        "oLanguage": {
            "sLengthMenu": "Show _MENU_",
            "sSearch": "_INPUT_" //search
        },
     	autoWidth: false,
        columnDefs: [
                      { 'width': '1%', 'targets': 0 },
                      { 'width': '15%', 'targets': 1},
                      { 'width': '10%', 'targets': 2},
                      { 'width': '10%', 'targets': 3},
                      { 'width': '1%', 'targets': 4},
                 	  { 'width': '1%', 'targets': 5},
                 	  { 'width': '4%', 'targets': 6},
                 	  { 'width': '4%', 'targets': 7},
               	 	  { 'width': '3%', 'targets': 8},
           	     	  { 'width': '4%', 'targets': 9},
           		  	  { 'width': '1%', 'targets': 10} 
                    
                     ],
        lengthMenu: [
            [5,10, 25, 50, -1],
            ['5','10', '25 ', '50 ', 'All']
        ],
        
        
        //dom: 'lBfrtip',
        dom:'<lBfr<"scrollDivTable"t>ip>',
        buttons: {
            buttons: [
                //      {
                //      extend: 'pageLength',
                //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                //  }, 
                {
                    extend: 'pdf',
                    className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: ':visible.print-col'
                    },
                    customize: function(doc, config) {
                   	 doc.content.forEach(function(item) {
                   		  if (item.table) {
                   		  item.table.widths = [20,'*','*','*',40,40,40,50,30,50] 
                   		 } 
                   		    })
                         /* var tableNode;
                        for (i = 0; i < doc.content.length; ++i) {
                          if(doc.content[i].table !== undefined){
                            tableNode = doc.content[i];
                            break;
                          }
                        }
       
                        var rowIndex = 0;
                        var tableColumnCount = tableNode.table.body[rowIndex].length;
                         
                        if(tableColumnCount > 6){
                          doc.pageOrientation = 'landscape';
                        }  */
                        /*for customize the pdf content*/ 
                        doc.pageMargins = [5,20,10,5];   	                         
                        doc.defaultStyle.fontSize = 8	;
                        doc.styles.title.fontSize = 12;
                        doc.styles.tableHeader.fontSize = 11;
                        doc.styles.tableFooter.fontSize = 11;
                        doc.styles.tableHeader.alignment = 'center';
                        doc.styles.tableBodyEven.alignment = 'center';
                        doc.styles.tableBodyOdd.alignment = 'center';
                      },
                },
                {
                    extend: 'excel',
                    className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: ':visible.print-col'
                    },
                },
                {
                    extend: 'print',
                    className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                    //title of the page
                    title: function() {
                        var name = $(".heading").text();
                        return name
                    },
                    //file name 
                    filename: function() {
                        var d = new Date();
                        var date = d.getDate();
                        var month = d.getMonth();
                        var year = d.getFullYear();
                        var name = $(".heading").text();
                        return name + date + '-' + month + '-' + year;
                    },
                    //  exports only dataColumn
                    exportOptions: {
                        columns: ':visible.print-col'
                    },
                },
                {
                    extend: 'colvis',
                    className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                    text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                    collectionLayout: 'fixed two-column',
                    align: 'left'
                },
            ]
        }

    });
	 $("select")
 .change(function() {
     var t = this;
     var content = $(this).siblings('ul').detach();
     setTimeout(function() {
         $(t).parent().append(content);
         $("select").material_select();
     }, 200);
 });
$('select').material_select();
$('.dataTables_filter input').attr("placeholder", "Search");
if ($.data.isMobile) {
									var table = $('#tblData').DataTable(); // note the capital D to get the API instance
									var column = table.columns('.hideOnSmall');
									column.visible(false);
								}
}); 
</script>
<style>
	
	 .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
</style>
</head>

<body>
 <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
            <div class="col s12 m12 l12">
                    <div class="col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
      	
       			
                            <div class="col s12 l4 m6">
                            <p>
                             <span class="leftHeader">Order Id:</span>
                             <b><c:out value="${returnOrderReportModel.orderId}" /></b>
                                    <!--  <hr style="border:1px dashed teal;"> -->
                            </p>
                            </div>
                            <div class="col s12 l4 m6">
                               <p id="department"><span class="leftHeader">Shop Name: </span><b><c:out value="${returnOrderReportModel.shopName}" /></b></p>
                                     <!-- <hr style="border:1px dashed teal;"> -->
                                     
                            </div>
                             <div class="col s12 l4 m6">
                               <p><span class="leftHeader">Salesman: </span><b><c:out value="${returnOrderReportModel.employeeName}" /></b></p>
                                     <!-- <hr style="border:1px dashed teal;"> -->
                            </div>
                                 <div class="col s12 l4 m6">
                                 <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${returnOrderReportModel.orderDate}"  />
                                   <p id="Add"><span class="leftHeader">Date:</span> 
                                   <b>
                                   <c:out value="${date}" />
                                   </b>
                                   </p>
                                     <!-- <hr style="border:1px dashed teal;"> -->
                            </div>
                            
                            <div class="col s12 l4 m6">
                            <fmt:formatNumber var="amount" value="${returnOrderReportModel.returnTotalAmountWithTax}" maxFractionDigits="2" minFractionDigits="2" />
                                <p><span class="leftHeader">Total Amount: </span><b><c:out value="${amount}" /></b></p>
                                     <!-- <hr style="border:1px dashed teal;"> -->
                            </div>
                            
                            <div class="col s12 l4 m6 hide-on-small-only">
                            <div class="col s4 m4 l4 leftHeader" style="padding:0"><p id="area"><span>Area:</span></p></div>
                            <div class="col s8 m8 l8" style="word-wrap:break-word;padding:0"><p><b><c:out value="${returnOrderReportModel.areaName}" /></b></p></div>
                             
                            </div>
                            <div class="col s12 l12 m6 hide-on-small-only">
                               <div class="col s2 m2 l2" style="padding:0"><p><span>Address:</span></p></div>
                             <div class="col s10 m8 l8" style="word-wrap:break-word;padding-left:4.5%"><p><b><c:out value="${returnOrderReportModel.businessAddress}" /></b></p></div>
                                                      
                            </div>
                       </div> 
            </div>
        
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Product</th>
                            <th class="print-col hideOnSmall">Category</th>
                            <th class="print-col hideOnSmall">Brand</th>
                            <th class="print-col">Issued Quantity</th>
                            <th class="print-col">Return Quantity</th>
                            <th class="print-col">MRP</th>
                            <th class="print-col hideOnSmall">Taxable Amount</th>
                            <th class="print-col hideOnSmall">Tax</th>
                            <th class="print-col">Total Amount</th>
                            <th>Reason</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                        <% int rowincrement=0; %>
                        <c:if test="${not empty returnOrderProductDetailsList}">
						<c:forEach var="listValue" items="${returnOrderProductDetailsList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<!-- <script>alert("");</script> -->
						<tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.productModel.productName}" /><font color="green"><b>${listValue.type=='Free'?'-Free':''}</b></font></td>
                            <td><c:out value="${listValue.productModel.categories.categoryName}" /></td>
                            <td><c:out value="${listValue.productModel.brand.name}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            <td><c:out value="${listValue.returnQuantity}" /></td>
                            <td><c:out value="${listValue.sellingRate}" /></td>
                            <td><c:out value="${listValue.sellingRate*listValue.returnQuantity}" /></td>
                            <td><c:out value="${listValue.returnTotalAmountWithTax-(listValue.sellingRate*listValue.returnQuantity)}" /></td>
                            <td><c:out value="${listValue.returnTotalAmountWithTax}" /></td>
                            <td><a href="#reason_${listValue.returnId}" class="modal-trigger tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Reason">Reason</a></td>
                          
                        </tr>
	                       
	  
				<div id="reason_${listValue.returnId}" class="modal deleteModal row">
					<div class="modal-content">
						<h5 class="center-align" style="margin-bottom:30px"><u>Reason</u><i class="modal-close material-icons right">clear</i></h5>
						
						<h6 id="msg" class="center-align"><b>Reason: </b><c:out value="${listValue.reason}"/></h6>
						<br>
					</div>
					<div class="modal-footer">
                        <div class="col s12 m12 l12 center ">
                                <a href="#!"
                                class="modal-action modal-close waves-effect  btn">OK</a>
                        </div>
						
					</div>
				</div>
			
	                       	
								
								
						</c:forEach>
						</c:if>
                       
                    </tbody>
                </table>
            </div>
          </div>    
<!--  <a href="#addeditmsg" >Reason</a> -->
							<!-- <div class="row">
								<div class="col s8 m2 l2">
									<div id="demo" class="modal" style="width:40%;">
										<div class="modal-content">
											<h5 id="msgHead">Reason of Res123 is : </h5>
											<hr>
											<p id="msg">dfdff</p>
										</div>
										<div class="modal-footer">
											<a href="#!"
												class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
										</div>
									</div>
								</div>
							</div>  -->
    </main>
    <!--content end-->
</body>

</html>