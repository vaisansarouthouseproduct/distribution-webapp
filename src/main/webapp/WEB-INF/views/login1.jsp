<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <title>Blue Square</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="resources/img/bs_icon2-01.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Compiled and minified JavaScript -->
    <link rel="stylesheet"  href="resources/css/materialize.min.css">
    <link rel="stylesheet" href="resources/css/font-awesome.min.css">
   <script	src="resources/js/jquery.min.js"></script>
<script src="resources/js/materialize.min.js"></script>
    <link href="resources/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/script.js"></script>
    <link href="resources/css/materialicon.css"	rel="stylesheet">
    
    <!-- scripts -->
     <link href="resources/css/particle.css"	rel="stylesheet">



    <script>
        $(document).ready(function() {
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
           
            var msg="${validateMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
				 $('#loginError').html('<font color="red" size="4">'+msg+'</font>');
			 }
            
            $('#loginValidateButton').click(function(){
            	
            	var user=$('#userId').val().trim();
            	var pass=$('#password').val().trim();
            	
            	if(user==="" && pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name and Password</font>');
            		return false;
            	}
            	else if(user==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name</font>');
            		return false;
            	}
            	else if(pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill Password</font>');
            		return false;
            	}
            	$('#loginError').html('');
          	 	/*$('#loginError').html('<font color="red" size="4"><b>Wait..</b></font>');
        		$.ajax({
        			type:"GET",
					url : "${pageContext.request.contextPath}/loginAdmin?userId="+user+"&password="+pass,
					dataType : "json",
					success : function(data) {
						//alert(data.status);
						if(data.status==="Success")
						{
							$('#loginError').html('<font color="green" size="4"><b>Login SuccessFully</b></font>');
							window.location.href="${pageContext.request.contextPath}/";
							//location.reload();
						}
						else
						{
							$('#loginError').html('<font color="red" size="4">Login failed</font>');
						}
					}
				});	 */
            });
            
        });
    </script>
    <style>
    .container{
    background-color: white !important;
    opacity:0.4;
    }
      .btn{
      	background-color: transparent !important;
      	
      }
      input[type=text],
      input[type=password]{
      	/* border:1px solid #9e9e9e !important; */
      	/* border-radius:4px !important; */
      	height:2rem !important;
      	border-bottom:1px solid #263238 !important;
      }
      input[type=text]:focus,
      input[type=password]:focus{
      	border-bottom:1px solid #263238 !important;
      	box-shadow:none !important;
      }
      ::-webkit-input-placeholder { /* WebKit browsers */
    color:black;
     opacity: 1 !important;
}
	button.btn-floating {
    border: 1px black solid;
}
	
  .footer{
  	 position:relative; 
  	/*   float:center !important; */
  	 /* bottom:10%; */
  	 top:40%;
  	text-align:center;
  	  /* left:30vw;  */
   margin-left:3%; 
  }
 
    </style>

    <body>
    <!-- count particles -->
	<!-- <div class="count-particles">
	  <span class="js-count-particles">--</span>
	</div> -->
<!-- particles.js container -->

	<div id="particles-js"></div>
        <div class="container" style="transform: translate(20px, 140px);">
            <form action="${pageContext.request.contextPath}/loginEmployee" method="post">
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2  transparent">
                        <!-- <div class="card-content black-text"> -->
                            <!-- <div class="col s12 m12 l12">
                            <h2 class="card-title center teal-text" style="font-family: Serif;">Login</h2>
                        </div> -->
                           <!--  <div class="col l6 m6">
                                <div class="col s6 m7 l12">
                                    <img src="resources/img/bluelogo.jpg" class="responsive-img" style="margin-top:9%;border-right:2px solid teal;">
                                    
                                </div>
                            </div>  -->
                            
                            <div class="col l8 m8 offset-l2">
                                <br><br>
                                <h1 class="center"><i class="material-icons" style="font-size:40px;">account_circle</i></h1>
                                  <h1 class="center" style="font-size:20px;">Dashboard Login</h1>
                               <br><br>
                               
                                <div class="row" style="margin-bottom:4%">
                                    <div class="col s12 l8 m12 offset-l2">
                                        <!-- <label for="userId" class="black-text">User Name</label> -->
                                        <!-- <i class="material-icons prefix">account_circle</i> --> <input id="userId" name="userId" placeholder="User Name" type="text" required class="border" data-error="wrong" data-success="right">
                                        
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom:0">
                                    <div class="col s10 l8 m10 offset-l2">
                                    <!-- <label for="password" class="black-text">Password</label> -->
                                        <!-- <i class="material-icons prefix">lock</i> --> <input id="password" placeholder="Password" name="password" type="password" class="border" required>
                                        
                                    </div>
                                    <!-- <div class="col s2 m2 l1" style="transform: translate(-45px, 30px);">
                                        <i class="fa fa-eye fa-lg eye" aria-hidden="true"></i>
                                        <i class="fa fa-eye-slash fa-lg eye-slash" aria-hidden="true"></i>
                                    </div> -->
                                   	<div class="input-field col s10 l8 m8  offset-l2 center" style="margin-bottom:2%">                                   
                                   		<span id="loginError"></span>
                                   	</div>
                                   	
                                </div>
                                <div class="col s10 l8 m8  offset-l2 center">
                              <button class="btn-floating btn-medium waves-effect waves-light transparent black-text center" type="submit" id="loginValidateButton" style="margin-left:2%;">
							 <i class="material-icons small center black-text">arrow_forward</i>
							</button>
							 <br><br>
                                </div>
                               
                            </div>
                         
                        </div>
                    <!-- </div> -->
                </div>
            </form>
            </div>
        
        <div class="footer">
             <h2> � 2018 Designed & Developed By Vaisansar Technologies Pvt. Ltd </h2>            
        	</div>
 
      		
<script src="resources/js/particles.js"></script>
<script src="resources/js/app.js"></script>
		

    </body>
	
</html>