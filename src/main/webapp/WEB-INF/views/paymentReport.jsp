<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                   	  { 'width': '3%', 'targets': 1 },
	   	                	  { 'width': '5%', 'targets': 2 },
	   	             		  { 'width': '1%', 'targets': 3 },
	   	                      { 'width': '3%', 'targets': 4 },
	   	                   	  { 'width': '3%', 'targets': 5 },
	   	                	  { 'width': '1%', 'targets': 6 },
	   	                	  { 'width': '1%', 'targets': 7 },
	   	                	  { 'width': '1%', 'targets': 8 }
	   	                     ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,50,120,60,70,70,50,70] 
		                    		 } 
		                    		    })
	   	        /*                  var tableNode;
	   	                         for (i = 0; i < doc.content.length; ++i) {
	   	                           if(doc.content[i].table !== undefined){
	   	                             tableNode = doc.content[i];
	   	                             break;
	   	                           }
	   	                         }
	   	        
	   	                         var rowIndex = 0;
	   	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	   	                          
	   	                         if(tableColumnCount > 6){
	   	                           doc.pageOrientation = 'landscape';
	   	                         } */
	   	                         /*for customize the pdf content*/ 
	   	                         doc.pageMargins = [10,20,10,10];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                      	 doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select").change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
	         if ($.data.isMobile) {
					var table = $('#tblData').DataTable(); // note the capital D to get the API instance
					var column = table.columns('.hideOnSmall');
					column.visible(false);
				}
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            
            
          //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            console.log(column);
            if(${sessionScope.loginType=='CompanyAdmin'}){            
                column.visible(true);
            }else{
            	 column.visible(false);
            }
        });

    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:50% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
/* #oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
} */

@media only screen and (max-width: 600px) {
	
	#oneDateDiv {
		margin-top: 0.5rem;
		margin-left: 0;
		
	}
	.input-field {
    position: relative;
    margin-top: 0.6rem;
}
	
}
@media only screen and (min-width: 601px) and (max-width: 992px) {
	.input-field {
    position: relative;
    margin-top: 0.4rem;
}
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br>
        <div class="row">
                 
         <div class="col s12 l3 m3 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Amount</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId" class="totalAmountId"><c:out value="${totalAmountPaid}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 l3 m3 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">No. Of Paid Bills</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="" id="paidBills" class="paidBills"><c:out value="${noOfBills}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
		   </div>  
		   <div class="col s7 m4 hide-on-large-only">
				<h6 class=""><b>Total Amount : <span id="totalAmountId" class="blueColor-text">&#8377;<c:out value="${totalAmountPaid}" /></span></b></h6>
				<h6 class="" id="paidBills"><b>No. Of Paid Bills : <span class="blueColor-text"><c:out value="${noOfBills}" /></span></b></h6>
		   </div>
		   
           <div class="col s5 m3 l3 right right-align actionDiv actionBtn" style="">
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                     
                     <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s12 l3 m3  center actionDiv" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/paymentReport" method="post">
	                    <div class="input-field col s4 m7 l7 offset-s3 offset-m2">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s4 m3 l2">
	                     <button type="submit" class="btn">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l3 m5 right actionDiv rangeDiv">      
           		 <form action="${pageContext.servletContext.contextPath}/paymentReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s5 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s5 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s2 m2 l2">
                            <button type="submit" class="btn">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
          <%-- <div class="col s12 m5 l5 right">
       			 <h5 class="center red-text">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></h5>
		</div> --%>
<!-- </div>

        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Shop Name/Customer Name</th>                            
                            <th class="print-col">Amount</th>
                            <th class="print-col">Payment Mode</th>    
                            <th class="print-col">Collecting Person</th>    
                            <th class="print-col">Status</th>
                            <th class="print-col hideOnSmall">Date & Time</th>
                                                   
                            <th class="toggle hideOnSmall">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:if test="${not empty paymentReportModelList}">
						<c:forEach var="listValue" items="${paymentReportModelList}">
						
		                    <tr>
	                            <td><c:out value="${listValue.srno}" /></td>
	                            <td><c:out value="${listValue.orderId}" /></td>
	                            <td><c:out value="${listValue.customerShopName}" /></td>                            
	                            <td><c:out value="${listValue.amountPaid}" /></td>
	                            <td><c:out value="${listValue.paymentMode}" /></td>
	                            <td><c:out value="${listValue.employeeName}" /></td>
	                            <td>
	                             <c:choose>
	                             	<c:when test="${listValue.payType=='Refund'}">
	                             		<font color="green">
	                            			<b><c:out value="${listValue.payType}" /></b>
	                            		</font>
	                            	</c:when>
	                            	<c:otherwise>
	                            		<font color="blue">
	                            			<b><c:out value="${listValue.payType}" /></b>
	                            		</font>
	                            	</c:otherwise>
	                             </c:choose>
	                            </td>
	                            <td>
	                            	<fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.paidDate}"  />
	                            	<c:out value="${date}" /><br><fmt:formatDate pattern = "hh:MM:SS" var="time"   value = "${listValue.paidDate}"  /> <c:out value="${time}" />
	                            	
	                            	
								</td>
	                            <td>
	                            <c:choose>
	                             	<%-- <c:when  test="${listValue.findLast==true and listValue.payType=='Paid'}"> --%>
	                             	<c:when  test="${listValue.payType=='Paid'}">
	                             		<%-- <c:if test="${not fn:contains(listValue.orderId, 'CORD')}"> --%>
	                             			<c:if test="${listValue.orderDeleted==false}">
	                                       		<a href="${pageContext.request.contextPath}/editOrderDetailsPayment?paymentId=${listValue.paymentId}&orderId=${listValue.orderId}" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a>
		                             	<%-- 	</c:if> --%>
					    				        <a href="#delete${listValue.paymentId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a>
							       			</c:if>
						       		</c:when>
						       		<c:otherwise>
						       			NA
						       		</c:otherwise>
						       	</c:choose>
						       </td>
		                   </tr>
		                   
		                    <!-- Modal Structure for delete -->
		                    <div id="delete${listValue.paymentId}" class="modal deleteModal row">
		                        <div class="modal-content  col s12 m12 l12">
		                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
		                            <h5 class="center-align">Do You Want to Delete?</h5>
		                            <br/>
		                            </div>
		                        <div class="modal-footer">
		            
		         				   <div class="col s6 m6 l3 offset-l3">
		                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
		                			</div>
		            				<div class="col s6 m6 l3">
		             				   <a href="${pageContext.request.contextPath}/deleteOrderDetailsPayment?paymentId=${listValue.paymentId}&orderId=${listValue.orderId}" class="modal-action modal-close waves-effect  btn">Delete</a>
		               				 </div>
		                
		            			</div>
		                    </div>
		                   
	                   </c:forEach>
                   </c:if>
                  </tbody>
                </table>
            </div>
        </div>

 

    </main>
    <!--content end-->
</body>

</html>