<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>

		<head>
			<%@include file="components/header_imports.jsp" %>
				<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
				<script type="text/javascript" src="resources/js/additional-method.js"></script>
				<script>var myContextPath = "${pageContext.request.contextPath}"</script>
				<script type="text/javascript">
				var productArray =[];
				var priceBy;
					$(document).ready(function() {
						priceBy=$("#priceType").val();
						$("#priceType").change(function(){
							priceBy=$("#priceType").val();
							$('.inputVal').val(0);
								$('.inputVal').change();
								$("#applyAllInput").val(0);
						});
						$(".inputVal").change(function(){
							var id=parseFloat($(this).attr('data-id'));
							var commissionVal=parseFloat($(this).val());
							if(priceBy =="percentage"){
								var obj = productArray.find(function (obj) { return obj.productId === id; });
								var commission=(obj.unitPrice*commissionVal)/100;
									commission=parseFloat(commission.toFixedVSS(2));
									obj.commissionVal=commission;
								$('#commissionVal_'+id).text(commission);
							}
							else if(priceBy =="value"){
								var obj = productArray.find(function (obj) { return obj.productId === id; });
								obj.commissionVal=commissionVal;
								$('#commissionVal_'+id).text(commissionVal);
								
							}
							else{
								// do nothing
							}
							

													
						});
						$('#applyAll').click(function(){
							var commission=$("#applyAllInput").val();
							if(commission != ""){
								commission=parseFloat(commission);
								$('.inputVal').val(commission);
								$('.inputVal').change();
							}
							else{
								Materialize.Toast.removeAll();
			 					Materialize.toast('Please enter commission value to apply all product', '2000', 'toastError');
							}
						});
					});
					/* incomplete function we will work  after discussion  */
					function submitData(){
						var productSlabs =[];
						var commissionName =$('#commissionName').val();
						var includeTax ;
						if($("#includeTax").is(':checked')){
				
						}
						for(var i=0;i<productArray.length;i++){
							var commissionValue =productArray[i].commissionVal,
							var productId=productArray[i].productId;

							var data={
								commissionAssignProductSlabs={
									commissionValue:commissionValue,
									commissionType :priceBy,
									product :{
										productId :productId
									}
								}
							}
							productSlabs.push(data);
						}


						
					
						hitForSubmit(resultData);
						
					}
					function hitForSubmit(resultData){
						var resulData=JSON.stringify(resultData);
						$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/savePermanentOrder",
					dataType : "json",
					contentType: "application/json",
					data:resulData,
    				async:false,
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						if (data.status == 'Success') {
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
							Materialize.Toast.removeAll();
							Materialize.toast('Commission Assigned Successfully', 2000);
							setTimeout(function(){
								window.location.href="${pageContext.request.contextPath}/fetchReturnOrderReportForWeb"; 
								}, 3000);
							
						} else {
							 Materialize.Toast.removeAll();
								Materialize.toast('Something went wrong!', 2000, 'toastError');
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
						}

					},
					complete : function() {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						  Materialize.Toast.removeAll();
						Materialize.toast('Something went wrong!', '2000', 'toastError');
						}
				});
					}
				</script>
				<style>
					.tableDiv th,
					.tableDiv td {
						border: 1px solid #9e9e9e;
						padding: 2px;
						background-color: white;
					}

					.sticky {
						position: sticky;
						top: -1px;
					}

					.tableDiv .wrapper {
						height: 400px;
						overflow-y: auto;
					}

					.tableDiv .wrapper table {
						position: relative;
						width: 100%;
					}

					table select.browser-default {
						display: inline-block !important;
						width: 70% !important;
					}
					table input{
						width: 35% !important;
					}
					@media only screen and (min-width: 993px) {
						.container {
						width: 90%;
					}
					}
				</style>
		</head>

		<body>
			<!--navbar start-->
			<%@include file="components/navbar.jsp" %>
				<!--navbar end-->
				<!--content start-->
				<main class="paddingBody">
					<br><br>
					<div class="container">
						<div class="row  z-depth-3">
							<div class="row">
								<br>
								<div class="input-field col s12 m4 l3 push-l1 push-m1" id="commissionNameDiv">
									<i class="material-icons prefix">account_balance</i>
									<input id="commissionName" type="text" class="validate" name="companyName" required>
									<label for="commissionNameDiv" class="active">
										<span class="red-text">*</span>Commission Name</label>
								</div>
								<div class="col s12 m5 l5 push-l1 push-m1">
									<p>
										<input type="checkbox" id="includeTax" />
										<label for="includeTax">Include Tax</label>
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col s12 m10 l10 push-l1 push-m1 locationTable">
									<div class="wrapper">
										<table class="" width="100%">
											<thead>
												<tr>

													<th class="sticky" width="250px">
														<select id="commisionType" class="browser-default">
															<option value="" selected>Target Type</option>
															<option value="1">Option 1</option>
															<option value="2">Option 2</option>
															<option value="3">Option 3</option>
														</select>
													</th>
													<th class="sticky" width="200px">
														<span style="width:50%">price</span>
														<select id="priceType" class="browser-default" style="">
															<option value="percentage">By percentage</option>
															<option value="value">By value</option>
														</select>
													</th>
													<th class="sticky">
														<input type="number" id="applyAllInput">													
														<button type="button" class="btn-flat" id="applyAll"> <i class="material-icons left">money</i> Apply All</button>
													</th>
													<th class="sticky">commission</th>
												</tr>
											</thead>
											<tbody id="commissionTableData">
												<c:if test="${not empty productList}">
												<c:forEach var="listValue" items="${productList}">
												<script>
												var productId=parseFloat("${listValue.productId}"),
													unitPrice=parseFloat("${listValue.rate}"),
													commissionVal=0;													
												var data={
													productId:productId,
													commissionVal:commissionVal,
													unitPrice:unitPrice
												}
													productArray.push(data);
													console.log(productArray);	
												</script>
												<tr id="${listValue.productId}">
													<td><c:out value="${listValue.productName}"/></td>
													<td><c:out value="${listValue.rate}"/></td>
													<td>
														<input type="number" class="inputVal" data-id="${listValue.productId}">
													</td>
													<td><span class="commissionVal" id="commissionVal_${listValue.productId}"  data-id="${listValue.productId}">0</span></td>
												</tr>
												</c:forEach>
												</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12 m12 l12 center">
									<button type="button" class="btn">Save</button>
								</div>
							</div>
						</div>
					</div>
				</main>
				<!--content end-->
		</body>

		</html>