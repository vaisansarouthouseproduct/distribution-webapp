<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
 <script type="text/javascript" src="resources/js/hashtable.js"></script>
 
 		<!-- base url store in myContextPath variable -->
        <script>var myContextPath = "${pageContext.request.contextPath}"</script>
        
        <script type="text/javascript">
        var productList = [];  
        var count = 1;
        
        //find this page is open for edit or add
        var isEdit=${invtUrl=='editInventory'};
        
        //if this page is open for edit then pre added product set on productList[]
        <c:if test="${invtUrl=='editInventory'}">         
		var invtAddedDate="${invtAddedDate}";
		
		//[productId,quantity,unitPrice,mrp,taxableAmountBeforeDiscount,totalAmountBeforeDiscount,
		//discountAmount,discountPercentage,discountType,taxableAmount,totalAmount]
       
		/*
		private double totalAmountWithoutTax;
		private double totalAmountWithTax;
		private long productId;
		private long quantity;
	
		private double unitPrice;
		private double mrp;
		private double totalAmountBeforeDiscount;
		private double discountAmount;
		private double discountPercentage;
		private String discountType;
		*/
		
        <c:if test="${not empty inventoryDetailsList}">
		<c:forEach var="listValue" items="${inventoryDetailsList}">
				var prdData=[
				             "${listValue.orderUsedProduct.product.productId}",
				             "${listValue.quantity}",
				             "${listValue.unitPrice}",
				             "${listValue.mrp}",
				             "${listValue.totalAmountBeforeDiscount}",
				             "${listValue.discountAmount}",
				             "${listValue.discountPercentage}",
				             "${listValue.discountType}",
				             "${listValue.totalAmountWithoutTax}",
				             "${listValue.totalAmountWithTax}",
				             "${listValue.discountOnMRP}"
				             ];
          		productList.push(prdData);
          		count++;
	   </c:forEach>	   
	   </c:if>
	   </c:if>
	   
        </script>
	<script type="text/javascript" src="resources/js/addMultipleInventory.js"></script>

	
    
   <script>
	$(document).ready(function() {
		let discountGiven=${discountGiven};
		totalAmount=parseFloat(${inventory.totalAmount});
		totalAmountWithTax=parseFloat(${inventory.totalAmountTax});
		totalAmountBeforeDisc=parseFloat(${inventory.totalAmountBeforeDiscount});
		totalAmountWithTaxBeforeDisc=parseFloat(${inventory.totalAmountTaxBeforeDiscount});
		totalQuantity=parseFloat(${inventory.totalQuantity});
		discountAmountMain=parseFloat(${inventory.discountAmount});
		discountPercentageMain=parseFloat(${inventory.discountPercentage});
		discountTypeMain=("${inventory.discountType}");
		discountOnMRP=("${inventory.discountOnMRP}");

		if(discountGiven){
			$('#discountMainCheck').prop('checked', true);
			if(discountTypeMain=="PERCENTAGE"){
				$('#percentageMainCheck').prop('checked', true);				
				$('#discountMainId').val(discountPercentageMain);
			}else{
				$('#percentageMainCheck').prop('checked', false);
				$('#discountMainId').val(discountAmountMain);
			}
			if(discountOnMRP=="YES"){
				$('#discountOnMRPMainCheck').prop('checked', true);
			}else{
				$('#discountOnMRPMainCheck').prop('checked', false);
			}
			$('.discountMainClass').show();
			$('.editable').attr('disabled','disabled');
		}else{
			$('.discountMainClass').hide();
			$('.editable').removeAttr('disabled');	
		}
		
		findAmountAfterDisc();
		
		isDiscountGiven=discountGiven

		for(let i=1; i<=count; i++){
			/* set discount input events */
			discountChangeEvents(i);
		}
		
		var supplierId=$('#supplierId').val();
		
		if(supplierId != "0"){
			oldSupplierId=supplierId;
			
			$.ajax({
				url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
				dataType : "json",
				beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
				success : function(data) {
					$('#productId').empty();
					$("#productId").append('<option value="0">Choose Product</option>');
					for(var i=0; i<data.length; i++)
					{								
						//alert(data[i].productId +"-"+ data[i].productName);
						$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
					}	
					
					$("#productId").change();
					
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					  //alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
	       	     	$('#msgHead').text("Message : ");
	       	     	$('#msg').text("Product List Not Found"); 
	       	     		setTimeout(function() 
						  {
		     					$('#addeditmsg').modal('close');
						  }, 1000);
					}
			});
		}
		
	});

   /* $("#resetAddQuantitySubmit").click(function(){
	
		$("#productAddId").html('<i class="material-icons left">add</i> Add');

	}); */
   </script>
   <style>
   	tfoot td{
   		border:1px solid #9e9e9e;
   		text-align:center !important;
   		font-weight:bold;
   	}
	#chooseDate{
		width:40%;
	}
	#chooseDate .modal-footer{
		text-align: center !important;
	}
	@media only screen and (max-width:992px){
		.discountInp{
			margin-left:40px !important;
		}
		#chooseDate{
		width:60%;
		}
	}
	@media only screen and (max-width:600px){
		.discountInp{
			margin-left:40px !important;
		}
		#chooseDate{
		width:90%;
		}
	}
   </style>
</head>

		<body >
		   <!--navbar start-->
		   	<%@include file="components/navbar.jsp" %>
		    <!--navbar end-->
		    <!--content start-->
		    <main class="paddingBody">
            <br>
  
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/${invtUrl}" method="post" id="saveAddQuantityForm">
            <input type='hidden' id='currentUpdateProductId'>
            <input id="productlistinput" type="hidden" class="validate" name="productIdList">
                <div class="row  z-depth-3">
                     <div class="col l12 m12 s12">
                        <h4 class="center">Inventory Details </h4>                     
                    </div>
					<div class="row">
						
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">person</i>
						<span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Supplier</span>
                        <c:choose>
                        <c:when test="${invtUrl=='editInventory'}">
                        	<input id="inventoryId" type="hidden" class="validate" name="inventoryId" value="${inventory.inventoryTransactionId}">
							
	                        <select name="supplierId" class="select2" id="supplierId">
	                                 <option value="0" >Select Supplier</option>
	                                <c:if test="${not empty supplierList}">
										<c:forEach var="listValue" items="${supplierList}">
											<option value="<c:out value="${listValue.supplierId}" />" ${inventory.supplier.supplierId==listValue.supplierId ? 'selected' :'' }><c:out value="${listValue.name}" /></option>
										</c:forEach>
									</c:if>
	                        </select>
                        </c:when>
                        <c:otherwise>
                        	<select name="supplierId" class="select2" id="supplierId">
	                                 <option value="0" >Select Supplier</option>
	                                <c:if test="${not empty supplierList}">
										<c:forEach var="listValue" items="${supplierList}">
											<option value="<c:out value="${listValue.supplierId}" />"><c:out value="${listValue.name}" /></option>
										</c:forEach>
									</c:if>
	                        </select>
                        </c:otherwise>
                        </c:choose>
                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">star</i>
						<span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Brand</span>
                        <select name="brandId" class="select2" id="brandId">
                                 <option value="0" selected>Brand Name</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
				</div>
				<div class="row">
				
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">filter_list</i>
						<span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Category</span>
                        <select name="categoryId" class="select2" id="categoryId">
                                 <option value="0" selected>Category</option>
                                <c:if test="${not empty categorieslist}">
									<c:forEach var="listValue" items="${categorieslist}">
										<option value="<c:out value="${listValue.categoryId}" />"><c:out
												value="${listValue.categoryName}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">shopping_cart<span class="red-text">*&nbsp;</span></i>
                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Product</span>
                        <select name="productId" id="productId">
                        	<option value="0" selected>Choose Product</option>
                        	<c:if test="${not empty productList}">
								<c:forEach var="listValue" items="${productList}">
									<option value="<c:out value="${listValue.productId}" />"><c:out value="${listValue.productName}" /></option>
								</c:forEach>
							</c:if>
                        </select>
                    </div>
				</div>
				
				<div class="row">
				
                    <div class="input-field col s12 m3 l2 push-l1 push-m1">
                        <i class="material-icons prefix">playlist_add</i>
                        <input id="quantity" type="text" name="">
                        <label for="quantity" class="active"><span class="red-text">*&nbsp;</span>Quantity</label>
                    </div>
					<div class="input-field col l2 m2 s6 offset-l1 discountClass discountInp">
						<input id="discountId" type="text" class="num"
							title="only number allowed" required> <label
							for="discountId">Discount</label><br>
					</div>
					
					<div class="col s12 m4 l2 discountClass" style="margin-top: 3%;">
							<input id="discountOnMRPCheck" type="checkbox" checked /> <label
							for="discountOnMRPCheck" style="padding-left: 20px;">Discount On MRP</label>
					</div>
					
                    <div class="col s4 m4 l2 discountClass" style="margin-top: 3%;">
						<input id="percentageCheck" type="checkbox" checked /> <label
							for="percentageCheck" style="padding-left: 25px;">Percentage</label>
					</div>
					
				
					
					
                   	<div class="input-field col s12 center">
                        <button type="button" class="btn waves-effect waves-light blue-gradient editable" type="button" id="productAddId"><span id="productAddButtonId">Add</span><i class="material-icons right">add</i></button>
                   
	                    <button class="btn waves-effect waves-light blue-gradient" type="button" id="resetAddQuantitySubmit">Reset<i class="material-icons right">refresh</i></button>
	                </div>
                   <!--      <i class="material-icons prefix">playlist_add</i>
                        <input id="quantity" type="text" name="">
                        <label for="quantity" class="active"><span class="red-text">*&nbsp;</span>Quantity</label>
                    </div> -->
                    </div>
                    
				</div>
                    <div class="row  z-depth-3">
                    
                   
                    <div class="row">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Other Details </h4>                     
                    </div>
                    
                    
                    
                    <div class="input-field col s6 m3 l3 offset-l1 offset-m1">
                      <i class="material-icons prefix">rate_review</i>
                        <input id="hsncode" type="text" name="" readonly>
                        <label for="hsncode" class="active">HSN Code</label>
                    </div>
                    <div class="input-field col s6 m3 l2">
                            <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="AmtPerUnit" type="text" name="" readonly>
                        <label for="AmtPerUnit" class="active">Unit Price</label>
                    </div>
                    <div class="input-field col s6 m4 l2">
                            <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="AmtMRP" type="text" name="" readonly>
                        <label for="AmtMRP" class="active">MRP</label>
                    </div>
                    <div class="input-field col s6 m3 l3 offset-m1">
                        <i class="material-icons prefix">playlist_add_check</i>
                        <input id="oldquantity" type="text" name="" readonly>
                        <label for="oldquantity" class="active">Old Quantity</label>
                    </div>
                     <div class="input-field col s6 m3 l3  offset-l1 ">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="totalAmt" type="text" name="totalAmount" readonly>
                        <label for="totalAmt" class="active">Amount</label>
                    </div>
                     <div class="input-field col s6 m4 l4 ">
                        <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="igst" type="text" name="" readonly>
                        <label for="igst" class="active">Total Tax(<font color='blue'><span id="igstPer"></span>%</font>)</label>
                    </div>  
                    <div class="input-field col s7 m4 l3 offset-m1">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="amountWithTax" type="text" name="totalAmountWithTax" readonly>
                        <label for="amountWithTax" class="active">Amount With Tax</label>
                    </div>
                  <!--   <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                      <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="cgst" type="text" name="" readonly>
                        <label for="cgst" class="active">CGST(<font color='blue'><span id="cgstPer"></span>%</font>)</label>
                    </div>
                      <div class="input-field col s12 m4 l3">
                        <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="sgst" type="text" name="" readonly>
                        <label for="sgst" class="active">SGST(<font color='blue'><span id="sgstPer"></span>%</font>)</label>
                    </div>  -->
				</div>              
                    
                    <div class="row">
                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
						<div class="scrollDivTable">
                        <table class="centered tblborder">
                            <thead>
                                <!-- <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>MRP</th>
                                    <th>Unit Price</th>
                                    <th>Quantity</th>
                                    <th>Edit</th>
                                    <th>Cancel</th>
                                </tr> -->
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                     <th>Unit Price</th>
                                    <th>MRP</th>
                                    <!-- <th>Unit Price</th> -->
                                    <th>Quantity</th>
                                    <th>Taxable Amt</th>
                                    <th>Amount</th>
                                    <th>Disc.(%)</th>
                                    <th>Disc.Amt.</th>
                                    <th>Total(&#8377;)</th>
                                    <!-- <th>Edit</th> -->
                                    <th><i class="material-icons">delete</i></th>
                                </tr>
                                
                            </thead>
                            
                            
                            <tbody id="t1">
                            	<% int rowincrement=0; %>
                            	<c:if test="${not empty inventoryDetailsList}">
								<c:forEach var="listValue" items="${inventoryDetailsList}">
								<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
	                                <tr id='rowdel_${rowincrement}' >
			           				<td id='rowcount_${rowincrement}'>${rowincrement}</td>
			           				
			           				<td id='rowproductname_${rowincrement}'>				               				
			           				<input type='hidden' id='rowproduct_total_amt_${rowincrement}' value='${listValue.totalAmountWithoutTax}'>
			           				<input type='hidden' id='rowproduct_total_amt_with_tax${rowincrement}' value='${listValue.totalAmountWithTax}'>
			           				<input type='hidden' id='rowproductkey_${rowincrement}' value='${listValue.orderUsedProduct.product.productId}'>
			           				<center><span id='tbproductname_${rowincrement}'><c:out value="${listValue.orderUsedProduct.product.productName}"/></span></center>
			           				</td>
			           				
			           				<c:set var="reqVal" value="${listValue.unitPrice}" scope="request"/>
									<c:set var="reqIgst" value="${listValue.orderUsedProduct.categories.igst}" scope="request"/>
									<c:set var="reqQty" value="${listValue.quantity}" scope="request"/>
									<%
										double rate=(double)(request.getAttribute("reqVal"));
										float igst=(float)(request.getAttribute("reqIgst"));
									
										double mrp=rate+((rate*igst)/100);
										mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
										int mrp_int=(int)mrp;
										
										//new code for calculate the taxable amt 09-10-2019
										long quantity=(long)(request.getAttribute("reqQty"));
										double taxableAmount=rate*quantity;
										taxableAmount=Double.parseDouble(new DecimalFormat("###.##").format(taxableAmount));
										
										pageContext.setAttribute("mrp", mrp_int);
										pageContext.setAttribute("rate", rate);
										pageContext.setAttribute("taxableAmount", taxableAmount);
									%>
			           				
			           				<c:choose>
			           					<c:when test="${listValue.discountOnMRP == 'YES'}">
				           					<td id='rowproductunitprice_${rowincrement}'><c:out value="${rate}"/></td>
			           						<td id='rowproductmrp_${rowincrement}'><b><c:out value="${mrp}"/></b></td> 
			           					</c:when>
			           					<c:otherwise>
				           					<td id='rowproductunitprice_${rowincrement}'><b><c:out value="${rate}"/></b></td>
				           					<td id='rowproductmrp_${rowincrement}'><c:out value="${mrp}"/></td> 
			           					</c:otherwise>
			           				</c:choose>
			           				<%-- <td id='rowproductunitprice_${rowincrement}'><c:out value="${rate}"/></td>
			           				<td id='rowproductmrp_${rowincrement}'><c:out value="${mrp}"/></td>  --%>
			           				
			           				<%-- <td id='rowproductmrp_${rowincrement}'><c:out value="${mrp}"/></td> --%> 			           				
			           				<td id='rowproductqty_${rowincrement}'><c:out value="${listValue.quantity}"/></td>
			           				<td id='rowproducttaxableamt_${rowincrement}'><c:out value="${taxableAmount}"/></td>
									<td id='rowproductamt_${rowincrement}'><c:out value="${listValue.totalAmountBeforeDiscount}"/></td> 
			           				<c:choose>
			           					<c:when test="${listValue.discountType == 'PERCENTAGE'}">
				           					<td><input type="text" id="rowproductdiscper_${rowincrement}" value=${listValue.discountPercentage} class="editable center qty"  style="width: 4em;" /></td>
										  	<td><span id="rowproductdiscamt_${rowincrement}">${listValue.discountAmount}</span></td>
			           					</c:when>
			           					<c:otherwise>
			           						<td><span id="rowproductdiscper_${rowincrement}">${listValue.discountPercentage}</span></td>
									  		<td><input type="text" id="rowproductdiscamt_${rowincrement}" value=${listValue.discountAmount} class="editable center qty"  style="width: 4em;" /></td>
			           					</c:otherwise>
			           				</c:choose>
			           				
									<td id='rowproducttotalamt_${rowincrement}'><c:out value="${listValue.totalAmountWithTax}"/></td>   
									<td id='rowdelbutton_${rowincrement}'><button class='editable btn-flat' type='button' onclick='deleterow(${rowincrement})'><i class='material-icons '>delete</i></button></td>
			           				</tr>   
		           				</c:forEach>
								</c:if> 
							</tbody>
							<tfoot>
								<tr id="finalProductTotalsId">
                                    <td></td>
                                    <td>Total </td>
                                    <td><!-- Unit price --></td>
                                    <td><!-- Mrp --></td>
                                    <td><!-- Quantity --><span id="totalQuantityId"><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${inventory.totalQuantity}" /></span></td>
                                                                        <td><span id="totalTaxableAmtBeforeAllDiscountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTaxableAmtBeforeAllDiscount}" /></span></td>
                                    <td><!-- Amount --><span id="totalBeforeAllDiscountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalBeforeAllDiscount}" /></span></td>
                                    <td></td>
                                    <td><!-- Disc.Amt. --><span id="totalProductsDiscountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProductsDiscount}" /></span></td>
                                    <td><!-- Total --><span id="totalAfterDiscountAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAfterDiscountAmount}" /></span></td>
                                    <td></td>
                                </tr>                           
                            </tfoot>    
                        </table>
						</div>
                    </div>
					</div>

					<div class="row">
						<!-- overall discount fields -->
                   <div class="col s6 m3 l3 push-l1 push-m1" style="margin-top: 3%;">
						<input id="discountMainCheck" type="checkbox"  /> <label
							for="discountMainCheck">Discount</label>
					</div>
					
					<div class="col s12 m4 l2 discountMainClass" style="margin-top: 3%;">
							<input id="discountOnMRPMainCheck" type="checkbox" checked /> <label
							for="discountOnMRPMainCheck" style="padding-left: 25px;">Discount On MRP</label>
					</div>
						<div class="col s6 m2 l3  discountMainClass" style="margin-top: 3%;">
						<input id="percentageMainCheck" type="checkbox"  /> <label
							for="percentageMainCheck">Percentage</label>
					</div>
					<div class="input-field col l2 m3 s6 discountMainClass">
						<input id="discountMainId" type="text" class="num" value=""
							title="only number allowed" required>
							 <label for="discountMainId">Discount Percent</label><br>
					</div>
					
				
					<div class="input-field col l2 m3 s6 discountMainClass">
						<input id="discountCalculated" type="text" class="num" value=""
							title="only number allowed" readonly>
						 <label for="discountCalculated">Discount Amount</label><br>
					</div>
					</div>
				
					<!-- 
                    <div class=" col s12 m10 l5 push-l1 push-m1 ">

                       <h6 class="red-text"><b>Total Amount(Without Tax)</b> : <span id="finalTotalAmount"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${inventory.totalAmountBeforeDiscount}" /></span></h5>
                           <br>

                           <br>
                   </div>
                   <div class=" col s12 m10 l5 push-l2 push-m1 ">
                       <h6 class="red-text"><b>Total Amount(With Tax)</b> : <span id="finalTotalAmountWithTax"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${inventory.totalAmountTaxBeforeDiscount}" /></span></h6>
                       <br>

                       <br>
                   </div>
					-->
                 
                   
                   <div class=" col s12 m10 l5 push-l1 push-m1 ">

                       <h6 class="red-text"><b>Net Total Amount(Without Tax)</b> : <span id="netfinalTotalAmount"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${inventory.totalAmount}" /></span></h5>
                           <br>

                           <br>
                   </div>
                   <div class=" col s12 m10 l5 push-l2 push-m1 ">
                       <h6 class="red-text"><b>Net Total Amount(With Tax)</b> : <span id="netfinalTotalAmountWithTax"><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${inventory.totalAmountTax}" /></span></h6>
                       <br>

                       <br>
                   </div>
                    
                </div>
			<div class="row">
				<div class="col s8 m2 l2">
					<div id="chooseDate" class="modal">
						<div class="modal-content">
							 <i class="material-icons modal-close right">clear</i>
							  <br>
							  <c:choose>
									<c:when test="${invtUrl=='editInventory'}">
										<div class="input-field col s12 m5 l6">
			                   				  <i class="material-icons prefix">event</i>
			                       		      <input id="paymentDate" type="text" value="${paymentDate}" name="paymentDate" title="Enter Payment Date" class="datepicker inventoryDate">
			                           		  <label for="paymentDate" class="active black-text">Payment Date</label>
			                           		  <span id="errorPaymentDate" class="red-text center-align"></span>
			                    		 </div>
		                    		 </c:when>
									<c:otherwise>
										<div class="input-field col s12 m5 l6">
			                   				  <i class="material-icons prefix">event</i>
			                       		      <input id="paymentDate" type="text" name="paymentDate" title="Enter Payment Date" class="datepicker disableDate">
			                           		  <label for="paymentDate" class="active black-text">Payment Date</label>
			                           		  <span id="errorPaymentDate" class="red-text center-align"></span>
			                    		 </div>
									</c:otherwise>
								</c:choose>
                    		  <c:choose>
									<c:when test="${invtUrl=='editInventory'}">
										<div class="input-field col s12 m5 l6">
											<i class="material-icons prefix">event</i>
											<input id="selectedBillDate" type="text" name="billDate" value="${billDate}" title="Enter Select Date" class="datepicker previousDate inventoryDate"> 
											<label for="selectedBillDate" class="active black-text">Select Bill Date</label>
											<span id="errorBillDate" class="red-text center-align"></span>
										</div>
									</c:when>
									<c:otherwise>
										<div class="input-field col s12 m5 l6">
											<i class="material-icons prefix">event</i> <input id="selectedBillDate"
											type="text" name="billDate" title="Select Bill Date"
											class="datepicker previousDate"> <label for="selectedBillDate"
											class="active black-text">Select Bill Date</label>
											<span id="errorBillDate" class="red-text center-align"></span>
										</div>
									</c:otherwise>
								</c:choose>
			                    
			                    <c:choose>
			                        <c:when test="${invtUrl=='editInventory'}">
										<div class="input-field col s12 m12 l12">
											<i class="material-icons prefix">playlist_add</i> 
											<input id="bill_number" type="text" name="billNumber" value="${inventory.billNumber}"> <label for="bill_number"
											class="active black-text">Bill Number</label>
											<span id="errorBillNumber" class="red-text center-align"></span>
										</div>
									</c:when>
			                        <c:otherwise>
			                         	<div class="input-field col s12 m12 l12">
											<i class="material-icons prefix">playlist_add</i>
											<input id="bill_number" type="text" name="billNumber"> 
											<label for="bill_number" class="active black-text">Bill Number</label>
											<span id="errorBillNumber" class="red-text center-align"></span>
										</div>
			                        </c:otherwise>
			                    </c:choose>
						</div>
						<div class="col s12 m2 l2 offset-l5 modal-footer center-align">
							<button  type="button" id="submitModal" class="modal-action waves-effect btn">OK</button>
						</div>		
					</div>
				</div>
		  </div>
								

                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                	 <c:choose>
                        <c:when test="${invtUrl=='editInventory'}">
                        	<button class="btn waves-effect waves-light blue-gradient" type="button" id="saveAddQuantitySubmit">Update Inventory<i class="material-icons right">send</i> </button>
                        	<a href="${pageContext.servletContext.contextPath}/deleteInventory?inventoryId=${inventory.inventoryTransactionId}" class="modal-action modal-close waves-effect btn teal">Delete</a>
                        </c:when>
                        <c:otherwise>
                        	<button class="btn waves-effect waves-light blue-gradient" type="button" id="saveAddQuantitySubmit">Add Inventory<i class="material-icons right">send</i> </button>
                        </c:otherwise>
                     </c:choose>                    
                </div>
              
                <br>
            </form>

        </div>

       <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>


    </main>
    <!--content end-->
</body>

</html>