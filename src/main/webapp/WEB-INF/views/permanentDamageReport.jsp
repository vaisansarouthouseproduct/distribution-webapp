<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>
    
     <script type="text/javascript">
     	var myContextPath = "${pageContext.request.contextPath}";
     </script>
	 <script type="text/javascript" src="resources/js/moment.js"></script>
	<script type="text/javascript" src="resources/js/damageRecovery.js"></script>
	<script>
		$(document).ready(function(){
			var d=new Date();
		   	var currentYear=d.getFullYear();
		   	$(".yearSelect").val(currentYear);	 
		   	$(".yearSelect").change();
			var table = $('#tblData').DataTable();
			 table.destroy();
			 $('#tblData').DataTable({
		         "oLanguage": {
		             "sLengthMenu": "Show _MENU_",
		             "sSearch": "_INPUT_" //search
		         },
		  
		      	autoWidth: false,
		         columnDefs: [
		                      { 'width': '1%', 'targets': 0},
		                      { 'width': '8%', 'targets': 1},
		                      { 'width': '2%', 'targets': 2},
		                  	  { 'width': '2%', 'targets': 3}		                	  
		                     ],
		         lengthMenu: [
		             [10, 25., 50, -1],
		             ['10 ', '25 ', '50 ', 'All']
		         ],
		         
		        
		        // dom: 'lBfrtip',
		        dom:'<lBfr<"scrollDivTable"t>ip>',
		         buttons: {
		             buttons: [
		                 //      {
		                 //      extend: 'pageLength',
		                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
		                 //  }, 
		                 {
		                     extend: 'pdf',
		                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                     customize: function(doc, config) {
		                    	 doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [40,90,100,40,90,80,80] 
		                    		 } 
		                    		    })
		                         /*for customize the pdf content*/ 
		                         doc.pageMargins = [5,20,10,5];   	                         
		                         doc.defaultStyle.fontSize = 8	;
		                         doc.styles.title.fontSize = 12;
		                         doc.styles.tableHeader.fontSize = 11;
		                         doc.styles.tableFooter.fontSize = 11;
		                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
		                       },
		                 },
		                 {
		                     extend: 'excel',
		                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'print',
		                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'colvis',
		                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
		                     collectionLayout: 'fixed two-column',
		                     align: 'left'
		                 },
		             ]
		         }

		     });
			 $("select").change(function() {
	           var t = this;
	           var content = $(this).siblings('ul').detach();
	           setTimeout(function() {
	               $(t).parent().append(content);
	               $("select").material_select();
	           }, 200);
	       });
	   $('select').material_select();
	   $('.dataTables_filter input').attr("placeholder", "Search");
	   if ($.data.isMobile) {
	     		var table = $('#tblData').DataTable(); // note the capital D to get the API instance
	     		var column = table.columns('.hideOnSmall');
	     		column.visible(false);
	     	}
	   //$(".showQuantity").hide();
       $(".showDates").hide();
       $("#oneDateDiv").hide();
      
       $(".rangeSelect").click(function() {
      
           $(".showDates").show();
         //  $(".showQuantity").hide();
           $("#oneDateDiv").hide();
       });
       $(".pickdate").click(function(){
       	//$(".showQuantity").hide();
	   		 $(".showDates").hide();
	   		$("#oneDateDiv").show();
	   	}); 	
	});
		
		 // permanent damage details list by permanentDamageMonthWiseId
	    function fetchPermanentDamageDetails(permanentDamageMonthWiseId){
			$.ajax({
				type:'GET',
				url: "${pageContext.servletContext.contextPath}/permanentDamageDetailsList?permanentDamageMonthWiseId="+permanentDamageMonthWiseId,
				async: false,
				success : function(data)
				{
					$('#permanentDamageDetails').empty();
					
					if(data.status=="Success"){
						var permanentDamageDetails=data.permanentDamageDetailsList;
						for(var i=0; i<permanentDamageDetails.length; i++)	
						{
							permanentDamage=permanentDamageDetails[i];
							
							//long to dd/MM/yyyy format of date
							var date=moment(permanentDamage.damageDate).format("Do MMM") //parse integer
							// var dateFrom=new Date(permanentDamage.damageDate);
							// month = dateFrom.getMonth() + 1;
							// day = dateFrom.getDate();
							// year = dateFrom.getFullYear();
							// var dateFromString = day + "-" + month + "-" + year;
							var dateFromString = date;
							$('#permanentDamageDetails').append('<tr>'+
															'<td class="hideColumnOnSmall">'+permanentDamage.srno+'</td>'+
															'<td>'+permanentDamage.damageQuantity+'</td>'+
															'<td>'+permanentDamage.damageFrom+'</td>'+
															'<td>'+dateFromString+'</td>'+
															'<td>'+permanentDamage.reason+'</td></tr>');
							
						}
						
						$('#viewPermanentDamageDetail').modal('open');
					}else{
						Materialize.Toast.removeAll();
						Materialize.toast(data.errorMsg, '2000', 'teal lighten-2');
					}
					
					
				},
				error: function(xhr, status, error) 
				{
					Materialize.Toast.removeAll();
					Materialize.toast('Something went wrong', '2000', 'teal lighten-2');
				}
				
			});
		} 
		
	</script>
<style>
   
   .input-field {
    position: relative;
    margin-top: 0.5rem;
}

tfoot td{
	border:1px solid #9e9e9e;
	text-align:center !important;
}
.commentSection{
	height:200px;
	overflow-y:auto;
	border:1px solid #e0e0e0;
}
.commentSection p{
	padding:10px;
}
.btnmargin{
	margin-top: 3%;
}
@media only screen and (min-width: 601px) and (max-width: 992px) {
	.input-field {
    position: relative;
    margin-top: 0.3rem;
}
.btnmargin{
	margin-top: 5%;
}
}
@media only screen and (max-width: 600px) {
	.input-field {
    position: relative;
    margin-top: 0.6rem;
} 
.btnmargin{
	margin-top: 10%;
}
#viewPermanentDamageDetail{
	width:90% !important;
}
.modalHeading{
font-size: 1.3rem !important;
}
}
/* #startYear_table,
#endYear_table{
	display:none !important;
}
 */
</style>


</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
       		   <div class="col s12 m12 l12" style="padding-bottom:15px;">
       		   <form method="post" action="${pageContext.request.contextPath}/permanentDamageReport">
       		   
       		   <div class="col s9 m4 l4 offset-l4 offset-m4">	
       		   
            		<fieldset>
 					 <legend>Filter</legend>
                      <div class="input-field col s6 m6 l6 right-align">
                      	<!-- <input type="text" id="startYear" class="datepicker"> -->
                        <select id="startMonthId" name="monthNumber" required>
                                 <option value="">Month</option>
                                 <option value="1">Jan</option>
                                 <option value="2">Feb</option>
                                 <option value="3">Mar</option>
                                 <option value="4">Apr</option>
                                 <option value="5">May</option>
                                 <option value="6">June</option>
                                 <option value="7">July</option>
                                 <option value="8">Aug</option>
                                 <option value="9">Sept</option>
                                 <option value="10">Oct</option>
                                 <option value="11">Nov</option>
                                 <option value="12">Dec</option>                                 
                        </select>
                      </div>
                      <div class="input-field col s6 m6 l6 right-align">
                      		<!-- <input type="text" id="endYear" class="datepicker"> -->
                            <select id="startYearId" class="yearSelect" name="year" required>
                                 <option value="">Start Year</option>
                                 <%
                                 for(int year=2018; year<2051; year++){%>
                                 	<option value="<%=year%>"><%=year%></option>
                                 <%} %>
                            </select>   
                       </div>
                       </fieldset>
                       </div>
                 		
					   <div class="input-field col s3 m2 l1 btnmargin">
						<button type="submit" class="btn">View</button>
				  </div>
                  </form>
                
                </div>
                
                
                    <div class="col s12 m12 l12">
                    
                        <table class="striped highlight centered" id="tblData">
                            <thead>
                                <tr>
                                    <th class="print-col hideOnSmall">Sr.No</th>
                                    <th class="print-col">Product Name</th>
                                    <th class="print-col">Qty</th>                                   
                                    <th class="print-col">Date & Time</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
	                            <c:if test="${not empty permanentDamageReport}">
										<c:forEach var="listValue" items="${permanentDamageReport}">
			                            	<tr>
			                            		<td><c:out value="${listValue.srno}" /></td>
			                            		<td><a class="modal-trigger cursorPointer" onclick="fetchPermanentDamageDetails(${listValue.permanentDamageMonthWiseId})"><c:out value="${listValue.productName}" /></a></td>		                            		
			                            		<td><c:out value="${listValue.totalQuantityDamage}" /></td>
			                            		<td>
			                            		   <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.damageDate}"  /><c:out value="${date}" />
			                            		   <br/><fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.damageDate}" /><c:out value="${time}" />
			                            		</td>
			                            	</tr>
	                            		</c:forEach>
	                            </c:if>
                            </tbody>
                        </table>
                        <br><br>
                        <!-- permanent damage list view model -->
							<div id="viewPermanentDamageDetail" class="modal">
					            <div class="modal-content">
					                <h5 class="center modalHeading"><u>Permanent Damage Details</u> <i class="modal-close material-icons right">clear</i></h5>
					           
					                <!-- <div class="row">
					                
					                    <div class="col s12 l6 m6">
					                        <h6 id="boothNo" class="black-text">Booth No:</h6>
					                    </div>
					                    
					                    <div class="col s12 l6 m6">
					                        <h6 id="orderId" class="black-text">Order Id:</h6>
					                    </div>
					                    
					                    <div class="col s12 l6 m6">
					                        <h6 id="totalAmt" class="black-text">Total Amount:</h6>
					                    </div>
					                    
					                    <div class="col s12 l6 m6">
					                        <h6 id="balanceAmt" class="black-text">Balance Amount : </h6>
					                    </div> 
					                    
					                </div> -->
					               
					                <table border="2" class="centered tblborder">
					                    <thead>
					                        <tr>
					                            <th class="hideColumnOnSmall">Sr.No</th>
					                            <th>Qty</th>
					                            <th>From</th>
					                            <th class="">Date</th>
					                            <th>Reason</th>
					                        </tr>
					                    </thead>
					                    <tbody id="permanentDamageDetails">
					                       
					                    </tbody>
									</table>
								
					            </div>
					            <div class="modal-footer">
					             <div class="col s12 m12 l12 center">
					                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
					                 </div>	
					            </div>
					        </div>
							<!-- permanent damage list view model end -->
                    </div>
              </div>
                    	
                    	
      	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!-- <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
       
    </main>
    <!--content end-->
</body>

</html>