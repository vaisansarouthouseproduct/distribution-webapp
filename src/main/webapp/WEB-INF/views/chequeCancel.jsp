<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
	
	
	$('#chqDetailsId').hide();
	$('#cancelDivId').hide();
	$('#addedDateTimeId').hide();
	$('#cancelReasonRow').hide();
	//allowed only numbers 
	$('#chqNo').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	
	
});


function getChequeDetails(chqNo) {
	if(chqNo==""){
		var $toastContent = $('<span>Please Enter Cheque Number <span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
	}else if(chqNo.length!=6){
		var $toastContent = $('<span>Please Enter Valid Cheque Number <span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
	}else{
		$.ajax({
			type : "GET",
			url : "${pageContext.request.contextPath}/fetchChequeDetaisByChequeNo?chqNo="+chqNo,
			/* data: "id=" + id + "&name=" + name, */
			beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
			success : function(data) {
				var chequeEntry = data;
				if(data==""){
					$('#chqDetailsId').hide();
					$('#cancelDivId').hide();
					
					var $toastContent = $('<span>Data not found or Enter Valid Cheque Number <span>');
					Materialize.Toast.removeAll();
					Materialize.toast($toastContent, 3000);
					$('#msg').text("Please Enter Valid Cheque No");
				}else{
					$('#chqDetailsId').show(); 
					$('#addedDateTimeId').show();
					
					var formattedDate = new Date(chequeEntry.chequeDate);
					var d = formattedDate.getDate();
					var m =  formattedDate.getMonth();
					m += 1;  // JavaScript months are 0-11
					var y = formattedDate.getFullYear();
					var chqDate1=d+"-"+m+"-"+y;
					
					// to get Cheque Issue Date time
					var formattedDate = new Date(chequeEntry.chequeAddedDate);
					var d = formattedDate.getDate();
					var m =  formattedDate.getMonth();
					m += 1;  // JavaScript months are 0-11
					var y = formattedDate.getFullYear();
					
					var hours = formattedDate.getHours(); //returns 0-23
					var minutes = formattedDate.getMinutes(); //returns 0-59
					var seconds = formattedDate.getSeconds(); //returns 0-59
					if(minutes<10)
						  minutesString = 0+minutes+""; //+""casts minutes to a string.
						else
						  minutesString = minutes;
					if(seconds<10){
						secondsString=0+""+seconds+"";
					}else{
						secondsString=seconds;
					}

					var chqIssueTime=d+"-"+m+"-"+y +" & "+ hours+":"+minutesString+":"+secondsString;
					
					//to get cancel Date Time
					// to get Cheque Issue Date time
					if(chequeEntry.cancelDate!=null){
						var formattedDate = new Date(chequeEntry.cancelDate);
						var d = formattedDate.getDate();
						var m =  formattedDate.getMonth();
						m += 1;  // JavaScript months are 0-11
						var y = formattedDate.getFullYear();
						
						var hours = formattedDate.getHours(); //returns 0-23
						var minutes = formattedDate.getMinutes(); //returns 0-59
						var seconds = formattedDate.getSeconds(); //returns 0-59
						if(minutes<10)
							  minutesString = 0+minutes+""; //+""casts minutes to a string.
							else
							  minutesString = minutes;
						if(seconds<10){
							secondsString=0+""+seconds+"";
						}else{
							secondsString=seconds;
						}

						var chqCancelDateTime=d+"-"+m+"-"+y +" & "+ hours+":"+minutesString+":"+secondsString;	
					}
					
					
					
				
					if(chequeEntry.bankChequeBook!=null){
						$('#accountNo').text(chequeEntry.bankChequeBook.bankAccount.accountNumber)
						$('#chqBookRefNo').text(chequeEntry.bankChequeBook.chqBookName);
					}else{
						$('#accountNo').text("Manual Printing");
						$('#chqBookRefNo').text("Manual Printing");
					}
					
					//$('#chqBookRefNo').text(chequeEntry.bankChequeBook.chqBookName);	
					$('#chqDate').text(chqDate1);
					$('#payeeName').text(chequeEntry.payeeName);
					$('#chqAmt').text(chequeEntry.chequeAmt);
					$('#remark').text(chequeEntry.remark);
					$('#chqStatus').text(chequeEntry.chequeStatus);
					$('#cancelReason').text(chequeEntry.cancelReason);
					
					$('#cancelChqNo').val(chqNo);
					
					$('#chqIssueDate').text(chqIssueTime);
					$('#chqCancelDate').text(chqCancelDateTime);
					
					if(chequeEntry.chequeStatus=="Cancelled"){
						$('#cancelDivId').hide();
						$('#cancelReasonRow').show();
					}else{
						$('#cancelDivId').show();
					}
					
				}
				
			/* 	$('#brandname').val(brand.name);	
				$('#brandId').val(brand.brandId);
				$('#brandname').focus();
				$("#saveBrandSubmit").html('<i class="material-icons left">send</i> update');
				 */
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Something Went Wrong"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000);
				}					
		
		});
	}
	
	
}

</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	

i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	
</style>
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        	 <!-- <button class="btn waves-effect waves-light blue-gradient" id="test">Text</button> -->
            <form action="saveCancelChequeEntry" method="post" id="saveCancelChequeForm">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Cheque Cancel </h4>
                    </div>
                    <div class="row">
	                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">rate_review</i>
	                        <input id="chqNo" type="text" class="validate" name="chqNo" required minlength="6" maxlength="6">
	                        <label for="chqNo" class="active"><span class="red-text">*&nbsp;</span>Enter Cheque Number</label>
	                     </div>
	                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                     	<button class="btn" type="button" onclick='getChequeDetails($("#chqNo").val())'>Get Details</button>
	                     </div>
                    
                    
                    </div>
                </div>
            
            
               <div class="row  z-depth-3" id="chqDetailsId">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Cheque Details </h4>
                    </div>
                    <div class="row">
	                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                       
	                       <input type="hidden" id="cancelChqNo" name="cancelChqNo" minLength="6" maxLength="6">
	                       <!-- <tr>
											<th>Account Holder Name</th>
											<td><input placeholder="Account Holder Name" id="accountHolderName" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>Account Type</th>
											<td><input placeholder="Account Type" id="accountType" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>Actual Balance</th>
											<td><input placeholder="Actual Balance" id="actualBalance" type="text" class="validate center"></td>
										</tr> -->
	                       
	                       <tr>
	                      		 <th><b>Account No.:</b></th>
	                      		  <td><span id="accountNo" readonly></span></td>
	                       
	                       </tr>
	                       <br>
	                       <br>
	                       <tr>
	                       			<td><b>Cheque Book Ref.:</b></td>
	                       			<td><span id="chqBookRefNo"></span></td>
	                       			
	                       </tr>
	                       <br>
	                       <br>
	                       <tr>
	                       			<td><b>Cheque date:</b></td>
	                       			<td><span id="chqDate"></span></td>
	                       </tr>
	                       <br>
	                       <br>
	                       <tr>
	                     			<td><b>Payee Name:</b></td>
	                       			<td><span id="payeeName"></span></td>
	                       </tr>
	                       <br>
	                       <br>
	                       <tr>
			                       	<td><b>Cheque Amount:</b></td>
			                       	<td><span id="chqAmt"></span></td>
	                       	</tr>
	                       <br>
	                       <br>
	                       <tr>
	                       			<td><b>Remarks:</b></td>
	                       			<td><span id="remark"></span></td>
	                       </tr>
	                       <br>
	                       <br>
	                       <tr>
	                       			<td><b>Cheque Current Status:</b></td>
	                       			<td><span id="chqStatus"></span></td>
	                       </tr>
	                       <br>
	                       <br>
	                       
	                        <div id="cancelReasonRow">
                        		<tr>
	                       			<td><b>Cancel Reason:</b></td>
	                       			<td><span id="cancelReason"></span></td>
	                       		</tr>
	                       </div>
	                       
	                       <br>
	                       <br>
	                     
	                       
	                       </div>
	                       
	                       
                    	
                    	
                    
                    </div>
                    
                        
               
                </div>
                
                   <div class="row z-depth-3" id="addedDateTimeId">
		                   <div class="col s12 l12 m12">
		                       <div class="col s12 l5 m5 push-l1 push-m1">
		                       <br>
		                           <h6>Cheque Issue Date : 
		                          	 <span id="chqIssueDate" val=""></span>
		                           </h6>.
		                            <br>
		                       </div>
		                       <div class="col s12 l5 m5 push-l1 push-m1">
		                        <br>
		                           <h6>Cheque Cancel Date : 
									<span id="chqCancelDate" val=""></span>                           
		                           </h6>
		                            <br>
		                       </div>
		                   </div>
             	  </div>
                 <div id="cancelDivId" class="row  z-depth-3">
	                       <div class="col l10 offset-l1 m12 s12">
	                      
		                       
		                         <h5 class="center"> Proceed with Cancellation </h5>
		                       
		                        <div class="input-field col s12 m12 l12">
		                       		 <lable for="cancelReason">Enter Cancel Reason:</label>
		                       			<input type="text" id="cancelReason" name="cancelReason" required maxLength="200">
		                       
		                       </div>
		                       <div class="row">
		                        <div class="input-field col s12 m12 l12 center-align">
				                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveBusinessSubmit">Proceed<i class="material-icons right">send</i> </button>
				                </div>
				                </div>
				                
	                       </div>
	                     
                    	</div> 
                    	
                    	<%--  <div class="row z-depth-3" id="addedDateTimeId">
		                   <div class="col s12 l12 m12">
		                       <div class="col s12 l5 m5 push-l1 push-m1">
		                       <br>
		                           <h6>Added Date : 
									<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${businessName.businessAddedDatetime}" /><c:out value="${dt}" />
									&
									<fmt:formatDate pattern="HH:mm:ss" var="time" value="${businessName.businessAddedDatetime}" /><c:out value="${time}" />
		                           </h6>.
		                            <br>
		                       </div>
		                       <div class="col s12 l5 m5 push-l1 push-m1">
		                        <br>
		                           <h6>Last Updated Date : 
									<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${businessName.businessUpdatedDatetime}" /><c:out value="${dt}" />
									&
									<fmt:formatDate pattern="HH:mm:ss" var="time" value="${businessName.businessUpdatedDatetime}" /><c:out value="${time}" />                           
		                           </h6>
		                            <br>
		                       </div>
		                   </div>
             	  </div> --%>
            
               
          
                <br>
            </form>

        </div>

		
		

    </main>
    <!--content end-->
</body>

</html>
