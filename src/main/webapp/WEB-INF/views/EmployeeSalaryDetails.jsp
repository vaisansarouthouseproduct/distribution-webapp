<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript" src="resources/js/moment.js"></script>
    <script type="text/javascript">
   
    //base url
    var myContextPath = "${pageContext.request.contextPath}";
    
    //employee details id
    var employeeDetailsId="${employeeDetailsId}";
    
    //last filter range,startDate,endDate
    var lastStartDate="";
    var lastEndDate="";
    var lastChoice="1";
	
	var employeeNameForPrint,employeeDepartmentForPrint,employeeTotalAmtForPrint,employeeAmtPendingForPrint;
    $(document).ready(function() {
    	
    	
    	
     $("#incentivemodal").modal({
    	 dismissible: false
     });
   	 $('#empdetailtable').DataTable({
         "oLanguage": {
             "sLengthMenu": "Show _MENU_",
             "sSearch": " _INPUT_" //search
         },
         columnDefs: [
                      { 'width': '1%', 'targets': 0},
                      { 'width': '2%', 'targets': 1},
                      { 'width': '4%', 'targets': 2},
                      { 'width': '2%', 'targets': 3},
                      { 'width': '4%', 'targets': 4},
                     /*  { 'width': '2%', 'targets': 5}, */
                      { 'width': '5%', 'targets': 5},
                      { 'width': '5%', 'targets': 6}

                      ], 
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'All']
         ],
 
         //dom: 'lBfrtip',
         dom:'<lBfr<"scrollDivTable"t>ip>',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light   grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                    
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: '.print-col'
                     },
                     customize: function(doc, config) {
                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = ['*','*','*','*','*','*'] 
	                    		 } 
	                   	 })
                         var tableNode;
                         for (i = 0; i < doc.content.length; ++i) {
                           if(doc.content[i].table !== undefined){
                             tableNode = doc.content[i];
                             break;
                           }
                         }
        
                         var rowIndex = 0;
                         var tableColumnCount = tableNode.table.body[rowIndex].length;
                          
                         if(tableColumnCount > 6){
                           doc.pageOrientation = 'landscape';
                         }
                         /*for customize the pdf content*/ 
                         doc.pageMargins = [5,20,10,5];
                         
                         doc.defaultStyle.fontSize = 8	;
                         doc.styles.title.fontSize = 12;
                         doc.styles.tableHeader.fontSize = 11;
                         doc.styles.tableFooter.fontSize = 11;
                         doc.styles.tableHeader.alignment = 'center';
                         doc.styles.tableBodyEven.alignment = 'center';
                         doc.styles.tableBodyOdd.alignment = 'center';
					   },
					   messageTop: function () {                 
			                             return 'Name:'+employeeNameForPrint+'\n'+'Department :'+employeeDepartmentForPrint+'\n'+'Total Amount :'+employeeTotalAmtForPrint+'\n'+'Amount Pending :'+employeeAmtPendingForPrint;
			                         }, 
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
					 },
					 messageTop: function () {                 
			                             return 'Name:'+employeeNameForPrint+'     '+'Department :'+employeeDepartmentForPrint+'      '+'Total Amount :'+employeeTotalAmtForPrint+'      '+'Amount Pending :'+employeeAmtPendingForPrint;
			                         }, 
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading h4").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
					 },
					 customize: function (win) {
				                    	 console.log(win);
				                    	
				                         $(win.document.body)
				                             .css( 'font-size', '10pt' )
				                             .prepend(
				                                 '<h6 style="position:absolute; top:0; left:0;">Name:'+employeeNameForPrint+'</h6>'+
				                                 '<h6 style="position:absolute; top:0; right:10%;">Department: '+employeeDepartmentForPrint+'</h6>'+
				                                 '<br/>'+
				                                 '<h6 style="position:absolute; top:5%; left:0;">Total Amount: '+employeeTotalAmtForPrint+'</h6>'+
				                                 '<h6 style="position:absolute; top:5%; right:10%;">Amount Pending: '+employeeAmtPendingForPrint+'</h6>'+
												 '<br/>'
				                             );
				     				                     } ,
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }

     });	
    
   	$("select").change(function() {
        var t = this;
        var content = $(this).siblings('ul').detach();
        setTimeout(function() {
            $(t).parent().append(content);
            $("select").material_select();
        }, 200);
    });
      $('select').material_select();
	  $('.dataTables_filter input').attr("placeholder", "Search");
	  if ($.data.isMobile) {
	  $(".payBtn").removeClass('left left-align');
	  $(".payBtn").addClass('right right-align');
	  }
    	// on page load current month wise data show
    	filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	var areaArray=[];
    	$.ajax({
    		//url : "${pageContext.request.contextPath}/fetchEmployeeDetail?employeeDetailsId="+employeeDetailsId,//current month
    		url:url,
    		dataType : "json",
    		success : function(data) {
    			employeeSalaryStatus=data[0];
    			//console.log(employeeSalaryStatus);
    			areaArray=employeeSalaryStatus.areaList.split(',');
    			$("#areaList").html('');
    			for(var i=0; i < areaArray.length;i++){
    				$("#areaList").append("<tr>"+
					        "<td>"+(i+1)+"</td>"+
					        "<td>"+areaArray[i]+"</td>"+
					    	"</tr>");    
    			}
				//console.log(areaArray);
				employeeNameForPrint = employeeSalaryStatus.name;
				employeeDepartmentForPrint = employeeSalaryStatus.departmentname;
				employeeTotalAmtForPrint = employeeSalaryStatus.totalAmount.toFixedVSS(2);
				employeeAmtPendingForPrint = employeeSalaryStatus.amountPendingCurrentMonth.toFixedVSS(2);

    			$('#employeeareaname').text(employeeSalaryStatus.name);
    			$('#employeeareadeptname').text(employeeSalaryStatus.departmentname);
    			$('#employeeAddress').text(employeeSalaryStatus.address);    			
    			$('#empdetailname').html(employeeSalaryStatus.name);
    			$('#empdetaildept').html(employeeSalaryStatus.departmentname);
    			$('#empdetailmobile').html(employeeSalaryStatus.mobileNumber);
    			$('#empdetailaddress').html("<a id='viewAreaDetails' class='modal-trigger' href='#area'>View</a>");
    			
    			/* if(areaArray.length>1){
        			$('#empdetailareas').html("<a id='viewAreaDetails' class='modal-trigger' href='#area'>View</a>");
        			} 
    			else{
    				$('#empdetailareas').html(employeeSalaryStatus.areaList);
    			} */
    			
    			var date=new Date(parseInt(employeeSalaryStatus.date));
				month = date.getMonth() + 1;
				day = date.getDate();
				year = date.getFullYear();
				var joiningDate = day + "-" + month + "-" + year;
    			
    			$('#empdetailjoiningdate').html(joiningDate);
    			
    			$('#empdetailnoofholidays').html(employeeSalaryStatus.noOfHolidays+" days");
    			$('#empdetailbasicsalary').html(" &#8377;"+employeeSalaryStatus.basicMonthlySalary.toFixedVSS(2));
    			$('#empdetaildeduction').html(" &#8377;"+employeeSalaryStatus.deduction.toFixedVSS(2));
    			$('#empdetailincentive').html("<a id='incentivesModel' class='cursorPointer'> &#8377;"+employeeSalaryStatus.getIncentive.toFixedVSS(2)+"</a>");
    			$('#empdetailtotalsalary').html(" &#8377;"+employeeSalaryStatus.totalAmount.toFixedVSS(2));
    			$('#empdetailamtpaid').html(" &#8377;"+employeeSalaryStatus.amountPaidCurrentMonth.toFixedVSS(2));
    			$('#empdetailamtpending').html(" &#8377;"+employeeSalaryStatus.amountPendingCurrentMonth.toFixedVSS(2));
    			//alert(employeeSalaryStatus.employeeDetailsId);
    			//$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
    			$("#payHrefId").attr("href", "${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId);
    			
    			$('#incentivesModel').click(function(){
    				openIncentivesModel();
    			});
    			
    			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
    			
    			var t = $('#empdetailtable').DataTable();
						t.clear().draw();
						var count = $("#empdetailtable").find("tr:first th").length;
    			if(employeeSalarytable!==null)
    			{
    				$('#deleteModals').empty();
    				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
    				{
    					employeeSalary=employeeSalarytable[i];
    					/* var currentTime=new Date(parseInt(employeeSalary.date));
    					var month = currentTime.getMonth() + 1;
    					var day = currentTime.getDate();
    					var year = currentTime.getFullYear();
    					var date = day + "/" + month + "/" + year; */
    					var editDeleteButton;
    					//edit delete button show only employee is not disabled
    					if(employeeSalaryStatus.status==false){
	    					editDeleteButton="<a href='${pageContext.servletContext.contextPath}/editEmployeePayment?employeeSalaryId="+employeeSalary.employeeSalaryId+"&employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId+"' class='btn-flat' ><i class='material-icons'>edit</i></a>"+
	    					"<a href='#delete"+employeeSalary.employeeSalaryId+"' class='modal-trigger btn-flat' ><i class='material-icons'>delete</i></a>";
    					}else{
    						editDeleteButton="NA";
    					}
    					var comment;
    					if(employeeSalary.comment=='' || employeeSalary.comment==undefined){
    						comment='NA';
    					}else{
    						comment=employeeSalary.comment;
    					}
						t.row.add([
									employeeSalary.srno,
									employeeSalary.amount.toFixedVSS(2),
									moment(parseInt(employeeSalary.date)).format("DD/MM/YYYY")+' & '+moment(parseInt(employeeSalary.date)).format("h:mm:ss"),
									employeeSalary.modeOfPayment,
									employeeSalary.bankDetail,
									/* employeeSalary.checkDate, */
									comment,
									editDeleteButton
								  ]).draw(false); 
						//delete modal set for delete confirmation
						$('#deleteModals').append('<div id="delete'+employeeSalary.employeeSalaryId+'" class="modal deleteModal row">'+
									                '<div class="modal-content  col s12 m12 l12">'+
									                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
									                    '<h5 class="center-align">'+
									                	'Do you want to Delete?'+
														'</h5>'+
									                    '<br/>'+
									                 '</div>'+
									                '<div class="modal-footer">'+
									 				    '<div class="col s6 m6 l3 offset-l3">'+
									         				'<a  class="modal-action modal-close cursorPointer waves-effect btn red">Close</a>'+
										    			'</div>'+
														'<div class="col s6 m6 l3">'+
										 				   '<a onclick="deleteEmployeePayment('+employeeSalary.employeeSalaryId+')"'+
									                       'class="modal-action modal-close waves-effect cursorPointer btn">'+
									                       'Delete'+
									                       '</a>'+
										   				'</div>'+    
									    			'</div>'+
									            '</div>');
												
    				}    				
    			}
    			else
    			{
					t.row.add('<tr>'
							+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
							+ '</tr>');					
    			}
    			
    			$('.modal').modal();
    		}
    	});
    	
    	//update incentive
    	$('#saveIncentivesSubmit').click(function(){
			
			var incentiveAmount=$('#incentiveAmount').val();
			var incentiveReasonId=$('#incentiveReasonId').val();
			if(incentiveAmount==="")
			{
				$('#msgincentivesave').html("Enter Incentive Amount");
				return false;
			}
			if(incentiveReasonId.trim()==="")
			{
				$('#msgincentivesave').html("Enter Incentive Reason");
				return false;
			}
			var form = $('#saveIncentive');
			
				$('#msgincentivesave').html("");
				
				$.ajax({
					type : form.attr('method'),
					url : myContextPath+"/editIncentivesAjax",
					data : form.serialize(),
					success : function(data) {
					
						if(data==="Success")
						{
							$('#msgincentivesave').html("<font color='green'>Incentive SuccessFully Updated</font>");
							$('#incentiveName').val('');
					    	$('#incentiveAmount').val('');
					    	$('#incentiveReasonId').val('');
					    	
					     	$('#startDateIncentive').val(lastStartDate);
							$('#startDateIncentive').val(lastEndDate);
					    	searchEmployeeIncentive(lastChoice);
						}
						else
						{
							$('#msgincentivesave').html("<font color='red'>Incentive Updating Failed</font>");
						}
						
						$('#startDate1').val(lastStartDate);
		    			$('#endDate1').val(lastEndDate);
		    			searchEmployeeSalary(lastChoice);
					},
					error: function(xhr, status, error) {
						$('#addeditmsg').modal('open');
						$('#head').html("Incentive Update Error :  ");
						$('#msg').html("<font color='red'>Incentive Updating Problem</font>");
					}
				});
			
			
		});
		$('#incentivesModel').click(function(){
			openIncentivesModel();
		});
		
		var msg="${saveMsg}";
     	 //alert(msg);
     	 if(msg!='' && msg!=undefined)
     	 {
			Materialize.Toast.removeAll();
     		Materialize.toast(msg, '2000', 'teal lighten-2');
     	 }
	});
    //open incentive modal with data by last filtered range,startDate,endDate
    function openIncentivesModel()
    {

    	$('#incentiveName').val('');
    	$('#incentiveAmount').val('');
    	$('#msgincentivesave').html('');
    	$('#incentiveReasonId').val('');
    	
    	$('#startDateIncentive').val(lastStartDate);
		$('#startDateIncentive').val(lastEndDate);
    	searchEmployeeIncentive(lastChoice);
    	
    	$('#incentivemodal').modal('open');
    	$("#saveIncentive").hide();
    }
    //filter incentive data by startdate,enddate,range(choice)
    function searchEmployeeIncentive(choice)
    {
    	$(".showDates").hide();
    	$("#saveIncentive").hide();
    	
    	var filter="";
    	var url="";
    	var startDate=$('#startDateIncentive').val();
    	var endDate=$('#endDateIncentive').val();
    	
    	if(choice==1)
    	{
    		filter="CurrentMonth";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==2)
    	{
    		filter="Last6Months";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";		
    	}
    	else if(choice==3)
    	{
    		filter="Last1Year";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==4)
    	{
    		filter="Range";
    		
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
    	}
    	else if(choice==5)
    	{
    		filter="ViewAll";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    		
    	
    	$.ajax({
    		type : "GET",
    		url : url,
    		/* data: "id=" + id + "&name=" + name, */
    		success : function(data) {
    			 
    			employeeIncentiveList=data.employeeIncentiveList;
    			
    			$('#incentiveName').val(data.employeeName);
    			
    			$("#incentiveTable").empty();
    			if(employeeIncentiveList!==null)
    			{
    				$('#incentiveModals').empty();
    				for (var i = 0, len = employeeIncentiveList.length; i < len; ++i)
    				{
    					employeeIncentive=employeeIncentiveList[i];
    					var incentiveGivenDate=new Date(parseInt(employeeIncentive.incentiveGivenDate));
    					var month = incentiveGivenDate.getMonth() + 1;
    					var day = incentiveGivenDate.getDate();
    					var year = incentiveGivenDate.getFullYear();
    					var incentiveGivenDateString = day + "/" + month + "/" + year;
    					
    					var editDeleteButton;
    					if(data.status==true)
						{
    						editDeleteButton="<td><a href='#' class='btn-flat' disabled='disabled' ><i class='material-icons'>edit</i></a>"+
    						"<a href='#' class='btn-flat' disabled='disabled' ><i class='material-icons'>delete</i></a></td>";
						}	
						else
						{
							editDeleteButton="<td><a href='#' class='btn-flat' onclick='editIncentives("+employeeIncentive.employeeIncentiveId+")'><i class='material-icons'>edit</i></a>"+
							"<a onclick='deleteConfirmationForIncentiveDelete("+employeeIncentive.employeeIncentiveId+")' href='#' class=' btn-flat' ><i class='material-icons'>delete</i></a></td>";
						
							 /* $('#incentiveModals').append('<div id="delete'+employeeIncentive.employeeIncentiveId+'" class="modal deleteModal row">'+
											                 '<div class="modal-content  col s12 m12 l12">'+
											                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
											                    '<h5 class="center-align">'+
											                	'Do you want to Delete?'+
																'</h5>'+
											                    '<br/>'+
											                 '</div>'+
											                '<div class="modal-footer">'+
											 				    '<div class="col s6 m6 l3 offset-l3">'+
											         				'<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>'+
												    			'</div>'+
																'<div class="col s6 m6 l3">'+
												 				   '<a href="#" onclick="deleteIncentives('+employeeIncentive.employeeIncentiveId+')"'+
											                       'class="modal-action modal-close waves-effect  btn">Delete</a>'+
												   				'</div>'+    
											    			'</div>'+
											            '</div>'); */ 							
						}
    					
    					$("#incentiveTable").append("<tr>"+
    										        "<td>"+(i+1)+"</td>"+
    										        "<td>"+employeeIncentive.incentiveAmount.toFixedVSS(2)+"</td>"+
    										        "<td>"+employeeIncentive.reason+"</td>"+
    										        "<td>"+incentiveGivenDateString+"</td>"+
    										        editDeleteButton+
    										    	"</tr>");     						
    				}
    				//$('.modal').modal();
    				
    			}				
    		}
    	});
    	
    }
    //delete incentive confirmations
    function deleteConfirmationForIncentiveDelete(id){
    	/* $('.modal').modal();
    	$('#delete'+id).modal('open'); */
    	Materialize.Toast.removeAll();
    	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deleteIncentives('+id+')">Delete</button>  '));
    	  Materialize.toast($toastContent, "abc");
    }
    //on edit incentive button click fill incentive amount, reason
    function editIncentives(id){
    	$('#incentiveId').val(id);
    	$('#msgincentivesave').html('');
    	$.ajax({
    		type : "GET",
    		url : myContextPath+"/fetchEmployeeIncentivesByEmployeeIncentivesId?employeeIncentivesId="+id,
    		success : function(data) {
    			$('#incentiveAmount').val(data.incentiveAmount.toFixedVSS(2));
    			$('#incentiveAmount').change();
    	    	$('#incentiveReasonId').val(data.reason);
    	    	$('#incentiveReasonId').change();
    	    	$('#msgincentivesave').html('');
    	    	$("#saveIncentive").show();
    		},
    		error: function(xhr, status, error) {
    			$('#msgincentivesave').html("<font color='red'>Something went wrong</font>");
			}
    	});
    }
    /**
     *delete incentive by incentive id
     */
    function deleteIncentives(id){
    	$.ajax({
    		type : "GET",
    		url : myContextPath+"/deleteIncentive?employeeIncentivesId="+id,
    		success : function(data) {
    			if(data=="Success"){
    				Materialize.Toast.removeAll();
    				$('#msgincentivesave').html("<font color='green'>Incentive Delete SuccessFully</font>");
    			 	$('#startDateIncentive').val(lastStartDate);
    				$('#startDateIncentive').val(lastEndDate);
    		    	searchEmployeeIncentive(lastChoice);
    			}else{
    				Materialize.Toast.removeAll();
    				$('#msgincentivesave').html("<font color='red'>Incentive Deleting Failed</font>");
    				 //Materialize.toast('Incentive Deleting Failed!', 3000)
    			}
    			//searchEmployeeIncentive(1);
    			
    			$('#startDate1').val(lastStartDate);
    			$('#endDate1').val(lastEndDate);
    			searchEmployeeSalary(lastChoice);
    		},
    		error: function(xhr, status, error) {
    			Materialize.Toast.removeAll();
				//$('#msgincentivesave').html("<font color='red'>Something went wrong</font>");
				Materialize.Toast.removeAll();
    			Materialize.toast('Something went wrong!', 3000)
			}
    	});
    }
    /**
     * filter employee payment list by startDate,enddate,range(choice)
     */
    function searchEmployeeSalary(choice)
    {
    	 $(".showDates1").hide();
    	var filter="";
    	var url="";
    	var startDate=$('#startDate1').val();
    	var endDate=$('#endDate1').val();
    	
    	lastStartDate=startDate;
    	lastEndDate=endDate;
    	lastChoice=choice;
    	
    	//alert(employeeDetailsId);
    	//return false;
    	if(choice==1)
    	{
    		filter="CurrentMonth";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==2)
    	{
    		filter="Last6Months";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    		
    	}
    	else if(choice==3)
    	{
    		filter="Last1Year";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==4)
    	{
    		filter="Range";
    		
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
    	}
    	else if(choice==5)
    	{
    		filter="ViewAll";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    		
    	
    	$.ajax({
    		type : "GET",
    		url : url,
    		/* data: "id=" + id + "&name=" + name, */
    		success : function(data) {
    			
				
    			employeeSalaryStatus=data[0];
    			areaArray=employeeSalaryStatus.areaList.split(',');
    			$("#areaList").html('');
    			for(var i=0; i < areaArray.length;i++){
    				$("#areaList").append("<tr>"+
					        "<td>"+(i+1)+"</td>"+
					        "<td>"+areaArray[i]+"</td>"+
					    	"</tr>");    
				}
				
				employeeNameForPrint = employeeSalaryStatus.name;
				employeeDepartmentForPrint = employeeSalaryStatus.departmentname;
				employeeTotalAmtForPrint = employeeSalaryStatus.totalAmount.toFixedVSS(2);
				employeeAmtPendingForPrint = employeeSalaryStatus.amountPendingCurrentMonth.toFixedVSS(2);

    			$('#empdetailname').html(employeeSalaryStatus.name);
    			$('#empdetaildept').html(employeeSalaryStatus.departmentname);
    			$('#empdetailmobile').html(employeeSalaryStatus.mobileNumber);
    			$('#empdetailaddress').html("<a id='viewAreaDetails' class='modal-trigger' href='#area'>View</a>");
    			//$('#empdetailaddress').html(employeeSalaryStatus.address);
    		 /* if(areaArray.length>1){
        			$('#empdetailareas').html("<a id='viewAreaDetails' class='modal-trigger' href='#area'>View</a>");
        			} 
    			else{
    				$('#empdetailareas').html(employeeSalaryStatus.areaList);
    			} */
    			//$('#empdetailareas').html(employeeSalaryStatus.areaList);
    			
    			var date=new Date(parseInt(employeeSalaryStatus.date));
				month = date.getMonth() + 1;
				day = date.getDate();
				year = date.getFullYear();
				var joiningDate = day + "-" + month + "-" + year;
    			
    			$('#empdetailjoiningdate').html(joiningDate);
    			$('#empdetailnoofholidays').html(employeeSalaryStatus.noOfHolidays+" days");
    			$('#empdetailbasicsalary').html("&#8377;"+employeeSalaryStatus.basicMonthlySalary.toFixedVSS(2));
    			$('#empdetaildeduction').html("&#8377;"+employeeSalaryStatus.deduction.toFixedVSS(2));
    			$('#empdetailincentive').html("<a id='incentivesModel' href='#'> &#8377;"+employeeSalaryStatus.getIncentive.toFixedVSS(2)+"</a>");
    			$('#empdetailtotalsalary').html("&#8377;"+employeeSalaryStatus.totalAmount.toFixedVSS(2));
    			$('#empdetailamtpaid').html("&#8377;"+employeeSalaryStatus.amountPaidCurrentMonth.toFixedVSS(2));
    			$('#empdetailamtpending').html("&#8377;"+employeeSalaryStatus.amountPendingCurrentMonth.toFixedVSS(2));
    			//alert(employeeSalaryStatus.employeeDetailsId);
    			//$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
    			$("#payHrefId").attr("href", "${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId);
    			
    			$('#incentivesModel').click(function(){
    				openIncentivesModel();
    			});
    			
    			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
    			
    			var t = $('#empdetailtable').DataTable();
						t.clear().draw();
						var count = $("#empdetailtable").find("tr:first th").length;
    			if(employeeSalarytable!==null)
    			{
    				$('#deleteModals').empty();
    				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
    				{
    					employeeSalary=employeeSalarytable[i];
    					var currentTime=new Date(parseInt(employeeSalary.date));
    					var month = currentTime.getMonth() + 1;
    					var day = currentTime.getDate();
    					var year = currentTime.getFullYear();
    					var date = day + "/" + month + "/" + year;
    					
    					var editDeleteButton;
    					//edit delete button show only employee is not disabled 
    					if(employeeSalaryStatus.status==false){
	    					editDeleteButton="<a href='${pageContext.servletContext.contextPath}/editEmployeePayment?employeeSalaryId="+employeeSalary.employeeSalaryId+"&employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId+"' class='btn-flat' ><i class='material-icons'>edit</i></a>"+
	    					"<a href='#delete"+employeeSalary.employeeSalaryId+"' class='modal-trigger btn-flat' ><i class='material-icons'>delete</i></a>";
    					}else{
    						editDeleteButton="NA";
    					}
    					var comment;
    					if(employeeSalary.comment=='' || employeeSalary.comment==undefined){
    						comment='NA';
    					}else{
    						comment=employeeSalary.comment;
    					}
    					
						t.row.add([
									employeeSalary.srno,
									employeeSalary.amount.toFixedVSS(2),
									moment(parseInt(employeeSalary.date)).format("DD/MM/YYYY")+' & '+moment(parseInt(employeeSalary.date)).format("h:mm:ss"),
									employeeSalary.modeOfPayment,
									employeeSalary.bankDetail,
									/*employeeSalary.checkDate,*/
									comment,
									editDeleteButton	
								  ]).draw(false); 
						//delete confirmation modal
						$('#deleteModals').append('<div id="delete'+employeeSalary.employeeSalaryId+'" class="modal deleteModal row">'+
									                '<div class="modal-content  col s12 m12 l12">'+
									                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
									                    '<h5 class="center-align">'+
									                	'Do you want to Delete?'+
														'</h5>'+
									                    '<br/>'+
									                 '</div>'+
									                '<div class="modal-footer">'+
									 				    '<div class="col s6 m6 l3 offset-l3">'+
									         				'<a  class="modal-action cursorPointer modal-close waves-effect btn red">Close</a>'+
										    			'</div>'+
														'<div class="col s6 m6 l3">'+
										 				   '<a onclick="deleteEmployeePayment('+employeeSalary.employeeSalaryId+')"'+
									                       'class="modal-action cursorPointer modal-close waves-effect  btn">Delete</a>'+
										   				'</div>'+    
									    			'</div>'+
									            '</div>');
						
						
    				}
    			}
    			else
    			{
					t.row.add('<tr>'
							+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
							+ '</tr>');					
    			}
    			//$('.modal').modal(); please do not uncomment these line for preventing modal clsoe
    		}
    	});
    }
    /**
     * delete employee payment by employeeSalaryId
     * refresh payment list by last accessed startDate,endDate,range
     */
    function deleteEmployeePayment(employeeSalaryId){
    	    	
    	$.ajax({
    		type : "GET",
    		url : "${pageContext.servletContext.contextPath}/deleteEmployeePayment?employeeSalaryId="+employeeSalaryId,
    		success : function(data) {
    			
    			if(data=="Success"){
    			
    				$('#startDate1').val(lastStartDate);
    		    	$('#endDate1').val(lastEndDate);    				
    		    	searchEmployeeSalary(lastChoice);
    		    	Materialize.Toast.removeAll();
    				Materialize.toast('Payment Deleted SuccessFully', '2000', 'teal lighten-2');
    			}else{
					Materialize.Toast.removeAll();
    				Materialize.toast('Payment Deleting Failed', '2000', 'teal lighten-2');
    			}
    			
    		},
			error: function(xhr, status, error) 
			{
				Materialize.Toast.removeAll();
				Materialize.toast('Payment Deleting Failed', '2000', 'teal lighten-2');
			}		
	});
    	
    }
    
    </script>
    
    <style>
        /*main {
            flex: 1 0 auto;
            padding-left: 255px !important;
        }*/
	.cursorPointer{
		cursor: pointer;
	}
         .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
        .blueshade{
        	background-color:#3494cc !important;
        }
      @media only screen and (min-width:992px){
			 .leftHeader{
    	width:125px !important;
    	display:inline-block;
    }
        #area{
		width:37% !important;
	
		}
      }
	  @media only screen and (min-width:600px){
		#area{
		width:60% !important;
	
		}
	  }
	  @media only screen and (max-width:600px){
		.actionDivThis{
			margin: 20px 0; 
		}
		.rangeDiv{
			margin-bottom:20px;
		}
		#area,#incentivemodal{
			width:90% !important;
	
		}
	  }
	.noBottomMargin{
		margin-bottom: 0 !important;
	}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        
        <div class="row">
        <div class="col s12 l12 m12">
      	<div class="col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
       			
                <div class="col s12 l4 m4">
                <p>
                 <span class="leftHeader">Name:</span>
                 <b id="empdetailname"></b>
               		
                </p>
                </div>
                    <div class="col s12 l4 m4">
                       <p><span class="leftHeader">No.of Holiday:</span> 
                       <b id="empdetailnoofholidays">
                       </b>
                       </p>               		  
                </div>
                <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Total Amount:</span> 
                       <b id="empdetailtotalsalary">
                       </b>
                       </p>               		  
                </div>
                  <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Department:</span> 
                       <b id="empdetaildept">
                       </b>
                       </p>
               		  
                </div>
                
                <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Basic Salary:</span> 
                       <b id="empdetailbasicsalary">
                       </b>
                       </p>               		  
                </div>
                  <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Amount Paid:</span> 
                       <b id="empdetailamtpaid">
                       </b>
                       </p>               		  
                </div>
                
                    <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Mobile No:</span> 
                       <b id="empdetailmobile">
                       </b>
                       </p>               		  
                </div>
                
                
                
                  <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Incentive:</span> 
                       <b id="empdetailincentive">
                       </b>
                       </p>               		  
                </div>
               <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Amount Pending:</span> 
                       <b id="empdetailamtpending">
                       </b>
                       </p>               		  
                </div>
              
                   <div class="col s12 l4 m4">
                   <p><span class="leftHeader">Address-Area :</span>
               <b id="empdetailaddress">
                       </b></p>
                       <!-- <p><span class="leftHeader">Address:</span> 
                       <b id="empdetailaddress">
                       </b>
                       </p>       -->         		  
                </div>
               
                 <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Deduction:</span> 
                       <b id="empdetaildeduction">
                       </b>
                       </p>               		  
                </div>
                 <div class="col s12 l4 m4">
                   <p><span class="leftHeader">Joining Date :</span>
                   <b id="empdetailjoiningdate">
                       </b></p>
                            		  
                </div>
              
                <!-- <div class="col s12 l4 m4">
               
                <div clasus="col s6 m1 l4 left left-align"><span>Area:</span></div>
                <div class="col s6 m8 l8" style="overflow-wrap: break-word;padding-left:6%;white-space: pre-line;"><b id="empdetailareas"></b>
                </div>
                                   		  
                </div> -->
                </div> 
             </div> 
         </div>
        
        <div class="row noBottomMargin">
        		<div class="col s6 m2 l2 left left-align">
	               <button id="viewIncentive" onclick="openIncentivesModel()" class="btn btn-waves blue-gradient"><i class="material-icons right">send</i>Edit Incentive</button>
	           </div>
	              <div class="col s6 m2 l2 left left-align payBtn">
	               <a id="payHrefId" class="btn btn-waves blue-gradient cursorPointer"><i class="material-icons right ">send</i>Pay</a>
	           </div>
            
                <div class="col s12 m3 l3 right right-align actionDivThis">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light  right right-align' href='#' data-activates='filter1'>Action<i
       				 class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter1' class='dropdown-content'>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(1)">Current Month</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(2)">Last 6 Months</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(3)">Last 1 Year</button></li>
                        <li><button type="button" class="rangeSelect1 btn-flat">Range</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(5)">View All</button></li>
                    </ul>
                </div>
             
	            <div class="col s12 m4 l4 rangeDiv" style="margin-top:0">
                <form action="ledger">
                    <input type="hidden" name="range" value="dateRange"> 
                    <input type="hidden" name="employeeDetailsId" id="employeeDetailsId">
                    <span class="showDates1">
                      <div class="input-field col s4 m5 l5" style="margin-top:0">
                        <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate1" required>
                        <label for="startDate1">From</label>
                      </div>

                      <div class="input-field col s4 m5 l5" style="margin-top:0">
                            <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate1">
                             <label for="endDate1">To</label>
                       </div>
                        <div class="input-field col s4 m2 l2" style="margin-top:1%">
                    <button type="button"  onclick="searchEmployeeSalary(4)">View</button>
                    </div>
                  </span>
                </form>
            </div>
        </div>
        
       <!--  <div class="col s12 l6 m6  card-panel hoverable">
       
                <div class="col s12 l6 m6">
                    <p id="empdetailname" class="blue-text">Name:abc</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                    <p id="empdetaildept" class="blue-text">Department: abc</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailmobile" class="blue-text">Mobile No: 7506018815</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                   <p id="empdetailareas" class="blue-text">Area: Poisar</p>
                	  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                    <p id="empdetailaddress" class="blue-text">Address: </p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                  <p id="empdetailnoofholidays" class="blue-text">No.of Holiday:</p>
                	  <hr style="border:1px dashed teal;">
                </div>
            
          </div>  
        	<div class="col s12 l5 m6 offset-l1 card-panel hoverable">
        	    <div class="col s12 l6 m6 ">
                    <p id="empdetailbasicsalary" class="blue-text">Basic Salary:</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                    <p id="empdetaildeduction" class="blue-text">Incentive:</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                       <p id="empdetailincentive" class="blue-text">Deduction:</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailtotalsalary" class="blue-text">Total Amount:</p>
                	  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailamtpaid" class="blue-text">Amount Paid:7506018815</p>
               		  <hr style="border:1px dashed teal;">
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailamtpending" class="blue-text">Amount Pending:10101010</p>
                	  <hr style="border:1px dashed teal;">
                </div>
           
          </div>
 -->        
        
        
          
       

 		<div class="row">
 			
        
        
 <div class="col s12 l12 m12">
        <table id="empdetailtable" border="2" class="striped highlight centered" cellspacing="0" width="100%">
            <thead>
                <tr>
                     <th class="print-col">Sr. No</th>
                     <th class="print-col">Amount Paid</th>
                     <th class="print-col">Date</th>
                     <th class="print-col">Mode of Payment</th>
                     <th class="print-col">Bank Detail</th>
                     <!-- <th class="print-col">Cheque Date</th> -->
                     <th class="print-col">Comment</th>
                     <th>Action</th>
                 </tr>
            </thead>
            <tbody>
            	
            </tbody>
        </table>
        <br>
  </div>    
  </div>  
  <!-- edit incentive modal start -->
        <div id="incentivemodal" class="row modal modal-fixed-footer">
        
        		<div class="modal-content">
                    <h5 class="center"><u>Incentive Details</u><i class="modal-close material-icons right">clear</i></h5>
               
                    <form class="col s12 l12 m12" action="${pageContext.request.contextPath}/giveIncentives" method="post" class="col s12 l12 m12" id="saveIncentive">
                    <input id="incentiveEmployeeDetailsId" name="employeeDetailsId" type="hidden" value="${employeeDetailsId}"/>
                                      
                            <div class="input-field col s12 l3 m3 offset-l2">
                            	<input id="incentiveId" type="hidden" name="incentiveId" value="0">
                                <label for="incentiveAmount">Incentive</label>
                                <input id="incentiveAmount" type="text" name="incentives">
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                             <div class="input-field col s12 l4 m4">
                            	<label for="incentiveReasonId">Reason</label>
                               <textarea id="incentiveReasonId" class="materialize-textarea" name="reason" style="padding: 0 0 0 0;"></textarea>
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                             
                      			    <br> 
                      			<div class="col s6 m6 l2">
			              			  <button type="button" id="saveIncentivesSubmit" class="modal-action waves-effect  btn ">Update</button>
			               	    </div>
			               	   <!--  <div class="col s6 m6 l2">
			              			  <button type="reset" id="resetIncentive" class="modal-action waves-effect  btn ">Reset</button>
			                	 </div>  -->                    
			            </form>
			            <div class="col s12 l7  m5 offset-l3 offset-m4">                                
                   			  <font color='red'><span id="msgincentivesave"></span></font>                   			
            				</div> 
			            <div class="row">
                           <div class="col s12 m4 l4" style="margin-top:2%;">
		                            <!-- Dropdown Trigger -->
		                            <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filterIncentive'>Filter &nbsp &nbsp &nbsp &nbsp   <i
		                class="material-icons right">arrow_drop_down</i></a>
		                            <!-- Dropdown Structure -->
		                            <ul id='filterIncentive' class='dropdown-content'>
		                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeIncentive(1)">This Month</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(2)">Last 6 Months</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(3)">Last 1 Year</button></li>
		                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(5)">View All</button></li>
		                            </ul>
		                        </div>
		                        <form action="ledger">
		                            <input type="hidden" name="range" value="dateRange"> 
		                            <span class="showDates">
		                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsIdIncentive">
		                              <div class="input-field col s5 m3 l2">
		                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDateIncentive" required> 
		                                <label for="startDateIncentive">From</label>
		                              </div>
		                              <div class="input-field col s5 m3 l2">
		                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDateIncentive">
		                                     <label for="endDateIncentive">To</label>
		                               </div>
		                               <div class="input-field col s2 m3 l2 noPadding">
		                            <button type="button" onclick="searchEmployeeIncentive(4)">View</button>
		                            </div>
		                          </span>
		                        </form>
                        
                        </div>
                  <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="incentiveTable">
                    	
                    </tbody>
                </table>
                      </div>
        
             			 <div class="modal-footer">                  
                            
                          	
			                	 <div class="col s12 m12 l12 center">
			               				 <a href="#!" class="modal-close waves-effect btn red">Close</a>
		                		</div>
		                		  <br>
		               </div>  
        </div>
        
        
        
        
        <!-- edit incentive modal end-->
        <!-- view details of area -->
        <div class="row">
			<div class="col s12 m12 l2">
        <div id="area" class="modal row">
            <div class="modal-content">
                <h5 class="center"><u>Area Details</u><i class="modal-close material-icons right">clear</i></h5>
            
                	 
                    <div class="col s12 l6 m6">
                        <h6  class="black-text"><b>Name:</b> <span id="employeeareaname"></span></h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6  class="black-text"><b>Department:</b> <span id="employeeareadeptname"></span></h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 class="black-text"><b>Address:</b> <span id="employeeAddress"></span></h6>
                    </div>
                   
                
                <table id="tblArea" border="2" class="centered tblborder">
                	
                    <thead>
                    	
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Area Name</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            
				<div class="col s12 m12 l12 center">
                <a href="#!" class="modal-close waves-effect btn">Ok</a>
                </div>
            </div>
        </div>
	</div>
        </div>
        
<!--Add Edit Confirm Modal Structure -->
		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Structure for delete -->
        <div id="deleteModals"></div>
        <div id="incentiveModals"></div>
    </main>
    <!--content end-->
</body>

</html>