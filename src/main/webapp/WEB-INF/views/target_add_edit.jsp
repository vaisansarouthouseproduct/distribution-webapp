<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>

    <script>
    var commissionAssignListOld=[];
    var myContextPath = "${pageContext.request.contextPath}";
    var targetAssignedId="${targetAssign.targetAssignId}";
    if(targetAssignedId=='' || targetAssignedId==undefined){
    	targetAssignedId=0;
    }

    var mode="${mode}";
    <c:if test="${mode=='edit'}">
	    var employeeId="${targetAssign.employeeDetails.employee.employeeId}";	    
	    <c:if test="${not empty targetAssignTargets}">
		<c:forEach var="listValue" items="${targetAssignTargets}">
			commissionAssignListOld.push(["${listValue.commissionAssign.commissionAssignId}","${listValue.commissionAssign.commissionAssignName}","${listValue.targetValue}"]);
		</c:forEach>		
	 	</c:if>
 	</c:if>
    </script>
<script type="text/javascript">
 $(document).ready(function(){
	 $('select').material_select();
	 var msg="${saveMsg}";
	 if(msg!='' && msg!=undefined)
	 {
        Materialize.Toast.removeAll();
		 Materialize.toast(msg, '4000', 'teal lighten-2');
	 }
	 
	 $("#btnprint").click(function(){
		 $('.tblExport tr').find('th.print-col:last-child, td.print-col:last-child').hide();
			window.print();
			$('.tblExport tr').find('th.print-col:last-child, td.print-col:last-child').show();
		});
		   $("#btnExcel").click(function(e) {
	      	    e.preventDefault();
	      	 
	      	    //getting data from our table
	      	    var data_type = 'data:application/vnd.ms-excel';
	      	    var table_div = document.getElementsByClassName('tblExport');
	      	    //var table_div = document.getElementById('tblSales');
	      	    var table_html = table_div[0].outerHTML;
	      	    var a = document.createElement('a');
	      	    a.href = data_type + ', ' + escape(table_html);
	      	    a.download = 'Target Assign View'+'.xls';
	      	    a.click();
	     		
	      	  });
});

</script>
	<script type="text/javascript" src="resources/js/target_add_edit.js"></script>
<style>
/* .formBg{
	background-color:#cfd8dc !important;
	border:1px solid #b0bec5;
	border-radius:5px;
	padding:5px !important;
} */
label{
	color:black !important;
}
 .btnmarginInform{
	margin-top:4%;
} 
.btnmargin{
margin-top:2%;
}
.dataTables_wrapper th {
    padding: 5px 10px !important;
}
span.selectLabel{
left:3.1rem;
}
@media only screen and (max-width:600px){
	.modal{
				width:95%;
			}	
	.modal .modal-content{
		padding: 5px !important;
	}
	.modal .modal-content h5>u{
		width: 90%;
	}
}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
   margin:auto;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}

</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> 
        <div class="row">
        
        
        <div class="col s12 m12 l12">
         <div class="col s12 m12 l12 formBg">
        	<input id="brandId" type="hidden" name="brandId" value="0" > 
          	<div class="input-field col s12 m4 l2 left">
          		<i class="material-icons prefix">view_stream</i>
          		<span class="selectLabel"><span class="red-text">*</span>Select Department</span>
               	<select name="departmentId" id="departmentId" ${mode=='add'?'':'disabled'} >
                            <option value="0" selected>Department</option>
                            
                            <c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
								<c:choose>
								<c:when test="${mode=='add'}">
									<option value="<c:out value="${listValue.departmentId}"/>"><c:out
										value="${listValue.name}" /></option>
								</c:when>
								<c:otherwise>
									<option value="<c:out value="${listValue.departmentId}" />" ${listValue.departmentId==targetAssign.employeeDetails.employee.department.departmentId ? 'selected' : ''}><c:out
										value="${listValue.name}" /></option>
								</c:otherwise>
								</c:choose>
							</c:forEach>
							</c:if>
               </select>
          	</div>
          	
          	<div class="input-field col s12 m4 l4 left">
          		<i class="material-icons prefix">people</i>
          		<span class="selectLabel"><span class="red-text">*</span>Select Employee</span>
               	<select name="employeeId" id="employeeId" ${mode=='add'?'':'disabled'} multiple >
                     <option value="" disabled>Employee</option>
                </select>
          	</div>
          	
          	<%-- <div class="input-field col s12 m4 l4 left">
          		<i class="material-icons prefix">access_time <span class="red-text">*</span></i>
               	<select name="targetPeriodId" id="targetPeriodId" ${mode=='add'?'':'disabled'}>
                       <option value="">Target Period</option>
                       <option value="Daily" ${'Daily'==targetAssign.targetPeriod ? 'selected' : ''}>Daily</option>
                       <option value="Weekly" ${'Weekly'==targetAssign.targetPeriod ? 'selected' : ''}>Weekly</option>
                       <option value="Monthly" ${'Monthly'==targetAssign.targetPeriod ? 'selected' : ''}>Monthly</option>
                       <option value="Yearly" ${'Yearly'==targetAssign.targetPeriod ? 'selected' : ''}>Yearly</option>
               </select>
          	</div> --%>
          	
          	<div class="input-field col s12 m4 l2 left">
          		<i class="material-icons prefix">assignment</i>
          		<span class="selectLabel"><span class="red-text">*</span>Commission Assign</span>
               	<select id="commissionAssignId">
                          <option value="" selected disabled>Commission Assign</option>
               </select>
          	</div>
          	
            <div class="input-field col s12 m4 l2" style="padding-bottom:15px;">
            	<i class="fa fa-inr prefix"></i>               	
                <label for="targetValueId"><span class="red-text">*</span> Value</label>
                <input id="targetValueId" type="text" name="name" value="" required>
            </div>
            
            <div class="input-field col s12 l2 m2 center">
                <button type="button" id="addTarget" class="btn waves-effect waves-light blue-gradient" type="submit" name="action"><i class="material-icons left" >add</i>Add</button>
            </div>
            <!--  <div class="col s10 l2 m2 btnmarginInform">
                <button type="button" id="resetTarget" class="btn waves-effect waves-light blue-gradient" type="button" name="action"><i class="left material-icons">refresh</i>Reset</button>
            </div> -->
               
        </div> 
        <div class="col s12 l12 m12 center">
        <br>
		<button class="btn waves-effect waves-light blue-gradient"  id="btnprint">Print<i class="material-icons left">local_printshop</i> </button>
		
			<button class="btn waves-effect waves-light blue-gradient center"   id="btnExcel">Excel<i class="material-icons left">insert_drive_file</i> </button>
   			 </div>
        </div>
</div>    
<div class="row">
    <div id="section-to-print">
	 <div class="col s12 m12 l12"> 
       <table class="tblborder centered tblExport" border="1" id="tblDataTarget">
           <thead>
              <tr>
                  <th>Sr.No.</th>
                  <th>Commission Assign</th>
                  <th>Value</th>
                  <th class="print-col">Remove</th>
              </tr>
          </thead>
          <tbody id="tblDataTargetRows">
          
          </tbody>
       </table>
     </div>
     </div>  
</div>	 
	 <!-- submit target start -->
	 <div class="input-field col s12 m6 l4 offset-l5 center-align">
         <button class="btn waves-effect waves-light blue-gradient" id="saveTargetSubmit">Submit<i class="material-icons right">send</i> </button>
	 </div>
	 <!-- submit target end -->
	 
	 <!-- Modal Structure for View Commission Slabs -->

        <div id="viewDetails" class="modal">
            <div class="modal-content">
                <h5 class="center"><u>Commission Assign Details</u><i class="material-icons right modal-close">clear</i></h5>
                <div class="row productNameHide" >
                	<div class="col s12">
						<ul class="tabs filterTab">
							<li class="tab col s3"><a href="#productTable">Product list</a></li>
							<li class="tab col s3"><a class="active" href="#commissionTable" id="commissionTab">Commission List</a></li>
						</ul>
					</div>
                </div> 
                <div class="row">
                <div class="col s12" id="productTable">
						<div class="scrollDivTable">
						<table>
							<thead>
								<tr>
									<th colspan="3">Products</th>
								</tr>
							</thead>
							<tbody id="productsNamesBody">
								
							</tbody>
						</table>
					</div>
					</div>
					<div class="col s12" id="commissionTable">
						<div class="scrollDivTable">
	                <table border="2" class="tblborder center">
	                    <thead>
	                        <tr>
	                            <th>Sr.No</th>
	                            <th>From</th>
	                            <th>To</th>
	                            <th>Commission Type</th>
	                            <th>Target Period</th>
	                            <th>Commission</th>
	                        </tr>
	                    </thead>
	                    <tbody id="commissionAssignSlabId">
	                    </tbody>
					</table>
				</div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
            <div class="col s12 m12 l12 center">
            	<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
            </div>
                
            </div>
        </div> 
        <!-- End -->
    </main>
    <!--content end-->
</body>

</html>