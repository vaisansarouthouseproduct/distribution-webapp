<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<html>

<head>
<%@include file="components/header_imports.jsp"%>

<style>
.containerOfAccountDetails {
	margin: 0 auto;
	width: 95% !important;
}

.tabs .tab a:hover, .tabs .tab a.active {
	background-color: #0073b7 !important;
	border: 3px solid #0073b7 !important;
	color: white !important;
	font-weight: bold;
}

.tabs .tab a {
	border: 1px solid gray !important;
	color: darkgrey !important;
	display: block;
	width: 100%;
	height: 100%;
	padding: 0 24px;
	font-size: 14px;
	text-overflow: ellipsis;
	overflow: hidden;
	-webkit-transition: color .28s ease;
	transition: border .28s ease !important;
	background:#eeeeee !important;
}

.tabs .indicator {
	border: 1px solid transparent !important;
	position: absolute;
	bottom: 0;
	height: 52px;
	background-color: transparent;
	will-change: left, right;
}

#clearChequeModal {
	margin-top: 1% !important;
	height: 50% !important;
}

#bounceChequeModal {
	margin-top: 1% !important;
	height: 70% !important;
}

.modalButtonMargin {
	margin-left: 4% !important;
}

table, th, td {
	border: 1px solid black;
}
</style>
</head>

<body>
	<%@include file="components/navbar.jsp"%>
	<main class="paddingBody">
	<div class="row">
		
		<div class="col s12 m12 l4">
			<h5>${bankAccount.accountType.toUpperCase()}</h5>
			<h5>${bankAccount.accountNumber.toUpperCase()}</h5>
			<h5>${bankAccount.accountHolderName.toUpperCase()}</h5>
		</div>
		<div class="col s12 m12 l4 center">
			<br> <br> <a
				class="dropdown-button btn waves-effect waves-light printbtn cursorPointer"
				 data-activates="filter">Action<i
				class="material-icons right">date_range</i></a>
			<!-- Dropdown Structure -->
			<ul id="filter" class="dropdown-content actionDropdown">
				<li><a  onclick="fetchTransactions(0)"
					class="black-text hideDate cursorPointer">Last 10 Transactions</a></li>
				<li><a  onclick="fetchTransactions(1)"
					class="black-text hideDate cursorPointer">Yesterday</a></li>
				<li><a  onclick="fetchTransactions(2)"
					class="black-text hideDate cursorPointer">Today</a></li>
				<li><a  onclick="fetchTransactions(3)"
					class="black-text hideDate cursorPointer">Last 7 Days</a></li>
				<li><a  onclick="fetchTransactions(4)"
					class="black-text hideDate cursorPointer">Last 1 Month</a></li>
				<li><a  onclick="fetchTransactions(5)"
					class="black-text hideDate cursorPointer">Last 3 Months</a></li>
				<li><a class="rangeSelect black-text">Select Range</a></li>
				<li><a  onclick="fetchTransactions(6)"
					class="black-text hideDate cursorPointer">View All</a></li>
			</ul>
		</div>
		<div class="col s12 m12 l4 center">
			<br> <br> <input name="sort" type="radio" class="with-gap"
				id="ASCENDING" onclick="fetchTransactions(7)" checked />
			<label for="ASCENDING">Ascending</label> <input name="sort"
				type="radio" class="with-gap" onclick="fetchTransactions(8)"
				id="DESCENDING" /> <label for="DESCENDING">Descending</label>
		</div>
	</div>
	<div class="row container noMarginBottom">
		<div class="showDates">
			<form name="dateRangeForm" method="post">
				<div class="col s12 m12 l10 offset-l2">
					<input type="hidden" name="range" value="dateRange">
					<div class="input-field col s6 m6 l4">
						<input type="date" class="datepicker" placeholder="Choose Date"
							name="from" id="fromDate" required> <label for="fromDate">From</label>
					</div>
					<div class="input-field col s6 m6 l4">
						<input type="date" class="datepicker" placeholder="Choose Date"
							name="to" id="toDate"> <label for="toDate">To</label>
					</div>
					<div class="input-field col s6 m6 l4">
						<button type="button" onclick="fetchTransactions(13)"
							class="btn waves-effect waves-light">View</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col s12 m12 l12">
			<ul class="tabs">
				<li class="tab col s3"><a class="active"
					onclick="fetchTransactions(9)" href="#accountDetails">Account
						Details</a></li>
				<li class="tab col s3"><a href="#chequeIssued"
					onclick="fetchTransactions(10)">Cheque Issued</a></li>
				<li class="tab col s3"><a href="#chequeDeposited"
					onclick="fetchTransactions(11)">Cheque Deposited</a></li>
				<li class="tab col s3"><a href="#chequeBounced"
					onclick="fetchTransactions(12)">Cheque Bounced</a></li>
			</ul>
		</div>
		<div id="accountDetails" class="col s12 m12 l12 center">
			<div class="col l6 s12">
				<h5 id="opening"></h5>
			</div>
			<div class="col l6 s12">
				<h5 id="closing"></h5>
			</div>
			<div class="scrollDivTable">
			<table id="accountDetailsTable" class="centered striped highlight">
				<thead>
					<tr>
						<th>Date</th>
						<th>Party Name</th>
						<th>Payment Mode</th>
						<th>Deposit / Credit</th>
						<th>Withdrawal / Debit</th>
						<th>Balance</th>

					</tr>
				</thead>

				<tbody>
					<tr>
						<td>
							<!-- <select class="browser-default statusSelect" onchange="statusChanged(this);">
													<option value="Processing">Processing</option>
													<option value="Cleared">Cleared</option>
													<option value="Bounced">Bounced</option>
											</select> -->
						</td>
					</tr>
				</tbody>

			</table>
		</div>

		</div>
		<div id="chequeIssued" class="col s12 m12 l12 center">
			<br>
			<table id="chequeIssuedTable" class="centered striped highlight">
				<thead>
					<tr>
						<th>Date</th>
						<th>Party Name</th>
						<th>Cheque Detail</th>
						<th>Amount</th>
						<th>Status</th>

					</tr>
				</thead>

				<tbody>


				</tbody>

			</table>
		</div>
		<div id="chequeDeposited" class="col s12 m12 l12 center">
			<br>
			<table id="chequeDepositedTable" class="centered striped highlight">
				<thead>
					<tr>
						<th>Date</th>
						<th>Party Name</th>
						<th>Cheque Detail</th>
						<th>Amount</th>
						<th>Status</th>


					</tr>
				</thead>

				<tbody>


				</tbody>

			</table>
		</div>
		<div id="chequeBounced" class="col s12 m12 l12 center">
			<br>
			<table id="chequeBouncedTable" class="centered striped highlight">
				<thead>
					<tr>
						<th>Date</th>
						<th>Party Name</th>
						<th>Cheque Detail</th>
						<th>Deposit / Credit</th>
						<th>Withdrawal / Debit</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>

				<tbody>

				</tbody>

			</table>
		</div>
	</div>


	<!-- clear Cheque Modal  Structure -->
	<div id="clearChequeModal" class="modal modal-fixed-footer endCurve">
		<div class="modal-content">
			<h5 class="center-align" style="margin-top: 0px !important;">
				<u>Are you sure following cheque is cleared?</u><i
					class="material-icons notranslate right modal-close  waves-effect waves-red">close</i>
			</h5>
			<br>
			<div class="row noMarginBottom">
				<div class="col s12 m12 l12 center">
					<table id="clearChequeModalTable">
						<tr>
							<th>Party Name</th>
							<td></td>
						</tr>
						<tr>
							<th>Amount</th>
							<td></td>
						</tr>

						<tr>
							<th>Cheque Detail</th>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#!"
				class="modal-action modal-close waves-effect waves-green btn red">Cancel</a>
			<a href="#!" id="clearConfirmButton"
				class="waves-effect waves-green btn modalButtonMargin"
				data-uniqueId=""
				class="waves-effect waves-green btn modalButtonMargin"
				data-updatedStatus="CLEARED" onclick="caterClearModalData(this);">Confirm</a>
		</div>
	</div>
	<!--clearChequeModal ends--> <!-- bounce Cheque Modal  Structure -->
	<div id="bounceChequeModal" class="modal modal-fixed-footer endCurve">
		<div class="modal-content">
			<h5 class="center-align" style="margin-top: 0px !important;">
				<u>Are you sure following cheque is bounced?</u><i
					class="material-icons notranslate right modal-close  waves-effect waves-red">close</i>
			</h5>
			<br>
			<div class="row noMarginBottom">
				<div class="col s12 m12 l12 center">
					<table id="bounceChequeModalTable">
						<tr>
							<th>Party Name</th>
							<td></td>
						</tr>
						<tr>
							<th>Amount</th>
							<td></td>
						</tr>

						<tr>
							<th>Cheque Detail</th>
							<td></td>
						</tr>

						<tr>
							<th>Reason <span class="red-text">*</span></th>
							<td><textarea id="reason" class="materialize-textarea"
									placeholder="Enter reason here"></textarea></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#!"
				class="modal-action modal-close waves-effect waves-green btn red">Cancel</a>
			<a href="#!" id="bounceConfirmButton"
				class="waves-effect waves-green btn modalButtonMargin"
				data-uniqueId="" data-updatedStatus="BOUNCED"
				onclick="caterBounceModalData(this);">Confirm</a>
		</div>
	</div>
	<!--bounce modal ends--> </main>
	<%@include file="components/footer.jsp"%>
	<script>
		var transactionCount, startDate, endDate, ordering, transactionType, hitApi, chequeDetails, tabNumber;
		$(document).ready(function() {
			$(".showDates").hide();
			// enables date button
			$(".rangeSelect").on("click", function() {
				$(".showDates").show();
			});
			$(".hideDate").on("click", function() {
				$(".showDates").hide();
			});
			hitOnPageLoad();
		});

		function hitOnPageLoad() {
			fetchTransactions(0);
			fetchTransactions(7);
			hitApi = true;
			fetchTransactions(9);
		}

		function fetchTransactions(options) {

			switch (options) {
			case 0:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment;
				endDateMoment = startDateMoment;
				transactionCount = 10;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 1:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment.subtract(1, 'days');
				endDateMoment = startDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 2:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment;
				endDateMoment = endDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 3:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment.subtract(7, 'days');
				endDateMoment = endDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 4:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment.subtract(1, 'months');
				endDateMoment = endDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 5:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = startDateMoment.subtract(3, 'months');
				endDateMoment = endDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 6:
				var startDateMoment = moment();
				var endDateMoment = moment();
				startDateMoment = moment("1970-01-01", "YYYY-MM-DD");
				endDateMoment = endDateMoment;
				transactionCount = -1;
				startDate = startDateMoment.format("YYYY-MM-DD");
				endDate = endDateMoment.format("YYYY-MM-DD");
				break;
			case 7:
				ordering = "ASCENDING";
				break;
			case 8:
				ordering = "DESCENDING";
				break;
			case 9:
				transactionType = "ALL";
				chequeDetails = false;
				break;
			case 10:
				transactionType = "CHEQUE_ISSUED";
				chequeDetails = true;
				tabNumber = 1;
				break;
			case 11:
				transactionType = "CHEQUE_DEPOSITED";
				chequeDetails = true;
				tabNumber = 2;
				break;
			case 12:
				transactionType = "CHEQUE_BOUNCED";
				chequeDetails = true;
				tabNumber = 3;
				break;
			case 13:
				var startDateMoment = $("#fromDate").val();
				var endDateMoment = $("#toDate").val();
				transactionCount = -1;
				startDate = moment(startDateMoment).format("YYYY-MM-DD");
				endDate = moment(endDateMoment).format("YYYY-MM-DD");
				break
			}

			if (hitApi == true) {
				sendData();
			}

		}

		function sendData() {
			var fetchBankTransactionsRequest = {
				transactionType : transactionType,
				ordering : ordering,
				fromDate : startDate,
				toDate : endDate,
				transactionCount : transactionCount
			}

			$
					.ajax({
						type : 'POST',
						url : "${pageContext.request.contextPath}/fetchTransactionsForAccount",
						headers : {
							'Content-Type' : 'application/json'
						},
						data : JSON.stringify(fetchBankTransactionsRequest),
						success : function(resultant) {
							if (resultant.success == true) {
								console.log(resultant);
								setData(resultant);
							} else {
								Materialize.Toast.removeAll();
								Materialize.toast(resultant.msg, '3000',
										'toastError');
							}
						},
						error : function(err) {
							/*Show Dialog for Server Error*/
							Materialize.Toast.removeAll();
							Materialize.toast('Internal Server Error', '3000',
									'toastError');
						}
					});
		}

		function setData(resultant) {
			if (chequeDetails == true) {
				var data = resultant.bankAccountTempTransactions;
				var tableName;
				switch (tabNumber) {
				case 1:
					tableName = "chequeIssuedTable";
					$("#chequeIssuedTable tbody").empty();
					for (i = 0; i < data.length; i++) {
						var isCheque = data[i].payMode;
						var payChequeDetails;
						if (isCheque == "CHEQUE") {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						} else if (isCheque == "CASH") {
							payChequeDetails = data[i].payMode;
						} else {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						}

						var date = data[i].insertedDate.split(" ");
						var newDate = moment(date[0]).format("DD-MM-YYYY");
						$("#chequeIssuedTable tbody")
								.append(
										'<tr id="chequeIssuedRow_"' + data[i].id + '">'
												+ '<td> '
												+ newDate
												+ " & "
												+ date[1]
												+ ' </td>'
												+ '<td> '
												+ data[i].partyName
												+ ' </td>'
												+ '<td> '
												+ payChequeDetails
												+ ' </td>'
												+ '<td> '
												+ data[i].debit
												+ ' </td>'
												+ '<td> '
												+ '<select class="browser-default statusSelect" onchange="statusChanged(this);" id="status_'
												+ data[i].id
												+ '" '
												+ ' data-uniqueId='
												+ data[i].id
												+ ' data-partyName="'
												+ data[i].partyName
												+ '" data-amount="'
												+ data[i].debit
												+ '" data-chequeDetail = "'
												+ payChequeDetails
												+ '">'
												+ '<option value="PROCESSING">Processing</option>'
												+ '<option value="CLEARED">Cleared</option>'
												+ '<option value="BOUNCED">Bounced</option>'
												+ '</select>'
												+ ' </td>'
												+ '</tr>');
					}
					break;
				case 2:
					$("#chequeDepositedTable tbody").empty();
					for (i = 0; i < data.length; i++) {
						var isCheque = data[i].payMode;
						var payChequeDetails;
						if (isCheque == "CHEQUE") {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						} else if (isCheque == "CASH") {
							payChequeDetails = data[i].payMode;
						} else {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						}
						var date = data[i].insertedDate.split(" ");
						var newDate = moment(date[0]).format("DD-MM-YYYY");
						$("#chequeDepositedTable tbody")
								.append(
										'<tr id="chequeDepositedRow_"' + data[i].id + '">'
												+ '<td> '
												+ newDate
												+ " & "
												+ date[1]
												+ ' </td>'
												+ '<td> '
												+ data[i].partyName
												+ ' </td>'
												+ '<td> '
												+ payChequeDetails
												+ ' </td>'
												+ '<td> '
												+ data[i].credit
												+ ' </td>'
												+ '<td> '
												+ '<select class="browser-default statusSelect" onchange="statusChanged(this);" id="status_'
												+ data[i].id
												+ '" '
												+ ' data-uniqueId='
												+ data[i].id
												+ ' data-partyName="'
												+ data[i].partyName
												+ '" data-amount="'
												+ data[i].credit
												+ '" data-chequeDetail = "'
												+ payChequeDetails
												+ '">'
												+ '<option value="PROCESSING">Processing</option>'
												+ '<option value="CLEARED">Cleared</option>'
												+ '<option value="BOUNCED">Bounced</option>'
												+ '</select>'
												+ ' </td>'
												+ '</tr>');
						// $("#status_"+data[i].id+" option[value=" + data[i].transactionStatus + "]").attr("selected", "selected");
					}
					break;
				case 3:

					$("#chequeBouncedTable tbody").empty();
					for (i = 0; i < data.length; i++) {
						var isCheque = data[i].payMode;
						var payChequeDetails;

						if (isCheque == "CHEQUE") {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						} else if (isCheque == "CASH") {
							payChequeDetails = data[i].payMode;
						} else {
							payChequeDetails = data[i].bankName + " - "
									+ data[i].referenceNo;
						}
						var date = data[i].insertedDate.split(" ");
						var newDate = moment(date[0]).format("DD-MM-YYYY");
						var transactionAmount = 0;
						if (data[i].credit != 0) {
							transactionAmount = data[i].credit;
						} else if (data[i].debit != 0) {
							transactionAmount = data[i].debit;
						}
						$("#chequeBouncedTable tbody")
								.append(
										'<tr id="chequeBouncedRow_"' + data[i].id + '">'
												+ '<td> '
												+ newDate
												+ " & "
												+ date[1]
												+ ' </td>'
												+ '<td> '
												+ data[i].partyName
												+ ' </td>'
												+ '<td> '
												+ payChequeDetails
												+ ' </td>'
												+ '<td> '
												+ data[i].credit
												+ ' </td>'
												+ '<td> '
												+ data[i].debit
												+ ' </td>'
												+ '<td> '
												+ data[i].reason
												+ ' </td>'
												+ '<td> '
												+ '<select class="browser-default statusSelect" onchange="statusChanged(this);" id="status_'
												+ data[i].id
												+ '" '
												+ ' data-uniqueId='
												+ data[i].id
												+ ' data-partyName="'
												+ data[i].partyName
												+ '" data-amount="'
												+ transactionAmount
												+ '" data-chequeDetail = "'
												+ payChequeDetails
												+ '">'
												+ '<option value="BOUNCEDd">Bounced</option>'
												+ '<option value="CLEARED">Cleared</option>'
												+ '</select>'
												+ ' </td>'
												+ '</tr>');
						// $("#status_"+data[i].id+" option[value=" + data[i].transactionStatus + "]").attr("selected", "selected");
					}
					break;
				}

			} else if (chequeDetails == false) {
				var data = resultant.bankAccountTransactions;

				$("#opening").html(
						"<b>Opening Balance : " + resultant.opening + "</b>");
				$("#closing").html(
						"<b>Closing Balance : " + resultant.closing + "</b>");
				$("#accountDetailsTable tbody").empty();
				for (i = 0; i < data.length; i++) {
					var isCheque = data[i].payMode;
					var payChequeDetails;
					if (isCheque == "CHEQUE") {
						payChequeDetails = data[i].bankName + " - "
								+ data[i].referenceNo;
					} else if (isCheque == "CASH") {
						payChequeDetails = data[i].payMode;
					} else {
						payChequeDetails = data[i].payMode + " - "
								+ data[i].referenceNo;
					}
					var date = data[i].insertedDate.split(" ");
					var newDate = moment(date[0]).format("DD-MM-YYYY");
					$("#accountDetailsTable tbody").append(
							'<tr>' + '<td> ' + newDate + " & " + date[1]
									+ ' </td>' + '<td> ' + data[i].partyName
									+ ' </td>' + '<td> ' + payChequeDetails
									+ ' </td>' + '<td> ' + data[i].credit
									+ ' </td>' + '<td> ' + data[i].debit
									+ ' </td>' + '<td> ' + data[i].closing
									+ ' </td>' + '</tr>');
					// $("#status_" + data[i].id + " option[value=" + data[i].transactionStatus + "]").attr("selected", "selected");
				}
			}
		}
		/* after changing status */
		function statusChanged(data) {
			var selectedStatus = $(data).find("option:selected").val();
			var name = $(data).attr("data-partyName");
			var amount = $(data).attr("data-amount");
			var chequeDetail = $(data).attr("data-chequeDetail");
			var uniqueId = $(data).attr("data-uniqueId");
			switch (selectedStatus) {
			case 'CLEARED':
				$("#clearChequeModalTable").empty();
				$("#clearChequeModalTable").append(
						'<tr data-uniqueId=' + uniqueId + '>'
								+ '<th>Party Name</th>' + '<td> ' + name
								+ ' </td>' + '</tr>' + '<tr>'
								+ '<th>Amount</th>' + '<td> ' + amount
								+ ' </td>' + '</tr>' + '<tr>'
								+ '<th>Cheque Detail</th>' + '<td> '
								+ chequeDetail + ' </td>' + '</tr>');
				$("#clearConfirmButton").attr("data-uniqueid", uniqueId);
				$("#bounceChequeModal").modal('close');
				$("#clearChequeModal").modal('open');
				break;
			case 'BOUNCED':
				$("#bounceChequeModalTable").empty();
				$("#bounceChequeModalTable")
						.append(
								'<tr data-uniqueId=' + uniqueId + '>'
										+ '<th>Party Name</th>'
										+ '<td> '
										+ name
										+ ' </td>'
										+ '</tr>'
										+ '<tr>'
										+ '<th>Amount</th>'
										+ '<td> '
										+ amount
										+ ' </td>'
										+ '</tr>'
										+ '<tr>'
										+ '<th>Cheque Detail</th>'
										+ '<td> '
										+ chequeDetail
										+ ' </td>'
										+ '</tr>'
										+ '<tr>'
										+ '<th>Reason</th>'
										+ '<td> <textarea id="reason" class="materialize-textarea" placeholder="Enter reason here"></textarea> </td>'
										+ '</tr>');
				$("#bounceConfirmButton").attr("data-uniqueid", uniqueId);
				$("#clearChequeModal").modal('close');
				$("#bounceChequeModal").modal('open');
				break;
			}
		}

		function caterClearModalData(data) {
			var status = $(data).attr("data-updatedStatus");
			var reason = "";
			var uniqueid = $(data).attr("data-uniqueid");
			var updateTempTransactionRequest = {
				id : uniqueid,
				updatedStatus : status,
				reason : reason
			}
			submitModalData(updateTempTransactionRequest);
		}

		function caterBounceModalData(data) {
			var reason = $("#reason").val();
			if (reason == "") {
				Materialize.Toast.removeAll();
				Materialize
						.toast('Please provide reason', '3000', 'toastError');
				return false;
			}
			var status = $(data).attr("data-updatedStatus");
			var uniqueid = $(data).attr("data-uniqueid");
			var updateTempTransactionRequest = {
				id : uniqueid,
				updatedStatus : status,
				reason : reason
			}
			submitModalData(updateTempTransactionRequest);
		}

		function submitModalData(updateTempTransactionRequest) {
			$
					.ajax({
						type : 'POST',
						url : "${pageContext.request.contextPath}/updateTempTransaction",
						headers : {
							'Content-Type' : 'application/json'
						},
						data : JSON.stringify(updateTempTransactionRequest),
						success : function(resultant) {
							if (resultant.success == true) {
								console.log(resultant);
								Materialize.Toast.removeAll();
								Materialize.toast(
										'Transaction completed successfully',
										'3000', 'toastAlign');
								setTimeout(
										function() {
											window.location.href = "${pageContext.request.contextPath}/accountDetails"
										}, 500);
							} else {
								Materialize.Toast.removeAll();
								Materialize.toast(resultant.msg, '3000',
										'toastError');
							}
						},
						error : function(err) {
							/*Show Dialog for Server Error*/
							Materialize.Toast.removeAll();
							Materialize.toast('Internal Server Error', '3000',
									'toastError');
						}
					});
		}
	</script>
</body>

</html>