<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
			<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
				<html>

				<head>
					<%@include file="components/header_imports.jsp" %>
						<script type="text/javascript">
							var changeTb = "";
							$(document).ready(function () {
								var table = $('#tblData').DataTable();
								table.destroy();
								$('#tblData').DataTable({
									"oLanguage": {
										"sLengthMenu": "Show _MENU_",
										"sSearch": "_INPUT_" //search
									},
									autoWidth: false,
									columnDefs: [
										{ 'width': '1%', 'targets': 0 },
										{ 'width': '2%', 'targets': 1 },
										{ 'width': '12%', 'targets': 2 },
										{ 'width': '15%', 'targets': 3 },
										{ 'width': '3%', 'targets': 4 },
										{ 'width': '3%', 'targets': 5 },
										{ 'width': '5%', 'targets': 6 },
										{ 'width': '10%', 'targets': 7 },
										{ 'width': '5%', 'targets': 8 },
										{ 'width': '5%', 'targets': 9 },
										{ 'width': '1%', 'targets': 10 },
										{ 'width': '1%', 'targets': 11 }
									],
									lengthMenu: [
										[10, 25, 50, -1],
										['10', '25 ', '50 ', 'All']
									],

									//dom: 'lBfrtip',
									dom: '<lBfr<"scrollDivTable"t>ip>',
									buttons: {
										buttons: [
											//      {
											//      extend: 'pageLength',
											//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
											//  }, 
											{
												extend: 'pdf',
												className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
												text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
												//title of the page
												title: function () {
													var name = $(".heading").text();
													return name
												},
												//file name 
												filename: function () {
													var d = new Date();
													var date = d.getDate();
													var month = d.getMonth();
													var year = d.getFullYear();
													var name = $(".heading").text();
													return name + date + '-' + month + '-' + year;
												},
												//  exports only dataColumn
												exportOptions: {
													columns: ':visible.print-col'
												},
												customize: function (doc, config) {
													doc.content.forEach(function (item) {
														if (item.table) {
															item.table.widths = [20, 70, '*', '*', 40, 40, 30, 40, 50, 60, 60, 60]
														}
													})
													var tableNode;
													for (i = 0; i < doc.content.length; ++i) {
														if (doc.content[i].table !== undefined) {
															tableNode = doc.content[i];
															break;
														}
													}

													var rowIndex = 0;
													var tableColumnCount = tableNode.table.body[rowIndex].length;

													if (tableColumnCount > 6) {
														doc.pageOrientation = 'landscape';
													}
													/*for customize the pdf content*/
													doc.pageMargins = [5, 20, 10, 5];
													doc.defaultStyle.fontSize = 8;
													doc.styles.title.fontSize = 12;
													doc.styles.tableHeader.fontSize = 11;
													doc.styles.tableFooter.fontSize = 11;
													doc.styles.tableHeader.alignment = 'center';
													doc.styles.tableBodyEven.alignment = 'center';
													doc.styles.tableBodyOdd.alignment = 'center';
												},
											},
											{
												extend: 'excel',
												className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
												text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
												//title of the page
												title: function () {
													var name = $(".heading").text();
													return name
												},
												//file name 
												filename: function () {
													var d = new Date();
													var date = d.getDate();
													var month = d.getMonth();
													var year = d.getFullYear();
													var name = $(".heading").text();
													return name + date + '-' + month + '-' + year;
												},
												//  exports only dataColumn
												exportOptions: {
													columns: ':visible.print-col'
												},
											},
											{
												extend: 'print',
												className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
												text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
												//title of the page
												title: function () {
													var name = $(".heading").text();
													return name
												},
												//file name 
												filename: function () {
													var d = new Date();
													var date = d.getDate();
													var month = d.getMonth();
													var year = d.getFullYear();
													var name = $(".heading").text();
													return name + date + '-' + month + '-' + year;
												},
												//  exports only dataColumn
												exportOptions: {
													columns: ':visible.print-col'
												},
											},
											{
												extend: 'colvis',
												className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
												text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
												collectionLayout: 'fixed two-column',
												align: 'left'
											},
										]
									}

								});
								$("select").change(function () {
									var t = this;
									var content = $(this).siblings('ul').detach();
									setTimeout(function () {
										$(t).parent().append(content);
										$("select").material_select();
									}, 200);
								});
								$('select').material_select();
								$('.dataTables_filter input').attr("placeholder", "Search");
								//if there is mobile device the element contain this class will be hidden
								if ($.data.isMobile) {
									var table = $('#tblData').DataTable(); // note the capital D to get the API instance
									var column = table.columns('.hideOnSmall');
									column.visible(false);
								}
								$(".showDates").hide();
								$(".rangeSelect").click(function () {
									$("#oneDateDiv").hide();
									$(".showDates").show();
								});
								$("#oneDateDiv").hide();
								$(".pickdate").click(function () {
									$(".showDates").hide();
									$("#oneDateDiv").show();
								});
								/* on area change employee set on employee drop down */
								$('#areaId').change(function () {

									var areaId = $('#areaId').val();



									// Get the raw DOM object for the select box
									var select = document.getElementById('salesmanId');

									// Clear the old options
									select.options.length = 0;
									//$('#countryListForState').html('');

									//Load the new options

									select.options.add(new Option("SalesMan Filter", 0));

									table.draw();
									if (areaId === "0") {
										//changeTb="area";
										//table.draw();
										return false;
									}

									$.ajax({
										url: "${pageContext.request.contextPath}/fetchEmployeeDetailbyAreaId/" + areaId,
										dataType: "json",
										success: function (data) {

											/* alert(data); */
											var options, index, option;
											select = document.getElementById('salesmanId');

											for (var i = 0, len = data.length; i < len; ++i) {
												var area = data[i];
												select.options.add(new Option(area.name, area.employeeDetailsId));
											}
											changeTb = "area";
											table.draw();
										}
									});

								});
								/* 
								on change employee reset table data
								*/
								var table = $('#tblData').DataTable();
								$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {

									var searchText = $('.dataTables_filter input').val().toLowerCase();
									var id = data[2];
									var idName = id.trim();
									var salesmanId = $('#salesmanId').val();
									var salesmanName = $('#salesmanId option:selected').text();

									var areatb = data[7].trim();
									var areaId = $('#areaId').val();
									var areaName = $('#areaId option:selected').text();

									if (salesmanName === "SalesMan Filter") {


										if (areaId === "0") {
											var len = table.columns().nodes().length;
											for (var i = 0; i < len; i++) {
												var val = data[i].trim().toLowerCase();
												if (val.includes(searchText)) {
													return true;
												}
											}
										}


										var len = table.columns().nodes().length;
										for (var i = 0; i < len; i++) {
											var val = data[i].trim().toLowerCase();
											if (val.includes(searchText)) {
												if (areatb === areaName) {
													return true;
												}
											}
										}


										return false;
									}
									else {
										/* if(areaId==="0")
										{
											var len=table.columns().nodes().length;
											for(var i=0; i<len; i++)
											{
												var val=data[i].trim().toLowerCase();
												if(val.includes(searchText))
												{    							
													return true;	
												}
											} 	
										} */


										var len = table.columns().nodes().length;
										for (var i = 0; i < len; i++) {
											var val = data[i].trim().toLowerCase();
											if (val.includes(searchText)) {
												if (idName === salesmanName && areatb === areaName) {
													return true;
												}
											}
										}

										return false;
									}

								});

								$('#salesmanId').change(function () {
									changeTb = "salesman";
									//when i select any type then its show changes in datable depend on which type select
									table.draw();
								});



								/* employee selection validation */
								$('#searchButtonId').click(function () {

									var empId = $('#salesmanId').val();
									if (empId === "0") {
										$('#addeditmsg').modal('open');
										$('#msgHead').text("Filter Message");
										$('#msg').text("Select SalesPerson For Filter");
										return false;
									}

								});

							});

						</script>
						<style>
							/* #oneDateDiv{
								margin-top: 0.3rem; 
								margin-left:4%
							} */

							.input-field {
								position: relative;
								margin-top: 0.5rem;
							}

							/* .right {
								padding-right: 0 !important;
							} */
							.filerMargin{
								margin-top: 0.8rem;
							}
							.commentSection {
								height: 200px;
								overflow-y: auto;
								border: 1px solid #e0e0e0;
							}

							.commentSection p {
								padding: 10px;
							}
						</style>
				</head>

				<body>
					<!--navbar start-->
					<%@include file="components/navbar.jsp" %>
						<!--navbar end-->
						<!--content start-->
						<main class="paddingBody">
							<br class="hide-on-small-only">
							<div class="row" style="margin-bottom:0">

								<!-- <div class="col s12 l2 m2">
									<select name="orderStatus" id="orderStatusId">
									<option value="0"  selected>Order Status</option>
									<option value="Booked">Booked</option>
									<option value="Packed">Packed</option>
									<option value="Issued">Issued</option>
									<option value="Delivered">Delivered</option>
									<option value="Delivered Pending">Delivered Pending</option>
									<option value="Canceled">Canceled</option>
									</select>
								</div> -->
								<div class="col s6 m3 l2 right right-align actionDiv actionBtn  filerMargin">
									<!-- <div class="col s6 m4 l4 right"> -->
									<!-- Dropdown Trigger -->
									<a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action
										<i class="material-icons right">arrow_drop_down</i>
									</a>
									<!-- Dropdown Structure -->
									<ul id='filter' class='dropdown-content'>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=today&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Today</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=yesterday&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Yesterday</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=last7days&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Last 7 Days</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=currentMonth&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Current Month</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=lastMonth&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Last Month</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=last3Months&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">Last 3 Month</a>
										</li>
										<li>
											<a class="rangeSelect">Range</a>
										</li>
										<li>
											<a class="pickdate">Pick date</a>
										</li>
										<li>
											<a href="${pageContext.request.contextPath}/${url}?range=viewAll&businessNameId=${businessNameId}&employeeSMId=${employeeSMId}">View All</a>
										</li>
									</ul>
								</div>

								<div class="col s6 l2 m2 right right-align" style="padding-right:0;display:none">
									<select id="salesmanId" name="employeeDetailsId" class="btnSelect">
										<option value="0" selected>SalesMan Filter</option>
									</select>
								</div>
								<div class="col s6 l2 m3 right right-align actionDiv filerMargin">
									<select id="areaId" name="areaId" class="btnSelect">
										<option value="0">Area Filter</option>
										<c:if test="${not empty areaList}">
											<c:forEach var="listValue" items="${areaList}">
												<option value="<c:out value="${listValue.areaId}" />"><c:out value="${listValue.name}" /></option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<!-- <div class="col s12 l2 m2">
                <button class="btn waves-effect waves-light blue-gradient" style="margin-top:7%" id="searchButtonId">Search</button>
			</div> -->
								<div class="input-field col s12 l3 m3 offset-l4 offset-m center actionDiv" id="oneDateDiv">
									<form action="${pageContext.request.contextPath}/${url}" method="post">
										<div class="input-field col s5 m8 l7 offset-s3">
											<input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
											<label for="oneDate" class="black-text">Pick Date</label>
										</div>
										<div class="input-field col s2 m2 l2" style="margin-top:5%;">
											<button type="submit" class="btn">View</button>
											<input type="hidden" value="${businessNameId}" name="businessNameId">
											<input type="hidden" value="${employeeSMId}" name="employeeSMId">
											<input type="hidden" value=pickDate name="range">
										</div>
									</form>
								</div>
								<div class="input-field col s12 l4 m5 offset-l3  center actionDiv">
									<form action="${pageContext.request.contextPath}/${url}" method="post">
										<span class="showDates">
											<div class="input-field col s5 m5 l5">
												<input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required>
												<input type="hidden" value="${businessNameId}" name="businessNameId">
												<input type="hidden" value="${employeeSMId}" name="employeeSMId">
												<input type="hidden" value="range" name="range">
												<label for="startDate">From</label>
											</div>
											<div class="input-field col s5 m5 l5">
												<input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
												<label for="endDate">To</label>
											</div>

											<div class="input-field col s2 m1 l1">
												<button type="submit" class="btn">View</button>
											</div>
										</span>
									</form>
								</div>



							</div>
							<!--  </div> -->
							 <div class="row">
							<div class="col s12 l12 m12">
								<table class="striped highlight centered" id="tblData" width="100%">

									<thead>
										<tr>
											<th class="print-col hideOnSmall" rowspan="2">Sr. No</th>
											<th class="print-col" rowspan="2">Order Id</th>
											<th class="print-col" rowspan="2">Salesman Name</th>
											<th class="print-col" rowspan="2">Shop Name</th>
											<th class="print-col hideOnSmall" rowspan="2">Taxable Amount</th>
											<th class="print-col" rowspan="2">Total Amount</th>
											<th class="print-col" rowspan="2">Total Qty</th>

											<th class="print-col hideOnSmall" rowspan="2">Area</th>
											<th class="print-col" rowspan="2">Payment Period Days</th>
											<!-- <th class="print-col" rowspan="2">Order Date</th> -->
											<th class="print-col" colspan="3">Status</th>
											<c:if test="${sessionScope.loginType=='GateKeeper' and url!='cancelOrderReport'}">
												<th rowspan="2">Edit</th>
											</c:if>

										</tr>
										<tr>
											<th class="print-col">SalesMan</th>
											<th class="print-col">GateKeeper</th>
											<th class="print-col">DeliveryBoy</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty orderReportLists}">
											<c:forEach var="listValue" items="${orderReportLists}">
												<tr>
													<td>
														<c:out value="${listValue.srno}" />
													</td>
													<td>
														<c:choose>
															<c:when test="${fn:contains(listValue.orderId, 'CORD')}">
																<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/counterOrderProductDetailsListForWebApp?counterOrderId=${listValue.orderId}">
																	<c:out value="${listValue.orderId}" />
																</a>
															</c:when>
															<c:otherwise>
																<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/orderProductDetailsListForWebApp?orderDetailsId=${listValue.orderId}">
																	<c:out value="${listValue.orderId}" />
																</a>
															</c:otherwise>
														</c:choose>
													</td>
													<td class="wrapok">
														<c:out value="${listValue.employeeName}" />
													</td>
													<td>
														<c:out value="${listValue.shopName}" />
													</td>
													<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountWithoutTax}"
														/>
													</td>
													<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountWithTax}"
														/>
													</td>
													<td>
														<c:out value="${listValue.totalQuantity}" />
													</td>

													<td class="wrapok">
														<c:out value="${listValue.areaName}" />
													</td>
													<td>
														<c:out value="${listValue.paymentPeriodDays}" />
													</td>
													<%--  <td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.orderDate}"  /><c:out value="${date}" />
	                            </td> --%>
														<td class="wrapok">
															<c:choose>
																<c:when test="${url=='cancelOrderReport' and listValue.orderStatusSM=='Cancelled'}">
																	<a class="modal-trigger" href="#cancelReason_${listValue.srno}">
																		<c:out value="${listValue.orderStatusSM}" />
																	</a>
																</c:when>
																<c:otherwise>
																	<c:out value="${listValue.orderStatusSM}" />
																</c:otherwise>
															</c:choose>
															<br>
															<c:out value="${listValue.orderStatusSMDate}" />
															<br>
															<c:out value="${listValue.orderStatusSMTime}" />
														</td>
														<td class="wrapok">
															<c:choose>
																<c:when test="${url=='cancelOrderReport' and listValue.orderStatusGK=='Cancelled'}">
																	<a class="modal-trigger" href="#cancelReason_${listValue.srno}">
																		<c:out value="${listValue.orderStatusGK}" />
																	</a>
																</c:when>
																<c:otherwise>
																	<c:out value="${listValue.orderStatusGK}" />
																</c:otherwise>
															</c:choose>
															<br>
															<c:out value="${listValue.orderStatusGKDate}" />
															<br>
															<c:out value="${listValue.orderStatusGKTime}" />
														</td>
														<td>
															<c:choose>
																<c:when test="${url=='cancelOrderReport' and listValue.orderStatusDB=='Cancelled'}">
																	<a class="modal-trigger" href="#cancelReason_${listValue.srno}">
																		<c:out value="${listValue.orderStatusDB}" />
																	</a>
																</c:when>
																<c:otherwise>
																	<c:out value="${listValue.orderStatusDB}" />
																</c:otherwise>
															</c:choose>
															<br>
															<c:out value="${listValue.orderStatusDBDate}" />
															<br>
															<c:out value="${listValue.orderStatusDBTime}" />
														</td>

														<!-- edit only for gatekeeper -->
														<c:if test="${sessionScope.loginType=='GateKeeper' and url!='cancelOrderReport'}">
															<td>
																<c:choose>
																	<c:when test="${listValue.orderStatusCurrent=='Booked' or listValue.orderStatusCurrent=='Packed' or listValue.orderStatusCurrent=='Issued'}">
																		<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" href="${pageContext.request.contextPath}/editOrder?orderId=${listValue.orderId}">
																			<i class="material-icons">edit</i>
																		</a>
																	</c:when>
																	<c:otherwise>
																		NA
																	</c:otherwise>
																</c:choose>
															</td>
														</c:if>
												</tr>
												<c:if test="${url=='cancelOrderReport'}">
													<div id="cancelReason_${listValue.srno}" class="modal row">
														<div class="modal-content">
															<h5 class="center">
																<u>Cancel Reason</u>
															</h5>
															<div class="col s12 m12 l12">
																<div class="col s12 m12 l12">
																	<br>
																	<h6>
																		<b>Reason</b>
																	</h6>
																</div>
																<div class="col s12 m12 l12">
																	<div class="commentSection">
																		<p>
																			<c:out value="${listValue.cancelReason}" />
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<div class="col s12 m2 l2 offset-l5 center center-align">
																<a class="btn modal-action modal-close waves-effect">Ok</a>
															</div>
														</div>
													</div>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
							 </div>


							<div class="row">
								<div class="col s12 m12 l8">
									<div id="addeditmsg" class="modal">
										<div class="modal-content" style="padding:0">
											<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
											<!--  <h5 id="msgHead"></h5> -->

											<h6 id="msg" class="center"></h6>
										</div>
										<div class="modal-footer">
												<div class="col s12 center">
														<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
												</div>
												
											</div>
									</div>
								</div>
							</div>


						</main>
						<!--content end-->
				</body>

				</html>