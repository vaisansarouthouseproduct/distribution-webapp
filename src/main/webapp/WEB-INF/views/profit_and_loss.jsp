<!DOCTYPE html>
<html lang="en">
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
$(document).ready(function() {
			var from_$input = $('#startDate').pickadate(), from_picker = from_$input
				.pickadate('picker')
			var to_$input = $('#endDate').pickadate(), to_picker = to_$input
				.pickadate('picker')
			// Check if there’s a “from” or “to” date to start with.
			if (from_picker.get('value')) {
			to_picker.set('min', from_picker.get('select'))
			}
			if (to_picker.get('value')) {
			from_picker.set('max', to_picker.get('select'))
			}
			// When something is selected, update the “from” and “to”
			// limits.
			from_picker.on('set', function(event) {
			if (event.select) {
				to_picker.set('min', from_picker.get('select'))
			} else if ('clear' in event) {
				to_picker.set('min', false)
			}
			})
			to_picker.on('set', function(event) {
			if (event.select) {
				from_picker.set('max', to_picker.get('select'))
			} else if ('clear' in event) {
				from_picker.set('max', false)
			}
			});
			/*for validation of all fields  */
			/* $("form[action='filterByDateExpense']").validate({
			// Specify validation rules
			rules : {
				// The key name on the left side is the name attribute
				// of an input field. Validation rules are defined
				// on the right side
				from : "required",
				to : "required"
			},
			// Specify validation error messages
			messages : {
				from : "Please Enter starting date",
				to : "Please Enter ending date"
			},
			errorElement : 'div',
			errorPlacement : function(error, element) {
				var placement = $(element).data('error');
				if (placement) {
					$(placement).append(error)
				} else {
					error.insertAfter(element);
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler : function(form) {
				form.submit();
			}
			}); */
	
	/* to handle repeaing of columns in excel  */
	$("#excelBtn").click(function(){
		
		/* reloads page after 2 secs after clicking on excel */
		/*   window.setTimeout(function(){window.location.reload()}, 2000); */
	});
	
			$("#btnExcel").click(function(e) {
	      	    e.preventDefault();
	      	    //getting data from our table
	      	    var data_type = 'data:application/vnd.ms-excel';
	      	    var table_div = document.getElementsByClassName('tblExport');
	      	    //var table_div = document.getElementById('tblSales');
	      	    var table_html = table_div[0].outerHTML;
	      	    var tablefinal='<h4><b>Distribution System</b></h4><h4>${sessionScope.companyDetails.address}</h4>'+table_html+table_div[1].outerHTML;
	      	    var a = document.createElement('a');
	      	    a.href = data_type + ', ' + escape(tablefinal);
	      	    a.download = 'Profit And Loss  Report'+'.xls';
	      	    a.click();
	      	  });
	
	
	/*Profit loss info Ajax Call*/
	
	/* current month start and end date */
	var fromDate = moment().startOf('month').format("YYYY-MM-DD");
	var endDate = moment().endOf('month').format("YYYY-MM-DD");
	
	/* fetch profit and loss details by current month start and end date */
	var profitAndLossRequest={
			fromDate:fromDate,
			endDate:endDate
	}
	$.ajax({
		type:'POST',
		url:"${pageContext.request.contextPath}/profit-loss-info",
		headers:{
			'Content-Type' : 'application/json'
		},
		data:JSON.stringify(profitAndLossRequest),
		success:function(resultData){
			var totalSales=resultData.totalSales;
			var totalIncome=resultData.totalIncome;
			$('#datePeriod').html("for The Period Ended "+fromDate+ " to "+endDate);
			$('#totalSales').html(totalSales);
			$('#totalIncome').html(totalIncome);
			insertIntoProfitLoss(resultData);
				
		},
		error:function(err){
			alert("Internal Server Error");
		}
	});
	/* filter profit and loss by slected start and end date */
	//profit loss date filter function call by form name
	$('#viewPL').click(function(){
		var fromDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		
		var profitAndLossRequest={
				fromDate:fromDate,
				endDate:endDate
		}
		$.ajax({
			type:'POST',
			url:"${pageContext.request.contextPath}/profit-loss-info",
			headers:{
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			data:JSON.stringify(profitAndLossRequest),
			success:function(resultData){
				var totalSales=resultData.totalSales;
				var totalIncome=resultData.totalIncome;
				$('#datePeriod').html("for The Period Ended "+fromDate+ " to "+endDate);
				$('#totalSales').html(totalSales);
				$('#totalIncome').html(totalIncome);
				insertIntoProfitLoss(resultData);
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error:function(err){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				alert("Internal Server Error");
			}
			
		});
		return false;
	});
	/* set fetched records to page */
	function insertIntoProfitLoss(resultData){
		var response=resultData;
		var profitAndLossEntities=response.profitAndLossEntities;
		var totalExpenses=0,profitLossAmt=0;
		/* Append All Data*/
		$("#expensesTable tbody").remove();
		$("#expensesTable").append("<tbody>...</tbody>");
			for(var i=0;i<profitAndLossEntities.length;i++){
				var expenseType=profitAndLossEntities[i];
				totalExpenses+=expenseType.amount;
				$("#expensesTable tbody").append('<tr>'
						+'<td>'
						+expenseType.name
						+'</td>'
						+'<td>'
						+expenseType.amount
						+'</td>'
						+'</tr>');
			}
		 $('#expensesTable tbody').append('<tr>'
						+'<th  class="center">'+'Total'+'</th>'
						+'<th class="center">'+totalExpenses.toFixedVSS(2)+'</th>'
						+'</tr>');
		 profitLossAmt=totalIncome-totalExpenses;
		 $('#profitLossAmt').html(profitLossAmt);
		 $('.totalAmountClass').text(resultData.profitOrLoss);
		 
	}
	
});
</script>
<style>
		label {
			color: black;
			font-weight: bold;
		}

		.select-wrapper span.caret {
			color: initial;
			position: absolute;
			right: 0;
			top: 0;
			bottom: 5em !important;
			height: 10px;
			margin: auto 0;
			font-size: 10px;
			line-height: 10px;
		}
		.btnMarginForAlign{
			margin-top:2%;
		}
		@media print {
		  body * {
			visibility: hidden;
		  }
		  #section-to-print, #section-to-print * {
			visibility: visible;
		  }
		  #section-to-print {
			position: absolute;
			left: 0;
			top: 0;
		  }
		}
	
		 @page {
			   @bottom-left {
					content: counter(page) "/" counter(pages);
				}
			 }
			  .card{
			 height: 2.5rem;
			 line-height:2.5rem;
			/*  text-align:center !important; */
			 }
			.card-image{
					width:45% !important;
					background-color:#0073b7 !important;
				}
				.card-image h6{
				padding:5px;		
			}
			.card-stacked .card-content{
			 padding:5px;
			}
			
			@media only screen and (max-width: 600px) {
				.printBtnDiv{
	
				text-align: left !important;
				}
				.btnMarginForAlign{
					margin-top:5%;
				}
			}

		</style>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<main class="paddingBody">
	<div class="row">
	<div class="col s12 m12 l12" style="padding:0">
	<br>
	<div class="col s12 m3 hide-on-large-only">
			<h6><b>Total Revenue  : <span class="blueColor-text">&#8377;</span><span class="totalAmountClass blueColor-text"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitOrLoss}" /></span></b></h6>
		</div>
		<div class="col s3 m3 l3 hide-on-med-and-down" style="margin-top:1%;">
				<div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Revenue </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span class="totalAmountClass"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitOrLoss}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
		</div>	
		
		<!-- <form name="profitLossByDateFilter"  method="post"> -->
			<!-- <span class="showDates"> -->	
					
					<input type="hidden" name="range" value="dateRange"><!--  <span
						class="showDates"> -->
						<div class="input-field col s4 m2 l2">
							<input type="date" class="datepicker" placeholder="Choose Date"
								name="from" id="startDate" required> <label
								for="startDate">From</label>
						</div>
						<div class="input-field col s4 m2 l2">
							<input type="date" class="datepicker" placeholder="Choose Date"
								name="to" id="endDate" required> <label for="endDate">To</label>
						</div>
						<div class="input-field col s4 m1 l1 center btnMarginForAlign">
							<button type="button" id="viewPL" class="btn waves-effect waves-light">View</button>
						</div>
					
		 <!-- </form> -->
		
		<div class="col s6 l2 m2  right right-align printBtnDiv btnMarginForAlign">
    	     		<button class="btn waves-effect waves-light blue-gradient" onclick="window.print()"  id="btnprint">Print<i class="material-icons left">local_printshop</i> </button>
  				 </div>
    		<div class="col s6 l2 m2  right right-align excelBtnDiv btnMarginForAlign">
    				 <button class="btn waves-effect waves-light blue-gradient center"   id="btnExcel">Excel<i class="material-icons left">insert_drive_file</i> </button>
   			 </div>
		<!-- <div class="col s12 m6 l4">
			<button class="waves-effect waves-light btn teal hoverable"
				onclick="window.print()" style="margin-right:20px;">Print</button>
		</div>
		<div class="col s12 m6 l1" style="padding-top: 25px;"> 
			
			<button class="waves-effect waves-light btn teal hoverable"
				type="button" id="btnExcel" value="Export to Excel">Excel</button>
		</div> -->
		</div>
	</div>
	
		
		
	
	<!-- <h6 class="center-align">GST NO : 27AFHPD2383K1ZC</h6> -->
	<div id="section-to-print">
		<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="center-align"> <b> Profit Loss Report</b></h4>
			<h6 class="center-align" id="datePeriod"></h6>
			<h6 class="center-align paddingTop">
			<b>${sessionScope.companyDetails.companyName}</b>
			</h6>
			<h6 class="center-align" style="margin-bottom:10px;">${sessionScope.companyDetails.address}</h6>
			</div>
			<div class="col s12 m12 l12 center center-align">
	<table id="incomeTable" border="1" class="tblborder centered  highlight tblExport">
				<thead>
				<tr>
					<th colspan="2">
							<h5>Income Table</h5>
					</th>
					
				</tr>
					<tr>
						<th width="70%">
							Income
						</th>
						<th>
							Amount
						</th>
						
					</tr>
					
				</thead>
					<tbody>
					<tr>
					<td>
					Sales
					</td>
					<td id='totalSales'></td>
					
					</tr>
					</tbody>
					<thead>
					<tr>
						<th>
							Total
						</th>
						<th>
							<span id='totalIncome'></span>
						</th>
						
					</tr>
					
				</thead>
			</table>
			
			<br>
			<table id="expensesTable" border="1" class="tblborder centered  highlight tblExport">
				<thead>
				<tr>
					<th colspan="2">
							<h5>Expenses Table</h5>
					</th>
					
				</tr>
					<tr>
						<th width="70%">
							Expenses
						</th>
						<th>
							Amount
						</th>
						
					</tr>
					
				</thead>
				<tbody>
				</tbody>
			</table>
			<br>
			<table class="tblborder  centered  highlight tblExport" border="1">
				 <tr>
				 	<th class="center" width="70%">Total Revenue </th>
				 	<th class="center"><span class="totalAmountClass"></span></th>
				 </tr>
			</table>
		</div>
		
		
	</div>
	</div>
	
	</main>
	<%-- <%@include file="components/footer.jsp"%>
	<script src=${pageContext.request.contextPath}/resources/js/jsExcel.js
		type="text/javascript"></script> --%>
</body>
</html>