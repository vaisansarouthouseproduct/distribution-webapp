<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
	<%@include file="components/header_imports.jsp" %>
	<script src="http://cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>

	<script type="text/javascript">
		var mode = "${mode}";
		var myContextPath = "${pageContext.request.contextPath}";
		var commissionSlabArr = [];
		$(document).ready(function () {

			var msg = "${saveMsg}";
			//alert(msg);
			if (msg != '' && msg != undefined) {
				$('#addeditmsg').find("#modalType").addClass("success");
				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
				$('#addeditmsg').modal('open');
				//$('#msgHead').text("Brand Message");
				$('#msg').text(msg);
			}

			var table = $('#tblData').DataTable();
			table.destroy();
			$('#tblData').DataTable({
				"oLanguage": {
					"sLengthMenu": "Show _MENU_",
					"sSearch": "_INPUT_" //search
				},
				autoWidth: false,
				columnDefs: [
					{ 'width': '1%', 'targets': 0 },
					{ 'width': '12%', 'targets': 1 },
					{ 'width': '12%', 'targets': 2 },
					{ 'width': '3%', 'targets': 3 },
					{ 'width': '3%', 'targets': 4 }
				],
				lengthMenu: [
					[50, 75, 100, -1],
					['50 ', '75 ', '100', 'All']
				],


				//dom: 'lBfrtip',
				dom: '<lBfr<"scrollDivTable"t>ip>',
				buttons: {
					buttons: [
						//      {
						//      extend: 'pageLength',
						//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
						//  }, 
						{
							extend: 'pdf',
							className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: '.print-col'
							},
							customize: function (doc, config) {
								doc.content.forEach(function (item) {
									if (item.table) {
										item.table.widths = [20, 30, 100, 70, 50, 60]
									}
								})
								var tableNode;
								for (i = 0; i < doc.content.length; ++i) {
									if (doc.content[i].table !== undefined) {
										tableNode = doc.content[i];
										break;
									}
								}

								var rowIndex = 0;
								var tableColumnCount = tableNode.table.body[rowIndex].length;

								if (tableColumnCount > 6) {
									doc.pageOrientation = 'landscape';
								}
								/*for customize the pdf content*/
								doc.pageMargins = [5, 20, 10, 5];

								doc.defaultStyle.fontSize = 8;
								doc.styles.title.fontSize = 12;
								doc.styles.tableHeader.fontSize = 11;
								doc.styles.tableFooter.fontSize = 11;
								doc.styles.tableHeader.alignment = 'center';
								doc.styles.tableBodyEven.alignment = 'center';
								doc.styles.tableBodyOdd.alignment = 'center';
							},
						},
						{
							extend: 'excel',
							className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: '.print-col'
							},
						},
						{
							extend: 'print',
							className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
							//title of the page
							title: function () {
								var name = $(".heading").text();
								return name
							},
							//file name 
							filename: function () {
								var d = new Date();
								var date = d.getDate();
								var month = d.getMonth();
								var year = d.getFullYear();
								var name = $(".heading").text();
								return name + date + '-' + month + '-' + year;
							},
							//  exports only dataColumn
							exportOptions: {
								columns: '.print-col'
							},
						},
						{
							extend: 'colvis',
							className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
							text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
							collectionLayout: 'fixed two-column',
							align: 'left'
						},
					]
				}

			});
			$("select").change(function () {
				var t = this;
				var content = $(this).siblings('ul').detach();
				setTimeout(function () {
					$(t).parent().append(content);
					$("select").material_select();
				}, 200);
			});
			$('select').material_select();
			$('.dataTables_filter input').attr("placeholder", "Search");
			if ($.data.isMobile) {
				var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				var column = table.columns('.hideOnSmall');
				column.visible(false);

			}
			var table = $('#tblData').DataTable(); // note the capital D to get the API instance
			var column = table.columns('.toggle');
			column.visible(false);
			$('#showColumn').on('click', function () {

				//console.log(column);

				column.visible(!column.visible()[0]);

			});


		});

	</script>
	<script type="text/javascript" src="resources/js/commissionAssign.js"></script>
	<style>
		table.dataTable tbody td {
			padding: 0 2px !important;
		}

		.dataTables_wrapper {

			margin-left: 2px !important;
		}

		/* #sendMsg{
	width:35%;
	border-radius: 20px;
} */
		table.dataTable thead th,
		table.dataTable thead td {
			padding: 10px 10px;
			border-bottom: 1px solid #111;
		}
	</style>

</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp" %>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody">

		<!--Add New Supplier-->
		<div class="row">
			<br />
			<div class="col s10 l3 m6 left">
				<a class="btn waves-effect waves-light blue-gradient"
					href="${pageContext.request.contextPath}/commissionAssign?commissionAssignId=0"><i
						class="material-icons left">add</i>Commission Assign</a>
			</div>
			<div class="col s12 l12 m12 left">
				<br />
				<table class="striped highlight centered  display " id="tblData" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="print-col hideOnSmall">Sr. No.</th>
							<th class="print-col">Commission Name</th>
							<th class="print-col">Target Type</th>
							<th class="print-col">Target Period</th>
							<th class="print-col">Action</th>
						</tr>
					</thead>

					<tbody id="commissionAssignList">

					</tbody>
				</table>
			</div>
		</div>
		<!-- Modal Structure for View Commission Slabs -->

		<div id="viewDetails" class="modal">
			<div class="modal-content">
				<h5 class="center"><u>Commission Assign Details</u><i class="material-icons right modal-close">clear</i>
				</h5>
				<div class="row productNameHide">
					<div class="col s12">
						<ul class="tabs filterTab">
							<li class="tab col s3"><a href="#productTable">Product list</a></li>
							<li class="tab col s3"><a class="active" href="#commissionTable"
									id="commissionTab">Commission List</a></li>
						</ul>
					</div>

				</div>
				<div class="row">
					<div class="col s12" id="productTable">
						<table>
							<thead>
								<tr>
									<th colspan="3">Products</th>
								</tr>
							</thead>
							<tbody id="productsNamesBody">

							</tbody>
						</table>
					</div>
					<div class="col s12" id="commissionTable">
						<table border="2" class="tblborder centered">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>From</th>
									<th>To</th>
									<th>Commission Type</th>
									<th>Target Period</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody id="commissionAssignSlabId">
							</tbody>
						</table>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<div class="col s12 m12 l12 center">
					<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
				</div>

			</div>
		</div>
		<!-- End -->
		<div id="openCommissionAssignForm" class="modal modal-fixed-footer1">
			<div class="row">
				<div class="col l12 m12 s12">
					<h5 class="center"><b><u>Target Commission Assign</u></b></h5>
				</div>
				<div class="input-field col s3 m3 l4 forProductChange">
					<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>
					<select id="departmentId" name="departmentId">
						<option value="0">Department</option>
						<c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
								<option value="<c:out value=" ${listValue.departmentId}" />" >
								<c:out value="${listValue.name}" />
								</option>
							</c:forEach>
						</c:if>
					</select>
				</div>
				<div class="input-field col s3 m3 l4 ">
					<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>
					<select id="targetTypeId" name="targetTypeId">
						<option value="0">Target Type</option>
					</select>
				</div>
				<div class="input-field col s3 m3 l4 productDiv">
					<i class="material-icons prefix">filter_list<span class="red-text">*</span></i>
					<select id="productId" name="productId">
						<option value="0">Choose Product</option>
						<c:if test="${not empty productList}">
							<c:forEach var="listValue" items="${productList}">
								<option value="<c:out value=" ${listValue.productId}" />" >
								<c:out value="${listValue.productName}" />
								</option>
							</c:forEach>
						</c:if>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s3 m3 l4 forProductChange">
					<i class="material-icons prefix">access_time <span class="red-text">*</span></i>
					<select name="targetPeriodId" id="targetPeriodId">
						<option value="0">Target Period</option>
						<option value="Daily">Daily</option>
						<option value="Weekly">Weekly</option>
						<option value="Monthly">Monthly</option>
						<!-- <option value="Yearly">Yearly</option> -->
					</select>
				</div>
				<div class="input-field col s2 m3 l4 ">
					<i class="fa fa-inr prefix" aria-hidden="true"></i>
					<input id="commissionName" type="text" name="rate">
					<label for="commissionName" class="active">Name</label>
				</div>
			</div>
			<div class="row">
				<div class="col s3 m3 l4 forProductChange">
					<input id="includeTax" type="checkbox" />
					<label for="includeTax">Include Tax</label>
				</div>
				<div class="col s2 m3 l4 ">
					<input id="cascadComm" type="checkbox" />
					<label for="cascadComm">Cascading Commission</label>
				</div>
			</div>
			<div class="row">
				<div class="col l12 m12 s12 left">
					<br>
					<h6 class="center"><b><u>Commission Slab</u></b></h6>
				</div>
			</div>
			<div id="addCommSlab">
				<!-- <div class="row">
	                    	<div class="input-field col s1 m1 l1 push-l1">
		                        <span style="font-size:34px;">1.</span>
		                    </div>
	                    	<div class="input-field col s4 m4 l2 push-l1">
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="from0" type="text">
		                        <label for="from0" class="active">From</label>
		                    </div>
		                    <div class="input-field col s4 m4 l2  push-l1"> 
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="to0" type="text">
		                        <label for="to0" class="active">To</label>
		                    </div>
		                    <div class="input-field col s4 m4 l3  push-l1">  
		                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>                         
		                        <select id="commissionValueType0">
		                                 <option value="0">Value Type</option>
		                                 <option value="percentage">Percentage</option>
		                                 <option value="value">Value</option>
		                        </select>
		                    </div>  
		                    <div class="input-field col s4 m4 l2  push-l1"> 
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="value0" type="text">
		                        <label for="value0" class="active">Value</label>
		                    </div>
	                   </div> -->
			</div>
			<div class="row">
				<div class="input-field col s12 m12 l12 center">
					<button class="btn waves-effect waves-light blue-gradient" type="button" id="addNewSlabId"><i
							class="material-icons left">add</i>Add
						new slab</button>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12 modal-footer">
					<center>
						<a href="#!" id="commissionAssignId" class="waves-effect btn">Submit</a>
						<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
					</center>
				</div>
			</div>
		</div>



	</main>
	<!--content end-->
</body>

</html>