<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
    
    <script type="text/javascript">
	    
	    $(document).ready(function(){
	       	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	     /*  initComplete: function () {
	   	    	.on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val());
	   	      } */
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '3%', 'targets': 1},
	   	                   	  { 'width': '15%', 'targets': 2},
	   	                  	  { 'width': '7%', 'targets': 3},
	   	                  	  { 'width': '5%', 'targets': 4},
	   	                 	  { 'width': '5%', 'targets': 5},
	   	                	  { 'width': '3%', 'targets': 6},
	   	             		  { 'width': '3%', 'targets': 7},
	   	          			  { 'width': '5%', 'targets': 8},
	   	       				  { 'width': '1%', 'targets': 9}
	   	    				 
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [10, 25., 50, -1],
	   	             ['10 ', '25 ', '50 ', 'All']
	   	         ],
	   	         
	   	         
	   	         
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: '.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                         /* var tableNode;
	   	                         for (i = 0; i < doc.content.length; ++i) {
	   	                           if(doc.content[i].table !== undefined){
	   	                             tableNode = doc.content[i];
	   	                             break;
	   	                           }
	   	                         }
	   	        
	   	                         var rowIndex = 0;
	   	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	   	                          
	   	                         if(tableColumnCount > 6){
	   	                           doc.pageOrientation = 'landscape';
	   	                         } */
	   	                         /*for customize the pdf content*/ 
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                        doc.styles.tableHeader.alignment = 'center';
		                        doc.styles.tableBodyEven.alignment = 'center';
		                        doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select").change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
			 if ($.data.isMobile) {
				 
				 var table = $('#tblData').DataTable(); // note the capital D to get the API instance
				 var column = table.columns('.hideOnSmall');
				 column.visible(false);
		 }
		 	//hiding column on tablet device
			 if($.data.isTablet){
					var table = $('#tblData').DataTable(); // note the capital D to get the API instance
					var column = table.columns('.hideOnMed');
					column.visible(false);
				}
	    	$(".showDates").hide();
	        $(".rangeSelect").click(function() {
	       	 $("#oneDateDiv").hide();
	            $(".showDates").show();
	        });
	   	$("#oneDateDiv").hide();
	   	$(".pickdate").click(function(){
	   		 $(".showDates").hide();
	   		$("#oneDateDiv").show();
	   	});
	    
	   	//table data and amount show according area selected
   		var totalAmount=0; 
		 var table = $('#tblData').DataTable();	
	    	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
	    		
	    				var searchText=$('.dataTables_filter input').val().toLowerCase();
	    		
	    		        var areaName=data[4].trim();
	    				var areaName2=$("#areaId option:selected").text().trim();
	    				//alert(areaName+"-"+areaName2);
	    				if(areaName2==="Area Filter")
	    				{
	    					var len=table.columns().nodes().length;
	    					for(var i=0; i<len; i++)
	    					{
	    						var val=data[i].trim().toLowerCase();
	    						if(val.includes(searchText))
	    						{
	    							totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim());
	    							return true;	
	    						}
	    					}    					
	    				}
	    				
	    		        if ( areaName2 === areaName )
	    		        {
	    					var len=table.columns().nodes().length;
	    					for(var i=0; i<len; i++)
	    					{
	    						var val=data[i].trim().toLowerCase();
	    						if(val.includes(searchText))
	    						{
	    							totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim());
	    							return true;	
	    						}
	    					}	    		        	
	    		        }
	    		        return false;
	    		        
	    	}); 
	    	//table data and amount show according area selected
		$('#areaId').change(function(){
				totalAmount=0;
	    		table.draw();
	    		$('#totalAmountId').text(totalAmount);
	    	});
			/* $('.dataTables_filter input').unbind().on('keyup', function() {
				totalAmount=0;
	    		table.draw();
	    		$('#totalAmountId').text(totalAmount);
			}); */
	    });
    </script>
     <style>
   .card{
     height: 2.5rem;
     line-height:2.5rem;
	 margin-top:0 !important; 
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
	.card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
	.input-field {
		position: relative;
		margin-top: 3px;
	}
	.actionBtn {
    margin-top: 0;
	}
	@media only screen and (max-width: 600px){
		.marginTop{
		margin-top: 10px;
		margin-bottom: 10px !important;
	}
	}
	
	@media only screen and (min-width: 601px) and (max-width: 992px) {
		.input-field {
		position: relative;
		margin-top: 0.5rem;
	}
	.marginTop{
		margin-top: 0px;
		margin-bottom: 0px !important;
	}
    }
	/* #oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
} */
    </style>
    
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br class="hide-on-small-only">
        <div class="row">
        	<div class="col s12 l4 m4 left hide-on-med-and-down">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Amount </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${customerReportView.totalAmountAllBusiness}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
		   </div> 
		   <div class="col s7 m4 hide-on-large-only marginTop">
				<h6 class=""><b>Total Amount : <span id="totalAmountId" class="blueColor-text">&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${customerReportView.totalAmountAllBusiness}" /></span></b></h6>
		   </div> 
           <%-- <div class=" col s12 l4  m4 right">
                <h5 class="right red-text" >Total Amount :  <span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${customerReportView.totalAmountAllBusiness}" /></span></h5>
            </div> --%>
          
            
           <div class="col s5 m3 l2 right right-align actionDiv marginTop actionBtn">
              <!--   <div class="col s6 m4 l4 right">  style="margin-top:0.5%;width:15%;" -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=viewAll">View All</a></li>
                    </ul>
              
                </div>
                  <div class="col s12 m3 l2 right right-align hide-on-small-only">
               <select name="areaId" id="areaId" class="btnSelect">
                                 <option value="0" selected>Area Filter</option>
                           <c:if test="${not empty customerReportView.areaList}">
							<c:forEach var="listValue" items="${customerReportView.areaList}">
								<option value="<c:out value="${listValue.areaId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
               </select>
          	  </div>
				<div class="input-field col s12 l3 m3 actionDiv center" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchBusinessNameForReport" method="post">
	                    <div class="input-field col s4 m7 l6 offset-s3">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s2 m2 l2" style="margin-top:5%;">
	                     <button type="submit" class="btn">View</button>
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
               <div class="input-field col s12 l4 m5 actionDiv rangeDiv">   
                <form action="${pageContext.request.contextPath}/fetchBusinessNameForReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     
                             
                              <div class="input-field col s5 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s5 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                                 	 <div class="input-field col s2 m1 l1">
                            <button type="submit" class="btn">View</button>
                            </div>
                               
                          </span>
                </form>
            </div>
            
           <%--  <div class=" col s12 l4  m4 right">
                <h5 class="right red-text" >Total Amount :  <span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${customerReportView.totalAmountAllBusiness}" /></span></h5>
            </div> --%>
       <!--  </div>

        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No.</th>
                            <th class="print-col">Customer Id</th>
                            <th class="print-col">Customer Name</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col hideOnSmall">Area</th>
                            <th class="print-col hideOnSmall hideOnMed">Region</th>
                            <th class="print-col hideOnSmall">City</th>
                            <th class="print-col hideOnSmall">State</th>
                            <th class="print-col hideOnSmall hideOnMed">Country</th>
                            <!-- <th class="print-col">Date</th> -->
                            <th class="print-col">Orders</th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:if test="${not empty customerReportView.customerReportlist}">
					<c:forEach var="listValue" items="${customerReportView.customerReportlist}">
                        <tr>
                            <td><c:out value="${listValue.srno}"/></td>
                            <td><c:out value="${listValue.businessNameId}"/></td>
                            <td><c:out value="${listValue.businessName}"/></td>
                           <td class="wrapok"><c:out value="${listValue.totalAmount}"/></td>
                            <td><c:out value="${listValue.areaName}"/></td>
                            <td><c:out value="${listValue.regionName}"/></td>
                            <td><c:out value="${listValue.cityName}"/></td>
                            <td><c:out value="${listValue.stateName}"/></td>
                            <td><c:out value="${listValue.countryName}"/></td>
                            <%-- <td class="wrapok">
	                            <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.dateAdded}"  />
		                        <c:out value="${date}" />
                            </td> --%>
                            <td><a class="btn-floating  waves-effect waves-light" href="${pageContext.servletContext.contextPath}/showOrderReportByBusinessNameId?businessNameId=${listValue.businessNameId}&${partUrl}"><c:out value="${listValue.noOfOrders}"/></a></td>
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
    <!--content end-->
    <!--Modal Structure for Customer details 
        <div id="custDetails" class="modal">
            <div class="modal-content">
                <h4 class="center">Customer Details</h4>
                <hr>

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="orderId" class="black-text">Order Id:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="shopName" class="black-text">Shop Name:</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="salesmanName" class="black-text">Salesman Name:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="shopAdd" class="black-text">Shop Address:</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="date" class="black-text">Date:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="totalAmount" class="black-text">Total Amount:</h6>
                    </div>
                </div>
                <table border="2" class="centered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>5</td>
                            <td>123456</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Ok</a>
            </div>
        </div>-->



</body>

</html>