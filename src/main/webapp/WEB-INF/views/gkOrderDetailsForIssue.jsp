<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
	<script type="text/javascript">
	
	/* function productListQtyMngFunc(id,qty,isAdd){
		for(var i=0; i<productListQtyMng.length; i++){
			if(productListQtyMng[i].productId==id){
				var productListQtyMngTemp=productListQtyMng[i];
				productListQtyMng.splice(i, 1);
				if(isAdd){
					productListQtyMngTemp.currentQuantity+=parseInt(qty);
				}else{
					productListQtyMngTemp.currentQuantity-=parseInt(qty);
				}
				productListQtyMng.push(productListQtyMngTemp);
				break;
			}
		}
		refreshAllowedQuantityOfProducts();
	}
	function refreshAllowedQuantityOfProducts(){			
		for(var i=0; i<productListQtyMng.length; i++){
			$('.current_qty_'+productListQtyMng[i].productId).text(productListQtyMng[i].currentQuantity);
		}
	} 
	function productListData(){
		$.ajax({
			type:"GET",
			url : myContextPath+"/fetchProductListAjax",
			dataType : "json",
			async : false,
			success:function(data)
			{
				productListQtyMng=data;
				//console.log("product data list loaded");
			},
			error: function(xhr, status, error) {
				//console.log("product data list not loaded");
			}
		});
	} */

	isTransportationHave='No';
 	transportationId=0;
 	vehicalNumber=null;
	    docketNumber=null;
    $(document).ready(function() {
    	
    	<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
    	
    	/* issue quantity only allowed numbers */
    	$('#issuedQuantity${listValue.orderProductDetailsid}').keypress(function( event ){
    	    var key = event.which;
    	    
    	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
    	        event.preventDefault();
    	});
    	/* when issued quantity change */
    	$('#issuedQuantity${listValue.orderProductDetailsid}').keyup(function(){
    		
    		var issuedQuantity=$('#issuedQuantity${listValue.orderProductDetailsid}').val();
    		//alert(issuedQuantity);
    		if(issuedQuantity==="" || issuedQuantity===undefined){
    			issuedQuantity=0;
    		}
    		//issued quantity can not be greater than ordered quantity
    		if(parseInt(issuedQuantity) > parseInt($('#orderdQuantity${listValue.orderProductDetailsid}').text()))
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('${listValue.product.product.productName} Product Issued Quantity exceeds Order Quantity', 2000, 'teal lighten-2');
    			/*  $('#addeditmsg').modal('open');
	   		     $('#msgHead').text("Warning Message");
	   		     $('#msg').html("<font color='red'><b>${listValue.product.productName}</b> Product <b>Issued Quantity</b> exceed <b>Order Quantity</b></font>");
    		    setTimeout(function() {
    		    	$('#addeditmsg').modal('close');
    		    }, 2000); */
    		    $('#issuedQuantity${listValue.orderProductDetailsid}').val("0");
    		    //return false;
    		}
    		//issued quantity can not be greater than current quantity
    		if(parseInt(issuedQuantity)>parseInt($("#currrentQuantity_${listValue.type=='Free'?'Free' : 'NonFree'}_${listValue.orderProductDetailsid}").text()))
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('${listValue.product.product.productName} Product Issued Quantity exceeds Current Quantity', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		     $('#msgHead').text("Warning Message");
	   		     $('#msg').html("<font color='red'><b>${listValue.product.productName}</b> Product <b>Issued Quantity</b> exceed <b>Current Quantity</b></font>");
	   		    setTimeout(function() {
	   		    	$('#addeditmsg').modal('close');
	   		    }, 2000); */
	   		    $('#issuedQuantity${listValue.orderProductDetailsid}').val("0");
	   		    //return false;
    		}
			
			var issuedQuantity=$('#issuedQuantity${listValue.orderProductDetailsid}').val();
   		
			if(issuedQuantity==="" || issuedQuantity===undefined)
			{
    			issuedQuantity=0;
    		}
			
			//updating issued quantity on session order product list
    	    var data = {
    	    		'issuedQuantity': issuedQuantity,
    	    		'orderProductDetailsid': "${listValue.orderProductDetailsid}"
    	    	};
    	    
    		$.ajax({
        		type:"POST",
    			url : "${pageContext.request.contextPath}/issueProductDetailsCalculate",
        		dataType : "json",
        		data : data,
        		success : function(data) {
        			orderProductDetailListNew=data;
        			var totalOrderQuantity=0;
        			var totalIssuedQuantity=0;
        			var totalAmount=0;
        			
        			for(var i=0; i<orderProductDetailListNew.length; i++)
        			{
        				if(orderProductDetailListNew[i].orderProductDetailsid=="${listValue.orderProductDetailsid}")
        				{
        					$('#amountWithTax${listValue.orderProductDetailsid}').text(parseFloat(orderProductDetailListNew[i].issueAmount).toFixedVSS(2));
        				}
        				totalOrderQuantity=totalOrderQuantity+orderProductDetailListNew[i].purchaseQuantity;
        				totalIssuedQuantity=totalIssuedQuantity+orderProductDetailListNew[i].issuedQuantity;
        				totalAmount=totalAmount+orderProductDetailListNew[i].issueAmount;
        			}
        			
        			//$('#totalOrderQuantity').text(totalOrderQuantity);
        			$('#totalIssuedQuantity').text(parseInt(totalIssuedQuantity));
        			$('#totalOrderAmountWithTax').text(parseFloat(totalAmount).toFixedVSS(2));
        			
        		}
			});
			// productListData();
    	});
    	</c:forEach>
    	/* var transportId=$("#transport_select").val();
		var gstNo=$("#transGstNo").val();
		var vehicleNo=$("#transVehicleNo").val();
		var docektNo=$("#transDocketNo").val();
		if(transportId==="" || transportId===undefined)
		{
			Materialize.toast('Select Transport', 2000, 'teal lighten-2');
			
   		    return false;
		}
		else if(vehicleNo==="" || vehicleNo==undefined)
		{
			Materialize.toast('Vehicle no is required', 2000, 'teal lighten-2');
			
   		    return false;
		}
		else if(docektNo==="" || docektNo==undefined)
		{
			Materialize.toast('Docket no is required', 2000, 'teal lighten-2');
			
   		    return false;
		} */
		
		//transportation check box checked / un-checked
		$('#transport_details').change(function(){
			
			
				if(this.checked){					  
					isTransportationHave='No';
	     	    	transportationId=0;
	     	    	vehicalNumber=null;
	     	   	    docketNumber=null;					
					
					 // remove "selected" from any options that might already be selected
					  $('#transport_select option[selected="selected"]').each(
					      function() {
					          $(this).removeAttr('selected');
					      }
					  );

					  // mark the first option as selected
					  $("#transport_select option:first").attr('selected','selected');
					  
					  $("#transport_select").change();
					  $('#transportationModal').modal('open');
					  $('#transport_details').prop("checked",false);
					  $('#transDocketNo').val('');
				}		
				else{
					
					isTransportationHave='No';
	     	    	transportationId=0;
	     	    	vehicalNumber=null;
	     	   	    docketNumber=null;
	     	   	    
					$('#transportationModal').modal('close');
				}
					
			
			});
		//on issued button click this button going to disabled
		//request for issue order with transportation,delivery boy and delivery date 
		//delivery date must be after current date
    	$('#issueConfirmButtonId').click(function(){
    		
    		var deliveryBoyId=$('#deliveryBoyId').val();
    		var deliveryDateId=$('#deliveryDateId').val();		
    		
    		 if(deliveryBoyId==="0")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Boy', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy</font>"); */
	   		    return false;
    		}
    		else if(deliveryDateId==="")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date</font>"); */
	   		    return false;
    		}
    		else if(deliveryBoyId==="0" && deliveryDateId==="")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Boy And Delivery Date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy And Delivery Date</font>"); */
	   		    return false;
    		}
    		
    		
    		var deliveryDate=new Date(deliveryDateId).setHours(0,0,0,0);
    		var today=new Date().setHours(0,0,0,0);    
    		if(deliveryDate < today)
			{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Date in Current Date or After that date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date in Current Date or After that date</font>"); */
				return false;
			}    	
    		
    		 var data = {
     	    		'deliveryBoyId': deliveryBoyId,
     	    		'deliveryDate': deliveryDateId,
     	    		'isTransportationHave':isTransportationHave,
     	    		'transportationId':transportationId,
    				'vehicalNumber':vehicalNumber,
    				'docketNumber':docketNumber
     	    	};
     	    $("#issueConfirmButtonId").attr('disabled','disabled')
     		$.ajax({
         		type:"POST",
     			url : "${pageContext.request.contextPath}/packedOrderToDeliverBoyForWeb",
         		dataType : "json",
         		data : data,
         		beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
         		success : function(data) {
         			if(data.status==="Success")
         			{
						Materialize.Toast.removeAll();
         				Materialize.toast('Order SuccessFully Issued', 2000, 'teal lighten-2');
         				/* $('#addeditmsg').modal('open');
        	   		    $('#msgHead').text("Message");
        	   		    $('#msg').html("<font color='green'>Order SuccessFully Issued</font>");
        	   		 	*/
	        	   		setTimeout(function() 
	        	   		{
	     	   		    	$('#addeditmsg').modal('close');
	     	   		        window.location.href="${pageContext.request.contextPath}/openIssuedBill?orderId="+data.errorMsg;
	     	   		    }, 2000); 
         			}
         			else
         			{
         				 $('#addeditmsg').find("#modalType").addClass("warning");
         				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
         				$('#addeditmsg').modal('open');
        	   		    /* $('#msgHead').text("Error Message"); */
        	   		    $('#msg').html("<font color='red'>"+data.status+"</font>");
        	   		 	$("#issueConfirmButtonId").removeAttr("disabled");
         			}
         			$('.preloader-background').hide();
					$('.preloader-wrapper').hide();
         		},error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					Materialize.Toast.removeAll();
					Materialize.toast('Order Issuing Failed', 2000, 'teal lighten-2');
					
				}		
     		});
     	    
     	    
    	});
		/*tranportation submit : 
			validate fields
			store transportation details on globle variable
			*/ 
    	$('#transportationSubmitId').click(function(){
 	    	
 	    	isTransportationHave='No';
 	    	transportationId=0;
 	    	vehicalNumber=null;
 	   	    docketNumber=null;
 	    	
 	    	var transportValue = $('#transport_select').val();
 	    	if(transportValue==0){
				Materialize.Toast.removeAll();
 	    		Materialize.toast('Select Transportation', 2000, 'teal lighten-2');
 	    		return false;
 	    	}
 	    	
 	    	var vehicalNo=$('#transVehicleNo').val();
 	    	if(vehicalNo=='' || vehicalNo==undefined){
				Materialize.Toast.removeAll();
 	    		Materialize.toast('Enter Vehical Number', 2000, 'teal lighten-2');
 	    		return false;
 	    	}
 	    	
 	    	var transDocketNo=$('#transDocketNo').val();
 	    	if(transDocketNo=='' || transDocketNo ==undefined){
 	    		docketNumber=null;
 	    	}else{
 	    		docketNumber=transDocketNo;
 	    	}
 	    	var info = transportValue.split("~");
 	    	
 	    	isTransportationHave='Yes';
 	    	transportationId=info[4];
 	    	vehicalNumber=vehicalNo;
 	    	
 	    	$('#transport_details').prop("checked",true);  
 	    	$('#transportationModal').modal('close');
 	    });
		/* on transaction change set gst number and vehical number */
    	$('#transport_select').change(function(){
    		var transportValue = $('#transport_select').val();
    		var info = transportValue.split("~");
    		$('#transMobNo').prop('readonly',false);
    		$('#transGstNo').prop('readonly',false);
    		//$('#transVehicleNo').prop('readonly',false);
    		$('#transportName').val(info[0]);
    		$('#transMobNo').val(info[1]);
    		$('#transGstNo').val(info[2]);
    		$('#transVehicleNo').val(info[3]);
    		$('#transMobNo').prop('readonly',true);
    		$('#transGstNo').prop('readonly',true);
    		//$('#transVehicleNo').prop('readonly',true);
    		
    	});
    });
    </script>
    <style>
     .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
        <div class="col s12">
        <div class="row">
        	
        
        
        
            <%-- <div class="col s12 l6 m6">
                <div class="card grey lighten-3 hoverable z-depth-3">
                    <div class="card-content blue-text">
                        <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${orderDetailResponse.orderId}" /></b> </p>
                        <hr style="border:1px dashed teal; ">
                        <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${orderDetailResponse.shopName}" /></b></p>
                        <hr style="border:1px dashed teal;"> 
                        <p id="area" class="center-align blue-text">Area: <b><c:out value="${orderDetailResponse.areaName}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${orderDetailResponse.mobileNumber}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Salesman Name: <b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="Add" class="center-align blue-text">Date of Order: <b><c:out value="${orderDetailResponse.orderDetailsAddedDatetime}" /></b></p>
                    </div>
                </div>
            </div> --%>
            
            
            
             <div class="col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
      	
       			<div class="col s12 l2 m2 right right-align" style="padding:0">
                <a href="${pageContext.request.contextPath}/editOrder?orderId=${orderDetailResponse.orderId}" id="editOrderId" class="btn waves-effect waves-light blue-gradient">Edit Order</a>
            </div>
                <div class="col s12 l5 m5">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${orderDetailResponse.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                     <div class="col s12 l5 m5">
                       <p id="Add"><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <c:out value="${orderDetailResponse.orderDetailsAddedDatetime}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l5 m5">
                   <p id="department"><span class="leftHeader">Shop Name: </span><b><c:out value="${orderDetailResponse.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
               		  
                </div>
                 <div class="col s12 l5 m5">
                   <p><span class="leftHeader">Salesman: </span><b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l5 m5">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${orderDetailResponse.mobileNumber}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
                <div class="col s12 l5 m5">
                   <div class="col s12 m2 l2 leftHeader" style="padding:0"><p id="area"><span>Area:</span></p></div>
                 <div class="col s12 m8 l8" style="word-wrap:break-word;padding:0;"><p><b><c:out value="${orderDetailResponse.areaName}" /></b></p></div>
                
                
                  <%--  <p id="area"><span class="leftHeader">Area:</span> <b><c:out value="${orderDetailResponse.areaName}" /></b></p> --%>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
           		
           </div>
         	<div class="col l3 m6 s12" style="padding-top: 2%;">
							<input type="checkbox" value="1" name="transport"
								id="transport_details" class="issue_check" /> <label
								for="transport_details">Transportation Details</label>
		</div>
			<div class=" col s12 l2 m2 ">
                <select id="deliveryBoyId">
			      <option value="0"  selected>Delivery Person</option>
			      	  <c:if test="${not empty orderDetailResponse.employeeNameAndIdSMAndDBList}">
							<c:forEach var="listValue" items="${orderDetailResponse.employeeNameAndIdSMAndDBList}">
								<option value="<c:out value="${listValue.employeeId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>
            <div class="col s12 l2 m2">
                <input type="text" class="datepicker disableDate" placeholder="Choose Date" id="deliveryDateId" id="Date">
            </div>
            <div class="col s12 l2 m12 center" style="margin-top:1%">
                <button type="button" id="issueConfirmButtonId" class="btn waves-effect waves-light blue-gradient">Confirm</button>
            </div>
            
        </div>
        <div class="row">
            <div class="col s12 l12 m12" style="padding:0">
                <table class="striped highlight tblborder centered" id="tblData1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">Current Qty</th>
                            <th class="print-col">Ordered Qty</th>
                            <th class="print-col">Issue Qty</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty orderDetailResponse.orderProductDetailList}">
						<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td class="wrapok"><c:out value="${listValue.product.product.productName}" />
                            	<font color="green"><c:out value="${listValue.type=='Free'?'-(Free)' : ''}" /></font>
                            </td>
                            <td>
                            <span id="currrentQuantity_${listValue.type=='Free'?'Free' : 'NonFree'}_${listValue.orderProductDetailsid}"><c:out value="${listValue.product.product.currentQuantity}" /></span>
                            </td>
                            <td>
                            <span id="orderdQuantity${listValue.orderProductDetailsid}"><c:out value="${listValue.purchaseQuantity}" /></span>
                            <c:set var="totalOrderQuantity" value="${totalOrderQuantity + listValue.purchaseQuantity}" scope="page"/>
                            </td>
                            <td>
	                            <input style="text-align:center;" type="text" value="<c:out value="${listValue.purchaseQuantity}" />" id="issuedQuantity${listValue.orderProductDetailsid}">
	                         
	                            <c:set var="totalIssuedQuantity" value="${totalIssuedQuantity + listValue.purchaseQuantity}" scope="page"/>
                            </td>
                           <td>
	                            <span id="amountWithTax${listValue.orderProductDetailsid}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.purchaseQuantity * listValue.sellingRate}" /></span>
	                            <c:set var="totalOrderAmountWithTax" value="${totalOrderAmountWithTax + (listValue.purchaseQuantity * listValue.sellingRate)}" scope="page"/>
                           </td>
                        </tr>
                        </c:forEach>
                        </c:if>                        
                    </tbody>
                    <tbody>
                   		<tr>
                            <td colspan="3"><b>Total</b></td>
                            <td><span id="totalOrderQuantity"><c:out value="${totalOrderQuantity}" /></span></td>
                            <td><span id="totalIssuedQuantity"><c:out value="${totalIssuedQuantity}" /></span></td>
                            <td><span id="totalOrderAmountWithTax"><c:out value="${totalOrderAmountWithTax}" /></span></td>
                        </tr>
                    </tbody>
                   </table>
            </div>
        </div>
      </div>
      </div>
        <div id="transportationModal" class="modal" style="width:35%">
		<div class="modal-content row">
			<h4 class="center-align">Transportation Details</h4>
			<div class="divider"></div>
			<form name="updateTransport" method="post">
				
				<div class="input-field col l6 m12 s12">
					<select id="transport_select" class="select" required>
						<option selected value="0">Select Transport</option>
						<c:forEach var="transportation" items="${orderDetailResponse.transportations}" varStatus="status">
							<option
								value="${transportation.transportName}~${transportation.mobNo}~${transportation.gstNo}~${transportation.vehicleNo}~${transportation.id}">${transportation.transportName}</option>
						</c:forEach>
					</select>
				</div>

				<div class="input-field col l6 m12 s12">
					<label for="transGstNo">GST No.</label> <input placeholder="GST No"
						type="text" id="transGstNo" name="gstNo" value="" class="grey lighten-3" readonly>
				</div>

				<div class="input-field col l6 m12 s12">
					<label for="transVehicleNo">Vehicle No.</label> 
					<input
						placeholder="Vehicle No" type="text" id="transVehicleNo"
						name="vehicleNo" value="">
				</div>

				<div class="input-field col l6 m12 s12">
					<label for="transDocketNo">Docket No.</label> <input
						placeholder="Docket No" type="text" id="transDocketNo"
						name="docketNo">
				</div>
				<input type="hidden" id="transMobNo" name="mobNo"> 
				<input
					type="hidden" id="transportName" name="transportName">
				<div class="input-field col l12 m3 s3 center">
				<a href="#!"
				class="modal-action waves-effect waves-green modal-close btn red" style="margin-left:5%;">close</a>
					<button type="button" value="SUBMIT" id="transportationSubmitId"
						class="waves-effect waves-light btn testButton btnEffect" style="width: 115px;">
						Submit <i class="material-icons tiny right btnHover">navigate_next</i>
					</button>
					
					
				</div>	
				
			</form>
		</div>
		<!--/*<div class="divider"></div>
		<div class="modal-footer">
			<a href="#!"
				class="modal-action waves-effect waves-green modal-close btn red">close</a>
		</div>*/-->
	</div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
        
      
    </main>
    <!--content end-->
</body>

</html>