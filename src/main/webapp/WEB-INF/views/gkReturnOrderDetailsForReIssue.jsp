<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
    $(document).ready(function() {
    	
    	<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
    	/* issued-confrm quantiy is non zero then show product with return quantity */
    	 <c:if test="${(listValue.issuedQuantity-listValue.confirmQuantity)>0}">
    	
    	 //reissue quantity allowed only number
    	 $('#reIssuedQuantity${listValue.orderProductDetailsid}').keypress(function( event ){
    	    var key = event.which; 
    	    
    	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
    	        event.preventDefault();
    	}); 
    	/* 
    	on change reissue quantity
    	reissue quantity must be not greater then return quantity
    	reissue quantity must be equal or less than current quantity
    	*/
    	$('#reIssuedQuantity${listValue.orderProductDetailsid}').keyup(function(){
    		
    		var reIssuedQuantity=$('#reIssuedQuantity${listValue.orderProductDetailsid}').val();
    		//alert(issuedQuantity);
    		if(reIssuedQuantity==="" || reIssuedQuantity===undefined)
    		{
    			reIssuedQuantity=0;
    		}
    		
    		if(parseInt(reIssuedQuantity) > parseInt($('#returnQuantity${listValue.orderProductDetailsid}').text()))
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('${listValue.product.productName} Product ReIssued Quantity exceed Return Quantity', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').find("#modalType").addClass("warning");
				$('#addeditmsg').find(".modal-action").addClass("red lighten-2");
    			 $('#addeditmsg').modal('open');
	   		      $('#msgHead').text("Warning Message"); 
	   		     $('#msg').html("<font><b>${listValue.product.productName}</b> Product <b>ReIssued Quantity</b> exceed <b>Return Quantity</b></font>");
    		    setTimeout(function() {
    		    	$('#addeditmsg').modal('close');
    		    }, 2000); */
    		    $('#reIssuedQuantity${listValue.orderProductDetailsid}').val("0");
    		   // return false;
    		}
    		
    		if(parseInt(reIssuedQuantity)>parseInt($('#currrentQuantity${listValue.orderProductDetailsid}').text()))
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('${listValue.product.productName} Product ReIssued Quantity exceed Current Quantity', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		     $('#msgHead').text("Warning Message");
	   		     $('#msg').html("<font color='red'><b>${listValue.product.productName}</b> Product <b>ReIssued Quantity</b> exceed <b>Current Quantity</b></font>");
	   		    setTimeout(function() {
	   		    	$('#addeditmsg').modal('close');
	   		    }, 2000); */
	   		    $('#reIssuedQuantity${listValue.orderProductDetailsid}').val("0");
	   		    //return false;
    		}
			
			var reIssuedQuantity=$('#reIssuedQuantity${listValue.orderProductDetailsid}').val();
    		//alert(issuedQuantity);
    		if(reIssuedQuantity==="" || reIssuedQuantity===undefined)
    		{
    			reIssuedQuantity=0;
    		}
			
    		//update reissue quantity in session order product list
    	    var data = {
    	    		'reIssuedQuantity': reIssuedQuantity,
    	    		'productId': "${listValue.product.productId}"
    	    	};
    	    
    		$.ajax({
        		type:"POST",
    			url : "${pageContext.request.contextPath}/reIssueProductDetailsCalculate",
        		dataType : "json",
        		data : data,
        		success : function(data) {
        			reIssueOrderProductDetailsListNew=data;
        			var totalOrderQuantity=0;
        			var totalReIssuedQuantity=0;
        			var totalAmount=0;
        			
        			for(var i=0; i<reIssueOrderProductDetailsListNew.length; i++)
        			{
        				if(reIssueOrderProductDetailsListNew[i].product.productId=="${listValue.product.productId}")
        				{
        					$('#amountWithTax${listValue.orderProductDetailsid}').text(parseFloat(reIssueOrderProductDetailsListNew[i].reIssueAmountWithTax).toFixedVSS(2));
        				}
        				
        				totalReIssuedQuantity=totalReIssuedQuantity+reIssueOrderProductDetailsListNew[i].reIssueQuantity;
        				totalAmount=totalAmount+reIssueOrderProductDetailsListNew[i].reIssueAmountWithTax;
        			}
        			
        			$('#totalReIssuedQuantity').text(parseInt(totalReIssuedQuantity));
        			$('#totalOrderAmountWithTax').text(parseFloat(totalAmount).toFixedVSS(2)); 
        			
        		}
    		});
    	});
    	</c:if>
    	</c:forEach>
    	/*
    	on confirm button
    	deleivery date and delivery boy selection validation
    	reissue order request
    	*/
    	$('#reIssuedQuantityConfirmButtonId').click(function(){
    		
    		var deliveryBoyId=$('#deliveryBoyId').val();
    		var deliveryDateId=$('#deliveryDateId').val();		
    		/* var transportId=$("#transport_select").val();
    		var gstNo=$("#transGstNo").val();
    		var vehicleNo=$("#transVehicleNo").val();
    		var docektNo=$("#transDocketNo").val(); */
    		/* if(transportId==="" || transportId===undefined)
    		{
    			Materialize.toast('Select Transport', 2000, 'teal lighten-2');
	   		    return false;
    		}
    		else if(vehicleNo==="" || vehicleNo==undefined)
    		{
    			Materialize.toast('Vehicle no is required', 2000, 'teal lighten-2');
	   		    return false;
    		}
    		else if(docektNo==="" || docektNo==undefined)
    		{
    			Materialize.toast('Docket no is required', 2000, 'teal lighten-2');
	   		    return false;
    		} */
    		 if(deliveryBoyId==="0" && deliveryDateId==="")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Boy And Delivery Date!', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy And Delivery Date</font>"); */
	   		    return false;
    		}
    		else if(deliveryBoyId==="0")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Boy!', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy</font>"); */
	   		    return false;
    		}
    		else if(deliveryDateId==="")
    		{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Date!', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date</font>"); */
	   		    return false;
    		}
    		
    		var deliveryDate=new Date(deliveryDateId).setHours(0,0,0,0);
    		var today=new Date().setHours(0,0,0,0);    
    		if(deliveryDate < today)
			{
				Materialize.Toast.removeAll();
    			Materialize.toast('Select Delivery Date in Current Date or After that date!', '4000', 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date in Current Date or After that date</font>"); */
				return false;
			}    	
    		
    		 var data = {
     	    		'deliveryBoyId': deliveryBoyId,
     	    		'deliveryDate': deliveryDateId
     	    	};
    		 $("#reIssuedQuantityConfirmButtonId").attr('disabled','disabled');
     		$.ajax({
         		type:"POST",
     			url : "${pageContext.request.contextPath}/reIssueForWeb",
         		dataType : "json",
         		data : data,
         		beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		        },
         		success : function(data) {
         			if(data.status==="Success")
         			{
						Materialize.Toast.removeAll();
         				Materialize.toast('Order SuccessFully ReIssued!', '4000', 'teal lighten-2');
         				window.location.href="${pageContext.request.contextPath}/fetchReturnOrderReportForWeb";
         				
         				/* $('#addeditmsg').modal('open');
        	   		    $('#msgHead').text("Message");
        	   		    $('#msg').html("<font color='green'>Order SuccessFully ReIssued</font>");
        	   		    
	        	   		setTimeout(function() 
	        	   		{
	     	   		    	$('#addeditmsg').modal('close');
	     	   		        window.location.href="${pageContext.request.contextPath}/fetchReturnOrderReportForWeb";
	     	   		    }, 2000);  */       	   		    
         				
         			}
         			else
         			{
         				$('#addeditmsg').find("#modalType").addClass("warning");
        				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
         				$('#addeditmsg').modal('open');
        	   		    /* $('#msgHead').text("Error Message"); */
        	   		    $('#msg').html("<font color='red'>"+data.status+"</font>");
        	   		    
        	   		    $("#reIssuedQuantityConfirmButtonId").removeAttr("disabled");
         			} 
         			
         			$('.preloader-background').hide();
					$('.preloader-wrapper').hide();
         		},error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					Materialize.Toast.removeAll();
					Materialize.toast('Order ReIssuing Failed', 2000, 'teal lighten-2');
					
				}	
     		});
    	});
    	
    });
    </script>
    <style>
    	 /* .card-panel p{
        	font-size:15px !important;
        } */
               .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    tfoot th,
    tfoot td    {
    	text-align:center;
    }
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
	}
	@media  only screen and (min-width:992px){
		.deliverDateDiv{
		width:16%
	}
	}
	
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>     
        
        
        
        
        <div class="row">
			<div class="col s12">
        <div class="col  s12 l12 m12 card-panel hoverable blue-grey lighten-4">
      	
       			
                <div class="col s12 l6 m6">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${orderDetailResponse.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                 <div class="col s12 l6 m6">
                   <p id="department"><span class="leftHeader">Shop Name: </span>
                   <b><c:out value="${orderDetailResponse.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${orderDetailResponse.mobileNumber}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                     <div class="col s12 l6 m6">
                       <p id="Add"><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${orderDetailResponse.orderDetailsAddedDatetime}"  />
				       	<c:out value="${date}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
             	<div class="col s12 l6 m6 ">
                    <p><span class="leftHeader">Salesman: </span><b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6">
                	<div class="col s12 m2 l2" style="padding:0;width:105px"><p id="area"><span>Area:</span></p></div>
                 <div class="col s12 m8 l8" style="word-wrap:break-word;"><p><b><c:out value="${orderDetailResponse.areaName}" /></b></p></div>
                
                  <%--  <p id="area"><span class="leftHeader">Area:</span> <b><c:out value="${orderDetailResponse.areaName}" /></b></p> --%>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
               
                
           
               <%--  <div class="col s12 l6 m6 ">
                       <p id="Add" class=" blue-text">Date of Order: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div> --%>
                
           
           </div> 
        </div>
        
           <%--  <div class="col s12 l6 m6 card-panel hoverable">
               			<div class="col s12 m6 l6">
                        <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${orderDetailResponse.orderId}" /></b> </p>
                        </div>
                        <div class="col s12 m6 l6">
                        <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${orderDetailResponse.shopName}" /></b></p>
                        </div>
                        <div class="col s12 m6 l6">
                        <p id="area" class="center-align blue-text">Area: <b><c:out value="${orderDetailResponse.areaName}" /></b></p>
                        </div>
                       	 <div class="col s12 m6 l6">
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${orderDetailResponse.mobileNumber}" /></b></p>
                        </div>
                          <div class="col s12 m6 l6">
                        <p id="salesmanName" class="center-align blue-text">Salesman Name: <b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
                        </div>
                        
						  <div class="col s12 m6 l6">
                         <p id="Add" class="center-align blue-text">Date of Order: <b>
                         <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${orderDetailResponse.orderDetailsAddedDatetime}"  />
				       	<c:out value="${date}" />
                        </b></p>
                        </div>
                                                
                       
                    
                       
                    </div> --%>
              	<%-- <div class="input-field col l2 m2 s12">
									<select id="transport_select" class="select" required>
										<option val="" selected>Select Transport</option>
										<c:forEach var="transportation" items="${transportations}"
											varStatus="status">
											<option
												value="${transportation.transportName}~${transportation.mobNo}~${transportation.gstNo}~${transportation.vehicleNo}">${transportation.transportName}</option>
										</c:forEach>
									</select>
								</div> 
								<div class="input-field col l2 m2 s12">
									<label for="transGstNo">GST No.</label> 
									<input placeholder="GST No" type="text" id="transGstNo"
										name="gstNo" value="" class="grey lighten-3" readonly>
								</div>
           
						<div class="input-field col l2 m12 s12">
									<label for="transVehicleNo">Vehicle No.</label> <input
										placeholder="Vehicle No" type="text" id="transVehicleNo"
										name="vehicleNo" value="" >
								</div>

								<div class="input-field col l2 m12 s12">
									<label for="transDocketNo">Docket No.</label> <input
										placeholder="Docket No" type="text" id="transDocketNo"
										name="docketNo">
								</div>
								<input type="hidden" id="transMobNo" name="mobNo"> 
								<input type="hidden" id="transportName" name="transportName">  --%> 
            <div class="input-field col s8 l2 m3 offset-l3">
                <select id="deliveryBoyId">
			      <option value="0"  selected>Delivery Person</option>
			      	  <c:if test="${not empty orderDetailResponse.employeeNameAndIdSMAndDBList}">
							<c:forEach var="listValue" items="${orderDetailResponse.employeeNameAndIdSMAndDBList}">
								<option value="<c:out value="${listValue.employeeId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>

            <div class="input-field col s4 l2 m2 deliverDateDiv">
                <input type="date" class="datepicker disableDate" placeholder="Choose Date" id="deliveryDateId" id="Date">

            </div>
            <div class="input-field col s12 l2 m3 center">
                <button type="button" id="reIssuedQuantityConfirmButtonId" class="btn waves-effect waves-light actionBtn">Reissue</button>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12 m12">
				<div class="scrollDivTable">
                <table class="striped highlight tblborder centered" id="tblData1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">Current Quantity</th>
                            <th class="print-col">Return Quantity</th>
                            <th class="print-col">Replace Quantity</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty orderDetailResponse.orderProductDetailList}">
						<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
						  <c:if test="${(listValue.issuedQuantity-listValue.confirmQuantity)>0}">
	                        <c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
	                        <tr>
	                            <td><c:out value="${rowincrement}" /></td>
	                            <td><c:out value="${listValue.product.productName}" /><font color="green"><b>${listValue.type=='Free'?'-Free':''}</b></font></td>
	                            <td>
	                            <span id="currrentQuantity${listValue.orderProductDetailsid}"><c:out value="${listValue.product.currentQuantity}" /></span>
	                            </td>
	                            <td>
	                            <span id="returnQuantity${listValue.orderProductDetailsid}"><c:out value="${listValue.issuedQuantity-listValue.confirmQuantity}" /></span>
	                            <c:set var="totalOrderQuantity" value="${totalOrderQuantity + (listValue.issuedQuantity-listValue.confirmQuantity)}" scope="page"/>
	                            </td>
	                            <td>
	                            <input style="text-align:center;" type="text" value="<c:out value="${listValue.issuedQuantity-listValue.confirmQuantity}" />" id="reIssuedQuantity${listValue.orderProductDetailsid}">
	                            <c:set var="totalReIssuedQuantity" value="${totalReIssuedQuantity + (listValue.issuedQuantity-listValue.confirmQuantity)}" scope="page"/>
	                            </td>
	                            <td>
		                            <span id="amountWithTax${listValue.orderProductDetailsid}">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(listValue.issuedQuantity-listValue.confirmQuantity)*listValue.sellingRate}" />
									</span>
		                            <c:set var="totalOrderAmountWithTax" value="${totalOrderAmountWithTax + ((listValue.issuedQuantity-listValue.confirmQuantity)*listValue.sellingRate)}" scope="page"/>
	                            </td>
	                        </tr>
	                        </c:if>
                        </c:forEach>
                        </c:if>                        
                    </tbody>
                    <tbody>
                   		<tr>
                            <td colspan="3"><b>Total</b></td>
                            <td><span id="totalOrderQuantity"><c:out value="${totalOrderQuantity}" /></span></td>
                            <td><span id="totalReIssuedQuantity"><c:out value="${totalReIssuedQuantity}" /></span></td>
                            <td><span id="totalOrderAmountWithTax"><c:out value="${totalOrderAmountWithTax}" /></span></td>
                        </tr>
                    </tbody>
				   </table>
				</div>
            </div>
        </div>
        
        <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
     
    </main>
    <!--content end-->
</body>

</html>