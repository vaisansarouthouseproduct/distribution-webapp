<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>


<script  type="text/javascript" src="resources/js/commissionAssign.js"></script>
<script type="text/javascript">
var mode="${mode}";
var myContextPath = "${pageContext.request.contextPath}";
var commissionSlabArr=[];
var commissionAssignId="${commissionAssignId}";
$(document).ready(function() {

});

</script>

<style>
table.dataTable tbody td {
    padding: 0 2px !important;
}
	.dataTables_wrapper {
 
    margin-left: 2px !important;
}

/* #sendMsg{
	width:35%;
	border-radius: 20px;
} */
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 10px;
    border-bottom: 1px solid #111;
}
table input[type=text]:not(.browser-default ){
	height: 3rem !important;
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        <div class="row z-depth-3">
			<div class="col s12 l10 m10 offset-l1 offset-m1">
        <div class="row">
         			 <div class="col l12 m12 s12">
                        <h5 class="center"><b>Commission Details</b></h5>
                     </div>
					 <div class="input-field col s12 m6 l4 forProductChange">
                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>                         
                        <select id="departmentId" name="departmentId">
                                 <option value="0">Department</option>
                                <c:if test="${not empty departmentList}">
									<c:forEach var="listValue" items="${departmentList}">
										<option value="<c:out value="${listValue.departmentId}" />"  ><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                     </div>	
                     <div class="input-field col s12 m6 l4 ">
                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>                         
                        <select id="targetTypeId" name="targetTypeId">
                                 <option value="0">Target Type</option>
                        </select>
                     </div>	
                     <div class="input-field col s12 m6 l4 productDiv">
                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>                         
                        <select id="productId" name="productId" multiple>
                                 <option value="0" class="productId1" >All</option>
                                  <c:if test="${not empty productList}">
									<c:forEach var="listValue" items="${productList}">
										<option value="<c:out value="${listValue.productId}" />"><c:out
												value="${listValue.productName}" /></option>
									</c:forEach>
								  </c:if>
                        </select>
                     </div>	
                  </div>
                     <div class="row">
                     <div class="input-field col s12 m6 l4 forProductChange">
		          		<i class="material-icons prefix">access_time <span class="red-text">*</span></i>
		               	<select name="targetPeriodId" id="targetPeriodId" >
		                       <option value="0">Target Period</option>
		                       <option value="Daily">Daily</option>
		                       <option value="Weekly">Weekly</option>
		                       <option value="Monthly">Monthly</option>
		                       <option value="Yearly">Yearly</option>
		               </select>
		          	</div>
                     <div class="input-field col s12 m6 l4 ">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="commissionName" type="text" name="rate">
                        <label for="commissionName" class="active">Name</label>
                    </div> 
                    </div>
                    <div class="row">
	                     <div class="col s12 m6 l4 forProductChange">
			          		<input id="includeTax" type="checkbox" /> 
			          		<label for="includeTax">Include Tax</label>
			          	</div>
	                     <div class="col s12 m6 l4 ">
	                        <input id="cascadComm" type="checkbox" /> 
			          		<label for="cascadComm">Cascading Commission</label>
	                    </div>
	                    <div class="col s12 m3 l2 forProductChangeHide">
	                        <input id="amtPrdct" type="radio" name="productCommType" /> 
			          		<label for="amtPrdct">Amount</label>
	                    </div>
	                    <div class="col s12 m3 l2 forProductChangeHide">
	                        <input id="qtyPrdct" type="radio" checked name="productCommType" /> 
			          		<label for="qtyPrdct">Quantity</label>
	                    </div> 
                    </div>
                    </div>
                </div>    
        		<div class="row z-depth-3">
                    <div class="row">
					 <div class="col l12 m12 s12">
						 <br>
                        <h5 class="center"><b>Commission Slab</b></h5>                       
                     </div>
                     	 <div class="input-field col s12 m4 l3 offset-l1 left-align">  
                     	 	<span class="selectLabel">Commission Value Type</span>                        
	                        <select id="commissionValueTypeMain">
	                                 <option value="percentage">Percentage</option>
	                                 <option value="value">Value</option>
	                        </select>
	                    </div>  
                    </div>
                    <div id="addCommSlab">
	                    <!-- <div class="row">
	                    	<div class="input-field col s1 m1 l1 push-l1">
		                        <span style="font-size:34px;">1.</span>
		                    </div>
	                    	<div class="input-field col s4 m4 l2 push-l1">
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="from0" type="text">
		                        <label for="from0" class="active">From</label>
		                    </div>
		                    <div class="input-field col s4 m4 l2  push-l1"> 
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="to0" type="text">
		                        <label for="to0" class="active">To</label>
		                    </div>
		                    <div class="input-field col s4 m4 l3  push-l1">  
		                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>                         
		                        <select id="commissionValueType0">
		                                 <option value="0">Value Type</option>
		                                 <option value="percentage">Percentage</option>
		                                 <option value="value">Value</option>
		                        </select>
		                    </div>  
		                    <div class="input-field col s4 m4 l2  push-l1"> 
		                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
		                        <input id="value0" type="text">
		                        <label for="value0" class="active">Value</label>
		                    </div>
	                   </div> -->
                   </div>
                    <div class="row">
	                    <div class="input-field col s12 m12 l12 center"> 
	                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="addNewSlabId" ><i class="material-icons left">add</i>Add new slab</button>
	                    </div>
                    </div>
			 <div class="row">
                   <div class="col s12 m12 l12 modal-footer" >
                   		<center>
							<a href="#!" id="commissionAssignIdSubmit" class="waves-effect btn">Submit</a>
						
						</center>
					</div>
			</div>
			</div>
		  </div>
    </main>
    <!--content end-->
</body>

</html>