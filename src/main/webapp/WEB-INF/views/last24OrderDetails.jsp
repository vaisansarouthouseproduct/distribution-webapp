<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
        <%@include file="components/header_imports.jsp" %>
        

    <script type="text/javascript">
    
    function showOrderDetails(id)
	   {
		   $.ajax({
				url : "${pageContext.servletContext.contextPath}/fetchProductBySupplierOrderId?supplierOrderId="+id,
				dataType : "json",
				success : function(data) {
				//alert(data);
				var totalAll=0,totalAmtWithTaxAll=0;
				    $("#productOrderedDATA").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var supplierOrderDetail = data[i];
						
						/* 
						<tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                            <th>Total Amount</th>
                            <th>Total Amount With Tax</th>
                            </tr>
						*/
						var igst=supplierOrderDetail.product.categories.igst;
						var cgst=supplierOrderDetail.product.categories.cgst;
						var sgst=supplierOrderDetail.product.categories.sgst;
						
						var total=parseFloat(supplierOrderDetail.quantity)*parseFloat(supplierOrderDetail.supplierRate);
						var totalAmtWithTax=parseFloat(total)+
												( (parseFloat(total)*parseFloat(igst))/100 )+
												( (parseFloat(total)*parseFloat(cgst))/100 )+
												( (parseFloat(total)*parseFloat(sgst))/100 );
						$("#productOrderedDATA").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                           "<td>"+supplierOrderDetail.product.productName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.categories.categoryName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.brand.name+"</td>"+
	                           "<td>"+supplierOrderDetail.quantity+"</td>"+
	                           "<td>"+supplierOrderDetail.supplierRate+"</td>"+
	                           "<td>"+total+"</td>"+
	                           "<td>"+totalAmtWithTax+"</td>"+
	                       "</tr>"); 
						srno++;
						totalAmtWithTaxAll=parseFloat(totalAmtWithTaxAll)+totalAmtWithTax;
						totalAll=parseFloat(totalAll)+total;
					}
					
					$("#productOrderedDATA").append("<tr>"+	     
													   "<td colspan='6'><b>All Total</b></td>"+
							                           "<td  class='red-text'><b>"+totalAll+"</b></td>"+
							                           "<td  class='red-text'><b>"+totalAmtWithTaxAll+"</b></td>"+
							                       "</tr>");
					
					$('.modal').modal();
					$('#product').modal('open');
					
				},
				error: function(xhr, status, error) {
					  	alert("error");
					}
			});
	   }
    
    </script>
    
    <style>
    
         .card-panel p{
        	font-size:15px !important;
        }
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       
        <br>
         <div class="row">
         <form action="${pageContext.servletContext.contextPath}/fetchLast24HoursOrders" method="post">
         <input type="hidden" name="fetchRecordType" value="${fetchRecordType}">
         <input type="hidden" name="fetchBy" value="supplier">
            <div class="col s12 l2 m2">
                <select id="supplierId" name="supplierId">
			      <option value="0"  selected>Choose Supplier</option>   
			       <c:if test="${not empty supplierList}">
							<c:forEach var="listValue" items="${supplierList}">
								<option value="<c:out value="${listValue.supplierId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					</c:if>   
			    </select>
            </div>
            <div class="col s12 l2 m2">
                <button class="btn waves-effect waves-light blue darken-8" style="margin-top:7%">Search</button>
            </div>
         </form>
             <div class="col s12 m4 l4" style="margin-top:1%;">
                <div class="col s6 m12 l12 ">
                    <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLast24HoursOrders?range=last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLast24HoursOrders?range=last1month">Last 1 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLast24HoursOrders?range=all">View All</a></li>
                    </ul>
                </div>
 
                <form action="${pageContext.servletContext.contextPath}/fetchLast24HoursOrders"  method="post">
                    <input type="hidden" name="range" value="range"> 
                    <input type="hidden" name="fetchRecordType" value="${fetchRecordType}">
                    <span class="showDates">
                              <div class="input-field col s6 m3 l4">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>

                              <div class="input-field col s6 m3 l4">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required>
                                     <label for="endDate">To</label>
                               </div>
                            <button type="submit" class="btn">View</button>
                          </span>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Order Details</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Total Amount With Tax</th>
                            <th class="print-col">Order Date</th>
                            <th>Edit Order</th>
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                   <c:if test="${not empty last24Order}">
					<c:forEach var="listValue" items="${last24Order}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.supplierOrderId}" /></td>
                            <td><button onclick="showOrderDetails('${listValue.supplierOrderId}')" class="btn blue-gradient">View</button></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax}" /></td>
                            <td>
                            	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.supplierOrderDatetime}"  />
		                        <c:out value="${date}" />
		                    </td>
                            <td><a class="btn blue-gradient"	href="${pageContext.servletContext.contextPath}/editOrder?supplierOrderId=${listValue.supplierOrderId}">Edit</a></td>
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal Structure for View Product Details -->
        <div id="product" class="modal ">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>
                <hr>
                <br>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Total Amount</th>
                            <th>Total Amount With Tax</th>
                        </tr>
                    </thead>
                    <tbody id="productOrderedDATA">
                        <!-- <tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
                </div>

            </div>
        </div>




    </main>
    <!--content end-->
</body>

</html>