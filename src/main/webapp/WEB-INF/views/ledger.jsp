<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>


<script type="text/javascript">

$(document).ready(function() 
{
	var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	     /*  initComplete: function () {
	    	.on('change', function () {
          var val = $.fn.dataTable.util.escapeRegex(
          $(this).val());
	      } */
	      	autoWidth: false,
	         columnDefs: [
	                      { 'width': '1%', 'targets': 0 },
	                      { 'width': '1%', 'targets': 1},
	                      { 'width': '8%', 'targets': 2},
	                  	  { 'width': '2%', 'targets': 3},
	                  	{ 'width': '3%', 'targets': 4},
	                	  { 'width': '3%', 'targets': 5},
	                	{ 'width': '2%', 'targets': 6},
	                	{ 'width': '2%', 'targets': 7}
	                	  
	             		  	                     
	                      ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	         
	         //dom: 'lBfrtip',
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [30,60,'*',60,60,70,70,50] 
	                    		 } 
	                    		    })	 
	                     
	                         /* var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         } */
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];   	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select").change(function() {
           var t = this;
           var content = $(this).siblings('ul').detach();
           setTimeout(function() {
               $(t).parent().append(content);
               $("select").material_select();
           }, 200);
       });
   $('select').material_select();
   $('.dataTables_filter input').attr("placeholder", "Search");
   if ($.data.isMobile) {
		var table = $('#tblData').DataTable(); // note the capital D to get the API instance
		var column = table.columns('.hideOnSmall');
		column.visible(false);
	}
	$(".showDates").hide();
    $(".rangeSelect").click(function() {
   	 $("#oneDateDiv").hide();
        $(".showDates").show();
    });
	$("#oneDateDiv").hide();
	$(".pickdate").click(function(){
		 $(".showDates").hide();
		$("#oneDateDiv").show();
	});
	
	/* //filter in databale
	var table = $('#tblData').DataTable();	
	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
		       	      
		var searchText=$('.dataTables_filter input').val().toLowerCase();
		        var id=data[1];
				var idName=id.substr(0, 3);
				var typeId=$('#typeId').val();
				
				if(typeId==="0")
				{
					var len=table.columns().nodes().length;
					for(var i=0; i<len; i++)
					{
						var val=data[i].trim().toLowerCase();
						if(val.includes(searchText))
						{
							
							return true;	
						}
					}	
				}
				
		        if ( idName === typeId )
		        {
		        	var len=table.columns().nodes().length;
					for(var i=0; i<len; i++)
					{
						var val=data[i].trim().toLowerCase();
						if(val.includes(searchText))
						{
							
							return true;	
						}
					}	
		        }
		        return false;
		        
	});
	
	$('#typeId').change(function(){
		//when i select any type then its show changes in datable depend on which type select
		table.draw();
	}); */
});

</script>
<style>
.card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}


.input-field {
    position: relative;
    margin-top: 0;
}
.dateFieldMargin{
	margin-top:0.5%;
}
</style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    <br>
    <div class="row">
    			<div class="col s12 l4 m5 left" >
			 <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Balance </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId">  <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${balance}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
    		   <div class="col s6 m2 l2 right right-align dateFieldMargin">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=viewAll">View All</a></li>
                    </ul> 
                </div>
            <!-- <div class="col s12 m5 l2">
                <h6 class="white-text center">Balance : &#8377; <c:out value="${balance}" /></h6>
            </div>         -->
            <div class="col s12 m4 l4">
                
                <form action="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView" method="post">
                    <input type="hidden" name="range" value="range"> 
                    		<span class="showDates">                    
                              <div class="input-field col s4 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                                <div class="input-field col s4 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s4 m1 l1" style="margin-top:3%;">
	                            <button type="submit" class="btn">View</button>
	                           </div>
                          </span>
                </form>
             
            </div>
			<div class="input-field col s12 l3 m3 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchLedgerPaymentView" method="post">
	                    <div class="input-field col s4 m8 l5">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s4 m4 l2" style="margin-top:3%;">
	                     <button type="submit" class="btn">View</button>
                         <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>

        </div>
       

       <input type="hidden" value="<c:out value="${ledgerPaymentViews.size()}" />" id="srNoId">
          <div class="row">

		  
                <div class="col s12 l12 m12 ">
                    <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="print-col hideOnSmall">Sr. No.</th>
                                <th class="print-col hideOnSmall">Date & Time</th>
                                <th class="print-col">Description</th>
                                <th class="print-col">Reference</th>
                                <th class="print-col hideOnSmall">Payament Mode</th>
                                <th class="print-col">Debit</th>
                                <th class="print-col">Credit</th>
                                <th class="print-col">Balance</th>
                            </tr>
                        </thead>

                        <tbody>
                        <% int rowincrement=0; %>
                        <c:if test="${not empty ledgerPaymentViews}">
						<c:forEach var="listValue" items="${ledgerPaymentViews}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>	
							<tr>					
							 <td><c:out value="${rowincrement}" /></td>
                             <td>
                             	<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.dateTime}" />
                             	<c:out value="${dt}" /> <br> 	  <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.dateTime}" /><c:out value="${time}" />
                             
                             </td>
                             <td>
                                <c:choose>
	                        	<c:when test="${empty  listValue.description}">
	                        		NA
		                        </c:when>
		                        <c:otherwise>
			                        <c:out value="${listValue.description}" />
		                        </c:otherwise>
		                        </c:choose>  
                             </td>
                             <td><c:out value="${listValue.reference}" /></td>
                             <td><c:out value="${listValue.payMode}" /></td>
                             
                             <td> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.debit}" /></td>
                             <td> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.credit}" /></td>
                             <td> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balance}" /></td>
                             <%-- <td><c:out value="${listValue.debit}" /></td>
                             <td><c:out value="${listValue.credit}" /></td>
                             <td><c:out value="${listValue.balance}" /></td> --%>
                            </tr>
                        </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
			</div>


    </main>
    <!--content end-->
</body>

</html>