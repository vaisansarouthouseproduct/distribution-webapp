<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    
    
    <script type="text/javascript">
    
    $(document).ready(function() {
    	var table = $('#tblData').DataTable();
  		 table.destroy();
  		 $('#tblData').DataTable({
  	         "oLanguage": {
  	             "sLengthMenu": "Show _MENU_",
  	             "sSearch": "_INPUT_" //search
  	         },
  	     /*  initComplete: function () {
  	    	.on('change', function () {
               var val = $.fn.dataTable.util.escapeRegex(
               $(this).val());
  	      } */
  	      	autoWidth: false,
  	         columnDefs: [
  	                      { 'width': '1%', 'targets': 0 },
  	                      { 'width': '5%', 'targets': 1},
  	                  	  { 'width': '10%', 'targets': 3},
  	                	  { 'width': '3%', 'targets': 5},
  	                	  { 'width': '3%', 'targets': 6},
  	             		  { 'width': '3%', 'targets': 7},
  	          			  { 'width': '3%', 'targets': 8},
  	       				  { 'width': '2%', 'targets': 9},
  	    				  { 'width': '1%', 'targets': 10}  	                     
  	                      ],
  	         lengthMenu: [
  	             [10, 25., 50, -1],
  	             ['10 ', '25 ', '50 ', 'All']
  	         ],
  	         
  	       
  	         //dom: 'lBfrtip',
  	         dom:'<lBfr<"scrollDivTable"t>ip>',
  	         	
  	         buttons: {
  	             buttons: [
  	                 //      {
  	                 //      extend: 'pageLength',
  	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
  	                 //  }, 
  	                 {
  	                     extend: 'pdf',
  	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                     customize: function(doc, config) {
  	                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [40,70,100,80,70,50,60,70,70,70,60] 
	                    		 } 
	                    	})
  	                     
  	                         var tableNode;
  	                         for (i = 0; i < doc.content.length; ++i) {
  	                           if(doc.content[i].table !== undefined){
  	                             tableNode = doc.content[i];
  	                             break;
  	                           }
  	                         }
  	        
  	                         var rowIndex = 0;
  	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
  	                          
  	                         if(tableColumnCount > 6){
  	                           doc.pageOrientation = 'landscape';
  	                         }
  	                         /*for customize the pdf content*/ 
  	                         doc.pageMargins = [5,20,10,5];   	                         
  	                         doc.defaultStyle.fontSize = 8	;
  	                         doc.styles.title.fontSize = 12;
  	                         doc.styles.tableHeader.fontSize = 11;
  	                         doc.styles.tableFooter.fontSize = 11;
  	                       doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
  	                       },
  	                 },
  	                 {
  	                     extend: 'excel',
  	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'print',
  	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'colvis',
  	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
  	                     collectionLayout: 'fixed two-column',
  	                     align: 'left'
  	                 },
  	             ]
  	         }

  	     });
  		 $("select").change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
		$('.dataTables_filter input').attr("placeholder", "Search");
		if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
							}
        
        //msg come when orders not found
    	var msg="${orderDetailsList.errorMsg}";
    	if(msg!=='' && msg!=undefined)
	    {
			Materialize.Toast.removeAll();
    		Materialize.toast(msg, 2000, 'teal lighten-2');// 'rounded' is the class I'm applying to the toast
    		/*  $('#addeditmsg').modal('open');
    	     $('#msgHead').text("Order List Message");
    	     $('#msg').text(msg); */
	    }
    	
    	var pageType="${orderDetailsList.pageName}";
    	
    	//reset table data according area select
    	 var table = $('#tblData').DataTable();	
    	 $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
    		 var searchText=$('.dataTables_filter input').val().toLowerCase();       
    		        var areaNameTb=data[4].trim();
    		        var areaId=$('#areaId').val();
    				var areaName=$('#areaId option:selected').text();
    				//alert(salesManId+"-"+salesManId2);
    				if(areaId==="0")
    				{
    					var len=table.columns().nodes().length;
    					for(var i=0; i<len; i++)
    					{
    						var val=data[i].trim().toLowerCase();
    						if(val.includes(searchText))
    						{    							
    							return true;	
    						}
    					} 	
    				
    						    					
    				}
    				
    		        if ( areaName === areaNameTb)
    		        {
    		        	var len=table.columns().nodes().length;
    					for(var i=0; i<len; i++)
    					{
    						var val=data[i].trim().toLowerCase();
    						if(val.includes(searchText))
    						{    							
    							return true;	
    						}
    					} 	
    		        }
    		        return false;
    		        
    	}); 
    	
    	$('#areaId').change(function(){
    		
    		table.draw();
    	});
    	
    	/* $('#areaIds').change(function(){
    		
    		if($('#areaIds').val()==="0")
    		{
    			if(pageType==="Current")
        		{
        			window.location.href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0";
        		}
        		else
        		{
        			window.location.href="${pageContext.servletContext.contextPath}/GKOrderDetailsPendingList?areaId=0";
        		}
    			return false;
    		}
    		else
    		{
	    		if(pageType==="Current")
	    		{
	    			window.location.href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId="+$('#areaIds').val();
	    		}
	    		else
	    		{
	    			window.location.href="${pageContext.servletContext.contextPath}/GKOrderDetailsPendingList?areaId="+$('#areaIds').val();
	    		}
    		}
    	}); */
    	
    });
    
    </script>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
			<div class="row noBottomMarginOnLarge">
            <div class="col s6 l2 m4 right">
                <select id="areaId" class="btnSelect">
			      <option value="0" selected>Area Filter</option>
                      <c:if test="${not empty orderDetailsList.areaList}">
							<c:forEach var="listValue" items="${orderDetailsList.areaList}">
								<option value="<c:out value="${listValue.areaId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>
			
			<c:if test="${orderDetailsList.pageName=='Pending'}">
            <div class="col s6 l2 m2 right">
                <a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0" class="btn waves-effect waves-light blue-gradient">Current order</a>
            </div>
            </c:if>
            <c:if test="${orderDetailsList.pageName=='Current'}">
            <div class="col s6 l2 m2 right">
                <a href="${pageContext.servletContext.contextPath}/GKOrderDetailsPendingList?areaId=0" class="btn waves-effect waves-light blue-gradient">Pending Order</a>
            </div>
            </c:if>
		</div>
            <!--<div class="col s12 m4 l4" style="margin-top:1%;">
                <div class="col s6 m12 l12 ">
                     Dropdown Trigger 
                    <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                     Dropdown Structure 
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="ledger?range=last7days">Last 7 Days</a></li>
                        <li><a href="ledger?range=last1month">Last 1 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a href="ledger?range=all">View All</a></li>
                    </ul>
                </div>

                <form action="ledger">
                    <input type="hidden" name="range" value="dateRange"> <span class="showDates">
                              <div class="input-field col s6 m3 l4">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>

                              <div class="input-field col s6 m3 l4">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                            <button type="submit" class="btn">View</button>
                          </span>
                </form>
            </div>-->
      <!--   </div>




        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr. No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Salesman Name</th>
                            <th class="print-col">Shop Name</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Total Qty</th>
                            <th class="print-col">Taxable Amount</th>
                            <th class="print-col">Tax</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Payment Period Days</th>
                            <th class="print-col">Order Date</th>
                        </tr>
                    </thead>

                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty orderDetailsList.orderDetailsList}">
						<c:forEach var="listValue" items="${orderDetailsList.orderDetailsList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.servletContext.contextPath}/GKfetchOrderDetailsByOrderId?orderId=${listValue.orderId}"><c:out value="${listValue.orderId}" /></a></td>
                            <td class="wrapok">
                            <c:choose>
                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
                            		<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.employeeName}" /></a>
                            	</c:when>
                            	<c:otherwise>
 	                            	<c:out value="${listValue.employeeName}" />
                            	</c:otherwise>
                            </c:choose>
                            </td>
                            <td class="wrapok"><c:out value="${listValue.shopName}" /></td>
                            <td><c:out value="${listValue.areaName}" /></td>
                            <td><c:out value="${listValue.totalQuantity}" /></td>
                            <td><c:out value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax - listValue.totalAmount}" /></td>
                            <td><c:out value="${listValue.totalAmountWithTax}" /></td>
                            <td><c:out value="${listValue.paymentPeriodDays}" /></td>
                            <td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value="${listValue.orderDetailsAddedDatetime}"  /><c:out value="${date}" />
                            </td>
                            
                        </tr>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" >
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
      
    </main>
    <!--content end-->
</body>

</html>