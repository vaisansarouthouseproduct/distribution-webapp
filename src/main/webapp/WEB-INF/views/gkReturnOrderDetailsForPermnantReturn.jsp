<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
<%@include file="components/header_imports.jsp"%>
<script type="text/javascript">
var rtnOrderId='${returnOrderId}';
var productArray=[];
var orderId,empId;
	function hitForData() {
		$.ajax({
					type : "GET",
					url : "${pageContext.request.contextPath}/fetchReturnOrderProductDetailsByReturnOrderProductIdForReturn/${returnOrderId}",
					dataType : "json",
    				async:false,
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						if (data.status == 'Success') {
							setData(data);
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
						} else {
							 Materialize.Toast.removeAll();
								Materialize.toast('Something went wrong!', '2000', 'toastError');
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
						}

					},
					complete : function() {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						  Materialize.Toast.removeAll();
						Materialize.toast('Something went wrong!', '2000', 'toastError');
						}
				});

	}
	function hitForSubmit(data1) {
		var resulData=JSON.stringify(data1);
		$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/savePermanentOrder",
					dataType : "json",
					contentType: "application/json",
					data:resulData,
    				async:false,
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						if (data.status == 'Success') {
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
							Materialize.Toast.removeAll();
							Materialize.toast('Permanent Return Defined Successfully', 2000);
							setTimeout(function(){
								window.location.href="${pageContext.request.contextPath}/fetchReturnOrderReportForWeb"; 
								}, 3000);
							
						} else {
							 Materialize.Toast.removeAll();
								Materialize.toast('Something went wrong!', 2000, 'toastError');
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
						}

					},
					complete : function() {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						  Materialize.Toast.removeAll();
						Materialize.toast('Something went wrong!', '2000', 'toastError');
						}
				});

	}
	function setData(data){
		console.log('ajax data');
		console.log(data);
		var orderId1=data.returnOrderProduct.orderDetails.orderId,
			shopName=data.returnOrderProduct.orderDetails.businessName.shopName,
			mobileNo=data.returnOrderProduct.orderDetails.businessName.contact.mobileNumber,
			dateOfOrder=milisecondsToDate(data.returnOrderProduct.orderDetails.orderDetailsAddedDatetime),
			returnOrderProduct=data.returnOrderProductDetailsList;
		orderId=orderId1;
		empId=data.returnOrderProduct.employee.employeeId;
		$('#orderId').text(orderId1);
		$('#returnOrderId').text(rtnOrderId);
		$('#shopName').text(shopName);
		$('#mobileNo').text(mobileNo);
		$('#dateOfOrder').text(dateOfOrder);
		var tbl=$('#tableData').empty();
		productArray=[];
		if(returnOrderProduct.length>0){
			var srNo;
			
			for(var i=0;i<returnOrderProduct.length;i++){
				var productData =returnOrderProduct[i];
				var productId=productData.product.productId,
					returnQty=productData.returnQuantity,
					returnTotalAmt=productData.returnTotalAmountWithTax,
					sellingRate=productData.sellingRate,
					type=productData.type,
					rate=productData.product.rate,
					productName=productData.product.productName,
					reason=productData.reason,
					issuedQty=productData.issuedQuantity,
					insideProductId=productData.product.product.productId,
					permanentRtnQty=0,
					returnTotalAmtWithoutTax=0;
				
				var data={
						id:productId,
						productName:productName,
						returnQty:returnQty,
						returnTotalAmt:returnTotalAmt,
						sellingRate:sellingRate,
						type:type,
						rate:rate,
						reason:reason,
						issuedQty:issuedQty,
						permanentRtnQty:permanentRtnQty,
						returnTotalAmtWithoutTax:returnTotalAmtWithoutTax,
						isChange:false,
						insideProductId:insideProductId
						
				}
				productArray.push(data);
				
				
				srNo=i+1;
				tbl.append('<tr>'+
							'<td>'+srNo+'</td>'+
							'<td>'+productName+'</td>'+
							'<td>'+returnQty+'</td>'+
							'<td><input type="text" id="permanentRtn_'+productId+'" data-returnqty="'+returnQty+'" data-id="'+productId+'" class="num inputReturn" value='+permanentRtnQty+' disabled></td>'+
							'<td><span id="remainingQty_'+productId+'">'+returnQty+'</span></td>'+
							'<td>'+
							'<input type="checkbox" class="filled-in checkBox" id="chb_'+productId+'" data-id="'+productId+'"/>'+
                            '<label for="chb_'+productId+'" style="padding-left: 20px;"></label>'+
                            '</td>'+
						+'</tr>');
			}
			console.log(productArray);
			
		}
		else{
			tbl.append('<tr><td colspan="6">No data Found</td></tr>');
		}
			
	}
	
	function showToast(){
		 Materialize.Toast.removeAll();
		 var $toastContent = $('<span>Do you want to return?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">No</button><button class="btn white-text toast-action" onclick="submitPermanentReturn();">Yes</button>'));
		  Materialize.toast($toastContent, 1500);
	}
	function submitPermanentReturn(){
		var totalAmount=0 ,totalAmountWithTax=0 ,totalQuantity =0,returnOrderProductDetailsPList=[];
		for(var i=0;i<productArray.length;i++){
			var productData =productArray[i];
			if(productData.isChange == true){
				console.log(productData);
				if(productData.permanentRtnQty == 0 || productData.permanentRtnQty == undefined){
					Materialize.Toast.removeAll();
					 Materialize.toast('Permanent Return Quantity should be Greater than zero for <span class="yellow-text">'+productData.productName+'</span>!', '2000', 'toastError');
					 return false;
				}
				else{
					
					var issuedQuantity =productData.issuedQty,
						returnQuantity= productData.permanentRtnQty,
						sellingRate =productData.sellingRate,
						returnTotalAmountWithoutTax=productData.returnTotalAmtWithoutTax,
						returnTotalAmountWithTax =productData.returnTotalAmt,
						reason=productData.reason,
						productId =productData.id,
						type=productData.type,
						insideProductId=productData.insideProductId;
					totalQuantity=totalQuantity+returnQuantity;
					totalAmount=totalAmount+returnTotalAmountWithoutTax;
					totalAmountWithTax=totalAmountWithTax+returnTotalAmountWithTax;
					
						
						
					var data={
							issuedQuantity:issuedQuantity,
							returnQuantity:returnQuantity,
							sellingRate:sellingRate,
							returnTotalAmountWithTax:returnTotalAmountWithTax,
							reason:reason,
							type:type,
							product :{
								productId:productId,
								product :{
									productId:insideProductId
								}
							}
					}
					returnOrderProductDetailsPList.push(data);
				}
			}
		}
		if(totalQuantity !=0 && totalQuantity !=undefined){
			var data={
				returnOrderProductP:{
					totalAmount :totalAmount ,
					totalQuantity :totalQuantity ,
					totalAmountWithTax :totalAmountWithTax,
					employee : {
						employeeId:empId		
					},
					orderDetails:{
						orderId : orderId
					}

				},
				returnOrderProductDetailsPList:returnOrderProductDetailsPList,
				returnOrderProductId:rtnOrderId
		
		}
		hitForSubmit(data);
		}
		
	}
	
	function milisecondsToDate(str){
		var date=moment.unix(str/1000).format("DD-MM-YYYY");
		return date;
	}
	function inputChange(id){
		var qty=$('#permanentRtn_'+id).val().trim();
		var returnQty=$('#permanentRtn_'+id).attr('data-returnqty');
		if(qty != ""||qty != undefined){
			qty=parseFloat(qty);
			returnQty=parseFloat(returnQty);			
			if(qty >returnQty){
				var qty=$('#permanentRtn_'+id).val('');
				Materialize.Toast.removeAll();
				 Materialize.toast('Permanent Return Quantity should be less than Return Quantity!', '2000', 'toastError');
				 return false;
			}
			else{
				console.log(obj);
				var obj = productArray.find(function (obj) { return obj.id === id; });				
				var returnTotalAmt =obj.sellingRate * qty;
				var returnTotalAmtWithoutTax =obj.rate * qty;
				var remainingQty=obj.returnQty-qty;
				obj.isChange=true;
				obj.permanentRtnQty=qty;	
				obj.returnTotalAmt =parseFloat(returnTotalAmt.toFixedVSS(2));
				obj.returnTotalAmtWithoutTax =parseFloat(returnTotalAmtWithoutTax.toFixedVSS(2));
				$('#remainingQty_'+id).text(remainingQty);
				console.log('changed object');
				console.log(obj);
			}
			
		}
		else{
			 /* Materialize.Toast.removeAll();
			 Materialize.toast('Please Enter Return Quantity', '2000', 'toastError'); */		
		}
	}
	$(document).ready(function() {
		hitForData();
		$('.num').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		});
		$('.checkBox').change(function(){
			var id=$(this).attr('data-id');
				if($(this).is(':checked')){
					
					$('#permanentRtn_'+id).attr('disabled',false);
				}
				else{
					$('#permanentRtn_'+id).val(0).change();
					$('#permanentRtn_'+id).attr('disabled',true);
				}
		});
		$('.inputReturn').change(function(){
			var id=$(this).attr('data-id');
			if(id != "" || id!=undefined){
				id=parseFloat(id);
				if($(this).val().trim() != ""){
					inputChange(id);
				}
	
			}
						
		});
	});
</script>
<style>
/* .card-panel p{
        	font-size:15px !important;
        } */
.card-panel p {
	margin: 5px !important;
	font-size: 16px !important;
	color: black;
	/* border:1px solid #9e9e9e; */
}

.card-panel {
	padding: 8px !important;
	border-radius: 8px;
}

tfoot th, tfoot td {
	text-align: center;
}
td input{
text-align:center !important;
}
.leftHeader {
	width: 120px !important;
	display: inline-block;
}
.toastError{
	background-color:red;
}
</style>
</head>

<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	<!--content start-->
	<main class="paddingBody"> <br>
	<div class="row">
		<div class="col s12">
		<div class="col col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
			<div class="col s12 l6 m6">
				<p>
					<span class="leftHeader">Order Id:</span> <b id="orderId"></b>
				</p>
			</div>
			<div class="col s12 l6 m6">
				<p id="department">
					<span class="leftHeader">Shop Name: </span> <b id="shopName"></b>
				</p>
			</div>
			<div class="col s12 l6 m6 ">
				<p>
					<span class="leftHeader">Return Order Id: </span> <b id='returnOrderId'></b>
				</p>
			</div>
			<div class="col s12 l6 m6 ">
				<p>
					<span class="leftHeader">Mobile No: </span> <b id='mobileNo'></b>
				</p>
			</div>
			<div class="col s12 l6 m6">
				<p id="Add">
					<span class="leftHeader">Date of Order:</span> <b id='dateOfOrder'>
					</b>
				</p>

			</div>
		</div>
	</div>
	</div>
	<div class="row">
		<div class="col s12 l12 m12">
			<table class="striped highlight tblborder centered" id="tblData5"
				cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr. No.</th>
						<th class="print-col">Product Name</th>
						<th class="print-col">Return Quantity</th>
						<th class="print-col">Permanent Return</th>
						<th class="print-col">Remaining Qty</th>
						<th class="print-col">Select Return</th>
					</tr>
				</thead>
				<tbody id="tableData">
					<tr>
						<td>1</td>
						<td>Mouse</td>
						<td>20</td>
						<td><input type='text' class='num'></td>
						<td>15</td>
						<td>
						 <input type="checkbox" class="filled-in" id="id" />
                            <label for="id" style="padding-left: 20px;"></label>

						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12 l12 center">
			<button class="btn" onclick="showToast();">Return</button>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12 l8">
			<div id="addeditmsg" class="modal">
				<div class="modal-content" style="padding: 0">
					<div class="center   white-text" id="modalType"
						style="padding: 3% 0 3% 0"></div>
					<!--  <h5 id="msgHead"></h5> -->

					<h6 id="msg" class="center"></h6>
				</div>
				<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
			</div>
		</div>
	</div>

	</main>
	<!--content end-->
</body>

</html>