<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
    <script>var myContextPath = "${pageContext.request.contextPath}"</script>
	
    <script>
    var isUpdate=false;
    var url="${url}";
    
    /* for update process */
    <c:if test="${not empty meeting}"> 
    	isUpdate=true;
		var employeeId="${meeting.employeeDetails.employee.employeeId}";
		var businessId="${meeting.businessName.businessNameId}";
		
		var ownerName="${meeting.ownerName}";
		var shopName="${meeting.shopName}";
		var mobileNumber="${meeting.mobileNumber}";
		var address="${meeting.address}";
	</c:if>
	/* End */
    
        $(document).ready(function() { 
        	
        	$.validator.setDefaults({
     	       ignore: []
     		});
        	jQuery.validator.addMethod("employeeIdCheck", function(value, element){
           		if(value=='0')
           		{
           			return false;
           		}		
           	    return true;
           	}, "Select Employee"); 
        	jQuery.validator.addMethod("businessIdCheck", function(value, element){
           		if(value=='0')
           		{
           			return false;
           		}		
           	    return true;
           	}, "Select Business Or Other"); 
        	
        	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
           		if(value=='' || value==undefined){
           			return false;
           		}else if(value.length!=10){
           			return false;
           		}else if(isNaN(value)){ 
           			return false;
           		}		
           	    return true;
           	}, "Invalid Mobile Number"); 
        	
        	jQuery.validator.addMethod("meetingDateCheck", function(value, element){
           		if(value=='' || value==undefined){
           			return false;
           			
           		}else{
           			if(isUpdate==false){
	           			var todayDate=new Date().setHours(0,0,0,0);
	           			var meetingDate=new Date(value).setHours(0,0,0,0);
	           			
	           			if(todayDate > meetingDate){
	           				return false;
						}
           			}
           			if(url=="reschedule_meeting"){
	           			var todayDate=new Date().setHours(0,0,0,0);
	           			var meetingDate=new Date(value).setHours(0,0,0,0);
	           			
	           			if(todayDate > meetingDate){
	           				return false;
						}
           			}
           		}		
           		
           	    return true;
           	}, "Invalid Meeting Date"); 
        	
        	jQuery.validator.addMethod("meetingStartTimeCheck", function(value, element){
        		var meetingStartTime=(value);
        		var meetingEndTime=$('#meetingEndTime').val();
        		var meetingDate=$('#meetingDate').val();
        		
        		if(meetingEndTime==meetingStartTime){
           			return false;
           		}		
           		
        		meetingStartTime=moment(moment(meetingStartTime, 'hh:mma').toDate()).format('hh:mm a');
        		meetingEndTime=moment(moment(meetingEndTime, 'hh:mma').toDate()).format('hh:mm a');
        		
        		meetingStartTime=new Date(meetingDate+" "+meetingStartTime);
        		meetingEndTime=new Date(meetingDate+" "+meetingEndTime);
        		
        		var todayDate=new Date();
        		
        		if(meetingEndTime.getTime()<meetingStartTime.getTime() || todayDate.getTime()>meetingStartTime.getTime()){
           			return false;
           		}	
       			
           	    return true;
           	}, "Check Meeting Start And End Time"); 
        	
        	jQuery.validator.addMethod("meetingEndTimeCheck", function(value, element){
        		var meetingStartTime=$('#meetingStartTime').val();
        		var meetingEndTime=(value);
        		var meetingDate=$('#meetingDate').val();
        		
        		if(meetingEndTime==meetingStartTime){
           			return false;
           		}		

        		meetingStartTime=moment(moment(meetingStartTime, 'hh:mma').toDate()).format('hh:mm a');
        		meetingEndTime=moment(moment(meetingEndTime, 'hh:mma').toDate()).format('hh:mm a');
        		
        		meetingStartTime=new Date(meetingDate+" "+meetingStartTime);
        		meetingEndTime=new Date(meetingDate+" "+meetingEndTime);
           		
        		var todayDate=new Date();
        		
        		if(meetingEndTime.getTime()<meetingStartTime.getTime() || todayDate.getTime()>meetingEndTime.getTime()){
           			return false;
           		}	
       			
           	    return true;
           	}, "Check Meeting Start And End Time"); 
        	
        	$('#setMeetingForm').validate({
        	    
        		rules: {    		    
        			employeeId:{
        				employeeIdCheck:true
                    },
        			businessId:{
                    	businessIdCheck:true
                    },
                    ownerNumber:{
                    	required:true
                    },
                    mobileNumber:{
                    	mobileNumberCheck:true,
                    	required:true
                    },
                    shopName:{
                    	required:true
                    },
                    address:{
                    	required:true
                    },
                    meetingDate:{
                    	meetingDateCheck:true,
                    	required:true
                    },
                    meetingStartTime:{
                    	meetingStartTimeCheck:true,
                    	required:true
                    },
                    meetingEndTime:{
                    	meetingEndTimeCheck:true,
                    	required:true
                    }
        		  },
        		
        		errorElement : "span",
        	    errorClass : "invalid error",
        	    errorPlacement : function(error, element) {
        	      var placement = $(element).data('error');
        	    
        	      if (placement) {
        	        $(placement).append(error)
        	      } else {
        	        error.insertAfter(element);
        	      }
        	      $('select').change(function(){ $('select').valid(); });
        	    }
        	  });
        	
        	//mobile number allowed only number without decimal
            $('#mobileNumber').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
        	
            $('select').material_select();
            
            $('.timepicker').pickatime({  
	            default: 'now', // Set default time: 'now', '1:30AM', '16:30' 
	            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
	            twelvehour: true, // Use AM/PM or 24-hour format
	            donetext: 'OK', // text for done-button
	            cleartext: 'Clear', // text for clear-button
	            canceltext: 'Cancel', // Text for cancel-button,
	            container: undefined, // ex. 'body' will append picker to body
	            autoclose: false, // automatic close timepicker
	            ampmclickable: true, // make AM PM clickable
	            aftershow: function(){} //Function for after opening timepicker
	        });
            
            /* for update */
            <c:if test="${not empty meeting}">
            	
            	var meetingStartTime="${meeting.meetingFromDateTime}";
            	var startTime=moment(moment(meetingStartTime, 'yyyy-mm-dd hh-mm-ssz').toDate()).format('hh:mm a');
            	$('#meetingStartTime').val(startTime);
            	
            	var meetingEndTime="${meeting.meetingToDateTime}";
            	var endTime=moment(moment(meetingEndTime, 'yyyy-mm-dd hh-mm-ssz').toDate()).format('hh:mm a');
            	$('#meetingEndTime').val(endTime);
            	
            	var meetingDate="${meeting.meetingToDateTime}";
            	var meetingDate=moment(new Date(meetingDate)).format('YYYY-MM-DD');
            	$('#meetingDate').val(meetingDate);
            	
            	$('#setMeetingSubmit').html('Update Meeting<i class="material-icons right">send</i>');
            </c:if>
            /* end */
        });
    </script>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script  type="text/javascript" src="resources/js/MeetingSchedule.js"></script>
    <style>
        .eye .eye-slash .eye1 .eye-slash1 {
            transform: translateY(-16%);
		}
		span.selectLabel {
	position: absolute;
	top: -1rem;
	left: 3.1rem;
	font-size: 0.8rem;
}
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
		
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/${url}" method="post" id="setMeetingForm">
            	<input id="meetingId" type="hidden" value="${meeting.meetingId}" name="meetingId">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Business Details</h4>
                    </div>
                    <div class="col s12 m12 l12"> 
	                    <div class="row">
		                    <div class="input-field col s12 m4 l4">
								<i class="material-icons prefix">view_stream</i>                        
								<span class="selectLabel"><span class="red-text">*&nbsp</span>Select Department</span>
								
		                        <select id="departmentId" name="departmentId"  >
		                                
		                                <c:choose>
										<c:when test="${not empty meeting}">
											<option value="0" >Department</option>
										</c:when>
										<c:otherwise>
											<option value="0" selected>Department</option>
										</c:otherwise>
										</c:choose>
		                                
		                                <c:if test="${not empty departmentList}">
										<c:forEach var="listValue" items="${departmentList}">
											<c:choose>
											<c:when test="${not empty meeting}">
												<option value="<c:out value="${listValue.departmentId}" />" ${meeting.employeeDetails.employee.department.departmentId==listValue.departmentId?'selected':''}><c:out value="${listValue.name}" /></option>
											</c:when>
											<c:otherwise>
												<option value="<c:out value="${listValue.departmentId}" />" ><c:out value="${listValue.name}" /></option>
											</c:otherwise>
											</c:choose>											
										</c:forEach>
									</c:if>
		                        </select>
		                    </div>
		                    <div class="input-field col s12 m4 l4 ">
								<i class="material-icons prefix">people</i>  
								<span class="selectLabel"><span class="red-text">*&nbsp</span>Select Employee</span>                      
		                        <select id="employeeId" name="employeeId"  >
		                                 <option value="0" selected>Employee</option>
		                        </select>
		                    </div>		
		                    <div class="input-field col s12 m4 l4 ">
								<i class="material-icons prefix">business</i>                        
								<span class="selectLabel"><span class="red-text">*&nbsp</span>Select Business</span>                      
		                        <select id="businessId" name="businessId">
		                                 <option value="0" selected>Business</option>
		                                 <option value="Other" >Other</option>
		                        </select>
		                    </div>
	                    </div>
                    </div>
                    <div class="row">
	                     <div class="col s12 m12 l12">                     
	                    	<div class="input-field col s12 m6 l6">
		                        <i class="material-icons prefix">person</i>
		                        <input id="ownerName" type="text" name="ownerName" value="${meeting.ownerName}" required>
		                        <label for="ownerName" class="active"><span class="red-text">*&nbsp</span>Owner Name </label>
		                    </div>
		                    <div class="input-field col s12 m6 l6">
		                        <i class="material-icons prefix">phone</i>
		                        <input id="mobileNumber" type="text" name="mobileNumber" value="${meeting.mobileNumber}" required minlength="10" maxlength="10">
		                        <label for="mobileNumber" class="active"><span class="red-text">*&nbsp</span>Mobile number</label>
		                    </div>
	                    </div>
                     </div>
                     <div class="row">
	                     <div class="col s12 m12 l12">                     
	                    	<div class="input-field col s12 m6 l6">
		                        <i class="material-icons prefix">store</i>
		                        <input id="shopName" type="text" name="shopName" value="${meeting.shopName}" required>
		                        <label for="shopName" class="active"><span class="red-text">*&nbsp</span>Shop Name </label>
		                    </div>
		                   <div class="input-field col s12 m6 l6">
		                        <i class="material-icons prefix">location_on</i>
		                         
		                        <textarea id="address" class="materialize-textarea" name="address" required><c:out value="${meeting.address}" /></textarea>
		                        <label for="address"><span class="red-text">*&nbsp</span>Address</label>
		                    </div>
	                    </div>
                     </div>
                </div>
               
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Meeting Details</h4>
                    </div>
                    <div class="row">
	                     <div class="col s12 m12 l12">                     
	                    	<div class="input-field col s12 m6 l6">
								<i class="material-icons prefix">event</i> 
								<input id="meetingDate" type="text" name="meetingDate" class="datepicker" required> 
								<label for="meetingDate" class="active black-text"><span class="red-text">*&nbsp</span>Meeting Date</label>
							</div>
		                   <div class="input-field col s12 m6 l6">
		                        <i class="material-icons prefix">event_note</i>
		                        <textarea id="meetingVenue" class="materialize-textarea" name="meetingVenue">${meeting.meetingVenue}</textarea>
		                        <label for="meetingVenue">Meeting Venue</label>
		                    </div>
	                    </div>
                    </div>
                    <div class="row">
	                     <div class="col s12 m12 l12">                     
	                    	<div class="input-field col s12 m6 l6">
								<i class="material-icons prefix">event</i> 
								<input id="meetingStartTime" type="text" name="meetingStartTime" class="timepicker" required> 
								<label for="meetingStartTime" class="active black-text"><span class="red-text">*&nbsp</span>Meeting Start Time</label>
							</div>
							<div class="input-field col s12 m6 l6">
								<i class="material-icons prefix">event</i> 
								<input id="meetingEndTime" type="text" name="meetingEndTime"  class="timepicker" required> 
								<label for="meetingEndTime" class="active black-text"><span class="red-text">*&nbsp</span>Meeting End Time</label>
							</div>
	                    </div>
                    </div>
                </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue darken-8" type="submit" id="setMeetingSubmit">Set Meeting<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>
 
        </div>


 		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
						<div class="col s12 center">
								<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>