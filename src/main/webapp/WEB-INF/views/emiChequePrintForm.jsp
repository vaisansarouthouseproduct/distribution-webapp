<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resources/js/chequePrint.js"></script>
	<script type="text/javascript" src="resources/js/emiCalc.js"></script>

<script type="text/javascript">
	var myContextPath = "${pageContext.request.contextPath}";
	var templateDateFormate;
	/*function to set the data*/
	function previewDataSet(id) {
		
		var payeeName=$('#payeeName').val();
		var emiAmt=$('#installmentAmt').val();
		
		var payeeName = $('#payeeName').val();
		var amt =$('#installmentAmt').val();
		var amtInWord = convertNumberToWords(amt) + "Rupees Only";
		var chqDate = $('#chq_date' + id).text();
		//to convert dd-MM-yyyy to yyyy-MM-dd
		var chqDateFormate = chqDate.split("-").reverse().join("-");
		var chqNo = $('#chq_no' + id).text();
		var remark = $('#emiPurpose').val();;
		var textData = "";
		/* if ($('#chb' + id).is(':checked')) {
			textData = "A/c Payee";
		} */
		
		
		/* DATE sPLIIRING */
		var dateData = moment(chqDateFormate);
		var date = dateData.format("DD");
		var month = dateData.format("MM");
		var year = dateData.format("YYYY");
	
		
		
		var lineDate="",d1="",d2="",m1="",m2="",y1="",y2="",y3="",y4="";
		if(templateDateFormate=="lineFormate"){
			 lineDate=date+"/"+month+"/"+year;	
		}else{
			 d1 = date.substring(0, 1);
			 d2 = date.substring(1, 2);
			 m1 = month.substring(0, 1);
			 m2 = month.substring(1, 2);
			 y1 = year.substring(0, 1);
			 y2 = year.substring(1, 2);
			 y3 = year.substring(2, 3);
			 y4 = year.substring(3, 4);
		}
		
		
		/* create aarray for sending data to Backend */
		var data={
				chequeNo:chqNo,
				payeeName:payeeName,
				chequeAmt:amt,
		        acPayee:textData,
				chequeDate:chqDateFormate,
				remark:remark
		}
		dataList.push(data);
		$(".modal-content").append('<div class="container1" id="container_'+id+'" style="position: relative">'
								+ '<div class="center-align" style="height: 100%; width:fit-content;padding:0;">'
								+ '<div class="center-align" style="padding:0">'
								+ '<img style="border: 1px solid; height: 360px; width: 816px;" id="addImage_'+id+'" class="chqImg"  src="'+imgSrc+'"/>'
								+ '<div id="chqDatePreView_'
								+ id
								+ '" class="chqDate chqDatePreView" style="position: absolute; top:6%;border: 0px solid blue; padding: 5px; background: solid white;">'
								+ '<div id="lineDate_'+id+'" class="lineDateFormat">'
								+ '<p id="lineDatePreview_'+id+'" class="lineDatePosition">'+lineDate+'</p>'
								+ '</div>'
								+ '<div id="boxDate_'+id+'" class="boxDateFormat" style="display: inline-block">'
								+ '<div id="d1_'+id+'" class="dateBox dateDimension " style="display:table-cell;vertical-align: middle;">'
								+ '<p id="d1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ d1
								+ '</p>'
								+ '</div>'
								+ '<div id="d2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="d2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ d2
								+ '</p>'
								+ '</div>'
								+'<div id="m1_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="m1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ m1
								+ '</p>'
								+ '</div>'
								+'<div id="m2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p  id="m2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ m2
								+ '</p>'
								+ '</div>'
								+'<div id="y1_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y1Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y1
								+ '</p>'
								+ '</div>'
								+'<div id="y2_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p  id="y2Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y2
								+ '</p>'
								+ '</div>'
								+'<div id="y3_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y3Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y3
								+ '</p>'
								+ '</div>'
								+'<div id="y4_'+id+'" class="dateBox dateDimension" style="display:table-cell;vertical-align: middle;">'
								+ '<p id="y4Preview_'+id+'" class="textPosition1 dateDimension" style="text-align:center">'
								+ y4
								+ '</p>'
								+ '</div>'
								+ '</div>'
								+ '</div>'
								+ '<div id="payeeNamePreView_'+id+'" class="draggable" style="position: absolute; left: 120px; top: 60px; width: 400px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">'
								+ '<p id="payeeNamePreViewText_'+id+'" name="payeeNamePreViewText" class="textPosition payeeNamePreView" style="margin:0">'
								+ payeeName
								+ '</p>'
								+ '</div>'
								+'<div id="amountPreView_'+id+'" class="draggable" style="position: absolute; right: 60px; top: 130px; width: 130px; height: 30px; min-width: 60px; border: 0px solid blue; padding: 5px;">'
								+ '<p id="amountPreViewText_'+id+'" class="textPosition amountPreView" style="margin:0">***'
								+ amt
								+ '/-</p>'
								+ '</div>'
								+'<div id="amountInWordPreView_'+id+'" class="draggable" style="position: absolute; left: 120px; top: 100px; width: 380px; height: 30px; border: 0px solid blue; padding: 5px; min-width: 290px;">'
								+ '<p id="amountInWordPreViewText_'+id+'" class="textPosition amountInWordPreView"  style="margin:0" >'
								+ amtInWord
								+ '</p>'
								+ '</div>'
								+'<div id="acPayeePreView_'+id+'" class="draggable" style="position: absolute; left: 350px; top: 170px; width: 120px; height: 30px; border: 0px solid white; padding: 5px; min-width: 75px;">'
								+ '<p id="acPayeePreViewText_'+id+'" class="textPosition acPayeePreView" style="margin:0">'
								+ textData
								+ '</p>'
								+ '</div>'
								+ '</div>'
								+ '</div>' + '</div>');
	}
/* Table Init */
	function tableInitialize() {
		//var table = $('#tblData').DataTable();
		//table.destroy();
		$('#tblData')
				.DataTable(
						{
							"oLanguage" : {
								"sLengthMenu" : "Show _MENU_",
								"sSearch" : "_INPUT_" //search
							},
							"bDestroy" : true,
							autoWidth : false,
							columnDefs : [ {'width' : '5%','targets' : 0},
								{'width' : '10%','targets' : 1},
								{'width' : '25%','targets' : 2},
								{'width' : '8%','targets' : 3},
								{'width' : '15%','targets' : 4},
								{'width' : '5%','targets' : 5}
							
							],
							lengthMenu : [ [ 10, 25., 50, -1 ],
									[ '10 ', '25 ', '50 ', 'All' ] ],

							//dom: 'lBfrtip',
							dom : '<lBfr<"scrollDivTable"t>ip>',
							buttons : {
								buttons : [
										//      {
										//      extend: 'pageLength',
										//      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
										//  }, 
										{
											extend : 'pdf',
											className : 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
											customize : function(doc, config) {
												doc.content.forEach(function(
														item) {
													if (item.table) {
														item.table.widths = [
																30, '*', '*',
																'*', '*', 50 ]
													}
												})
												/* var tableNode;
												for (i = 0; i < doc.content.length; ++i) {
												  if(doc.content[i].table !== undefined){
													tableNode = doc.content[i];
													break;
												  }
												}
												
												var rowIndex = 0;
												var tableColumnCount = tableNode.table.body[rowIndex].length;
												 
												if(tableColumnCount > 6){
												  doc.pageOrientation = 'landscape';
												} */
												/*for customize the pdf content*/
												doc.pageMargins = [ 5, 20, 10,
														5 ];
												doc.defaultStyle.fontSize = 8;
												doc.styles.title.fontSize = 12;
												doc.styles.tableHeader.fontSize = 11;
												doc.styles.tableFooter.fontSize = 11;
												doc.styles.tableHeader.alignment = 'center';
												doc.styles.tableBodyEven.alignment = 'center';
												doc.styles.tableBodyOdd.alignment = 'center';
											},
										},
										{
											extend : 'excel',
											className : 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
										},
										{
											extend : 'print',
											className : 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
											//title of the page
											title : function() {
												var name = $(".heading").text();
												return name
											},
											//file name 
											filename : function() {
												var d = new Date();
												var date = d.getDate();
												var month = d.getMonth();
												var year = d.getFullYear();
												var name = $(".heading").text();
												return name + date + '-'
														+ month + '-' + year;
											},
											//  exports only dataColumn
											exportOptions : {
												columns : ':visible.print-col'
											},
										},
										{
											extend : 'colvis',
											className : 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
											text : '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
											collectionLayout : 'fixed two-column',
											align : 'left'
										}, ]
							}

						});
	}

		/* feild validation  */
	function feildValidation() {
	var payeeName = $('#payeeName').val();
	var emiPurpose = $('#emiPurpose').val();
	var installmentAmt = $('#installmentAmt').val();
	var noOfCheque = $('#noOfCheque').val();
	var startDate = $('#startDate').val();
	var bankAccountId1 = $('#bankAccountId').val();
	

	if(bankAccountId1==null){
		var $toastContent = $('<span>Please Select Bank Account<span>')
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		
	}else if (emiPurpose =="") {
		msg = "Please Enter EMI Purpose";
		
		var $toastContent = $('<span>Please Enter EMI Purpose <span>')
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		return false;
	}else if (installmentAmt <=0) {
		msg = "Please Enter installmentAmt";
		
		var $toastContent = $(
				'<span>Please Enter Installment Amount<span>')
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		return false;
	} else if (noOfCheque <=0) {
		
		var $toastContent = $('<span>Please Enter No of Cheque<span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		return false;
	}else if (startDate =="") {
		msg = "Please Enter EMI Purpose";
		//message.innerHTML = "Please Enter Cheque Amount";
		$('#msg').text(msg);
		var $toastContent = $('<span>Please Enter Start Date <span>')
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		return false;
	}else if(intervalType==""){
		var $toastContent = $('<span>Please Select Time Interval <span>')
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
	}else if (payeeName == "") {
		msg = "Please Enter Payee Name";
		//message.innerHTML = "Please Enter Payee Name";
		$('#msg').text(msg);
		//window.alert("Please Enter Payee Name for Cheque No:"+chqNo)

		var $toastContent = $('<span>Please Enter Payee Name<span>');
		Materialize.Toast.removeAll();
		Materialize.toast($toastContent, 3000);
		return false
	} else {
		return true;
	}
	
	
}

function chequeFormHide(){
	if(feildValidation()){
		$('#chequeFormId').hide();
		$('#bulkEMIData').show();
	}
	
	/* if(feildValidation){
		$('#chequeFormId').hide();	
		} */
}

function backClick(){
	$('#chequeFormId').show();
	$('#bulkEMIData').hide();
}

function openEMICalculator(){
	$('#emiCalculatorModal').modal('open');
	
}
function openTDSCalculator(){
	$('#tdsCalculatorModal').modal('open');
}
/*function for sending data to server after pint window is opened*/
function sendingBulkData(){
	Materialize.Toast.removeAll();
	$('#contentData').css('display','block');
	window.print();
	
	var dataListReq={
			bulkPrintSaveModelList:dataList,
			cbId:cbId,
			isEMICheque:"Yes"
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/saveBulkPrintCheque",
		type : "POST",
		headers : {
			'Content-Type' : 'application/json'
 		},
		beforeSend:function(){
			$('.contentData').css('display','none');
		},
		data : JSON.stringify(dataListReq),
		success : function(result) {

			
			//console.log(CategoryIgst);
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			window.location.reload();
		},
		error : function(xhr,status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			Materialize.Toast.removeAll();
			Materialize.toast('Something Went Wrong!','2000','red lighten-2');
		}
	});
}
$(document).ready(function() {

	/*  $('#openEMICalculator').onclick(){
		 $('#emiCalculatorModal').modal('open');
	 } */

	 $('#emiCalculatedDiv').hide();
	//allowed only numbers with decimal 
	$('#creditLimit').keydown(function(e){            	
		-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
	 });
	/* //allowed only numbers with decimal 
	document.getElementById('installmentAmt').onkeypress=function(e){
    	
    	if (e.keyCode === 46 && this.value.split('.').length === 2) {
      		 return false;
  		 }

    } */
	 //allowed only numbers 
	$('#loanAmt').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});  
    
 // only number allowed
	$('#timePeriod').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	
	// only number allowed with decimal point
	$('#interestRate').keypress(function( event ){
	    var key = event.which;
	    
	    if($(this).val().indexOf('.') !== -1 && event.keyCode == 46)
            event.preventDefault(); 
	    
	    if(key!=46){
	    	 if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	 	        event.preventDefault();
	    }
	   
	});
	
	
	 //allowed only numbers 
	$('#salaryAmt').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});  
	
	// only number allowed with decimal point
	$('#percentageSlab').keypress(function( event ){
	    var key = event.which;
	    
	    if($(this).val().indexOf('.') !== -1 && event.keyCode == 46)
            event.preventDefault(); 
	    
	    if(key!=46){
	    	 if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	 	        event.preventDefault();
	    }
	   
	});
	
	//allowed only numbers
	 $('#installmentAmt').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		});
	//allowed only numbers
	 $('#noOfCheque').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		}); 
	
	
	 /* on Account number change Cheque Book List set on Cheque Book drop down */
		$('#bankAccountId').change(function() {

							var bankAccountId = $('#bankAccountId').val();
							if (bankAccountId === "0") {
								//changeTb="area";
								//table.draw();
								return false;
							}

							var select = document.getElementById('chequeBookId');

							// Clear the old options
							select.options.length = 0;
							select.options
									.add(new Option("Select Cheque Book ",'0'));
							$.ajax({
										url : "${pageContext.request.contextPath}/fetchBankCBListByBankAccountId?bankAccountId="
												+ bankAccountId,
												type : "get",
										dataType : "json",
										success : function(data) {

											/* alert(data); */
											var options, index, option;
											select = document
													.getElementById('chequeBookId');

											for (var i = 0; i < data.length; i++) {
												var chequeBook = data[i];
												select.options.add(new Option(chequeBook.chqBookName+" ("+chequeBook.numberOfUnusedLeaves+" )",chequeBook.chequeBookId));
											}

											//console.log(CategoryIgst);
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											var table = $('#tblData').DataTable();
											table.clear().draw();

										},
										error : function(xhr,
												status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											Materialize.Toast.removeAll();
											Materialize.toast(
															'Please Add Cheque Template or Something went Wrong!',
															'2000',
															'red lighten-2');
										}
									});

						});
	 	/* Account number change end here */
	 
	 
	 /* New Ajax */
	 // on Cheque Book Id Select fetch their unused cheque
						$('#chequeBookId').change(function() {
											var chequeBookId = $(
													'#chequeBookId').val();
											cbId=chequeBookId;

											if (chequeBookId === "0") {
												//changeTb="area";
												//table.draw();
												return false;
											}

											/* var select = document.getElementById('chequeBookId'); */

											/* // Clear the old options
											select.options.length = 0;
											select.options.add(new Option("Select Cheque Book ", '0')); */
											$.ajax({
														url : "${pageContext.request.contextPath}/fetchChequeNoListByCBId?chequeBookId="
																+ chequeBookId,
														dataType : "json",
														type : "get",
														success : function(data) {

															
															
															
															/* alert(data); */
															/* var options, index, option;
															select = document.getElementById('chequeBookId');
															 */
															/* for (var i = 0; i < data.length; i++) {
																var chequeBook = data[i];
																select.options.add(new Option(chequeBook.chqBookName, chequeBook.chequeBookId));
															} */
															var srno = 1;
															var table = $('#tblData').DataTable();
															table.clear().draw();
															
															table.destroy();
															
															$('#chequeNumberPrintData').empty();
															selectedPreviewListId=[];
															if(data.length==0){
																var $toastContent = $('<span>All Cheque Leaves are Printed for the Selected Cheque Book.<span>');
																Materialize.Toast.removeAll();
																Materialize.toast($toastContent, 3000);
															}
															var payeeName=$('#payeeName').val();
															var emiAmt=$('#installmentAmt').val();
															var noOfCheque=$('#noOfCheque').val();
															var acPayeeText="";
															if ($('#acPayee').is(':checked')) {
																acPayeeText = "A/c Payee";
															}
															
															var days=0;
															console.log(intervalType);
															
															
															 var startDate = new Date($("#startDate").val());
															 
															 var date=startDate.getDate();
															 
															if(noOfCheque<=data.length){
																for (var i = 0; i < noOfCheque; i++) {
																	
																	var endDate=startDate;
																	
																	/* if(i!=0){
																		if(startDate.getMonth()==11){
																			endDate=new Date(startDate.getFullYear()+1, startDate.getMonth()-11, startDate.getDate());
																		}else{
																			endDate=new Date(startDate.getFullYear(), startDate.getMonth()+1, startDate.getDate())
																		}
																		if(date!=endDate.getDate()){
																			endDate=new Date(startDate.getFullYear(), startDate.getMonth()+1, date)
																		}
																		nextMonth=endDate.getMonth();
																		if(nextMonth!=0){
																			if(nextMonth-lastMonth!=1){
																				var lastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
																				endDate = new Date(endDate.getFullYear(), endDate.getMonth()-1, lastDay.getDate());
																			}
																		}
																		
																		startDate=endDate;
																	} */
																	if(i!=0){
																		endDate=getEndDateByIntervalAndStartDate(startDate,intervalType,date)
																	}
																	startDate=endDate;
																	

																	endDate=(dateFormatter(endDate));
																	$('#chequeNumberPrintData').append(
																					'<tr>'
																							+ '<td>'+ srno+ '</td>'
																							+ '<td><span id="chq_no'+srno+'">'+ data[i]+ '</span></td>'
																							+ '<td>'+payeeName+'</td>'
																							+ '<td>'+emiAmt+'</td>'
																							+ '<td><span id="chq_date'+srno+'">'+ endDate+ '</span></td>'
																							+ '<td><input type="checkbox" class="filled-in checkAll" id="chb'+srno+'"/>'
																							+ '<label for="chb'+srno+'"></label></td>'
																							+ '</tr>');
																	
																	selectedPreviewListId.push(srno)
																	
																	console.log($('#chq_date' + srno).text());
											
																	srno++;
																	
																	//startDate=endDate;

																}
															}else{
																Materialize.Toast.removeAll();
																Materialize.toast(noOfCheque+' cheque leaves not available in this cheque Book!','2000','red lighten-2');
															}
															

														
														
															tableInitialize();

															//console.log(CategoryIgst);
															$('.preloader-wrapper').hide();
															$('.preloader-background').hide();
															$("#productRate").keyup();
														},
														error : function(xhr,
																status, error) {
															$('.preloader-wrapper').hide();
															$('.preloader-background').hide();
															Materialize.Toast.removeAll();
															Materialize.toast('Something Went Wrong!','2000','red lighten-2');
														}
													});
											
											//template fetch 
											
														$.ajax({
														type : "get",
														url : "${pageContext.request.contextPath}/fetchChequeTempalteDesigntByCBId?id="
																+ chequeBookId,
														dataType : "json",
														success : function(data) {
															var chequeTemplateDesign = data;
															imgSrc = chequeTemplateDesign.templateImgBase64;

															console.log(data);
															$("#addImage").attr("src",imgSrc);
															
															templateDateFormate=data.dateFormate;
															if(templateDateFormate=="boxFormate"){
																$('.boxDateFormat').show();
																$('.lineDateFormat').hide();
															}else{
																$('.boxDateFormat').hide();
																$('.lineDateFormat').show();
															}
															
															 var css = '.payeeNamePreView{ left: '+ data.payeeNameLeft+'px;width:'+data.payeeNameWidth+'px;top:'+data.payeeNameTop+'px;}'+
															 '.amountPreView{ left: '+ data.amountLeft+'px;width:'+data.amountWidth+'px;top:'+data.amountTop+'px;}'+	
															 '.amountInWordPreView{ left: '+ data.amountInWordLeft+'px;width:'+data.amountInWordWidth+'px;top:'+data.amountInWordTop+'px;}'+
															 '.acPayeePreView{ left: '+ data.acPayeeLeft+'px;width:'+data.acPayeeWidth+'px;top:'+data.acPayeeTop+'px;}'+
															 '.chqDatePreView{ left: '+ data.chqDateLeft+'px;width:'+data.chqDateWidth+'px;top:'+data.chqDateTop+'px;}'+
															 '.dateDimension{ height: '+ data.chqDateHeight+'px;width:'+(data.chqDateWidth)/8.25+'px;}'
															 , 
														    head = document.head || document.getElementsByTagName('head')[0],
														    style = document.createElement('style');

																style.type = 'text/css';
																if (style.styleSheet){
																  // This is required for IE8 and below.
																  style.styleSheet.cssText = css;
																} else {
																  style.appendChild(document.createTextNode(css));
																}
		
																head.appendChild(style);
															

														},
														error : function(xhr,
																status, error) {
															$(
																	'.preloader-wrapper')
																	.hide();
															$(
																	'.preloader-background')
																	.hide();
															Materialize.Toast
																	.removeAll();
															Materialize
																	.toast(
																			'Something Went Wrong!',
																			'2000',
																			'red lighten-2');
														}
													});

											//template fetch end
											
						});
	 /* New Ajax End here */
	 
	 				
						/* $("select[name='time_interval']").change(function () {

				               alert('value is' + $(this).val());
				           }); */
				           $('#timeIntervalId').change(function(){
				        	   //alert('value is' + $(this).val());
				        	   intervalType=$(this).val();
				           });
				           
				          
				           
				         
});

</script>

<style> 
.lineDatePosition{
margin-top:0 !important;
}
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	

i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	.currencyinput {
    border: 1px inset #ccc;
}
	/* .modal{
		width:30% !important;
	} */
	
	.dateDimension {
	display: table-cell;
	vertical-align: middle;
}

.dateDimension p {
	margin: 0 !important;
}

.dateBox {
	display: table-cell;
	vertical-align: middle;
}
.sliderBtnsDiv{
	display:none;
}

	#emiCalculatorModal{
	 width:450px;
    height:550px;
    padding:20px; 
    -webkit-border-radius:8px;
    -moz-border-radius:8px;
    border-radius:8px;
	}
	
	#tdsCalculatorModal{
	 width:450px;
    height:400px;
    padding:20px; 
    -webkit-border-radius:8px;
    -moz-border-radius:8px;
    border-radius:8px;
	}
	.contentData{
		display:none;
	}
	span.underline {
                font-size: 20px;
                border-bottom: 3px solid;
	}
	@page {
  margin-top:0 !important;
}
@media print {

	body * {
		visibility: hidden;
	}
	 .contentData, .contentData * {
		visibility: visible;
	}
	
	
	
	.contentData div.container1{ 
		page-break-after: always;
		
		
	}
	
	.contentData div.container1:last-child{ 
		page-break-after: avoid;
	}
	

		#printBtn,.sliderBtnsDiv,#addImage,.chqImg {
		visibility: hidden;
	}
 	 .contentData {
   	position: relative !important;
   	margin:0 !important
    left: 0 !important;
    top: 0 !important;
  }  
}
</style>
</head>

<body >

		<div class="contentData" id="contentData" >
			
		</div>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        	 <!-- <button class="btn waves-effect waves-light blue-gradient" id="test">Text</button> -->
           
                <div class="row  z-depth-3" id="chequeFormId">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> EMI Payment Details </h4>
                    </div>
                    
                    <div class="row">
                      <div class="col l2 m12 s12 push-l1 center-align">
                       <span id="openEMICalculator" class="btn blue-gradient" onclick="openEMICalculator();">EMI Calulator</span>
                    </div>
                    
                     <div class="col l2 m12 s6  push-l1 center-align">
                       <span  id="openTDSCalculator" class="btn blue-gradient" onclick="openTDSCalculator();">TDS Calulator</span>
                    </div>
                    </div>
                    
                    
                     <div class="row">
                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
                      
	                        <i class="material-icons prefix">star</i>
	                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Bank Account </span>
	                        <select name="bankAccountId" id="bankAccountId" class="validate" required="" aria-required="true" title="Please select Bank Account">
	                                 <option value="" selected disabled>Bank Account</option>
	                                <c:if test="${not empty bankAccountList}">
								<c:forEach var="listValue" items="${bankAccountList}">
									<option value="<c:out value="${listValue.id}" />"><c:out
											value="${listValue.bankShortName}${listValue.accountNumber}" /></option>
								</c:forEach>
							</c:if>
	                        </select>
                    </div>
                    
	                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">edit</i>
	                        <input id="emiPurpose" type="text" class="validate" name="emiPurpose" required>
	                        <label for="emiPurpose" class="active"><span class="red-text">*&nbsp;</span>Purpose of EMI Payment</label>
	
	                    </div>
                    </div>
                    
                      <div class="row">
                      		 <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                       <i class="fa fa-inr prefix"></i>
		                        <input id="installmentAmt" type="text" class="validate" name="installmentAmt" required>
		                        <label for="installmentAmt" class="active"><span class="red-text">*&nbsp;</span>Installment Amount</label>
	
	                    	</div>
	                    	
	                    	<div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <i class="material-icons prefix">rate_review</i>
		                        <input id="noOfCheque" type="text" class="validate" name="noOfCheque" required>
		                        <label for="noOfCheque" class="active"><span class="red-text">*&nbsp;</span>Enter No. of Cheque</label>
	
	                    	</div>
                     </div>
              
                
                  <div class="row">
                      		 <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <i class="material-icons prefix">date_range</i>
		                        <input id="startDate" type="text" class="datepicker validate" name="startDate" required>
		                        <label for="startDate" class="active"><span class="red-text">*&nbsp;</span>Start date</label>
	
	                    	</div>
	                    	
	                    	<div class="input-field col s12 m5 l5 push-l1 push-m1">
                      
		                        <i class="material-icons prefix">av_timer</i>
		                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Time Interval </span>
		                        <select name="time_interval" id="timeIntervalId" class="validate" required="" aria-required="true" title="Please select Bank Account">
		                                 <option value="" selected disabled>Select time interval</option>
		                                <option value="Monthly">Monthly</option>
		                                <option value="BiMonthly">Bi-Monthly</option>
		                                <option value="Quarterly">Quarterly</option>
		                        </select>
                    		</div>
						        
	                    	
	                    	<!-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <i class="material-icons prefix">store</i>
		                        <input id="interval" type="text" class="validate" name="interval" required>
		                        <label for="interval" class="active"><span class="red-text">*&nbsp;</span>Interval(Monthly)</label>
	
	                    	</div> -->
	                    	
	                    	<!-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
                      
		                        <i class="material-icons prefix">work</i>
		                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Time Interval </span>
		                       <select id="intervalTime" class="browser-default">
														<option value="Current">Monthly</option>
														<option value="Savings">Bi-Monthly</option>
														<option value="Savings">Quaterly</option>
								</select>
                   		 </div> -->
                    
                 
                     </div>
                     <div class="row">
                      		 <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                        <i class="material-icons prefix">person</i>
		                        <input id="payeeName" type="text" class="validate" name="payeeName" required>
		                        <label for="payeeName" class="active"><span class="red-text">*&nbsp;</span>Enter Payee Name</label>
	
	                    	</div>
	                    	
	      						<div class="col s12 m12 l5 push-l1 push-m1" style="margin-left:1%;margin-top:4%;">

											<input id="acPayee" type="checkbox" class="validate" name="acPayee" checked required>
											<label for="acPayee" class="active"><span class="red-text">*&nbsp;</span>A/c Payee</label>
								</div>
                     </div>
                     <br>
                    
                      <div class="input-field col s12 center-align" style="margin-bottom:2%;">
                    		<button class="btn waves-effect waves-light blue-gradient" onclick="chequeFormHide()"type="button" id="nextEmiDetails">Next<i class="material-icons right">send</i> </button>
               		 </div>
               		 <br>
                     
                 </div>
                 
                 </div>
                 <!-- Bulk cheque Data set  -->
                 
		                 <div class="row" id="bulkEMIData" style="display:none">
				
				<div class="col s6 l3 m3 left left-align actionDiv filerMargin">
					<select id="chequeBookId" name="chequeBookId" class="btnSelect">
						<option value="0">Select Cheque Book</option>
					</select>
				</div>
		
		
				<br>
			
				<div class="col s12 l12 m12 ">
					<table class="striped highlight bordered centered " id="tblData"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="print-col hideOnSmall">Sr. No</th>
								<th class="print-col">Cheque No.</th>
								<th class="print-col">Payee Name</th>
								<th class="print-col">Cheque Amount</th>
								<th class="print-col hideOnSmall">ChequeDate</th>
								<!-- <th class="print-col">A/C Payee</th> -->
								<th class="print-col">A/C Payee <input type="checkbox"
									class="filled-in checkAll" id="checkAll" /> <label for="checkAll"></label>
								</th>
								
							</tr>
						</thead>
		
						<tbody id="chequeNumberPrintData">
							<!-- <tr>
													<td></td>
													<td></td>
													<td><input type="text" id="payee_name" /></td>
													<td><input type="text" id="chq_Amt" /></td>
													<td><input type="date" id="chq_date" /></td>
													<td> <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
		                           					 <label for="checkAll"></label></td>
													<td><input type="text" id="remark" /></td>
													<td><Input type="button"  value="Print" id="chk_all" /></td>
												</tr> -->
						</tbody>
					</table>
				</div>
				<div class="col s12 l12 m12 ">
					<button class="btn" id="backBtn" center onclick="backClick()">Back</button>
					<button class="btn" id="printBtnMain" center onclick="printData()">Print</button>
					<button class="btn" onclick="showPreview()">Preview</button>
				</div>
			</div>
                 
                 <!-- Bulk Cheque Data set end -->
                     
                



               
                <br>
            <form action="saveBusiness" method="post" id="saveBusinessForm">
            </form>

        </div>
        <!-- PreView Model Start  --> <!-- Modal Structure for Preview -->
	<form
		action="${pageContext.request.contextPath}/savePrintedChequeFromCB"
		method="post" class="col s12 l12 m12" id="savePrintModal1">
		<div id="chqPreviewModal" class="modal">
			<div class="modal-content">
				
				
			</div>

			<div class="modal-footer row">
				<div class="col s12 l12 m12  center">
					<button id="printBtn" type="button" class="btn">Print</button>
				</div>

				<div class="row sliderBtnsDiv">
					<div class="col s12 l12 m12  center">
					<button id="previousPreviewtBtn" type="button" class="btn">
						<i class="material-icons right">chevron_left</i>
					</button>
				
					<button id="nextPreviewtBtn" type="button" class="btn">
						<i class="material-icons right">chevron_right</i>
					</button>
					</div>
				
				</div>

			</div>





		</div>
	</form>
<!--PreView Model End  --> <!-- msg Model -->
	<div class="row">
		<div class="col s12 m12 l12">
			<div id="addeditmsg" class="modal">
				<div class="modal-content" style="padding: 0">
					<div class="center  white-text" id="modalType"
						style="padding: 3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
					<h6 id="msg" class="center"></h6>
				</div>
				<div class="modal-footer">
					<div class="col s12 center">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>

				</div>
			</div>
		</div>
	</div>


<!-- EMI Calulator ModaL Start -->
<div id="emiCalculatorModal" class="modal">

			
			<div class="center">
				<span class="underline ">EMI Calculator:</span>
			</div>

	                      <div class="input-field col s12">
	                      	<i class="fa fa-inr prefix"></i>
	                        <input id="loanAmt" type="text" class="validate currencyinput" name="loanAmt" required maxlength="10">
	                        <label for="loanAmt" class="active"><span class="red-text">*&nbsp;</span>Enter Loan Amount</label>
	                     </div>
	                     
	                     <div class="input-field col s12">
	                     <i class="fa fa-percent prefix"></i>
	                        <input id="interestRate" type="text" class="validate" name="interestRate" required maxlength="3">
	                        <label for="interestRate" class="active"><span class="red-text">*&nbsp;</span>Enter Rate of Interest  (per annum)</label>
	                     </div>
	                     	
	                 	<div class="input-field col s12">
	                 	<i class="material-icons prefix">event</i>
	                        <input id="timePeriod" type="text" class="validate" name="timePeriod" required maxlength="4">
	                        <label for="timePeriod" class="active"><span class="red-text">*&nbsp;</span>Enter TIme Period (In Months only)</label>
	                     </div>
	                     
	                     <br>
	                     <br>
	                     <div class="center">
	                     
	                     <Button class="btn" type="button" onclick="calculateEMI();">Calculate</Button>
						 <Button class="btn" type="button" onclick="clearFunction();">Clear</Button>
	                     </div>
	                     
	                     <br>
	                     <br>
	                     <div id="emiCalculatedDiv">
		                      <div> <span>Monthly EMI Amount:</span><span id="emiAmt"></span></div><br>
		                      <div> <span>Total Interest Amount:</span><span id="totalInterestAmt"></span></div><br>
		                      <div> <span>Total Payable Amount:</span><span id="totalPayableAmt"></span></div>
		                      
	                     </div>

</div>

<!-- EMI Calulator Modal End -->


<!-- TDS Calulator modal start -->

<div id="tdsCalculatorModal" class="modal">

			
			<div class="center">
				<span class="underline ">TDS Calculator:</span>
			</div>

	                      <div class="input-field col s12">
	                      	<i class="fa fa-inr prefix"></i>
	                        <input id="salaryAmt" type="text" class="validate currencyinput" name="salaryAmt" required maxlength="10">
	                        <label for="salaryAmt" class="active"><span class="red-text">*&nbsp;</span>Enter Salary Amount</label>
	                     </div>
	                     
	                     <div class="input-field col s12">
	                     <i class="fa fa-percent prefix"></i>
	                        <input id="percentageSlab" type="text" class="validate" name="percentageSlab" required maxlength="3">
	                        <label for="percentageSlab" class="active"><span class="red-text">*&nbsp;</span>Enter Percentage Slab (in % only)</label>
	                     </div>
	                   
	                     <br>
	                     <br>
	                     <div class="center">
	                     <Button class="btn" type="button" onclick="calculateTDS();">Calculate</Button>
						 <Button class="btn" type="button" onclick="clearFunction();">Clear</Button>
	                     </div>
	                     
	                     <br>
	                     <br>
	                     <div id="tdsCalculatedDiv">
		                      <div> <span>TDS Amount:</span><span id="tdsAmt"></span></div><br>
		                      
	                     </div>

</div>

<!-- TDS Calulator modal end -->
    </main>
    <!--content end-->
</body>

</html>