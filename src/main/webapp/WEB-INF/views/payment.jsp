<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
      <script>
      	    $(document).ready(function() {
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0},
	   	                      { 'width': '3%', 'targets': 1},
	   	                      { 'width': '25%', 'targets': 2},
	   	                 	  { 'width': '8%', 'targets': 3},
	   	               		  { 'width': '8%', 'targets': 4},
	   	                      { 'width': '8%', 'targets': 5},
	   	                      { 'width': '8%', 'targets': 6},
	   	                   { 'width': '8%', 'targets': 7}
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [10, 25., 50, -1],
	   	             ['10 ', '25 ', '50 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	 doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [40,70,'*',70,70,70] 
		                    		 } 
		                    		    })
	   	                            /* var tableNode;
	   	                         for (i = 0; i < doc.content.length; ++i) {
	   	                           if(doc.content[i].table !== undefined){
	   	                             tableNode = doc.content[i];
	   	                             break;
	   	                           }
	   	                         }
	   	        
	   	                      var rowIndex = 0;
	   	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	   	                          
	   	                         if(tableColumnCount > 6){
	   	                           doc.pageOrientation = 'landscape';
	   	                         } */
	   	                         /*for customize the pdf content*/ 
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                      doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select").change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
           //$(".showQuantity").hide();
          } );
      	  </script>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
 
    <div class="row">
    <div class="col s12 m12 l12">
            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Id</th>
                        <th class="print-col"> Name</th>
                        <th class="print-col">Total Amount</th>
                        <th class="print-col">Amount Paid</th>
                        <th class="print-col">Amount Remaining</th>
                        <th>Make Payment</th>
                        <th>View </th>
                    </tr>
                </thead>

                <tbody>
                <c:if test="${not empty paymentPendingLists}">
				<c:forEach var="listValue" items="${paymentPendingLists}">
			   <!-- <script type="text/javascript">
						alert("${listValue.name}");					
					</script> -->
					<c:choose>  
					    <c:when test="${listValue.type == 'Supplier'}">  
						       <tr>
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td>		                        
			                        <c:choose>
		                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
		                            		<a class="orange-text text-lighten-1 tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=${listValue.genId}"><c:out value="${listValue.genId}" /></a>
		                            	</c:when>
		                            	<c:otherwise>
		 	                            	<c:out value="${listValue.genId}" />
		                            	</c:otherwise>
		                            </c:choose>
		                        </td>
		                        <td class="wrapok"><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td> <a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Make payment" href="${pageContext.request.contextPath}/fetchInventoryReportView?range=viewAll&supplierId=${listValue.genId}" >
                        			Pay
                        		</a></td>
		                        <td><a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Payment Info" href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?supplierId=${listValue.genId}&range=viewAll">Order Details</a></td>
		                       </tr> 
					    </c:when>  
					    <c:when test="${listValue.type == 'GateKeeper'}">  
					           <tr>
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td>
								<c:choose>
	                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
	                            		<a class="light-green-text text-darken-1 tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a>
	                            	</c:when>
	                            	<c:otherwise>
	 	                            	<c:out value="${listValue.genId}" />
	                            	</c:otherwise>
	                            </c:choose>
								</td>
		                        <td class="wrapok"><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td> <a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Make payment" href="${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId=${listValue.id}" >
                        			Pay
                        		</a></td>
		                        <td><a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Payment Info" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>  
					    <c:when test="${listValue.type == 'SalesMan'}">  
					           <tr>
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td>
								<c:choose>
	                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
	                            		<a class="light-green-text text-darken-1 tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a>
	                            	</c:when>
	                            	<c:otherwise>
	 	                            	<c:out value="${listValue.genId}" />
	                            	</c:otherwise>
	                            </c:choose>
								</td>
		                        <td class="wrapok"><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td> <a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Make payment" href="${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId=${listValue.id}" >
                        			Pay
                        		</a></td>
		                        <td><a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Payment Info" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>  
					    <c:when test="${listValue.type == 'DeliveryBoy'}">  
					           <tr>
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td>
								<c:choose>
	                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
	                            		<a class="light-green-text text-darken-1 tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a>
	                            	</c:when>
	                            	<c:otherwise>
	 	                            	<c:out value="${listValue.genId}" />
	                            	</c:otherwise>
	                            </c:choose>
								</td>
		                        <td class="wrapok"><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td> <a type="button" class="btn-flat tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Make payment" href="${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId=${listValue.id}" >
                        			Pay
                        		</a></td>
		                        <td><a class="tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="View Payment Info" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>   
					</c:choose>  

				</c:forEach>
				</c:if>
                </tbody>
            </table>
</div>
    </div>

        
    </main>
    <!--content end-->
</body>

</html>