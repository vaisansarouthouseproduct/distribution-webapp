<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

			<%@ page import="java.util.*"%>
				<%@ page import="java.text.*"%>
					<html>

					<head>
						<%@include file="components/header_imports.jsp"%>

							<style>
								.containerOfBanking {
									margin: 0 auto;
									width: 95% !important;
								}

								th,
								td {
									padding: 8px 15px;
								}

								.addMarginRight {
									margin: 5% 5%;
								}

								input {
									height: 2rem !important;
									margin: -5px 0 0px 0 !important;
									border-bottom: 1px solid white !important;
								}
								input[type=text]:not(.browser-default)[readonly="readonly"]{
								color : black !important;
								
								}
								table ,th ,td{
								border : 1px solid black;
								}
								@media only screen and (max-width:600px){
									.leftAlignSmall{
										text-align:left !important;
										margin-bottom:20px;
									}
								}
							</style>


					</head>

					<body>
						<%@include file="components/navbar.jsp" %>
						 <main class="paddingBody">
        				<br>

							<div class="row">
								<div class="col s12 m4 l4 right-align leftAlignSmall">
									<a href=${pageContext.request.contextPath}/addBankAccount type="button" class="waves-effect waves-light btn" style="padding: 0 1.5rem !important;"> <i
										class="material-icons left">add</i> Add New Bank Account
									</a>
								</div>
								
								<c:if test='${not empty bankAccounts}'>
									<div class="col s12 m4 l4 center leftAlignSmall" >
										<a href=${pageContext.request.contextPath}/addTransaction class="waves-effect waves-light btn hoverable"><i
											class="material-icons left">add</i> Add Transaction</a> 
											</div>
											<div class="col s12 m4 l4 left">
											<a href=${pageContext.request.contextPath}/editBankAccount class="waves-effect waves-light btn  hoverable"><i
											class="material-icons left">create</i> Edit Bank Account</a>
									</div>
									
								</c:if>
								
							</div>
							<div class="row noMarginBottom center">
								<div class="col s12 m12 l8 offset-l2">
								
									<table>
										<tr>
											<th>Select Account</th>
											<td><select id="accountNumberSelect" class="browser-default" onchange="sendSelectedAccount();">
                            <c:forEach var="bankAccount" items="${bankAccounts}" varStatus="status">
                            	<option value="${bankAccount.id}">${bankAccount.bankName} - ${bankAccount.accountNumber}</option>           
                            </c:forEach>
							<!-- <option value="" disabled selected>Choose your option</option>
							<option value="1">Option 1</option>
							<option value="2">Option 2</option>
							<option value="3">Option 3</option> -->
					</select></td>
										</tr>
										<tr>
											<th>Account Holder Name</th>
											<td><input placeholder="Account Holder Name" id="accountHolderName" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>Account Type</th>
											<td><input placeholder="Account Type" id="accountType" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>Actual Balance</th>
											<td><input placeholder="Actual Balance" id="actualBalance" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>Drawable Balance</th>
											<td class="center" id="drawableBalance"></td>
										</tr>
										<tr>
											<th>Cheque Issued Amount</th>
											<td class="center" id="chequeIssuedAmount"></td>
										</tr>
										<tr>
											<th>Cheque Deposited Amount</th>
											<td class="center" id="chequeDepositedAmount"></td>
										</tr>
										<tr>
											<th>Bank Name</th>
											<td><input placeholder="Bank Name" id="bankName" type="text" class="validate center"></td>
										</tr>
										<tr>
											<th>IFSC Code</th>
											<td><input placeholder="IFSC Code" id="ifscCode" type="text" class="validate center"></td>
										</tr>
									</table>
								</div>
								
								<c:if test='${not empty bankAccounts}'>
								
									<div class="col s12 m12 l12">
										<a href=${pageContext.request.contextPath}/accountDetails> See Mini statement </a>
									</div>
								</c:if>
							</div>
						</main>
						<%@include file="components/footer.jsp"%>

							<script>
								var selectedAccountNumber, response;
								$(document).ready(function () {
									sendSelectedAccount();
								});

								function sendSelectedAccount() {
									selectedAccountId = $("#accountNumberSelect option:selected").val();
									if(selectedAccountId=="" || selectedAccountId==null || selectedAccountId==undefined){
										return false;
									}
									var FetchAccountInfoRequest = {
										id: selectedAccountId
									}
									var UpdateSelectedBankAccountRequest = {
											bankAccountId : selectedAccountId
									}
									var FetchAccountInfoRequest = {
											id: selectedAccountId
										}
									$.ajax({
										type: 'POST',
										url: "${pageContext.request.contextPath}/fetchAccountInfo",
										headers: {
											'Content-Type': 'application/json'
										},
										data: JSON.stringify(FetchAccountInfoRequest),
										success: function (resultData) {
											if (resultData.success == true) {
												response = (resultData);
												console.log(resultData);
												setData(resultData);
											}
										},
										error: function (err) {
											/*Show Dialog for Server Error*/
											Materialize.Toast.removeAll();
										Materialize.toast('Internal Server Error', '3000', 'toastError');
										}
									});

									$.ajax({
										type: 'POST',
										url: "${pageContext.request.contextPath}/updateSelectedBankAccount",
										headers: {
											'Content-Type': 'application/json'
										},
										data: JSON.stringify(UpdateSelectedBankAccountRequest),
										success: function (resultData1) {
										},
										error: function (err) {
											/*Show Dialog for Server Error*/
											Materialize.Toast.removeAll();
											Materialize.toast('Internal Server Error', '3000', 'toastError');
										}
									});
								}

								/* set table data */
								function setData(result) {
									var data = result.bankAccount,
										accountHolderName = data.accountHolderName,
										accountType = data.accountType,
										actualBalance = data.actualBalance,
										drawableBalance = data.drawableBalance,
										chequeDepositedAmount = data.chequeDepositedAmount,
										chequeIssuedAmount = data.chequeIssuedAmount,
										bankName = data.bankName,
										ifscCode = data.ifscCode;

									$("#accountHolderName").val(accountHolderName);
									$("#accountHolderName,#accountType,#actualBalance,#bankName,#ifscCode").attr("readonly", true);

									$("#accountType").val(accountType);
									$("#actualBalance").val(actualBalance);
									$("#drawableBalance").text(drawableBalance);
									$("#chequeIssuedAmount").text(chequeIssuedAmount);
									$("#chequeDepositedAmount").text(chequeDepositedAmount);
									$("#bankName").val(bankName);
									$("#ifscCode").val(ifscCode);

								}
								
								
							</script>
					</body>

					</html>