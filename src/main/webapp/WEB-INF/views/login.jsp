<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <title>VSS Distribution</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="resources/img/favicon_vaisansar.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Compiled and minified JavaScript -->
    <link rel="stylesheet"  href="resources/css/materialize.min.css">
    <link rel="stylesheet" href="resources/css/font-awesome.min.css">
   <script	src="resources/js/jquery.min.js"></script>
<script src="resources/js/materialize.min.js"></script>
    <link href="resources/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/script.js"></script>
    <link href="resources/css/materialicon.css"	rel="stylesheet">
    
    <!-- scripts -->
   <!--   <link href="resources/css/particle.css"	rel="stylesheet"> -->



    <script>
        $(document).ready(function() {
        	$('#userId').focus();
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
           
            var msg="${validateMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
				 $('#loginError').html('<font color="red" size="4">'+msg+'</font>');
			 }
           /* VALIDATION OF USER ID AND PASSWORD */
            $('#loginValidateButton').click(function(){
            	
            	var user=$('#userId').val().trim();
            	var pass=$('#password').val().trim();
            	
            	if(user==="" && pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name and Password</font>');
            		return false;
            	}
            	else if(user==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name</font>');
            		return false;
            	}
            	else if(pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill Password</font>');
            		return false;
            	}
            	$('#loginError').html('');
          	 	/*$('#loginError').html('<font color="red" size="4"><b>Wait..</b></font>');
        		$.ajax({
        			type:"GET",
					url : "${pageContext.request.contextPath}/loginAdmin?userId="+user+"&password="+pass,
					dataType : "json",
					success : function(data) {
						//alert(data.status);
						if(data.status==="Success")
						{
							$('#loginError').html('<font color="green" size="4"><b>Login SuccessFully</b></font>');
							window.location.href="${pageContext.request.contextPath}/";
							//location.reload();
						}
						else
						{
							$('#loginError').html('<font color="red" size="4">Login failed</font>');
						}
					}
				});	 */
            });
            
        });
    </script>
    <style>
 		.background {
            background: grey;
            position: relative;
            height: 100vh;
            width: 100vw;
        }
        
        .img {
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            background: url('resources/img/loginBack.jpg') no-repeat;
            background-size: cover;
            -webkit-filter: blur(2px);
            -moz-filter: blur(2px);
            -ms-filter: blur(2px);
            filter: blur(2px);
        }
        
        .img:before {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-image: linear-gradient(to bottom right, #002f4b, #dc4225);
            opacity: .6;
        }
        
        .overlay {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgba(0, 0, 0, 0.5)), color-stop(1, rgba(0, 0, 0, 0.2)));
            /* Opera 11.1 - 12 */
            background-image: -o-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Firefox 3.6 - 15 */
            background-image: -moz-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Safari 5.1, iOS 5.0-6.1, Chrome 10-25, Android 4.0-4.3 */
            background-image: -webkit-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* IE */
            background-image: -ms-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Opera 15+, Chrome 25+, IE 10+, Firefox 16+, Safari 6.1+, iOS 7+, Android 4.4+ */
            background-image: linear-gradient(to bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
        }
        
        .container1 {
            /*background-color: #303641 !important;
            opacity: 0.2;*/
            width: 400px;
        }
        
        .container .row {
            margin-left: auto;
            margin-right: auto;
            border-radius: 5px;
        }
        /*.btn {
            background-color: transparent !important;
        }*/
        
        input[type=text],
        input[type=password] {
            /* border:1px solid #9e9e9e !important; */
            /* border-radius:4px !important; */
            height: 2rem !important;
            border-bottom: 1px solid #bdbdbd !important;
            color: #bdbdbd !important;
        }
        
        input[type=text]:focus,
        input[type=password]:focus {
            border-bottom: 1px solid #bdbdbd !important;
            box-shadow: none !important;
            color: #bdbdbd !important;
        }
        
         ::-webkit-input-placeholder {
            /* WebKit browsers */
            color: #bdbdbd;
            opacity: 1 !important;
        }
        
        button.btn-floating {
            border: 1px #bdbdbd solid;
        }
        
        .footer {
            position: relative;
            /*   float:center !important; */
            /* bottom:10%; */
            top: 20%;
            text-align: center;
            /* left:30vw;  */
            margin-left: 3%;
        }
        .container{
            transform: translate(20px, 50px);
        }
        @media only screen and (max-width: 600px) {
            .container{
            transform: translate(0, 0);
        }
        .footer {
            top: 7%;
        }
        }
        @media only screen and (min-width: 601px) and (max-width: 992px) {
              .container{
             transform: translate(0px, 120px); 
        }
        }
    </style>

    <body>
    <!-- count particles -->
	<!-- <div class="count-particles">
	  <span class="js-count-particles">--</span>
	</div> -->
<!-- particles.js container -->

	<!-- <div id="particles-js"></div> -->
	<div class="background">
        <div class="img"></div>
        <div class="overlay"></div>
        <div class="container" style="">
            <form action="${pageContext.request.contextPath}/loginEmployee" method="post">
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2  transparent">
                        <!-- <div class="card-content black-text"> -->
                            <!-- <div class="col s12 m12 l12">
                            <h2 class="card-title center teal-text" style="font-family: Serif;">Login</h2>
                        </div> -->
                           <!--  <div class="col l6 m6">
                                <div class="col s6 m7 l12">
                                    <img src="resources/img/bluelogo.jpg" class="responsive-img" style="margin-top:9%;border-right:2px solid teal;">
                                    
                                </div>
                            </div>  -->
                            
                            <div class="col s12 l8 m10 offset-l2 offset-m1">
                                <br><br>
                                <h1 class="center grey-text text-lighten-1" style="margin-bottom:0"><i class="material-icons" style="font-size:40px;">account_circle</i></h1>
                                  <h1 class="center grey-text text-lighten-1" style="font-size:20px;margin-top:10px">Dashboard Login</h1>
                               
                               
                                <div class="row" style="margin-bottom:4%">
                                    <div class="col s12 l8 m8 offset-l2 offset-m2">
                                        <!-- <label for="userId" class="black-text">User Name</label> -->
                                        <!-- <i class="material-icons prefix">account_circle</i> --> 
                                        <input id="userId" name="userId" placeholder="User Name" type="text" required class="border" data-error="wrong" data-success="right">
                                        
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom:0">
                                    <div class="col s12 l8 m8 offset-l2 offset-m2">
                                    <!-- <label for="password" class="black-text">Password</label> -->
                                        <!-- <i class="material-icons prefix">lock</i> -->
                                         <input id="password" placeholder="Password" name="password" type="password" class="border" required>
                                        
                                    </div>
                                    <!-- <div class="col s2 m2 l1" style="transform: translate(-45px, 30px);">
                                        <i class="fa fa-eye fa-lg eye" aria-hidden="true"></i>
                                        <i class="fa fa-eye-slash fa-lg eye-slash" aria-hidden="true"></i>
                                    </div> -->
                                   	<div class="input-field col s10 l8 m12  offset-l2 center" style="margin-bottom:2%">                                   
                                   		<span id="loginError"></span>
                                   	</div>
                                   	
                                </div>
                                <div class="col s12 l12 m12  center">
                              <button class="btn-floating btn-medium waves-effect waves-light transparent grey-text text-lighten-1 center" type="submit" id="loginValidateButton" style="margin-left:2%;">
							 <i class="material-icons small center grey-text text-lighten-1">arrow_forward</i>
							</button>
							 <br class="hide-on-small-only"><br>
                                </div>
                               
                            </div>
                         
                        </div>
                    <!-- </div> -->
                </div>
            </form>
            </div>
        
        <div class="footer">
             <h6 class="grey-text text-lighten-1"> &#169; 2018 Designed & Developed By Vaisansar Technologies Pvt. Ltd </h6>            
        </div>
 	</div>
    </body>
	
</html>