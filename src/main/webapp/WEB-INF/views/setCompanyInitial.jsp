<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
	
	$('#companyList').change(function(){
		var companyId=$('#companyList').val();
		// Get the raw DOM object for the select box
		if(${sessionScope.showNavbar==true}){
			var branchIdselect = document.getElementById('branchId_Nav');
			// Clear the old options
			branchIdselect.options.length = 0;
			branchIdselect.options.add(new Option('Choose Branch', '-1'));
			
		}else{
			var branchIdselect2 = document.getElementById('branchId');
			// Clear the old options
			branchIdselect2.options.length = 0;
			branchIdselect2.options.add(new Option('Choose Branch', '-1'));
		}
		if(companyId==-1){
			return false;
		}
		$.ajax({
			type:'GET',
			url: myContextPath+"/fetch_branch_list_by_companyId/"+companyId,
			headers : {
				'Content-Type' : 'application/json'
			},
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },	           
			async: false,
			success : function(response){
				if(response.status=="Success"){
					
					//setting employee list
					var branchList=response.branchList;
					if(branchList!=null){
						var options, index, option;
						if(${sessionScope.showNavbar==true}){
							employeeIdselect = document.getElementById('branchId_Nav');
							for (var i = 0, len = branchList.length; i < len; ++i) {
								var branch = branchList[i];
								employeeIdselect.options.add(new Option(branch.name, branch.branchId));
							}
						}else{
							employeeIdselect = document.getElementById('branchId');
							for (var i = 0, len = branchList.length; i < len; ++i) {
								var branch = branchList[i];
								employeeIdselect.options.add(new Option(branch.name, branch.branchId));
							}
						}
					}
					
				}else{
					Materialize.Toast.removeAll();
					Materialize.toast(response.errorMsg, '2000', 'red lighten-2');
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error){
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.Toast.removeAll();
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
	});	
	
	<c:if test="${companyBranchSelect==true}"> 
			$('#setCompanyForm1').attr("action", myContextPath+'/setCompanyBranch');
			$('#setCompanyForm2').attr("action", myContextPath+'/setCompanyBranch');
			$('#setBranch').click(function(){
				var branchId=$('#branchId').val();
				if(branchId=="-1"){
					Materialize.Toast.removeAll();
					Materialize.toast('Select Branch', '2000', 'red lighten-2');
					return false;
				}
			});
	</c:if>
	$('#setBranch').on('click',function(){
		var branchId='${sessionScope.showNavbar==true?"branchId_Nav":"branchId"}';
		<c:if test="${companyBranchSelect==false}"> 		
		var companyId=$('#companyList').val();
		if(companyId!=-1 || companyId==undefined){
		</c:if>
			var branchId=$('#'+branchId).val();
			if(branchId==-1){
				Materialize.Toast.removeAll();
				Materialize.toast("Select Branch", '2000', 'teal lighten-2');
				return false;
			} 
		<c:if test="${companyBranchSelect==false}"> 
		} 
		</c:if>
		
	});
});

</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	
i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	.background {
            background: grey;
            position: relative;
            height: 100vh;
            width: 100vw;
        }
        
        .img {
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            background: url('resources/img/loginBack.jpg') no-repeat;
            background-size: cover;
            -webkit-filter: blur(2px);
            -moz-filter: blur(2px);
            -ms-filter: blur(2px);
            filter: blur(2px);
        }
        
        .img:before {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-image: linear-gradient(to bottom right, #002f4b, #dc4225);
            opacity: .6;
        }
        .backgroundColor{
			background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgba(0, 0, 0, 0.5)), color-stop(1, rgba(0, 0, 0, 0.2)));
            /* Opera 11.1 - 12 */
            background-image: -o-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Firefox 3.6 - 15 */
            background-image: -moz-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Safari 5.1, iOS 5.0-6.1, Chrome 10-25, Android 4.0-4.3 */
            background-image: -webkit-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* IE */
            background-image: -ms-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Opera 15+, Chrome 25+, IE 10+, Firefox 16+, Safari 6.1+, iOS 7+, Android 4.4+ */
            background-image: linear-gradient(to bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
		}
        .overlay {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgba(0, 0, 0, 0.5)), color-stop(1, rgba(0, 0, 0, 0.2)));
            /* Opera 11.1 - 12 */
            background-image: -o-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Firefox 3.6 - 15 */
            background-image: -moz-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Safari 5.1, iOS 5.0-6.1, Chrome 10-25, Android 4.0-4.3 */
            background-image: -webkit-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* IE */
            background-image: -ms-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Opera 15+, Chrome 25+, IE 10+, Firefox 16+, Safari 6.1+, iOS 7+, Android 4.4+ */
            background-image: linear-gradient(to bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
        }
        .paddingBody1 input.select-dropdown{
        	color:#bdbdbd;
        }
        .paddingBody1 .select-wrapper span.caret{
        	color:#bdbdbd !important;
        } 
        .select2-container--default .select2-selection--single .select2-selection__rendered{
			color:#bdbdbd !important;
		}
		.whiteBack .select2-container--default .select2-selection--single .select2-selection__rendered{
			color:black !important;
		}
        .containerDiv{
        	transform: translate(20px, 50px);
        }
		span.selectLabel {  
 			   left: 4.1rem;  
		}
        @media only screen and (max-width: 600px) {
        	.containerDiv{
        	transform: translate(0px, -30px);
        }
		span.selectLabel {  
 			   left: 4.1rem;  
		}
		.cardheading{
			font-size: 1.5rem !important;
		}
        }
        @media only screen and (min-width: 601px) and (max-width: 992px) {
        	.containerDiv{
        	transform: translate(110px, 100px);
        }
        }
</style>
</head>

<body>
	

   <!--navbar start-->
   	<c:if test="${sessionScope.showNavbar==true}">
   	 <%@include file="components/navbar.jsp" %>
   	 <main class="paddingBody">
      
  	<div class="container">
        	 <br><br><br><br class="hide-on-small-only"><br class="hide-on-small-only"><br class="hide-on-small-only">
            <form action="${pageContext.request.contextPath}/setCompanyAndBranch" method="post" id="setCompanyForm1">
                <div class="col l8 m8 s12 z-depth-3">
                    <div class="row">
                    <c:choose>
                    <c:when test="${companyBranchSelect==false}"> 
                    <div class="col l12 m12 s12">
                        <h4 class="center cardheading"> Set Company And Branch</h4>
                    </div>
                       <div class="input-field col s12 m5 l5 offset-l3 offset-m3 whiteBack" style="margin-bottom:20px">
                        <i class="material-icons prefix">account_balance</i>
                   <span class="selectLabel">   Select Company</span>
                        <select name="companyId" id="companyList">
                                 <option value="-1" selected>Choose Company</option>
                                <c:if test="${not empty companyList}">
									<c:forEach var="listValue" items="${companyList}">								
										<option value="<c:out value="${listValue.companyId}" />" ${listValue.companyId==companyId?'selected':'' }><c:out
												value="${listValue.companyName}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                       </div>
                       </c:when>
                       <c:otherwise>
                       	<div class="col l12 m12 s12">
	                        <h4 class="center cardheading"> Set Branch</h4>
	                    </div>
                       </c:otherwise>
                       </c:choose>
                       <div class="input-field col s12 m5 l5 offset-l3 offset-m3 whiteBack">
							<i class="material-icons prefix">call_split</i>	
							<span class="selectLabel">Select Branch</span>                   
	                        <select name="branchId" id="branchId_Nav">
	                                 <option value="-1" >Choose Branch</option>
	                                 <c:if test="${not empty branchList}">
										<c:forEach var="listValue" items="${branchList}">										
											<option value="<c:out value="${listValue.branchId}" />" ${listValue.branchId==branchId?'selected':'' }><c:out
													value="${listValue.name}" /></option>
										</c:forEach>
									</c:if>
	                        </select>
                       </div>
                       <div class="input-field col s12 m12 l12  center-align">
                         <br>
                    		<button class="btn waves-effect waves-light blue-gradient" type="submit" id="setBranch">Set<i class="material-icons right">send</i> </button>
                    		<br><br>                     		
                	  </div>
                 </div>
                </div>
                 <br>
              
               
              
            </form>

        </div>
		

    </main>
   	 </c:if> 
    <!--navbar end-->
    <!--content start-->
    <c:if test="${sessionScope.showNavbar==false}">
    <main class="paddingBody1">
      
        <div class="background backgroundColor">
        <div class="img"></div>
        <div class="overlay"></div>
        <div class="container containerDiv">
            <form action="${pageContext.request.contextPath}/setCompanyAndBranch" method="post" id="setCompanyForm2">
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2">
                                <br><br><br><br><br><br>                   
                            <div class="col s12 l8 m8 offset-l2 z-depth-5">
                            	<c:choose>
                                <c:when test="${companyBranchSelect==false}"> 
                               		<div class="col l12 m12 s12">
                       				 	<h4 class="center grey-text text-lighten-1 cardheading"> Set Company and Branch</h4>
                  			   		</div>   
                  			   		<div class="row" style="margin-bottom:4%;margin-top:7%">
											 <br><br>
	                                    <div class="input-field col s12 m10 l10 offset-l1 offset-m1">
	                        				<i class="material-icons prefix grey-text text-lighten-1">account_balance</i>
	                   						<span class="selectLabel grey-text text-lighten-1">   Select Company</span>
					                        <select name="companyId" id="companyList">
					                                 <option value="-1" selected>Choose Company</option>
					                                <c:if test="${not empty companyList}">
														<c:forEach var="listValue" items="${companyList}">														
															<option value="<c:out value="${listValue.companyId}" />"><c:out
																	value="${listValue.companyName}" /></option>
														</c:forEach>
													</c:if>
					                        </select>
	                       				</div>                       				
	                                </div>
                  			   	 </c:when>
                                <c:otherwise> 
                                	<div class="col l12 m12 s12">
                       				 	<h4 class="center grey-text text-lighten-1"> Set Branch</h4>
                  			   		</div> 
		                       </c:otherwise>
		                       </c:choose>              			   
	                                
                               
                                <div class="row"  style="margin-bottom:4%">
	                                <div class="input-field col s12 m10 l10 offset-l1 offset-m1">
										<i class="material-icons prefix grey-text text-lighten-1">call_split</i>	 
										<span class="selectLabel grey-text text-lighten-1">Select Branch</span>                  
				                        <select name="branchId" id="branchId" >
				                                 <option value="-1" selected>Choose Branch</option>
				                                  <c:if test="${not empty branchList}">
													<c:forEach var="listValue" items="${branchList}">													
														<option value="<c:out value="${listValue.branchId}" />"><c:out
																value="${listValue.name}" /></option>
													</c:forEach>
												</c:if>
				                        </select>
			                       </div>
		                       </div>
		                       <div class="row">
                                <div class="col s12 l12 m12   center center-align">
                             	 		<button class="btn waves-effect waves-light blue-gradient" type="submit" id="setBranch">Set<i class="material-icons right">send</i> </button>                     		
										 <br><br>
                                </div>
                               </div>
                            </div>
                         
                        </div>
                    <!-- </div> -->
                </div>
            </form>
            </div>
             		 
       		
		</div>
 </c:if> 
		

    </main>
    <!--content end-->
</body>

</html>