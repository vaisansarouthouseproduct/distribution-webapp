<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
 <%@include file="components/header_imports.jsp" %>
    

<script type="text/javascript">

$(document).ready(function() {
	var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	     /*  initComplete: function () {
	    	.on('change', function () {
          var val = $.fn.dataTable.util.escapeRegex(
          $(this).val());
	      } */
	      	autoWidth: false,
	         columnDefs: [
	                      { 'width': '1%', 'targets': 0 },
	                      { 'width': '2%', 'targets': 1},
	                      { 'width': '15%', 'targets': 2},
	                  	  { 'width': '10%', 'targets': 3},
	                  	  { 'width': '2%', 'targets': 4},
	                	  { 'width': '3%', 'targets': 5},
	                	  { 'width': '3%', 'targets': 6},
	             		  { 'width': '3%', 'targets': 7},
	          			  { 'width': '3%', 'targets': 8},
	       				  { 'width': '2%', 'targets': 9},
	    				  { 'width': '2%', 'targets': 10}  	                     
	                      ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	        
	        // dom: 'lBfrtip',
	        dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                        
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];   	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISIBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select").change(function() {
           var t = this;
           var content = $(this).siblings('ul').detach();
           setTimeout(function() {
               $(t).parent().append(content);
               $("select").material_select();
           }, 200);
       });
   $('select').material_select();
   $('.dataTables_filter input').attr("placeholder", "Search");
	
   if ($.data.isMobile) {
								var table = $('#tblData').DataTable(); // note the capital D to get the API instance
								var column = table.columns('.hideOnSmall');
								column.visible(false);
   }
	
	 $(".showDates").hide();
     $(".rangeSelect").click(function() {
    	 $("#oneDateDiv").hide();
         $(".showDates").show();
     });
	$("#oneDateDiv").hide();
	$(".pickdate").click(function(){
		 $(".showDates").hide();
		$("#oneDateDiv").show();
	});
	/* $('#areaId').change(function(){
		
		var areaId=$('#areaId').val();	
		if(areaId==="0")
		{
			return false;
		}
		
		var url = "${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?areaId="+areaId;
		window.location.href=url;
		
	}); */
	//reset table data according area
	var table = $('#tblData').DataTable();	
	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
		       	        
		        var areaNameTb=data[3].trim();
		        var areaId=$('#areaId').val();
				var areaName=$('#areaId option:selected').text();
				//alert(salesManId+"-"+salesManId2);
				if(areaId==="0")
				{
					return true;	    					
				}
				
		        if ( areaName === areaNameTb )
		        {
		            return true;
		        }
		        return false;
		        
	});
	
	$('#areaId').change(function(){
		table.draw();
	});
	
	
	/* //filter in databale
	var table = $('#tblData').DataTable();	
	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
		       	        
		        var id=data[9];
				var idName=id.trim();
				var typeId=$('#orderStatusId').val();
				
				if(typeId==="0")
				{
					return true;
				}
				
		        if ( idName === typeId )
		        {
		            return true;
		        }
		        return false;
		        
	});
	
	$('#orderStatusId').change(function(){
		//when i select any type then its show changes in datable depend on which type select
		table.draw();
	}); */
	
	/* $('#searchButtonId').click(function(){
		
		var empId=$('#salesmanId').val();
		if(empId==="0")
		{
			$('#addeditmsg').modal('open');
		     $('#msgHead').text("Filter Message");
		     $('#msg').text("Select SalesPerson For Filter");
			return false;
		}
		
	}); */
	
});

</script>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
      <div class="row noBottomMarginOnLarge"><!-- 
      	
            <div class="col s12 l2 m2">
                <select name="orderStatus" id="orderStatusId">
			      <option value="0"  selected>Order Status</option>
			      <option value="Booked">Booked</option>
			      <option value="Packed">Packed</option>
			      <option value="Issued">Issued</option>
			      <option value="Delivered">Delivered</option>
			      <option value="Delivered Pending">Delivered Pending</option>
			      <option value="Canceled">Canceled</option>
			    </select>
            </div> -->
		
            
         <!--    <div class="col s12 l2 m2">
                <select id="salesmanId" name="employeeDetailsId">
			      <option value="0"  selected>Select SalesPerson</option>      
			    </select>
            </div> -->

            <!-- <div class="col s12 l2 m2">
                <button class="btn waves-effect waves-light blue darken-8" style="margin-top:7%" id="searchButtonId">Search</button>
            </div> -->
		

            <div class="col s6 m3 l2 right  right-align" style="margin-top:0.5%;">
           <!--      <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchFilteredOrderIssueReportForWeb?range=viewAll">View All</a></li>
                    </ul>
                </div>
                <div class="col s6 l2 m4 right right-align" style="margin-top:0.5%;">
             <select id="areaId" name="areaId" class="btnSelect">
             	<option value="0">Area Filter</option>
                 <c:if test="${not empty orderProductIssueReportResponse.areaList}">
					<c:forEach var="listValue" items="${orderProductIssueReportResponse.areaList}">
						<option value="<c:out value="${listValue.areaId}" />"><c:out
								value="${listValue.name}" /></option>
					</c:forEach>
				</c:if>
				</select>
            </div>
				<div class="input-field col s12 l3 m3 center" id="oneDateDiv" style="margin-top:0;">                            	
                    <form action="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb" method="post">
	                    <div class="input-field col s6 m7 l7">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s6 m2 l2" style="margin-top:5%;">
	                     <button type="submit" class="btn">View</button>
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
               <div class="col s12 l4 m4 center">
                <form action="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">                	 
                              
                                <div class="input-field col s4 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <input type="hidden" value="range" name="range">
                                <label for="startDate">From</label>
								 </div>
								 <div class="input-field col s4 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                         		<div class="input-field col s4 m1 l1">
                            <button type="submit" class="btn">View</button>
                            </div>      
                          </span>
                </form>
            </div>
         </div>
        <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0"; width="100%">
                    <thead>
                        <tr>
                            <th class="print-col hideOnSmall">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Shop Name</th>  
                            <th class="print-col">Area</th>
                            <th class="print-col">Total Qty</th> 
                            <th class="print-col">Taxable Amount</th>  
                            <th class="print-col">Tax</th>
                            <th class="print-col">Total Amount</th>                                                     
                            <th class="print-col">Packed date</th>
                            <th class="print-col">Issue date</th>
                            <th>Get Bill</th>
                        </tr>
                        
                        
                        
                    </thead> 
                    <tbody>
                    <% int rowincrement=0; %>
                    <c:if test="${not empty orderProductIssueReportResponse.fetchOrderDetailsModelList}">
					<c:forEach var="listValue" items="${orderProductIssueReportResponse.fetchOrderDetailsModelList}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
	                        <tr>
	                            <td><c:out value="${rowincrement}" /></td>
	                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/fetchIssueProductDetailsForIssueReportByOrderIdForWeb?orderId=${listValue.orderId}"><c:out value="${listValue.orderId}" /></a></td>
	                            <td class="wrapok"><c:out value="${listValue.shopName}" /></td>
	                            <td><c:out value="${listValue.areaName}" /></td>
	                            <td><c:out value="${listValue.issuedQuantity}" /></td>
	                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.issuedTotalAmount}" />
	                            </td>
	                            <td>
	                            	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.issuedTotalAmountWithTax-listValue.issuedTotalAmount}" />
	                            </td>
	                            <td>
	                            	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.issuedTotalAmountWithTax}" />
	                            </td>
	                            <td class="wrapok">
	                            	<c:choose>
		                            	<c:when test="${listValue.orderStatus!='Booked'}">
				                            <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.packedDate}"  />
					                        <c:out value="${date}" />
				                        </c:when>
				                        <c:otherwise>
				                        	NA
				                        </c:otherwise>
			                        </c:choose>
	                            </td>
	                            <td class="wrapok">
	                            	<c:choose>
		                            	<c:when test="${listValue.orderStatus!='Packed' && listValue.orderStatus!='Booked' }">
				                            <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.issuedDate}"  />
					                        <c:out value="${date}" />
				                        </c:when>
				                        <c:otherwise>
				                        	NA
				                        </c:otherwise>
			                        </c:choose>
	                            </td>
	                             <td><a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="left" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/openIssuedBill?orderId=${listValue.orderId}"><i class="material-icons">receipt</i></a></td>
	                        </tr>
                     </c:forEach>
                     </c:if>
                    </tbody>
                </table>
            </div>
        </div>

 <div >
 	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>
 	
			

    </main>
    <!--content end-->
</body>

</html>