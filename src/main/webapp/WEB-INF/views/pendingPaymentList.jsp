<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <%@include file="components/header_imports.jsp" %>

    <script>
    var checkedId=[];
    var checkedIdPending=[];
        $(document).ready(function() {
        	
            //  for no of entries and global search
            $('#tblPartialPayment').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_",
                    "sSearch": " _INPUT_" //search
                },
                lengthMenu: [
                    [10, 25., 50, -1],
                    ['10 ', '25 ', '50 ', 'all']
                ],
                autoWidth: false,
	   	          columnDefs: [
	   	                      { 'width': '1%', 'targets': 0},
	   	                  	 { 'width': '15%', 'targets': 1},
	   	                      { 'width': '3%', 'targets': 2},
	   	                  	  { 'width': '3%', 'targets': 3},
	   	                	  { 'width': '5%', 'targets': 4},
	   	             		  { 'width': '3%', 'targets': 5},
	   	          			  { 'width': '3%', 'targets': 6},
		   	          		  { 'width': '2%', 'targets': 7},
		   	          		  { 'width': '2%', 'targets': 8}
	   	       				 
	   	                      ], 
                
                //dom: 'lBfrtip',
                dom:'<lBfr<"scrollDivTable"t>ip>',
                buttons: {
                    buttons: [
                        //      {
                        //      extend: 'pageLength',
                        //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                        //  }, 
                        {
                            extend: 'pdf',
                            className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF</span>',
                            //title of the page
                            title:'Partial Payment Report',
                           
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = $(".heading h4").text();
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: '.print-col'
                            },
                            customize: function(doc, config) {
                            	doc.content.forEach(function(item) {
     	                    		  if (item.table) {
     	                    		  item.table.widths = [20,65,65,45,40,30,30,40,40,40,45,45] 
     	                    		 } 
     	                    		    })
                              /*   var tableNode;
                                for (i = 0; i < doc.content.length; ++i) {
                                  if(doc.content[i].table !== undefined){
                                    tableNode = doc.content[i];
                                    break;
                                  }
                                }
               
                                var rowIndex = 0;
                                var tableColumnCount = tableNode.table.body[rowIndex].length;
                                 
                                if(tableColumnCount > 6){
                                  doc.pageOrientation = 'landscape';
                                } */
                                /*for customize the pdf content*/ 
                                doc.pageMargins = [5,20,10,5];                           
                                doc.defaultStyle.fontSize = 8	;
                                doc.styles.title.fontSize = 12;
                                doc.styles.tableHeader.fontSize = 11;
                                doc.styles.tableFooter.fontSize = 11;
                                doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
                              },
                        }, {
                            extend: 'excel',
                            className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL</span>',
                            //title of the page
                            title: 'Partial Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = $(".heading h4").text();
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'print',
                            className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT</span>',
                            //title of the page
                            title:'Partial Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = $(".heading h4").text();
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'colvis',
                            className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<span style="font-size:15px;">COLUMN VISIBILITY</span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                            collectionLayout: 'fixed two-column',
                            align: 'left'
                        },
                    ]
                }

            });
            $('select').material_select();
			     $("select")
			         .change(function() {
			             var t = this;
			             var content = $(this).siblings('ul').detach();
			             setTimeout(function() {
			                 $(t).parent().append(content);
			                 $("select").material_select();
			             }, 200);
			         });
			     $('.dataTables_filter input').attr("placeholder", "Search");

        });
            //payment Details start
    		// payament details list by order id (salesman or counter order id)
    		function fetchPaymentDetails(amount,orderId){
            		amount=parseFloat(amount);
            		if(amount==0){
            			var $toastContent = $('<span>Payment is not received yet !!!<span>');
						Materialize.Toast.removeAll();
						Materialize.toast($toastContent, 3000);
            			return false;
            		}
            	
            		if(orderId.includes("CORD")){
    					$.ajax({
    						type:'GET',
    						url: "${pageContext.servletContext.contextPath}/fetchPaymentCounterOrder?counterOrderId="+orderId,
    						async: false,
    						success : function(data)
    						{
    							$('#paymentDetail').empty();
    							
    							var totalAmountPaid=0;
    							for(var i=0; i<data.length; i++)	
    							{
    								totalAmountPaid+=data[i].currentAmountPaid.toFixedVSS(2)-data[i].currentAmountRefund.toFixedVSS(2)
    							}
    							
    							if(data[0].counterOrder.businessName=="" || data[0].counterOrder.businessName==undefined){
    								$('#boothNo').html("<b>Customer Name : </b>"+data[0].counterOrder.customerName);
    							}else{
    								$('#boothNo').html("<b>Booth No. : </b>"+data[0].counterOrder.businessName.businessNameId);
    							}
    							
    							$('#orderId').html("<b>Order Id. : </b>"+data[0].counterOrder.counterOrderId);
    							$('#totalAmt').html("<b>Total Amount : </b>"+data[0].counterOrder.totalAmountWithTax);
    							$('#balanceAmt').html("<b>Balance Amount : </b>"+(parseFloat(data[0].counterOrder.totalAmountWithTax)-parseFloat(totalAmountPaid)));
    							
    							var totalAmountPaid=0;
    							var srno=1;
    							$('#showPaymentCounterStatus').show();
    							for(var i=0; i<data.length; i++)	
    							{
    								var mode="";
    								if(data[i].payType==="Cash")
    								{
    									mode="Cash";
    								}
    								else if(data[i].payType==="Cheque")
    								{
    									mode=data[i].bankName+"-"+data[i].chequeNumber;
    								}else if(data[i].payType==="Wallet")
    								{
    									mode="Wallet";
    								}else{
    									mode=data[i].paymentMethodName+"-"+data[i].transactionRefNo;
    								}
    								
    								var dateFrom=new Date(parseInt(data[i].paidDate));
    								month = dateFrom.getMonth() + 1;
    								day = dateFrom.getDate();
    								year = dateFrom.getFullYear();
    								var dateFromString = day + "-" + month + "-" + year;
    								
    								if(data[i].currentAmountRefund=='' || data[i].currentAmountRefund==undefined){
    									$('#paymentDetail').append('<tr>'+
    																'<td>'+srno+'</td>'+
    																'<td>'+data[i].currentAmountPaid.toFixedVSS(2)+'</td>'+
    																'<td><font color="blue"><b>Paid</b></font></td>'+
    																'<td>'+mode+'</td>'+
    																'<td>'+data[i].employeeName+'</td>'+
    																'<td>'+dateFromString+'</td></tr>');
    									totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].currentAmountPaid.toFixedVSS(2));
    								}else{
    									$('#paymentDetail').append('<tr>'+
    											'<td>'+srno+'</td>'+
    											'<td>'+data[i].currentAmountRefund.toFixedVSS(2)+'</td>'+
    											'<td><font color="green"><b>Refund</b></font></td>'+
    											'<td>'+mode+'</td>'+
    											'<td>'+data[i].employeeName+'</td>'+
    											'<td>'+dateFromString+'</td></tr>');
    									totalAmountPaid=parseFloat(totalAmountPaid)-parseFloat(data[i].currentAmountRefund.toFixedVSS(2));
    								}
    								
    								srno++;
    							}
    							$('#totalAmountPaid').text(totalAmountPaid);
    							
    								$('#viewPaymentDetail').modal('open');
    							
    							
    						},
    						error: function(xhr, status, error) 
    						{
    							 Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
    							/* $('#addeditmsg').modal('open');
    			       	     	$('#msgHead').text("Message : ");
    			       	     	$('#msg').text("Product List Not Found"); 
    			       	     		setTimeout(function() 
    								  {
    				     					$('#addeditmsg').modal('close');
    								  }, 1000); */
    						}
    						
    					});
    			}else{
    				$('#showPaymentCounterStatus').hide();
    				$.ajax({
    					type:'GET',
    					url: "${pageContext.servletContext.contextPath}/fetchPaymentListByOrderId?orderId="+orderId,
    					async:false,
    					success : function(data)
    					{
    						$('#paymentDetail').empty();
    						
    						$('#boothNo').html("<b>Booth No. : </b>"+data[0].orderDetails.businessName.businessNameId);
    						$('#orderId').html("<b>Order Id. : </b>"+data[0].orderDetails.orderId);
    						$('#totalAmt').html("<b>Total Amount : </b>"+data[0].orderDetails.totalAmountWithTax);
    						$('#balanceAmt').html("<b>Balance Amount : </b>"+(parseFloat(data[0].dueAmount)));
    						
    						var totalAmountPaid=0;
    						var srno=1;
    						for(var i=0; i<data.length; i++)	
    						{
    							var mode="";
    							if(data[i].payType==="Cash")
    							{
    								mode="Cash";
    							}
    							else if(data[i].payType==="Cheque")
    							{
    								mode=data[i].bankName+"-"+data[i].chequeNumber;
    							}else{
    								mode=data[i].paymentMethodName+"-"+data[i].transactionRefNo;
    							}
    							
    							var dateFrom=new Date(parseInt(data[i].paidDate));
    							month = dateFrom.getMonth() + 1;
    							day = dateFrom.getDate();
    							year = dateFrom.getFullYear();
    							var dateFromString = day + "-" + month + "-" + year;
    							
    							$('#paymentDetail').append('<tr>'+
    														'<td>'+srno+'</td>'+
    														'<td>'+data[i].paidAmount.toFixedVSS(2)+'</td>'+
    														'<td><font color="blue"><b>Paid</b></font></td>'+
    														'<td>'+mode+'</td>'+
    														'<td>'+data[i].employeeName+'</td>'+
    														'<td>'+dateFromString+'</td></tr>');
    							totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].paidAmount.toFixedVSS(2));
    							srno++;
    						}
    						$('#totalAmountPaid').text(totalAmountPaid);
    						/* $('#viewPaymentDetail').modal('open'); */
    						
								$('#viewPaymentDetail').modal('open');
							
    					},
    					error: function(xhr, status, error) 
    					{
    						 Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
    						/* $('#addeditmsg').modal('open');
    		       	     	$('#msgHead').text("Message : ");
    		       	     	$('#msg').text("Product List Not Found"); 
    		       	     		setTimeout(function() 
    							  {
    			     					$('#addeditmsg').modal('close');
    							  }, 1000); */
    					}
    					
    				});
    			}	
            	
    		
    			
    		}

            //payment Details end
           
       
    
  
    </script>
</head>
<body>
<!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    
        <main class="paddingBody">
        <br>
   
   

        <!--Pending Payment List START-->
        <div id="pendingPayment">

            <div class="col s12 l8 m12 ">
                <table class="striped highlight centered" id="tblPartialPayment" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Shop Name /<br> Customer Name</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Mobile No</th>
                            <th class="print-col">City</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Amount Paid</th>
                            <th class="print-col">Balance Amount</th>
                             <th class="print-col">Estimated payment Date</th>
                            <!-- <th class="print-col">Mode</th> -->
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                    <c:if test="${not empty collectionReportPaymentDetailsList}">
					<c:forEach var="listValue" items="${collectionReportPaymentDetailsList}">					
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
					
		                        <tr>
		                            <td><c:out value="${rowincrement}"/></td>
		                            <td class="wrapok"><c:out value="${listValue.shopName}" /></td>
		                            <td><c:out value="${listValue.orderId}" /></td>
		                            <td><c:out value="${listValue.mobileNumber}" /></td>
		                            <td><c:out value="${listValue.cityName}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                            <td><button class="btn-flat" id="viewDetails" onclick="fetchPaymentDetails(${listValue.amountPaid},'${listValue.orderId}')"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></button></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}" /></td>
		                            <td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.nextDueDate}"  /><c:out value="${date}" />
			                        </td>
		                          
		
		                        </tr>
                      
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>
        <!--Pending Payment List END-->


<!-- Payment List Modal start -->
		<div class="row">
			<div class="col s12 m12 l6">
			
		<div id="viewPaymentDetail" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Payment Detail</u> <i class="modal-close material-icons right">clear</i></h5>
         

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="boothNo" class="black-text">Booth No:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="orderId" class="black-text">Order Id:</h6>
                    </div>
                
                
                    <div class="col s12 l6 m6">
                        <h6 id="totalAmt" class="black-text">Total Amount:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="balanceAmt" class="black-text">Balance Amount : </h6>
                    </div> 
                    
                </div>
               
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th class="showPaymentCounterStatus">Status</th>
                            <th>Mode</th>
                            <th>Collecting Person</th>
                            <th> Date</th>
                        </tr>
                    </thead>
                    <tbody id="paymentDetail">
                       
                    </tbody>
                    <tfoot>
                    <tr>
                    <th>Total</th>
                     <th><span id="totalAmountPaid"></span></th>
                     <th class="showPaymentCounterStatus"></th>
                     <th></th>
                     <th></th>
                     <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer row">
             <div class="col s6 m6 l3 offset-l4">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                 </div>	
            </div>
        </div>
		</div>
		</div>

<!-- Payment List Modal end -->

       
    </main>

</body>
</html>