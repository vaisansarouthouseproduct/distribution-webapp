<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    	 <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
    <script>
    /* base url */
    var myContextPath = "${pageContext.request.contextPath}";
    
   	var areaidlist = new Array();
    var count = 1;
    var userIdValid=false; 
    var employeeDetailsId="${employeeDetails.employeeDetailsId}";
    </script>
	<script type="text/javascript" src="resources/js/updateEmployee.js"></script>

    
    <script>
        $(document).ready(function() {
        	
        	/* set area ids from productidlist */
        	var prdctlst="${areaidlist}";
			//alert(prdctlst);
		    if(prdctlst!='' && prdctlst!=undefined)
			 {
		    	areaidlist=prdctlst.split(',');
		    	$('#areaListIds').val(areaidlist);
		    	//alert(productidlist);
			 }
		    var cout="${count}";
		   // alert(cout);
		    if(cout!='' && cout!=undefined)
			 {
		    	count=cout;
		       	//alert(count);
			 }
		    $('select').material_select();
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
            $(".eye-slash1").hide();
            $(".eye1").click(function() {
                $(".eye1").hide();
                $(".eye-slash1").show();
                $("#Cnfrmpassword").attr("type", "text");
            });
            $(".eye-slash1").click(function() {
                $(".eye-slash1").hide();
                $(".eye1").show();
                $("#Cnfrmpassword").attr("type", "password");
            });
           
            /* only number allowed with decimal */
            $('#basicSalary').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('basicSalary').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }
           
        });
        
       
    </script>
    <style>
       
       h6{
       font-size:0.9rem !important;
       }
       @media only screen and (min-width:601px){
            .passwordEyeDiv{
                transform:translate(0,10px);
            }
            .eye .eye-slash .eye1 .eye-slash1 {
            transform: translateY(-16%);
        }
        }
        @media only screen and (max-width:600px){
            .passwordEyeDiv{
                transform:translate(0,-53px);
            }
        }
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateEmployee" method="post" id="updateEmployeeForm">
           <!--  <input id="areaListIds" type="hidden" class="validate" name="areaIdLists"> -->
            <input id="employeeId" type="hidden" class="validate" name="employeeId" value="<c:out value="${employeeDetails.employee.employeeId}" />">
            <input id="employeeDetailsId" type="hidden" class="validate" name="employeeDetailsId" value="<c:out value="${employeeDetails.employeeDetailsId}" />">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Personal Details </h4>
                    </div>
                    <div class="row">
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="employeeName" value="<c:out value="${employeeDetails.name}" />" required>
                        <label for="name" class="active"><span class="red-text">*&nbsp;</span>Name</label>

                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" id="mobileNo" class="validate" name="mobileNumber"  minlength="10" maxlength="10" value="<c:out value="${employeeDetails.contact.mobileNumber}" />" required>
                        <label for="mobileNo" class="active"><span class="red-text">*&nbsp;</span>Mobile No.</label>
                        <span id="mobileNumberlabel" class="red-text"></span>
                    </div>
                    </div>
					<div class="row">
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" class="validate" name="emailId" value="<c:out value="${employeeDetails.contact.emailId}" />">
                        <label for="emailId" class="active">Email Id</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea" name="address" required><c:out value="${employeeDetails.address}" /></textarea>
                        <label for="textarea1"><span class="red-text">*&nbsp;</span>Address</label>
                    </div>
                    </div>
                </div>
               
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                    <div class="row" style="margin-bottom:5px">
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="fa fa-money prefix" aria-hidden="true"></i>
                        <input id="basicSalary" type="text" class="validate" name="basicSalary" value="<c:out value="${employeeDetails.basicSalary}" />" required>
                        <label for="basicSalary" class="active"><span class="red-text">*&nbsp;</span>Basic Salary</label>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">view_stream</i>
                        <!--  <label for="departmentid" class="active"></label> -->
                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Department</span>
                        <select id="departmentid" name="departmentId" required>
                                 <option value="" selected>Department</option>
                                <c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
								<option value="<c:out value="${listValue.departmentId}" />"  <c:if test="${listValue.departmentId == employeeDetails.employee.department.departmentId}">selected</c:if>><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                   </div>
                    <div class="row" style="margin-bottom:5px;">
                  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                  		<c:set var="branchIdsOld" value="${branchIds}" />
                  		<%
                  		List<Long> branchIds =  (List<Long>) pageContext.getAttribute("branchIdsOld");
                  		%>
                        <i class="material-icons prefix">location_on</i>    
                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Branch</span>
                        <select name="branchIds" id="branchIds" required="" aria-required="true" multiple>
                                 <option value="" disabled>Branch</option>
                                 <c:if test="${not empty branchList}">
									<c:forEach var="listValue" items="${branchList}">
										<c:set var="branchId" value="${listValue.branchId}" />
										<%
											long branchId=(long)pageContext.getAttribute("branchId");
										%>
										<option value="<c:out value="${listValue.branchId}" />" <%=branchIds.contains(branchId)?"selected":""%>><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div> 
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                  		<c:set var="routeIdsOld" value="${routeIds}" />
                  		<%
                  		List<Long> routeIds =  (List<Long>) pageContext.getAttribute("routeIdsOld");

                  		%>
                        <i class="material-icons prefix">location_on </i>    
                        <span class="selectLabel">Select Route</span>
                        <select name="routeIds" id="routeIds"  multiple>
                                 <option value="" disabled>Route</option>
                                 <c:if test="${not empty routeList}">
									<c:forEach var="listValue" items="${routeList}">
										<c:set var="routeId" value="${listValue.routeId}" />
										<%
											long routeId=(long)pageContext.getAttribute("routeId");
										%>
										<option value="<c:out value="${listValue.routeId}" />" <%=routeIds.contains(routeId)?"selected":""%>><c:out
												value="${listValue.routeName}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div> 
                    </div>
                  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select City</span>
                        <select name="cityid" id="cityList">    
                                 <option  value="" selected>City</option>
                                  <c:if test="${not empty cityList}">
										<c:forEach var="listValue" items="${cityList}">
											<option value="<c:out value="${listValue.cityId}" />"><c:out
													value="${listValue.name}" /></option>
										</c:forEach>
									</c:if> 
                        </select>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">track_changes</i>
                        <span class="selectLabel"><span class="red-text">*&nbsp;</span>Select Region</span>
                        <select name="regionId" id="regionList">
                                 <option value="" selected>Region</option>
                        </select>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">business<span class="red-text">*&nbsp;</span></i>
                        
                        <select name="AreaId"  id="areaList" class="active">
                                <option value="" selected>Area</option>
                        </select>
                    </div>
                    <!--<div class="input-field col s12 m6 l1 push-l1 ">
                        <button class="btn waves-effect waves-light blue darken-8" type="button">Ok</button>
                    </div>-->
                   <div class="input-field col s12 m6 l6 offset-l3 offset-m3 center-align">
                        <table class="centered tblborder" id="areatable">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Area </th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                            <% int rowincrement=0; %>
						
						
                            <c:if test="${not empty employeeAreaList}">
								<c:forEach var="employeeArea" items="${employeeAreaList}">
								<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                                	<tr id='rowdel_${rowincrement}'> 
		               				<td id='rowcount_${rowincrement}'> <c:out value="${rowincrement}" /> </td>
		               				<td id='rowproductname_${rowincrement}'><input type='hidden' id='rowproductkey_${rowincrement}' value='<c:out value="${employeeArea.areaId}" />'><center><span id='tbproductname_${rowincrement}'><c:out value="${employeeArea.name}" /></span></center></td>
		               				<td id='rowdelbutton_${rowincrement}'><button class='btn-flat' type='button' onclick='deleterow(${rowincrement})'><i class='material-icons '>clear</i></button></td>
		               				</tr>
		               			</c:forEach>
                            </c:if>
                            </tbody>
                        </table>
                        <br><br>
                        <input id="areaListIds" type="hidden" class="validate" title="Add atleast one Area" name="areaIdLists" required>
                    </div>

                </div>

                <%-- <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Login Credentials </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="userId" type="text"  name="userId" required value="<c:out value="${employeeDetails.employee.userId}" />" readonly>
                        <label for="userId" id="useridvalid"  class="active"><span class="red-text">*&nbsp;</span>User Id</label>
					 </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">

                        <i class="material-icons prefix">vpn_key</i>

                        <input id="password"  type="text" name="password" required value="<c:out value="${employeeDetails.employee.password}" />" readonly>
                        <label for="password" class="active"><span class="red-text">*&nbsp;</span>Password</label>

                    </div>
                    
                </div> --%>
                
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Login Credentials </h4>
                    </div>
                    <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="userId" type="text" class="validate" name="userId" value="<c:out value="${employeeDetails.employee.userId}" />" required>
                        <label for="userId" id="useridvalid" data-error="" data-success="" class="active"><span class="red-text">*&nbsp;</span>User Id</label>
					 </div>	
					               
					             

                    <div class="input-field col col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">

                        <i class="material-icons prefix">vpn_key</i>
                        <input id="password"  type="password" name="password"  value="<c:out value="${employeeDetails.employee.password}" />" required>
                        <label for="password" class="active"><span class="red-text">*&nbsp;</span>Password</label>

                    </div>
                   <div class="input-field col s12 m1 l1 push-l1 push-m1 passwordEyeDiv">
                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                    </div>

                   <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                        <i class="material-icons prefix">vpn_key</i>

                        <input id="Cnfrmpassword" type="password" name="confirmpass"  value="<c:out value="${employeeDetails.employee.password}" />" required style="margin-bottom:5px;">
                        <label for="Cnfrmpassword" class="active"><span class="red-text">*&nbsp;</span>Confirm Password</label>
                        <span id="CnfrmpasswordLabel" class="red-text"></span>
                         
                    </div>
                     <div class="input-field col s12 m1 l1 push-l1 push-m1 passwordEyeDiv">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    </div>
                  
                </div>
                
                <div class="row z-depth-3">
                   <div class="col s12 l12 m12">
                   
                   <br>
                       <div class="col s12 l5 m5 push-l1 push-m1">
                           <h6><b>Added Date : </b>
                           <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${employeeDetails.employeeDetailsAddedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${employeeDetails.employeeDetailsAddedDatetime}" /><c:out value="${time}" />                           
                           </h6>
                       </div>
                       <div class="col s12 l5 m5 push-l1 push-m1">
                           <h6><b>Last Updated Date :</b> 
							<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${employeeDetails.employeeDetailsUpdatedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${employeeDetails.employeeDetailsUpdatedDatetime}" /><c:out value="${time}" />                           
                           </h6>
                       </div>
                       <br> <br>
                   </div>
                    
               </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue darken-8" type="submit" id="updateEmployeeSubmit">Update Employee<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>
	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
                            <div class="col s12 center">
                                    <a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
                            </div>
                            
                        </div>
				</div>
			</div>
		</div>


	
    </main>
    <!--content end-->
</body>

</html>