<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

    <script>
        $(document).ready(function() {
        	/*  var dateDisable = $('.datepicker').pickadate();
        	var picker = dateDisable.pickadate('picker');
        	   var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        	   picker.set( 'disable', { from: [0,0,0], to: yesterday } ); */
        	  
        	   
        	   $('.otherPayMode').hide();
          	 
               $('#cash').click(function() {
            	   
	               	$("#cheqNo").removeAttr("required","required");
	               	$("#bankName").removeAttr("required","required");
	               	
	               	$("#paymentMethodId").removeAttr("required","required");
	               	$("#transactionRefNo").removeAttr("required","required");
	               //	$("#comment").removeAttr("required","required");
	               	
	               	$('.otherPayMode').hide();     
	               	$('.chequeNo').hide();
                    $('#paymentType').val('Cash'); 
                    
               });
               $('#cheque').click(function() {
            	   
	               	$("#cheqNo").attr("required","required");
	               	$("#bankName").attr("required","required");
	               	
	               	$("#paymentMethodId").removeAttr("required","required");
	               	$("#transactionRefNo").removeAttr("required","required");
	               //	$("#comment").removeAttr("required","required");
	               	
	               	$('.otherPayMode').hide();
                   $('.chequeNo').show();
                   $('#paymentType').val('Cheque');
                   
               });
               $('#other').click(function() {
	               	$("#cheqNo").removeAttr("required","required");
	               	$("#bankName").removeAttr("required","required");
	               	
	               	$("#paymentMethodId").attr("required","required");
	               	$("#transactionRefNo").attr("required","required");
	               //	$("#comment").attr("required","required");               	
               	
                   $('.otherPayMode').show();
                   $('.chequeNo').hide();
                   $('#paymentType').val('Other');
               });
            $('#cheqNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
            /* allowed only number with decimal allowed */
            $('#amount').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('amount').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
            
            /* select cash / cheque section according paymentType*/
			var paymentType="${expense.type}";
        	if(paymentType=="Cash"){
        		$('#cash').click();
        	}else if(paymentType=="Cheque"){
        		$('#cheque').click(); 
        	}else{
        		$('#other').click();
        	}
        });
    </script>
    <style>
    
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40% !important;
        	background-color:#0073b7 !important;
        }
	.card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
    </style>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> 
       	 <div class="container">
            <form action="${pageContext.request.contextPath}/updateExpense" method="post" id="saveExpenseForm">
            <input type="hidden" name="expenseId" value="${expense.expenseId}">
                <div class="row  z-depth-3">
                <!-- <div class="col s12 m11 l11 push-l1 push-m1 center">
	                    	 <h5> Add Expense</h5>
	                    </div>
						 -->
						 <br>
                	<div class="row" style="margin-bottom:0">
	                     <div class="input-field col s6 m5 l5 push-l1 push-m1" id="unitprice">
							 <!--<i class="material-icons prefix">edit</i>
	                         <input id="description" type="text" class="validate"  name="description" required>
	                        <label for="description" class="active"><span class="red-text">*</span>Description</label>    -->
	                        <i class="material-icons prefix">view_stream <span class="red-text">*</span></i>                        
	                        <select id="expenseTypeId" name="expenseTypeId" required="" aria-required="true" >
	                                 <option value="" > Expense Type</option>
	                                <c:if test="${not empty expenseTypeList}">
										<c:forEach var="listValue" items="${expenseTypeList}">
											<option value="<c:out value="${listValue.expenseTypeId}" />" ${listValue.expenseTypeId==expense.expenseTypeId?'selected':''}><c:out
													value="${listValue.name}" /></option>
										</c:forEach>
									</c:if>
	                        </select>                       
	                    </div>
	                <!--  </div>
	                 <div class="row" style="margin-bottom:0"> -->
	                     
	                    <!--  <div class="input-field col s12 m5 l5 push-l1 push-m1">
							 <i class="material-icons prefix">event</i>
	                        <input id="expenseDate" type="text" class="datepicker"  name="expenseDate" required>
	                        <label for="expenseDate" class="active"><span class="red-text">*</span>Expense Date</label>                       
	                    </div> -->
	                    <div class="input-field col s6 m5 l5 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">chrome_reader_mode</i>
	                        <input id="reference" type="text" class="validate"  name="reference" value="${expense.reference}" required>
	                        <label for="reference" class="active"><span class="red-text">*</span>Reference</label>                       
	                    </div>
	                    </div>
	                    <div class="row">
	                    <div class="col s12 m11 l11 push-l1 push-m1">
	                    <br>
	                    	
	                    </div>
	                    
	                    <div class="col s12 m5 l5 push-l1 push-m1 chqcash" style="margin-top:10px;">
	                    
                         <h6> Payment Mode</h6>
                        <input type="hidden" name="type" id="paymentType" value="">
                        <input type="radio" id="cash" name="group1" checked /> 
                        <label for="cash">Cash</label>
                        <input type="radio" id="cheque" name="group1"/> 
                        <label for="cheque">Cheque</label>
                        <input type="radio" id="other" name="group1"/> 
	                    <label for="other">Other</label>
                   		 </div>
                   		 <div class="input-field col s12 m5 l5 push-l1 push-m1  amount1">
                   		 <i class="fa fa-inr prefix"></i>
                        <input type="text" name="amount" id="amount" title="Enter Amount" value="${expense.amount}" required>
                        <label for="amount"><span class="red-text">*</span>Amount</label>
                    	</div>
                   		  <div class="input-field col s12 m5 l5 push-l1 push-m1 chequeNo">
                   		  <i class="material-icons prefix">account_balance</i>
                        	<input type="text" name="bankName" class="validate" value="${expense.bankName}" id="bankName" title="Enter Bank Name"> <label for="customerBankName">Bank Name</label>
                    	</div>
                      	
                    	<div class="input-field col s12 m5 l5 push-l1 push-m1   chequeNo">
                        <i class="material-icons prefix">featured_play_list</i>
                        <input type="text" name="chequeNumber" class="validate num" value="${expense.chequeNumber}" id="cheqNo" maxlength="6" minlength="6" title="Enter Cheque Number">
                         <label for="customerChequeNumber">Cheque Number</label>
                   	 </div>
                   	 <div class="row otherPayMode">
		                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
		                    <i class="material-icons prefix">account_balance_wallet</i>
		                        <select id="paymentMethodId" name="paymentMethodId" required>
									<option value="0"  selected><span class="red-text">*</span>Choose Payment Method  </option>
									<c:if test="${not empty paymentMethodList}">
										<c:forEach var="paymentMethod" items="${paymentMethodList}" varStatus="status">
											<option value="${paymentMethod.paymentMethodId}" ${paymentMethod.paymentMethodId==expense.paymentMethodId?'selected':''}>${paymentMethod.paymentMethodName}</option>
										</c:forEach>
									</c:if>
								</select>
		                    </div>
		                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
		                    <i class="material-icons prefix">assignment</i>
		                        <input id="transactionRefNo" name="transactionRefNo" type="text" class="" title="" value="${expense.transactionReferenceNumber}"> 
								<label	for="transactionRefNo"><span class="red-text">*</span>Transaction Ref. No.</label><br>
		                    </div>	
		                 </div>
		                 <div class="row otherPayMode">
		                    <div class="input-field col s10 m10 l10 push-l1 push-m1">
		                    	 <i class="material-icons prefix">description</i>
		                        <textarea id="comment" type="text" name="comment" class="materialize-textarea" title="Enter Comment" >${expense.comment}</textarea>
		                        <label for="comment">Comment</label>
		                    </div>
                    	</div>
	                    </div>
	                   <div class="input-field col s6 m12 l12 center center-align">
                        <button id="submit" class="btn waves-effect waves-light" type="submit">Submit
                                </button>
                                <br><br>
                    </div> 
                </div>
            </form>
         </div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="modal-footer">
							<div class="col s12 center">
									<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
							</div>
							
						</div>
				</div>
			</div>
		</div>     
    </main>





    <!--content end-->
</body>

</html>