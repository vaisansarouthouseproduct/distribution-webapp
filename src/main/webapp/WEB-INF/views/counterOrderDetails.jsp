<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
			<html>

			<head>
				<%@include file="components/header_imports.jsp" %>

					<script type="text/javascript">

						function viewProducts(url) {
							$.ajax({
								url: url,
								dataType: "json",
								success: function (data) {
									//alert(data);
									$("#tbproductlist").empty();
									var srno = 1;
									for (var i = 0, len = data.length; i < len; ++i) {
										var supplierproduct = data[i];
										$("#tbproductlist").append("<tr>" +
											"<td>" + srno + "</td>" +
											"<td>" + supplierproduct.product.productName + "</td>" +
											"<td>" + supplierproduct.product.categories.categoryName + "</td>" +
											"<td>" + supplierproduct.product.brand.name + "</td>" +
											"<td>" + supplierproduct.supplierRate + "</td>" +
											"</tr>");
										srno++;
									}


									$('.modal').modal();
									$('#viewDetails').modal('open');
									//alert("data came");
									return false;
									/* for (index = 0; index < options.length; ++index) {
									  option = options[index];
									  select.options.add(new Option(option.name, option.cityId));
									} */
								},
								error: function (xhr, status, error) {
									//var err = eval("(" + xhr.responseText + ")");
									//alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
									$('#msgHead').text("Manage Supplier Message");
									$('#msg').text("Not Have Product List");
								}
							});
						}
						$(document).ready(function () {

							$("select")
								.change(function () {
									var t = this;
									var content = $(this).siblings('ul').detach();
									setTimeout(function () {
										$(t).parent().append(content);
										$("select").material_select();
									}, 200);
								});
							$('select').material_select();
							$('.dataTables_filter input').attr("placeholder", "Search");
						});
						function openReason(reason){
					   		$('#reasonText').text(reason);  
					   		$('#reasonModal').modal('open');
					   	}
					</script>

					<style>
						.card-panel p {
							font-size: 15px !important;
						}
					</style>
			</head>

			<body>
				<!--navbar start-->
				<%@include file="components/navbar.jsp" %>
					<!--navbar end-->
					<!--content start-->
					<main class="paddingBody">


						<div class="row">
							<br>
							<br>
							<div class="col s12 l12 m12 ">
								<div class="scrollDivTable">
									<table class="striped highlight bordered centered tblborder" id="" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="print-col">Sr. No.</th>
												<!--   <th class="print-col">Sales Person Name</th> -->
												<th class="print-col">Category Name</th>
												<th class="print-col">Brand Name</th>
												<th class="print-col">Product Name</th>
												<th class="print-col">MRP</th>
												<th class="print-col">Qty</th>
												<c:if test="${report=='counterOrder'}">
													<th class="print-col">Return Qty</th>
												</c:if>
												<th class="print-col">Total Amt</th>
												<th class="print-col">Disc(%)</th>
												<th class="print-col">Disc. Amt.</th>
												<th class="print-col">Net Amount</th>
												<!-- <th class="print-col">Order Date</th> -->
												<c:if test="${report=='returnCounterOrder'}">
													<th>Reason</th>
												</c:if>
											</tr>
										</thead>

										<tbody>
											<c:if test="${not empty counterOrderProductDetailListForWebApps}">
												<c:forEach var="listValue" items="${counterOrderProductDetailListForWebApps}">
													<tr>
														<td>
															<c:out value="${listValue.srno}" />
														</td>
														<%-- <td><a title="View Sales person Details" href="getEmployeeView?employeeDetailsId=${listValue.employeeId}"><c:out value="${listValue.salesPersonName}" /></a></td><!-- ManageSupplier.html --> --%>
															<td class="wrapok">
																<c:out value="${listValue.categoryName}" />
															</td>
															<td class="wrapok">
																<c:out value="${listValue.brandName}" />
															</td>
															<td class="wrapok">
																<c:out value="${listValue.productName}" />
																<font color="green">
																	<c:out value="${listValue.type=='Free'? '-(Free)':''}" />
																</font>
															</td>
															<td>
																<c:out value="${listValue.rateWithTax}" />
															</td>
															<td>
																<c:out value="${listValue.quantity}" />
															</td>
															<c:if test="${report=='counterOrder'}">
																<td>
																	<c:out value="${listValue.returnQuantity}" />
																</td>
															</c:if>
															<td>
																<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax}"
																/>
															</td>
															<td>
																<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.discount}"
																/>
															</td>
															<td>
																<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.discountAmt}"
																/>
															</td>
															<td>
																<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.netAmount}"
																/>
															</td>
															<c:if test="${report=='returnCounterOrder'}">
																<td><button area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Reason" onclick="openReason('${listValue.reason}')" class="btn-flat tooltipped">Reason</button></td>
															</c:if>
													</tr>
													
												</c:forEach>
											</c:if>
										</tbody>
										<tfoot class="centered">
											<tr>
												<th class="center" colspan="5">Total</th>
												<th>
													<center>
														<c:out value="${totalQuantity}" />
													</center>
												</th>
												<c:if test="${report=='counterOrder'}">
													<th>
														<center>
															<c:out value="${totalReturnQuantity}" />
														</center>
													</th>
												</c:if>
												<th>
													<center>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTaxMain}"
														/>
													</center>
												</th>
												<th></th>
												<th>
													<center>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDiscountAmount}" />
													</center>
												</th>
												<th>
													<center>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" />
													</center>
												</th>
												<c:if test="${report=='returnCounterOrder'}">
													<th></th>
												</c:if>
											</tr>
											<tr>
												<th class="center" colspan="5">Discount </th>
												<th></th>
												<c:if test="${report=='counterOrder'}">
													<th></th>
												</c:if>
												<th></th>
												<th>
													<center>${discountPerCut}</center>
												</th>
												<th></th>
												<th>
													<center>${discountAmtCut}</center>
												</th>
												<c:if test="${report=='returnCounterOrder'}">
													<th></th>
												</c:if>
											</tr>
											<tr>
												<th class="center" colspan="5">Net Total Amount</th>
												<th></th>
												<c:if test="${report=='counterOrder'}">
													<th></th>
												</c:if>
												<th></th>
												<th></th>
												<th></th>
												<th>
													<center>(${totalAmountWithTaxWithDisc})</center>
												</th>
												<c:if test="${report=='returnCounterOrder'}">
													<th></th>
												</c:if>
											</tr>
											<tr>
												<th class="center" colspan="5">Round Off </th>
												<th></th>
												<c:if test="${report=='counterOrder'}">
													<th></th>
												</c:if>
												<th></th>
												<th></th>
												<th></th>
												<th>
													<center>(${roundOffAmount})</center>
												</th>
												<c:if test="${report=='returnCounterOrder'}">
													<th></th>
												</c:if>
											</tr>
											<tr>
												<th class="center" colspan="5">Net Payable</th>
												<th></th>
												<c:if test="${report=='counterOrder'}">
													<th></th>
												</c:if>
												<th></th>
												<th></th>
												<th></th>
												<th>
													<center>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetPayable}" />
													</center>
												</th>
												<c:if test="${report=='returnCounterOrder'}">
													<th></th>
												</c:if>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>

						<!-- Modal Structure for View Product Details -->
						<div id="product" class="modal ">
							<div class="modal-content">
								<h5 class="center">
									<u>Product Details</u> <i class="material-icons modal-close right">clear</i>
								</h5>

								<br>
								<table border="2" class="centered tblborder">
									<thead>
										<tr>
											<th>Sr.No</th>
											<th>Product Name</th>
											<th>Category</th>
											<th>Brand</th>
											<th>Quantity</th>
											<th>Rate</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Mouse</td>
											<td>I.T</td>
											<td>BlueSquare</td>
											<td>5</td>
											<td>350</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="modal-footer row">

								<div class="col s12 m6 l6 offset-l1">
									<a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
								</div>

							</div>
						</div>
		<div class="row">
			<div class="col s8 m2 l2">
				<div id="reasonModal" class="modal deleteModal row">
					<div class="modal-content">
					<h5 class="center-align" style="margin-bottom:30px"><u>Reason</u></h5>
					
						<%-- <h5 id="msgHead">Reason of ${listValue.id} is :</h5>
						<hr> --%>	
						
						<h6 id="msg" class="center-align"><b>Reason: </b><span id="reasonText"></span></h6>
						  <br/>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
					</main>
					<!--content end-->
			</body>

			</html>