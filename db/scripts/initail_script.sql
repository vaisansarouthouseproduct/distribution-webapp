
--insert company details for add admin
INSERT INTO COMPANY (company_id, company_name,password, user_id)
values (0,'admin','admin','admin')

--insert employee login for admin
insert into employee (password,user_id,company_id,department_id)
values ('admin','admin',0,4)

--insert departments
INSERT INTO `department` (`department_id`, `department_added_datetime`, `details`, `name`, `short_name_for_employee_id`) VALUES
(1, NULL, NULL, 'SalesMan', 'SM'),
(2, NULL, NULL, 'GateKeeper', 'GK'),
(3, NULL, NULL, 'DeliveryBoy', 'DB'),
(4, NULL, NULL, 'Admin', 'AD');

--insert app_version data
INSERT INTO `app_version` (`app_version_id`, `app_version`) VALUES ('1', '1.1');

--insert payment methods
INSERT INTO `payment_method` (`payment_method_id`, `payment_method_name`) VALUES
(1, 'Net Banking'),
(2, 'Paytm'),
(3, 'RTGS'),
(4, 'NEFT'),
(5, 'IMPS'),
(6, 'Debit/Credit Card'),
(7, 'Other wallet');

-- insert order status
INSERT INTO `order_status` (`order_status_id`, `Order_status_added_datetime`, `details`, `status`) VALUES
(1, NULL, NULL, 'Booked'),
(2, NULL, NULL, 'Packed'),
(3, NULL, NULL, 'Issued'),
(4, NULL, NULL, 'Delivered'),
(5, NULL, NULL, 'Delivered Pending'),
(6, NULL, NULL, 'Cancelled');

